<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 09/03/16
 * Time: 2:28 PM
 */
/*GET rucksack request
    $api_url='https://rucksack.entrepreneurscircle.org/post.php'
*/
if (!function_exists('rucksack_request'))
{
    function rucksack_request($action, $action_details, $Local = true,$QueryFields=[]){

        switch ($action){
            case "addcon_is":
                $action = "addContact";
                break;
            case "apply_tag":
                $action = "applyTag";
                break;
            case "create_is":
                $action = "createRecord";
                break;
            case "update_is":
                $action = "updateRecord";
                break;
            case "query_is":
                $action = "readRecord";
                break;
            case "delete_is":
                $action = "deleteRecord";
                break;
            case "email_is":
                $action = "sendEmail";
                break;
            case "email_template_is":
                $action = "getEmailTemplate";
                break;
            case "saved_search_columns_is":
                $action = "getAllReportColumns";
                break;
            case "saved_search_report_is":
                $action = "getSavedSearchResultsAllFields";
                break;
            case "setting_is":
                $action = "getAppSetting";
                break;
            case "webform_html_is":
                $action = "getFormsHTML";
                break;
            case "webform_map_is":
                $action = "getWebFormMap";
                break;
            default:
                break;

        }
        //$CI =& get_instance();
        //$CI->load->library('rucksack/rucksack');
        require_once (APPPATH."libraries/rucksack/Rucksack.php");
        $Rucksack = new Rucksack();
        $return = new stdClass();
        $CI =& get_instance();
        $CI->load->helper('macanta_helper');
        $CI->load->helper('infusionsoft_helper');
        $CI->db->reset_query();
        $HasLocalIds = ["Company","Contact","ContactAction","ContactGroup"];
        $LocalCall = ["addContact","applyTag","createRecord","updateRecord","readRecord","restRetrieveFile","restEmailHistory"];
        $defaultTimeZone = 'UTC';
        if (date_default_timezone_get() != $defaultTimeZone) date_default_timezone_set($defaultTimeZone);
        $action_details_arr =  json_decode($action_details, true);
        if(isset($action_details_arr['table'])) $Table = $action_details_arr['table'];
        if(isset($action_details_arr['sync'])) $Sync = $action_details_arr['sync'];
        if(isset($action_details_arr['id'])) $Id = $action_details_arr['id'];
        if(isset($action_details_arr['limit'])) $Limit = (int) $action_details_arr['limit'];
        if(isset($action_details_arr['page'])) $Page = (int) $action_details_arr['page'];
        $OrderBy = isset($action_details_arr['order_by']) ? $action_details_arr['order_by']:false;
        $Order = isset($action_details_arr['order']) ? $action_details_arr['order']:null;
        if(isset($action_details_arr['fields'])){
            $Fields = $action_details_arr['fields'];
            $Fields = rucksack_remove_quote($Fields);
        }

        if($Local == true && in_array($action,$LocalCall)){
            $return_value =  new stdClass();

            $NoteCustomFields = json_decode($CI->config->item('NoteCustomFields'), true);
            $CF_MacantaNotes = '_' . $NoteCustomFields['JSON'];
            //echo $action_details;

            switch ($action){
                case "addContact":
                    $defaultTimeZone = 'Europe/London';
                    if (date_default_timezone_get() != $defaultTimeZone) date_default_timezone_set($defaultTimeZone);
                    $UTC_date = gmdate('Y-m-d H:i:s');
                    $UTC_date = date('Y-m-d H:i:s', strtotime($UTC_date) - 14400);
                    $LastUpdated = '{"date":"'.$UTC_date.'","timezone_type":3,"timezone":"UTC"}';
                    $Fields['LastUpdated'] = $LastUpdated;
                    $Fields['Origin'] = 'Local';
                    if(isset($Fields['Email']) && trim($Fields['Email']) != ''){
                        $CI->db->where('Email',$Fields['Email']);
                        $query = $CI->db->get("InfusionsoftContact");
                        if ($query->num_rows() > 0){
                            $existing_record = $query->result_array();
                            $CI->db->where('Email', $Fields['Email']);
                            $CI->db->update('InfusionsoftContact',$Fields);
                            $res = $existing_record[0]['Id'] ? $existing_record[0]['Id'] :$existing_record[0]['IdLocal'];
                        }
                        else{
                            $LocalId = rucksack_generate_local_id('InfusionsoftContact');
                            $Fields['IdLocal'] = $Fields['Id'] = $LocalId;
                            $CI->db->insert('InfusionsoftContact',$Fields);
                            $res = $Fields['IdLocal'];
                        }


                        if(isset($action_details_arr['optin'])){
                            //todo: Implement Optin
                            $OPTINFields = [
                                //'Email' =>
                            ];
                        }
                    }

                    break;
                case "applyTag":
                    //e.g $action_details = '{"tag":' . $TagId . ',"id":' . $ConId . '}';
                    $CI->db->where('Id',$action_details_arr['id']);
                    $CI->db->or_where('IdLocal',$action_details_arr['id']);
                    $query = $CI->db->get("InfusionsoftContact");
                    $res = false;
                    if ($query->num_rows() > 0){
                        foreach ($query->result() as $row)
                        {
                            $NEW_groups = $OLD_groups = $row->Groups == "" ? []:explode(',',$row->Groups);
                            $NEW_groups[] = $action_details_arr['tag'];
                            $Groups = implode(',',$NEW_groups);
                            $Fields['Groups'] = $Groups;
                            $CI->db->where('Id',$row->Id);
                            $CI->db->or_where('IdLocal',$row->Id);
                            $CI->db->update('InfusionsoftContact',$Fields);
                            $res = true;
                        }
                    }
                    else{
                        $CI->db->where('IdLocal',$action_details_arr['id']);
                        $query = $CI->db->get("InfusionsoftContact");
                        foreach ($query->result() as $row)
                        {
                            $NEW_groups = $OLD_groups = $row->Groups == "" ? []:explode(',',$row->Groups);
                            $NEW_groups[] = $action_details_arr['tag'];
                            $Groups = implode(',',$NEW_groups);
                            $Fields['Groups'] = $Groups;
                            $CI->db->where('Id',$row->Id);
                            $CI->db->or_where('IdLocal',$row->Id);
                            $CI->db->update('InfusionsoftContact',$Fields);
                            $res = true;
                        }
                    }
                    break;
                case "createRecord":
                    $defaultTimeZone = 'Europe/London';
                    $defaultTimeZone = 'UTC';
                    if ($CI->db->table_exists('Infusionsoft'.$Table) ==  false) {
                        $res = false;
                        break;
                    }
                    if (date_default_timezone_get() != $defaultTimeZone) date_default_timezone_set($defaultTimeZone);
                    $UTC_date = gmdate('Y-m-d H:i:s');
                    //$UTC_date = date('Y-m-d H:i:s', strtotime($UTC_date) - 14400);
                    $LastUpdated = '{"date":"'.$UTC_date.'","timezone_type":3,"timezone":"UTC"}';
                    $Fields['LastUpdated'] = $LastUpdated;
                    $Fields['Origin'] = 'Local';
                    if($Table == 'Company' || $Table == 'Contact')  $Fields['DateCreated'] = $LastUpdated;
                    if($Table == 'ContactAction'){
                        $Fields['CreationDate'] = $LastUpdated;
                        if(isset($Fields['ActionDate'])){
                            $Fields['ActionDate'] = '{"date":"'.$Fields['ActionDate'].'","timezone_type":3,"timezone":"UTC"}';
                        }
                        if(empty($Fields['ActionType'])) $Fields['ActionType'] = 'Other';

                        if($Fields['ActionType'] == 'MacantaTask')
                            $Fields['Origin'] = 'Native';
                            $Fields['CreationDate'] = $LastUpdated;

                    }

                    //Detect Customfields and add to CustomField column
                    $CustomFields = [];
                    $NewFields = [];
                    foreach ($Fields as $FieldName=>$FieldValue){
                        if($FieldName[0] == "_"){
                            $CustomFields[$FieldName] = $FieldValue;
                        }else{
                            $NewFields[$FieldName] = $FieldValue;
                        }
                    }
                    // make sure table has custom field column
                    //echo "Table: $Table";
                    $TableFields = $CI->db->list_fields('Infusionsoft'.$Table);
                    if(in_array('CustomField', $TableFields )) $NewFields['CustomField'] = json_encode($CustomFields);

                    if(!in_array('LastUpdated', $TableFields )) unset($NewFields['LastUpdated']);

                    $LocalId = rucksack_generate_local_id('Infusionsoft'.$Table);
                    if(!$LocalId) $LocalId = 1;
                    $NewFields['IdLocal'] = $NewFields['Id'] =  $LocalId;
                    $CI->db->insert('Infusionsoft'.$Table,$NewFields);
                    $res = $NewFields['IdLocal'];
                    break;
                case "updateRecord":
                    if(empty($Table)) break;
                    if ($CI->db->table_exists('Infusionsoft'.$Table) ==  false) {
                        $res = false;
                        break;
                    }
                    $res = false;
                    $defaultTimeZone = 'Europe/London';
                    $defaultTimeZone = 'UTC';
                    if (date_default_timezone_get() != $defaultTimeZone) date_default_timezone_set($defaultTimeZone);
                    $UTC_date = gmdate('Y-m-d H:i:s');
                    //$UTC_date = date('Y-m-d H:i:s', strtotime($UTC_date) - 14400);
                    //$UTC_date = _date('Y-m-d H:i:s', false, 'EST');
                    $LastUpdated = '{"date":"'.$UTC_date.'","timezone_type":3,"timezone":"UTC"}';
                    $Fields['LastUpdated'] = $LastUpdated;
                    $CustomFields = [];
                    $NewFields = [];
                    unset($Fields['CreationDate']);
                    if(isset($Fields['CompletionDate'])) $Fields['CompletionDate'] = base64_decode($Fields['CompletionDate']);
                    foreach ($Fields as $FieldName=>$FieldValue){
                        if($FieldName[0] == "_"){
                            $CustomFields[$FieldName] =  $FieldValue;
                        }else{
                            $NewFields[$FieldName] = $FieldValue;
                        }
                    }
                    // make sure table has custom field column
                    $TableFields = $CI->db->list_fields('Infusionsoft'.$Table);
                    if(in_array('CustomField', $TableFields ) && in_array('IdLocal', $TableFields )){
                        $CI->db->where('Id', $Id);
                        $CI->db->or_where('IdLocal', $Id);
                        $query = $CI->db->get('Infusionsoft'.$Table);
                        $row = $query->row();
                        if($row->CustomField != ''){
                            $OldCustomFields = json_decode($row->CustomField,true);
                            $NewCustomFields = array_merge($OldCustomFields,$CustomFields);
                            $CustomFields = $NewCustomFields;
                        }
                        $NewFields['CustomField'] = json_encode($CustomFields);
                    }



                    $KeyField = in_array($Table,$HasLocalIds) ? 'IdLocal':'Id';
                    $CI->db->where($KeyField, $Id);
                    $res['local'] = $CI->db->update('Infusionsoft'.$Table, $NewFields);
                    if($Table == "Contact" && $Sync == 'yes'){
                        //todo: Sync Infusionsoft after contact saved locally, check if existing, and if not restore all the info
                        try {
                            $result = $Rucksack->Call($action,$action_details);
                            $return = json_decode(trim($result['Data']));
                        } catch (Exception $e) {
                            $return->message = "Error: ".$e->getMessage();
                        }
                        $res['infusionsoft'] = $return->message;
                        //success results : {"local":true,"infusionsoft":554}
                        //non-contact : {"local":true,"infusionsoft":"Error: [DatabaseError]Error updating: [RecordNotFound]Unable to perform update. Record not found for Id:571"}

                        if($res['local'] == true && strpos($res['infusionsoft'], 'RecordNotFound') !== false){
                            $res['new_id'] = infusionsoft_add_contact_from_local($Id);
                        }
                    }

                    break;
                case "restRetrieveFile":
                    $res = [];
                    $URLonly = $action_details_arr['URLonly'] == 'yes' ? true:false;
                    $CI->db->where('Id', $action_details_arr['FileId']);
                    $query = $CI->db->get("InfusionsoftFileBox");
                    $row = $query->row();
                    if (isset($row)){
                        $res = json_decode($row->FileData);
                        $Filename = $res->infusionsoft->file_name;
                        $res = macanta_b2_download_file_by_name($row->ContactId, $Filename, "FILEBOX", $URLonly);
                        break;

                    }
                    break;
                case "restEmailHistory":
                    $res = new stdClass();
                    $ContactId = $action_details_arr['ContactId'];
                    $limit = $action_details_arr['limit'];
                    $offset = $action_details_arr['offset'];
                    $CI->db->where('contact_id', $ContactId);
                    $CI->db->limit($limit, $offset);
                    $res->emails = $CI->db->get("InfusionsoftEmailSent")->result_object();
                    break;
                case "readRecord":
                    $res = [];
                    if ($CI->db->table_exists('Infusionsoft'.$Table) ==  false) {
                        break;
                    }
                    $Offset = $Page * $Limit;
                    $CustomFields = [];
                    $TableFields = [];
                    $HasCustomField = false;
                    $Fields = $action_details_arr['fields'];
                    $Query = $action_details_arr['query'];
                    foreach ($Fields as $FieldKey => $Field){
                        if($Field[0] == '_' ) {
                            $HasCustomField = true;
                            $CustomFields[] = $Field;
                        }else{
                            $TableFields[] = $Field;
                        }
                    }
                    if($HasCustomField) $TableFields[] = 'CustomField';
                    $CI->db->select(implode(',',$TableFields));
                    if($QueryFields == []){
                        $Like=false;
                        foreach ($Query as $FieldName => $FieldValue){
                            if(in_array($Table,$HasLocalIds)) $FieldName = $FieldName=='Id'? 'IdLocal':$FieldName;
                            if (preg_match("/~(.*)~/", $FieldValue, $matches)) {
                                $Operator = $matches[1];
                                $FieldValue =  trim(str_replace($matches[0],'',$FieldValue));
                                if($Operator == "null"){
                                    $CI->db->where($FieldName,'');
                                }else{
                                    $Operator = $Operator=='<>' ? '!=':$Operator;
                                    if($Table == 'SavedFilter' && $FieldName == 'UserId' && $Operator == '!='){
                                        $CI->db->not_like($FieldName,$FieldValue);
                                    }else{
                                        $CI->db->where($FieldName." $Operator",$FieldValue);
                                    }

                                }
                            }elseif (preg_match("/%(.*)%/", $FieldValue, $matches)) {
                                $FieldValue =  trim(str_replace('%','',$FieldValue));
                                $CI->db->like($FieldName,$FieldValue, 'both');
                                $Like = true;
                            }elseif($FieldName == 'Id' && is_array($FieldValue)){
                                $CI->db->where_in('Id', $FieldValue);
                            }elseif($FieldName == 'GroupId' && is_array($FieldValue)){
                                $CI->db->where_in('GroupId', $FieldValue);
                            }elseif($FieldName == 'IdLocal' && is_array($FieldValue)){
                                $CI->db->where_in('IdLocal', $FieldValue);
                            }elseif($FieldName == 'ContactId' && is_array($FieldValue)){
                                $CI->db->where_in('ContactId', $FieldValue);
                            }elseif($FieldValue == '%'){
                                // disregard where
                            }else{
                                $CI->db->where($FieldName,$FieldValue);
                            }


                        }
                    }else{
                        $SearchKey = '';
                        foreach ($Query as $FieldName => $FieldValue){
                            $FieldValue =  trim(str_replace('%','',$FieldValue));
                            $SearchKey = $FieldValue;
                            $CI->db->like($FieldName,$FieldValue, 'after');
                        }
                        /*foreach ($QueryFields as $QueryField){
                            $CI->db->or_like($QueryField,$SearchKey, 'after');
                        }*/

                    }

                    if($OrderBy) $CI->db->order_by("$OrderBy","$Order");

                    $query = $CI->db->get('Infusionsoft'.$Table, $Limit, $Offset);
                    if ($query && $query->num_rows() > 0){
                        foreach ($query->result() as $row) {
                            if($HasCustomField){
                                if($row->CustomField){
                                    $CustomFieldsArr = json_decode($row->CustomField,true);
                                    foreach ($CustomFields as $CustomField){
                                        if(isset($CustomFieldsArr[$CustomField]) ){
                                            $row->$CustomField = $CustomFieldsArr[$CustomField];
                                        }else{
                                            $row->$CustomField = '';
                                        }
                                    }
                                }
                            }
                            //Convert all json value to object for dates only
                            foreach ($row as $FieldName => $FieldValue){
                                if(isJson($FieldValue) && !is_numeric($FieldValue)){
                                     $row->$FieldName = json_decode($FieldValue);
                                }
                            }
                            unset($row->CustomField);
                            $res[] = $row;
                        }
                    }
                    break;
                default:
                    break;
            }
            $return_value->message = $res;
            $return = $return_value;

        }else{
            try {
                $result = $Rucksack->Call($action,$action_details);
                $return = json_decode(trim($result['Data']));
                if($action == 'addContact'){
                    //Sync Local to Infusionsoft
                    if(is_numeric($return->message)){
                        $TagAppliedOnContactCreate = $CI->config->item('tag_applied_on_contact_create');
                        if($TagAppliedOnContactCreate && !empty($TagAppliedOnContactCreate)){
                            $TagAppliedOnContactCreateArr = explode(',',$TagAppliedOnContactCreate);
                            foreach($TagAppliedOnContactCreateArr as $Tag){
                                $Tag = trim($Tag);
                                if(is_numeric($Tag)){
                                    infusionsoft_apply_tag($return->message, $Tag);
                                }
                            }


                        }
                        $exec_string = '/usr/bin/php /var/www/macanta/shared/services/sync.php '.$CI->config->item('MacantaAppName').' 2 '.$return->message;
                        exec($exec_string,$output);
                        //file_put_contents(dirname(__FILE__)."/output".$CI->config->item('MacantaAppName').".txt",$output."\n\n", FILE_APPEND);

                    }

                }

            } catch (Exception $e) {
                $return->message = "Error: ".$e->getMessage();
            }
        }
        return $return;
    }
}

if (!function_exists('rucksack_remove_quote'))
{
    function rucksack_remove_quote($Param){
        $NewValue = [];
        foreach ($Param as $FieldName => $FieldValue){
            $NewValue[str_replace('"', '', $FieldName)] = str_replace('"', '', $FieldValue);
        }
        return $NewValue;
    }
}

if (!function_exists('rucksack_generate_local_id'))
{
    function rucksack_generate_local_id($Table){
        $CI =& get_instance();
        $CI->load->helper('macanta_helper');
        $CI->load->helper('infusionsoft_helper');
        $LastId = $CI->db->select_max('IdLocal')->get($Table)->row();
        return $LastId->IdLocal + 2;
    }
}
