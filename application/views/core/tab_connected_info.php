<?php
$ConnectorRelationship = $this->config->item('ConnectorRelationship') ? json_decode($this->config->item('ConnectorRelationship')):[];
$availableRelationships = [];
$CurrentMacantaUser = macanta_get_user_access_by_id($session_data['InfusionsoftID']);
$CurrentMacantaUserLevel = $CurrentMacantaUser['Level'];
foreach ($ConnectorRelationship as $RelationshipItem){
    $availableRelationships[$RelationshipItem->Id] = ["RelationshipName"=>$RelationshipItem->RelationshipName,"RelationshipDescription"=>$RelationshipItem->RelationshipDescription];
}
?>
<script>
    $("button").parents(".panel-heading").addClass("has-btn");
</script>
<!--<div class="col-xs-12 col-sm-12 col-lg-12 ConnectedInfoPanel">-->
    <div class="panel panel-primary theConnectedInfoPanel">
        <div class="panel-heading "><h3 class="panel-title NoteName"><i class="fa fa-info"></i> <?php echo $title;?></h3></div>
        <div class="panel-body theConnectedInfoPanelBody">
            <div class="">
                <div class="tab-panel">
                    <h2 class="oppmainlabel">Connected <?php echo $title;?> <span></span></h2>
                    <div class="table-responsive">
                        <table id="UserConnectorInfoTable" data-guid="<?php echo $ConnectorTab['id'] ?>" class="UserConnectorInfoTable<?php echo $ConnectorTab['id'] ?> table table-hover table-striped table-bordered <?php echo $ConnectorTab['id'] ?>" cellspacing="0" >
                        <thead>
                        <?php
                        $Order = [];
                        $OrderId = [];
                        foreach ($ConnectorTab['fields'] as $field){
                            if($field["showInTable"] == "yes"){
                                $key = (int) $field["showOrder"];
                                if(isset($Order[$key])){
                                    while (isset($order[$key])){
                                        $key++;
                                    }
                                    $Order[$key] = $field["fieldLabel"];
                                    $OrderId[$key] = $field["fieldId"];
                                }else{
                                    $Order[$key] = $field["fieldLabel"];
                                    $OrderId[$key] = $field["fieldId"];
                                }
                            }
                        }
                        ksort($Order);
                        ksort($OrderId);
                        ?>
                        <tr>
                            <?php
                            if(sizeof($Order) > 0){
                                foreach ($Order as $field){
                                    echo "<th>$field</th>";
                                }
                            }else{
                                $count = 0;
                                foreach ($ConnectorTab['fields'] as $field){
                                    $count++;
                                    $OrderId[] = $field["fieldId"];
                                    echo "<th>$field[fieldLabel]</th>";
                                    if($count == 3) break;
                                }
                            }
                            ?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($UserValue as $itemId => $ValuesArr){
                            $UserField = $ValuesArr['value'];
                            $ConnectedContact = $ValuesArr['connected_contact'];
                            foreach ($ConnectedContact as $ContactId => $ContactDetails){
                                $this->db->select('Groups');
                                $this->db->where('Id',$ContactId);
                                $query = $this->db->get('InfusionsoftContact');
                                $row = $query->row();
                                if (isset($row))
                                {
                                    $Groups = $row->Groups;
                                    if ($Groups != '' && checkContactRestriction($Groups,$session_data)) unset($ConnectedContact[$ContactId]); // skip this contact when restricted
                                }
                            }
                            echo "<tr class='infoItem' data-itemid='$itemId' >";
                            $ShowItemId = true;
                            foreach ($OrderId as $fieldName){
                                $ItemId = '';
                                foreach ($ConnectorTab['fields'] as $field){
                                    if($field['fieldId'] == $fieldName){
                                        $Val = $field['defaultValue'];
                                        break;
                                    }
                                }
                                if(isset($UserField[$fieldName])){
                                    if(is_array($UserField[$fieldName])){
                                        $Val = isset($UserField[$fieldName]["id_".$ContactInfo->Id]) ? $UserField[$fieldName]["id_".$ContactInfo->Id]:$Val;
                                    }else{
                                        $Val = $UserField[$fieldName];
                                    }
                                }
                                if($ShowItemId) $ItemId = '<div class="cd-item-id">ID:<span>'.$itemId.'</span>  </div><div class="deleteDataObjectItemContainer"><button class=" nb-btn nb-primary icon-btn deleteDataObjectItem" data-id="'.$itemId.'" data-groupid="'. $ConnectorTab['id'] .'" data-type="dashboard"><i class="fa fa-trash-o" aria-hidden="true"></i></button></div>';
                                echo "<td><div class='DOtitle'>$Val</div>$ItemId</td>";
                                $ShowItemId = false;
                            }
                            echo "</tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                    </div>
                    <div class="flex-row">
	                    <button type="button" data-guid="<?php echo $ConnectorTab['id'] ?>" class="nb-nb-btn nb-primary icon-btn  link-connect-data" >
	                        <i class="fa fa-plus-square-o"></i> Connect Existing Data
	                    </button>
	                    <button type="button" data-guid="<?php echo $ConnectorTab['id'] ?>" class="nb-nb-btn nb-primary icon-btn  addUserConnectedInfo">
	                        <i class="fa fa-plus-square-o"></i> Add New <?php echo ucfirst($titleSingular) ?>
	                    </button>
                    </div><!-- flex-row -->
                </div>
                <div class="ConnectedContactsNoteContainer ConnectedContactsNoteContainer<?php echo $ConnectorTab['id'] ?>">
                    <h2 class="oppmainlabel">Connected Contact Notes <span></span></h2>
                    <table id="ConnectedContactsNote<?php echo $ConnectorTab['id'] ?>" class="ConnectedContactsNote table table-striped table-bordered" width="98%">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Contact</th>
                            <th>Type</th>
                            <th>Title</th>
                            <th>Description</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
            <div class="">
                <h2 class="label-lg oppmainlabel">Information Details</h2>
                <div  class="InformationDetailsContainer" >
                    <form class="FormUserConnectedInfo" data-itemid="" data-guid="<?php echo $ConnectorTab['id'] ?>">

                        <?php
                        echo "Connected Data=>";
                        $Sections = [];
                        $AlltagDetails = infusionsoft_get_tags();
                        $tagsArr = explode(',',$session_data['Groups']);
                        $ViewedContactTagArr = explode(',',$ContactInfo->Groups);
                        echo "<!--";
                        print_r($ViewedContactTagArr);
                        echo "-->";
                        foreach ($ConnectorTab['fields'] as $field){
                            $tagIdValue = trim($field['sectionTagId']);
                            if($tagIdValue == ''){
                                $Sections['General'][] = $field;
                            }else{
                                if($CurrentMacantaUserLevel == "administrator"){
                                    $key = array_search($tagIdValue, array_column($AlltagDetails, 'Id'));
                                    $theTag[0] = $AlltagDetails[$key];
                                    $tagDetails = $theTag;
                                    $Sections[$tagDetails[0]->GroupName][] = $field;
                                }else{
                                    if(in_array($tagIdValue,$tagsArr)  || in_array($tagIdValue,$ViewedContactTagArr)) {
                                        if(in_array($tagIdValue,$tagsArr)  && in_array($tagIdValue,$ViewedContactTagArr)){
                                            $key = array_search($tagIdValue, array_column($AlltagDetails, 'Id'));
                                            $theTag[0] = $AlltagDetails[$key];
                                            $tagDetails = $theTag;
                                            $Sections[$tagDetails[0]->GroupName][] = $field;
                                        }
                                    }else{
                                        $key = array_search($tagIdValue, array_column($AlltagDetails, 'Id'));
                                        $theTag[0] = $AlltagDetails[$key];
                                        $tagDetails = $theTag;
                                        $Sections[$tagDetails[0]->GroupName][] = $field;
                                    }
                                }
                            }
                        }
                        //sort($Sections);
                        ksort($Sections, SORT_NUMERIC);
                        //usort($Sections, "sort_obj_arr_tag_categories");
                        if(isset($Sections['General']) && sizeof($Sections['General']) > 0){
                            $Uncategorized = $Sections['General'];
                            echo "<fieldset class='cd-fields collapsible'>";
                            echo "<legend>General</legend>";
                            foreach ($Uncategorized as $field){
                                $jsonData = $field['contactspecificField'] == 'yes' ? 'data-jsonvalue="{}"':'';
                                $domId = str_replace("=","",base64_encode($field['fieldLabel']));
                                $required = $field['requiredField'] == 'yes' ? 'required':"";
                                $asterisk = $field['requiredField'] == 'yes' ? '<span style="color: red;">*</span>':"";
                                $Helper = trim($field['helperText']) == '' ? '':"<small id=\"txt".$domId."\" class=\"form-text text-muted\">".$field['helperText']."</small>";

                                if(strstr($field['fieldType'],'field_'))
                                {
                                    $defaultValue = '';
                                    $PrimaryFieldData = macanta_get_data_reference_values($field['fieldType']);
                                    sort($PrimaryFieldData);
                                    $options = "<option  value>Please Select One</option>";
                                    foreach ($PrimaryFieldData as $choice){
                                        $choiceVal = str_replace(['<','>','&'],['&lt;','&gt;','&amp;'],$choice);
                                        $choiceVal = html_entity_decode($choiceVal);
                                        if(empty($choiceVal)) continue;
                                        $selected = trim($field['defaultValue'])==trim($choice) ? "selected":"";
                                        $options.= "<option data-value='".strtolower($choiceVal)."' value='".$choiceVal."' $selected>$choice</option>";
                                    }
                                    echo "
									<div class=\"form-group\">
										<label for=\"".$domId."\">".$field['fieldLabel'].$asterisk."</label>
										<select    class=\"addGroup select input-select form-control\" 
												id=\"".macanta_generate_key($domId, $length=5)."\" 
												aria-describedby=\"txt".$domId."\" 
												data-default=\"".$defaultValue."\" 
												name=\"".$field['fieldId']."\"
												".$jsonData."
												placeholder=\"".$field['placeHolder']."\" disabled 
												$required>
										  ".$options."
										</select>
										$Helper
									 </div>									 
									";
                                }
                                else {

                                    switch ($field['fieldType']) {
                                        case 'Currency':
                                            echo "
                                        <div class=\"form-group\">
                                            <label for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                            <input  type='number'
                                                    step='any'
                                                    class=\"addGroup input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\"
                                                    disabled 
                                                    $required
                                                    >
                                            $Helper
                                         </div>
                                        ";
                                            break;

                                        case 'Text':
                                            echo "
                                        <div class=\"form-group\">
                                            <label for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                            <input  type=\"text\" 
                                                    class=\"addGroup input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\"
                                                    disabled 
                                                    $required
                                                    >
                                            $Helper
                                         </div>
                                        ";
                                            break;

                                        case 'TextArea':
                                            echo "
                                        <div class=\"form-group\">
                                            <label for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                            <textarea 
                                                    class=\"addGroup input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\" rows=\"3\" 
                                                    disabled  
                                                    $required>" . $field['defaultValue'] . "</textarea>
                                            $Helper
                                         </div>
                                        ";
                                            break;

                                        case 'Number':
                                            echo "
                                        <div class=\"form-group\">
                                            <label for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                            <input  type='number'
                                                    class=\"addGroup input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\" disabled 
                                                    $required>
                                            $Helper
                                         </div>
                                        ";
                                            break;

                                        case 'Email':
                                            echo "
                                        <div class=\"form-group\">
                                            <label for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                            <input  type='email'
                                                    class=\"addGroup input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\" disabled  
                                                    $required>
                                            $Helper
                                         </div>
                                        ";
                                            break;

                                        case 'Password':
                                            echo "
                                        <div class=\"form-group\">
                                            <label for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                            <input  type='password'
                                                    class=\"addGroup input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\" disabled 
                                                    $required>
                                            $Helper
                                         </div>
                                        ";
                                            break;

                                        case 'Select':
                                            $defaultValue = $field['defaultValue'];
                                            $choices = explode("\r\n", $field['fieldChoices']);
                                            $options = "<option  value>Please Select One</option>";
                                            foreach ($choices as $choice) {
                                                $choiceVal = str_replace(['<', '>', '&'], ['&lt;', '&gt;', '&amp;'], $choice);
                                                $choiceVal = html_entity_decode($choiceVal);
                                                $selected = trim($field['defaultValue']) == trim($choice) ? "selected" : "";
                                                $options .= "<option data-value='" . strtolower($choiceVal) . "' value='" . $choiceVal . "' $selected>$choice</option>";
                                            }
                                            echo "
                                        <div class=\"form-group\">
                                            <label for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                            <select    class=\"addGroup select input-select form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    data-default=\"" . $defaultValue . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\" disabled 
                                                    $required>
                                              " . $options . "
                                            </select>
                                            $Helper
                                         </div>
                                         
                                        ";
                                            break;

                                        case 'Checkbox':
                                            $choices = explode("\r\n", $field['fieldChoices']);
                                            $checkbox = '';
                                            $count = 0;
                                            foreach ($choices as $choice) {
                                                $FullValue = $choice;
                                                $choice = explode(';',$choice);
                                                $choice = $choice[0];
                                                $choiceVal = str_replace(['<', '>', '&'], ['&lt;', '&gt;', '&amp;'], $choice);
                                                $choiceVal = html_entity_decode($choiceVal);
                                                $choice = trim($choice);
                                                $Checked = $field['defaultValue'] == $choice ? "checked" : "";
                                                $count++;
                                                $checkbox .= "<div class=\"form-check\">
                                                          <span class=\"form-check-label\">
                                                            <input type=\"checkbox\" 
                                                            class=\"addGroup checkbox form-check-input\" 
                                                            name=\"$field[fieldId]\" 
                                                            id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                            value=\"" . $choiceVal . "\" 
                                                            data-fullvalue=\"" . strtolower($FullValue) . "\"  
                                                            data-value=\"" . strtolower($choiceVal) . "\"
                                                            " . $jsonData . "
                                                             disabled
                                                             >
                                                                $choice
                                                          </span>
                                                        </div>";
                                            }
                                            echo "
                                        <div class=\"form-group\">
                                        <label>$field[fieldLabel]</label>
                                        <input type=\"hidden\" name=\"checkbox_".str_replace('field_','',$field['fieldId']) ."\" value=\"".$field['fieldId']."\">
                                        $checkbox
                                        $Helper
                                      </div>
                                        ";
                                            break;

                                        case 'Radio':
                                            $choices = explode("\r\n", $field['fieldChoices']);
                                            $checkbox = '';
                                            foreach ($choices as $choice) {
                                                $FullValue = $choice;
                                                $choice = explode(';',$choice);
                                                $choice = $choice[0];
                                                $choiceVal = str_replace(['<', '>', '&'], ['&lt;', '&gt;', '&amp;'], $choice);
                                                $choiceVal = html_entity_decode($choiceVal);
                                                $choice = trim($choice);
                                                $Checked = $field['defaultValue'] == $choice ? "checked" : "";
                                                $checkbox .= "<div class=\"form-check\">
                                                          <span class=\"form-check-label\">
                                                            <input type=\"radio\"
                                                             class=\"addGroup radio form-check-input\" 
                                                             id=\"" . macanta_generate_key('radio_', 5) . "\"
                                                                " . $jsonData . "
                                                            name=\"$field[fieldId]\" id=\"$domId\"  data-fullvalue=\"" . strtolower($FullValue) . "\"  data-value=\"" . strtolower($choiceVal) . "\" value=\"" . $choiceVal . "\" {$Checked} disabled>
                                                                $choice
                                                          </span>
                                                        </div>";
                                            }
                                            echo "
                                        <div class=\"form-group\">
                                        <label>$field[fieldLabel]</label>
                                        $checkbox
                                        $Helper
                                      </div>
                                        ";

                                            break;

                                        case 'Date':
                                            $placeholder = $field['placeHolder'] == "" ? "YYYY-MM-DD" : $field['placeHolder'];
                                            echo "
                                        <div class=\"form-group\">
                                            <label for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                            <input  type='text'
                                                    class=\"addGroup infoDate input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $placeholder . "\" disabled 
                                                    $required>
                                            $Helper
                                         </div>
                                        ";
                                            break;

                                        case 'DateTime':
                                            $placeholder = $field['placeHolder'] == "" ? "YYYY-MM-DD HH:mm:ss" : $field['placeHolder'];
                                            echo "
                                        <div class=\"form-group\">
                                            <label for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                            <input  type='text'
                                                    class=\"addGroup infoDateTime input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $placeholder . "\" disabled 
                                                    $required>
                                            $Helper
                                         </div>
                                        ";
                                            break;

                                        case 'URL':
                                            echo "
                                        <div class=\"form-group\">
                                            <label for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . " <i class=\"fa fa-external-link connected-data-url\" data-field=\"" . $field['fieldId'] . "\" aria-hidden=\"true\"></i></label>
                                            <input  type='url'
                                                    class=\"addGroup input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\" disabled 
                                                    $required>
                                            $Helper
                                         </div>
                                        ";
                                            break;

                                        case 'Repeater':

                                            break;
                                    }
                                }

                            }
                            echo "</fieldset>";
                        }
                        unset($Sections['General']);
                        foreach ($Sections as $SectionName=>$theFields){
                            echo "<fieldset class='cd-fields collapsible collapsed'>";
                            echo "<legend>$SectionName</legend>";
                            foreach ($theFields as $field){
                                $jsonData = $field['contactspecificField'] == 'yes' ? 'data-jsonvalue="{}"':'';
                                $domId = str_replace("=","",base64_encode($field['fieldLabel']));
                                $required = $field['requiredField'] == 'yes' ? 'required':"";
                                $asterisk = $field['requiredField'] == 'yes' ? '<span style="color: red;">*</span>':"";
                                $Helper = trim($field['helperText']) == '' ? '':"<small id=\"txt".$domId."\" class=\"form-text text-muted\">".$field['helperText']."</small>";

                                if(strstr($field['fieldType'],'field_'))
                                {
                                    $defaultValue = '';
                                    $PrimaryFieldData = macanta_get_data_reference_values($field['fieldType']);
                                    sort($PrimaryFieldData);
                                    $options = "<option  value>Please Select One</option>";
                                    foreach ($PrimaryFieldData as $choice){
                                        $choiceVal = str_replace(['<','>','&'],['&lt;','&gt;','&amp;'],$choice);
                                        $choiceVal = html_entity_decode($choiceVal);
                                        if(empty($choiceVal)) continue;
                                        $selected = trim($field['defaultValue'])==trim($choice) ? "selected":"";
                                        $options.= "<option data-value='".strtolower($choiceVal)."' value='".$choiceVal."' $selected>$choice</option>";
                                    }
                                    echo "
									<div class=\"form-group\">
										<label for=\"".$domId."\">".$field['fieldLabel'].$asterisk."</label>
										<select    class=\"addGroup select input-select form-control\" 
												id=\"".macanta_generate_key($domId, $length=5)."\" 
												aria-describedby=\"txt".$domId."\" 
												data-default=\"".$defaultValue."\" 
												name=\"".$field['fieldId']."\"
												".$jsonData."
												placeholder=\"".$field['placeHolder']."\" disabled 
												$required>
										  ".$options."
										</select>
										$Helper
									 </div>									 
									";
                                }
                                else {
                                    switch ($field['fieldType']) {
                                        case 'Currency':
                                            echo "
                                        <div class=\"form-group\">
                                            <label for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                            <input  type='number'
                                                    step='any'
                                                    class=\"addGroup input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\"
                                                    disabled 
                                                    $required
                                                    >
                                            $Helper
                                         </div>
                                        ";
                                            break;

                                        case 'Text':
                                            echo "
                                        <div class=\"form-group\">
                                            <label for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                            <input  type=\"text\" 
                                                    class=\"addGroup input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\"
                                                    disabled 
                                                    $required
                                                    >
                                            $Helper
                                         </div>
                                        ";
                                            break;

                                        case 'TextArea':
                                            echo "
                                        <div class=\"form-group\">
                                            <label for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                            <textarea 
                                                    class=\"addGroup input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\" rows=\"3\" 
                                                    disabled  
                                                    $required>" . $field['defaultValue'] . "</textarea>
                                            $Helper
                                         </div>
                                        ";
                                            break;

                                        case 'Number':
                                            echo "
                                        <div class=\"form-group\">
                                            <label for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                            <input  type='number'
                                                    class=\"addGroup input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\" disabled 
                                                    $required>
                                            $Helper
                                         </div>
                                        ";
                                            break;

                                        case 'Email':
                                            echo "
                                        <div class=\"form-group\">
                                            <label for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                            <input  type='email'
                                                    class=\"addGroup input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\" disabled  
                                                    $required>
                                            $Helper
                                         </div>
                                        ";
                                            break;

                                        case 'Password':
                                            echo "
                                        <div class=\"form-group\">
                                            <label for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                            <input  type='password'
                                                    class=\"addGroup input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\" disabled 
                                                    $required>
                                            $Helper
                                         </div>
                                        ";
                                            break;

                                        case 'Select':
                                            $defaultValue = $field['defaultValue'];
                                            $choices = explode("\r\n", $field['fieldChoices']);
                                            $options = "<option  value>Please Select One</option>";
                                            foreach ($choices as $choice) {
                                                $choiceVal = str_replace(['<', '>', '&'], ['&lt;', '&gt;', '&amp;'], $choice);
                                                $choiceVal = html_entity_decode($choiceVal);
                                                $selected = trim($field['defaultValue']) == trim($choice) ? "selected" : "";
                                                $options .= "<option data-value='" . strtolower($choiceVal) . "' value='" . $choiceVal . "' $selected>$choice</option>";
                                            }
                                            echo "
                                        <div class=\"form-group\">
                                            <label for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                            <select    class=\"addGroup select input-select form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    data-default=\"" . $defaultValue . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\" disabled 
                                                    $required>
                                              " . $options . "
                                            </select>
                                            $Helper
                                         </div>
                                         
                                        ";
                                            break;

                                        case 'Checkbox':
                                            $choices = explode("\r\n", $field['fieldChoices']);
                                            $checkbox = '';
                                            $count = 0;
                                            foreach ($choices as $choice) {
                                                $FullValue = $choice;
                                                $choice = explode(';',$choice);
                                                $choice = $choice[0];
                                                $choiceVal = str_replace(['<', '>', '&'], ['&lt;', '&gt;', '&amp;'], $choice);
                                                $choiceVal = html_entity_decode($choiceVal);
                                                $choice = trim($choice);
                                                $Checked = $field['defaultValue'] == $choice ? "checked" : "";
                                                $count++;
                                                $checkbox .= "<div class=\"form-check\">
                                                          <span class=\"form-check-label\">
                                                            <input type=\"checkbox\" 
                                                            class=\"addGroup checkbox form-check-input\" 
                                                            name=\"$field[fieldId]\" 
                                                            id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                            value=\"" . $choiceVal . "\"  
                                                            data-fullvalue=\"" . strtolower($FullValue) . "\"  
                                                            data-value=\"" . strtolower($choiceVal) . "\"
                                                            " . $jsonData . "
                                                             disabled
                                                             >
                                                                $choice
                                                          </span>
                                                        </div>";
                                            }
                                            echo "
                                        <div class=\"form-group\">
                                        <label>$field[fieldLabel]</label>
                                        <input type=\"hidden\" name=\"checkbox_".str_replace('field_','',$field['fieldId']) ."\" value=\"".$field['fieldId']."\">
                                        $checkbox
                                        $Helper
                                      </div>
                                        ";
                                            break;

                                        case 'Radio':
                                            $choices = explode("\r\n", $field['fieldChoices']);
                                            $checkbox = '';
                                            foreach ($choices as $choice) {
                                                $FullValue = $choice;
                                                $choice = explode(';',$choice);
                                                $choice = $choice[0];
                                                $choiceVal = str_replace(['<', '>', '&'], ['&lt;', '&gt;', '&amp;'], $choice);
                                                $choiceVal = html_entity_decode($choiceVal);
                                                $choice = trim($choice);
                                                $Checked = $field['defaultValue'] == $choice ? "checked" : "";
                                                $checkbox .= "<div class=\"form-check\">
                                                          <span class=\"form-check-label\">
                                                            <input type=\"radio\"
                                                             class=\"addGroup radio form-check-input\" 
                                                             id=\"" . macanta_generate_key('radio_', 5) . "\"
                                                                " . $jsonData . "
                                                            name=\"$field[fieldId]\" id=\"$domId\"   data-fullvalue=\"" . strtolower($FullValue) . "\"  data-value=\"" . strtolower($choiceVal) . "\" value=\"" . $choiceVal . "\" {$Checked} disabled>
                                                                $choice
                                                          </span>
                                                        </div>";
                                            }
                                            echo "
                                        <div class=\"form-group\">
                                        <label>$field[fieldLabel]</label>
                                        $checkbox
                                        $Helper
                                      </div>
                                        ";

                                            break;

                                        case 'Date':
                                            $placeholder = $field['placeHolder'] == "" ? "YYYY-MM-DD" : $field['placeHolder'];
                                            echo "
                                        <div class=\"form-group\">
                                            <label for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                            <input  type='text'
                                                    class=\"addGroup infoDate input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $placeholder . "\" disabled 
                                                    $required>
                                            $Helper
                                         </div>
                                        ";
                                            break;

                                        case 'DateTime':
                                            $placeholder = $field['placeHolder'] == "" ? "YYYY-MM-DD HH:mm:ss" : $field['placeHolder'];
                                            echo "
                                        <div class=\"form-group\">
                                            <label for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                            <input  type='text'
                                                    class=\"addGroup infoDateTime input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $placeholder . "\" disabled 
                                                    $required>
                                            $Helper
                                         </div>
                                        ";
                                            break;

                                        case 'URL':
                                            echo "
                                        <div class=\"form-group\">
                                            <label for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . " <i class=\"fa fa-external-link connected-data-url\" data-field=\"" . $field['fieldId'] . "\" aria-hidden=\"true\"></i></label>
                                            <input  type='url'
                                                    class=\"addGroup input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\" disabled 
                                                    $required>
                                            $Helper
                                         </div>
                                        ";
                                            break;

                                        case 'Repeater':

                                            break;
                                    }
                                }
                            }
                            echo "</fieldset>";
                        }

                        ?>
                        <div class="container-relationships">
                            <label><?php echo $ContactInfo->FirstName." ".$ContactInfo->LastName ?>'s Relationship</label>
                            <div class="displayed-contact-relationship">
                                <select
                                        data-contactid="<?php echo $ContactInfo->Id; ?>"
                                        class="multiselect-ui field-relationships form-control ConnectedContactRelationships ConnectedContactRelationships<?php echo $ContactInfo->Id; ?> addGroup"
                                        multiple="multiple"
                                        onchange="get_multi_select_values(this)"
                                        required
                                        disabled
                                >
                                    <?php
                                    $ConnectorFields = macanta_get_config('connected_info');
                                    $ConnectorFields = $ConnectorFields ? json_decode($ConnectorFields, true):[];
                                    $RelationshipRules = [];
                                    foreach ($ConnectorFields as $FieldGroups){
                                        $RelationshipRules[$FieldGroups['id']] = $FieldGroups['relationships'];
                                    }
                                    $Allowed = [];
                                    foreach ($RelationshipRules[$ConnectorTab['id']] as $theRules){
                                        $Allowed[] = $theRules['Id'];
                                    }
                                    foreach ($availableRelationships as $Id=>$availableRelationship){
                                        if(in_array($Id,$Allowed))
                                            echo '<option value="'.$Id.'" >'.$availableRelationship["RelationshipName"].'</option>';
                                    }
                                    ?>
                                </select>
                                <?php
                                ?>
                            </div>
                        </div>
                        <div class="container-relationships">
                            <label >Other Related Contacts</label>
                            <ul class="relationship-list">

                            </ul>
                            <div class="">
                                <button type="button" class="addGroup link-connect-contact" disabled> <i class="fa fa-plus-square-o"></i> Connect Other Contact </button>
                            </div>

                        </div>


                        <div class="container-file-attachment-container container-file-attachment-container-<?php echo $ConnectorTab['id'] ?>">
                            <div class="container-file-attachments">
                                <label >File Attachments</label>
                                <ul class="file-attachments-list">

                                </ul>

                                <input  data-guid="<?php echo $ConnectorTab['id'] ?>"   id="CDFileAttachments<?php echo $ConnectorTab['id'] ?>" class="CDFileAttachments<?php echo $ConnectorTab['id'] ?> CDFileAttachments nb-btn nb-primary icon-btn" name="CDFileAttachments" type="file" disabled>

                            </div>
                        </div>
                        <div class="container-url-attachment-container">
                            <div class="input-group" >
                                <input type="text" class="form-control url-attachment" placeholder="Attach File By URL.." disabled>
                                <span class="input-group-btn">
                                <a class="nb-btn nb-primary icon-btn url-attachment-btn" disabled ><i class="fa fa-file-o"></i>   Attach File</a>
                              </span>
                            </div><!-- /input-group -->
                        </div>


                                <div class="flex-row btn-container">
                                    <input class="col-quarter" type="reset" style="display: none">
                                    <button type="button" data-guid="<?php echo $ConnectorTab['id'] ?>" class="addGroup nb-btn nb-base-color icon-btn col-quarter action-danger deleteUserConnectedInfo " disabled>
                                        <i class="fa fa-chain-broken"></i> Unlink <?php echo $ContactInfo->FirstName ?>
                                    </button>
                                    <button type="button" data-guid="<?php echo $ConnectorTab['id'] ?>" class="addGroup nb-btn nb-base-color icon-btn col-quarter action-warning cancelUserConnectedInfo hideThis" >
                                        <i class="fa fa-times"></i> Cancel
                                    </button>
                                    <button type="submit" data-guid="<?php echo $ConnectorTab['id'] ?>" class="addGroup nb-btn nb-primary icon-btn col-quarter saveUserConnectedInfo " disabled>
                                        <span class="btn-inner">
                                            <i class="fa fa-floppy-o"></i>
                                            <span>Save Changes</span>
                                        </span>
                                    </button>
                                </div>

                    </form>
                  </div>
            </div>
        </div>
     
    </div>
<!--</div>-->
<script>
    $(document).ready(function() {
        $("input.infoDate").daterangepicker({
            autoUpdateInput: false,
            locale: {
                format: 'YYYY-MM-DD'
            },
            singleDatePicker:true,
            showDropdowns: true
        }).on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD'));
        });

        $("input.infoDateTime").daterangepicker({
            timePicker: true,
            autoUpdateInput: false,
            locale: {
                format: 'YYYY-MM-DD HH:mm:ss'
            },
            singleDatePicker:true,
            startDate: moment().startOf('hour'),
            endDate: moment().startOf('hour').add(32, 'hour'),
            showDropdowns: true
        }).on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD HH:mm:ss'));
        });

        $("select.ConnectedContactRelationships").multiselect({
            numberDisplayed: 5,
            maxHeight: 200,
            allSelectedText:false
        });

        $("fieldset.collapsible").collapsible({
            animation: true,
            speed: "medium"
        });
        UserConnectorInfoTable['<?php echo $ConnectorTab['id'] ?>'] = $(".UserConnectorInfoTable<?php echo $ConnectorTab['id'] ?>").DataTable( {
            "order": [[ 0, "desc" ]],
            "paging":   false,
            "searching":   false,
            "destroy": true,
            "info":     false,
            "createdRow": function ( row, data, index ) {
                //$(row).attr('data-contactid',data[3]);
            }
        } )
            .on( 'page.dt', function () {
                $(".UserConnectorInfoTable<?php echo $ConnectorTab['id'] ?> tr").removeClass('activeItem');
                console.log('Page Changed.');
            } )
            .on( 'draw', function () {
                var theForm = $("form.FormUserConnectedInfo[data-guid=<?php echo $ConnectorTab['id'] ?>]");
                var theItemId = theForm.attr('data-itemid');
                if(theItemId)
                    $(".UserConnectorInfoTable<?php echo $ConnectorTab['id'] ?> tr[data-itemid="+theItemId+"]").addClass('activeItem');
            } );
        //makeCDListDataTable(".UserConnectorInfoTable<?php echo $ConnectorTab['id'] ?>");
        FileAttachmentsIt('container-file-attachment-container-<?php echo $ConnectorTab['id'] ?>');
        if (typeof CustomTabFnCallback !== "undefined") {
            console.log("Calling CustomTabFnCallback();");
            CustomTabFnCallback();
        }
    } );
</script>