<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/
$hook['post_controller_constructor'] = array(
    'class'    => '',
    'function' => 'load_config',
    'filename' => 'my_config.php',
    'filepath' => 'hooks'
);

$GLOBALS['bugsnag'] = Bugsnag\Client::make("057002caa7eae198c961aad4edc96a83");
Bugsnag\Handler::register($GLOBALS['bugsnag']);
$GLOBALS['bugsnag']->setReleaseStage(ENVIRONMENT);
$GLOBALS['bugsnag']->setNotifyReleaseStages(['production', 'staging']);
//$GLOBALS['bugsnag']->notifyError('ErrorType', 'A wild error appeared!');
//$GLOBALS['bugsnag']->notifyException(new RuntimeException("Test error From Staging"));
// Setup Bugsnag to handle our errors
/*
 \Bugsnag::register($container->getParameter('bugsnag.api_key'));
\Bugsnag::setReleaseStage($releaseStage);
\Bugsnag::setNotifyReleaseStages($container->getParameter('bugsnag.notify_stages'));
\Bugsnag::setProjectRoot(realpath($container->getParameter('kernel.root_dir') . '/..'));
// Attach to support reporting PHP errors
set_error_handler("\\Bugsnag::errorHandler");
*/