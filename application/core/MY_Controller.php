<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 07/03/16
 * Time: 3:34 PM
 */
/*
 * Description:  This handles the wrapper to the main layout "macanta.php"
*/
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    protected $layout = 'macanta';
    protected $stylesheets = array(
        'bootstrap/css/bootstrap.min.css',
        'font-awesome/css/font-awesome.min.css',
//         'css/form-elements.css',
        'css/content.min.css',
        'css/bootstrap-switch.css',
        'css/bootstrap.vertical-tabs.css',
        'css/dataTables.bootstrap.min.css',
        'format-phone/build/css/intlTelInput.css',
//         'format-phone/build/css/demo.css',
        'css/jquery.tagsinput.css',
        'css/daterangepicker.css',
        'jquery-ui-1.11.4/jquery-ui.css',
        'bootstrap-select/css/bootstrap-select.css',
        'bootstrap/css/bootstrap-tour.css',
        'bootstrap-fileinput/css/fileinput.css',
        'css/multi-select.css',
        'bootstrap-multiselect/css/bootstrap-multiselect.css',
        'css/toggles.css',
        'css/toggles-iphone.css',
        'css/normalize.css',
        'css/responsive.jqueryui.css',
        'css/responsive.bootstrap.css',
        'css/responsive.dataTables.css',
        'collapsible-fieldsets/css/jquery-collapsible-fieldset.css',
        'hopscotch/dist/css/hopscotch.css',
        'css/macanta.css'


    );
    protected $javascripts = array(
        'js/jquery-1.11.1.min.js',
        'jquery-ui-1.11.4/jquery-ui.min.js',
        'bootstrap/js/bootstrap.min.js',
        'js/bootstrap-switch.js',
        'js/jquery.backstretch.min.js',
        'js/moment.js',
        'js/jquery.dataTables.min.js',
        'js/dataTables.bootstrap.min.js',
        'tinymce/tinymce.min.js',
        'js/twilio_script.js',
        'format-phone/build/js/intlTelInput.js',
        'format-phone/build/js/utils.js',
        'js/jquery.tagsinput.js',
        'js/daterangepicker.js',
        'bootstrap-select/js/bootstrap-select.js',
        'bootstrap/js/bootstrap-tour.js',
        'bootstrap-fileinput/js/fileinput.js',
        'bootstrap-fileinput/js/locales/fr.js',
        'bootstrap-fileinput/js/locales/es.js',
        'js/jquery.multi-select.js',
        'bootstrap-multiselect/js/bootstrap-multiselect.js',
        'js/jquery.quicksearch.js',
        'js/wrap.js',
        'js/Toggles.js',
        'js/readmore.min.js',
        'js/dataTables.responsive.js',
        'js/responsive.jqueryui.js',
        'js/responsive.bootstrap.js',
        'collapsible-fieldsets/js/jquery-collapsible-fieldset.js',
        'js/multiselect.js',
        'js/functions.js',
        'js/macanta.js',
        'edit_area/edit_area_full.js',
        'hopscotch/dist/js/hopscotch.js'

    );
    protected $local_stylesheets = array();
    protected $local_javascripts = array();
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
        $this->load->library('gravatar');
        $this->load->helper('url');
        $this->load->dbforge();
        $language = $this->config->item('macanta_lang');
        if ($language === NULL)
        {
            $language = 'english';
        }

        // Load the language file
        $this->lang->load('macanta', $language);

    }
    protected function render($contents)
    {


        $theBody = '';
        foreach($contents as $content){
            $theBody .= $content;
        }
        $mobile = $this->agent->is_mobile() ? 'yes':'no';
        $view_data = array('content' => $theBody,
            'stylesheets' => $this->get_stylesheets(),
            'javascripts' => $this->get_javascripts(),
            'mobile' => $mobile
        );
        $this->load->view($this->layout, $view_data);
    }

    //other parts of the class..

    protected function get_stylesheets()
    {
        return array_merge($this->stylesheets, $this->local_stylesheets);
    }

    protected function get_javascripts()
    {
        return array_merge($this->javascripts, $this->local_javascripts);
    }

}
