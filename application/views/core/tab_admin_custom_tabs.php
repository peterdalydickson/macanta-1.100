<?php
$CustomTabsEncoded = macanta_get_config('custom_tabs');
if($CustomTabsEncoded){
    $CustomTabs = json_decode($CustomTabsEncoded, true);
}else{
    $CustomTabs = [];
}

?>
<script>
    $("button").parents(".panel-heading").addClass("has-btn");
</script>
<small class="dev-only php">tab_admin_custom_tabs.php</small>
<div class="theNotePanel">
    <div class="mainbox-top">
        <div class="tab-panel">
            <div class="panel panel-primary left-CustomTabs">
                <div class="panel-heading">

                    <h3 class="panel-title"><div class="panel-title-inner"><i class="fa fa-cogs"></i> Custom Tabs Manager</div></h3>
                    <button type="button" class="nb-btn icon-btn nb-secondary saveCustomTabs"><i class="fa fa-save"></i>
                        Save Custom Tabs
                    </button>
                </div>
                <div class="panel-body admin-panelBody CustomTabContainer ">
                    <div class="panel-body-content CustomTabListContainer">

                        <form id="CustomTabList" method="post" class="form-horizontal CustomTabList dynamic"
                              _lpchecked="1">
                            <ul class="itemContainer">

                                <div id="saved"  class="saved" ></div>
                                <?php
                                foreach ($CustomTabs as $key => $theTab) {
                                    ?>
                                    <li class="form-group-item ui-sortable-handle"
                                        data-guid="<?php echo $theTab['id'] ?>">
                                        <div class="bullet-item"><span class="CustomTabListTitle"
                                                                       data-guid="<?php echo $theTab['id'] ?>"><?php echo $theTab['title'] ?></span>
                                            <!--<input type="text" class="col-xs-11 FilterPairName" name="pairitem[]" placeholder="Type Here">-->
                                            <button type="button" class="nb-btn btn-default  removeButton"><i
                                                        class="fa fa-trash-o"
                                                        title="Delete Custom Tab"></i></button>
                                        </div>

                                    </li>
                                    <?php
                                }
                                ?>
                                <div id="dummy"></div>

                                <li class="form-group-item remove-button">
                                    <div class="bullet-item">

                                        <!--<input type="text" class="col-xs-11 FilterPairName" name="pairitem[]" placeholder="Type Here">-->

                                        <button type="button" class="nb-btn btn-default addButton  addButton-connector"><i class="fa fa-plus-square-o"></i>Add Custom Tab
                                        </button>
                                    </div>

                                </li>


                            </ul>
                            <!--<button type="button" class="nb-btn btn-default addButton "><i class="fa fa-plus-square-o"></i>Add Custom Tab</button>-->
                            <p class="note">Drag &amp; Drop the tabs above to rearrange them.</p>
                        </form>
                        <hr>
                        <div class="refresh-cache-container">
	                        <p class="note refresh-cache-note"><strong>When you add custom fields or create/modify web forms, tags and settings in Infusionsoft, click the button below to tell macanta about the changes. Thanks.</strong></p>
	                        <div class="refreshCustomFields">
	                            <a class="nb-btn btn-default   refreshCustomFieldsButton" onclick="refreshCustomFields();">Refresh Cache</a>
	                        </div>
                        </div>
                        <hr>
                        <label class="tab-override-label label-lg">Custom Tab Override</label>
                        <div class="tab-override-container">
	                        <div class="tab-override">
	                            <input class="form-control customTabOffTagId" name="customTabOffTagId"  type="text"
	                                <?php
	                                $custom_tabs_tag = $this->config->item('custom_tabs_tag');
	                                $custom_tabs_tag = $custom_tabs_tag ? $custom_tabs_tag:"";
	                                ?>
	                                   value="<?php echo $custom_tabs_tag;?>"
	                            >
	                            <p class="note"><strong>Tag ID To Hide All Custom Tabs</strong></p>
	                        </div>
	                        <div class="tab-override">
	                            <input class="form-control customTabOnTagId" name="customTabOnTagId"  type="text"
	                                <?php
	                                $custom_tabs_on_tag = $this->config->item('custom_tabs_on_tag');
	                                $custom_tabs_on_tag = $custom_tabs_on_tag ? $custom_tabs_on_tag:"";
	                                ?>
	                                   value="<?php echo $custom_tabs_on_tag;?>"
	                            >
	                            <p class="note"><strong>Tag IDs Of Custom Tabs To Display</strong></p>
	                        </div>
                        </div>

                    </div>

                    <div class="panel-body-content CustomTabSettingsContainer">
                        <?php
                        foreach ($CustomTabs as $key => $theTab) {
                            ?>
                            <div class="customTabContent <?php echo $theTab['id'] ?>"
                                 data-guid="<?php echo $theTab['id'] ?>">
                                <form class="TabContent<?php echo $theTab['id'] ?>" >
                                    <div class="">
                                        <div class="tab-content-container">
                                            <div class="form-group ">
                                                <label class="label-lg control-label requiredField" for="customTabTitle">
                                                    Tab Title <span class="asteriskField"> * </span>
                                                </label>
                                                <input class="form-control customTabTitle" name="customTabTitle"
                                                       placeholder="Your custom tab title" type="text"
                                                       value="<?php echo $theTab['title'] ?>"/>
                                            </div>
                                            <div class="form-group ">
                                                <label class="label-lg control-label" for="customTabContent">
                                                    Content
                                                </label>
                                                <textarea id="Editor<?php echo $theTab['id'] ?>" name="" class=" TextEditor"
                                                          placeholder="Text Here"><?php echo base64_decode($theTab['content']) ?></textarea>

                                            </div>
                                            <!-- <div class="form-group checkboxGroup">
                                            <label class="control-label" for="customTabPermission">
                                                Smart Workflow Tab (create corresponding tag in Infusionsoft)
                                            </label>
                                            <input type="checkbox"  data-group="<?php /*echo $theTab['id'] */?>" id="Checkbox<?php /*echo $theTab['id'] */?>" name="customTabPermission"
                                                   class="form-control  customTabPermission" <?php /*echo $theTab['permission'] == "1" ? "checked":"" */?>/>
                                        </div>
                                        <div class="form-group checkboxGroup">
                                            <label class="control-label customTabPermissionAll<?php /*echo $theTab['id'] */?>" for="customTabPermissionAll" <?php /*echo $theTab['permission'] == "1" ? "":'style="opacity: 0.25;" ' */?>>
                                                Display for ALL contacts (only display/hide by logged-in user)
                                            </label>
                                            <input type="checkbox"  data-group="<?php /*echo $theTab['id'] */?>" id="CheckboxAllContact<?php /*echo $theTab['id'] */?>" name="customTabPermissionAll"
                                                   class="form-control  customTabPermissionAll"  <?php /*echo $theTab['permission_all'] == "1" ? "checked":"" */?> <?php /*echo $theTab['permission'] == "1" ? "":"disabled" */?>/>
                                        </div>-->
                                            <?php
                                            $suffix = strtoupper(str_replace('_', '', $theTab['id']));
                                            ?>
                                            <small class="dev-only php">tab_admin_custom_tabs.php</small>
                                            <div class="tab-visibility-container">
                                                <label class="label-lg hide">Tab Visibility</label>
                                                <div class="form-group checkboxGroup hide">
                                                    <input type="radio"  data-suffix="<?php echo $suffix ?>"  data-group="<?php echo $theTab['id'] ?>" id="ALL<?php echo $suffix ?>" name="customTabPermission"
                                                           class="form-control  customTabPermission" value="ShowToAll"  checked />
                                                    <label class="control-label" for="ALL<?php echo $suffix ?>">
                                                        Show this Tab all the time, with no restriction by contact or user
                                                    </label>
                                                </div>
                                                <div class="form-group checkboxGroup hide">
                                                    <input type="radio"  data-suffix="<?php echo $suffix ?>"  data-group="<?php echo $theTab['id'] ?>" id="SOMEUSER<?php echo $suffix ?>" name="customTabPermission"
                                                           class="form-control  customTabPermission"  value="ShowToAll_UserSpecific" />
                                                    <label class="control-label customTabPermission<?php echo $theTab['id'] ?>" for="SOMEUSER<?php echo $suffix ?>">
                                                        Show this Tab for all contacts, but only to specific users
                                                    </label>

                                                </div>
                                                <div class="form-group checkboxGroup hide">
                                                    <input type="radio"  data-suffix="<?php echo $suffix ?>"  data-group="<?php echo $theTab['id'] ?>" id="SOMECONTACTUSER<?php echo $suffix ?>" name="customTabPermission"
                                                           class="form-control  customTabPermission"  value="ContactUserSpecific" />
                                                    <label class="control-label customTabPermission<?php echo $theTab['id'] ?>" for="SOMECONTACTUSER<?php echo $suffix ?>" >
                                                        Only show Tab to specific users and for specific contacts
                                                    </label>

                                                </div>
                                                <div class="form-group checkboxGroup hide">
                                                    <input type="radio" data-suffix="<?php echo $suffix ?>" data-group="<?php echo $theTab['id'] ?>" id="SOMECONTACTTAG<?php echo $suffix ?>" name="customTabPermission"
                                                           class="form-control  customTabPermission"  value="ContactHaveTag" />
                                                    <label class="control-label customTabPermission<?php echo $theTab['id'] ?>" for="SOMECONTACTTAG<?php echo $suffix ?>" >
                                                        Show this Tab for all contacts who DON’T have the corresponding tag
                                                    </label>

                                                    <!--<input class="form-control ContactHaveTag <?php /*echo $suffix */?>" name="ContactHaveTag"
                                                       placeholder="Corresponding Tag" type="number"
                                                       value="<?php /*echo isset($theTab['contact_have_tag']) ? $theTab['contact_have_tag']:""; */?>" <?php /*echo $theTab['permission'] == "ContactHaveTag" ? '':'style="display: none"'; */?>/>-->
                                                </div>
                                            </div><!-- /.tab-visibility-container -->
                                            <hr>
                                            <div class="form-group checkboxGroup hide">
                                                <input type="checkbox"  data-group="<?php echo $theTab['id'] ?>" id="TABGLOBAL<?php echo $suffix ?>" name="customTabGlobal"
                                                       class="form-control  customTabGlobal"  value="yes" <?php echo $theTab['global'] == "yes" ? 'checked':''; ?> />
                                                <label class="control-label customTabGlobal<?php echo $theTab['id'] ?>" for="TABGLOBAL<?php echo $suffix ?>" >
                                                    Global Tab (non contact-specific)
                                                </label>

                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <script>
                                <?php
                                $theId = "ALL".$suffix;
                                if(isset($theTab['permission']) && $theTab['permission'] == "1"){

                                    $theId = "SOMECONTACTUSER".$suffix;

                                }
                                if(isset($theTab['permission']) && $theTab['permission'] == "1" && isset($theTab['permission_all']) && $theTab['permission_all']== "1"){
                                    $theId = 'SOMEUSER'.$suffix;
                                }
                                if($theTab['permission'] == "ContactUserSpecific") $theId = "SOMECONTACTUSER".$suffix;
                                if($theTab['permission'] == "ShowToAll_UserSpecific")  $theId = "SOMEUSER".$suffix;
                                if($theTab['permission'] == "ShowToAll") $theId = "ALL".$suffix;
                                if($theTab['permission'] == "ContactHaveTag") $theId = "SOMECONTACTTAG".$suffix;
                                ?>
                                $(function() {
                                    document.getElementById("<?php echo $theId ?>").checked = true;
                                });

                            </script>
                            <?php
                        }
                        ?>
                        <div class="ContentHeader"></div>
                        <div class="CustomTabSettingsContainerPlaceholder"><i class="glyphicon glyphicon-edit"></i>Please
                            create or select your Custom Tab
                        </div>


                    </div>
<!--                     <div class="footnote"> -->
                        <!-- <strong>Available Shortcodes: </strong><br>
                         1. Infusionsoft Webform, e.g [ISwebform formid=1234]<br>
                         2. More Shortcodes coming soon!..-->
<!--                     </div> -->
                </div>
            </div>

        </div>
    </div>
<!--
    <div class="row">
    </div>
-->
</div>
<!-- Custom Tab Template -->
<div id="customTabContentTemplate" class="customTabContent" style="display: none;">
    <div class="">
        <div class="">
            <div class="form-group ">
                <label class="label-lg control-label requiredField" for="customTabTitle">
                    Tab Title <span class="asteriskField"> * </span>
                </label>
                <input class="form-control customTabTitle" name="customTabTitle"
                       placeholder="Your custom tab title" type="text"/>
            </div>
            <div class="form-group ">
                <label class="label-lg control-label" for="customTabContent">
                    Content
                </label>
                <textarea name="" class="TextEditor" placeholder="Text Here"></textarea>
            </div>
            <div class="form-group checkboxGroup hide">
                <label class="control-label" for="ALL">
                    Show this Tab all the time, with no restriction by contact or user
                </label>
                <input type="radio"  data-group="" id="ALL" name="customTabPermission"
                       class="form-control  customTabPermission" value="ShowToAll"  checked />
            </div>
            <div class="form-group checkboxGroup hide">
                <label class="control-label customTabPermission" for="SOMEUSER">
                    Show this Tab for all contacts, but only to specific users
                </label>
                <input type="radio"  data-group="" id="SOMEUSER" name="customTabPermission"
                       class="form-control  customTabPermission"  value="ShowToAll_UserSpecific" />
            </div>
            <div class="form-group checkboxGroup hide">
                <label class="control-label customTabPermission" for="SOMECONTACTUSER" >
                    Only show Tab to specific users and for specific contacts
                </label>
                <input type="radio"  data-group="" id="SOMECONTACTUSER" name="customTabPermission"
                       class="form-control  customTabPermission"  value="ContactUserSpecific" />
            </div>
            <div class="form-group checkboxGroup hide">
                <label class="control-label customTabPermission" for="SOMECONTACTTAG" >
                    Show this Tab for all contacts who DON’T have the corresponding tag
                </label>
                <input type="radio"  data-group="" id="SOMECONTACTTAG" name="customTabPermission"
                       class="form-control  customTabPermission"  value="ContactHaveTag" />
                <!--<input class="form-control ContactHaveTag" name="ContactHaveTag"
                       placeholder="Corresponding Tag" type="number"
                       value=""/>-->
            </div>
            <hr>
            <div class="form-group checkboxGroup hide">
                <label class="control-label customTabGlobal" for="TABGLOBAL" >
                    Global Tab (non contact-specific)
                </label>
                <input type="checkbox"  data-group="" id="TABGLOBAL" name="customTabGlobal"
                       class="form-control  customTabGlobal"  value="yes" />
            </div>
        </div>
    </div>
</div>
<script>
    var previousValue = "";

    $('form.CustomTabList.dynamic')
        ._once('click', '.addButton', function () {
            $('.CustomTabSettingsContainerPlaceholder').hide();
            $('#CustomTabList .form-group-item').removeClass('active');
            var GUID = 'ct_' + new Date().getTime().toString(36);
            console.log(GUID);
            var template = $(this).parents(".form-group-item"),
                form = $(this).parents('form#CustomTabList'),
                CTcontentTemplate = $("#customTabContentTemplate");
            template.find('input').attr('readonly', true);
            template
                .clone()
                .removeClass('remove-button')
                .addClass('active')
                .attr('data-guid', GUID)
                .removeAttr('id')
                .insertBefore($('#dummy', form))
                .find('div.bullet-item').prepend('<span class="CustomTabListTitle" data-guid="' + GUID + '">Your custom tab title </span>')
                .find('button.addButton').addClass('removeButton').removeClass('addButton')
                .html('<i class="fa fa-trash-o" title="Delete Custom Tab"></i>');
            $('.customTabContent').css('display', 'none');
            CTcontentTemplate
                .clone()
                .removeAttr('style')
                .removeAttr('Id')
                .attr('data-guid', GUID)
                .addClass(GUID)
                .insertBefore($('.CustomTabSettingsContainer .ContentHeader'))
                .find('textarea').attr('Id', 'Editor' + GUID);
            initTinymceCustomTab(GUID + ' textarea.TextEditor');
            bindCTEDIT(GUID);
            $("."+GUID+" input.customTabTitle").focus();
            $("form.CustomTabList ul").sortable({
                cancel: ".remove-button",
                stop: function (event, ui) {
                    if ($(ui.item).prev('.remove-button').length > 0)
                        $(this).sortable('cancel');
                }
            });


        })

        ._once('click', '.removeButton',function () {
            var $row = $(this).parents('.form-group-item');
            var nth = $row.index();
            var nthPrev = $row.index() - 2;
            var theGuid = $row.attr('data-guid');
            var theContent = $('div.customTabContent[data-guid=' + theGuid + ']');
            if (nth == 1 && $("form#CustomTabList ul li:eq( 1 )").hasClass('remove-button')) $('.CustomTabSettingsContainerPlaceholder').show();
            console.log(nth);
            console.log(nthPrev);
            $form = $(this).parents('form');
            if ($row.hasClass('active')) {
                var nextActive = $("form#CustomTabList ul li:eq( " + nth + " )");
                var prevActive = $("form#CustomTabList ul li:eq( " + nthPrev + " )");
                var nextGuid = nextActive.attr('data-guid');
                var prevGuid = prevActive.attr('data-guid');
                if (nextActive.hasClass('remove-button')) {
                    prevActive.addClass('active');
                    $('.' + prevGuid).fadeIn('fast')
                } else {
                    nextActive.addClass('active');
                    $('.' + nextGuid).fadeIn('fast')
                }
            }
            $row.remove();
            theContent.remove();

        })
        ._once('click', 'span.CustomTabListTitle', function () {
            $('.CustomTabSettingsContainerPlaceholder').hide();
            $('#CustomTabList .form-group-item').removeClass('active');
            $('.customTabContent').css('display', 'none');
            var $row = $(this).parents('.form-group-item'),
                $form = $(this).parents('form'),
                theClassContent = $row.attr('data-guid');
            $row.addClass('active');
            $('.' + theClassContent).fadeIn('fast').find(".customTabTitle").focus();

        });
    $('button.saveCustomTabs').once('click', function () {
        saveCustomTabs();

    });
    $('input.customTabPermission')._once('click', function () {
        var theGroupId = $(this).attr('data-group');
        var theCheckbox = $("input#CheckboxAllContact"+theGroupId);
        var theLabel = $("label.customTabPermissionAll"+theGroupId);
        if($(this).is(":checked")){
            theCheckbox.attr("checked", true).removeAttr("disabled");
            theLabel.removeAttr("style");
        }else{
            theCheckbox.attr("disabled", true).removeAttr("checked");
            theLabel.css("opacity",0.25);
        }
    });
    $('.customTabContent').css('display', 'none');
    <?php
    foreach($CustomTabs as $key => $theTab){?>
    initTinymceCustomTab('<?php echo $theTab['id']?> textarea.TextEditor');
    bindCTEDIT('<?php echo $theTab['id']?>');
    $("form.CustomTabList ul").sortable({
        cancel: ".remove-button",
        stop: function (event, ui) {
            if ($(ui.item).prev('.remove-button').length > 0)
                $(this).sortable('cancel');
        }
    });
    <?php }
    ?>
    if (!$("form#CustomTabList ul li:eq( 0 )").hasClass('remove-button')) {
        $('form#CustomTabList ul li:eq( 0 ) span.CustomTabListTitle').trigger('click');
    }
    function reloadWebform(editor){
        console.log(editor);
        console.log(editor.getElement());
    }
    function initTinymceCustomTab(theClass) {
        var theEditor;
        tinymce.init({
            allow_script_urls: true,
            valid_elements : '*[*]',
            valid_children : "+body[style|iframe|script|img]",
            extended_valid_elements : "style[type|src],base[href|target],a[class|name|href|target|title|onclick|rel],script[type|src],iframe[src|style|width|height|scrolling|marginwidth|marginheight|frameborder|target|sandbox],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name]",
            selector: '.' + theClass,
            content_css: 'assets/css/content.min.css',
            init_instance_callback: function (editor) {
            },
            menubar: 'template edit insert view format table tools',
            setup: function (editor) {
                theEditor = editor;
                /*editor.addMenuItem('shortcode', {
                    text: 'Infusionsoft Web Form',
                    context: 'insert',
                    image: '<?php echo $this->config->item('base_url').'assets/img/isicon.png'?>',
                    icon: false,
                    menu:[
                        <?php
                        echo infusionsoft_generate_webform_menu();
                        ?>
                    ]
                });
                editor.addMenuItem('tagcategories', {
                    text: 'Infusionsoft Tag Categories',
                    context: 'insert',
                    image: '<?php echo $this->config->item('base_url').'assets/img/tagicon.jpg'?>',
                    icon: false,
                    menu:[
                        <?php
                        echo infusionsoft_get_tag_categories_menu();
                        ?>
                    ]
                });
                editor.addMenuItem('emailhistory', {
                    text: 'Infusionsoft Email History',
                    context: 'insert',
                    image: '<?php echo $this->config->item('base_url').'assets/img/historyemailicon.png'?>',
                    icon: false,
                    onclick: function() {editor.insertContent('[EmailHistory ]');}

                });
                editor.addMenuItem('emailform', {
                    text: 'Send Email via Infusionsoft',
                    context: 'insert',
                    image: '<?php echo $this->config->item('base_url').'assets/img/isemailicon.png'?>',
                    icon: false,
                    onclick: function() {editor.insertContent('[EmailForm createnote=no]');}

                });
                editor.addMenuItem('emailtemplate', {
                    text: 'Send Email via Infusionsoft Template',
                    context: 'insert',
                    image: '<?php echo $this->config->item('base_url').'assets/img/isemailicon2.png'?>',
                    icon: false,
                    menu: [
                        <?php
                        $Objs = array();
                        $EmailTemplates = infusionsoft_get_email_template();
                        foreach($EmailTemplates as $Template){
                            if($Template->PieceType != 'Email') continue;

                            $Title = json_encode($Template->PieceTitle);
                            $Id = $Template->Id;
                            $Objs[] = '{text:'.$Title.',onclick: function() {editor.insertContent(\'[EmailTemplate id='.$Id.'  createnote=no]\');}}';
                        }
                        $theMenu = implode(',',$Objs);
                        echo $theMenu;
                        ?>
                    ]

                });
                editor.addMenuItem('opportunities', {
                    text: 'Infusionsoft Opportunities',
                    context: 'insert',
                    image: '<?php echo $this->config->item('base_url').'assets/img/opportunities-32.png'?>',
                    icon: false,
                    onclick: function() {editor.insertContent('[ISopportunities Only_Show_Opps_Assigned_To_Logged_In_User=no pipelines=all]');}
                });
                editor.addMenuItem('filebox', {
                    text: 'Infusionsoft File Box',
                    context: 'insert',
                    image: '<?php echo $this->config->item('base_url').'assets/img/filebox.png'?>',
                    icon: false,
                    onclick: function() {editor.insertContent('[FileBox ]');}
                });*/
            },
            plugins: [
                'template advlist autolink link image lists charmap print preview hr anchor pagebreak ',
                'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                'save table contextmenu directionality emoticons template paste textcolor '
            ]
        });
    }
</script>