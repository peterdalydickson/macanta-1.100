<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 07/08/2018
 * Time: 1:13 PM
 */
//Example usage
//-------------
//$pa = new Capture_Interactive_Find_v1_00 ("AA11-AA11-AA11-AA11","wr2 6nj","","","","","");
//$pa->MakeRequest();
//if ($pa->HasData())
//{
//   $data = $pa->HasData();
//   foreach ($data as $item)
//   {
//      echo $item["Id"] . "<br/>";
//      echo $item["Type"] . "<br/>";
//      echo $item["Text"] . "<br/>";
//      echo $item["Highlight"] . "<br/>";
//      echo $item["Description"] . "<br/>";
//   }
//}
//--------------------------------------//
class Capture_Interactive_Find_v1_00
{

    //Credit: Thanks to Stuart Sillitoe (http://stu.so/me) for the original PHP that these samples are based on.

    private $Key; //The key to use to authenticate to the service.
    private $Text; //The search term to find.
    private $Container; //A container for the search. This should be an Id previously returned from this service.
    private $Origin; //A starting location for the search. This can be the name or ISO 2 or 3 character code of a country, or Latitude and Longitude, to start the search.
    private $Countries; //A comma separated list of ISO 2 or 3 character country codes to limit the search within e.g. US,CA,MX
    private $Limit; //The maximum number of results to return.
    private $Language; //The preferred language for results. This should be a 2 or 4 character language code e.g. (en, fr, en-gb, en-us etc).
    private $Data; //Holds the results of the query

    function __construct($Key, $Text, $Container, $Origin, $Countries, $Limit, $Language)
    {
        $this->Key = $Key;
        $this->Text = $Text;
        $this->Container = $Container;
        $this->Origin = $Origin;
        $this->Countries = $Countries;
        $this->Limit = $Limit;
        $this->Language = $Language;
    }

    function MakeRequest()
    {
        $url = "http://services.postcodeanywhere.co.uk/Capture/Interactive/Find/v1.00/xmla.ws?";
        $url .= "&Key=" . urlencode($this->Key);
        $url .= "&Text=" . urlencode($this->Text);
        $url .= "&Container=" . urlencode($this->Container);
        $url .= "&Origin=" . urlencode($this->Origin);
        $url .= "&Countries=" . urlencode($this->Countries);
        $url .= "&Limit=" . urlencode($this->Limit);
        $url .= "&Language=" . urlencode($this->Language);

        //Make the request to PostalCode Anywhere and parse the XML returned
        //echo $url;
        //$file = simplexml_load_file($url);
        $feed = file_get_contents($url);
        $file = simplexml_load_string($feed);
        //print_r($file);
        //Check for an error, if there is one then throw an exception
        if ($file->Columns->Column->attributes()->Name == "Error")
        {
            throw new Exception("[ID] " . $file->Rows->Row->attributes()->Error . " [DESCRIPTION] " . $file->Rows->Row->attributes()->Description . " [CAUSE] " . $file->Rows->Row->attributes()->Cause . " [RESOLUTION] " . $file->Rows->Row->attributes()->Resolution);
        }

        //Copy the data
        if ( !empty($file->Rows) )
        {
            foreach ($file->Rows->Row as $item)
            {
                $this->Data[] = array('Id'=>$item->attributes()->Id,'Type'=>$item->attributes()->Type,'Text'=>$item->attributes()->Text,'Highlight'=>$item->attributes()->Highlight,'Description'=>$item->attributes()->Description);
            }
        }
    }

    function HasData()
    {
        if ( !empty($this->Data) )
        {
            return $this->Data;
        }
        return false;
    }

}

