<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mailer {

    protected $to;

    protected $replyTo;

    protected $senderEmail;

    protected $senderName;

    protected $subject;

    protected $userName;

    protected $password;

    protected $emailHost;

    protected $mailFrom;

    protected $mailDirectory;


    public function __construct() {


        $this->userName = $_SERVER['BOX_USERNAME'];
        $this->password = $_SERVER['BOX_PASSWORD'];


        $this->emailHost = 'box.macantacrm.com';
        $this->mailFrom = ['help@macantacrm.com' => 'Macanta Help'];

        // Define email directory
        $this->mailDirectory = VIEWPATH . '/emails';
    }



    protected function init()
    {
        $Encryption = 'tls'; // Amazon SES requires usage of TLS encryption., normally its `ssl`
        $transport = (new Swift_SmtpTransport( $this->emailHost, 587, $Encryption))
            ->setUsername( $this->userName )
            ->setPassword( $this->password );

        // Create the Mailer using your created Transport
        $mailer = new Swift_Mailer($transport);
        return $mailer;
    }




    protected function initializeTemplate( $template, $__data ) {

        ob_start();
        extract( $__data );

        include $this->mailDirectory .'/'.$template;

        return ltrim( ob_get_clean() );
    }



    public function to( $email )
    {
        $this->to = $email;
        return $this;
    }


    public function senderEmail( $email )
    {
        $this->senderEmail = $email;
        return $this;
    }
    public function senderName( $name )
    {
        $this->senderName = $name;
        return $this;
    }
    public function subject( $subject )
    {
        $this->subject = $subject;
        return $this;
    }



    public function send( $view, array $data = [] )
    {
        // Initialize Swift Mailer
        $mailer = $this->init();
        $template = $this->initializeTemplate( $view, $data );

        // Create a message
        $message = (new Swift_Message( $this->subject ))
            ->setFrom( $this->mailFrom )
            ->setTo([$this->to])
            ->setReplyTo($this->senderEmail,$this->senderName)
            ->setBody( $template, 'text/html' );

        // Send the message
        $result = $mailer->send($message);
        return $result;
    }
}