<li class="info-item contactIdInfo">
    <?php
    $IS_App_Name = $this->config->item('MacantaAppName');
    $IS_App_Name = explode('-', $IS_App_Name);
    ?>
    <span class="label">Infusionsoft Id: </span>
    <span class="InvoiceTotal"><a target="_blank" href="https://<?php echo $IS_App_Name[0]; ?>.infusionsoft.com/app/searchResults/searchResult?searchResult=<?php echo $Id; ?>"><?php echo $Id; ?></a></span>
</li>
<li class="info-item contactOwnerInfo">
    <span class="label">Contact Owner: </span>
    <?php if($ContactOwner) { ?>
        <span class="InvoiceOwed"><?php echo $ContactOwner->FirstName." ".$ContactOwner->LastName; ?></span>
    <?php } ?>
</li>
<li class="info-item invoiceInfo">
    <span class="label"><?php echo $this->lang->line('text_total_invoiced'); ?>: </span>
    <span class="InvoiceTotal"><?php echo $InvoiceTotal; ?></span>
</li>
<li class="info-item owedInfo">
    <span class="label"><?php echo $this->lang->line('text_amount_owed'); ?>: </span>
    <span class="InvoiceOwed"><?php echo $InvoiceOwed; ?></span>
</li>

