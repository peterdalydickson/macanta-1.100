<?php
$ConnectedInfosEncoded = macanta_get_config('connected_info');
if (!$ConnectedInfosEncoded) $ConnectedInfosEncoded = '[]';
$ConnectedInfos = json_decode($ConnectedInfosEncoded, true);
$ConnectorRelationship = $this->config->item('ConnectorRelationship');
$ConnectorRelationshipArr = json_decode($ConnectorRelationship, true);
?>
<script>
    ConnectedInfoSettings = <?php echo macanta_get_config('connected_info',true); ?>;
    ConnectorRelationshipArr = <?php echo macanta_get_config('ConnectorRelationship',true); ?>;


    $("button").parents(".panel-heading").addClass("has-btn");

</script>
<small class="dev-only php">tab_admin_connector.php</small>
<div class="theNotePanel">
    <div class="mainbox-top">
        <div class="tab-panel">
            <div class="panel panel-primary left-ConnectedInfos">
                <div class="panel-heading">

                    <h3 class="panel-title "><div class="panel-title-inner"><i class="fa fa-info-circle"></i> Connected Information Manager</div></h3>
                    <button type="button" class="nb-btn nb-secondary saveConnectedInfos"><i
                                class="fa fa-save"></i>
                        Save Connected Info
                    </button>
                </div>
                <div class="panel-body admin-panelBody ConnectedInfoContainer ">
                    <div class="ConnectedInfoListContainer">

                        <form method="post" class="form-horizontal ConnectedInfoList dynamic"
                              _lpchecked="1">
                            <ul class="itemContainer">

                                <div class="saved"></div>
                                <script>
                                    renderConnectedInfoTitle();
                                </script>
                                <div id="dummy"></div>
                                <li class="form-group-item remove-button">
                                    <div class="bullet-item">
                                        <!--<input type="text" class="col-xs-11 FilterPairName" name="pairitem[]" placeholder="Type Here">-->
                                        <button type="button" class="nb-btn nb-secondary addButton "><i class="fa fa-plus-square-o"></i>Add Connected
                                            Info
                                        </button>
                                    </div>

                                </li>

                            </ul>

                            <p class="note">Drag &amp; Drop the items above to rearrange them.</p>
                        </form>
                    </div>

                    <div class="ConnectedInfoSettingsContainer">


                        <div class="ContentHeader"></div>
                        <div class="add-field-container">
                            <button type="button"
                                    class="nb-btn nb-primary saveConnectedInfosB <?php if (sizeof($ConnectedInfos) == 0) echo "hideThis" ?>">
                                <i class="fa fa-save"></i>
                                Save Connected Info
                            </button>
                        </div>
                        <div class="ConnectedInfoSettingsContainerPlaceholder"><i class="fa fa-pencil"></i>
                            Please Create Connected Information
                        </div>


                    </div>
<!--
                    <div class="footnote">
                        <strong>Available Shortcodes: </strong><br>
                         1. Infusionsoft Webform, e.g [ISwebform formid=1234]<br>
                         2. More Shortcodes coming soon!..
                    </div>
-->
                </div>
            </div>

        </div>
    </div>

</div>

<!-- Connector Template -->
<div id="ConnectedInfoContentTemplate" class="ConnectedInfoContent container-fluid" style="display: none;">
    <div class="">
        <div class="text-right">
            <a href="javaScript:void(0)" onclick="downloadConnectorCSV()" class="downloadConnectorCSV" title="Download CSV File"></a>
        </div>
        <div class="">
	        <div class="info-item-id-container">
	            <div class="info-title-container">
	                <div class="form-group ">
	                    <label class="control-label label-lg requiredField" for="ConnectedInfoTitle">
	                        Information Title <span class="asteriskField"> * </span>
	                    </label>
	                    <input class="form-control ConnectedInfoTitle" name="ConnectedInfoTitle"
	                           placeholder="Type in your Information Title" type="text"
	                           value=""/>
	                </div>
	            </div><!-- /.info-title-container -->
	            <div class="item-id-container infusionsoft-hide">
	                <div class="form-group ">
		                <div class="label-csv-container">
		                    <label class="control-label requiredField" for="ConnectedInfoTitle">
		                        Item ID Custom Field<span class="asteriskField">  </span>
		                    </label>
		                    <div class="csv-download-container">
			                    <a href="javaScript:void(0)" onClick="downloadConnectorCSV()" class="downloadConnectorCSV" title="Download CSV File"></a>
		                    </div><!-- /.csv-download-container -->
	                    </div><!-- /.label-csv-container -->
	                    <select name="ItemIdCustomField"
	                            id="<?php echo macanta_generate_key('infusionsoftCustomField_', 5); ?>"
	                            class=" selectpicker form-control ItemIdCustomField"
	                            data-size="false">
							<option value="-- Select One --"> -- Select One --</option>
	                    </select>
	                </div>
	            </div><!-- /.item-id-container -->
	        </div><!-- /.info-item-id-container -->
            <div class="form-group ">
                <label class="control-label label-lg" for="ConnectedInfoContent">
                    Fields
                </label>
                <ul id="connectorsTable" data-guid="">
                </ul>

            </div>
            <div class="form-group buttonContainer">
                <button type="button" class="nb-btn nb-primary icon-btn addConnectorField"><i class="fa fa-plus-square"
                                                                                   aria-hidden="true"></i> <span>Add A Field</span>
                </button>
            </div><!-- /.buttonContainer -->

            <div class="form-group RelationshipOptions">
                <label class="control-label label-lg control-label-fullwidth">
                    Relationship Options
                </label>
                <div class="all-available-container">
	                <div class="relationship-left">
	                    <h3 class="label-sm">All Relationships:</h3>
	                    <div class="connector-all-relationships-container">
	
	                        <ol class="connector-all-relationships">
	                            <?php
	                            foreach ($ConnectorRelationshipArr as $Relationship) {
	
	                                ?>
	                                <li class="field-details"
	                                    title="<?php echo $Relationship['RelationshipDescription'] ?>">
	                                    <div class="checkbox">
	                                        <label>
	                                            <input type="checkbox" name="AllRelationship"
	                                                   data-relationid="<?php echo $Relationship['Id'] ?>"
	                                                   value="<?php echo $Relationship['RelationshipName'] ?>">
	                                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
	                                            <span class="checkbox-label"><?php echo $Relationship['RelationshipName'] ?></span>
	                                        </label>
	                                    </div>
	                                </li>
	                                <?php
	                            }
	                            ?>
	
	                    </div>
                    </div><!-- /.relationship-left -->

	                <div class="relationship-right">
	                    <h3 class="label-sm">Available Relationships:</h3>
	                    <div class="connector-relationships-container">
	                        <div class="connector-relationships-title">Name</div>
	                        <div class="connector-relationships-title">Can have multiple
	                            items?
	                            <small>Leave limit blank for unlimited</small>
	                        </div>
	                        <form class="FormRelationship">
	                            <ol class="connector-relationships">
	
	                            </ol>
	                        </form>

	                    </div><!-- /.connector-relationships-container -->
	                </div><!-- /.relationship-right -->
                </div>

            </div>

        </div><!-- /."" -->
    </div>
    
    <form class="cd-tab-visibility">
        <div class="form-group checkboxGroup">
            <input type="radio"     name="connectedDataVisibility"
                   class="form-control  connectedDataVisibility" value="ShowCDToAll"  checked />
            <label class="control-label"  >
                Show this Connected Data Tab all the time, with no restriction by contact or user
            </label>
        </div>
        <div class="form-group checkboxGroup">
            <input type="radio" name="connectedDataVisibility"
                   class="form-control  connectedDataVisibility"  value="ShowCDToAllUserSpecific" />
			<label class="control-label customTabPermission"  >
			    Show this Connected Data Tab for all contacts, but only to specific users
			</label>
        </div>
    </form>
    
</div>

<li class="HTML-Template relationship-option field-details">
    <h3 class="RelationshipName"></h3>
    <label class="no">
        <input class="multiple-options" type="radio" name="" value="yes" checked="">
        No
    </label>
    <label class="yes">
        <input class="multiple-options" type="radio" name="" value="no">
        Yes
    </label>
    <label class="limit">
        Limit
        <input maxlength="3" max="999" min="0" type="number" name="MultipleLimit" value=""
               class="form-control multiple_limit" disabled="">

    </label>
</li>

<!-- Field Template -->
<li class="field-item field-item-template">
    <div class="field-item">
        <span class="field-label">
            <span class="label-title">Please Enter Your Field Label</span>
        <span class="cd-field-id">ID: </span>
        </span>
<!--         <span class="field-properties">
                                        Type: <span class="field-type"></span><br>
                                        Custom Field: <span class="is-custom-field"></span>
                                    </span> -->
        <span class="field-properties">
                                        Type: <span class="field-type"></span>
                                    </span>
	    <div class="field-add-remove">
		    <i class="fa fa-plus-square-o AddFieldItem" aria-hidden="true"></i>
		    <i class="fa fa-trash-o DeleteFieldItem" aria-hidden="true"></i>
	    </div><!-- /.field-add-remove -->
    </div>

    <form class="FormFieldDetails">
        <input type="hidden" name="fieldId" value="">
<!--         <ul class="connectorsFieldDetails"> -->
	        <div class="connectorsFieldDetails">
	        
<!-- ----- Group Start - 3 Col ----- -->

				<!-- Field Group Container Start -->
				<div class="field-group-container col-3">
			        
		            <div class="field-details">
		                <div class="field-title">
		                    <h3 class="label-lg">Field Label</h3>
		                    <small>This is the name which will appear in before the field.</small>
		                </div>
		                <div class="field-content">
		                    <input type="text" id="<?php echo macanta_generate_key('fieldLabel_', 5); ?>" name="fieldLabel"
		                           class="form-control field-input" value="" title=""
		                           required="required">
		                </div>
		            </div>
		            <div class="field-details">
		                <div class="field-title">
		                    <h3 class="label-lg">Place Holder Text</h3>
		                    <small>Text to show when the field is blank</small>
		                </div>
		                <div class="field-content">
		                    <input type="text" id="placeHolder" name="placeHolder" class="form-control field-input"
		                           value=""
		                           title="" required="required">
		                </div>
		            </div>
		
		            <div class="field-details">
		                <div class="field-title">
		                    <h3 class="label-lg">Helper Text</h3>
		                    <small>Some information regarding this field if needed.</small>
		                </div>
		                <div class="field-content">
		                   <textarea name="helperText" onkeyup="textAreaAdjust()" class="form-control field-input helperText" required="required"></textarea>
		               </div>
		               <div class="preview_content" style="display:none;"></div>
		                   <a class="fa fa-eye markdown_preview" aria-hidden="true" title="Preview"></a><a class="fa fa-edit undo_markdown_preview" aria-hidden="true" title="Write" style="display:none;"></a> <a href="https://www.markdownguide.org/cheat-sheet" target="_blank" class="helperResourceurl">Markdown Guide</a>
		            </div>
		         </div>
				<!-- Field Group Container End -->
				<!-- Field Group Container Start -->
				<div class="field-group-container col-2">   
		            <div class="field-details connector-field-type">
		                <div class="field-title">
		                    <h3 class="label-lg">Field Type</h3>
		                    <small>Select your field type</small>
		                </div>
		                <div class="field-content">
		                    <select name="fieldType" id="fieldType" class="form-control">
		                        <option value=""> -- Select One --</option>
		                        <optgroup label="Basic">
		                            <option value="Text">Text</option>
		                            <option value="TextArea">Text Area</option>
		                            <option value="Number">Whole Number</option>
		                            <option value="Currency">Decimal Number</option>
		                            <option value="Email">Email</option>
		                            <option value="Password">Password</option>
		                        </optgroup>
		                        <optgroup label="Choice">
		                            <option value="Select">Select</option>
		                            <option value="Checkbox">Checkbox</option>
		                            <option value="Radio">Radio Button</option>
		                        </optgroup>
		                        <optgroup label="Other">
		                            <option value="Date">Date Picker</option>
		                            <option value="DateTime">Date/Time Picker</option>
		                            <option value="URL">URL</option>
		                            <option value="Repeater" disabled="disabled">Repeater (coming soon)</option>
		                        </optgroup>
		                        <?php
		                        $SharedFlag=0;
		                        foreach($ConnectedInfos as $ShKey=>$SharedConnector)
		                        {
		                            if($key!=$ShKey)
		                            {
		                                foreach($SharedConnector['fields'] as $SharedFields)
		                                {
		                                    if($SharedFields['useAsPrimaryKey']=='yes')
		                                    {
		                                        if($SharedFlag==0)
		                                        {
		                                            echo '<optgroup label="Data Reference">';
		                                        }
		                                        ?>
		                                        <option <?php if($theField['fieldType'] == $SharedFields['fieldId']) echo "selected"; else echo ""; ?> value="<?php echo $SharedFields['fieldId'];?>">
		                                            <?php echo $SharedConnector['title'];?> (<?php echo $SharedFields['fieldLabel'];?>)
		                                        </option>
		                                        <?php
		                                        if($SharedFlag==0)
		                                        {
		                                            echo '</optgroup>';
		                                            $SharedFlag=1;
		                                        }
		                                    }
		                                }
		                            }
		                        }
		                        ?>
		                    </select>
		                </div><!-- /.field-content -->
		            </div>
		            
		            <div class="field-details">
		                <div class="">
		                    <h3 class="label-lg">Default Value</h3>
		                    <small>Predefined value for this field</small>
		                </div>
		                <div class="">
		                    <input type="text" id="defaultValue" name="defaultValue"
		                           class="form-control  field-input" value=""
		                           title="" required="required">
		                </div>
		            </div>				
				
				</div>
				<!-- Field Group Container End -->
            
<!-- ----- Group End - 3 Col ----- -->       
 
 
<!-- ----- Group Start - 3 Col ----- -->

				<!-- Field Group Container Start -->
				<div class="field-group-container col-3 nb-flex-start">
			                    
		            <div class="field-details">
		                <div class="field-title">
		                    <h3 class="label-lg">Data Reference?</h3>
		                </div>
		                <div class="field-content item-col-2 type-radio">
		                    <label>
		                        <input type="radio" id="<?php echo macanta_generate_key('useAsPrimaryKey_', 5); ?>"
		                               name="useAsPrimaryKey" value="no" checked>
		                        No
		                    </label>
		                    <label>
		                        <input type="radio" id="<?php echo macanta_generate_key('useAsPrimaryKey_', 5); ?>"
		                               name="useAsPrimaryKey" value="yes" >
		                        Yes
		                    </label>
		                </div>
		            </div>
		            <div class="field-details">
		                <div class="field-title">
		                    <h3 class="label-lg">Required?</h3>
		                </div>
		                <div class="field-content item-col-2 type-radio">
		                    <label>
		                        <input type="radio" name="requiredField" value="no" checked="checked">
		                        No
		                    </label>
		                    <label>
		                        <input type="radio"  name="requiredField" value="yes">
		                        Yes
		                    </label>
		                </div>
		            </div>
		            <div class="field-details">
		                <div class="field-title">
		                    <h3 class="label-lg">Contact Specific?</h3>
		                </div>
		                <div class="field-content item-col-2 type-radio">
		                    <label>
		                        <input type="radio" class="contactspecificField"
		                               name="contactspecificField" value="no"
		                               checked="checked"
		                        >
		                        No
		                    </label>
		                    <label>
		                        <input type="radio" class="contactspecificField"
		                               name="contactspecificField" value="yes"
		                        >
		                        Yes
		                    </label>
		                </div>
		            </div>
		            
				</div>
				<!-- Field Group Container End -->
            
<!-- ----- Group End - 3 Col ----- -->


<!-- ----- Group Start - 4 Col ----- -->

				<!-- Field Group Container Start -->
				<div class="field-group-container col-4">
		
		            <div class="field-details">
		                <div class="field-title">
		                    <h3 class="label-lg">Use As List Header</h3>
		                </div>
		                <div class="field-content item-col-3 type-mixed">
		                    <label>
		                        <input type="radio" name="showInTable" value="no" checked="checked">
		                        No
		                    </label>
		                    <label>
		                        <input type="radio" name="showInTable" value="yes">
		                        Yes
		                    </label>
		                    <label>
		                        <input type="number" name="showOrder" value="0" class="ordernumber">
		                        Order
		                    </label>
		                </div>
		            </div>
		
		            <div class="field-details">
		                <div class="field-title">
		                    <h3 class="label-lg">Sub-Section</h3>
		                    <small>Group-name to group fields inside sections.</small>
		                </div>
		                <div class="field-content">
		                    <input type="text" id="fieldSubGroup" name="fieldSubGroup" class="form-control field-input"
		                           value=""
		                           placeholder="Group A"
		                           title=""></div>
		            </div>
		            
		            <div class="field-details">
		                <div class="field-title">
		                    <h3 class="label-lg">Add Divider After This Field</h3>
		                </div>
		                <div class="field-content">
		                    <label>
		                        <input type="radio" name="addDivider" value="no" checked="checked">
		                        No
		                    </label>
		                    <label>
		                        <input type="radio" name="addDivider" value="yes">
		                        Yes
		                    </label>
		                </div>
		            </div>
		            
		            <div class="field-details">
		                <div class="field-title">
		                    <h3 class="label-lg">Add Heading Text Above This Field</h3>
		                </div>
		                <div class="field-content">
		                    <input type="text"  name="fieldHeadingText" class="form-control field-input"
		                           value=""
		                           title="" >
		                </div>
		            </div>
<!-- 		        </ul> -->
<!-- 				    </div> -->
				    
				</div>
				<!-- Field Group Container End -->
            
<!-- ----- Group End - 4 Col ----- -->
		    

<!-- ----- Group Start - Infusionsoft Only ----- -->

				<!-- Field Group Container Start -->
				<div class="field-group-container">		    
		            <div class="field-details hide">
		                <div class="">
		                    <h3>Infusionsoft Custom Field</h3>
		                    <small>Field to be updated in Infusionsoft</small>
		                </div>
		                <div class="">
		                    <select name="infusionsoftCustomField" id="infusionsoftCustomField" class=" form-control"
		                            data-size="false">
		                    </select>
		                </div>
		            </div>
		            <div class="field-details">
		                <div class="">
		                    <h3>Section Name</h3>
		                    <small>Some information regarding this field if needed.</small>
		                </div>
		                <div class="">
		                    <input type="text" id="sectionTagId" name="sectionTagId" class="form-control field-input"
		                           value=""
		                           title="" required="required"></div>
		            </div>
				</div>
				<!-- Field Group Container End -->
            
<!-- ----- Group End - Infusionsoft Only ----- -->

		</div>

    </form>
</li>

<!-- Choices Template-->
<div class="choices-container hidenTemplate">
	<small class="dev-only php"><span>tab_admin_connector.php - Line 556</span><span>-- Choices Template --</span></small>
    <h4 class="choices-label">Enter Choices
        <small> - seperated by new line</small>
    </h4>
    <textarea name="fieldChoices" id="fieldChoices" class="form-control"></textarea>
</div>
<input type="hidden" id="CurrentConnectorId"/>
<input type="hidden" id="CurrentConnectorDownLoadType" value=""/>

<?php
$ConnectorDeleteDialog = "
<div id=\"dialog-confirm\" title=\"Delete Connected Info\">
    <p><span class=\"ui-icon ui-icon-alert\"></span>
    Hang on there!<br><br>
    Have you told your Mom what you are about to do? Is she OK with it?
<br>
    If you continue, you will be removing this Data Object from your Macanta system. This can NOT be undone.

    
    <br><br>Are you sure this is what you want to do?</p>
</div>
";
$FieldDeleteDialog = "
<div id=\"dialog-confirm\" title=\"Deleting Field\">
    <p><span class=\"ui-icon ui-icon-alert\"></span>This Field will be deleted.<br> Are you sure?</p>
</div>
";
?>
<script>
    renderConnectedInfoSettingsContainer();
    $("select[name=infusionsoftCustomField]").on('change', function () {
        console.log('infusionsoftCustomField Changed');
        var value = $(this).val();
        var parentLi = $(this).parents("li.field-item");
        parentLi.find("div.field-item span.is-custom-field").html(value)
    });
</script>
<script>
    var previousValue = "";
    var ConnectorDeleteDialog = <?php echo json_encode($ConnectorDeleteDialog); ?>;
    var FieldDeleteDialog = <?php echo json_encode($FieldDeleteDialog); ?>;
    $('.ConnectedInfoContent').css('display', 'none');
    if (!$("form.ConnectedInfoList ul li:eq( 0 )").hasClass('remove-button')) {
        $('form.ConnectedInfoList ul li:eq( 0 ) span.ConnectedInfoListTitle').trigger('click');
    }
    initButtonAddField();
    initAddRelationship();
    $('button.saveConnectedInfos, button.saveConnectedInfosB').once('click', function () {
        $('button.saveConnectedInfos, button.saveConnectedInfosB').attr("disabled", true);
        saveConnectedInfos();
    });
    var txtSelectHTML = '<option value=""> -- Select One --</option>';
    $.each(ContactCustomFields, function (GroupName,CustomField) {
        txtSelectHTML += '<optgroup label="'+GroupName+'">';
        $.each(CustomField, function(Label ,Items){
            txtSelectHTML += '<option data-subtext="Type: '+Items.DataType+'" value="'+Items.Name+'" title="'+Items.Label+'">'+Items.Label+'</option>';
        });
        txtSelectHTML += '</optgroup>';
        $("#ConnectedInfoContentTemplate select[name=ItemIdCustomField]").html(txtSelectHTML);
        $(".field-item-template select#infusionsoftCustomField").html(txtSelectHTML);
    });
</script>
