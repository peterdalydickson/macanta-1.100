<div class="core-contact-index-inner <?php echo $Section['class']?>">
    <div class="form-top header">
<?php
foreach($Sections as $Section){
    $HTML = implode("\r\n",$Section['content']);
    $BlockOrder = ['contact_info'];
    foreach($Section['content'] as $Method => $HTML){
        echo "<!--".strtoupper($Method)." BLOCK -->";
        echo $HTML;
    }
}
?>
    </div><!-- End of .form-top .header -->
</div><!-- End of .core-contact-index-inner -->
