<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 07/03/16
 * Time: 3:00 PM
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Common extends MY_Controller
{
    public $content = array();
    public $userType = '';
    public $grantedAccess = array();
    public $ajaxResults = array(
        "status"=> 0, // true or false
        "message"=>"",// any massage
        "data" => array(), // returned data
        "script" => '' // javascript need to execute
    );
    public $session_name;
    public $IntercomAppId;
    public $IntercomSecretKey;
    public function __construct(){
        parent::__construct();
        //$this->load->database(); this is already loaded in My_Controller
        //$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file')); this is already loaded in My_Controller
        $this->load->config('version');
        $this->IntercomAppId = $this->config->item('INTERCOM_APP');
        $this->IntercomSecretKey = $this->config->item('INTERCOM_SECRET');

    }
    public function index()
    {

    }
    public function login($Data,$Auto=false){
        $start = time();
        $WriteLog = $this->config->item('MacantaAppName') == 'no' ? true:false;;
        $CurrentDir = dirname(__FILE__) . "/";
        if(!$Auto){
            $PostData = $Data;
            $Email = trim($PostData['Email']);
            $Password = trim($PostData['Password']);
            $results = infusionsoft_get_contact_by_email_password($Email, $Password);
            //$results = infusionsoft_get_contact_by_email_password($Email, $Password);
            $Contact = $results->message;
            foreach($Contact as $SingleContact){
                if(isset($SingleContact->CompanyID)){
                    if($SingleContact->CompanyID != $SingleContact->Id){
                        $Contact =   $SingleContact;
                        break;
                    }
                }else{
                    $Contact =   $SingleContact;
                    break;
                }
            }
        }else{
            // If from autologin
            $Contact = $Data;
        }
        if (isset($Contact->Email) && isset($Contact->Password))
        {
            //Check if the contact is in MacantaUsers
            $validateUser = macanta_get_user_access_by_id($Contact->Id);
            if($validateUser){
                //======= Store Autologin ID if not existing ========//
                $autologin_info =  $this->getAutoLoginKey($Contact->Id);
                if(!$autologin_info){
                    // generate autologin info
                    $autologin_info = random_string();
                    $DBdata = array(
                        'user_id' => $Contact->Id,
                        'meta_key' => 'autologin_key',
                        'meta_value' => $autologin_info
                    );
                    $this->db->insert('users_meta', $DBdata);
                }
                //===================================================//
                /* Set Time Zone*/
                if(isset($Contact->TimeZone) && $Contact->TimeZone != ''){
                    $Timezone = $Contact->TimeZone;
                }else{
                    $Timezone = 'UTC';
                }
                /*===============*/
                $start = time();
                $session_data = array(
                    'Email'             => isset($Contact->Email) ? $Contact->Email:"",
                    'Groups'            => isset($Contact->Groups) ? $Contact->Groups:"",
                    'InfusionsoftID'  => $Contact->Id,
                    'UserContactRestrictionTags'    => getUserRestrictionTagsByCatId(isset($Contact->Groups) ? $Contact->Groups:"", 'contactview_permission_cat'),
                    'Details'           => json_encode($Contact),
                    'AutoLoginKey' => $autologin_info,
                    'TimeZone' => $Timezone,
                    'UserMeta' => macanta_get_user_meta($Contact->Id)
                );
                $Duration = time()-$start;
                $ToRecordContactNotes = "StoringSession: " . $Duration . "\n----\n";
                if($WriteLog)
                    file_put_contents($CurrentDir . "_CommonTimeLapse_".$this->config->item('MacantaAppName').".txt", $ToRecordContactNotes , FILE_APPEND);

                $start = time();
                $Contact->UserType = $this->userType;
                $Contact->AppName = $this->config->item('MacantaAppName');

                if($Contact->AppName != 'qj311-dm')
                    record_user_actvity($Contact);

                //$this->ajaxResults['record'] = applyFn('record_user_actvity',$Contact);
                //$this->ajaxResults['user_success'] = applyFn('set_macanta_user_success',$Contact->Email);

                $Duration = time()-$start;
                $ToRecordContactNotes = "RecordUserActivity: " . $Duration . "\n----\n";
                if($WriteLog)
                    file_put_contents($CurrentDir . "_CommonTimeLapse_".$this->config->item('MacantaAppName').".txt", $ToRecordContactNotes , FILE_APPEND);

                /*$DBdata = [];
                $features = macanta_get_app_features();
                if(!$this->config->item('MacantaFeatures')){

                    $this->config->set_item('MacantaFeatures',json_decode($features,true));
                    $DBdata['value'] = $features;
                    $DBdata['key'] = 'MacantaFeatures';
                    $this->db->insert('config_data', $DBdata);

                }else{
                    $this->config->set_item('MacantaFeatures',json_decode($features,true));
                    $DBdata['value'] = $features;
                    $this->db->where('key','MacantaFeatures');
                    $this->db->update('config_data', $DBdata);
                }*/
                $DBdata = [];
                // Disabled this for KEAP
                if(!$this->config->item('NoteCustomFields')){
                    /*$NoteCustomFields = array(
                        array(
                            'Label'=>'Call Recording URL',
                            'dType'=>'Website'
                        ),
                        array(
                            'Label'=>'JSON',
                            'dType'=>'TextArea'
                        ),
                        array(
                            'Label'=>'Task assigned to',
                            'dType'=>'TextArea'
                        )

                    ) ;
                    $CustomFields = infusionsoft_generate_contact_action_custom_fields($NoteCustomFields);*/
                    $CustomFields = ['Call Recording URL' => 'CallRecordingURL','JSON'=>'JSON'];
                    if($CustomFields !== false){
                        $this->config->set_item('NoteCustomFields',$CustomFields);
                        $DBdata['value'] = '{"Call Recording URL":"CallRecordingURL","JSON":"JSON"}';
                        $DBdata['key'] = 'NoteCustomFields';
                        $this->db->insert('config_data', $DBdata);
                    }

                }
                $DBdata = [];
                if(!$this->config->item('TaskCustomFields')){
                    /*$TaskCustomFields = array(
                        array(
                            'Label'=>'Task assigned to',
                            'dType'=>'Text'
                        )
                    ) ;
                    $CustomFields = infusionsoft_generate_contact_action_custom_fields($TaskCustomFields);*/
                    $CustomFields = ['Task assigned to'=>'Taskassignedto'];
                    if($CustomFields !== false){
                        $this->config->set_item('TaskCustomFields',$CustomFields);
                        $DBdata['value'] = '{"Task assigned to":"Taskassignedto"}';
                        $DBdata['key'] = 'TaskCustomFields';
                        $this->db->insert('config_data', $DBdata);
                    }

                }
                /*$DBdata = [];
                if(!$this->config->item('CampaignRefCustomFields')){
                    $CampaignRefCustomFields = array(
                        array(
                            'Label'=>'Campaign Reference Id',
                            'dType'=>'Text'
                        )
                    ) ;
                    $CustomFields = infusionsoft_generate_contact_action_custom_fields($CampaignRefCustomFields, -1);
                    $this->ajaxResults['meta'] = $CustomFields;
                    if($CustomFields !== false){
                        $this->config->set_item('CampaignRefCustomFields',$CustomFields);
                        $DBdata['value'] = json_encode($CustomFields);
                        $DBdata['key'] = 'CampaignRefCustomFields';
                        $this->db->insert('config_data', $DBdata);
                    }

                }*/
                /**/
                //todo this should be deleted in future version
                $table = "connected_data";
                $fields = array(
                    'id' => array( 'type' => 'TEXT', 'null' => TRUE, ),
                    'group' => array( 'type' => 'TEXT', 'null' => TRUE, ),
                    'value' => array( 'type' => 'TEXT', 'null' => TRUE, ),
                    'connected_contact' => array( 'type' => 'TEXT', 'null' => TRUE, ),
                    'history' => array( 'type' => 'TEXT', 'null' => TRUE, ),
                    'meta' => array( 'type' => 'TEXT', 'null' => TRUE, ),
                    'created' => array( 'type' => 'TEXT', 'null' => TRUE, ),
                    'status' => array( 'type' => 'TEXT', 'null' => TRUE, ),
                );
                $this->dbforge->add_field($fields);
                $this->dbforge->create_table($table, true);
                /**/
                $this->ajaxResults['status'] = 1;
                $this->ajaxResults['AutoLoginKey'] = $autologin_info;
                $this->ajaxResults['message'] = $this->lang->line('text_login_successful');
                $session_name = macanta_generate_key('sess_');
                $this->session_name = $session_name;
                /*Create user_sessions Table if not exist*/
                $table = "user_sessions";
                $fields = array(
                    'id' => array( 'type' => 'INT','auto_increment' => TRUE ),
                    'infusionsoft_id' => array( 'type' => 'INT' ,'constraint' => 11, ),
                    'login_email' => array( 'type' => 'TEXT' ,'null' => TRUE ),
                    'last_access' => array( 'type' => 'INT' ,'constraint' => 11, ),
                    'session_name' => array( 'type' => 'TEXT' ,'null' => TRUE ),
                    'session_started' => array( 'type' => 'INT' ,'constraint' => 11, ),
                    'session_ttl' => array( 'type' => 'INT' ,'constraint' => 11,'default' => 86400 ),
                    'session_data' => array( 'type' => 'LONGTEXT' ,'null' => TRUE ),
                    'recent_actions' => array( 'type' => 'VARCHAR' ,'constraint' => '500','default' => '[]' ),
                );
                $this->dbforge->add_field($fields);
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table($table, true);
                $UserSessions = [
                    'user_id' => $Contact->Id,
                    'login_email' => $Contact->Email,
                    'last_access' => time(),
                    'session_name'=> $session_name,
                    'session_started' => time(),
                    'session_ttl' => $PostData['keep_me_logged_in'] == 'yes' ? 0:86400,
                    //'session_ttl' => 0,
                    'session_data' => serialize($session_data)

                ];
                macanta_record_user_session($UserSessions);
                //$this->db->insert('user_sessions', $UserSessions);
                /*session_start();
                foreach ($session_data as $key => &$value)
                {
                    $_SESSION[$key] = $value;
                }
                session_write_close();*/
                manual_cache_delete('searched_cache'.$Contact->Id,true);
                manual_cache_delete('selected_searched_cache'.$Contact->Id);
                manual_cache_delete('');
                $this->retrieveCache('', false, $session_data);
                $FrontPageContent = $this->load->view('searchbox', $session_data, true);
                $FrontPageContent .= $this->load->view('contact_add', null, true);
                $this->ajaxResults['data'] = "<div class=\"SearchPage\">".$FrontPageContent."</div>";
                $identify = $this->config->item('MacantaAppName')."_".$Contact->Id;
                $SegmentParamIdentify = [
                    'email' => $Contact->Email,
                    'firstName' => $Contact->FirstName,
                    'lastName' => $Contact->LastName
                ];
                $SegmentParamTrack = [
                    'username' => $Contact->Email,
                    'firstName' => $Contact->FirstName,
                    'lastName' => $Contact->LastName,
                    'version' => $this->config->item('macanta_verson')
                ];
                $SegmentIdentify = 'analytics.identify("'.$identify.'", '.json_encode($SegmentParamIdentify).');';
                $SegmentTrack = 'analytics.track("Signed In", '.json_encode($SegmentParamTrack).');';
                $this->ajaxResults['script'] .= $SegmentIdentify.$SegmentTrack.'UserInfo = '.$session_data['Details'].";session_name='".$session_name."';localStorage.setItem('session_name', '".$session_name."');localStorage.setItem('autologin".$Contact->Id."', 'yes');".$this->getIntercomCode('update').";$('.front-page-logo div.description').append($('div.logoutautologin'));";
                //$this->ajaxResults['script'] .= 'ExternalWebForms = '. $this->retrieveWebforms().";";
                $ConnectedInfosEncoded = macanta_get_config('connected_info');
                if($ConnectedInfosEncoded !== false){
                    $this->ajaxResults['script'] .= '$(".main-header").hide();$("body").addClass("main-search signed-in").removeClass("dashboard login");$(".footer-content").show();ConnectedInfoSettings = '.$ConnectedInfosEncoded.';';
                    // $this->ajaxResults['script'] .= '$(".main-header").addClass("class_alignleft");$("div.main-header").detach().prependTo("div.page-footer");';


                    $this->ajaxResults['script'] .= 'localStorage.setItem("cd_settings", JSON.stringify('.$ConnectedInfosEncoded.'));';
                }
                //Add UserGuiding Code
                $Admin = $validateUser['Level'] == 'administrator' ? 'true':'false';
                $User = $Admin == 'true'? 'false':'true';
                $this->ajaxResults['script'] .="(function (u, s, e, r, g) { u[r] = u[r] || []; u[r].push({ 'ug.start': new Date().getTime(), event: 'embed.js', }); var f = s.getElementsByTagName(e)[0], j = s.createElement(e); j.async = true; j.src = 'https://static.userguiding.com/media/user-guiding-' + g + '-embedded.js';j.onload = function(){ userGuiding.track('segment', { AppAdmin: ".$Admin.", AppUser: ".$User.", LoggedIn: true}); }; f.parentNode.insertBefore(j, f); })(window, document, 'script', 'userGuidingLayer', '04844493ID');";
                return $this->ajaxResults;
            }else{
                // if this contact doesn't have any group
                $this->ajaxResults['data'] = $results;
                $this->ajaxResults['message'] = $this->lang->line('text_oops_looks_like');
                $Duration = time()-$start;

                return $this->ajaxResults;
            }
        }
        else
        {
            // if this contact entered invalid login
            $this->ajaxResults['data'] = $results;
            // $this->ajaxResults['message'] =  $this->lang->line('text_thanks_for_visiting')." - Code: 0001".json_encode($results);
            $this->ajaxResults['message'] =  $this->lang->line('text_thanks_for_visiting');
            $Duration = time()-$start;

            return $this->ajaxResults;
        }
    }
    public function getAutoLoginKey($ContactId){
        $this->db->where('user_id',$ContactId);
        $this->db->where('meta_key','autologin_key');
        $query = $this->db->get('users_meta');
        $ret = $query->row();
        $autologin_info =  $ret->meta_value;
        return $autologin_info;
    }
    public function autologinConfirmed($ContactId,$id){
        $AutoLoginTag = infusionsoft_get_tags_by_groupname('Allow Auto-Login');
        $key = $this->getAutoLoginKey($ContactId);
        if($key == $id){
            $Contact = infusionsoft_get_contact_by_id($ContactId, array('all'), false);
            $ContactTagArr = explode(',',$Contact->Groups);
            if(in_array($AutoLoginTag->Id,$ContactTagArr)){
                $this->login($Contact,true);
                $Script = "
                    <script>
                        localStorage.setItem('session_name', '".$this->session_name."');
                        window.location = '".$this->config->item('base_url')."';
                    </script>
                    ";
                echo $Script;
            }else{
                $Script = "
                    <script>
                        localStorage.setItem('LoginMessage', 'Sorry, the account(".$Contact->Email.") does not have permission to auto-login.');
                        window.location = '".$this->config->item('base_url')."';
                    </script>
                    ";
                echo $Script;
            }
        }else{
            $Script = "
            <script>
                localStorage.setItem('LoginMessage', 'ERROR: Autologin key did not match!, Please login manualy and check the correct autologin URL.');
                window.location = '".$this->config->item('base_url')."';
            </script>
            ";
            echo $Script;
        }
    }
    public function autologinold($id,$key){
        $Script = "
                        <script>
                            localStorage.setItem('LoginMessage', '<span class=\"green\">Thank you for using Autologin!<br>For best security, we added key phrase in your new Autologin URL. Please login manualy and get your new URL. Cheers!</span>');
                            window.location = '".$this->config->item('base_url')."';
                        </script>
            ";
        echo $Script;
    }
    public function autologin($id,$key){
        //die($id." : ".$key);
        $AutoLoginTag = infusionsoft_get_tags_by_groupname('Allow Auto-Login');
        if(isset($AutoLoginTag->Id)){
            $CacheRefreshScript = 'if(typeof serviceWorkerRegistration !== "undefined"){ serviceWorkerRegistration.unregister();}caches.delete(\'macanta-cache\');';
            $Script = "
                        <script>
                        var securityCode = '".$key."';
                        var allowAutoLogin = localStorage.getItem('autologin".$id."') || 'no';
                        console.log('Allow Auto Login: '+allowAutoLogin);
                        if(allowAutoLogin === 'yes'){
                            console.log('redirect to: ".$this->config->item('base_url')."autologin/confirmed/'+$id+'/'+securityCode);
                            ".$CacheRefreshScript."
                            window.location = '".$this->config->item('base_url')."autologin/confirmed/'+$id+'/'+securityCode;
                        }else{
                            console.log('LoginMessage: Sorry, Your Auto-login is initially disabled. Login manually to enable auto-login.');
                            localStorage.setItem('LoginMessage', 'Sorry, Your Auto-login is initially disabled. Login manually to enable auto-login.');
                            window.location = '".$this->config->item('base_url')."';
                        }
                        </script>
                        ";
        }else{
            $Script = "
                        <script>
                            console.log('LoginMessage: Sorry, Your app is not configured to process auto-login. [Missing \"Allow Auto-Login Tag\"]');
                            localStorage.setItem('LoginMessage', 'Sorry, Your app is not configured to process auto-login. [Missing \"Allow Auto-Login Tag\"].');
                            window.location = '".$this->config->item('base_url')."';
                        </script>
            ";
        }

        echo $Script;
        //header("location:".$this->config->item('base_url'));
    }
    public function logout($Data){
        $PostData = $Data;
        $Param['login_disabled'] = isset($PostData['login_disabled']) ? $PostData['login_disabled']:null;
        $FrontPageContent = $this->load->view('loginbox', $Param, true);
        $this->ajaxResults['status'] = 1;
        $reload = 'window.location.reload(true);';
        $CacheRefreshScript = "";
        if(isset($PostData['script'])){
            $CacheRefreshScript = $PostData['script'];
        }
        if(!empty($PostData['session_name'])){
            $this->db->where('session_name',$PostData['session_name']);
            $this->db->delete('user_sessions');
        }
        if(isset($PostData['reload']) && $PostData['reload'] == false) $reload = "";
        $this->ajaxResults['script'] = '$("body").addClass("login").removeClass("main-search dashboard");if(typeof serviceWorkerRegistration !== "undefined"){ serviceWorkerRegistration.unregister();}caches.delete(\'macanta-cache\');localStorage.removeItem("session_name");localStorage.removeItem("macanta_tips_shown");clearLastSearchFilter();clearLastSearchKey();$(".gotoAdmin").hide();$(".user-settings").hide();$(".logout").hide();removeHash ();removeAddedClasses();/*localStorage.clear();*/'.$CacheRefreshScript.$reload;
        $this->ajaxResults['data'] = $FrontPageContent;
        $this->ajaxResults['logged_out'] = 'yes';
        $this->ajaxResults['message'] = "You have successfully logged out.";
        return $this->ajaxResults;
    }
    public function getIntercomCode($action = 'boot', $session_name = ''){
        $Intercom='';// disable intercom
        return $Intercom;
        /*$user_seession_data = macanta_get_user_seession_data($session_name);
        if ($user_seession_data == false){
            $boot_data = [
                'app_id' => $this->IntercomAppId
            ];
            $boot_data = json_encode($boot_data);
            $Intercom = "window.Intercom('$action', $boot_data);";
        }else{
            $session_data = unserialize($user_seession_data->session_data);
            $LoggedInDetails = json_decode($session_data['details']);
            $LastName = isset($LoggedInDetails->LastName) ? $LoggedInDetails->LastName:"";
            $boot_data = [
                'app_id' => $this->IntercomAppId,
                'email' =>  $session_data['email'],
                'user_id' =>  $this->config->item('MacantaAppName').'-'.$session_data['InfusionsoftID'],
                'user_hash' => hash_hmac( 'sha256', $this->config->item('MacantaAppName').'-'.$session_data['InfusionsoftID'], $this->IntercomSecretKey ),
                'created_at' => time(),
                'company' => [
                    'id' => $session_data['companyid'],
                    'name' => $session_data['company'],
                ]
            ];
            $boot_data = json_encode($boot_data);
            $Intercom = "window.Intercom('$action', $boot_data);";
            $Upscale = '';
            if($this->config->item('UPSCOPE_APP')){
                $Upscale_data=[
                    'apiKey' => $this->config->item('UPSCOPE_APP'),
                    'sendScreenshotOnSessionStart' =>  true,
                    'identities' =>  [$LoggedInDetails->FirstName." ".$LastName, $session_data['email']],
                    'uniqueId' =>  $this->config->item('MacantaAppName').'-'.$session_data['InfusionsoftID'],
                    //'lookupCodeElement' =>  "",
                    'trackConsole' =>  true,
                    'allowRemoteConsole' =>  true
                ];
                $Upscale_data = json_encode($Upscale_data);
                $Upscale = "Upscope('init', $Upscale_data );";
            }

            $Intercom .= $Upscale;
        }*/

    }
    public function frontpage($Data){
        $start = time();
        $WriteLog = $this->config->item('MacantaAppName') == 'no' ? true:false;;
        $CurrentDir = dirname(__FILE__) . "/";

        $PostData = $Data;

        //MODIFY SEARCH BY TAG
        $note_tags_columns = $this->db->list_fields('note_tags');
        if(!in_array('contact_id',$note_tags_columns)){
            set_time_limit(86400);
            ini_set("memory_limit", "2048M");
            //Add contact_id into note_tags table
            $fields = array(
                'contact_id' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'after' => 'note_id'

                )
            );
            $this->dbforge->add_column('note_tags', $fields);

            //Remove contact_ids from tags table
            $this->dbforge->drop_column('tags', 'contact_ids');

            $NoteIds = [];
            $NoteIdsIndex = 0;
            $query = $this->db->get('note_tags');
            foreach ($query->result() as $row)
            {
                if(!is_numeric($row->note_id)) continue;
                if(sizeof($NoteIds[$NoteIdsIndex]) > 999) $NoteIdsIndex++;
                $NoteIds[$NoteIdsIndex][] = (int) $row->note_id;

            }
            $action = "query_is";
            foreach ($NoteIds as $Index => $Batch){
                $action_details = '{"table":"ContactAction","limit":"1000","page":0,"fields":["Id","ContactId"],"query":{"Id":'.json_encode($Batch).'}}';
                $Notes = applyFn('rucksack_request',$action, $action_details);
                foreach ($Notes->message as $Note){
                    $theData['contact_id'] = $Note->ContactId;
                    $this->db->where('note_id',$Note->Id);
                    $this->db->update('note_tags',$theData);
                }
            }
        }
        /*=========================================*/


        $login_disabled =  $this->config->item('login_disabled');
        if ($login_disabled && trim($login_disabled)!=''){
            $LOGINPARAM['login_disabled'] = $login_disabled;
            $FrontPageContent = $this->load->view('loginbox', $LOGINPARAM, true);
            // $this->ajaxResults['script'] = '$("body").addClass("login").removeClass("main-search dashboard");$(".gotoAdmin").fadeOut("fast");$(".user-settings").fadeOut("fast");$(".logout").fadeOut("fast");$(".login-message").fadeIn();';
            $this->ajaxResults['script'] = '$("body").addClass("login").removeClass("main-search dashboard");$(".main-header").show();$(".login-message").fadeIn();';
        }else{
            $session_name = $PostData['session_name'];
            $user_seession_data = macanta_get_user_seession_data($session_name);

            $session_data = unserialize($user_seession_data->session_data);
            $CurrentMacantaUser = macanta_get_user_access_by_id($session_data['InfusionsoftID']);
            $CurrentMacantaUserLevel = $CurrentMacantaUser['Level'];
            $Contact = new stdClass();
            $LoggedInUser = json_decode($session_data['Details']);
            $Contact->Id = $session_data['InfusionsoftID'];
            $Contact->AppName = $this->config->item('MacantaAppName');
            $Contact->UserType = $CurrentMacantaUserLevel;
            $Contact->FirstName = $LoggedInUser->FirstName;
            $Contact->LastName = $LoggedInUser->LastName;
            $Contact->Email = $session_data['Email'];
            //$this->ajaxResults['record'] = applyFn('record_user_actvity',$Contact);
            //$this->ajaxResults['user_success'] = applyFn('set_macanta_user_success',$session_data['email']);
            $start = time();
            $FrontPageContent = $this->load->view('searchbox', $session_data, true);
            $Duration = time()-$start;
            $ToRecordContactNotes = "Frontpage searchbox: " . $Duration . "\n----\n";
            if($WriteLog)
                file_put_contents($CurrentDir . "_CommonTimeLapse_".$this->config->item('MacantaAppName').".txt", $ToRecordContactNotes , FILE_APPEND);

            $start = time();
            $FrontPageContent .= $this->load->view('contact_add', null, true);
            $Duration = time()-$start;
            $ToRecordContactNotes = "Frontpage contact_add: " . $Duration . "\n----\n";
            if($WriteLog)
                file_put_contents($CurrentDir . "_CommonTimeLapse_".$this->config->item('MacantaAppName').".txt", $ToRecordContactNotes , FILE_APPEND);

            $FrontPageContent = "<div class=\"SearchPage\">".$FrontPageContent."</div>";
            // retrieve last search results
            $this->ajaxResults['status'] = 1;
            $this->retrieveCache('', false, $session_data);
            $this->ajaxResults['script'] .= '$(".main-header").hide();$("body").addClass("main-search").removeClass("login dashboard");$(".footer-content").show();'."$('.front-page-logo div.description').append($('div.logoutautologin'));$('div.main-header').addClass('class_alignleft');showLastSearchKey();renderContactSearch()";



        }
        $this->ajaxResults['data'] = $FrontPageContent;
        $this->ajaxResults['message'] = "Frontpage Loaded!";
        return $this->ajaxResults;
    }
    public function notetags_get(){
        $tags = array();
        $tagsStr = array();
        $query = $this->db->get('tags');
        foreach ($query->result() as $row)
        {
            $tagsStr[] = $row->tag_name;
            $tags[] = array(
                'id'=>$row->tag_slug,
                'label'=>$row->tag_name,
                'value'=>$row->tag_name
            );
        }
        //echo  json_encode($tags);
        return json_encode($tagsStr);
    }
    public function dashboard($Data){
        $start = time();
        $WriteLog = $this->config->item('MacantaAppName') == 'no' ? true:false;;
        $CurrentDir = dirname(__FILE__) . "/";
        $PostData = $Data;
        $login_disabled =  $this->config->item('login_disabled');
        $script = '';
        if ($login_disabled && trim($login_disabled)!=''){
            $LOGINPARAM['login_disabled'] = $login_disabled;
            $PageContent = $this->load->view('loginbox', $LOGINPARAM, true);
            // $this->ajaxResults['script'] = '$("body").addClass("login").removeClass("main-search dashboard");$(".gotoAdmin").fadeOut("fast");$(".user-settings").fadeOut("fast");$(".logout").fadeOut("fast");$(".login-message").fadeIn();';
            // $this->ajaxResults['script'] = '$("body").addClass("login").removeClass("main-search dashboard");$("main-header .description").fadeOut("fast");$(".gotoAdmin").fadeOut("fast");$(".user-settings").fadeOut("fast");$(".logout").fadeOut("fast");$(".login-message").fadeIn();';
            $this->ajaxResults['script'] = '$("body").addClass("login").removeClass("main-search dashboard");$(".login-message").fadeIn();';
        }else{
            if(isset($_GET['user'])){
                //die($_GET['user']);
                $Email = str_replace(' ', '+',$_GET['user']);
                $user_seession_data = macanta_get_user_seession_data_by_email($Email);
                if($user_seession_data == false){
                    $MacantaUsers = macanta_get_users();
                    foreach ($MacantaUsers as $MacantaUser){
                        if($Email == $MacantaUser->Email){
                            $this->login($MacantaUser,true);
                            $user_seession_data = macanta_get_user_seession_data_by_email($Email);
                         break;
                        }
                    }
                }
                $Session_data = unserialize($user_seession_data->session_data);
                $script = 'session_name = "'.$user_seession_data->session_name.'";localStorage.setItem("session_name", "'.$user_seession_data->session_name.'");';
                $this->ajaxResults['session_script'] = $script;
            }
            else{
                $session_name = $PostData['session_name'];
                $start = time();
                $user_seession_data = macanta_get_user_seession_data($session_name);
                $Duration = time()-$start;
                $ToRecordContactNotes = "get_user_seession_data: " . $Duration . "\n----\n";
                if($WriteLog)
                    file_put_contents($CurrentDir . "_CommonTimeLapse_".$this->config->item('MacantaAppName').".txt", $ToRecordContactNotes , FILE_APPEND);

                $Session_data = unserialize($user_seession_data->session_data);
            }
            $CurrentMacantaUser = macanta_get_user_access_by_id($Session_data['InfusionsoftID']);
            $CurrentMacantaUserLevel = $CurrentMacantaUser['Level'];
            $start = time();
            $PageContent = $this->load->view('dashboard', $Session_data, true);
            $Duration = time()-$start;
            $ToRecordContactNotes = "Frontpage dashboard: " . $Duration . "\n----\n";
            if($WriteLog)
                file_put_contents($CurrentDir . "_CommonTimeLapse_".$this->config->item('MacantaAppName').".txt", $ToRecordContactNotes , FILE_APPEND);

            $start = time();
            $PageContent .= $this->load->view('contact_add', null, true);
            $Duration = time()-$start;
            $ToRecordContactNotes = "Frontpage contact_add: " . $Duration . "\n----\n";
            if($WriteLog)
                file_put_contents($CurrentDir . "_CommonTimeLapse_".$this->config->item('MacantaAppName').".txt", $ToRecordContactNotes , FILE_APPEND);

            $Contact = new stdClass();
            $LoggedInUser = json_decode($Session_data['Details']);
            $Contact->Id = $Session_data['InfusionsoftID'];
            $Contact->AppName = $this->config->item('MacantaAppName');
            $Contact->UserType = $CurrentMacantaUserLevel;
            $Contact->FirstName = $LoggedInUser->FirstName;
            $Contact->LastName = $LoggedInUser->LastName;
            $Contact->Email = $Session_data['Email'];
            //$this->ajaxResults['record'] = applyFn('record_user_actvity',$Contact);
            //$this->ajaxResults['user_success'] = applyFn('set_macanta_user_success',$Session_data['email']);

            /*<!--
            Array
                (
                    [agent_details] => Array
                        (
                            [sid] => WKff5dfc4dba258313ee0d0c09ceb2777a
                            [friendlyName] => 455 | Geover Zamora
                            [accountSid] => AC60a487d1a6b0d138a56d417993809d9c
                            [activitySid] => WAef7cf3458e1d1e617456b18131e4ea6a
                            [activityName] => Offline
                            [workspaceSid] => WSac74bbb15b5079c31ed876b8ee383ac6
                            [attributes] => Array
                                (
                                    [skills] => Array
                                        (
                                            [0] => technical
                                            [1] => support
                                        )

                                    [languages] => Array
                                        (
                                            [0] => english
                                        )

                                    [infusionsoftId] => 455
                                    [email] => geover@gmail.com
                                )

                            [available] =>
                            [dateCreated] => Array
                                (
                                    [date] => 2018-02-27 08:54:34.000000
                                    [timezone_type] => 2
                                    [timezone] => Z
                                )

                            [dateUpdated] => Array
                                (
                                    [date] => 2018-02-27 08:54:34.000000
                                    [timezone_type] => 2
                                    [timezone] => Z
                                )

                            [dateSstatusChanged] => Array
                                (
                                    [date] => 2018-02-27 08:54:34.000000
                                    [timezone_type] => 2
                                    [timezone] => Z
                                )

                            [url] => https://taskrouter.twilio.com/v1/Workspaces/WSac74bbb15b5079c31ed876b8ee383ac6/Workers/WKff5dfc4dba258313ee0d0c09ceb2777a
                            [links] => Array
                                (
                                    [cumulative_statistics] => https://taskrouter.twilio.com/v1/Workspaces/WSac74bbb15b5079c31ed876b8ee383ac6/Workers/CumulativeStatistics
                                    [reservations] => https://taskrouter.twilio.com/v1/Workspaces/WSac74bbb15b5079c31ed876b8ee383ac6/Workers/WKff5dfc4dba258313ee0d0c09ceb2777a/Reservations
                                    [real_time_statistics] => https://taskrouter.twilio.com/v1/Workspaces/WSac74bbb15b5079c31ed876b8ee383ac6/Workers/RealTimeStatistics
                                    [statistics] => https://taskrouter.twilio.com/v1/Workspaces/WSac74bbb15b5079c31ed876b8ee383ac6/Workers/Statistics
                                    [worker_channels] => https://taskrouter.twilio.com/v1/Workspaces/WSac74bbb15b5079c31ed876b8ee383ac6/Workers/WKff5dfc4dba258313ee0d0c09ceb2777a/Channels
                                    [channels] => https://taskrouter.twilio.com/v1/Workspaces/WSac74bbb15b5079c31ed876b8ee383ac6/Workers/WKff5dfc4dba258313ee0d0c09ceb2777a/Channels
                                    [worker_statistics] => https://taskrouter.twilio.com/v1/Workspaces/WSac74bbb15b5079c31ed876b8ee383ac6/Workers/WKff5dfc4dba258313ee0d0c09ceb2777a/Statistics
                                    [workspace] => https://taskrouter.twilio.com/v1/Workspaces/WSac74bbb15b5079c31ed876b8ee383ac6
                                    [activity] => https://taskrouter.twilio.com/v1/Workspaces/WSac74bbb15b5079c31ed876b8ee383ac6/Activities/WAef7cf3458e1d1e617456b18131e4ea6a
                                )

                        )

                )
            -->
            */

            $this->ajaxResults['status'] = 1;
            $start = time();
            $this->retrieveCache('',true,$Session_data);
            $Duration = time()-$start;
            $ToRecordContactNotes = "retrieveCache: " . $Duration . "\n----\n";
            if($WriteLog)
                file_put_contents($CurrentDir . "_CommonTimeLapse_".$this->config->item('MacantaAppName').".txt", $ToRecordContactNotes , FILE_APPEND);

            //$end = time();
            /*$lapse = $end - $start;
            if($this->config->item('MacantaAppName') == 'qj395'){
                file_put_contents(dirname(__FILE__)."/".__CLASS__."-class.txt",date('Y-m-d H:i:s').": ".$lapse."\n",FILE_APPEND);
            }*/
            $this->ajaxResults['script'] .= "showLastSearchKey();";
            if(isset($CurrentMacantaUserLevel) && $CurrentMacantaUserLevel == 'administrator'){
                $CallCenterAdmin = 'CallCenterAdmin=true;';
                $this->ajaxResults['script'] .= '$("body").addClass("admin");$(".main-header").hide();';
            }
            if(isset($Session_data['UserMeta']['agent_details'])){
                $CallCenterAgent = 'CallCenterAgent=true;';
                $sid = $Session_data['UserMeta']['agent_details']['sid'];
                $workspaceSid = $Session_data['UserMeta']['agent_details']['workspaceSid'];
                $this->ajaxResults['script'] .= '$("body").addClass("dashboard").removeClass("login main-search");agentSid="'.$sid.'";workspaceSid="'.$workspaceSid.'";'.$CallCenterAdmin.$CallCenterAgent;
            }
            $Admin = $CurrentMacantaUser['Level'] == 'administrator' ? 'true':'false';
            $User = $Admin == 'true'? 'false':'true';
            $this->ajaxResults['script'] .="(function (u, s, e, r, g) { u[r] = u[r] || []; u[r].push({ 'ug.start': new Date().getTime(), event: 'embed.js', }); var f = s.getElementsByTagName(e)[0], j = s.createElement(e); j.async = true; j.src = 'https://static.userguiding.com/media/user-guiding-' + g + '-embedded.js';j.onload = function(){ userGuiding.track('segment', { AppAdmin: ".$Admin.", AppUser: ".$User.", LoggedIn: true}); }; f.parentNode.insertBefore(j, f); })(window, document, 'script', 'userGuidingLayer', '04844493ID');";

        }
        $this->ajaxResults['data'] = $PageContent;
        $this->ajaxResults['message'] = "PageContent Loaded!";
        return $this->ajaxResults;
    }
    public function addTagCatMenu($Title,$FullTitle, $Key){
        if(strlen($FullTitle) > 30 ){
            $Objs = '{text:'.$Title.',tooltip:'.json_encode($FullTitle).',onclick: function() {editor.insertContent(\'[TagCat id='.$Key.' readonly=no]\');}}';
        }else{
            $Objs = '{text:'.$Title.',onclick: function() {editor.insertContent(\'[TagCat id='.$Key.'  readonly=no]\');}}';
        }
        return $Objs;
    }
    public function addWebFormMenu($Title,$FullTitle, $Key){
        if(strlen($FullTitle) > 30 ){
            $Objs = '{text:'.mb_convert_encoding($Title, "HTML-ENTITIES", "UTF-8").',tooltip:'.json_encode($FullTitle).',onclick: function() {editor.insertContent(\'[ISwebform formid='.$Key.' readonly=false createnote=no assign_note_to_contact_owner=no submitted_by_customfield=""]\');}}';
        }else{
            $Objs = '{text:'.mb_convert_encoding($Title, "HTML-ENTITIES", "UTF-8").',onclick: function() {editor.insertContent(\'[ISwebform formid='.$Key.' readonly=false createnote=no assign_note_to_contact_owner=no submitted_by_customfield=""]\');}}';
        }
        return $Objs;
    }
    public function getTagCategories(){
        $TagCategories = infusionsoft_get_tags_category();
        //addTagCatMenu($Title,$FullTitle, $Key)
        $Objs = [];

        $TagCategoriesArr = array();
        foreach($TagCategories as $TagCategory){
            $TagCategoriesArr[$TagCategory->Id] = $TagCategory->CategoryName;
            $val = html_entity_decode($TagCategory->CategoryName);
            $toolTip = $val;
            $valShorten =  strlen($val) > 30 ? substr($val, 0, 30)."...":$val;
            $val = json_encode($valShorten);
            if(!$val) $val = "'$toolTip'";
            $Objs[] = $this->addTagCatMenu($val,$toolTip, $TagCategory->Id);
        }
        manual_cache_writer('TagCategories', json_encode($TagCategoriesArr),86400);
        $theMenu = implode(',',$Objs);

        return $theMenu;
    }
    public function refreshwebform(){
        infusionsoft_get_webform_map(true);
        infusionsoft_refresh_webform_html();
        infusionsoft_refresh_get_opportunities();
        infusionsoft_generate_webform_menu( true );
    }
    public function refreshISsettings(){
        infusionsoft_refresh_settings();
    }
    public function refreshCustomFields($Data){
        $session_name = $Data['session_name'];
        $user_seession_data = macanta_get_user_seession_data($session_name);
        $session_data = unserialize($user_seession_data->session_data);
        infusionsoft_get_webform_map(true);
        infusionsoft_refresh_get_tags_by_catId();
        infusionsoft_refresh_webform_html();
        infusionsoft_refresh_get_opportunities(false, $session_data);
        infusionsoft_generate_webform_menu( true );
        infusionsoft_get_tag_categories_menu(true);
        get_opp_stages("%", true);
        infusionsoft_refresh_custom_fields();
        infusionsoft_refresh_settings();
        infusionsoft_get_email_template(true);
        infusionsoft_get_tags_category('','', true);
        infusionsoft_get_email_template_content('', true);
        infusionsoft_get_contact_action_turbo_dial_header_id(true);
        infusionsoft_contact_custom_fields(true);
        infusionsoft_get_custom_fields("%",true, -4, false,false);
    }
    public function getWebforms(){
        $ValidExternalForm = manual_cache_loader('ValidISwebForms');
        $ValidExternalForm = $ValidExternalForm ? json_decode($ValidExternalForm):[];
        $Objs = [];
        $WebFormPairs = infusionsoft_get_webform_map();
        foreach($WebFormPairs as $key => $val){
            $val = str_replace("\n", "", $val);
            $val = str_replace("~br~", " ", $val);
            $val = html_entity_decode($val);
            $toolTip = $val;
            $valShorten =  strlen($val) > 30 ? substr($val, 0, 30)."...":$val;
            $val = json_encode($valShorten);
            if(!$val) $val = "'$toolTip'";
            if(!array_key_exists($key,$ValidExternalForm)){
                $ValidExternalForm[$key]['Title'] = $val;
                $theForm = infusionsoft_get_webform_html($key);
                if (strpos($theForm, '<form') !== false) {
                    $ValidExternalForm[$key]['Valid'] = 1;
                    $Objs[] = $this->addWebFormMenu($val,$toolTip, $key);
                    continue;
                }else{
                    $ValidExternalForm[$key]['Valid'] = 0;
                    continue;
                }
            }elseif($ValidExternalForm[$key]['Valid']){
                $Objs[] = $this->addWebFormMenu($val,$toolTip, $key);
                continue;
            }

        }
        manual_cache_writer('ValidISwebForms', json_encode($ValidExternalForm),86400);
        $theMenu = implode(',',$Objs);
        return base64_encode($theMenu);
    }
    public function retrieveWebforms(){
        $theMenu = $this->getWebforms();
        $webFormObjs = base64_decode($theMenu);
        return $webFormObjs;
    }
    public function retrieveCache($TourScript = '', $dashboard = false,$Session_data){
        $CurrentMacantaUser = macanta_get_user_access_by_id($Session_data['InfusionsoftID']);
        $CurrentMacantaUserLevel = $CurrentMacantaUser['Level'];
        $CurrentMacantaUserAccess = $CurrentMacantaUser['Access'];
        $availableTags = $this->notetags_get();
        if(isset($Session_data['InfusionsoftID'])){
            $searched_cache = manual_cache_loader('searched_cache'.$Session_data['InfusionsoftID'],true);
            $searched_cache = $searched_cache ? $searched_cache: '{}';
            $selected_searched_cache = '""';
            $searched_column_cache = '{}';
            if($dashboard == false){
                $selected_searched_cache = manual_cache_loader('selected_searched_cache'.$Session_data['InfusionsoftID']);
                $searched_column_cache = manual_cache_loader('searched_column_cache'.$Session_data['InfusionsoftID']);
                $selected_searched_cache = $selected_searched_cache ? $selected_searched_cache: '""';
                $searched_column_cache = $searched_column_cache ? $searched_column_cache: '{}';
            }
            if(isset($CurrentMacantaUserLevel) && $CurrentMacantaUserLevel == 'administrator'){
                $AdminButton = '$(".gotoAdmin").fadeIn("fast");$(".gotoAdmin").attr("data-href","'.$this->config->item('base_url').'#contact/'.$Session_data['InfusionsoftID'].'");';
            }else{
                $AdminButton = '$(".gotoAdmin").fadeOut("fast");$(".gotoAdmin").attr("data-href","'.$this->config->item('base_url').'#contact/'.$Session_data['InfusionsoftID'].'");';
            }
            $this->ajaxResults['script'] = $AdminButton."availableTags=$availableTags;$('.user-settings').fadeIn('fast');$('.logout').fadeIn('fast'); SelectedSearchCache = $selected_searched_cache; SearchCache = $searched_cache; DataTableColumn = $searched_column_cache;Macanta('Common retrieveCache');";
        }

    }
    public function search($Data){
        set_time_limit(86400);
        ini_set("memory_limit", "2048M");
        $start = time();

        $PostData = $Data;
        $Source = $PostData['source'];
        $noteTags = $PostData['noteTags'];
        $session_name = $PostData['session_name'];
        $user_seession_data = macanta_get_user_seession_data($session_name);
        $Session_data = unserialize($user_seession_data->session_data);
        $SearchResults = array();
        $LoggedInUser = isset($Session_data['Details']) ? json_decode($Session_data['Details']) : NULL;
        manual_cache_delete('searched_column_cache'.$Session_data['InfusionsoftID']);
        manual_cache_delete('selected_searched_cache'.$Session_data['InfusionsoftID']);
        manual_cache_delete('connected_searched_cache'.$Session_data['InfusionsoftID'], true);
        manual_cache_delete('searched_cache'.$Session_data['InfusionsoftID'],true);
        manual_cache_delete('search_xcolumn_cache'.$Session_data['InfusionsoftID']);
        if(strpos($PostData['searchKey'],':') === false){
            $PostData['searchKey'] = 'default:'.$PostData['searchKey'];
        }
        $searchKey = explode(':', $PostData['searchKey']);
        if (trim($searchKey[1])!= ''){
            $SearchResults = infusionsoft_get_contact_by_search_string($PostData['searchKey'],$Session_data, false);
        }elseif(trim($noteTags) != ''){
            $SearchResults = infusionsoft_get_contact_by_note_tags($noteTags,$Session_data);
        }
        $extracomlumn = "";
        $search_xcolumn_cache="";
        if (strpos($PostData['searchKey'], 'connected:') !== false) {
            manual_cache_writer('connected_searched_cache'.$Session_data['InfusionsoftID'], json_encode($SearchResults),86400, true);
        }else{
            if (strpos($PostData['searchKey'], 'address:') !== false) {
                manual_cache_writer('search_xcolumn_cache'.$Session_data['InfusionsoftID'], "StreetAddress1",86400);
                $extracomlumn = "<th>StreetAddress1</th>";
                $search_xcolumn_cache="StreetAddress1";
            }
            manual_cache_writer('searched_cache'.$Session_data['InfusionsoftID'], json_encode($SearchResults),86400,true);
        }
        $TableHead = "<tr>
                               <th>Name</th>
                               <th>Company</th>
                               <th>Email</th>
                               $extracomlumn
                               <th>City</th>
                               <th>Postcode</th>
        </tr>";
        if (strpos($PostData['searchKey'], 'connected:') !== false) {
            $Script = "window.location.reload();";
        }else{
            $Script = "$('.search-results .panel-title').html('Contact Search Result');$('div.logoutautologin').appendTo('.description');if(ContactsDataTable){ContactsDataTable.destroy();}DataTableColumn={};$('.search_filter').selectpicker('val', '');$('.search_filter').selectpicker('render');$('#ContactsTable').removeClass('TaskSearch').removeClass('MacantaTaskSearch').removeClass('MacantaQuery');$('#ContactsTable tbody').html('');$('#ContactsTable thead').html(".json_encode($TableHead).");search_xcolumn_cache='$search_xcolumn_cache';SearchCache = ".json_encode($SearchResults).";clearLastSearchFilter();renderContactSearch();";
            if(sizeof($SearchResults) == 1 && isset($SearchResults[0]->Id)){
                $Script .= "$('.front-page-logo').addClass('class_alignleft'); $('.front-page-logo .description').addClass('class_alignright');window.location = '".$this->config->item('base_url').'#contact/'.$SearchResults[0]->Id."';";
                $Script .= "var theDelay = setTimeout(function () { window.location.reload();},50);";
            }
        }

        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['data'] = $SearchResults;
        $this->ajaxResults['message'] = "Search Results by user $Session_data[InfusionsoftID]:";
        $this->ajaxResults['script'] = $Script;
        //$this->ajaxResults['script'] = "";
        $Duration = time()-$start;

        return $this->ajaxResults;
    }
    public function quick_search($Data){
        $PostData = $Data;
        $SearchResults = array();
        $session_name = $PostData['session_name'];
        $user_seession_data = macanta_get_user_seession_data($session_name);
        $Session_data = unserialize($user_seession_data->session_data);
        if (trim($PostData['searchKey'])!= ''){
            $SearchResults = infusionsoft_get_contact_by_search_string($PostData['searchKey'],$Session_data);
        }
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['data'] = $SearchResults;
        return $this->ajaxResults;
    }
    public function verifyEmail($Data){
        $verifyResults = infusionsoft_get_contact_by_email($Data['Email']);
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['data'] = $verifyResults;
        if(isset($verifyResults[0]->Id)){
            $this->ajaxResults['message'] = '
            <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Notice!</strong> A contact with this email address already exists. <a href="'.$this->config->item('base_url').'#contact/'.$verifyResults[0]->Id.'">Click here to view contact</a></div>';
            $this->ajaxResults['script'] = "window.open('".$this->config->item('base_url').'#contact/'.$verifyResults[0]->Id."','_self');";
        }else{
            $this->ajaxResults['message'] = 'Email Not Existing';
        }
        return $this->ajaxResults;
    }
    public function savesearchreport($Data){
        set_time_limit(3600);
        ini_set('display_errors', 1);
        ini_set('memory_limit', '4000M');
        error_reporting(E_ERROR);
        $PostData = $Data;
        $savedSearchID = $PostData['SearchId'];
        $userID = $PostData['UserId'] == 0 ? 1:$PostData['UserId'];
        $Group = $PostData['Group'];
        $GlobalAccess = $PostData['GlobalAccess'];
        $user_seession_data = macanta_get_user_seession_data($PostData['session_name']);
        $Session_data = unserialize($user_seession_data->session_data);
        $pageNumber = 0;
        $SearchResults = infusionsoft_get_all_saved_search_contact_report($savedSearchID, $userID, $Group, $Session_data, $pageNumber);
        $LoggedInUser = isset($Session_data['Details']) ? json_decode($Session_data['Details']) : NULL;
        manual_cache_delete('connected_searched_cache'.$LoggedInUser->Id, true);
        manual_cache_writer('searched_cache'.$LoggedInUser->Id, json_encode($SearchResults['NewResults']),86400,true);
        manual_cache_writer('selected_searched_cache'.$LoggedInUser->Id, json_encode($PostData['UserId'].':'.$savedSearchID),86400);
        manual_cache_writer('searched_column_cache'.$LoggedInUser->Id, json_encode($SearchResults['TableColumn']),86400);
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['data'] = $SearchResults;
        $this->ajaxResults['message'] = "Search Results by user $LoggedInUser->Id:";
        $this->ajaxResults['script'] = "clearLastSearchFilter();clearLastSearchKey();location.reload();";
        //$this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function getWebformAPI($Data){
        $PostData = $Data;
        $user_seession_data = macanta_get_user_seession_data($PostData['session_name']);
        $Session_data = unserialize($user_seession_data->session_data);
        $LoggedInUser = isset($Session_data['Details']) ? json_decode($Session_data['Details']) : NULL;
        $app = $PostData['app'];
        $formid = $PostData['formid'];

        $web_form = infusionsoft_get_webform_html($formid);
        $re = "/<form\\b[^>]*>(.*?)<\\/form>/is";
        preg_match($re, $web_form, $theForm);
        $theForm[0] = str_replace("Infusion.depends(['Component.Calendar']);","",$theForm[0]);
        $theForm[0] = preg_replace('/(<([^>]*))(id=("((inf_field)|(inf_option)|(inf_misc)|(inf_custom)|(inf_custom))[^"]*"))/', '$1', $theForm[0]);
        $Searches = ['id="Email"','id="FirstName"','id="LastName"','id="StreetAddress1"','id="StreetAddress2"'];
        $theForm[0] = str_replace($Searches, '', $theForm[0]);
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['data'] = html_entity_decode($theForm[0]) ;
        $this->ajaxResults['message'] = "getWebform Results by user $LoggedInUser->Id:";
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function getWebform($Data){
        $PostData = $Data;
        $user_seession_data = macanta_get_user_seession_data($PostData['session_name']);
        $Session_data = unserialize($user_seession_data->session_data);
        $LoggedInUser = isset($Session_data['Details']) ? json_decode($Session_data['Details']) : NULL;
        $app = $PostData['app'];
        $formid = $PostData['formid'];
        $web_form = infusionsoft_get_web_form($app,$formid);
        $re = "/<form\\b[^>]*>(.*?)<\\/form>/is";
        preg_match($re, $web_form, $theForm);
        $theForm[0] = str_replace("Infusion.depends(['Component.Calendar']);","",$theForm[0]);
        $theForm[0] = preg_replace('/(<([^>]*))(id=("((inf_field)|(inf_option)|(inf_misc)|(inf_custom)|(inf_custom))[^"]*"))/', '$1', $theForm[0]);
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['data'] = $theForm[0];
        $this->ajaxResults['message'] = "getWebform Results by user $LoggedInUser->Id:";
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function getCustomFieldValue($Data){
        $PostData = $Data;
        $user_seession_data = macanta_get_user_seession_data($PostData['session_name']);
        $Session_data = unserialize($user_seession_data->session_data);
        $LoggedInUser = isset($Session_data['Details']) ? json_decode($Session_data['Details']) : NULL;
        $CustomFieldsArr = isset($PostData['CustomFields']) ? $PostData['CustomFields']: array('all');
        $ContactId = $PostData['ContactId'];
        $Contact = infusionsoft_get_contact_CF_by_id($ContactId, $CustomFieldsArr);

        foreach($Contact as $FieldName => $FieldValue){
            foreach (infusionsoft_get_custom_fields() as $ISCustomField){
                if($ISCustomField->Name == str_replace('_',"",  $FieldName)){
                    if($ISCustomField->DataType == 13){
                        $FieldValueTemp = $FieldValue->date;
                        $FieldValue =  date('Y-m-d', strtotime(trim($FieldValueTemp)));
                        $Contact->$FieldName = $FieldValue;
                    }
                    if($ISCustomField->DataType == 14){
                        $FieldValueTemp = $FieldValue->date;
                        $FieldValue =  date('Y-m-d H:i', strtotime(trim($FieldValueTemp)));
                        $Contact->$FieldName = $FieldValue;
                    }
                }
            }
        }


        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['data'] = $Contact;
        $this->ajaxResults['message'] = "getCustomFieldValue Results by user $LoggedInUser->Id:";
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function DOMinnerHTML(DOMNode $element)
    {
        $innerHTML = "";
        $children = $element->childNodes;
        foreach ($children as $child)
        {
            $innerHTML .= $element->ownerDocument->saveHTML($child);
        }
        return $innerHTML;
    }
    public function getElContentsByTag($html,$tag)
    {
        $doc = new DOMDocument();
        $doc->loadHTML($html);//Turn the $html string into a DOM document
        $els = $doc->getElementsByTagName($tag); //Find the elements matching our tag name ("div" in this example)
        foreach($els as $el)
        {
            //for each element, get the class, and if it matches return it's contents
            return $this->DOMinnerHTML($el);
        }
    }
    public function getElContentsByTagClass($html,$tag,$class)
    {
        $doc = new DOMDocument();
        $doc->loadHTML($html);//Turn the $html string into a DOM document
        $els = $doc->getElementsByTagName($tag); //Find the elements matching our tag name ("div" in this example)
        foreach($els as $el)
        {
            //for each element, get the class, and if it matches return it's contents
            $classAttr = $el->getAttribute("class");
            if(preg_match('#\b'.$class.'\b#',$classAttr) > 0) return $this->DOMinnerHTML($el);
        }
    }
    public function getElContentsByTagAttr($html,$tag,$attr,$attrVal)
    {
        $doc = new DOMDocument();
        $doc->loadHTML($html);//Turn the $html string into a DOM document
        $els = $doc->getElementsByTagName($tag); //Find the elements matching our tag name ("div" in this example)
        foreach($els as $el)
        {
            //for each element, get the class, and if it matches return it's contents
            $classAttr = $el->getAttribute($attr);
            if(preg_match('#\b'.$attrVal.'\b#',$classAttr) > 0) return $this->DOMinnerHTML($el);
        }
        return false;
    }
    public function send_mail_post($Data){
        $PostData = $Data;
        $this->load->library('mailer');
        $EmailTo = 'help@macantacrm.com';
        $Subject = $PostData['subject'];
        $AppName = $this->config->item('MacantaAppName');
        $session_name = $PostData['session_name'];
        if(!empty($session_name)){
            $EmailTemplate = 'logged_email_template.php';
            $user_seession_data = macanta_get_user_seession_data($session_name);
            $session_data = unserialize($user_seession_data->session_data);
            $CurrentMacantaUser = macanta_get_user_access_by_id($session_data['InfusionsoftID']);
            $CurrentMacantaUserLevel = $CurrentMacantaUser['Level'];
            $CurrentMacantaUserAccess = $CurrentMacantaUser['Access'];
            $templateData = [
                'AppName' => $AppName,
                'LoggedInUserEmail'=> $session_data['Email'],
                'InfusionsoftId'=> $session_data['InfusionsoftID'],
                'Name' =>  $PostData['name'],
                'Email' => $PostData['email'],
                'Subject' =>  $PostData['subject'],
                'UserLevel'=> $CurrentMacantaUserLevel,
                'MessageBody' => $PostData['message']
            ];
        }else{
            $EmailTemplate = 'email_template.php';
            $templateData = [
                'AppName' => $AppName,
                'Name' =>  $PostData['name'],
                'Email' => $PostData['email'],
                'Subject' =>  $PostData['subject'],
                'MessageBody' => $PostData['message']
            ];
        }


        $SendResult = $this->mailer
            ->to($EmailTo)
            ->senderEmail($PostData['email'])
            ->senderName($PostData['name'])
            ->subject($Subject)
            ->send($EmailTemplate, $templateData);
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['SendResult'] = $SendResult;
        $this->ajaxResults['PostData'] = $PostData;
        $this->ajaxResults['templateData'] = $templateData;
        $this->ajaxResults['EmailTemplate'] = $EmailTemplate;
        return $this->ajaxResults;
    }
    public function submitWebForm($Data){
        $PostData = $Data;
        $action = $PostData['url'];
        macanta_add_user_last_action($PostData['session_name'], 'WebFormSubmitted');
        $createNote = $PostData['createNote'];
        $ContactId = $PostData['ContactId'];
        $session_name = $PostData['session_name'];
        $user_seession_data = macanta_get_user_seession_data($session_name);
        $session_data = unserialize($user_seession_data->session_data);
        $assignNoteToContactOwner = $PostData['assignNoteToContactOwner'];
        $submitted_by_customfield = trim($PostData['submitted_by_customfield']);
        $radioMaps = $PostData['radioMaps'];
        //$action = str_replace("/process/","/processWebForm/",$action);
        //manual_cache_delete('searched_cache'.$session_data['InfusionsoftID']);

        $DateFields = ["Anniversary","Birthday","Validated","DateCreated","LastUpdated"];
        parse_str($PostData['postdata'], $DataPassed);
        $toNotes = $DataPassed;
        $FollowUpData = [];
        $FollowUpResults = '';
        $ContactNoteBody = '';
        unset($toNotes['inf_form_xid']);
        unset($toNotes['infusionsoft_version']);
        $toCache  = [];
        foreach ($toNotes as $theFieldName => $theValue){
            $toCacheName = '';
            if(strpos($theFieldName,'inf_field_')!==false){
                $toCacheName =  str_replace('inf_field_',"", $theFieldName);
                $toCache[] = ['name' => $toCacheName,'value'=>$theValue];
            }
            if(strpos($theFieldName,'inf_custom')!==false){
                $toCacheName =  str_replace('inf_custom',"", $theFieldName);
                $toCache[] = ['name' => $toCacheName,'value'=>$theValue];
            }

            if(isset($radioMaps[$theFieldName])) $theValue = $radioMaps[$theFieldName];
            $theFieldName = str_replace('inf_form_name',"Form Title", $theFieldName);
            $theFieldName = str_replace('inf_field_',"", $theFieldName);
            $theFieldName = str_replace('inf_custom_',"", $theFieldName);
            $ContactNoteBody .= $theFieldName.": ".$theValue."\n";
        }

        //Disable this infusionsoft_update_contact has this already
        //UpdateSearchedCache($toCache, $ContactId,$session_data);

        $ContactNoteBody = json_encode($ContactNoteBody);
        foreach ($DateFields as $DateField){
            if(isset($DataPassed['inf_field_'.$DateField])){
                $FollowUpData[] = ['name'=>$DateField, 'value' => $DataPassed['inf_field_'.$DateField] !== '' ?  infuDate($DataPassed['inf_field_'.$DateField]):''];
                //$ContactNoteBody .= $DateField.": ".$DataPassed['inf_field_'.$DateField]."\n";
                unset($DataPassed['inf_field_'.$DateField]);
            }
        }
        $query  = explode('&', $PostData['postdata']);
        $params = array();
        foreach( $query as $param )
        {
            list($name, $value) = explode('=', $param, 2);
            $params[urldecode($name)][] = urldecode($value);
        }
        foreach ($params as $FieldName=>$FieldValueArr){
            if(strpos($FieldName, 'inf_custom_') !== false){
                $name = str_replace('inf_custom_', '', $FieldName);
                foreach (infusionsoft_get_custom_fields() as $ISCustomField){
                    if($ISCustomField->Name == $name){
                        if($ISCustomField->DataType == 13){
                            $FollowUpData[] = ['name'=>'_'.$name, 'value' => $FieldValueArr[0] !== '' ? infuDateCustom($FieldValueArr[0]):''];
                            //$ContactNoteBody .= $FieldName.": ".$FieldValueArr[0]."\n";
                            unset($DataPassed['inf_custom_'.$name]);
                        }
                        if($ISCustomField->DataType == 14){
                            $DateTime = trim($FieldValueArr[0]." ".$FieldValueArr[1]);
                            $FollowUpData[] = ['name'=>'_'.$name, 'value' => $DateTime !== '' ? infuDateCustom($DateTime):''];
                            //$ContactNoteBody .= $FieldName.": ".$DateTime."\n";
                            unset($DataPassed['inf_custom_'.$name]);
                        }
                    }
                }
            }
        }

        $PostData['postdata'] = http_build_query($DataPassed);
        $opts = array(
            'http' => array(
                'method'  => 'POST',
                'follow_location' => 1,
                'header' =>
                //"Accept:  */*\r\n" .
                //"Connection: keep-alive\r\n" .
                //"Content-Length: ".strlen($PostData['postdata'])."\r\n" .
                    "Accept-language: en-US;q=0.7,en;q=0.6\r\n" .
                    "Content-type: application/x-www-form-urlencoded\r\n"
                //"Cookie: foo=bar\r\n" .  // check function.stream-context-create on php.net
                //"User-Agent: Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.102011-10-16 20:23:10\r\n"
            ,
                'content' => $PostData['postdata']
            )/*,
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),*/
        );
        $ckfile = tempnam("/tmp", "infusionsoft");
        $ch = curl_init($action);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $ckfile);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData['postdata']);
        $HTML = curl_exec($ch);
        curl_close($ch);
        if($HTML !== false){
            manual_cache_delete('infusionsoft_get_contact_by_id_'.$ContactId);
            $Body = trim($this->getElContentsByTag($HTML,'body'));
            $NeedCaptcha = strstr($Body,'captchaForm') == false ?  false:true;
            $WebformError = $this->getElContentsByTagAttr($HTML,'div','id','webformErrors');
            $SystemError_640 = strstr($Body,'Error 640') == false ?  false:true;
            $ThankYouText = '';
            $ThankYouPageURL = '';
            if($WebformError === false && $SystemError_640 === false){
                if($NeedCaptcha === false) {
                    if(sizeof($FollowUpData) > 0)
                        $FollowUpResults = infusionsoft_update_contact($FollowUpData,$ContactId, '',$session_data);
                    preg_match('!https?://\S+!', $Body, $matches);
                    // this is not detecting
                    //<script type="text/javascript">window.location='https://swampschool.infusionsoft.com/app/form/success/72cc5fa8ca4135d91c556964f7d58046/86877/ef1a0c073239f32ed37e5a841652fae5'</script>
                    $needle = ["</script>","'",";","}"];
                    $ThankYouPageURL = str_replace($needle,'',$matches[0]);
                    //$ThankYouPage = file_get_contents($ThankYouPageURL);
                    $ThankYouPage = $this->curlMe($ThankYouPageURL);
                    $TheTable = $this->getElContentsByTagClass($ThankYouPage,'table','bodyContainer');
                    if(trim($TheTable) != ''){
                        $TheTable = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $TheTable);
                        $TheTable = str_replace('&nbsp;','',$TheTable);
                        $TheTable = str_replace("\n",'',$TheTable);
                        $TheTable = str_replace("\r",'',$TheTable);
                        $TheTable = str_replace("\xC2",'',$TheTable);
                        $TheTable = str_replace("\xA0",'',$TheTable);
                        $ThankYouText = $TheTable;
                    }else{
                        $ThankYouText = $this->getElContentsByTagClass($ThankYouPage,'div','text');
                    }
                    if($submitted_by_customfield !== ''){
                        if($submitted_by_customfield[0] !== '_') $submitted_by_customfield = '_'.$submitted_by_customfield;
                        $SubmittedBy = [];
                        $LoggedInUser = json_decode($session_data['Details']);
                        $SubmittedBy[] = ['name'=>$submitted_by_customfield, 'value' => $LoggedInUser->FirstName.' '.$LoggedInUser->LastName];
                        $FollowUpResultsSubmmitedBy = infusionsoft_update_contact($SubmittedBy,$ContactId, '',$session_data);
                    }
                    $exec_string = '/usr/bin/php /var/www/macanta/shared/services/sync.php '.$this->config->item('MacantaAppName').' 2 '.$ContactId;
                    exec($exec_string,$output);
                }
            }
        }else{
            $Body = false;
            $NeedCaptcha = false;
            $WebformError = false;
            $SystemError_640 = false;
            $ThankYouText = '';
            $ThankYouPageURL = '';
        }
        $AddToScript = $createNote == 'yes' ? "createQuickNoteAfterFormSubmission($ContactNoteBody,'Form Submission','#form-submission','Other','".$assignNoteToContactOwner."');":"";
        $this->ajaxResults['Exec_Output'] = $output;
        $this->ajaxResults['RawOutput'] = $HTML;
        $this->ajaxResults['Opts'] = $opts;
        $this->ajaxResults['BodyOutput'] = $Body;
        $this->ajaxResults['NeedCaptcha'] = $NeedCaptcha;
        $this->ajaxResults['WebformError'] = $WebformError;
        $this->ajaxResults['SystemError_640'] = $SystemError_640;
        $this->ajaxResults['Data']['Submited'] = $PostData['postdata'];
        $this->ajaxResults['Data']['ApiSubmited'] = $FollowUpData;
        $this->ajaxResults['Data']['FollowUpResultsSubmmitedBy'] = $FollowUpResultsSubmmitedBy;
        $this->ajaxResults['Data']['ApiSubmitedResult'] = $FollowUpResults;
        $this->ajaxResults['script'] = $AddToScript;
        $this->ajaxResults['thankyou'] = $ThankYouText;
        $this->ajaxResults['thankyouhtml'] = $ThankYouPage;
        $this->ajaxResults['thankyouURL'] = $ThankYouPageURL;
        return $this->ajaxResults;
    }
    public function curlMe($action, $postdata = ""){
        $ckfile = tempnam("/tmp", "infusionsoft");
        $ch = curl_init($action);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $ckfile);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        if($postdata !== "") curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        $HTML = curl_exec($ch);
        curl_close($ch);
        return $HTML;
    }
    public function getPHPCache($Data){
        header('Content-Type: application/json; charset=UTF-8');
        $CacheName = $Data['CacheName'];
        if($CacheName == 'infusionsoft_get_custom_fields'){
            echo json_encode(infusionsoft_get_custom_fields('%', false, -1));
        }
        if($CacheName == 'infusionsoft_contact_custom_fields'){
            echo json_encode(infusionsoft_contact_custom_fields());
        }
    }
}
