<?php
/*production config*/
$protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
$config['base_url'] = $protocol . $_SERVER['HTTP_HOST'] . "/";
$config['CACHE_DIR'] = "/var/www/macantacrm/shared/cache/";
$config['DAEMON_SERVICE_NAME'] =  "consumer";
$config['DAEMON_SERVICE_VERBOSE'] =  false;
/*=========================*/

/*   Intercom Credentials  */
//$config['INTERCOM_APP'] =  $_SERVER['INTERCOM_APP'];
//$config['INTERCOM_SECRET'] =  $_SERVER['INTERCOM_SECRET'];
/*=========================*/

/*   Upscope Credentials  */
if(isset($_SERVER['UPSCOPE_APP'])) $config['UPSCOPE_APP'] =  $_SERVER['UPSCOPE_APP'];
/*=========================*/

/*   B2 Credentials  */
$config['FILEBOX'] = [
    "B2_APP_ID" => $_SERVER['B2_APP_ID_FILEBOX'],
    "B2_APP_KEY" => $_SERVER['B2_APP_KEY_FILEBOX'],
    "B2_BUCKET_ID" => $_SERVER['B2_BUCKET_ID_FILEBOX'],
    "B2_BUCKET_NAME" => $_SERVER['B2_BUCKET_NAME_FILEBOX'],
    "B2_BUCKET_TYPE" => "PRIVATE"
];
$config['CD_ATTACHMENT'] = [
    "B2_APP_ID" => $_SERVER['B2_APP_ID_CD_ATTACHMENT'],
    "B2_APP_KEY" => $_SERVER['B2_APP_KEY_CD_ATTACHMENT'],
    "B2_BUCKET_ID" => $_SERVER['B2_BUCKET_ID_CD_ATTACHMENT'],
    "B2_BUCKET_NAME" => $_SERVER['B2_BUCKET_NAME_CD_ATTACHMENT'],
    "B2_BUCKET_TYPE" => "PUBLIC"
];