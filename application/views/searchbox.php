<div class="text">
    <!--<a  class="btn btn-success logout" type="button" onclick="logout(); return false;">Logout</a>-->

    <div class="main-search-bar input-group flex-row">
        <div class="flex-row search-main">
            <select id="MainStrSearch" class="dropdown-select search_str_options" title="Type Your Search:">
                <optgroup label="Search Options">
                    <option value="default:" selected>by Name</option>
                    <option value="email:">by Email</option>
                    <option value="company:">by Company</option>
                    <option value="address:">by Address</option>
                    <option value="connected:">by Connected Data</option>
                </optgroup>
            </select>
            <div class="input-add-btn-container flex-row">
                <input type="text" class="searchKey  MainSearch" id="searchKey"
                       placeholder="<?php echo $this->lang->line('text_contact_search'); ?>..."
                       data-source="MainSearch">
                <button class="nb-btn nb-primary nb-btn-icon-only addContact" type="button"><i class="fa fa-user-plus"
                                                                                               aria-hidden="true"
                                                                                               title="Add Contact"></i>
                </button>
            </div><!-- /.input-add-btn-container -->
        </div><!-- /.search-main -->
        <div class="flex-row search-tag">
            <input type="text" class=" searchTag  MainSearch" id="searchTag" placeholder="or search by note #Tag..."
                   data-source="MainSearch">
            <button class="nb-btn nb-primary icon-btn search" type="button" data-source="MainSearch"><i
                        class="fa fa-search" aria-hidden="true"></i>
                <?php echo $this->lang->line('text_go'); ?><!--Go-->!
            </button>
        </div><!-- /.search-tag -->

    </div>
    <div class="input-group search_filter_bar">
        <select id="MainSavedSearch" class="search_filter dropdown-select" title="Saved Search Filter">
            <?php
            $MacantaUsers = macanta_get_users();
            $CurrentMacantaUser = macanta_get_user_access_by_id($InfusionsoftID);
            $CurrentMacantaUserLevel = $CurrentMacantaUser['Level'];
            $CurrentMacantaUserAccess = $CurrentMacantaUser['Access'];
            $UserMQBAccess = json_decode($CurrentMacantaUserAccess->MQBAccess, true);
            function sort_obj($a, $b)
            {
                return strcmp($a->FilterName, $b->FilterName);
            }

            $selected_searched_cache = manual_cache_loader('selected_searched_cache' . $InfusionsoftID);
            $Filters = infusionsoft_get_all_saved_search();
            $Filters = sortMultiArray($Filters, 'ReportStoredName');
            $FilterGroups = array();
            $TaskSearch = false;
            $MacantaTaskSearch = false;
            $MacantaQueryStatus = false;
            if (sizeof($Filters) > 0){
            foreach ($Filters as $Filter) {
                $FilterGroups[$Filter->ReportStoredName][] = $Filter;
            }
            foreach ($FilterGroups

            as $SavedFilterGroup => $SavedFilters){
            echo '<optgroup label="' . $SavedFilterGroup . '">';
            if ($SavedFilterGroup == 'TaskSearch'){
            //echo '<option data-group="TaskSearch" data-columns="" value="'.$InfusionsoftID.':0"  '.$selected.'>Macanta Task</option>';
            /*foreach ($MacantaUsers as $MacantaUser) {
                $selected = '';
                $UsersCallerId[] = array();
                $value = $MacantaUser->Id.':0';
                if(str_replace('"',"", $selected_searched_cache) == $value ){
                    $selected = "selected";
                    $TaskSearch = true;
                }
                //$Contact->FirstName." ".$Contact->LastName." Task"
                echo '<option data-loggedinusergroups="'.$Groups.'" data-group="TaskSearch" data-columns="" title="'.$MacantaUser->Email.'" value="'.$MacantaUser->Id.':0"  '.$selected.'>'.$MacantaUser->FirstName.' '.$MacantaUser->LastName.'\'Task</option>';
                */
            ?><!--
                            --><?php
            /*                        }*/
            }
            usort($SavedFilters, "sort_obj");
            foreach ($SavedFilters as $SavedFilter) {
                $UserId = explode(',', $SavedFilter->UserId);
                //$needle = array('macanta_','macanta');
                //$FilterDisplay = str_replace($needle,'',$SavedFilter->FilterName);
                $FilterDisplay = $SavedFilter->FilterName;
                //if(stristr($SavedFilter->FilterName,'macanta') != false){
                $Columns = '';
                //$Columns = infusionsoft_get_saved_search_columns($SavedFilter->Id, $UserId[0]);
                if (hasSavedSearchTag($SavedFilter->Id, $Groups)) {
                    $value = $UserId[0] . ':' . $SavedFilter->Id;
                    $selected = str_replace('"', "", $selected_searched_cache) == $value ? "selected" : "";
                    if ($selected != "" && $SavedFilterGroup == 'TaskSearch') {
                        $TaskSearch = true;
                    }
                    echo '<option data-group="' . $SavedFilterGroup . '" data-columns="' . json_encode($Columns) . '" value="' . $value . '" ' . $selected . '>' . $FilterDisplay . '</option>';
                }


                //}
            }
            echo '</optgroup>';
            }
            } else {
                echo '<optgroup label="No saved search for macanta"></optgroup>';
            }

            ?>
            <optgroup label="MacantaTaskSearch">
                <?php
                $Users = [];
                foreach ($MacantaUsers as $MacantaUser) {
                    $selected = '';
                    $SelectedMacantaTask = '';
                    $UsersCallerId[] = array();
                    $value = $MacantaUser->Id . ':0';
                    if (str_replace('"', "", $selected_searched_cache) == $value) {
                        $selected = "selected";
                        $MacantaTaskSearch = true;
                        $SelectedMacantaTask = $value;
                    }
                    //$Contact->FirstName." ".$Contact->LastName." Task"
                    $PermisionTagInfo = infusionsoft_get_create_tag_by_name($MacantaUser->FirstName . " " . $MacantaUser->LastName . " Task", "savedsearch_permission_cat", "macanta task saved search permission");
                    $AvailableTagArr = explode(',', $Groups);

                    if ($CurrentMacantaUserLevel != 'administrator') {
                        if ($Email != $MacantaUser->Email) continue;
                    }
                    $Label = $MacantaUser->FirstName . ' ' . $MacantaUser->LastName . '\' Task <small> (' . $MacantaUser->Email . ') </small>';
                    if (in_array($MacantaUser->Id, $Users)) continue;
                    echo '<option data-tagid="' . $PermisionTagInfo->Id . '" data-tagname="' . $PermisionTagInfo->GroupName . '" data-loggedinusergroups="' . $Groups . '" data-group="MacantaTaskSearch" data-columns="" title="' . $MacantaUser->Email . '" value="' . $MacantaUser->Id . ':0"  ' . $selected . '>' . $Label . '</option>';
                    $Users[] = $MacantaUser->Id;

                    ?>
                    <?php
                }
                ?>
            </optgroup>
            <optgroup label="MacantaQuery">
                <?php
                $ConnectedDataType = '';
                $ConnectedInfoGroupFieldsMap = [];
                $MacantaQueries = macanta_get_cd_query('', true);
                $MQBReadOnlyScript = false;
                foreach ($MacantaQueries as $MacantaQuery) {
                    $MQBAccess = $UserMQBAccess[$MacantaQuery->queryId];
                    if (!empty($MQBAccess)) {
                        if ($MQBAccess == 'GlobalAccess') {
                            // Theres no current Global settings for this.
                        } else {

                        }
                    } else {
                        if ($CurrentMacantaUserLevel != 'administrator') continue;
                    }

                    if (str_replace('"', "", $selected_searched_cache) == $InfusionsoftID . ":" . $MacantaQuery->queryId) {
                        $selected = "selected";
                        $ConnectedDataType = $MacantaQuery->queryConnectedDataType;
                        $ConnectedInfoGroupFieldsMap = macanta_get_connected_info_group_fields_map($ConnectedDataType, '', true, false);
                    } else {
                        $selected = "";
                    }
                    $selected = str_replace('"', "", $selected_searched_cache) == $InfusionsoftID . ":" . $MacantaQuery->queryId ? "selected" : "";
                    if ($selected == "selected") {
                        $MacantaQueryStatus = true;
                        if ($MQBAccess == 'ReadOnly') {
                            $MQBReadOnlyScript = true;
                        }
                    }
                    echo '<option data-access="' . $MQBAccess . '"  data-tagid="' . $MacantaQuery->tagId . '" data-group="MacantaQuery" data-columns="" value="' . $InfusionsoftID . ":" . $MacantaQuery->queryId . '" ' . $selected . '>' . $MacantaQuery->queryName . '</option>';
                };
                ?>
            </optgroup>
            <!--<optgroup label="Picnic">
                <option>Mustard</option>
                <option>Ketchup</option>
                <option>Relish</option>
            </optgroup>
            <optgroup label="Camping">
                <option>Tent</option>
                <option>Flashlight</option>
                <option>Toilet Paper</option>
            </optgroup>-->

        </select>
    </div><!-- /.main-search-bar -->
</div><!-- /.text -->

<?php
$searched_cache = manual_cache_loader('searched_cache' . $InfusionsoftID, true);
$extraField = manual_cache_loader('search_xcolumn_cache' . $InfusionsoftID);
$extracomlumn = $extraField ? "<th>$extraField<!--Email--></th>" : "";
$searched_column_cache = manual_cache_loader('searched_column_cache' . $InfusionsoftID);
$connected_searched_cache = manual_cache_loader('connected_searched_cache' . $InfusionsoftID, true);
?>
<?php
if ($MacantaQueryStatus) { ?>
    <script>
        ConnectedInfoGroupFieldsMap = <?php echo json_encode($ConnectedInfoGroupFieldsMap); ?>;
    </script>
    <?php
}
?>

<div class="form-box <?php echo !$searched_cache ? "hideThis" : '' ?> ">
    <h5 class="login-message"></h5>
    <div class="search-results">
        <div class="panel panel-default ">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php echo $this->lang->line('text_contact_search_result'); ?><!--Contact Search Result--></h3>
                <?php
                if ($MacantaQueryStatus == true) { ?>
                    <a href="/ajax/mcbr/<?php echo $InfusionsoftID; ?>" target="_blank"
                       class="downloadConnectorCSV" title="Download CSV File">
                       <i class="csv-download-icon"></i>
                       <span>Download CSV File</span>
                    </a>
                <?php }
                if ($MacantaTaskSearch == true) { ?>
                    <a href="/ajax/mt/<?php echo $SelectedMacantaTask; ?>" target="_blank"
                       class="downloadConnectorCSV" title="Download CSV File">
                       <i class="csv-download-icon"></i>
                       <span>Download CSV File</span>
                    </a>
                <?php }
                ?>
            </div>
            <div class="panel-body">

                <table id="ContactsTable"
                       class="table table-striped table-bordered <?php echo $MacantaQueryStatus == true ? "MacantaQuery" : "" ?> <?php echo $TaskSearch == true ? "TaskSearch" : "" ?> <?php echo $MacantaTaskSearch == true ? "MacantaTaskSearch" : "" ?>"
                       cellspacing="0"
                       width="98%">
                    <thead>
                    <tr>
                        <?php
                        if ($searched_column_cache && $searched_column_cache != "[]") {
                            $searched_column_cache = json_decode($searched_column_cache);
                            if ($TaskSearch == true || $MacantaTaskSearch == true) echo "<th>Status</th>";
                            foreach ($searched_column_cache as $column => $status) {
                                $column = str_replace('opt.out.search.report.field.', '', $column);
                                if ($column != 'Id') echo "<th>$column</th>";
                            }
                        } else {
                            ?>
                            <th><?php echo $this->lang->line('text_name'); ?><!--Name--></th>
                            <th><?php echo $this->lang->line('text_company'); ?><!--Company--></th>
                            <th><?php echo $this->lang->line('text_email'); ?><!--Email--></th>
                            <?php echo $extracomlumn; ?>
                            <th><?php echo $this->lang->line('text_town_city'); ?><!--Email--></th>
                            <th><?php echo $this->lang->line('text_zip_postcode'); ?><!--Email--></th>
                            <?php
                        }
                        ?>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /input-group -->
</div>


<?php

if ($connected_searched_cache) {
    $relationships_map = macanta_get_connected_info_relationships_map();
    ?>
    <div class="form-box form-box-connected-data">
        <h5 class="login-message"></h5>
        <?php
        $UserConnectedInfo = json_decode($connected_searched_cache, true);
        $ConnectorTabsEncoded = macanta_get_config('connected_info');
        $ConnectorTabs = json_decode($ConnectorTabsEncoded, true);
        function get_connector_group_name_by_id($Id, $ConnectorTabs)
        {
            foreach ($ConnectorTabs as $key => $ConnectorTab) {
                if ($ConnectorTab['id'] == $Id) {
                    return $ConnectorTab['title'];
                }
            }
            return '';
        }

        $CSVDownLoadData = array();
        foreach ($UserConnectedInfo as $GroupKey => $tmpContactorKey) {
            foreach ($tmpContactorKey as $Key => $tampValue) {
                $CSVDownLoadData[$GroupKey][] = $Key;
            }
        }

        foreach ($UserConnectedInfo as $GUID => $Items) { ?>

            <div class="search-results search-results-connected-data">
                <div class="input-group">
                    <div class="panel panel-default ">
                        <div class="panel-heading">
                            <h3 class="panel-title">Data Found
                                In <?php echo get_connector_group_name_by_id($GUID, $ConnectorTabs) ?>
                            </h3>
                            <a href="javaScript:void(0)"
                               onclick="CSVFieldsSelectionFun('<?php echo $GUID; ?>','<?php echo base64_encode(json_encode($CSVDownLoadData[$GUID])); ?>')"
                               class="downloadConnectorCSV" title="Download CSV File">
                                   <i class="csv-download-icon"></i>
                                   <span>Download CSV File</span>
                            </a>
                        </div>
                        <div class="panel-body">

                            <table id="ConnectedDataTable" class="table table-striped table-bordered conencted-search"
                                   cellspacing="0"
                                   width="100%" style="width: 100%;">
                                <thead>
                                <tr>
                                    <?php
                                    $Order = [];
                                    $OrderId = [];
                                    $UserValue = [];
                                    $FieldIdNamePair = [];
                                    $FieldIdNamePairAll = [];
                                    foreach ($ConnectorTabs as $key => $ConnectorTab) {
                                        if ($ConnectorTab['id'] == $GUID) {
                                            $UserValue = isset($UserConnectedInfo[$ConnectorTab['id']]) ? $UserConnectedInfo[$ConnectorTab['id']] : [];
                                            foreach ($ConnectorTab['fields'] as $field) {
                                                if ($field["showInTable"] == "yes") {
                                                    $key = (int)$field["showOrder"];
                                                    if (isset($Order[$key])) {
                                                        while (isset($order[$key])) {
                                                            $key++;
                                                        }
                                                        $Order[$key] = $field["fieldLabel"];
                                                        $OrderId[$key] = $field["fieldId"];
                                                        $FieldIdNamePair[$field["fieldId"]] = $field["fieldLabel"];
                                                    } else {
                                                        $Order[$key] = $field["fieldLabel"];
                                                        $OrderId[$key] = $field["fieldId"];
                                                        $FieldIdNamePair[$field["fieldId"]] = $field["fieldLabel"];
                                                    }
                                                }
                                                $FieldIdNamePairAll[$field["fieldId"]] = $field["fieldLabel"];
                                            }
                                            ksort($Order);
                                            ksort($OrderId);

                                            if (sizeof($Order) > 0) {
                                                foreach ($Order as $field) {
                                                    echo "<th>$field</th>";
                                                }
                                            } else {
                                                $count = 0;
                                                foreach ($ConnectorTab['fields'] as $field) {
                                                    $count++;
                                                    $OrderId[] = $field["fieldId"];
                                                    $FieldIdNamePair[$field["fieldId"]] = $field["fieldLabel"];
                                                    echo "<th>$field[fieldLabel]</th>";
                                                    if ($count == 3) break;
                                                }
                                            }
                                        }
                                    }

                                    ?>
                                    <th>Other Details</th>
                                    <th>Connected Contacts</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php


                                $html = '';
                                $ContactArr = [];
                                foreach ($UserValue as $itemId => $ValuesArr) {
                                    $UserField = $ValuesArr['value'];
                                    $ConnectedContacts = $ValuesArr['connected_contact'];
                                    $ContactDisplay = '';
                                    foreach ($ConnectedContacts as $index => $ContactInfo) {
                                        $theRalation = [];
                                        foreach ($ContactInfo["relationships"] as $relationId) {
                                            $theRalation[] = ucfirst($relationships_map[$relationId]);
                                        }
                                        $ContactArr[$ContactInfo['ContactId']] = "$ContactInfo[FirstName] $ContactInfo[LastName]";
                                        $relationships = implode(',', $theRalation);
                                        $ContactDisplay .= "<span class='contacts-connected'><a href='#contact/" . $ContactInfo['ContactId'] . "'>$ContactInfo[FirstName] $ContactInfo[LastName] ($ContactInfo[Email]) <small>Relationship: $relationships</small></a></span>";
                                    }
                                    $OtherDetails = '';
                                    $OtherDetailsCount = 0;
                                    $MenuShown = false;
                                    $Hide = '';
                                    foreach ($UserField as $fieldId => $fieldValue) {
                                        if (!array_key_exists($fieldId, $FieldIdNamePair)) {
                                            if ($OtherDetailsCount == 2 && sizeof($UserField > 2) && $MenuShown == false) {
                                                $OtherDetails .= "<span class='other-details-show'><strong class='show-more'>Show More</strong></span>";
                                                $MenuShown = true;
                                                $Hide = 'other-details-hidden hide';
                                            }
                                            if (is_array($fieldValue)) {
                                                $temp = [];
                                                foreach ($fieldValue as $theContactId => $value) {
                                                    if (!$value) continue;
                                                    $theContactId = str_replace('id_', '', $theContactId);
                                                    $temp[] = '<span class="other-details"><small><strong>' . $ContactArr[$theContactId] . ":</strong> " . $value . '</small></span>';
                                                }
                                                $tempStr = implode(' ', $temp);
                                                $OtherDetails .= "<span class='other-details $Hide'><strong>$FieldIdNamePairAll[$fieldId]:</strong>$tempStr</span>";
                                            } else {
                                                $OtherDetails .= "<span class='other-details $Hide'><strong>$FieldIdNamePairAll[$fieldId]:</strong> $fieldValue</span>";
                                            }
                                            $OtherDetailsCount++;
                                        }
                                        //$OtherDetails .= "<span class='other-details'><strong>$FieldIdNamePair[$fieldId]</strong>: $fieldValue</span>";
                                    }
                                    $ConnectedContactsArr = json_decode($ConnectedContacts, true);
                                    $html .= "<tr class='connected-search-result-row ' data-itemid='$itemId'  data-connectedcontacts ='" . str_replace("=", "", base64_encode(json_encode($ConnectedContacts))) . "' data-raw ='" . str_replace("=", "", base64_encode(json_encode($UserField))) . "'>";
                                    $FirstColumn = true;
                                    foreach ($OrderId as $fieldName) {
                                        if (is_array($UserField[$fieldName])) {
                                            $temp = [];
                                            foreach ($UserField[$fieldName] as $theContactId => $value) {
                                                if (!$value) continue;
                                                $theContactId = str_replace('id_', '', $theContactId);
                                                $temp[] = '<span class="other-details"><small><strong>' . $ContactArr[$theContactId] . ":</strong> " . $value . '</small></span>';
                                            }
                                            $ColumnValue = implode(' ', $temp);
                                        } else {
                                            $ColumnValue = $UserField[$fieldName];
                                        }
                                        $Button = '';
                                        if($FirstColumn){
                                            $Button = '<div class="deleteDataObjectItemContainer"><button class=" nb-btn nb-primary icon-btn deleteDataObjectItem" data-id="'.$itemId.'" data-groupid="'. $ConnectorTab['id'] .'" data-type="search"><i class="fa fa-trash-o" aria-hidden="true"></i></button></div>';
                                        }
                                        $html .= "<td>$ColumnValue  $Button</td>";
                                        $FirstColumn = false;
                                    }
                                    $OtherDetails = trim($OtherDetails) == '' ? "- no other details -" : $OtherDetails;
                                    $html .= "<td>" . $OtherDetails . "</td>";
                                    $html .= "<td>" . $ContactDisplay . "</td>";
                                    $html .= "</tr>";
                                }
                                echo $html;
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>

        <?php }
        ?>
        <script>
            search_xcolumn_cache = "<?php echo $extraField ? $extraField : ""; ?>";
            var ConnectedDataTable = $(".conencted-search").DataTable({
                //var itemFootnote = '<small class="footnote">Contact Id: '+theContact.Id+'</small>'
                "pageLength": 10,
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "info": false,
                "createdRow": function (row, data, index) {
                }
            });
        </script>
        <!-- /input-group -->
    </div>
<?php }
unset($ConnectorTabsEncoded);
?>

<script>
    var search_xcolumn_cache = "<?php echo $extraField ? $extraField : ""; ?>";
    $('.other-details-show').on('click', function () {
        var parentTD = $(this).parents('td');
        var hidden = parentTD.find('.other-details-hidden');
        if (hidden.hasClass('hide')) {
            hidden.removeClass('hide');
        } else {
            hidden.addClass('hide');
        }
    });
</script>
<input type="hidden" id="CurrentConnectorId"/>
<input type="hidden" id="CurrentConnectorDownLoadType" value=""/>

<!-- Modal -->
<div id="CSVFieldsSelection" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="width: 600px;">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:16px;">Please select fields to download CSV</h4>
            </div>

            <div class="modal-body ConnectedInfoSettingsContainer" style="text-align:left;">
            </div>

        </div>

    </div>
</div>
<style>
    .ConnectedInfoSettingsContainer {
        text-align: left;
    }

    .ConnectedInfoSettingsContainer h3 {
        display: none;
    }

    span.other-details {
        -webkit-transition: width 2s; /* Safari */
        transition: width 2s;
    }

    span.other-details-show {
        width: 100%;
        float: left;
        padding: 0px 1px 0px 0px;
        border-top: 1px dashed #a7af98;
        cursor: pointer;
        background-color: #f1f1f1;
    }

    strong.show-more {
        float: right;
        color: #757d65;
        cursor: pointer;
        font-size: 11px;
    }
</style>

<!-- Modal -->
<div id="SendEmailModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Compose Email</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <div class="containerInlineEmail">
                    <div class="EmailFormPanelBody panel-body InlineEmail">
                        <input type="hidden" class="form-control toEmail" name="toEmail">
                        <div class="form-group ">
                            <label class="label-sm" for="subject">Subject:</label>
                            <input type="text" class="form-control subject" name="subject">
                        </div>
                        <div class="label-sm" class="form-group ">
                            <label for="subject">BCC:</label>
                            <input type="text" class="form-control bcc" name="bcc" value="">
                        </div>
                        <div class="form-group ">
                            <label class="label-sm" for="message">Message:</label>
                            <textarea id="InlineEmail" name="InlineEmail" class="form-control EmailForm TextEditor"
                                      placeholder="Text Here"></textarea>
                        </div>
                        <div class="form-group form-btn-container">
                            <button type="button" value="Send" class="nb-btn nb-primary"
                                    onclick="sendISInlineEmail('InlineEmail');">Send Email
                            </button>
                        </div>
                    </div>
                    <script>
                        initTinymceById("InlineEmail");
                    </script>
                </div>

            </div>

        </div>

    </div>
</div>
<?php
if ($MQBReadOnlyScript) {
    echo "
    <style>
    span.editInlineQueryContainer {
    display: none !important;
}
    </style>
    ";

}
/*$MediaPresentation = $this->config->item('MediaPresentation');
if(macanta_check_viewed_media_presentation($InfusionsoftID,$MediaPresentation) == false){
    if($MediaPresentation['Type'] == 'wistia'){
        echo '<script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js" async></script>';
        echo '<div class="wistia_embed wistia_async_'.$MediaPresentation['MediaId'].' popover=true autoPlay=true popoverBorderWidth=2 popoverShowOnLoad=true" style="width:30px;height:30px;position: fixed; opacity: 0;">&nbsp;</div>';
    }else{
        echo '<script>';
        echo '$("#MediaPresentation").modal("show");';
        echo '$("#MediaPresentation").on("hidden.bs.modal",function(){ $("#iframeYoutube").attr("src","#"); })';
        echo '</script>';
    }

}*/
?>