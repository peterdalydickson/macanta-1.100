<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 21/07/16
 * Time: 1:34 PM
 */
$lang['text_welcome_to_macanta'] = 'Welcome to macanta';
$lang['text_login'] = 'Login';
$lang['text_please_fill_in'] = 'Please fill in the form below';
$lang['text_email'] = 'Email';
$lang['text_password'] = 'Password';
$lang['text_fed_by'] = 'powered by';
$lang['text_powered_by'] = 'simplified by';
$lang['text_or'] = 'or';
$lang['text_thanks_for_visiting_google'] = '"Thanks for visiting macanta.
Unfortunately, the email address we have on record for you is different to the one just given to us by Google.
Please enter your email address and password on the left and click ""Log me in..."".
If you still have difficulty logging in, please contact your system administrator. Thanks.
    "';

$lang['text_oops_looks_like'] = '"Oops! Looks like there’s a slight issue our end with how your account is setup.
Please contact your system administrator. Thanks."';

$lang['text_thanks_for_visiting'] = '"Thanks for visiting macanta.
Unfortunately, the email address we have on record for you is different to the one you just entered.
Please check your email address and password, and click ""Log me in..."".
If you still have difficulty logging in, please contact your system administrator. Thanks.
    "';
$lang['text_loading'] = 'Loading';
$lang['text_please_wait'] = 'Please wait';
$lang['text_contact_search'] = 'Contact Search';
$lang['text_contact_search_result'] = 'Contact Search Result';
$lang['text_show'] = 'Show';
$lang['text_entries'] = 'entries';
$lang['text_name'] = 'Name';
$lang['text_company'] = 'Company';
$lang['text_phone'] = 'Phone';
$lang['text_shipping'] = 'Shipping';
$lang['text_billing'] = 'Billing';
$lang['text_show_shipping_instead'] = 'show shipping instead';
$lang['text_show_billing_instead'] = 'show billing instead';
$lang['text_no_data_available'] = 'No data available in table';
$lang['text_previous'] = 'Previous';
$lang['text_next'] = 'Next';
$lang['text_go'] = 'Go';
$lang['text_address'] = 'Address';
$lang['text_search_for'] = 'Search for';
$lang['text_view_recent_results'] = 'Back to search page';
$lang['text_total_invoiced'] = 'Invoiced';
$lang['text_amount_owed'] = 'Owed';
$lang['text_call'] = 'Call';
$lang['text_note'] = 'Note';
$lang['text_appointment'] = 'Appointment';
$lang['text_task'] = 'Task';
$lang['text_triggers'] = 'Triggers';
$lang['text_ready'] = 'Ready';
$lang['text_edit_contact_details'] = 'Edit Contact Details';
$lang['text_status'] = 'Status';
$lang['text_error'] = 'Error';
$lang['text_start_phone_call'] = 'Start Phone Call';
$lang['text_end_phone_call'] = 'End Phone Call';
$lang['text_previous_calls'] = 'Previous Calls';
$lang['text_current_call'] = 'Current Call';
$lang['text_edit_note'] = 'Edit Note';
$lang['text_edit'] = 'Edit';
$lang['text_no_call_record'] = 'No Call record';
$lang['text_call_recording'] = 'Call recording';
$lang['text_no_tags'] = 'no tags';
$lang['text_add_note_tag'] = 'add note tag';
$lang['text_create_note'] = 'Create note';
$lang['text_call_note'] = 'Call Note';
$lang['text_contact_notes'] = 'Contact Notes';
$lang['text_search_and_filter'] = 'Search and Filter Notes';
$lang['text_search_note_content'] = 'Search Note Content';
$lang['text_add_tag_filter'] = 'add tag filter';
$lang['text_choose_staff_member'] = 'Choose staff member';
$lang['text_quick_note'] = 'Add Note';
$lang['text_by_date'] = 'By Date';
$lang['text_by_note_tag'] = 'By Note #Tag';
$lang['text_by_staff_member'] = 'By Staff Member';
$lang['text_hidden_from_client'] = 'Hidden From Client?';
$lang['text_triggers'] = 'Triggers';
$lang['text_apply_triggers'] = 'Apply Triggers';
$lang['text_address1'] = 'Address1';
$lang['text_address2'] = 'Address2';
$lang['text_town_city'] = 'Town/City';
$lang['text_county_state'] = 'County/State';
$lang['text_zip_postcode'] = 'Zip/PostCode';
$lang['text_country'] = 'Country';
$lang['text_firstname'] = 'FirstName';
$lang['text_lastname'] = 'LastName';
$lang['text_streetaddress1'] = 'StreetAddress1';
$lang['text_streetaddress2'] = 'StreetAddress2';
$lang['text_close'] = 'Close';
$lang['text_save'] = 'Save';
$lang['text_contact_details'] = 'Contact Details';
$lang['text_add_contact'] = 'Add Contact';
$lang['text_save_notes'] = 'Save Notes';
$lang['text_login_successful'] = 'Login Successful';
$lang['text_log_me_in'] = 'Log me in';
$lang['text_logout'] = 'Logout';
$lang['text_type_your_note_here'] = 'Type your notes here';
$lang['text_dial_number'] = 'Click here to manually enter a phone number';
$lang['text_valid'] = 'Valid';
$lang['text_invalid_number'] = 'Invalid number';
$lang['text_call_phone'] = 'Phone';
$lang['text_create_note'] = 'Create note';
$lang['text_please_wait_loading'] = 'Please wait. loading';
$lang['text_words'] = 'Words';
$lang['text_legacy_notes'] = 'Legacy notes';
$lang['text_legacy'] = 'Legacy';
$lang['text_cancel'] = 'Cancel';
$lang['text_tour_login'] = 'Login with an email address from your Infusionsoft app that has a password and the correct macanta user tag';
$lang['text_tour_search'] = 'Enter a first name, last name or email address and click GO';
$lang['text_tour_click_search_result'] = 'Click a search result to open the contact in macanta';
$lang['text_tour_edit_contact'] = 'Click here to edit contact details';
$lang['text_tour_switch_address'] = 'Use this to switch between BILLING and SHIPPING addresses';
$lang['text_tour_linked_contact'] = 'Click here to open the contact in Infusionsoft';
$lang['text_tour_start_call'] = 'Click here to start a phone call, and then click \'Call Phone 1\' or \'Call Phone 2\'';
$lang['text_tour_enter_notes'] = 'Enter notes here, for both inbound and outbound calls';
$lang['text_tour_create_notes'] = 'You must click \'Create Note\' on outbound calls to save the call recording';
$lang['text_current_action_plan'] = 'Current Action Plan';
$lang['text_sbsm'] = 'SBSM';
$lang['text_no_contact_id'] = "Oops! This saved search doesn't contain a contact id, so I don't know which contact to display!\\n\\rPlease add the contact id to the columns of the saved search in Infusionsoft, save it and re-select the saved search on this page.\\n\\rThanks.";