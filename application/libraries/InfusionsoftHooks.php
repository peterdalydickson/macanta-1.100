<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class InfusionsoftHooks
{
    public $CI;
    public $AvailableHooks = [
        "appointment.add",
        "appointment.delete",
        "appointment.edit",
        "company.add",
        "company.delete",
        "company.edit",
        "contact.add",
        "contact.delete",
        "contact.edit",
        "contact.redact",
        "contactGroup.add",
        "contactGroup.applied",
        "contactGroup.delete",
        "contactGroup.edit",
        "contactGroup.removed",
        "invoice.add",
        "invoice.delete",
        "invoice.edit",
        "invoice.payment.add",
        "invoice.payment.delete",
        "invoice.payment.edit",
        "leadsource.add",
        "leadsource.delete",
        "leadsource.edit",
        "note.add",
        "note.delete",
        "note.edit",
        "opportunity.add",
        "opportunity.delete",
        "opportunity.edit",
        "opportunity.stage_move",
        "order.add",
        "order.delete",
        "order.edit",
        "product.add",
        "product.delete",
        "product.edit",
        "subscription.add",
        "subscription.delete",
        "subscription.edit",
        "task.add",
        "task.delete",
        "task.edit",
        "user.activate",
        "user.add",
        "user.edit"
    ];
    public $TableMap = [
        "Company" => "InfusionsoftCompany",
        "Contact" => "InfusionsoftContact",
        "ContactAction" => "InfusionsoftContactAction",
        "ContactGroupAssign" => "InfusionsoftContactGroupAssign",
        "ContactGroup" => "InfusionsoftContactGroup",
        "ContactGroupCategory" => "InfusionsoftContactGroupCategory",
        "DataFormField" => "InfusionsoftDataFormField",
        "DataFormGroup" => "InfusionsoftDataFormGroup",
        "DataFormTab" => "InfusionsoftDataFormTab",
        "EmailAddStatus" => "InfusionsoftEmailAddStatus",
        "FileBox" => "InfusionsoftFileBox",
        "Invoice" => "InfusionsoftInvoice",
        "Lead" => "InfusionsoftLead",
        "LeadSource" => "InfusionsoftLeadSource",
        "SavedFilter" => "InfusionsoftSavedFilter",
        "Stage" => "InfusionsoftStage",
        "Template" => "InfusionsoftTemplate",
        "User" => "InfusionsoftUser",
        "StageMove" => "InfusionsoftStageMove"


    ];
    public $FormIdsArr = [
        'Contact' => -1,
        'Opportunity' => -4,
        'Company' => -6,
        'ContactAction' => -5
    ];
    public function __construct($CI)
    {
        $this->CI = $CI;
    }

    public function contact_delete($data){
        //{"event_key":"contact.delete","object_type":"contact","object_keys":[{"apiUrl":"","id":2611,"timestamp":"2018-11-09T04:34:38Z"}],"api_url":""}
        //return $this->del_action($data,'Contact');
        return $this->del_action($data,'Contact');
    }
    public function contact_edit($data){
        //{"event_key":"contact.edit","object_type":"contact","object_keys":[{"apiUrl":"","id":455,"timestamp":"2018-11-09T04:29:59Z"}],"api_url":""}
        sleep(5);
        return $this->do_action($data,'Contact', 'Edit');
    }
    public function contact_add($data){
        //{"event_key":"contact.add","object_type":"contact","object_keys":[{"apiUrl":"","id":4445,"timestamp":"2018-11-09T04:38:49Z"}],"api_url":""}
        return $this->do_action($data,'Contact','Add');
    }
    public function contact_redact($data){}
    public function company_add($data){
        return $this->do_action($data,'Company');
    }
    public function company_delete($data){
        return $this->del_action($data,'Company');
    }
    public function company_edit($data){
        sleep(5);
        return $this->do_action($data,'Company');
    }
    public function contactGroup_add($data){
        return $this->do_action($data,'ContactGroup');
    }
    public function contactGroup_delete($data){
        return $this->del_action($data,'ContactGroup');
    }
    public function contactGroup_applied($data){
        $data['object_keys'][0]['id'] = $data['object_keys'][0]['contact_details'][0]['id'];
        sleep(5);
        $return = $this->do_action($data,'Contact');
        infusionsoft_get_tags_applied($data['object_keys'][0]['id'], true);
        return $return;
    }
    public function contactGroup_edit($data){
        return $this->do_action($data,'ContactGroup');
    }
    public function contactGroup_removed($data){
        $data['object_keys'][0]['id'] = $data['object_keys'][0]['contact_details'][0]['id'];
        sleep(5);
        $return = $this->do_action($data,'Contact');
        infusionsoft_get_tags_applied($data['object_keys'][0]['id'], true);
        return $return;
    }
    public function note_add($data){
        return $this->do_action($data,'ContactAction');
    }
    public function note_delete($data){
        return $this->del_action($data,'ContactAction');
    }
    public function note_edit($data){
        sleep(5);
        return $this->do_action($data,'ContactAction');
    }
    public function task_add($data){
        return $this->do_action($data,'ContactAction');
    }
    public function task_delete($data){
        return $this->del_action($data,'ContactAction');
    }
    public function task_edit($data){
        sleep(5);
        return $this->do_action($data,'ContactAction');
    }
    public function appointment_add($data){
        return $this->do_action($data,'ContactAction');
    }
    public function appointment_delete($data){
        return $this->del_action($data,'ContactAction');
    }
    public function appointment_edit($data){
        sleep(5);
        return $this->do_action($data,'ContactAction');
    }
    public function opportunity_add($data){
        return $this->do_action($data,'Lead');
    }
    public function opportunity_delete($data){
        return $this->del_action($data,'Lead');
    }
    public function opportunity_edit($data){
        return $this->do_action($data,'Lead');
    }
    public function opportunity_stage_move($data){

        return $this->do_action($data,'StageMove');
    }
    public function leadsource_add($data){
        return $this->do_action($data,'LeadSource');
    }
    public function leadsource_delete($data){
        return $this->del_action($data,'LeadSource');
    }
    public function leadsource_edit($data){
        return $this->do_action($data,'LeadSource');
    }

    public function invoice_add($data){
        return $this->do_action($data,'Invoice');
    }
    public function invoice_delete($data){
        return $this->del_action($data,'Invoice');
    }
    public function invoice_edit($data){
        return $this->do_action($data,'Invoice');
    }
    public function invoice_payment_add($data){}
    public function invoice_payment_delete($data){}
    public function invoice_payment_edit($data){}
    public function user_activate($data){}
    public function user_add($data){
        return $this->do_action($data,'User');
    }
    public function user_edit($data){
        return $this->do_action($data,'User');
    }
    public function do_action($data,$table, $Action = 'Add'){
        $result = [];
        foreach ($data['object_keys'] as $object_key){
            $Id = $object_key['id'];
            $Record = $this->_get_infusionsoft_record($Id,$table);
            if($table == 'Contact'){
                $this->_HookContactCallback($Action,$Id,$Record);
                if($Action == 'Add'){
                    $TagAppliedOnContactCreate = $this->CI->config->item('tag_applied_on_contact_create');
                    if($TagAppliedOnContactCreate && !empty($TagAppliedOnContactCreate)){
                        $TagAppliedOnContactCreateArr = explode(',',$TagAppliedOnContactCreate);
                        foreach($TagAppliedOnContactCreateArr as $Tag){
                            $Tag = trim($Tag);
                            if(is_numeric($Tag)){
                                infusionsoft_apply_tag($Id, $Tag);
                            }
                        }


                    }
                }
            }
            if(isset($Record[0]->Id)){
                $result[] = $this->_update_create_local_record($table,$Record[0]);
            }
        }
        return ['record'=>$Record,'result'=>$result, 'data'=> $data];
    }
    public function del_action($data,$Table){
        if($Table == 'Contact'){
            foreach ($data['object_keys'] as $object_key){
                $Id = $object_key['id'];

                $this->_HookContactCallback('Delete',$Id);
                if($this->_ifMergedContact($Id) || $this->CI->config->item('ArchiveContacts') == null){
                    $this->CI->db->where('Id', $Id);
                    $result = $this->CI->db->delete($this->TableMap[$Table]);
                }
                else{
                    if($this->CI->config->item('ArchiveContacts') == 'no' || $this->CI->config->item('ArchiveContacts') == '0'  || $this->CI->config->item('ArchiveContacts') == 'false'){
                        $this->CI->db->where('Id', $Id);
                        $result = $this->CI->db->delete($this->TableMap[$Table]);
                    }else{
                        $this->CI->db->where('user_id',$Id);
                        $this->CI->db->where('meta_key','deleted');
                        $query = $this->CI->db->get('users_meta');
                        $row = $query->row();
                        if (!isset($row))
                        {
                            $Record = [
                                'user_id' => $Id,
                                'meta_key' => 'deleted'
                            ];
                            $result = $this->CI->db->insert('users_meta', $Record);
                        }
                    }

                }
            }
        }else{
            foreach ($data['object_keys'] as $object_key) {
                $Id = $object_key['id'];
                $this->CI->db->where('Id', $Id);
                $result = $this->CI->db->delete($this->TableMap[$Table]);
            }
        }
        return ['record'=>$Id,'result'=>$result];
    }
    public function _get_infusionsoft_fields($Table, $Type = 'string'){
        $FormIdsArr = [
            'Contact' => -1,
            'Opportunity' => -4,
            'Lead' => -4,
            'Company' => -6,
            'ContactAction' => -5
        ];

        $TableFields = $this->CI->db->list_fields($this->TableMap[$Table]);

        // Remove non infusionsoft fields
        $theKey = array_search('Origin', $TableFields);
        if ($theKey !== false) unset($TableFields[$theKey]);

        $theKey = array_search('FileData', $TableFields);
        if ($theKey !== false) unset($TableFields[$theKey]);

        $theKey = array_search('CustomField', $TableFields);
        if ($theKey !== false) unset($TableFields[$theKey]);

        $theKey = array_search('PieceContent', $TableFields);
        if ($theKey !== false) unset($TableFields[$theKey]);

        $theKey = array_search('IdLocal', $TableFields);
        if ($theKey !== false) unset($TableFields[$theKey]);

        if (array_key_exists($Table, $FormIdsArr)) {
            //Lets Add Custom Fields
            $CFObjs = infusionsoft_get_custom_fields("%", true, $FormIdsArr[$Table]);
            foreach ($CFObjs as $CFObj) {
                $TableFields[] = '_' . $CFObj->Name;
            }
        }
        if($Type == 'string'){
            $returnFields = [];
            foreach ($TableFields as $returnField) {
                $returnFields[] = '"' . $returnField . '"';
            }
            $returnFieldsStr = implode(', ', $returnFields);
            return $returnFieldsStr;
        }
        else{
            return $TableFields;
        }


    }
    public function _get_infusionsoft_record($Id,$Table, $page = 0)
    {
        $action = "query_is";
        $query = '{"Id":"'.$Id.'"}';
        if ($Table == 'Template') {
            $query = '{"Id":"'.$Id.'","PieceTitle":"~<>~null","Categories":"%"}';
        }
        if ($Table == 'FileBox') {
            $query = '{"Id":"'.$Id.'","ContactId":"~<>~0"}';
        }
        if ($Table == 'ContactGroupAssign') {
            $query = '{"GroupId":"'.$Id.'"}';
            $this->CI->db->order_by('DateCreated', 'DESC');
            $this->CI->db->limit(1);
            $GroupAssign = $this->CI->db->get($this->TableMap[$Table])->row();
            if ($GroupAssign) {
                $LastDateCreatedJson = $GroupAssign->DateCreated;
                $LastDateCreated = json_decode($LastDateCreatedJson);
                $query = '{"DateCreated":"~>~ '.$LastDateCreated->date.'"}';
            }
        }
        $returnFieldsStr =  $this->_get_infusionsoft_fields($Table);
        $action_details = '{"table":"' . $Table . '","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":' . $query . '}';
        //file_put_contents(dirname(__FILE__)."/log_".$this->CI->config->item('MacantaAppName').".txt",$action_details);
        $temp = applyFn('rucksack_request', $action, $action_details, false);
        $Records = isset($temp->message) ? $temp->message:[];
        return $Records;
    }
    public function _update_create_local_record($Table,$Record){
        $CustomFields = [];
        foreach ($Record as $FieldName => $FieldValue) {
            if ($FieldName[0] == "_") {
                $CustomFields[$FieldName] = html_entity_decode($FieldValue);
                $CustomFields[$FieldName] = isJson($CustomFields[$FieldName]) ? json_decode($CustomFields[$FieldName]) : $CustomFields[$FieldName];
                unset($Record->$FieldName);
            }
            if(is_array($Record->$FieldName) || is_object($Record->$FieldName)) $Record->$FieldName = json_encode($Record->$FieldName);

        }
        $returnFieldsArr =  $this->_get_infusionsoft_fields($Table,'array');

        foreach ($returnFieldsArr as $returnField){
            if(!isset($Record->$returnField) && $returnField[0] != "_") $Record->$returnField = '';
        }
        // make sure table has custom field column
        $TableFields = $this->CI->db->list_fields($this->TableMap[$Table]);
        if (in_array('CustomField', $TableFields)) $Record->CustomField = json_encode($CustomFields);
        if (in_array('IdLocal', $TableFields)) $Record->IdLocal = $Record->Id;
        // For Contact Table
        if($Table == 'Contact'){
            unMarkDeletedContact($Record->Id);
            if(!empty($Record->Email)){
                $Local = macanta_db_record_exist('Email',$Record->Email,$this->TableMap[$Table], true);
                if($Local){
                    if($Local->Id == $Record->Id){
                        $this->CI->db->where('Id', $Record->Id);
                        $this->CI->db->or_where('Email', $Record->Email);
                        $result = $this->CI->db->update($this->TableMap[$Table], $Record);
                        $SQLLOG = $this->CI->db->last_query();
                        return ['action' => 'update','status' => $result,'sql_log'=>$SQLLOG];
                    }
                    else{
                        // Update Local Ids with the new one
                        $IdLocal = $Local->Id;
                        $Id = $Record->Id;
                        $DBData['Id'] = $Id;
                        $DBData['IdLocal'] = $Id;
                        $this->CI->db->where('IdLocal', $IdLocal);
                        $result = $this->CI->db->update("InfusionsoftContact", $Record);
                        $SQLLOG = $this->CI->db->last_query();

                        $DBData = ['ContactId' => $Id];
                        $this->CI->db->where('ContactId', $IdLocal);
                        $this->CI->db->update("InfusionsoftContactAction", $DBData);

                        $this->CI->db->where('ContactId', $IdLocal);
                        $this->CI->db->update("InfusionsoftContactGroupAssign", $DBData);

                        $this->CI->db->where('ContactId', $IdLocal);
                        $this->CI->db->update("InfusionsoftFileBox", $DBData);

                        $DBData = ['ContactID' => $Id];
                        $this->CI->db->where('ContactID', $IdLocal);
                        $this->CI->db->update("InfusionsoftLead", $DBData);

                        $DBData = ['contact_id' => $Id];
                        $this->CI->db->where('contact_id', $IdLocal);
                        $this->CI->db->update("note_tags", $DBData);

                        $DBData = ['user_id' => $Id];
                        $this->CI->db->where('user_id', $IdLocal);
                        $this->CI->db->update("users_meta", $DBData);

                        $DBData = ['infusionsoft_id' => $Id];
                        $this->CI->db->where('infusionsoft_id', $IdLocal);
                        $this->CI->db->update("user_sessions", $DBData);
                        return ['action' => 'update','status' => $result,'sql_log'=>$SQLLOG];
                    }
                }else{
                    if(macanta_db_record_exist('Id',$Record->Id,$this->TableMap[$Table])){
                        $this->CI->db->where('Id', $Record->Id);
                        $result = $this->CI->db->update($this->TableMap[$Table], $Record);
                        $SQLLOG = $this->CI->db->last_query();
                        return ['action' => 'update','result' => $result,'sql_log'=>$SQLLOG];
                    }else{
                        $result = $this->CI->db->insert($this->TableMap[$Table], $Record);
                        $SQLLOG = $this->CI->db->last_query();
                        return ['action' => 'insert','status' => $result,'sql_log'=>$SQLLOG];
                    }
                }

            }else{
                if(macanta_db_record_exist('Id',$Record->Id,$this->TableMap[$Table])){
                    $this->CI->db->where('Id', $Record->Id);
                    $result = $this->CI->db->update($this->TableMap[$Table], $Record);
                    $SQLLOG = $this->CI->db->last_query();
                    return ['action' => 'update','result' => $result,'sql_log'=>$SQLLOG];
                }else{
                    $result = $this->CI->db->insert($this->TableMap[$Table], $Record);
                    $SQLLOG = $this->CI->db->last_query();
                    return ['action' => 'insert','status' => $result,'sql_log'=>$SQLLOG];
                }
            }
        }
        else{
            if(macanta_db_record_exist('Id',$Record->Id,$this->TableMap[$Table])){
                if (isset($Record->CreationDate)) unset($Record->CreationDate);

                if($Table == 'ContactAction'){
                    unset($Record->CreatedBy);
                    if($Record->LastUpdatedBy == -1)
                        unset($Record->LastUpdatedBy);

                    $this->CI->db->where('Id', $Record->Id);
                    $query = $this->CI->db->get($this->TableMap[$Table]);
                    $row = $query->row();
                    if($row->CustomField != ''){
                        $OldCustomFields = json_decode($row->CustomField, true);
                        $NewCustomFields = array_merge($OldCustomFields,$CustomFields);
                        $Record->CustomField = $NewCustomFields;
                    }
                    //check if the CreationNotes has hash tag `CreationNotes`
                    if(isset($Record->CreationNotes)){
                        $tag_data = [];
                        $HashTags = macanta_get_hashtags($Record->CreationNotes);
                        if(sizeof($HashTags) > 0){
                            $tag_data['note_id'] = $Record->Id;
                            $tag_data['tag_slugs'] = implode(',', $HashTags);
                            $tag_data['contact_id'] = $Record->ContactId;
                            $tag_data_existing = macanta_db_record_exist(['note_id','contact_id'],[$Record->Id,$Record->ContactId],'note_tags', true);
                            if($tag_data_existing != false){
                                $HashTagsOld = explode(',',$tag_data_existing->tag_slugs);
                                $HashTagsNew = array_merge($HashTags,$HashTagsOld);
                                $tag_data['tag_slugs'] = implode(',', $HashTagsNew);
                                $this->CI->db->where('id', $tag_data_existing->id);
                                $this->CI->db->update('note_tags', $tag_data);
                            }else{
                                $this->CI->db->insert('note_tags', $tag_data);
                            }
                            foreach ($HashTags as $tag) {
                                $tagSlug = strtolower($tag);
                                $tagName = $tag;
                                $tagData = macanta_db_record_exist('tag_slug',$tag,'tags');
                                if ($tagData == false) {
                                    /*Save Unique Tag To DB*/
                                    $DBdata = array(
                                        'tag_slug' => $tagSlug,
                                        'tag_name' => $tagName
                                    );
                                    $this->CI->db->insert('tags', $DBdata);
                                }
                            }
                        }
                    }
                }
                if($Table == 'Lead'){
                    $this->CI->db->where('Id', $Record->Id);
                    $query = $this->CI->db->get($this->TableMap[$Table]);
                    $row = $query->row();
                    if($row->CustomField != ''){
                        $OldCustomFields = json_decode($row->CustomField, true);
                        $NewCustomFields = array_merge($OldCustomFields,$CustomFields);
                        $Record->CustomField = $NewCustomFields;
                    }
                }
                $this->CI->db->where('Id', $Record->Id);
                $result = $this->CI->db->update($this->TableMap[$Table], $Record);
                $SQLLOG = $this->CI->db->last_query();
                return ['action' => 'update','result' => $result,'sql_log'=>$SQLLOG];
            }else{
                $result = $this->CI->db->insert($this->TableMap[$Table], $Record);
                //check if the CreationNotes has hash tag `CreationNotes`
                if(isset($Record->CreationNotes)){
                    $tag_data = [];
                    $HashTags = macanta_get_hashtags($Record->CreationNotes);
                    if(sizeof($HashTags) > 0){
                        $tag_data['note_id'] = $Record->Id;
                        $tag_data['tag_slugs'] = implode(',', $HashTags);
                        $tag_data['contact_id'] = $Record->ContactId;
                        $this->CI->db->insert('note_tags', $tag_data);
                        foreach ($HashTags as $tag) {
                            $tagSlug = strtolower($tag);
                            $tagName = $tag;
                            $tagData = macanta_db_record_exist('tag_slug',$tag,'tags');
                            if ($tagData == false) {
                                /*Save Unique Tag To DB*/
                                $DBdata = array(
                                    'tag_slug' => $tagSlug,
                                    'tag_name' => $tagName
                                );
                                $this->CI->db->insert('tags', $DBdata);
                            }
                        }
                    }
                }
                $SQLLOG = $this->CI->db->last_query();
                return ['action' => 'insert','status' => $result,'sql_log'=>$SQLLOG];
            }
        }

    }
    public function _ifMergedContact($Id){
        //Checking For Merge Contact
        $this->CI->db->where('Id',$Id);
        $query = $this->CI->db->get('InfusionsoftContact');
        $DeletedContact = $query->row();
        if (isset($DeletedContact)){
            $Email = $DeletedContact->Email;
            $FirstName = $DeletedContact->FirstName;
            $LastName = $DeletedContact->LastName;
            $Company = $DeletedContact->Company;
            $StreetAddress1 = $DeletedContact->StreetAddress1;
            $Fax1 = $DeletedContact->Fax1;
            $Phone1 = $DeletedContact->Phone1;
            //Stage 1 A
            if(!empty($FirstName) && !empty($LastName) && !empty($Company)){
                $this->CI->db->where('FirstName',$FirstName);
                $this->CI->db->where('LastName',$LastName);
                $this->CI->db->where('Company',$Company);
                $query = $this->CI->db->get('InfusionsoftContact');
                if (sizeof($query->result()) > 1){
                    return true;
                }
            }
            //Stage 1 B
            if(!empty($FirstName) && !empty($Email)){
                $this->CI->db->where('FirstName',$FirstName);
                $this->CI->db->where('Email',$Email);
                $query = $this->CI->db->get('InfusionsoftContact');
                if (sizeof($query->result()) > 1){
                    return true;
                }
            }
            //Stage 1 C
            if(!empty($LastName) && !empty($Email)){
                $this->CI->db->where('LastName',$LastName);
                $this->CI->db->where('Email',$Email);
                $query = $this->CI->db->get('InfusionsoftContact');
                if (sizeof($query->result()) > 1){
                    return true;
                }
            }
            //Stage 1 D
            if(!empty($FirstName) && !empty($LastName) && !empty($StreetAddress1)){
                $this->CI->db->where('FirstName',$FirstName);
                $this->CI->db->where('LastName',$LastName);
                $this->CI->db->where('StreetAddress1',$StreetAddress1);
                $query = $this->CI->db->get('InfusionsoftContact');
                if (sizeof($query->result()) > 1){
                    return true;
                }
            }
            //Stage 1 E
            if(!empty($FirstName) && !empty($LastName) && !empty($Fax1)){
                $this->CI->db->where('FirstName',$FirstName);
                $this->CI->db->where('LastName',$LastName);
                $this->CI->db->where('Fax1',$Fax1);
                $query = $this->CI->db->get('InfusionsoftContact');
                if (sizeof($query->result()) > 1){
                    return true;
                }


            }
            //Stage 1 F
            if(!empty($FirstName) && !empty($LastName) && !empty($Phone1)){
                $this->CI->db->where('FirstName',$FirstName);
                $this->CI->db->where('LastName',$LastName);
                $this->CI->db->where('Phone1',$Phone1);
                $query = $this->CI->db->get('InfusionsoftContact');
                if (sizeof($query->result()) > 1){
                    return true;
                }
            }

            //Stage 2 A
            if(!empty($Email) && !empty($Fax1)){
                $this->CI->db->where('Email',$Email);
                $this->CI->db->where('Fax1',$Fax1);
                $query = $this->CI->db->get('InfusionsoftContact');
                if (sizeof($query->result()) > 1){
                    return true;
                }
            }

            //Stage 2 B
            if(!empty($Email) && !empty($StreetAddress1)){
                $this->CI->db->where('Email',$Email);
                $this->CI->db->where('StreetAddress1',$StreetAddress1);
                $query = $this->CI->db->get('InfusionsoftContact');
                if (sizeof($query->result()) > 1){
                    return true;
                }
            }

            //Stage 2 B
            if(!empty($Email) && !empty($Phone1)){
                $this->CI->db->where('Email',$Email);
                $this->CI->db->where('Phone1',$Phone1);
                $query = $this->CI->db->get('InfusionsoftContact');
                if (sizeof($query->result()) > 1){
                    return true;
                }
            }

            //Stage 3 A
            if(!empty($Email)){
                $this->CI->db->where('Email',$Email);
                $query = $this->CI->db->get('InfusionsoftContact');
                if (sizeof($query->result()) > 1){
                    return true;
                }
            }
        }
        return false;
    }
    public function _HookContactCallback($ActionType,$Id,$RecordData = []){
        $Response = [];
        $Response['Id'] = $Id;
        $Response['Contact'] = $RecordData;
        $Response['AppName'] = $this->CI->config->item('MacantaAppName');
        $Response['Action'] = $ActionType;
        //$Get =  'http://box941.temp.domains/~satvikso/project/demo/?Macantacron=Y';
        $Get =  'https://aaaccontactcenter.howlingbrands.com?Macantacron=Y';
        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/json',
                'content' => json_encode($Response)
            )
        );
        $context  = stream_context_create($opts);
        $res = @file_get_contents($Get, false, $context);
        //@file_put_contents(dirname(__FILE__)."/".$Response['AppName']."-HookContactCallback.txt",json_encode($Response));
        return  $res;
    }
}