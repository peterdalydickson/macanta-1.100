<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
function adminer_object()
{
    // Required to run any plugin.
    include_once "./plugins/plugin.php";

    // Plugins auto-loader.
    foreach (glob("plugins/*.php") as $filename) {
        include_once "./$filename";
    }

    function permanentLogin() {
        // key used for permanent login
        return '1a4b5963672174445ed1d63a3b928e7b';
    }

    // Specify enabled plugins here.
    $plugins = [
        new AdminerDatabaseHide(["mysql", "information_schema", "performance_schema"]),
        new AdminerLoginServers([
            "mysql://10.138.2.179" => "MySQL on macantacrm-db-sfo2-01",
            "mysql://10.138.44.30" => "MySQL on macantacrm-db-sfo2-02",
            "mysql://10.138.42.118" => "MySQL on macantacrm-db-sfo2-03",
            "mysql://165.227.240.207" => "MySQL Floating DB",
            "mysql://localhost" => "Localhost",
        ]),
        new AdminerSimpleMenu(),
        new AdminerCollations(),
        new AdminerJsonPreview(),
        // AdminerTheme has to be the last one.
        new AdminerTheme()


    ];

    return new AdminerPlugin($plugins);
}

// Include original Adminer or Adminer Editor.
include "./adminer-4.7.6.php";