<div class="col-md-12 theNotePanel">
    <div class="row mainbox-top">
        <div class="col-md-12 tab-panel">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h3 class="panel-title "><i class="fa fa-lock" aria-hidden="true"></i> Call Center</h3>
                </div>
                <div class="panel-body admin-panelBody admin-panelBody-callcenter callcenterContainer">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 callcenterContent">

                                <?php
                                use Twilio\Exceptions\RestException;
                                try {
                                    $TwilioAccountSid = $this->config->item('Twilio_Account_SID');
                                    $TwiliOauthToken = $this->config->item('Twilio_TOKEN');
                                    $TwilioClient = new Twilio\Rest\Client($TwilioAccountSid, $TwiliOauthToken);
                                    $workspaces = $TwilioClient->taskrouter->workspaces->read();
                                    echo ' <script>setupCallcenter();</script>';
                                } catch (RestException $e) {
                                    echo '<h3 style=" color: red; text-align: center; width: 100%; font-weight: bold; ">Sorry, Twilio Communication System Down.. ('.$e->getMessage().')</h3>';
                                }
                                ?>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 callcenter-overviewtop-full no-pad-left no-pad-right">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6   no-pad-left no-pad-right">
                                <h4>Over View</h4>
                                <p class="note2"><span>Showing here you call center statistics, and let you add your call center agents.</span></p>

                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6   no-pad-left no-pad-right callcenter-statuses">

                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  table-responsive  no-pad-left no-pad-right">
                            <table class="table table-hover callcenter-sections" width="100%;">
                                <thead>
                                <tr>
                                    <th>Call Center Section</th>
                                    <th>Total/Eligible Agents</th>
                                    <th>Available Agents</th>
                                    <th>Activity Statistics</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 callcenter-overviewbottom-full no-pad-left no-pad-right">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12   no-pad-left no-pad-right">
                                <h4>Agents</h4>
                                <p class="agent-note note2"><span>List of created Call Center Agents.</span></p>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  table-responsive  no-pad-left no-pad-right">
                                    <table class="table table-hover callcenter-agents" width="100%;">
                                        <thead>
                                        <tr>
                                            <th>Agent Name</th>
                                            <th>Skills</th>
                                            <th>Languages</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <button type="button" class="btn btn-default btn-add-agent"> Add Agent </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>




    </div>
    <div class="row">
    </div>
</div>