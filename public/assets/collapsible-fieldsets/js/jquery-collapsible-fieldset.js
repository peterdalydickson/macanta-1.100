/**
 * jQuery Plugin for creating collapsible fieldset.
 *
 * Copyright (c) 2013 Mirza Busatlic
 */

(function($) {
  
	$.fn.collapsible = function(options) {
		
		var settings = $.extend({
			collapsed: false, 
			animation: true, 
			speed: "medium",
			expanded: function () {}
		}, options);
		
		this.each(function() {
			var $fieldset = $(this);
			var $legend = $fieldset.children("legend");
			var isCollapsed = $fieldset.hasClass("collapsed");
			
			$legend.click(function() {
				collapse($fieldset, settings, !isCollapsed, $fieldset);
				isCollapsed = !isCollapsed;
			});
			
			// Perform initial collapse.
			// Don't use animation to close for initial collapse.
			if(isCollapsed) {
				collapse($fieldset, {animation: false}, isCollapsed, $fieldset);
			} else {
				collapse($fieldset, settings, isCollapsed, $fieldset);
			}
			
		});
	};
	
	/**
	 * Collapse/uncollapse the specified fieldset.
	 * @param {object} $fieldset
	 * @param {object} options
	 * @param {boolean} collapse
	 */
	function collapse($fieldset, options, doCollapse, theFieldset) {
		$container = $fieldset.find("div");
        $container2 = $fieldset.find("fieldset");
		if(doCollapse) {
			if(options.animation) {
				$container.slideUp(options.speed);
                $container2.slideUp(options.speed);
			} else {
				$container.hide();
                $container2.hide();
			}
			$fieldset.removeClass("expanded").addClass("collapsed");
		} else {
			if(options.animation) {
				$container.slideDown(options.speed);
                $container2.slideDown(options.speed);
			} else {
				$container.show();
                $container2.show();
			}
			$fieldset.removeClass("collapsed").addClass("expanded");
			options.expanded(theFieldset);
		}
	};
	
})(jQuery);
