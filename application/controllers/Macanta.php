<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 07/03/16
 * Time: 3:00 PM
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Macanta extends MY_Controller
{
    //protected $local_stylesheets = array('mystylesheet.css');
    public $content = array();
    public function __construct(){
        parent::__construct();
        //$this->local_stylesheets = array('css/your.css'); // for additional css file
        //$this->local_javascripts = array('js/your.js'); // for additional js file

    }

    public function index()
    {
        // LETS START WITH BLANK SCREEN FOR DYNAMIC CONTENT!
        if(isset($_GET['ID']) && isset($_GET['tab'])){
            $URL = $this->config->item('base_url')."#contact/".$_GET['ID']."/tab/".$_GET['tab'];
            header("location: ".$URL);
        }
        // Get all controller assets
        $AllTabs = json_decode($this->config->item('macanta_tabs'),true);
        $this->MacantaIgniter =& get_instance();
        foreach ($AllTabs as $Tab => $Params){
            foreach($Params['controllers'] as $Controller => $Methods ){
                $loadedController = _loadMacantaController($this->MacantaIgniter, $Controller);
                if(isset($this->MacantaIgniter->$loadedController)){
                    //if(method_exists($this->MacantaIgniter->$loadedController, 'theCSS'))
                        //print_r($this->MacantaIgniter->$loadedController) ;

                    //if(method_exists($this->$loadedController, 'theJS'))
                        //$this->local_javascripts = $this->$loadedController->theJS;
                }
            }
        }
        $language = $this->config->item('macanta_lang');
        if ($language === NULL)
        {
            $language = 'english';
        }
        $this->config->load('version', FALSE, TRUE);
        // Load the language file
        $this->lang->load('macanta', $language);
        $data['FrontPage'] = '';

        if ($this->agent->is_browser())
        {
            $agent = $this->agent->browser().' '.$this->agent->version();
        }
        elseif ($this->agent->is_robot())
        {
            $agent = $this->agent->robot();
        }
        elseif ($this->agent->is_mobile())
        {
            $agent = $this->agent->mobile();
        }
        else
        {
            $agent = 'Unidentified User Agent';
        }
        $data['browser']['agent'] = $agent;
        $data['browser']['platform'] = $this->agent->platform();
        //echo $agent;
        //echo $this->agent->platform();
        $FrontPage = $this->agent->is_mobile() ? 'frontpage':'frontpage';
        $start = time();
        $this->content[] = $this->load->view($FrontPage, $data, true); // return the view
        $end = time();
        $lapse = $end - $start;
        if($this->config->item('MacantaAppName') == 'qj395'){
            file_put_contents(dirname(__FILE__)."/".__CLASS__."-class.txt",date('Y-m-d H:i:s').": ".$lapse."\n",FILE_APPEND);
        }
        $this->render($this->content); // render the data
    }
    public function clientTwiml(){
        header('Content-type: text/xml');
        $CallerId = $this->config->item('macanta_caller_id');
        if (isset($_GET['From']) || isset($_POST['From'])) {
            $From = isset($_GET['From']) ? $_GET['From'] : $_POST['From'];
            $CallerId = trim($From) != "" ? $From:$CallerId ;
        }
        if (isset($_GET['PhoneNumber']) || isset($_POST['PhoneNumber'])) {
            $request = isset($_GET['PhoneNumber']) ? $_GET['PhoneNumber'] : $_POST['PhoneNumber'];
            $number = htmlspecialchars($request);
            file_put_contents('php://stderr', print_r($number, TRUE));
        } else {
            file_put_contents('php://stderr', print_r("PhoneNumber not set", TRUE));
            // $number="+442476276203";
        }

        if (preg_match("/^[\d\+\-\(\) ]+$/", $number)) {
            $numberOrClient = "<Number>" . $number . "</Number>";
        } else {
            $numberOrClient = "<Client>" . $number . "</Client>";
        }
        echo '<Response><Dial record="record-from-ringing" callerId="'.$CallerId.'">'.$numberOrClient.'</Dial></Response>';
    }
    public function serviceWorker(){
        header("content-type: application/javascript");
        $this->load->config('version');
        $macantaVersion = $this->config->item('macanta_verson');
       // echo "Version: ".$macantaVersion."\n";
        $script = file_get_contents(FCPATH."ServiceWorkerFile.js");
        $script = str_replace('{macantaVersion}', $macantaVersion, $script);
        echo $script;
        exit(0);
    }
}
