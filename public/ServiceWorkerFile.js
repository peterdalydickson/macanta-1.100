/*
Copyright 2016 Google Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

(function() {
    'use strict';
    var filesToCache = [
        'assets/bootstrap/css/bootstrap.min.css?v={macantaVersion}',
        'assets/font-awesome/css/font-awesome.min.css?v={macantaVersion}',
        'assets/css/form-elements.css?v={macantaVersion}',
        'assets/css/content.min.css?v={macantaVersion}',
        'assets/css/bootstrap-switch.css?v={macantaVersion}',
        'assets/css/bootstrap.vertical-tabs.css?v={macantaVersion}',
        'assets/css/dataTables.bootstrap.min.css?v={macantaVersion}',
        'assets/format-phone/build/css/intlTelInput.css?v={macantaVersion}',
        'assets/format-phone/build/css/demo.css?v={macantaVersion}',
        'assets/css/jquery.tagsinput.css?v={macantaVersion}',
        'assets/css/daterangepicker.css?v={macantaVersion}',
        'assets/jquery-ui-1.11.4/jquery-ui.css?v={macantaVersion}',
        'assets/bootstrap-select/css/bootstrap-select.css?v={macantaVersion}',
        'assets/bootstrap/css/bootstrap-tour.css?v={macantaVersion}',
        'assets/bootstrap-fileinput/css/fileinput.css?v={macantaVersion}',
        'assets/css/multi-select.css?v={macantaVersion}',
        'assets/bootstrap-multiselect/css/bootstrap-multiselect.css?v={macantaVersion}',
        'assets/css/toggles.css?v={macantaVersion}',
        'assets/css/toggles-iphone.css?v={macantaVersion}',
        'assets/css/normalize.css?v={macantaVersion}',
        'assets/css/responsive.jqueryui.css?v={macantaVersion}',
        'assets/css/responsive.bootstrap.css?v={macantaVersion}',
        'assets/css/responsive.dataTables.css?v={macantaVersion}',
        'assets/css/macanta.css?v={macantaVersion}',
        'assets/collapsible-fieldsets/css/jquery-collapsible-fieldset.css?v={macantaVersion}',
        'assets/js/jquery-1.11.1.min.js?v={macantaVersion}',
        'assets/jquery-ui-1.11.4/jquery-ui.min.js?v={macantaVersion}',
        'assets/bootstrap/js/bootstrap.min.js?v={macantaVersion}',
        'assets/js/bootstrap-switch.js?v={macantaVersion}',
        'assets/js/jquery.backstretch.min.js?v={macantaVersion}',
        'assets/js/moment.js?v={macantaVersion}',
        'assets/js/jquery.dataTables.min.js?v={macantaVersion}',
        'assets/js/dataTables.bootstrap.min.js?v={macantaVersion}',
        'assets/tinymce/tinymce.min.js?v={macantaVersion}',
        'assets/js/twilio_script.js?v={macantaVersion}',
        'assets/format-phone/build/js/intlTelInput.js?v={macantaVersion}',
        'assets/format-phone/build/js/utils.js?v={macantaVersion}',
        'assets/js/jquery.tagsinput.js?v={macantaVersion}',
        'assets/js/daterangepicker.js?v={macantaVersion}',
        'assets/bootstrap-select/js/bootstrap-select.js?v={macantaVersion}',
        'assets/bootstrap/js/bootstrap-tour.js?v={macantaVersion}',
        'assets/bootstrap-fileinput/js/fileinput.js?v={macantaVersion}',
        'assets/bootstrap-fileinput/js/locales/fr.js?v={macantaVersion}', // to be set dynamically inside macanta
        'assets/bootstrap-fileinput/js/locales/es.js?v={macantaVersion}', // to be set dynamically inside macanta
        'assets/js/jquery.multi-select.js?v={macantaVersion}',
        'assets/bootstrap-multiselect/js/bootstrap-multiselect.js?v={macantaVersion}',
        'assets/js/jquery.quicksearch.js?v={macantaVersion}',
        'assets/js/wrap.js?v={macantaVersion}',
        'assets/js/Toggles.js?v={macantaVersion}',
        'assets/js/readmore.min.js?v={macantaVersion}',
        'assets/js/responsive.jqueryui.js?v={macantaVersion}',
        'assets/js/responsive.bootstrap.js?v={macantaVersion}',
        'assets/js/multiselect.js?v={macantaVersion}',
        'assets/js/functions.js?v={macantaVersion}-a',
        'assets/js/macanta.js?v={macantaVersion}',
        'assets/collapsible-fieldsets/js/jquery-collapsible-fieldset.js?v={macantaVersion}',
        'assets/tinymce/plugins/emoticons/plugin.min.js',
        'assets/tinymce/plugins/code/plugin.min.js',
        'assets/tinymce/plugins/fullscreen/plugin.min.js',
        'assets/tinymce/plugins/insertdatetime/plugin.min.js',
        'assets/tinymce/themes/modern/theme.min.js',
        'assets/tinymce/plugins/advlist/plugin.min.js',
        'assets/tinymce/plugins/autolink/plugin.min.js',
        'assets/format-phone/build/js/utils.js',
        'assets/tinymce/plugins/link/plugin.min.js',
        'assets/tinymce/plugins/image/plugin.min.js',
        'assets/tinymce/plugins/lists/plugin.min.js',
        'assets/tinymce/plugins/charmap/plugin.min.js',
        'assets/tinymce/plugins/print/plugin.min.js',
        'assets/tinymce/plugins/preview/plugin.min.js',
        'assets/tinymce/plugins/hr/plugin.min.js',
        'assets/tinymce/plugins/anchor/plugin.min.js',
        'assets/tinymce/plugins/pagebreak/plugin.min.js',
        'assets/tinymce/plugins/searchreplace/plugin.min.js',
        'assets/tinymce/plugins/wordcount/plugin.min.js',
        'assets/tinymce/plugins/visualblocks/plugin.min.js',
        'assets/tinymce/plugins/visualchars/plugin.min.js',
        'assets/tinymce/plugins/media/plugin.min.js',
        'assets/tinymce/plugins/nonbreaking/plugin.min.js',
        'assets/tinymce/plugins/save/plugin.min.js',
        'assets/tinymce/plugins/table/plugin.min.js',
        'assets/tinymce/plugins/contextmenu/plugin.min.js',
        'assets/tinymce/plugins/directionality/plugin.min.js',
        'assets/tinymce/plugins/template/plugin.min.js',
        'assets/tinymce/plugins/paste/plugin.min.js',
        'assets/tinymce/plugins/textcolor/plugin.min.js',
        'assets/tinymce/skins/lightgray/fonts/tinymce.woff',
        'assets/jquery-ui-1.11.4/images/ui-bg_highlight-soft_100_eeeeee_1x100.png',
        'assets/bootstrap/fonts/glyphicons-halflings-regular.woff2',
        'assets/font-awesome/fonts/fontawesome-webfont.woff2?v=4.7.0',
        'https://js.intercomcdn.com/fonts/proximanova-regular.a7942249.woff',
        'assets/img/listbg.jpg',
        'assets/img/logos_powerdby_twilio.png',
        'assets/tinymce/skins/lightgray/skin.min.css',
        'assets/img/isicon.png',
        'assets/img/macantaicon.png',
        'assets/img/minimal-background-pattern.jpg',
        'https://translate.yandex.net/website-widget/v1/widget.html',
        'assets/img/loadingmacanta.gif',
        'assets/img/listbg2.jpg',
        'assets/img/google.jpg',
        'https://translate.yandex.net/website-widget/v1/widget.js?widgetId=ytWidget&pageLang=en&widgetTheme=light&trnslKey=trnsl.1.1.20170324T105515Z.cd22b0b89d711c43.893727e9ddd730237b7eed44a266acbf77c5b81c&autoMode=true',
        //'https://translate.yandex.net/v2.177.1516698385/js/tr_page.js' // No 'Access-Control-Allow-Origin'

    ];

    var staticCacheName = 'macanta-cache';

    self.addEventListener('install', function(event) {
        console.log('Attempting to install service worker and cache static assets');
        console.log('Cache Name: '+staticCacheName);
        event.waitUntil(
            caches.open(staticCacheName)
                .then(function(cache) {
                    return cache.addAll(filesToCache);
                })
        );
    });

    self.addEventListener('fetch', function(event) {
        //console.log('Fetch event for ', event.request.url);
        event.respondWith(
            caches.match(event.request).then(function(response) {
                if (response) {
                    //console.log('Found ', event.request.url, ' in cache');
                    return response;
                }
                //console.log('Network request for ', event.request.url);
                return fetch(event.request).then(function(response) {
                    if (response.status === 404) {
                        return caches.match('404.html');
                    }
                    return caches.open(staticCacheName).then(function(cache) {
                        if (event.request.url.indexOf('adminer') < 0 && event.request.url.indexOf('user') < 0  && event.request.url.indexOf('contacts') < 0 && event.request.url.indexOf('rest/v1') < 0 && event.request.url.indexOf('rest/v2') < 0 && event.request.url.indexOf('ajax') < 0 && event.request.url.indexOf('unsubscribe') < 0 && event.request.url.indexOf('sei') < 0 && event.request.url.indexOf('token') < 0) {
                            cache.put(event.request.url, response.clone());
                        }
                        return response;
                    });
                });
            }).catch(function(error) {
                console.log('Error, ', error);
                return caches.match('pages/offline.html');
            })
        );
    });

    self.addEventListener('activate', function(event) {
        console.log('Activating new service worker...');

        var cacheWhitelist = [staticCacheName];

        event.waitUntil(
            caches.keys().then(function(cacheNames) {
                return Promise.all(
                    cacheNames.map(function(cacheName) {
                        if (cacheWhitelist.indexOf(cacheName) === -1) {
                            return caches.delete(cacheName);
                        }
                    })
                );
            })
        );
    });

})();