<!--Modal For Editing Contact Details-->
<?php
//$DVE = macanta_validate_feature('DVE');
$DVE = 'enabled';
if($DVE && $DVE == "enabled"){
    $DVE = true;
}else{
    $DVE = false;
}
$No = 'No';
$DisableThis = '';
if(!$DVE){
    $No = 'Unable';
    $DisableThis = 'Unable';
}
if(!empty($Phone1)){
    $Phone1Info = macanta_validate_phone_number($Id,$Phone1,$Country,$session_data);
    $Phone1Stat = $Phone1Info['IsValid'];
}else{
    $Phone1Stat = 'Unable';
}

if(!empty($Phone2)){
    $Phone2Info = macanta_validate_phone_number($Id,$Phone2,$Country,$session_data);
    $Phone2Stat = $Phone2Info['IsValid'];
}else{
    $Phone2Stat = 'Unable';
}
if($Email){
    $EmailInfo = macanta_validate_email($Email,$session_data);
    if($EmailInfo['format_valid']){
        $EmailStat = $EmailInfo['format_valid'] == 1 ? "Yes":$EmailInfo['format_valid'];
    }else{
        $EmailStat = $No;
    }
}else{
    $EmailStat = $No;
}
//$stripe_customer_status = macanta_get_stripe_customer_status();
//$stripe_customer_status_arr = json_decode($stripe_customer_status, true);
$appname = $this->config->item('MacantaAppName');
$messageVerify = '<a href="https://macanta.org/add-data-validation-enrichment/?app_name='.$appname.'" target="_blank">Oops! Automated address verification has not yet been enabled. 
    <b>Click here</b> to find out about contact data validation and enrichment.</a>';
$messageEdit = '<a href="https://macanta.org/add-data-validation-enrichment/?app_name='.$appname.'"  target="_blank">Would you like to use this automagical address finder?! 
    <b>Click here</b> to find out about the new Data Validation & Enrichment add-on for Macanta. Or click \'Manual Edit\' below.</a>';
?>
<div id="ContactDetails" class="modal fade ContactDetails" tabindex="-1" role="dialog" aria-labelledby="ContactDetailsLabel">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title"><?php echo $this->lang->line('text_contact_details'); ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="ContactDetailsFormEdit" id="ContactDetailsFormEdit">
                    <input data-id="<?php echo $Id; ?>" type="hidden" name="AddressBillingEdit" value="<?php echo checkVerifiedAddress($Id,"AddressBillingEdit") == true ? "verified":""?>">
                    <input data-id="<?php echo $Id; ?>" type="hidden" name="AddressShippingEdit" value="<?php echo checkVerifiedAddress($Id,"AddressShippingEdit") == true ? "verified":""?>">
                    
                <div class="flex-row">
                    <div class="user-info-b MainInfoEdit col-half">
                        <div class="MainInfoEditHeader label-lg">General Information </div>

                        <ul class="user-info">
                            <li class="info-item ">
                                <span class="label">
                                    <label class="label-sm">
                                        <?php echo $this->lang->line('text_firstname'); ?>
                                    </label>
                                    <input name="FirstName" class="FirstName" value="<?php echo $FirstName; ?>">
                                </span>
                            </li>
                            <li class="info-item ">
                                <span class="label">
                                    <label class="label-sm">
                                        <?php echo $this->lang->line('text_lastname'); ?>
                                    </label>
                                    <input name="LastName" class="LastName" value="<?php echo $LastName; ?>">
                                </span>
                            </li>
                            <li class="info-item EmailContainerEdit">
                                <div class="macanta-confirm-info EmailConfirm" data-target="Email" data-value="" style="display: none;">
                                    Do you mean: <span class="message"></span> ? <a class="btn btn-macanta-confirm-info yes"> Yes</a> / <a class="btn btn-macanta-confirm-info no"> No</a>
                                </div>
                                <div class="macanta-notice-info EmailStatEdit " style="display: none;">
                                    <?php
                                    $Format = $EmailInfo['format_valid'] ? 'Valid Format':'Invalid Format';
                                    $SMTPCheck = $EmailInfo['smtp_check'] ? 'Passed':'Failed';
                                    switch ($EmailStat){
                                        case 'Yes':
                                            echo '<ul class="macanta-notice-ul">
                                        <li><span>Domain:</span> '.$EmailInfo['domain'].'</li>
                                        <li><span>Format:</span> '. $Format .'</li>
                                        <li><span>SMTP Check:</span> '. $SMTPCheck .'</li>
                                        <li><span>Score:</span> '.$EmailInfo['score'].'</li>
                                      </ul>';
                                            break;
                                        case 'No':
                                            echo '<ul class="macanta-notice-ul">
                                        <li><span>Status:</span> Not A Valid Email Address</li>
                                      </ul>';
                                            break;
                                        case 'Unable':
                                            echo '<ul class="macanta-notice-ul">
                                        <li><span>Verification Disabled</span></li>
                                      </ul>';
                                            break;
                                    }
                                    ?>
                                </div>
                                <span class="label">
                                    <label class="label-sm">
	                                    <span>
                                            <?php echo $this->lang->line('text_email'); ?>
                                        </span>
	                                    <i data-show="EmailStatEdit" class="fa fa-envelope-square show-notice IsEmailValidEdit IsEmailValid<?php echo $EmailStat; ?>"></i>
                                    </label>
                                    <input name="Email" data-type="Edit" class="Email" data-original="<?php echo $Email; ?>" value="<?php echo $Email; ?>">
                                    <?php
                                    //Email icon should not be green when $SMTPCheck is Failed
                                    $EmailStat = $SMTPCheck == 'Passed' ? $EmailStat:$No
                                    ?>
                                    
                                </span>
                            </li>
                            <li class="info-item ">
                                <span class="label">
                                    <label class="label-sm">
                                        Title
                                    </label>
                                    <input name="JobTitle" class="JobTitle" value="<?php echo $JobTitle; ?>">
                                </span>
                            </li>
                            <li class="info-item ">
                                <span class="label">
                                    <label class="label-sm">
                                        <?php echo $this->lang->line('text_company'); ?>
                                    </label>
                                    <input name="Company" class="Company" value="<?php echo $Company; ?>">
                                </span>
                            </li>
                            <li class="info-item Phone1Container">
                                <div class="macanta-notice-info PhoneStatEdit Phone1StatEdit" style="display: none;">
                                    <?php
                                    switch ($Phone1Stat){
                                        case 'Yes':
                                            echo '<ul class="macanta-notice-ul">
                                        <li><span>Status:</span> Valid Phone Number</li>
                                        <li><span>PhoneNumber:</span> '.$Phone2Info['PhoneNumber'].'</li>
                                        <li><span>NetworkName:</span> '.$Phone1Info['NetworkName'].'</li>
                                        <li><span>NetworkCode:</span> '.$Phone1Info['NetworkCode'].'</li>
                                        <li><span>NetworkCountry:</span> '.$Phone1Info['NetworkCountry'].'</li>
                                        <li><span>NationalFormat:</span> '.$Phone1Info['NationalFormat'].'</li>
                                        <li><span>CountryPrefix:</span> '.$Phone1Info['CountryPrefix'].'</li>
                                        <li><span>NumberType:</span> '.$Phone1Info['NumberType'].'</li>
                                      </ul>';
                                            break;
                                        case 'No':
                                            echo '<ul class="macanta-notice-ul">
                                        <li><span>Status:</span> Not A Valid Phone Number</li>
                                      </ul>';
                                            break;
                                        case 'Unable':
                                            echo '<ul class="macanta-notice-ul">
                                        <li><span>Verification Disabled</span></li>
                                      </ul>';
                                            break;
                                    }
                                    ?>
                                </div>
                                <span class="label">
                                    <label class="label-sm">
	                                    <span>
                                            <?php echo $this->lang->line('text_phone'); ?> 1
                                        </span>
	                                    <i data-show="Phone1StatEdit" class="fa fa-phone-square show-notice IsPhoneValidEdit IsPhone1ValidEdit IsPhoneValid<?php echo $Phone1Stat; ?>"></i>
                                    </label>
                                    <input name="Phone1" data-type="Edit" class="Phone1" value="<?php echo $Phone1; ?>">
                                </span>
                            </li>
                            <li class="info-item Phone2Container">
                                <div class="macanta-notice-info PhoneStatEdit Phone2StatEdit" style="display: none;">
                                    <?php
                                    switch ($Phone2Stat){
                                        case 'Yes':
                                            echo '<ul class="macanta-notice-ul">
                                        <li><span>Status:</span> Valid Phone Number</li>
                                        <li><span>PhoneNumber:</span> '.$Phone2Info['PhoneNumber'].'</li>
                                        <li><span>NetworkName:</span> '.$Phone2Info['NetworkName'].'</li>
                                        <li><span>NetworkCode:</span> '.$Phone2Info['NetworkCode'].'</li>
                                        <li><span>NetworkCountry:</span> '.$Phone2Info['NetworkCountry'].'</li>
                                        <li><span>NationalFormat:</span> '.$Phone2Info['NationalFormat'].'</li>
                                        <li><span>CountryPrefix:</span> '.$Phone2Info['CountryPrefix'].'</li>
                                        <li><span>NumberType:</span> '.$Phone2Info['NumberType'].'</li>
                                      </ul>';
                                            break;
                                        case 'No':
                                            echo '<ul class="macanta-notice-ul">
                                        <li><span>Status:</span> Not A Valid Phone Number</li>
                                      </ul>';
                                            break;
                                        case 'Unable':
                                            echo '<ul class="macanta-notice-ul">
                                        <li><span>Verification Disabled</span></li>
                                      </ul>';
                                            break;
                                    }
                                    ?>
                                </div>
                                <span class="label">
                                    <label class="label-sm">
	                                    <span>
                                            <?php echo $this->lang->line('text_phone'); ?> 2
                                        </span>
	                                    <i data-show="Phone2StatEdit" class="fa fa-phone-square show-notice IsPhoneValidEdit IsPhone2ValidEdit IsPhoneValid<?php echo $Phone2Stat; ?>"></i>
                                    </label>
                                    <input name="Phone2" data-type="Edit" class="Phone2" value="<?php echo $Phone2; ?>">
                                </span>
                            </li>
                        </ul>
                    </div><!-- /.user-info-b.MainInfoEdit -->
                    <div class="user-info-b  col-half">
                        <div class="AddressSliderEdit">
                            <div class="AddressSliderContainerEdit">
                                <div class="AddressBillingEdit">

                                    <div class="switchAddressEdit">
	                                    <div class="flex-row">
	                                        <div class="address-status-container">
	                                            <i
	                                                    class="fa fa-address-card-o AddressStatus  <?php echo $DisableThis; ?> <?php echo checkVerifiedAddress($Id,"AddressBillingEdit") == true ? "ValidatedAddress":"InValidatedAddress"?>"
	                                                    title="<?php echo checkVerifiedAddress($Id,"AddressBillingEdit") == true ? "Address Verified":"Address Not Verified"?>">
	
	                                            </i></div>
	                                        <div class="">
	                                            <label class="label-sm">Billing Address</label>
	                                            <span class="show-other-address btn "><?php echo $this->lang->line('text_show_shipping_instead'); ?></span>
	                                        </div>
	                                    </div><!-- /.flex-row -->
                                        <div class="AddressBillingEditLoqateContainer " style="display: none">
                                            <input
                                                    class="LoqateField"
                                                    type="text"
                                                    data-addresstype="AddressBillingEdit"
                                                    name="AddressBillingEditLoqate"
                                                    id="AddressBillingEditLoqate"
                                                    placeholder="please type in your address..."
                                                   <?php echo $DVE ? "":"disabled"; ?>
                                            >
                                            <?php echo $DVE ? '':'<span class="notice-alert">'.$messageEdit.'</span>'; ?>
                                            <a  id="btnAddressBillingEditManual"  data-addresstype="AddressBillingEdit" class="btn btnEditLink AddressBillingEditManual AddressEditManual">Manual Edit</a>
                                            <ul class="AddressMainList">

                                            </ul>
                                        </div>
                                    </div>
                                    <ul class="user-info contactView">

                                    </ul>
                                    <div class="btn-toolbar address-edit-btn">
	                                        <a id="btnAddressBillingEditNoAddress" class="hideThis">- no address -</a>
	                                        <a  id="btnAddressBillingEditVerify" data-addresstype="AddressBillingEdit" class="btn btnEditLink <?php echo $DVE ? "AddressBillingEditVerify":"AddressVerifyAlert"; ?>">Verify</a>
	                                        <a  id="btnAddressBillingLoqateEdit" data-addresstype="AddressBillingEdit" class="btn btnEditLink AddressBillingEditLoqate ShowAddressEditLoqate">Edit</a>
	                                        <?php
	                                        if(!$DVE){ ?>
	                                        <div class="DVE-alert " style="display: none">
	                                            <?php echo $messageVerify; ?>
	                                        </div>
	                                        <?php }
	                                        ?>
	                                    </div>
	                                </div>
	                                <div class="AddressShippingEdit">
	
	                                    <div class="switchAddressEdit">
		                                    <div class="flex-row">
		                                        <div class="">
		                                            <i
		                                                    class="fa fa-address-card-o AddressStatus  <?php echo $DisableThis; ?>  <?php echo checkVerifiedAddress($Id,"AddressShippingEdit") == true ? "ValidatedAddress":"InValidatedAddress"?>"
		                                                    title="<?php echo checkVerifiedAddress($Id,"AddressShippingEdit") == true ? "Address Verified":"Address Not Verified"?>">
		
		                                            </i></div>
		                                        <div class="">
		                                            <label class="label-sm">Shipping Address</label>
		                                            <span class="show-other-address btn "><?php echo $this->lang->line('text_show_billing_instead'); ?></span>
		                                        </div>
		                                    </div><!-- /.flex-row -->
	                                        <div class="AddressShippingEditLoqateContainer"  style="display: none">
	                                            <input
	                                                    class="LoqateField"
	                                                    type="text"
	                                                    data-addresstype="AddressShippingEdit"
	                                                    name="AddressShippingEditLoqate"
	                                                    id="AddressShippingEditLoqate"
	                                                    placeholder="please type in your address..."
	                                                    <?php echo $DVE ? "":"disabled"; ?>
	                                            >
	                                            <?php echo $DVE ? '':'<span class="notice-alert">'.$messageEdit.'</span>'; ?>
	                                            <a  id="btnAddressShippingEditManual"  data-addresstype="AddressShippingEdit" class="btn btnEditLink AddressShippingEditManual AddressEditManual">Manual Edit</a>
	                                            <ul class="AddressMainList">
	
	                                            </ul>
	                                        </div>
	                                    </div>
	                                    <ul class="user-info contactView">
	
	                                    </ul>
	                                    <div class="btn-toolbar address-edit-btn">
	                                        <a id="btnAddressShippingEditNoAddress" class="hideThis">- no addresss -</a>
	                                        <a  id="btnAddressShippingEditVerify" data-addresstype="AddressShippingEdit" class="btn btnEditLink <?php echo $DVE ? "AddressShippingEditVerify":"AddressVerifyAlert"; ?>">Verify</a>
	                                        <a  id="btnAddressShippingLoqateEdit" data-addresstype="AddressShippingEdit"  class="btn btnEditLink AddressShippingEditLoqate ShowAddressEditLoqate">Edit</a>
	                                        <?php
	                                        if(!$DVE){ ?>
	                                            <div class="DVE-alert " style="display: none">
	                                                <?php echo $messageVerify; ?>
	                                            </div>
	                                        <?php }
	                                        ?>
	
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	
	                    </div>
	                </div><!-- /.flex-row -->
                    <div class="user-info-b MainInfoEdit">

                        <ul class="user-info">
                            <li class="info-item ">
	                                <span class="label"><label class="label-sm">Email Signature</label>
	                                    <textarea name="EmailSignature" id="EmailSignature" class="EmailSignature" placeholder=""><?php
                                            $this->db->where('user_id',$Id);
                                            $this->db->where('meta_key','EmailSignature');
                                            $query = $this->db->get('users_meta');
                                            $row = $query->row();
                                            if (isset($row)) echo $row->meta_value;
                                            ?></textarea>
	                                </span>
                            </li>
                        </ul>
                        <script>
                            tinymce.init({
                                selector: '#EmailSignature',
                                menubar: false,
                                menu: {
                                    view: {title: 'Options', items: 'code'}
                                },
                                plugins: [
                                    'template advlist autolink link image lists charmap print preview hr anchor pagebreak ',
                                    'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                                    'save table contextmenu directionality emoticons template paste textcolor '
                                ],
                                toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat ',
                                content_css: '/assets/css/content.min.css',
                            });
                        </script>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="nb-btn nb-secondary"
                        data-dismiss="modal"><?php echo $this->lang->line('text_close'); ?></button>
                <button type="button"
                        class="nb-btn nb-primary updateContactDetails"><?php echo $this->lang->line('text_save'); ?></button>
            </div>

            <script>
                var BillingAddressEdit = {
                    "StreetAddress1":"<?php echo $StreetAddress1; ?>",
                    "StreetAddress2":"<?php echo $StreetAddress2; ?>",
                    "City":"<?php echo $City; ?>",
                    "State":"<?php echo $State; ?>",
                    "PostalCode":"<?php echo $PostalCode; ?>",
                    "Country":"<?php echo $Country; ?>"
                };
                var AddressShippingEdit = {
                    "Address2Street1":"<?php echo $Address2Street1; ?>",
                    "Address2Street2":"<?php echo $Address2Street2; ?>",
                    "City2":"<?php echo $City2; ?>",
                    "State2":"<?php echo $State2; ?>",
                    "PostalCode2":"<?php echo $PostalCode2; ?>",
                    "Country2":"<?php echo $Country2; ?>"
                };
                showAddressesEdit(BillingAddressEdit,"AddressBillingEdit");
                showAddressesEdit(AddressShippingEdit,"AddressShippingEdit");
                // Prevent Bootstrap dialog from blocking focusin
                $(document).on('focusin', function(e) {
                    if ($(e.target).closest(".mce-window").length) {
                        e.stopImmediatePropagation();
                    }
                });
            </script>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
