<!--Start of e-commerce panel-->
<div class="user-info-b  col-sm-12">
    <ul class="user-info ">
        <li class="info-item invoiceInfo">
            <span class="label"><?php echo $this->lang->line('text_total_invoiced'); ?>: </span>
            <span class="InvoiceTotal"><?php echo $InvoiceTotal; ?></span>
        </li>
        <li class="info-item ">
            <span class="label"><?php echo $this->lang->line('text_amount_owed'); ?>: </span>
            <span class="InvoiceOwed"><?php echo $InvoiceOwed; ?></span>
        </li>
    </ul>
</div>
<!--End of e-commerce panel-->
