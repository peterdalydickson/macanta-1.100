<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 07/03/16
 * Time: 3:00 PM
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends MY_Controller
{
    public $ajaxResults = array(
        "status"=> 0, // true or false
        "message"=>"",// any massage
        "data" => array(), // returned data
        "script" => '' // javascript need to execute // returned data
    );
    private $MacantaIgniter;
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));

    }
    public function index($Data)
    {
        $Menu = json_decode($this->config->item('macanta_menu'), true);
        $AllowedMenu = $Data['Items'];
        foreach ($Menu as $MenuName => $Params){
            if(!in_array('all', $AllowedMenu)){
                if(!in_array($MenuName, $AllowedMenu)) continue;
            }
            $MacantaMenu[$MenuName] = $Params;
        }
        $MenuContent['Menu'] = $MacantaMenu;
        $HTML =  $this->load->view('core/menu_index', $MenuContent, true);
        $this->ajaxResults['data'] = $HTML;
        $this->ajaxResults['message'] = "Menu Displayed";
        $this->ajaxResults['status'] = 1;
        return $this->ajaxResults;
    }


}