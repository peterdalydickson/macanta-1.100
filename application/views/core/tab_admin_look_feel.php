<div class="theNotePanel">
    <div class="mainbox-top">
        <div class="tab-panel left-tab-panel">
            <div class="panel panel-primary left-LookAndFeel">
                <div class="panel-heading">

                    <h3 class="panel-title"><div class="panel-title-inner"><i class="fa fa-file-image-o"></i> Change Logo</div></h3><!--<span class="updateStat"><i class="fa fa-refresh"></i></span>-->

                </div>
                <div class="panel-body admin-panelBody">
                    <div class="uploaded-img">
                        <?php
                        $SiteLogo  = macanta_db_record_exist('key','macanta_custom_logo','config_data',true);
                        if($SiteLogo){
                            $SiteLogoArr = json_decode($SiteLogo->value)
                            ?>
                            <img class="adminloginLogo" src="data:<?php echo $SiteLogoArr->type;?>;base64,<?php echo $SiteLogoArr->imageData;?>" style=" " alt="">
                        <?php }else{ ?>
                            <img class="adminloginLogo" src="<?php echo $this->config->item('base_url');?>assets/img/macanta_logo.png" style=" " alt="">
                        <?php }
                        ?>
                        <form enctype="multipart/form-data" class="FormUploadLogo">
                            <input id="adminlogofile" class="btn btn-default" name="adminlogofile" type="file">
                        </form>
                        <!--        var jsonData = {"controler":"core/tabs/triggers","action":"applyGoals","data":{"InfusionsoftID":ContactId, "CallIdArr":checkedValues}};
-->
                        <script>
                            $('#adminlogofile').fileinput({
                                language: 'en',
                                uploadUrl: ajax_url,
                                allowedFileExtensions : ['jpg', 'png','gif'],
                                uploadExtraData:{"controler":"core/tabs/admin","action":"setLogo","session_name":session_name},
                                showRemove: false,
                                showCancel: false,
                                maxFileSize: 1000,
                                maxFilesNum: 10,
                                showAjaxErrorDetails: false,
                                browseOnZoneClick: true
                            }).on('fileuploaded', function(event, data, previewId, index) {
                                var form = data.form, files = data.files, extra = data.extra,
                                    response = data.response, reader = data.reader;
                                console.log('File uploaded triggered');
                                console.log(data);
                                $(this).fileinput('clear').fileinput('enable');
                                if(typeof response.script !==  'undefined'){
                                    eval(response.script);
                                }

                            });
                        </script>
                    </div>
                </div>
            </div>

        </div>
        <div class="tab-panel right-tab-panel">
            <div class="panel panel-primary right-LookAndFeel">
                <div class="panel-heading">
                    <h3 class="panel-title "><div class="panel-title-inner"><i class="fa fa-language"></i> Language Settings</div></h3>
                </div>
                <div class="panel-body admin-panelBody">
                    <?php
                    $LanguageFolder = APPPATH."language/";
                    $LanguageFolderFiles = scandir($LanguageFolder);
                    $AvailableLanguage = array();
                    foreach($LanguageFolderFiles as $Item){
                            if(is_dir($LanguageFolder.$Item) && !in_array($Item,array(".",".."))){
                                if(is_file($LanguageFolder.$Item."/macanta_lang.php")){
                                    $AvailableLanguage[] = $Item;
                                }
                            }
                    }
                    $SavedLanguage[$this->config->item('macanta_lang')] = $this->config->item('macanta_lang');


                    ?>
                    <div class="language-select">
	                    <h3>Default Language: </h3>
	                    <!-- <select class="languagepicker form-control"  title="Choose your default language"> -->
                        <select class="languagepicker form-control"  title="Choose your default language">
	                        <?php
	                        foreach($AvailableLanguage as $Language){
	                                $selected = isset($SavedLanguage[$Language]) ? 'selected':'';
	                                echo "<option value='".$Language."' $selected>".ucfirst($Language)."</option>";
	                        }
	                        ?>
	                    </select>
                    </div>
                </div>
            </div>



        </div>

        <div class="csseditor tab-panel">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title "><i class="fa fa-language"></i> Custom CSS Editor</h3>
                </div>
                <div class="panel-body admin-panelBody">
<textarea id="csseditor" style="height: 350px; width: 100%;" name="csseditor">
<?php echo $this->config->item('CustomCSS') ? base64_decode($this->config->item('CustomCSS')):"";?>
</textarea>
                </div>
            </div>
        </div>
        <div class="jseditor tab-panel">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title "><i class="fa fa-language"></i> Custom Javascript Editor</h3>
                </div>
                <div class="panel-body admin-panelBody">
                    <div class="ScriptError"></div>
<textarea id="jseditor" style="height: 350px; width: 100%;" name="jseditor">
<?php echo $this->config->item('CustomJS') ? base64_decode($this->config->item('CustomJS')):"";?>
</textarea>
                </div>
            </div>
        </div>
        <!--<div class="col-md-6">
            <div class="panel panel-primary CacheManagement">
                <div class="panel-heading">
                    <h3 class="panel-title "><i class="fa fa-exchange"></i> Cache Management</h3>

                </div>
                <div class="panel-body admin-panelBody admin-panelBody-CacheManagement">
                    <ul class="list-group col-sm-12 col-md-12 col-lg-12">
                        <li class="list-group-item refreshWebform col-sm-12 col-md-12 col-lg-12">
                            <a class="col-sm-12 col-md-12 col-lg-12 btn btn-default   refreshWebformButton" onclick="refreshWebform();">Refresh Webform HTML</a>
                        </li>
                        <li class="list-group-item refreshISsettings col-sm-12 col-md-12 col-lg-12">
                            <a class="col-sm-12 col-md-12 col-lg-12 btn btn-default   refreshISsettingsButton" onclick="refreshISsettings();">Refresh Saved Infusionsoft App Settings</a>
                        </li>
                        <li class="list-group-item refreshCustomFields col-sm-12 col-md-12 col-lg-12">
                            <a class="col-sm-12 col-md-12 col-lg-12 btn btn-default   refreshCustomFieldsButton" onclick="refreshCustomFields();">Refresh Saved Custom Fields</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>-->
    </div>
    <div class="row">
    </div>
</div>
<script>
    editAreaLoader.init({
        id: "csseditor"
        ,start_highlight: true
        ,font_size: "10"
        ,font_family: "verdana, monospace"
        ,allow_toggle: false
        ,language: "en"
        ,syntax: "css"
        ,toolbar: "save,|, search, |, undo, redo, |, select_font, |"
        ,save_callback: "csseditor_save"
        ,plugins: "charmap"
        ,charmap_default: "arrows"
        ,EA_load_callback:"ScrollTop"

    });
    editAreaLoader.init({
        id: "jseditor"
        ,start_highlight: true
        ,font_size: "10"
        ,font_family: "verdana, monospace"
        ,allow_toggle: false
        ,language: "en"
        ,syntax: "js"
        ,toolbar: "save,|, search, |, undo, redo, |, select_font, |"
        ,save_callback: "jseditor_save"
        ,plugins: "charmap"
        ,charmap_default: "arrows"
        ,EA_load_callback:"ScrollTop"

    });

    if($("div#admin_LookFeel").hasClass('active')){
        CustomEditorInit = true;
    }


</script>