<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('DS', DIRECTORY_SEPARATOR);


if (!function_exists('_getConfig'))
{
  function _getConfig()
  {
    $CI =& get_instance();
    $CI->load->config('assets');
    $config = array();
    $config['path_base'] = $CI->config->item('path_base');
    $config['path_js']   = $CI->config->item('path_js');
    $config['path_css']  = $CI->config->item('path_css');
    $config['path_img']  = $CI->config->item('path_img');
    
    return $config;
  }
}

if (!function_exists('_process_array'))
{
  function _process_array($data, $type, $attr)
  {
    if(is_array($data))
    {
      $head = '';
      foreach($data as $parent)
      {
          $file = $parent;
          $attribute = '';
          if(!empty($attr) && is_array($attr))
          {

              foreach($attr as $key => $value)
              {
                  $attribute .= $key.'='.$value;
              }
          }
        $config = _getConfig();
        // Change path so the extensions can group their assets
        //$path = base_url($config['path_base'].DS.$config['path_'.$type].DS.$file);
        $path = base_url($config['path_base'].DS.$file);
      
        if($type == 'js')
          $head .= "\r\n".'<script type="text/javascript" src="' . $path . '?' . $attribute . '"></script>';
        else if($type == 'css')
          $head .= "\r\n".'<link rel="stylesheet" type="text/css" href="' . $path . '?' . $attribute . '"">';
        else if($type == 'img')
          $head .= '<img src="' . $path . '"'.$attribute.'/>';
      }

      return $head."\r\n";
    }
  }
}

if (!function_exists('_assets_base'))
{
  function _assets_base($file, $attr ,$type)
  {
    if(is_array($file))
    {
      return _process_array($file, $type, $attr);
    }
    else
    {
      if(!empty($attr) && is_array($attr))
      {
        $attribute = ' ';
        foreach($attr as $key => $value)
        {
          $attribute .= ' '.$key.'="'.$value.'"';
        }
      }
    
      $config = _getConfig();
      // Change path so the extensions can group their assets
      //$path = base_url($config['path_base'].DS.$config['path_'.$type].DS.$file);
      $path = base_url($config['path_base'].DS.$file);
    
      if($type == 'js')
        return "\r\n".'<script type="text/javascript" src="' . $path . '"' . $attribute . '></script>';
      else if($type == 'css')
        return "\r\n".'<link rel="stylesheet" type="text/css" href="' . $path . '"' . $attribute . '>';
      else if($type == 'img')
        return '<img src="' . $path . '"'.$attribute.'/>';
    }
  }
}

if (!function_exists('assets_css'))
{
  function assets_css($file, $attr = array())
  {
    return _assets_base($file, $attr, 'css');
  }
}

if (!function_exists('assets_js'))
{
  function assets_js($file, $attr = array())
  {
    return _assets_base($file, $attr, 'js');
  }
}

if (!function_exists('assets_img'))
{
  function assets_img($file, $attr = array())
  {
    return _assets_base($file, $attr, 'img');
  }
}
