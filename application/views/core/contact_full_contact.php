<?php
//$DVE = macanta_validate_feature('DVE');
$DVE = 'enabled';
$BlurClassCSS='dve-disabled';
$Disabled = 'disabled';
if($DVE && $DVE == "enabled"){
    $BlurClassCSS='';
    $Disabled = '';
}
$Avatar = $FullContact['avatar'];
$FullName = $FullContact['fullName'];
$Title = $FullContact['title'];
$At = $Title == "" ? "":"at";
$Location = $FullContact['location'];
$Gender = $FullContact['gender'];
$AgeRange = $FullContact['ageRange'];
$Organization = $FullContact['organization'];
$Bio = $FullContact['bio'];
$SocialIcons = 'https://dashboard.fullcontact.com/images/social-icons-28x28.svg?v=177c6191ab52634d1ea1ff9c1efce742';
$ContactIcons = 'https://dashboard.fullcontact.com/images/contact-icons-16x16.svg?v=11040e4143615647edd04849b9599a00';
$SocialMedia = [
    'linkedin'=>['pos'=>'0px 0px'],
    'twitter'=>['pos'=>'-28px 0px'],
    'facebook'=>['pos'=>'-56px 0px'],
    'gravatar'=>['pos'=>'-1092px 0px'],
    'google'=>['pos'=>'-140px 0px'],
    'klout'=>['pos'=>'-532px 0px']

];
?>
<div class="full-contact-preview">
    <div class="fc-card-header col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="fc-card-avatar col-xs-2 col-sm-2 col-md-2 col-lg-2  no-pad-left">
            <?php
            if($Avatar){ ?>
                <div
                        style="align-items: center;
                        color: rgb(255, 255, 255);
                        font-size: 30px;
                        width: 54px;
                        flex: 0 0 110px;
                        justify-content: center;
                        display: flex;
                        margin-right: 21px;
                        background-position: 50% center;
                        border-radius: 50%;
                        background-size: cover;
                        height: 54px;
                        background-image: url(&quot;<?php echo $Avatar; ?>&quot;);">
                </div>
            <?php }
            ?>
        </div>
        <div class="fc-card-title col-xs-10 col-sm-10 col-md-10 col-lg-10  no-pad-left no-pad-right">
            <div style="display: flex; flex-direction: column;">
                <div style="color: rgb(0, 0, 0);
                            font-size: 16px;
                            font-weight: 500;
                            line-height: 20px;">
                    <?php echo $FullName; ?>
                </div>
                <div class="<?php echo $BlurClassCSS; ?>" style="color: rgb(93, 103, 109);
                            font-size: 13px;
                            font-weight: 500;
                            line-height: 16px;
                            margin-top: 2px;">
                    <?php echo $Title; ?> <?php echo $At; ?> <?php echo $Organization; ?>
                </div>
            </div>
        </div>
        <div class="fc-card-social no-pad-left no-pad-right" style="display: inline-block; flex-wrap: wrap; float: left;">
            <?php
            foreach ($SocialMedia as $MediaName => $MediaProps){
                $href = '';
                if(isset($FullContact[$MediaName])){
                    $href = $FullContact[$MediaName];
                }
                if(isset($FullContact['details']['profiles'][$MediaName])){
                    $href = $FullContact['details']['profiles'][$MediaName]['url'];
                }
                if($href == '') continue;
                $href = $Disabled != '' ? '': 'href="'.$href.'"';
                ?>

                <a
                        class="icon-<?php echo $Disabled; ?> <?php echo $Disabled; ?>"
                        rel="noopener noreferrer" target="_blank" <?php echo $href; ?>
                        style="color: rgb(38, 156, 226); flex-grow: 0; width: 28px; cursor: pointer;
                                    background-image: url(&quot;<?php echo $SocialIcons; ?>&quot;);
                                text-decoration: none;
                                display: inline-block;
                                position: relative;
                                background-position: <?php echo $MediaProps['pos']; ?>;
                                height: 28px;
                                margin: 6px 6px 0px 0px;"  <?php echo $Disabled; ?>>

                </a>
            <?php }
            ?>

        </div>
        <div class="fc-card-loc no-pad-left no-pad-right <?php echo $BlurClassCSS; ?>"
             style="margin-top: 4px;
                    float: left;
                    display: inline-block;
                    margin-left: 15px;">
            <div style="font-size: 12px;
                        font-weight: 500;
                        line-height: 15px;
                        display: flex;
                        align-items: center;">
                <i style="background-image: url(&quot;<?php echo $ContactIcons; ?>&quot;);
                                background-size: 240px 47px;
                                background-position: -49px 0px;
                                height: 13px;
                                width: 12px;
                                margin-right: 8px;">

                </i>
                <?php echo $Location; ?>
            </div>
            <div style="font-size: 12px;
                        font-weight: 500;
                        line-height: 15px;
                        display: flex;
                        align-items: center;
                        margin-top: 2px;">
                <i style="background-image: url(&quot;<?php echo $ContactIcons; ?>&quot;); background-size: 256px 64px; background-position: -224px 0px; height: 16px; width: 16px; margin-right: 8px;"></i>
                <!-- react-text: 89 --><?php echo $Gender; ?>  / Age <?php echo $AgeRange; ?><!-- /react-text --></div>
        </div>
        <div class="fc-card-bio col-xs-12 col-sm-12 col-md-12 col-lg-12  no-pad-left no-pad-right" style="margin-top: 0px;">
            <div style="line-height: 14px;
                        align-items: center;
                        color: rgb(48, 57, 64);
                        font-size: 10px;
                        font-weight: 500;
                        background: rgb(236, 236, 236);
                        padding: 0px 10px;
                        display: flex;
                        letter-spacing: 1px;
                        border-radius: 2px;
                        height: 22px;">
                BIO
            </div>
            <div class="<?php echo $BlurClassCSS; ?>" style="color: rgb(48, 57, 64);
                        font-size: 11px;
                        line-height: 13px;
                        padding: 0px 0px 0px 11px;">
                <?php echo $Bio; ?>
            </div>
        </div>
    </div>
    <a class="showFullContact<?php echo $Disabled;?>">Click here to view detail</a>
    <a class=" fc-main-logo" href="https://fullcontact.com" target="_blank">powered by</a>
</div>

<!--Modal For FullContact Display-->
<div id="FullContactCard" class="modal fade FullContactCard" tabindex="-1" role="dialog"
     aria-labelledby="FullContactCard">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">FullContact</h4>
            </div>
            <div class="modal-body">
                <div style="background: rgb(255, 255, 255); border: 1px solid rgb(214, 218, 219); border-radius: 3px; flex: 1 1 0%; display: flex; flex-direction: column; width: 610px; max-width: 610px;">
                    <div style="display: flex; flex-direction: column; padding: 31px 31px 19px; position: relative;">
                        <div style="display: flex; align-items: center;">
                            <?php
                            if($Avatar){ ?>
                                <div
                                        style="align-items: center; color: rgb(255, 255, 255); font-size: 30px; width: 110px; flex: 0 0 110px;
                                    background-image: url(&quot;<?php echo $Avatar; ?>&quot;);
                                    justify-content: center; display: flex; margin-right: 21px; background-position: 50% center; border-radius: 50%; background-size: cover; height: 110px;">
                                </div>
                            <?php }
                            ?>
                            <div style="display: flex; flex-direction: column;">
                                <div style="color: rgb(25, 30, 33); font-size: 24px; font-weight: 500; line-height: 33px;">
                                    <?php echo $FullName; ?>
                                </div>
                                <div class="<?php echo $BlurClassCSS; ?>" style="color: rgb(93, 103, 109); font-size: 16px; font-weight: 500; line-height: 22px; margin-top: 2px;">
                                    <?php echo $Title; ?> <?php echo $At; ?> <?php echo $Organization; ?>
                                </div>
                            </div>
                        </div>
                        <div style="display: flex; flex-wrap: wrap; margin-top: 29px;">
                            <?php
                            foreach ($SocialMedia as $MediaName => $MediaProps){
                                $href = '';
                                if(isset($FullContact[$MediaName])){
                                    $href = $FullContact[$MediaName];
                                }
                                if(isset($FullContact['details']['profiles'][$MediaName])){
                                    $href = $FullContact['details']['profiles'][$MediaName]['url'];
                                }
                                if($href == '') continue;
                                $href = $Disabled != '' ? '': 'href="'.$href.'"';
                                ?>

                                <a class="icon-<?php echo $Disabled; ?> <?php echo $Disabled; ?>"
                                        rel="noopener noreferrer" target="_blank" <?php echo $href; ?>
                                        style="color: rgb(38, 156, 226); flex-grow: 0; width: 28px; cursor: pointer;
                                    background-image: url(&quot;<?php echo $SocialIcons; ?>&quot;);
                                    text-decoration: none;
                                    display: inline-block;
                                    position: relative;
                                    background-position: <?php echo $MediaProps['pos']; ?>;
                                    height: 28px;
                                    margin: 6px 6px 0px 0px;" <?php echo $Disabled; ?>>

                                </a>
                            <?php }
                            ?>

                        </div>
                        <div style="margin-top: 24px;"  class="<?php echo $BlurClassCSS; ?>" >
                            <div style="font-size: 15px; font-weight: 500; line-height: 20px; display: flex; align-items: center;">
                                <i
                                        style="background-image: url(&quot;<?php echo $ContactIcons; ?>&quot;);
                                        background-size: 256px 64px;
                                        background-position: -32px 0px; height: 16px; width: 16px; margin-right: 8px;">

                                </i>
                                <?php echo $Location; ?>
                            </div>
                            <div style="color:font-size: 15px; font-weight: 500; line-height: 20px; display: flex; align-items: center; margin-top: 11px;">
                                <i style="background-image: url(&quot;<?php echo $ContactIcons; ?>&quot;); background-size: 256px 64px; background-position: -224px 0px; height: 16px; width: 16px; margin-right: 8px;"></i>
                                <!-- react-text: 89 --><?php echo $Gender; ?>  / Age <?php echo $AgeRange; ?><!-- /react-text --></div>
                        </div>
                        <div style="margin-top: 20px;">
                            <div style="line-height: 14px; align-items: center; color: rgb(48, 57, 64); font-size: 10px; font-weight: 500; background: rgb(245, 246, 247); padding: 0px 10px; display: flex; letter-spacing: 1px; border-radius: 2px; height: 22px;">
                                BIO
                            </div>
                            <div class="<?php echo $BlurClassCSS; ?>"  style="color: rgb(48, 57, 64); font-size: 14px; line-height: 22px; padding: 10px 0px 0px 10px;">
                                <?php echo $Bio; ?>
                            </div>
                        </div>
                        <div style="display: flex; flex-direction: column; margin-top: 20px;">
                            <div style="line-height: 14px; align-items: center; color: rgb(48, 57, 64); font-size: 10px; font-weight: 500; background: rgb(245, 246, 247); padding: 0px 10px; display: flex; letter-spacing: 1px; border-radius: 2px; height: 22px;">
                                TOPICS
                            </div>
                            <div style="display: flex; flex-wrap: wrap; margin-top: 1px;">
                                <?php
                                foreach ($FullContact['details']['topics'] as $topics){ ?>
                                    <div class="<?php echo $BlurClassCSS; ?>"  style="border: 1px solid rgb(235, 236, 237); border-radius: 2px; font-size: 14px; line-height: 19px; margin: 9px 9px 0px 0px; padding: 5px 12px;">
                                        <?php echo $topics['name']; ?>
                                    </div>
                                <?php }
                                ?>
                            </div>
                        </div><?php
                        if(sizeof($FullContact['details']['interests']) > 0){ ?>
                            <div style="display: flex; flex-direction: column; margin-top: 20px;">
                                <div style="line-height: 14px; align-items: center; color: rgb(48, 57, 64); font-size: 10px; font-weight: 500; background: rgb(245, 246, 247); padding: 0px 10px; display: flex; letter-spacing: 1px; border-radius: 2px; height: 22px;">
                                    SOCIAL AFFINITIES
                                </div>
                                <div style="color: rgb(125, 134, 140); font-size: 14px; line-height: 19px; margin: 15px 0px 6px 12px;">
                                    <!--This is the $FullContact['details']['interests']-->
                                    <?php
                                    foreach ($FullContact['details']['interests'] as $interest){ ?>
                                        <div class="<?php echo $BlurClassCSS; ?>" style="border: 1px solid rgb(235, 236, 237); border-radius: 2px;  font-size: 14px; line-height: 19px; margin: 9px 9px 0px 0px; padding: 5px 12px;">
                                            <?php echo $interest['name']; ?>
                                        </div>
                                   <?php } ?>

                                </div>
                            </div>
                        <?php } ?>

                        <?php
                        if(sizeof($FullContact['details']['employment']) > 0){ ?>
                            <div style="margin-top: 20px;">
                                <div style="line-height: 14px; align-items: center; color: rgb(48, 57, 64); font-size: 10px; font-weight: 500; background: rgb(245, 246, 247); padding: 0px 10px; display: flex; letter-spacing: 1px; border-radius: 2px; height: 22px;">
                                    CURRENT JOBS
                                </div>
                                <div style="color: rgb(48, 57, 64); font-size: 14px; line-height: 22px;">
                                    <?php
                                    foreach ($FullContact['details']['employment'] as $employment){
                                        //For current Jobs
                                        if(!$employment['current']) continue;
                                        ?>
                                        <div class="<?php echo $BlurClassCSS; ?>"  style="display: flex; align-items: center; padding: 19px 0px; border-bottom: 1px solid rgb(235, 236, 237);">
                                            <i style="background-image: url(&quot;https://dashboard.fullcontact.com/images/contact-icons-16x16.svg?v=11040e4143615647edd04849b9599a00&quot;); background-size: 256px 64px; background-position: -48px 0px; height: 16px; width: 16px; margin: 0px 18px 0px 10px;"></i>
                                            <div style="display: flex; flex-direction: column;">
                                                <div style=" font-size: 15px; font-weight: 500; line-height: 20px;">
                                                    <?php echo $employment['title']; ?>
                                                </div>
                                                <div style=" font-size: 15px; line-height: 20px; margin-top: 1px;">
                                                    <?php echo $employment['name']; ?>
                                                </div>
                                                <div style=" font-size: 13px; line-height: 18px; margin-top: 2px;">
                                                    <?php echo date('M Y',strtotime($employment['start']['year'].'-'.$employment['start']['month'].'-00')); ?> - Present
                                                </div>
                                            </div>
                                        </div>
                                    <?php }
                                    ?>
                                    <?php
                                    foreach ($FullContact['details']['employment'] as $employment){
                                        //For past Jobs
                                        if($employment['current']) continue;
                                        ?>
                                        <div class="<?php echo $BlurClassCSS; ?>"  style="display: flex; align-items: center; padding: 19px 0px; border-bottom: 1px solid rgb(235, 236, 237);">
                                            <i style="background-image: url(&quot;https://dashboard.fullcontact.com/images/contact-icons-16x16.svg?v=11040e4143615647edd04849b9599a00&quot;); background-size: 256px 64px; background-position: -48px 0px; height: 16px; width: 16px; margin: 0px 18px 0px 10px;"></i>
                                            <div style="display: flex; flex-direction: column;">
                                                <div style="font-size: 15px; font-weight: 500; line-height: 20px;">
                                                    <?php echo $employment['title']; ?>
                                                </div>
                                                <div style="font-size: 15px; line-height: 20px; margin-top: 1px;">
                                                    <?php echo $employment['name']; ?>
                                                </div>
                                                <div style="font-size: 13px; line-height: 18px; margin-top: 2px;">
                                                    <?php echo date('M Y',strtotime($employment['start']['year'].'-'.$employment['start']['month'].'-00')); ?> - <?php echo date('M Y',strtotime($employment['end']['year'].'-'.$employment['end']['month'].'-00')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }
                                    ?>
                                </div>
                            </div>
                       <?php }
                        ?>

                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">Close
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    $('.icon-disabled, .showFullContactdisabled').on('click',function () {
        $( "#dve-disabled-dialog" ).dialog( "open" );
    })
    $( function() {
        $( "#dve-disabled-dialog" ).dialog({
            /*modal: true,*/
            autoOpen: false,
            width: 400,
            buttons: {
                Ok: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    } );

</script>
<?php
$appname = $this->config->item('MacantaAppName');
?>
<div id="dve-disabled-dialog" class="dve-disabled-dialog" title="Data Validation & Enrichment">
    <p class="dialog-content">Oops! Data Validation & Enrichment isn't enabled..
        <a href="https://macanta.org/add-data-validation-enrichment/?app_name=<?php echo $appname; ?>" target="_blank">[Click here]</a>
        to find out more, and to set it up.</p>
</div>