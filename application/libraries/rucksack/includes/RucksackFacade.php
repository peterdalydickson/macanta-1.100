<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 30/01/2017
 * Time: 3:36 PM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class RucksackFacade {
    public $CI;
    public $AppObjs = array();

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
        $this->CI->load->database();
        $this->CI->load->dbforge();

        $this->CI->dbforge->add_field('id');
        $this->CI->dbforge->add_field("`UserId` int(15) NOT NULL");
        $this->CI->dbforge->add_field("`Name` text");
        $this->CI->dbforge->add_field("`Params` text");
        $this->CI->dbforge->add_field("`Details` text");
        $this->CI->dbforge->add_field("`DateTaken` text");
        // CREATE TABLE IF NOT EXIST
        $this->CI->dbforge->create_table('rucksack_logs', TRUE);
        $this->iniApps();
    }
    public function iniApps(){
        foreach ($this->Applications as $App => $Master){
            require_once (dirname(__FILE__)."/integrations/".$App.".php");
            $this->AppObjs[$App] = new $App($this->CI);

        }
    }

    public function callMethod($Method, $Params, $refreshedToken = false){
        $masterResults = array();
        $results = array();
        foreach ($this->Applications as $App => $Options){
            if(method_exists($this->AppObjs[$App],$Method)){
                $results = $this->AppObjs[$App]->doMethod($Params, $Method, false, $refreshedToken);
                if($Options['master'] == true){
                    $masterResults = $results;
                }
            }else{
                continue;
            }

            // Record Logs To Database
            $this->logResults($App, $Method, $Params, $results);
            // Record Logs To File
            //$Logs = implode("\n",$results['Message']);
            //$Logs = "\n".date("F d, Y H:i:s")." [ $Method from $App ] Process..\n" . $Logs."\n==========================================";
            //file_put_contents(RUCKSACK_LIB . "logs/".$Method.".txt", $Logs, FILE_APPEND);
        }
        return $masterResults;
    }
    public function logResults($App, $Method, $Params, $masterResults){
        $this->MasterResults = $masterResults;
        $Details =  array();
        $WriteRecord = false;
        if($this->LogConfig["Messages"]){
            $WriteRecord = true;
            $Details["Messages"] = $masterResults['Message'];
        }
        if($this->LogConfig["Results"]){
            $WriteRecord = true;
            $Details["Results"] = $masterResults['Data'];
        }
        if($this->LogConfig["Errors"]){
            $WriteRecord = true;
            $Details["Errors"] = $masterResults['Errors'];
        };
        $DBdata = array(
            'UserId' => isset($_SESSION['InfusionsoftID']) ? $_SESSION['InfusionsoftID']:0,
            'Name' => $Method." [$App]",
            'Params' => $Params,
            'Details' => json_encode($Details),
            'DateTaken' => date("Y-m-d H:i:s")
        );
        $WriteRecord = false;
        if($WriteRecord){
            $this->CI->db->insert('rucksack_logs', $DBdata);
            $this->CI->db->where('DateTaken <=', date('Y-m-d H:i:s', strtotime("-7 Days")));
            $this->CI->db->delete('rucksack_logs');
        }else{
            // empty table if not empty
            $this->CI->db->empty_table('rucksack_logs');

        }

    }
}