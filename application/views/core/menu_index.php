<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 31/05/16
 * Time: 9:00 PM
 */
foreach($Menu as $MenuName => $Params){ ?>
       <li> <a href="<?php  echo $Params['params']["href"]; ?>"
               target="<?php  echo $Params['params']["target"]; ?>"
               class="<?php  echo $Params['params']["class"]; ?>"
               title="<?php  echo $Params['params']["title"]; ?>">
               <?php  echo $MenuName; ?>
           </a>
       </li>
<?php
}