
<small class="dev-only php">tab_admin_permissions-savedsearch.php</small>
<form id="SaveSearchTagPair" method="post" class="form-horizontal SaveSearchTagPair dynamic" _lpchecked="1">
    <ul class="itemContainer">
        <?php
        foreach($PairNames as $PairName){ ?>
            <li class="form-group-item">
                <div class="bullet-item">
                    <input type="text" class="FilterPairName" name="pairitem[]"  data-filterid ="<?php echo $PairName['filterid']; ?>" data-tagid="<?php echo $PairName['tagid']; ?>" placeholder="Type Here" value="<?php echo $PairName['name']; ?>" readonly>
                    <button type="button" class="nb-btn btn-default removeButton "><i class="fa fa-minus"></i></button>
                </div>

            </li>
       <?php }
        ?>
        <div id="saved"></div>
        <div id="dummy"></div>
        <li class="form-group-item">
            <div class="bullet-item">
                <input type="text" class="FilterPairName" name="pairitem[]" placeholder="Type Here">
                <button type="button" class="nb-btn nb-secondary nb-btn-icon-only icon-btn addButton "><i class="fa fa-plus"></i></button>
            </div>

        </li>

    </ul>
</form>
<script>
    var validOptions = <?php echo $ForAutocomplete;?>;
    var previousValue = "";
    $('form.SaveSearchTagPair.dynamic')
        ._once('click', '.addButton', function() {
            var isValid = false;
            var newClass = randomString(5);
            var template = $(this).parents(".form-group-item"),
                form  = $(this).parents('form');
            var selectVal = template.find('input').val();
            if(!selectVal) return false;
            for (i in validOptions) {
                if (validOptions[i] == selectVal) {
                    isValid = true;
                }
            }
            if (!isValid) {
                return false;
            }
            template.find('input').attr('readonly', true);
            template
                .clone()
                .removeClass('hide')
                .removeAttr('id')
                .insertBefore($('#dummy', form))
                .find('button.addButton').addClass('removeButton').removeClass('addButton')
                .find('i.glyphicon-plus').addClass('glyphicon-minus').removeClass('glyphicon-plus')
            form.find(".form-group-item:visible:last").find('input').val('');
            form.find(".form-group-item:visible:last").find('input').removeAttr('readonly');
            form.find(".form-group-item:visible:last").find('input:visible:first').focus();

        })
        ._once('keydown', 'input', function(event) {
            var isValid = false;
            if (event.keyCode == 13) {
                var form  = $(this).parents('form');
                var template = $(this).parents(".form-group-item");
                var selectVal = template.find('input').val();
                if(!selectVal) return false;

                for (i in validOptions) {
                    if (validOptions[i] == selectVal) {
                        isValid = true;
                    }
                }
                if (!isValid) {
                    return false;
                }
                template.find('input').attr('readonly', true);
                template
                    .clone()
                    .removeClass('hide')
                    .removeAttr('id')
                    .insertBefore($('#dummy', form))
                    .find('button.addButton').addClass('removeButton').removeClass('addButton')
                    .find('i.glyphicon-plus').addClass('glyphicon-minus').removeClass('glyphicon-plus');
                form.find(".form-group-item:visible:last").find('input').val('');
                form.find(".form-group-item:visible:last").find('input').removeAttr('readonly');
                form.find(".form-group-item:visible:last").find('input:visible:first').focus();

            }
        })
        ._once('click', '.removeButton', function() {
            var $row  = $(this).parents('.form-group-item'),
                $form  = $(this).parents('form');
            $row.remove();

        });
    function SaveSearchAutoCompleteDestroy() {
        $( "input.FilterPairName").autocomplete( "destroy" );
    }
    function SaveSearchAutoComplete() {
        $( "input.FilterPairName" ).autocomplete({
            source: validOptions,
            select: function(event, ui) {
                var label = ui.item.label;
                var value = ui.item.value;
                $(this).val(value);
                var e = jQuery.Event("keypress");
                e.which = 13;
                $(this).trigger(e);

            }
        }).keyup(function() {
            var isValid = false;
            for (i in validOptions) {
                if (validOptions[i].toLowerCase().match(this.value.toLowerCase())) {
                    isValid = true;
                }
            }
            if (!isValid) {
                this.value = previousValue
            } else {
                previousValue = this.value;
            }
        });
    }
    SaveSearchAutoComplete();
    $(function() {
        $(document)
            .on('click','button.refreshSavedSearch', function () {
                var jsonData = {"controler":"core/tabs/admin","action":"refreshSavedSearchTag","session_name":session_name,"data":{}};
                var successFn = function(e){
                    if(typeof e === 'object'){
                        console.log('Saved Search refreshed');
                        eval(e.script);
                    }
                }
                ajaxRequester('right-Permissions', 'admin-panelBody-savedsearch', jsonData, successFn);
            })
    });
</script>
<button class="nb-btn nb-primary icon-btn save-filter-pair" onclick="saveSearchFilterTagPairs();"><i class="fa fa-save"></i>Save</button>