<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 02/03/16
 * Time: 6:59 PM
 */

class Siteconfig extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }
    public function get_all()
    {
        return $this->db->get('config_data');
    }
    public function update_config($data)
    {
        $success = true;
        foreach($data as $key=>$value)
        {
            if(!$this->save($key,$value))
            {
                $success=false;
                break;
            }
        }
        return $success;
    }
    public function add($key,$value)
    {
        $config_data=array(
            'key'=>$key,
            'value'=>$value
        );
        $this->db->where('key', $key);
        $query = $this->db->get('config_data');

        if(sizeof($query->result()) > 0){
            return "Failed: Setting Item ($key) Already Existing";
        }else{
            $results = $this->db->insert('config_data',$config_data);
            return "Success: $results";
        }

    }
    public function save($key,$value)
    {
        $config_data=array(
            'key'=>$key,
            'value'=>$value
        );

        $this->db->where('key', $key);
        return $this->db->update('config_data',$config_data);
    }

}