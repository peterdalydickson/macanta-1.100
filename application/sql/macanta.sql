-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `InfusionsoftStageMove`;
CREATE TABLE `InfusionsoftStageMove` (
  `CreatedBy` int(11) NOT NULL,
  `DateCreated` text NOT NULL,
  `Id` int(11) NOT NULL,
  `MoveDate` text NOT NULL,
  `MoveFromStage` int(11) NOT NULL,
  `MoveToStage` int(11) NOT NULL,
  `OpportunityId` int(11) NOT NULL,
  `PrevStageMoveDate` text NOT NULL,
  `UserId` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `campaign_requests`;
CREATE TABLE `campaign_requests` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `message` text,
  `date_received` datetime DEFAULT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'ready',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `config_data`;
CREATE TABLE `config_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(100) NOT NULL,
  `value` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `connected_data`;
CREATE TABLE `connected_data` (
  `id` varchar(100) NOT NULL,
  `group` varchar(100) NOT NULL,
  `value` longtext,
  `connected_contact` longtext,
  `history` longtext,
  `meta` longtext,
  `created` longtext,
  `status` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `connected_data_history`;
CREATE TABLE `connected_data_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` varchar(100) NOT NULL,
  `time` varchar(100) NOT NULL,
  `update_by` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `data` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


SET NAMES utf8mb4;

DROP TABLE IF EXISTS `InfusionsoftCompany`;
CREATE TABLE `InfusionsoftCompany` (
  `AccountId` int(11) NOT NULL,
  `Address1Type` text NOT NULL,
  `Address2Street1` text NOT NULL,
  `Address2Street2` text NOT NULL,
  `Address2Type` text NOT NULL,
  `Address3Street1` text NOT NULL,
  `Address3Street2` text NOT NULL,
  `Address3Type` text NOT NULL,
  `Anniversary` date NOT NULL,
  `AssistantName` text NOT NULL,
  `AssistantPhone` text NOT NULL,
  `BillingInformation` text NOT NULL,
  `Birthday` date NOT NULL,
  `City` text NOT NULL,
  `City2` text NOT NULL,
  `City3` text NOT NULL,
  `Company` text NOT NULL,
  `CompanyID` int(11) NOT NULL,
  `ContactNotes` longtext NOT NULL,
  `ContactType` text NOT NULL,
  `Country` text NOT NULL,
  `Country2` text NOT NULL,
  `Country3` text NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `DateCreated` datetime NOT NULL,
  `Email` text NOT NULL,
  `EmailAddress2` text NOT NULL,
  `EmailAddress3` text NOT NULL,
  `Fax1` text NOT NULL,
  `Fax1Type` text NOT NULL,
  `Fax2` text NOT NULL,
  `Fax2Type` text NOT NULL,
  `FirstName` text NOT NULL,
  `Groups` text NOT NULL,
  `Id` int(11) NOT NULL,
  `JobTitle` text NOT NULL,
  `LastName` text NOT NULL,
  `LastUpdated` datetime NOT NULL,
  `LastUpdatedBy` int(11) NOT NULL,
  `Password` text NOT NULL,
  `Phone2` text NOT NULL,
  `Phone1Ext` text NOT NULL,
  `Phone1` text NOT NULL,
  `MiddleName` text NOT NULL,
  `OwnerID` int(11) NOT NULL,
  `Nickname` text NOT NULL,
  `Phone2Type` text NOT NULL,
  `Phone2Ext` text NOT NULL,
  `PostalCode` text NOT NULL,
  `Phone1Type` text NOT NULL,
  `Phone3Ext` text NOT NULL,
  `PostalCode2` text NOT NULL,
  `Phone3` text NOT NULL,
  `Phone5Ext` text NOT NULL,
  `Phone5` text NOT NULL,
  `Phone5Type` text NOT NULL,
  `Phone4Ext` text NOT NULL,
  `Phone4` text NOT NULL,
  `Phone3Type` text NOT NULL,
  `Phone4Type` text NOT NULL,
  `PostalCode3` text NOT NULL,
  `ReferralCode` text NOT NULL,
  `SpouseName` text NOT NULL,
  `State` text NOT NULL,
  `State2` text NOT NULL,
  `State3` text NOT NULL,
  `StreetAddress1` text NOT NULL,
  `StreetAddress2` text NOT NULL,
  `Suffix` text NOT NULL,
  `Title` text NOT NULL,
  `Username` text NOT NULL,
  `Validated` text NOT NULL,
  `Website` text NOT NULL,
  `ZipFour1` text NOT NULL,
  `ZipFour2` text NOT NULL,
  `ZipFour3` text NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `InfusionsoftContact`;
CREATE TABLE `InfusionsoftContact` (
  `AccountId` int(11) NOT NULL,
  `Address1Type` text NOT NULL,
  `Address2Street1` text NOT NULL,
  `Address2Street2` text NOT NULL,
  `Address2Type` text NOT NULL,
  `Address3Street1` text NOT NULL,
  `Address3Street2` text NOT NULL,
  `Address3Type` text NOT NULL,
  `Anniversary` date NOT NULL,
  `AssistantName` text NOT NULL,
  `AssistantPhone` text NOT NULL,
  `BillingInformation` text NOT NULL,
  `Birthday` date NOT NULL,
  `City` text NOT NULL,
  `City2` text NOT NULL,
  `City3` text NOT NULL,
  `Company` text NOT NULL,
  `CompanyID` int(11) NOT NULL,
  `ContactNotes` longtext NOT NULL,
  `ContactType` text NOT NULL,
  `Country` text NOT NULL,
  `Country2` text NOT NULL,
  `Country3` text NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `DateCreated` datetime NOT NULL,
  `Email` text NOT NULL,
  `EmailAddress2` text NOT NULL,
  `EmailAddress3` text NOT NULL,
  `Fax1` text NOT NULL,
  `Fax1Type` text NOT NULL,
  `Fax2` text NOT NULL,
  `Fax2Type` text NOT NULL,
  `FirstName` text NOT NULL,
  `Groups` text NOT NULL,
  `Id` int(11) NOT NULL,
  `JobTitle` text NOT NULL,
  `LastName` text NOT NULL,
  `LastUpdated` datetime NOT NULL,
  `LastUpdatedBy` int(11) NOT NULL,
  `Password` text NOT NULL,
  `Phone2` text NOT NULL,
  `Phone1Ext` text NOT NULL,
  `Phone1` text NOT NULL,
  `MiddleName` text NOT NULL,
  `OwnerID` int(11) NOT NULL,
  `Nickname` text NOT NULL,
  `Phone2Type` text NOT NULL,
  `Leadsource` text NOT NULL,
  `LeadSourceId` int(11) NOT NULL,
  `Phone2Ext` text NOT NULL,
  `PostalCode` text NOT NULL,
  `Phone1Type` text NOT NULL,
  `Phone3Ext` text NOT NULL,
  `PostalCode2` text NOT NULL,
  `Phone3` text NOT NULL,
  `Phone5Ext` text NOT NULL,
  `Phone5` text NOT NULL,
  `Phone5Type` text NOT NULL,
  `Phone4Ext` text NOT NULL,
  `Phone4` text NOT NULL,
  `Phone3Type` text NOT NULL,
  `Phone4Type` text NOT NULL,
  `PostalCode3` text NOT NULL,
  `ReferralCode` text NOT NULL,
  `SpouseName` text NOT NULL,
  `State` text NOT NULL,
  `State2` text NOT NULL,
  `State3` text NOT NULL,
  `StreetAddress1` text NOT NULL,
  `StreetAddress2` text NOT NULL,
  `Suffix` text NOT NULL,
  `Title` text NOT NULL,
  `Username` text NOT NULL,
  `Validated` text NOT NULL,
  `Website` text NOT NULL,
  `ZipFour1` text NOT NULL,
  `ZipFour2` text NOT NULL,
  `ZipFour3` text NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `InfusionsoftContactAction`;
CREATE TABLE `InfusionsoftContactAction` (
  `Accepted` int(11) NOT NULL,
  `ActionDate` datetime NOT NULL,
  `ActionDescription` text NOT NULL,
  `ActionType` text NOT NULL,
  `CompletionDate` datetime NOT NULL,
  `ContactId` int(11) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreationDate` datetime NOT NULL,
  `CreationNotes` text NOT NULL,
  `EndDate` datetime NOT NULL,
  `Id` int(11) NOT NULL,
  `IsAppointment` int(11) NOT NULL,
  `LastUpdated` datetime NOT NULL,
  `LastUpdatedBy` int(11) NOT NULL,
  `Location` text NOT NULL,
  `ObjectType` enum('Note','Task','Appointment') NOT NULL,
  `OpportunityId` int(11) NOT NULL,
  `PopupDate` datetime NOT NULL,
  `Priority` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `InfusionsoftContactGroup`;
CREATE TABLE `InfusionsoftContactGroup` (
  `GroupCategoryId` int(11) NOT NULL,
  `GroupDescription` text NOT NULL,
  `GroupName` text NOT NULL,
  `Id` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `InfusionsoftContactGroupCategory`;
CREATE TABLE `InfusionsoftContactGroupCategory` (
  `CategoryDescription` text NOT NULL,
  `CategoryName` text NOT NULL,
  `Id` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `InfusionsoftDataFormField`;
CREATE TABLE `InfusionsoftDataFormField` (
  `DataType` int(11) NOT NULL,
  `DefaultValue` text NOT NULL,
  `FormId` int(11) NOT NULL,
  `GroupId` int(11) NOT NULL,
  `Id` int(11) NOT NULL,
  `Label` text NOT NULL,
  `ListRows` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Values` text NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `InfusionsoftDataFormFieldValue`;
CREATE TABLE `InfusionsoftDataFormFieldValue` (
  `Id` int(11) NOT NULL,
  `ContactId` int(11) NOT NULL,
  `DataFormFieldId` int(11) NOT NULL,
  `Value` text NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `InfusionsoftDataFormGroup`;
CREATE TABLE `InfusionsoftDataFormGroup` (
  `Id` int(11) NOT NULL,
  `Name` text NOT NULL,
  `TabId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `InfusionsoftEmailAddStatus`;
CREATE TABLE `InfusionsoftEmailAddStatus` (
  `DateCreated` datetime NOT NULL,
  `Email` text NOT NULL,
  `Id` int(11) NOT NULL,
  `LastClickDate` datetime NOT NULL,
  `LastOpenDate` datetime NOT NULL,
  `LastSentDate` datetime NOT NULL,
  `Type` text NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `InfusionsoftFileBox`;
CREATE TABLE `InfusionsoftFileBox` (
  `ContactId` int(11) NOT NULL,
  `Extension` text NOT NULL,
  `FileName` text NOT NULL,
  `FileSize` longtext NOT NULL,
  `Id` int(11) NOT NULL,
  `Public` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `InfusionsoftInvoice`;
CREATE TABLE `InfusionsoftInvoice` (
  `AffiliateId` int(11) NOT NULL,
  `ContactId` int(11) NOT NULL,
  `CreditStatus` int(11) NOT NULL,
  `DateCreated` datetime NOT NULL,
  `Description` text NOT NULL,
  `Id` int(11) NOT NULL,
  `InvoiceTotal` double NOT NULL,
  `InvoiceType` text NOT NULL,
  `JobId` int(11) NOT NULL,
  `LastUpdated` datetime NOT NULL,
  `LeadAffiliateId` int(11) NOT NULL,
  `PayPlanStatus` int(11) NOT NULL,
  `PayStatus` int(11) NOT NULL,
  `ProductSold` text NOT NULL,
  `PromoCode` text NOT NULL,
  `RefundStatus` int(11) NOT NULL,
  `Synced` int(11) NOT NULL,
  `TotalDue` double NOT NULL,
  `TotalPaid` double NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `InfusionsoftLead`;
CREATE TABLE `InfusionsoftLead` (
  `AffiliateId` int(11) NOT NULL,
  `ContactID` int(11) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `DateCreated` datetime NOT NULL,
  `DateInStage` datetime NOT NULL,
  `EstimatedCloseDate` datetime NOT NULL,
  `Id` int(11) NOT NULL,
  `IncludeInForecast` int(11) NOT NULL,
  `LastUpdated` datetime NOT NULL,
  `LastUpdatedBy` int(11) NOT NULL,
  `Leadsource` text NOT NULL,
  `MonthlyRevenue` double NOT NULL,
  `NextActionDate` datetime NOT NULL,
  `NextActionNotes` text NOT NULL,
  `Objection` text NOT NULL,
  `OpportunityNotes` text NOT NULL,
  `OpportunityTitle` text NOT NULL,
  `OrderRevenue` double NOT NULL,
  `ProjectedRevenueHigh` double NOT NULL,
  `ProjectedRevenueLow` double NOT NULL,
  `StageID` int(11) NOT NULL,
  `StatusID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `InfusionsoftLeadSource`;
CREATE TABLE `InfusionsoftLeadSource` (
  `CostPerLead` text NOT NULL,
  `Description` text NOT NULL,
  `EndDate` date NOT NULL,
  `Id` int(11) NOT NULL,
  `LeadSourceCategoryId` int(11) NOT NULL,
  `Medium` text NOT NULL,
  `Message` text NOT NULL,
  `Name` text NOT NULL,
  `StartDate` date NOT NULL,
  `Status` text NOT NULL,
  `Vendor` text NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `InfusionsoftSavedFilter`;
CREATE TABLE `InfusionsoftSavedFilter` (
  `FilterName` text NOT NULL,
  `Id` int(11) NOT NULL,
  `ReportStoredName` text NOT NULL,
  `UserId` text NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `InfusionsoftStage`;
CREATE TABLE `InfusionsoftStage` (
  `Id` int(11) NOT NULL,
  `StageName` text NOT NULL,
  `StageOrder` int(11) NOT NULL,
  `TargetNumDays` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `InfusionsoftTemplate`;
CREATE TABLE `InfusionsoftTemplate` (
  `Categories` text NOT NULL,
  `Id` int(11) NOT NULL,
  `PieceTitle` text NOT NULL,
  `PieceType` text NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `InfusionsoftUser`;
CREATE TABLE `InfusionsoftUser` (
  `City` text NOT NULL,
  `Email` text NOT NULL,
  `EmailAddress2` text NOT NULL,
  `EmailAddress3` text NOT NULL,
  `FirstName` text NOT NULL,
  `GlobalUserId` int(11) NOT NULL,
  `HTMLSignature` text NOT NULL,
  `Id` int(11) NOT NULL,
  `LastName` text NOT NULL,
  `MiddleName` text NOT NULL,
  `Nickname` text NOT NULL,
  `Partner` text NOT NULL,
  `Phone1` text NOT NULL,
  `Phone1Ext` text NOT NULL,
  `Phone1Type` text NOT NULL,
  `Phone2` text NOT NULL,
  `Phone2Ext` text NOT NULL,
  `Phone2Type` text NOT NULL,
  `PostalCode` text NOT NULL,
  `Signature` text NOT NULL,
  `SpouseName` text NOT NULL,
  `State` text NOT NULL,
  `StreetAddress1` text NOT NULL,
  `StreetAddress2` text NOT NULL,
  `Suffix` text NOT NULL,
  `Title` text NOT NULL,
  `ZipFour1` text NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `note_tags`;
CREATE TABLE `note_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note_id` varchar(100) NOT NULL,
  `tag_slugs` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `rucksack_logs`;
CREATE TABLE `rucksack_logs` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `UserId` int(15) NOT NULL,
  `Name` text,
  `Params` text,
  `Details` text,
  `DateTaken` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_slug` varchar(100) NOT NULL,
  `tag_name` varchar(100) DEFAULT NULL,
  `contact_ids` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `users_meta`;
CREATE TABLE `users_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(100) NOT NULL,
  `meta_key` varchar(100) NOT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `user_sessions`;
CREATE TABLE `user_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `infusionsoft_id` int(11) NOT NULL,
  `login_email` text,
  `last_access` int(11) NOT NULL,
  `session_name` text,
  `session_started` int(11) NOT NULL,
  `session_ttl` int(11) NOT NULL DEFAULT '86400',
  `session_data` text,
  `recent_actions` varchar(500) NOT NULL DEFAULT '[]',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2018-05-11 00:12:08
