<!--Modal For Adding Contact -->
<?php
//$DVE = macanta_validate_feature('DVE');
$DVE = 'enabled';
if($DVE && $DVE == "enabled"){
    $DVE = true;
}else{
    $DVE = false;
}
$No = 'No';
$DisableThis = '';
if(!$DVE){
    $No = 'Unable';
    $DisableThis = 'Unable';
}
if($Phone1){
    $Phone1Info = macanta_validate_phone_number($Id,$Phone1,$Country,$session_data);
    $Phone1Stat = $Phone1Info['IsValid'];
}else{
    $Phone1Stat = $No;
}

if($Phone2){
    $Phone2Info = macanta_validate_phone_number($Id,$Phone2,$Country,$session_data);
    $Phone2Stat = $Phone2Info['IsValid'];
}else{
    $Phone2Stat = $No;
}
if($Email){
    $EmailInfo = macanta_validate_email($Email,$session_data);
    if($EmailInfo['format_valid']){
        $EmailStat = $EmailInfo['format_valid'] == 1 ? "Yes":$EmailInfo['format_valid'];
    }else{
        $EmailStat = $No;
    }
}else{
    $EmailStat = $No;
}
//$stripe_customer_status = macanta_get_stripe_customer_status();
//$stripe_customer_status_arr = json_decode($stripe_customer_status, true);
$appname = $this->config->item('MacantaAppName');
$messageVerify = '<a href="https://macanta.org/add-data-validation-enrichment/?app_name='.$appname.'" target="_blank">Oops! Automated address verification has not yet been enabled. 
    <b>Click here</b> to find out about contact data validation and enrichment.</a>';
$messageEdit = '<a href="https://macanta.org/add-data-validation-enrichment/?app_name='.$appname.'"  target="_blank">Would you like to use this automagical address finder?! 
    <b>Click here</b> to find out about the new Data Validation & Enrichment add-on for Macanta. Or click  \'Manual Edit\' below .</a>';
?>
<div id="AddContact" class="modal fade ContactDetails" tabindex="-1" role="dialog" aria-labelledby="ContactDetailsLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" name="ContactDetailsFormAdd" id="ContactDetailsFormAdd" onsubmit="addIScontact(this); return false;" >
                <input  type="hidden" name="AddressBillingAdd" value="">
                <input  type="hidden" name="AddressShippingAdd" value="">

                <div class="modal-header">
                <h4 class="modal-title">Add Contact</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="flex-row">
                    <div class="user-info-b col-half MainInfoAdd">
                        <div class="MainInfoAddHeader label-lg">General Information</div>

                        <ul class="user-info">
                            <li class="info-item ">
                                <span class="label">
                                    <label class="label-sm">
                                        <?php echo $this->lang->line('text_firstname'); ?><span class="required">*</span>
                                    </label>
                                    <input name="FirstName" class="FirstName" value="<?php echo $FirstName; ?>" required>
                                </span>
                            </li>
                            <li class="info-item ">
                                <span class="label">
                                    <label class="label-sm">
                                        <?php echo $this->lang->line('text_lastname'); ?>
                                    </label>
                                    <input name="LastName" class="LastName" value="<?php echo $LastName; ?>">
                                </span>
                            </li>
                            <li class="info-item EmailAddContainerAdd">
                                <div class="macanta-confirm-info EmailAddConfirm" data-target="EmailAdd" data-value="" style="display: none;">
                                    Do you mean: <span class="message"></span> ? <a class="btn btn-macanta-confirm-info yes"> Yes</a> / <a class="btn btn-macanta-confirm-info no"> No</a>
                                </div>
                                <div class="macanta-notice-info EmailAddStatAdd " style="display: none;">
                                    <?php
                                    $Format = $EmailInfo['format_valid'] ? 'Valid Format':'Invalid Format';
                                    $SMTPCheck = $EmailInfo['smtp_check'] ? 'Passed':'Failed';
                                    switch ($EmailStat){
                                        case 'Yes':
                                            echo '<ul class="macanta-notice-ul">
                                        <li><span>Domain:</span> '.$EmailInfo['domain'].'</li>
                                        <li><span>Format:</span> '. $Format .'</li>
                                        <li><span>SMTP Check:</span> '. $SMTPCheck .'</li>
                                        <li><span>Score:</span> '.$EmailInfo['score'].'</li>
                                      </ul>';
                                            break;
                                        case 'No':
                                            echo '<ul class="macanta-notice-ul">
                                        <li><span>Status:</span> Not A Valid Email Address</li>
                                      </ul>';
                                            break;
                                        case 'Unable':
                                            echo '<ul class="macanta-notice-ul">
                                        <li><span style="color: #fe7b7f;font-weight: 600;text-shadow: 0px 0px 1px #000;">Verification Disabled</span></li>
                                      </ul>';
                                            break;
                                    }
                                    ?>
                                </div>
                                <span class="label">
                                    <label class="label-sm">
                                        <span>
                                            <?php echo $this->lang->line('text_email'); ?>
                                        </span>
                                        <i data-show="EmailAddStatAdd" class="fa fa-envelope-square show-notice IsEmailAddValidAdd IsEmailAddValid<?php echo $EmailStat; ?>"></i>
                                    </label>
                                    <input name="Email" data-type="Add" class="EmailAdd" data-original="<?php echo $Email; ?>" value="<?php echo $Email; ?>">
                                    <?php
                                    //Email icon should not be green when $SMTPCheck is Failed
                                    $EmailStat = $SMTPCheck == 'Passed' ? $EmailStat:$No
                                    ?>
                                    
                                </span>
                            </li>
                            <li class="info-item withPermission">
                                <input  type="checkbox" name="marketing" class="marketing" >
                                <span class="label label-checkbox">I have <span class="permissionInfo">permission</span> to send marketing to this address.</span>
                            </li>
                            <li class="info-item ">
                                <span class="label">
                                    <label class="label-sm">
                                        Title
                                    </label>
                                    <input name="JobTitle" class="JobTitle" value="<?php echo $JobTitle; ?>">
                                </span>
                            </li>
                            <li class="info-item ">
                                <span class="label">
                                    <label class="label-sm">
                                        <?php echo $this->lang->line('text_company'); ?>
                                    </label>
                                    <input name="Company" class="Company" value="<?php echo $Company; ?>">
                                </span>
                            </li>
                            <li class="info-item Phone1Container">
                                <div class="macanta-notice-info PhoneStatAdd Phone1StatAdd" style="display: none;">
                                    <?php
                                    switch ($Phone1Stat){
                                        case 'Yes':
                                            echo '<ul class="macanta-notice-ul">
                                        <li><span>Status:</span> Valid Phone Number</li>
                                        <li><span>PhoneNumber:</span> '.$Phone2Info['PhoneNumber'].'</li>
                                        <li><span>NetworkName:</span> '.$Phone1Info['NetworkName'].'</li>
                                        <li><span>NetworkCode:</span> '.$Phone1Info['NetworkCode'].'</li>
                                        <li><span>NetworkCountry:</span> '.$Phone1Info['NetworkCountry'].'</li>
                                        <li><span>NationalFormat:</span> '.$Phone1Info['NationalFormat'].'</li>
                                        <li><span>CountryPrefix:</span> '.$Phone1Info['CountryPrefix'].'</li>
                                        <li><span>NumberType:</span> '.$Phone1Info['NumberType'].'</li>
                                      </ul>';
                                            break;
                                        case 'No':
                                            echo '<ul class="macanta-notice-ul">
                                        <li><span>Status:</span> Not A Valid Phone Number</li>
                                      </ul>';
                                            break;
                                        case 'Unable':
                                            echo '<ul class="macanta-notice-ul">
                                        <li><span style="color: #fe7b7f;font-weight: 600;text-shadow: 0px 0px 1px #000;">Verification Disabled</span></li>
                                      </ul>';
                                            break;
                                    }
                                    ?>
                                </div>
                                <span class="label">
                                    <label class="label-sm">
                                        <span>
                                            <?php echo $this->lang->line('text_phone'); ?> 1
                                        </span>
                                        <i data-show="Phone1StatAdd" class="fa fa-phone-square show-notice IsPhoneValidAdd IsPhone1ValidAdd IsPhoneValid<?php echo $Phone1Stat; ?>"></i>
                                    </label>
                                    <input name="Phone1" data-type="Add" class="Phone1" value="<?php echo $Phone1; ?>"> 
                                </span>
                            </li>
                            <li class="info-item Phone2Container">
                                <div class="macanta-notice-info PhoneStatAdd Phone2StatAdd" style="display: none;">
                                    <?php
                                    switch ($Phone2Stat){
                                        case 'Yes':
                                            echo '<ul class="macanta-notice-ul">
                                        <li><span>Status:</span> Valid Phone Number</li>
                                        <li><span>PhoneNumber:</span> '.$Phone2Info['PhoneNumber'].'</li>
                                        <li><span>NetworkName:</span> '.$Phone2Info['NetworkName'].'</li>
                                        <li><span>NetworkCode:</span> '.$Phone2Info['NetworkCode'].'</li>
                                        <li><span>NetworkCountry:</span> '.$Phone2Info['NetworkCountry'].'</li>
                                        <li><span>NationalFormat:</span> '.$Phone2Info['NationalFormat'].'</li>
                                        <li><span>CountryPrefix:</span> '.$Phone2Info['CountryPrefix'].'</li>
                                        <li><span>NumberType:</span> '.$Phone2Info['NumberType'].'</li>
                                      </ul>';
                                            break;
                                        case 'No':
                                            echo '<ul class="macanta-notice-ul">
                                        <li><span>Status:</span> Not A Valid Phone Number</li>
                                      </ul>';
                                            break;
                                        case 'Unable':
                                            echo '<ul class="macanta-notice-ul">
                                        <li><span style="color: #fe7b7f;font-weight: 600;text-shadow: 0px 0px 1px #000;">Verification Disabled</span></li>
                                      </ul>';
                                            break;
                                    }
                                    ?>
                                </div>
                                <span class="label">
                                    <label class="label-sm">
                                        <span>
                                            <?php echo $this->lang->line('text_phone'); ?> 2
                                        </span>
                                        <i data-show="Phone2StatAdd" class="fa fa-phone-square show-notice IsPhoneValidAdd IsPhone2ValidAdd IsPhoneValid<?php echo $Phone2Stat; ?>"></i>
                                    </label>
                                    <input name="Phone2" data-type="Add" class="Phone2" value="<?php echo $Phone2; ?>">
                                </span>
                            </li>
                        </ul>
                    </div>
                    <div class="user-info-b col-half">
                        <div class="AddressSliderAdd">
                            <div class="AddressSliderContainerAdd">
                                <div class="AddressBillingAdd">

                                    <div class="switchAddressAdd">
                                        <div class="flex-row">
                                            <div class="">
                                                <i class="fa fa-address-card-o AddressStatus  <?php echo $DisableThis; ?> <?php echo checkVerifiedAddress($Id,"AddressBillingEdit") == true ? "ValidatedAddress":"InValidatedAddress"?>"
                                                        title="<?php echo checkVerifiedAddress($Id,"AddressBillingEdit") == true ? "Address Verified":"Address Not Verified"?>">
                                                </i>
                                            </div>
                                            <div class="">
                                                <label class="label-sm">Billing Address</label>
                                                <span class="show-other-address btn "><?php echo $this->lang->line('text_show_shipping_instead'); ?></span>
                                            </div>
                                        </div><!-- /.flex-row -->

                                        <div class="AddressBillingAddLoqateContainer " style="display: none">
                                            <input
                                                    class="LoqateField"
                                                    type="text"
                                                    data-addresstype="AddressBillingAdd"
                                                    name="AddressBillingAddLoqate"
                                                    id="AddressBillingAddLoqate"
                                                    placeholder="please enter your address..."
                                                <?php echo $DVE ? "":"disabled"; ?>
                                            >
                                            <?php echo $DVE ? '':'<span class="notice-alert">'.$messageEdit.'</span>'; ?>
                                            <a  id="btnAddressBillingAddManual"  data-addresstype="AddressBillingAdd" class="btn btnEditLink AddressBillingAddManual AddressAddManual">Manual Add</a>
                                            <ul class="AddressMainList">

                                            </ul>
                                        </div>
                                    </div>
                                    <ul class="user-info contactView">

                                    </ul>
                                    <div class="btn-toolbar address-edit-btn">
                                        <a  id="btnAddressBillingLoqateAdd" data-addresstype="AddressBillingAdd" class="btn btnEditLink AddressBillingAddLoqate ShowAddressAddLoqate">Edit Address</a>
                                        <?php
                                        if(!$DVE){ ?>
                                            <div class="DVE-alert " style="display: none">
                                                <?php echo $messageVerify; ?>
                                            </div>
                                        <?php }
                                        ?>
                                    </div>
                                </div>
                                <div class="AddressShippingAdd">
                                    <div class="switchAddressAdd">
                                        <div class="flex-row">
                                            <div class="">
                                                <i
                                                        class="fa fa-address-card-o AddressStatus  <?php echo $DisableThis; ?>  <?php echo checkVerifiedAddress($Id,"AddressShippingAdd") == true ? "ValidatedAddress":"InValidatedAddress"?>"
                                                        title="<?php echo checkVerifiedAddress($Id,"AddressShippingAdd") == true ? "Address Verified":"Address Not Verified"?>">

                                                </i>
                                            </div>
                                            <div class="">
                                                <label class="label-sm">Shipping Address</label>
                                                <span class="show-other-address btn "><?php echo $this->lang->line('text_show_billing_instead'); ?></span>
                                            </div>
                                        </div><!-- /.flex-row -->
                                            <div class="AddressShippingAddLoqateContainer"  style="display: none">
                                                <input
                                                        class="LoqateField"
                                                        type="text"
                                                        data-addresstype="AddressShippingAdd"
                                                        name="AddressShippingAddLoqate"
                                                        id="AddressShippingAddLoqate"
                                                        placeholder="please type in your address..."
                                                    <?php echo $DVE ? "":"disabled"; ?>
                                                >
                                                <?php echo $DVE ? '':'<span class="notice-alert">'.$messageEdit.'</span>'; ?>
                                                <a  id="btnAddressShippingAddManual"  data-addresstype="AddressShippingAdd" class="btn btnEditLink AddressShippingAddManual AddressAddManual">Manual Add</a>
                                                <ul class="AddressMainList">

                                                </ul>
                                            </div>
                                    </div>
                                    <ul class="user-info contactView">

                                    </ul>
                                    <div class="btn-toolbar address-edit-btn">
                                        <a  id="btnAddressShippingLoqateAdd" data-addresstype="AddressShippingAdd"  class="btn btnEditLink AddressShippingAddLoqate ShowAddressAddLoqate">Edit Address</a>
                                        <?php
                                        if(!$DVE){ ?>
                                            <div class="DVE-alert " style="display: none">
                                                <?php echo $messageVerify; ?>
                                            </div>
                                        <?php }
                                        ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.flex-row -->
            </div>
            <div class="modal-footer">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('text_cancel');?></button>
                    <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('text_save');?></button>
                </div>
            </div>
            </form>
            <script>
                var BillingAddressAdd = {
                    "StreetAddress1":"",
                    "StreetAddress2":"",
                    "City":"",
                    "State":"",
                    "PostalCode":"",
                    "Country":""
                };
                var AddressShippingAdd = {
                    "Address2Street1":"",
                    "Address2Street2":"",
                    "City2":"",
                    "State2":"",
                    "PostalCode2":"",
                    "Country2":""
                };
                showAddressesEdit(BillingAddressAdd,"AddressBillingAdd");
                showAddressesEdit(AddressShippingAdd,"AddressShippingAdd");
            </script>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
