<?php
$OpportunityPipeline = $this->config->item('OpportunityPipeline');
$OpportunityPipeline = $OpportunityPipeline ? json_decode($OpportunityPipeline, true) : [];
$OppsCustomFields =  infusionsoft_opps_custom_fields();
$PipelineCodes = [];
foreach ($OpportunityPipeline as $Pipeline){
    $PipelineCodes[$Pipeline['PipelineName']] = $Pipeline['PipelineCode'];
}
$StageNames = [];
$StageOrder = [];
$StageNamesStriped = [];
foreach ($Stages as $Stage){
    $StageNames[$Stage->Id] = $Stage->StageName;

    preg_match("/[A-Z]{2}\d{2}/", $Stage->StageName, $output_array);
    if(sizeof($output_array) > 0){
        $Striped = substr($Stage->StageName,2);
    }else{
        $Striped = $Stage->StageName;
    }
    $StageNamesStriped[$Stage->Id] = $Striped;

    $StageOrder[$Stage->Id] = $Stage->StageOrder;
}
$Grouped = [];
foreach ($StageNames as $Stage_Id => $Stage_Name){
    $PipelineName = getPipelineName($Stage_Name);
    $Grouped[$PipelineName][$Stage_Id] = $StageNamesStriped[$Stage_Id];
}
?>
<small class="dev-only php">tab_opportunities.php - Line 31</small>
<div class="flex-row">
	<div class="note-description-opptable-container col-half">
	    <div class="NoteDescriptionContainer">
	
	        <div id="OppFilterContainer" class="OppFilter funkyradio hideThis">
	
	            <div class="col-md-8 no-pad-left  no-pad-right ">
	                <h3 class="filter">Filter:</h3>
	                <div class="col-md-4 no-pad-left no-pad-right funkyradio-success">
	                    <input type="radio" name="OppFilter" id="OppAll" value="All" checked/>
	                    <label for="OppAll">All</label>
	                </div>
	                <div class="col-md-4  no-pad-right funkyradio-info">
	                    <input type="radio" name="OppFilter" id="OppOpen"  value="Open" />
	                    <label for="OppOpen">Open</label>
	                </div>
	                <div class="col-md-4 no-pad-right funkyradio-info">
	                    <input type="radio" name="OppFilter" id="OppWon"  value="Won" />
	                    <label for="OppWon">Won</label>
	                </div>
	                <div class="col-md-4 no-pad-left no-pad-right funkyradio-info">
	                    <input type="radio" name="OppFilter" id="OppClosed"  value="Closed" />
	                    <label for="OppClosed">Closed</label>
	                </div>
	                <div class="col-md-4 no-pad-right funkyradio-info">
	                    <input type="radio" name="OppFilter" id="OppLost"  value="Lost" />
	                    <label for="OppLost">Lost</label>
	                </div>
	            </div>
	            <div class="col-md-4  no-pad-right ">
	                <h3 class="pipeline">Select Pipeline:</h3>
	                <select name="OppStage" id="OppStage" class="form-control  OppStage">
	                    <option value="1">NEW CLIENT</option>
	                    <option value="2">NEW PROPOSAL</option>
	                </select>
	            </div>
	
	
	        </div>
	    </div>
	
	    <table id="OppTable" data-shortcodeid="<?php echo $ShortCodeId; ?>"
	           class="<?php echo $ShortCodeId; ?> table table-striped table-bordered"
	           cellspacing="0"
	           width="100%" >
	        <thead>
	        <tr>
	            <th>Pipeline</th>
	            <th>Opportunity</th>
	            <th>Stage</th>
	            <th>Next Action Date</th>
	        </tr>
	        </thead>
	
	        <tbody>
	    <?php
	    $AllowedPipelineArr = explode(',',$AllowedPipeline);
	    foreach ($Opportunities as $Opportunity){
	        $Order = $StageOrder[$Opportunity->StageID];
	        $PipelineName = getPipelineName($StageNames[$Opportunity->StageID]);
	        if($AllowedPipeline !== 'ALL'){
	            if(!in_array($PipelineCodes[$PipelineName],$AllowedPipelineArr)) continue;
	        }
	        ?>
	
	        <tr
	                data-oppid="<?php echo $Opportunity->Id?>"
	                data-nextactiondate="<?php echo isset($Opportunity->NextActionDate->date) ? date('Y-m-d H:i', strtotime($Opportunity->NextActionDate->date)):""; ?>"
	                data-closedate="<?php echo isset($Opportunity->EstimatedCloseDate->date) ? date('Y-m-d', strtotime($Opportunity->EstimatedCloseDate->date)):""; ?>"
	                data-nextactionnote="<?php echo str_replace("=","",base64_encode($Opportunity->NextActionNotes)); ?>"
	                data-stageid="<?php echo $Opportunity->StageID?>"
	                data-allowedmove='<?php echo getAllwedMove($StageNames[$Opportunity->StageID],$Stages); ?>'
	                data-customfields="<?php
	
	                $customfields = [];
	                foreach ($OppsCustomFields as $GroupName => $OppsCustomFieldObj){
	                    foreach ($OppsCustomFieldObj as $OppsCustomFieldName =>$OppsCustomFieldDetails){
	                        $field = '_'.$OppsCustomFieldDetails->Name;
	                        if(isset($Opportunity->$field)){
	                            $customfields[$OppsCustomFieldDetails->Name] = $Opportunity->$field;
	                        }
	                    }
	                }
	                echo str_replace("=","",base64_encode(json_encode($customfields)));
	                ?>"
	                class="OppItem"
	        >
	            <td><?php echo $PipelineName; ?></td>
	            <td><span class="displayOppId"><?php echo $Opportunity->Id?></span> | <?php echo $Opportunity->OpportunityTitle?></td>
	            <td data-stageid="<?php echo $Opportunity->StageID; ?>" data-stagename="<?php echo $StageNames[$Opportunity->StageID]; ?>"><?php echo $StageNamesStriped[$Opportunity->StageID]; ?></td>
	            <td><?php echo isset($Opportunity->NextActionDate->date) ? date('Y-m-d H:i', strtotime($Opportunity->NextActionDate->date)):""; ?></td>
	        </tr>
	   <?php  }
	    ?>
	
	
	        </tbody>
	    </table>
	</div><!-- /.note-description-opptable-container -->
<!-- </div> -->


	<div class="current-opp-notes-opptabs-container col-half">
		<div class="CurrentOpportunityNotesContainer CurrentOpportunityNotesContainer<?php echo $ShortCodeId; ?>">
		    <div class="CurrentOpportunityNotes NoteDescriptionContainer NoteDescriptionContainer<?php echo $ShortCodeId; ?>">
		        <h2 class="oppmainlabel label-lg">Add Contact Activity</h2>
		        <div class="flex-row">
			        <div class="NoteTitleContainer col-half">
			            <h3 class="label-sm">Title:</h3>
			            <input name="NoteTitle" id="NoteTitle" class="form-control NoteTitle">
			        </div>
			        <div class="NoteTypeContainer col-half">
			            <h3 class="label-sm">Type:</h3>
			            <select name="NoteType" id="NoteType" class="form-control  NoteType">
			                <option value="">Select an action type</option>
			                <?php
			                $Types = infusionsoft_get_settings('ContactAction', 'optionstype');
			                $Types =  explode(',', $Types->message);
			                foreach ($Types as $Type){
			                    echo "<option value='$Type'>$Type</option>";
			                }
			                ?>
			            </select>
			        </div>
		        </div><!-- /.flex-row -->
		        <h3 class="label-sm">Description:</h3>
		        <textarea name="" id="opp-notes" class="opp-notes" placeholder="<?php echo $this->lang->line('text_type_your_note_here');?>"></textarea>
		        <input id='quick_notes_tags_<?php echo $ShortCodeId; ?>' type='text' class='quick_notes_tags quick_notes_tags-<?php echo $ShortCodeId; ?>' value='#opp_<?php echo $Opportunity->Id?>,#opportunity' />
		        <div class="note-action">
		            <div class="SaveOppNoteContainer">
		                <button data-action="" data-shortcodeid="<?php echo $ShortCodeId; ?>" class="nb-btn nb-primary icon-btn SaveOppNote" disabled><i class="fa fa-sticky-note-o" aria-hidden="true"></i> Add Note </button>
		
		            </div>
		        </div>
		    </div>
		</div>
		
	
		
		<div class="opptabs">
		    <div id="<?php echo $ShortCodeId; ?>" class="opptabsContainer">
		        <div id="opptabs" class="opptabs-<?php echo $ShortCodeId; ?>">
		            <ul class="flex-row">
		                <li class="col-third"><a href="#tabs-1">Opportunity</a></li>
		                <li class="col-third"><a href="#tabs-2">Contact Notes</a></li>
		                <li class="col-third"><a href="#tabs-3">Connected Data</a></li>
		            </ul>
		            <div id="tabs-1">
		                <form id="updateopp" data-identifier="<?php echo $ShortCodeId; ?>">
		                    <input type="hidden" id="Id" name="Id" value="">
		                <div class="NextActionDateContainer">
		                    <h3 class="label-sm">Next Action Date:</h3>
		                    <input name="NextActionDate" id="NextActionDate" class="form-control NextActionDate" value="">
		                </div>
		                <div class="CloseActionDateContainer">
		                    <h3 class="label-sm">When will this close:</h3>
		                    <input name="EstimatedCloseDate" id="CloseActionDate" class="form-control CloseActionDate" value="">
		                </div>
		                <div class="NextActionNoteContainer">
		                    <h3 class="label-sm">Next Action Note:</h3>
		                    <textarea name="NextActionNotes" id="NextActionNote" class="form-control NextActionNote" placeholder="<?php echo $this->lang->line('text_type_your_note_here');?>"></textarea>
		                </div>
		                <div class="OppStageContainer">
		                    <h3 class="label-sm">Stage:</h3>
		                    <select name="StageID" id="OppStage" class="form-control  OppStage">
		                        <optgroup label="">
		
		                        </optgroup>
		                        <?php
		                        foreach ($Grouped as $GroupName => $Id_Name){
		                            echo '<optgroup label="'.$GroupName.'">';
		                                foreach ($Id_Name as $theId => $theName){ ?>
		                                     <option value="<?php echo $theId ?>"><?php echo $theName; ?></option>
		                                <?php }
		                            echo '</optgroup>';
		                                    }
		                        ?>
		
		                    </select>
		                </div>
		
		                <div class="OppCustomFIeldsContainer">

		                    <?php
		                    if(sizeof($OppsCustomFields) > 0) echo '<h3 class="opp-customFields">CustomFields:</h3>';
		                    $customfields = [];
		                    foreach ($OppsCustomFields as $GroupName => $OppsCustomFieldObj){
		                        foreach ($OppsCustomFieldObj as $OppsCustomFieldName =>$OppsCustomFieldDetails){
		                            $DataType = $OppsCustomFieldDetails->DataTypeName;
		                            switch ($OppsCustomFieldDetails->DataTypeName){
		                                case 'Currency':
		                                    echo "
		                                        <div class=\"form-group \">
		                                            <label data-type='".$DataType."'>".$OppsCustomFieldDetails->Label."</label>
		                                            <input  type='number'
		                                                    step='any'
		                                                    class=\"".$OppsCustomFieldDetails->Name	." input form-control\" 
		                                                    id=\"".$OppsCustomFieldDetails->Name."\" 
		                                                    aria-describedby=\"txt".$OppsCustomFieldDetails->Name."\" 
		                                                    name=\"".$OppsCustomFieldDetails->Name."\"
		                                                    >
		                                         </div>
		                                        ";
		                                    break;
		                                case 'Decimal Number':
		                                    echo "
		                                        <div class=\"form-group \">
		                                            <label data-type='".$DataType."'>".$OppsCustomFieldDetails->Label."</label>
		                                            <input  type='number'
		                                                    step='any'
		                                                    class=\"".$OppsCustomFieldDetails->Name." input form-control\" 
		                                                    id=\"".$OppsCustomFieldDetails->Name."\" 
		                                                    aria-describedby=\"txt".$OppsCustomFieldDetails->Name."\" 
		                                                    name=\"".$OppsCustomFieldDetails->Name."\"
		                                                    >
		                                         </div>
		                                        ";
		                                    break;
		                                case 'Text':
		                                    echo "
		                                        <div class=\"form-group \">
		                                            <label data-type='".$DataType."'>".$OppsCustomFieldDetails->Label."</label>
		                                            <input  type=\"text\" 
		                                                    class=\"".$OppsCustomFieldDetails->Name." input form-control\" 
		                                                    id=\"".$OppsCustomFieldDetails->Name."\" 
		                                                    aria-describedby=\"txt".$OppsCustomFieldDetails->Name."\" 
		                                                    name=\"".$OppsCustomFieldDetails->Name."\"
		                                                    >
		                                         </div>
		                                        ";
		                                    break;
		
		                                case 'Text Area':
		                                    echo "
		                                        <div class=\"form-group \">
		                                            <label data-type='".$DataType."'>".$OppsCustomFieldDetails->Label."</label>
		                                            <textarea 
		                                                    class=\"".$OppsCustomFieldDetails->Name." input form-control\" 
		                                                    id=\"".$OppsCustomFieldDetails->Name."\" 
		                                                    aria-describedby=\"txt".$OppsCustomFieldDetails->Name."\" 
		                                                    name=\"".$OppsCustomFieldDetails->Name."\"
		                                                    rows=\"3\" ></textarea>
		                                         </div>
		                                        ";
		                                    break;
		
		                                case 'Whole Number':
		                                    echo "
		                                        <div class=\"form-group \">
		                                            <label data-type='".$DataType."'>".$OppsCustomFieldDetails->Label."</label>
		                                            <input  type='number'
		                                                    class=\"".$OppsCustomFieldDetails->Name." input form-control\" 
		                                                    id=\"".$OppsCustomFieldDetails->Name."\" 
		                                                    aria-describedby=\"txt".$OppsCustomFieldDetails->Name."\" 
		                                                    name=\"".$OppsCustomFieldDetails->Name."\"
		                                                    >
		                                         </div>
		                                        ";
		                                    break;
		
		                                case 'Email':
		                                    echo "
		                                        <div class=\"form-group \">
		                                             <label data-type='".$DataType."'>".$OppsCustomFieldDetails->Label."</label>
		                                            <input  type='email'
		                                                    class=\"".$OppsCustomFieldDetails->Name." input form-control\" 
		                                                    id=\"".$OppsCustomFieldDetails->Name."\" 
		                                                    aria-describedby=\"txt".$OppsCustomFieldDetails->Name."\" 
		                                                    name=\"".$OppsCustomFieldDetails->Name."\"
		                                                    >
		                                         </div>
		                                        ";
		                                    break;
		
		                                case 'Password':
		                                    echo "
		                                        <div class=\"form-group \">
		                                            <label data-type='".$DataType."'>".$OppsCustomFieldDetails->Label."</label>
		                                            <input  type='password'
		                                                    class=\"".$OppsCustomFieldDetails->Name." input form-control\" 
		                                                    id=\"".$OppsCustomFieldDetails->Name."\" 
		                                                    aria-describedby=\"txt".$OppsCustomFieldDetails->Name."\" 
		                                                    name=\"".$OppsCustomFieldDetails->Name."\"
		                                                    >
		                                         </div>
		                                        ";
		                                    break;
		
		                                case 'Dropdown':
		                                    $choices = explode("\n", $OppsCustomFieldDetails->Values);
		                                    $options = "<option  value>Please Select One</option>";
		                                    foreach ($choices as $choice){
		                                        $choice = trim($choice);
		                                        $choice = str_replace("\r", '', $choice);
		                                        if(!$choice) continue;
		                                        $options.= "<option value='$choice' >$choice</option>";
		                                    }
		                                    echo "
		                                        <div class=\"form-group \">
		                                            <label  data-type='".$DataType."' >".$OppsCustomFieldDetails->Label."</label>
		                                            <select class=\"$OppsCustomFieldDetails->Name select form-control\" 
		                                                    id=\"".$OppsCustomFieldDetails->Name."\" 
		                                                    aria-describedby=\"txt".$OppsCustomFieldDetails->Name."\" 
		                                                    name=\"".$OppsCustomFieldDetails->Name."\"
		                                                    >
		                                              ".$options."
		                                            </select>
		                                         </div>
		                                         
		                                        ";
		                                    break;
		
		                                case 'List Box':
		                                    $choices = explode("\n", $OppsCustomFieldDetails->Values);
		                                    $options = "";
		                                    foreach ($choices as $choice){
		                                        $choice = trim($choice);
		                                        $choice = str_replace("\r", '', $choice);
		                                        if(!$choice) continue;
		                                        $options.= "<option value='$choice' >$choice</option>";
		                                    }
		                                    echo "
		                                        <div class=\"form-group \">
		                                            <label  data-type='".$DataType."'>".$OppsCustomFieldDetails->Label."</label>
		                                            <select multiple=\"MULTIPLE\" class=\"$OppsCustomFieldDetails->Name select form-control\" 
		                                                    id=\"".$OppsCustomFieldDetails->Name."\" 
		                                                    aria-describedby=\"txt".$OppsCustomFieldDetails->Name."\" 
		                                                    name=\"".$OppsCustomFieldDetails->Name."\"
		                                                    >".$options."</select>
		                                         </div>
		                                         
		                                        ";
		                                    break;
		
		                                case 'Radio':
		                                    $choices = explode("\n", $OppsCustomFieldDetails->Values);
		                                    $checkbox = '';
		                                    foreach ($choices as $choice){
		                                        $choice = trim($choice);
		                                        $choice = str_replace("\r", '', $choice);
		                                        $checkbox.= "<div class=\"form-check\">
		                                                          <span class=\"form-check-label\">
		                                                            <input data-value='$choice' 
		                                                                   type=\"radio\" 
		                                                                   class=\"$OppsCustomFieldDetails->Name radio form-check-input\" 
		                                                                   name=\"$OppsCustomFieldDetails->Name\" 
		                                                                   id=\"$OppsCustomFieldDetails->Name\" 
		                                                                   value=\"$choice\" >
		                                                                $choice
		                                                          </span>
		                                                        </div>";
		                                    }
		                                    echo "
		                                        <fieldset class=\"form-group \">
		                                        <label data-type='".$DataType."'>$OppsCustomFieldDetails->Label</label>
		                                        $checkbox
		                                      </fieldset>
		                                        ";
		
		                                    break;
		
		                                case 'Date':
		                                    echo "
		                                        <div class=\"form-group \">
		                                            <label data-type='".$DataType."'>".$OppsCustomFieldDetails->Label."</label>
		                                            <input  type='text'
		                                                    class=\"$OppsCustomFieldDetails->Name infoDate input form-control\" 
		                                                    id=\"".$OppsCustomFieldDetails->Name."\" 
		                                                    aria-describedby=\"txt".$OppsCustomFieldDetails->Name."\" 
		                                                    name=\"".$OppsCustomFieldDetails->Name."\"
		                                                    >
		                                         </div>
		                                        ";
		                                    break;
		
		                                case 'Date/Time':
		                                    echo "
		                                        <div class=\"form-group \">
		                                            <label data-type='".$DataType."'>".$OppsCustomFieldDetails->Label."</label>
		                                            <input  type='text'
		                                                    class=\"$OppsCustomFieldDetails->Name infoDateTime input form-control\" 
		                                                    id=\"".$OppsCustomFieldDetails->Name."\" 
		                                                    aria-describedby=\"txt".$OppsCustomFieldDetails->Name."\" 
		                                                    name=\"".$OppsCustomFieldDetails->Name."\"
		                                                    >
		                                         </div>
		                                        ";
		                                    break;
		                                case 'Website':
		                                    echo "
		                                        <div class=\"form-group \">
		                                            <label data-type='".$DataType."' for=\"".$OppsCustomFieldDetails->Name."\">".$OppsCustomFieldDetails->Label." <i class=\"fa fa-external-link connected-data-url\" data-field=\"".$OppsCustomFieldDetails->Name."\" aria-hidden=\"true\"></i></label>
		                                            <input  type='url'
		                                                    class=\"$OppsCustomFieldDetails->Name input form-control\" 
		                                                    id=\"".$OppsCustomFieldDetails->Name."\" 
		                                                    aria-describedby=\"txt".$OppsCustomFieldDetails->Name."\" 
		                                                    name=\"".$OppsCustomFieldDetails->Name."\"
		                                                    >
		                                         </div>
		                                        ";
		                                    break;
		
		                                default:
		                                    echo "
		                                        <div class=\"form-group \">
		                                            <label data-type='".$DataType."'>".$OppsCustomFieldDetails->Label."</label>
		                                            <span>(".$OppsCustomFieldDetails->DataTypeName.") sorry this field type is not yet supported</span>
		                                         </div>
		                                        ";
		                                    break;
		                            }
		                        }
		                    }
		                    ?>
		                </div>
		
		                <div class="UpdateOppContainer">
		                    <button type="submit" data-action="" class="nb-btn nb-primary icon-btn UpdateOpp"><i class="fa fa-save"></i>Update Opportunity </button>
		
		                </div>
		                </form>
		            </div>
		            <div id="tabs-2">
		                <h4 style="text-align: center">Coming Soon</h4>
		            </div>
		            <div id="tabs-3">
		                <h4 style="text-align: center">Coming Soon</h4>
		            </div>
		        </div>
		
		    </div>
		
		
		</div>
	</div><!-- /.current-opp-notes-opptabs-container -->
</div><!-- /.flex-row -->		
<script>
    $(document).ready(function() {
        OppTable['<?php echo $ShortCodeId; ?>'] = $('#OppTable.<?php echo $ShortCodeId; ?>').DataTable({
            responsive: true,
            "paging":   false,
            "searching": false,
            "info":     false,
            "createdRow": function ( row, data, index ) {
            }
        });
        $('#OppTable.<?php echo $ShortCodeId; ?> tbody').on( 'click', 'tr', function () {
            OppTableData['Data'] = OppTable['<?php echo $ShortCodeId; ?>'].row( this ).data();
            OppTableData['Row'] = this;
            console.log(OppTableData);

        } );


        $( function() {
            $( "#opptabs.opptabs-<?php echo $ShortCodeId; ?>" ).tabs();
        } );
        $("input.NextActionDate").daterangepicker({
            autoUpdateInput: false,
            showDropdowns: true,
            timePicker:true,
            locale: {
                format: "YYYY-MM-DD HH:mm"
            },
            singleDatePicker:true
        }).on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format("YYYY-MM-DD HH:mm"));
            $("#NextActionDate").removeClass('error');
        });
        $("input.CloseActionDate").daterangepicker({
            autoUpdateInput: false,
            showDropdowns: true,
            locale: {
                format: "YYYY-MM-DD"
            },
            singleDatePicker:true
        }).on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format("YYYY-MM-DD"));
            $("#NextActionDate").removeClass('error');
        });
        $("table#OppTable.<?php echo $ShortCodeId; ?> tr.OppItem:first").trigger('click');
        initTinymceOppNotes("<?php echo $ShortCodeId; ?>");
        tagIt();
        $("input.infoDate").daterangepicker({
            autoUpdateInput: false,
            locale: {
                format: 'YYYY-MM-DD'
            },
            singleDatePicker:true,
            showDropdowns: true
        }).on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD'));
        });
    });
</script>