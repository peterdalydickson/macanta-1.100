<?php
//$DVE = macanta_validate_feature('DVE');
$DVE = 'enabled';
$No = 'No';
$DisableThis = '';
$AddressShippingStat  = checkVerifiedAddress($Id,"AddressShippingEdit");
$AddressBillingStat  = checkVerifiedAddress($Id,"AddressBillingEdit");
if(empty($DVE) || $DVE != "enabled"){
    $No = 'Unable';
    $DisableThis = 'Unable';
}
if(!empty($Phone1)){
    $Phone1Info = macanta_validate_phone_number($Id,$Phone1,$Country,$session_data);
    $Phone1Stat = $Phone1Info['IsValid'];
    $Phone1Error = isset($Phone1Info['Error']) ? $Phone1Info['Error']:'';
}else{
    $Phone1Stat = 'Unable';
}

if(!empty($Phone2)){
    $Phone2Info = macanta_validate_phone_number($Id,$Phone2,$Country,$session_data);
    $Phone2Stat = $Phone2Info['IsValid'];
    $Phone2Error = isset($Phone2Error['Error']) ? $Phone2Error['Error']:'';
}else{
    $Phone2Stat = 'Unable';
}

if($Email){
    $EmailInfo = macanta_validate_email($Email,$session_data);
    if($EmailInfo['format_valid']){
        $EmailStat = $EmailInfo['format_valid'] == 1 ? "Yes":$EmailInfo['format_valid'];
    }else{
        $EmailStat = $No;
    }

}else{
    $EmailStat = $No;
}

?>
<small class="dev-only php">contact_info.php</small>
<div class="contact-info-container">
	<div class="contact-twillio-container panel-light">
		<div class="contact-info-avatar-container">
		    <div class="avatar-container">
		        <img class="avatar" src="<?php echo $Gravatar; ?>?s=150" alt="">
		        <?php
		        if(isDeletedContact($Id)) echo '<div class="deletedContact"><i class="fa fa-exclamation-triangle"></i> Deleted</div>';
		        ?>
		    </div>
		    <!-- Start of basic info & address info container -->
		    <div class="contact-basic-address-container">
			    <!--Start of basic info panel-->
			    <div class="contact-info-basic">
			
			        <div class="user-info-b">
			            <ul class="user-info">
			                <li class="info-item editContactSmall">
			                    <span class="editContact"><?php echo $this->lang->line('text_edit_contact_details'); ?><!--Edit Contact Details--></span>
			                    <span class="showContact">Show Contact Details</span>
			                </li>
			                <li class="info-item ContactInfoHideMobile"><span class="label"><?php echo $this->lang->line('text_name'); ?>: </span><span class="name-first-last"><span
			                            class="profile FirstName basic-info"><?php echo $FirstName; ?></span> <span
			                            class="profile LastName basic-info"><?php echo $LastName; ?></span></span></li>
			                <li class="info-item  ContactInfoHideMobile">
			                    <div class="macanta-notice-info EmailStat" style="display: none;">
			                        <?php
			                        $Format = $EmailInfo['format_valid'] ? 'Valid Format':'Invalid Format';
			                        $SMTPCheck = $EmailInfo['smtp_check'] ? 'Passed':'Failed';
			                        switch ($EmailStat){
			                            case 'Yes':
			                                echo '<ul class="macanta-notice-ul">
			                                        <li><span>Domain:</span> '.$EmailInfo['domain'].'</li>
			                                        <li><span>Format:</span> '. $Format .'</li>
			                                        <li><span>SMTP Check:</span> '. $SMTPCheck .'</li>
			                                        <li><span>Score:</span> '.$EmailInfo['score'].'</li>
			                                      </ul>';
			                                break;
			                            case 'No':
			                                echo '<ul class="macanta-notice-ul">
			                                        <li><span>Status:</span> Not A Valid Email Address</li>
			                                      </ul>';
			                                break;
			                            case 'Unable':
			                                echo '<ul class="macanta-notice-ul">
			                                        <li><span style="color: #fe7b7f;font-weight: 600;text-shadow: 0px 0px 1px #000;">Verification Disabled</span></li>
			                                      </ul>';
			                                break;
			                        }
			                        ?>
			                    </div>
			                    <span class="label IconEmailStat  show-notice"  data-show="EmailStat"><?php echo $this->lang->line('text_email'); ?>:
			                        <?php
			                        //Email icon should not be green when $SMTPCheck is Failed
			                        $EmailStat = $SMTPCheck == 'Passed' ? $EmailStat:$No
			                        ?>
			                        <i class="fa fa-envelope-square IsEmailValid IsEmailValid<?php echo $EmailStat; ?>"></i>
			                    </span>
			                    <span class="Email basic-info show-notice"  data-show="EmailStat"><?php echo $Email; ?></span>
			                </li>
			                <li class="info-item  ContactInfoHideMobile"><span class="label">Title: </span><span
			                            class="JobTitle basic-info"><?php echo $JobTitle; ?></span></li>
			                <li class="info-item  ContactInfoHideMobile"><span class="label"><?php echo $this->lang->line('text_company'); ?>: </span><span
			                            class="Company basic-info"><?php echo $Company; ?></span></li>
			                <li class="info-item  ContactInfoHideMobile">
			                    <div class="macanta-notice-info PhoneStat Phone1Stat" style="display: none;">
			                        <?php
			                        switch ($Phone1Stat){
			                            case 'Yes':
			                                echo '<ul class="macanta-notice-ul">
			                                        <li><span>Status:</span> Valid Phone Number</li>
			                                        <li><span>PhoneNumber:</span> '.$Phone2Info['PhoneNumber'].'</li>
			                                        <li><span>NetworkName:</span> '.$Phone1Info['NetworkName'].'</li>
			                                        <li><span>NetworkCode:</span> '.$Phone1Info['NetworkCode'].'</li>
			                                        <li><span>NetworkCountry:</span> '.$Phone1Info['NetworkCountry'].'</li>
			                                        <li><span>NationalFormat:</span> '.$Phone1Info['NationalFormat'].'</li>
			                                        <li><span>CountryPrefix:</span> '.$Phone1Info['CountryPrefix'].'</li>
			                                        <li><span>NumberType:</span> '.$Phone1Info['NumberType'].'</li>
			                                      </ul>';
			                                break;
			                            case 'No':
			                                echo '<ul class="macanta-notice-ul">
			                                        <li><span>Status:</span> Not A Valid Phone Number</li>
			                                      </ul>';
			                                break;
			                            case 'Unable':
			                                echo '<ul class="macanta-notice-ul">
			                                        <li><span style="color: #fe7b7f;font-weight: 600;text-shadow: 0px 0px 1px #000;">Verification Disabled. '.$Phone1Error.'</span></li>
			                                      </ul>';
			                                break;
			                        }
			                        ?>
			                    </div>
			                    <span class="label show-notice IconPhone1Stat" data-show="Phone1Stat"><?php echo $this->lang->line('text_phone'); ?>1:
			                        <i class="fa fa-phone-square IsPhoneValid IsPhoneValid<?php echo $Phone1Stat; ?>"></i>
			
			                    </span>
			                    <span class="Phone1 basic-info show-notice"  data-show="Phone1Stat"><?php echo $Phone1; ?></span>
			                    <span class="user-twilio"><?php //echo $Button1 ?></span>
			                </li>
			                <li class="info-item  ContactInfoHideMobile">
			                    <div class="macanta-notice-info PhoneStat Phone2Stat " style="display: none;">
			                        <?php
			                        switch ($Phone2Stat){
			                            case 'Yes':
			                                echo '<ul class="macanta-notice-ul">
			                                        <li><span>Status:</span> Valid Phone Number</li>
			                                        <li><span>PhoneNumber:</span> '.$Phone2Info['PhoneNumber'].'</li>
			                                        <li><span>NetworkName:</span> '.$Phone2Info['NetworkName'].'</li>
			                                        <li><span>NetworkCode:</span> '.$Phone2Info['NetworkCode'].'</li>
			                                        <li><span>NetworkCountry:</span> '.$Phone2Info['NetworkCountry'].'</li>
			                                        <li><span>NationalFormat:</span> '.$Phone2Info['NationalFormat'].'</li>
			                                        <li><span>CountryPrefix:</span> '.$Phone2Info['CountryPrefix'].'</li>
			                                        <li><span>NumberType:</span> '.$Phone2Info['NumberType'].'</li>
			                                      </ul>';
			                                break;
			                            case 'No':
			                                echo '<ul class="macanta-notice-ul">
			                                        <li><span>Status:</span> Not A Valid Phone Number</li>
			                                      </ul>';
			                                break;
			                            case 'Unable':
			                                echo '<ul class="macanta-notice-ul">
			                                        <li><span  style="color: #fe7b7f;font-weight: 600;text-shadow: 0px 0px 1px #000;">Verification Disabled'.$Phone1Error.'</span></li>
			                                      </ul>';
			                                break;
			                        }
			                        ?>
			                    </div>
			                    <span class="label show-notice IconPhone2Stat"  data-show="Phone2Stat"><?php echo $this->lang->line('text_phone'); ?>2:
			                        <i class="fa fa-phone-square IsPhoneValid IsPhoneValid<?php echo $Phone2Stat; ?>"></i>
			                    </span>
			                    <span class="Phone2 basic-info show-notice"  data-show="Phone2Stat"><?php echo $Phone2; ?></span>
			                    <span class="user-twilio"><?php //echo $Button2 ?></span>
			                </li>
			                <li class="info-item actionLi edit-contact-li"> <!-- Jean's amend - 30-03-2020 -->
			                    <span class="editContact"><i class="fa fa-edit" aria-hidden="true"> </i><?php echo $this->lang->line('text_edit_contact_details'); ?><!--Edit Contact Details--></span>
			                </li>
                            <li class="info-item editContactBig delete-contact-li"> <!-- Jean's amend - 30-03-2020 -->
                                <span class="deleteContact" data-id="<?php echo $Id ?>"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete Contact<!--Edit Contact Details--></span>
                            </li>
			            </ul>
			
			        </div>
			    </div>
			    <!--End of basic info panel-->
			    <!--Start of address info panel-->
			    <div class="contact-info-address ContactInfoHideMobile">
			        <div class="user-info-b AddressSlider">
			            <div class="AddressSliderContainer">
			                <div class="AddressBilling">
			                    <div class="switchAddress flex-row">
			                        <div class="">
			                            <div class="macanta-notice-info AddressStat AddressBillingStat " style="display: none;">
			                                <?php
			                                switch ($DisableThis){
			                                    case '':
			                                        if($AddressBillingStat){
			                                            echo '<ul class="macanta-notice-ul">
			                                        <li class="stat-label"><span style="border: none; font-style: inherit;">Address Verified </span></li>
			                                      </ul>';
			                                        }else{
			                                            echo '<ul class="macanta-notice-ul">
			                                        <li class="stat-label"><span  style="border: none; font-style: inherit;">Address Not Verified</span></li>
			                                      </ul>';
			                                        }
			
			                                        break;
			                                    case 'Unable':
			                                        echo '<ul class="macanta-notice-ul">
			                                        <li><span  style="color: #fe7b7f;font-weight: 600;text-shadow: 0px 0px 1px #000;border: none;font-style: inherit;">Verification Disabled</span></li>
			                                      </ul>';
			                                        break;
			                                }
			                                ?>
			                            </div>
			                            <span class="label show-notice IconAddressBillingStat"  data-show="AddressBillingStat">
			                                <i
			                                        class="fa fa-address-card-o AddressStatus <?php echo $DisableThis; ?> <?php echo $AddressBillingStat == true ? "ValidatedAddress ":"InValidatedAddress" ?>"
			                                        title="<?php echo checkVerifiedAddress($Id,"AddressBillingEdit") == true ? "Address Verified":"Address Not Verified"?>">
			
			                            </i>
			                            </span>
			                        </div>
			                        <div class="address-type">
			                            <label>Billing</label>
			                            <span class="show-other-address btn "><?php echo $this->lang->line('text_show_shipping_instead'); ?></span>
			                        </div>
			                    </div>
			                    <ul class="user-info contactView">
			                    </ul>
			                </div>
			                <div class="AddressShipping">
			                    <div class="switchAddress flex-row">
			                        <div class="">
			                            <div class="macanta-notice-info AddressStat AddressShippingStat " style="display: none;">
			                                <?php
			                                switch ($DisableThis){
			                                    case '':
			                                        if($AddressShippingStat){
			                                            echo '<ul class="macanta-notice-ul">
			                                        <li class="stat-label"><span style="border: none; font-style: inherit;">Address Verified </span></li>
			                                      </ul>';
			                                        }else{
			                                            echo '<ul class="macanta-notice-ul">
			                                        <li class="stat-label"><span  style="border: none; font-style: inherit;">Address Not Verified</span></li>
			                                      </ul>';
			                                        }
			
			                                        break;
			                                    case 'Unable':
			                                        echo '<ul class="macanta-notice-ul">
			                                        <li><span  style="color: #fe7b7f;font-weight: 600;text-shadow: 0px 0px 1px #000;border: none;font-style: inherit;">Verification Disabled</span></li>
			                                      </ul>';
			                                        break;
			                                }
			                                ?>
			                            </div>
			                            <span class="label show-notice IconAddressShippingStat"  data-show="AddressShippingStat">
			                                <i
			                                        class="fa fa-address-card-o AddressStatus  <?php echo $DisableThis; ?> <?php echo $AddressShippingStat == true ? "ValidatedAddress":"InValidatedAddress"?>"
			                                        title="<?php echo checkVerifiedAddress($Id,"AddressShippingEdit") == true ? "Address Verified":"Address Not Verified"?>">
			
			                            </i>
			                            </span>
			                        </div>
			                        <div class="address-label-switch">
			                            <label>Shipping</label>
			                            <span class="show-other-address btn "><?php echo $this->lang->line('text_show_billing_instead'); ?></span>
			                        </div>
			                    </div>
			                    <ul class="user-info contactView">
			
			                    </ul>
			                </div>
			            </div>
			        </div>
			    </div>
			    <!--End of address info panel-->
		    </div>
		    <!-- End of basic info & address info container -->
		</div>
		<!-- End of avatar & info container -->
		    <!-- Start of .contact-call-container -->
		    <div class="contact-call-container">
			    <!-- .contact-call gets moved here via JavaScript (***) below -->
		    </div>
	    <!-- End of .contact-call-container -->
	</div>
	<!-- End of .contact-twillio-container -->
	    <div class="in-app-search-panel-container panel-light">
	        <!--Start of search panel-->
	        <div class="search-results-container searchCall">
	            <div class="inside-search">
	                    <button class="nb-btn nb-secondary icon-btn addContact " type="button"><i class="fa fa-user-plus" aria-hidden="true" title="Add Contact"></i></button>
	                <input type="text" class="searchKey InAppSearch "
	                       placeholder="<?php echo $this->lang->line('text_search_for'); ?>..." data-source="InAppSearch">
	                        <button class="nb-btn nb-primary search" type="button"
	                                data-source="InAppSearch"><?php echo $this->lang->line('text_go'); ?>!</button>
	            </div>
	            <div class="recent-results">
	
	                <?php
	                $CacheContent = manual_cache_loader('searched_cache' . $session_data['InfusionsoftID'], true);
	                $Next = false;
	                $NextContactId = '';
	                $theKey = 0;
	                $hide = '';
	                $IdKey = 'Id';
	                if ($CacheContent) {
	                    $CacheContent = json_decode($CacheContent, true);
	                    foreach ($CacheContent as $key => $Contact) {
	                        $IdKey = isset($Contact['Contact ID']) ? "Contact ID" : $IdKey;
	                        $IdKey = isset($Contact['Contact Id']) ? "Contact Id" : $IdKey;
	                        $IdKey = isset($Contact['Contact id']) ? "Contact id" : $IdKey;
	                        $IdKey = isset($Contact['ContactId']) ? "ContactId" : $IdKey;
	                        $IdKey = isset($Contact['ContactID']) ? "ContactID" : $IdKey;
	                        if ($Next === true) {
	                            $theKey = $key;
	                            $NextContactId = $Contact[$IdKey];
	                            break;
	                        }
	                        if ($Contact[$IdKey] == $Id) $Next = true;
	                    }
	                    if ($NextContactId == '') {
	                        $NextContactId = $CacheContent[0][$IdKey];
	                    }
	                    if ($NextContactId == $Id) {
	                        $hide = 'hideThis';
	                    }
	
	                } else {
	                    $hide = 'hideThis';
	                }
                    $URL = $this->config->item('base_url');
                    $URL = str_replace('/public', '', $URL);
	                ?>
	                <!--<?php echo $CachePath; ?> -->
	                <span class="showNextResultContainer <?php echo $hide; ?>">
		                <a href="<?php echo $URL;?>#contact/<?php echo $NextContactId; ?>" class="nb-btn nb-secondary icon-btn showNextResult"
		                   title="<?php echo $CacheContent[$theKey]['FirstName'] . " " . $CacheContent[$theKey]['LastName'] . " (" . $CacheContent[$theKey]['Email'] . ")"; ?>">
		                    Next contact <i class="fa fa-arrow-right" aria-hidden="true"></i>
		                </a>
		            </span>
	            </div>
	            <!-- Back to search page button start -->
	            	                <a href="<?php echo $URL; ?>" class="nb-btn icon-btn txt-btn back-to-search-page">
	                    <span class="showRecentResults"><i class="glyphicon glyphicon-arrow-left"></i> <?php echo $this->lang->line('text_view_recent_results'); ?>
	                </span>
	                </a>
	            <!-- Back to search page button end -->
	        </div>
	        <!--End of search panel-->
	
	<!--
	        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  no-pad-left no-pad-right">
	            <div class="full-contact Lazy-full-contact">
	                <script>
	                    lazy_load("full-contact","Full Contact","FullContact","core/contact");
	                </script>
	
	            </div>
	        </div>
	-->
	        <!--End of e-commerce panel-->	    
	
	    </div>
	    <!-- End of in-app search panel container -->
	    

	    
</div>
<!-- End of contact info container -->

<!-- *** -->
<script>
	$(".contact-call").appendTo(".contact-call-container");
</script>

<script>
    $(".recent-resultsB").html('');
    $(".recent-results").appendTo(".recent-resultsB");
    var BillingAddress = {
        "StreetAddress1":"<?php echo $StreetAddress1; ?>",
        "StreetAddress2":"<?php echo $StreetAddress2; ?>",
        "City":"<?php echo $City; ?>",
        "State":"<?php echo $State; ?>",
        "PostalCode":"<?php echo $PostalCode; ?>",
        "Country":"<?php echo $Country; ?>"
    };
    var AddressShipping = {
        "Address2Street1":"<?php echo $Address2Street1; ?>",
        "Address2Street2":"<?php echo $Address2Street2; ?>",
        "City2":"<?php echo $City2; ?>",
        "State2":"<?php echo $State2; ?>",
        "PostalCode2":"<?php echo $PostalCode2; ?>",
        "Country2":"<?php echo $Country2; ?>"
    };
    showAddresses(BillingAddress,"AddressBilling");
    showAddresses(AddressShipping,"AddressShipping");
    $('.user-info-b li.info-item:visible:odd').addClass('odd-item');
    $('.user-info-b li.info-item:visible:even').addClass('even-item');
</script>



