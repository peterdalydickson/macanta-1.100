<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 07/03/16
 * Time: 3:00 PM
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

class ControllerTemplate extends MY_Controller
{
    //protected $local_stylesheets = array('mystylesheet.css');
    public $content = array();
    public function __construct(){
        parent::__construct();
        $this->local_stylesheets = array('your.css'); // for additional css file
        $this->local_javascripts = array('your.js'); // for additional js file

    }

    public function index()
    {
        $test = 'test';
        $this->content[] = $this->load->view('macanta-body', null, true); // return the view
        $this->render($this->content); // render the data
    }

}
