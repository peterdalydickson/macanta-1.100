<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 07/03/16
 * Time: 3:00 PM
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');
use Twilio\Rest\Client;

class Admin extends MY_Controller
{
    private $Contact;
    public $ajaxResults = array(
        "status"=> 0, // true or false
        "message"=>"",// any massage
        "html"=>"",// any html view
        "data" => array(), // returned data
        "script" => '' // javascript need to execute // returned data
    );
    public $AdminSections = array(
        1 => array("id" =>"admin_LookFeel", "label"=>"Look & Feel", "view"=>"core/tab_admin_look_feel", "status"=>"active"),
        2 => array("id" =>"admin_Permissions", "label"=>"Security & Saved Searches", "view"=>"core/tab_admin_permissions", "status"=>"inactive"),
        //3 => array("id" =>"admin_NoteTags", "label"=>"Note #tags", "view"=>"core/tab_admin_note_tags"),
        4 => array("id" =>"admin_CustomTabs", "label"=>"Custom Tabs", "view"=>"core/tab_admin_custom_tabs", "status"=>"inactive"),
	
        5 => array("id" =>"admin_UserManagement", "label"=>"User Management", "view"=>"core/tab_admin_user_management"),
        //6 => array("id" =>"admin_CallSettings", "label"=>"Call Settings", "view"=>"core/tab_admin_call_settings", "status"=>"inactive"),
        7 => array("id" =>"admin_Other", "label"=>"Relationships", "view"=>"core/tab_admin_Other", "status"=>"inactive"),
        8 => array("id" =>"admin_Connector", "label"=>"Connector", "view"=>"core/tab_admin_connector", "status"=>"inactive"),
        9 => array("id" =>"admin_Automation", "label"=>"Automation", "view"=>"core/tab_admin_automation", "status"=>"inactive"),
        //10 => array("id" =>"admin_CallCenter", "label"=>"Call Center", "view"=>"core/tab_admin_callcenter")


    );
    public function __construct($Prams=[]){
        parent::__construct();
        $this->Contact = $Prams;
        //$this->load->database(); this is already loaded in My_Controller
        //$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file')); this is already loaded in My_Controller

    }

    public function index($Data)
    {
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        $CurrentDir = dirname(__FILE__) . "/";
        if($this->config->item('CallCenter') && strtolower(trim($this->config->item('CallCenter'))) == 'enabled'){
            $this->AdminSections[] = array("id" =>"admin_CallCenter", "label"=>"Call Center", "view"=>"core/tab_admin_callcenter");
        }
//        $appname = $this->config->item('MacantaAppName');
//        $AppsForNewLayout = [
//            'tr410',
//            'qj311-dm'
//        ];
//        if(in_array($appname,$AppsForNewLayout)){
//            $this->AdminSections[] = array("id" =>"admin_Automation", "label"=>"CD Automation", "view"=>"core/tab_admin_automation");
//
//        }
        $ToRecord = '';
        foreach($this->AdminSections as $Sections){
            $TimeStarted = time();
            $TabStatus[$Sections["id"]] = isset($Sections["status"]) ? $Sections["status"]:false;
            $TabLabel[$Sections["id"]] = $Sections["label"];
            $TabView[$Sections["id"]] = sanitize_output($this->load->view('core/tab_admin_tabs_lazy', $Sections, true));
            $TimeEnded = time();
            $TimeLapse = $TimeEnded - $TimeStarted;
            $ToRecord .= $Sections["label"] ." ". $TimeLapse . " | Size: ". strlen($TabView[$Sections["id"]] )/1000 ."\n";

        }
        //file_put_contents($CurrentDir . "_AdminTimeLapse_".$this->config->item('MacantaAppName').".txt", $ToRecord . "\n");

        $Prams['theClasses'] = $Classes;
        $Prams['menu'] = $TabLabel;
        $Prams['status'] = $TabStatus;
        $Prams['content'] = $TabView;
        $HTML =  $this->load->view('core/tab_admin', $Prams, true);
        return $HTML;
    }
    public function lazyLoader($Data){
        if($this->config->item('CallCenter') && strtolower(trim($this->config->item('CallCenter'))) == 'enabled'){
            $this->AdminSections[] = array("id" =>"admin_CallCenter", "label"=>"Call Center", "view"=>"core/tab_admin_callcenter");
        }
        foreach($this->AdminSections as $Sections){
            if($Data['title'] == $Sections["label"]){
                $Content = sanitize_output($this->load->view($Sections["view"], null, true));
            break;
            }
        }
        $this->ajaxResults['data'] = $Content;
        $this->ajaxResults['message'] = $Data['title'];
        $this->ajaxResults['script'] = '';
        $this->ajaxResults['status'] = 1;
        return $this->ajaxResults;
    }
    public function deleteEmailTemplate($data){
        $TemplateId = $data['TemplateId'];
        $this->db->where('Id',$TemplateId);
        $this->db->where('Type','custom');
        $this->db->delete("MacantaStripoEmail");
        return $this->getTemplateByEmailId($data);
    }
    public function getTemplateByEmailId($data, $type="custom"){
        macanta_check_macantastripo_email_table();
        $CurrentEmailId = $data['EmailId'];
        $this->db->select('Id, EmailId, Title, Version, Status, Updated, Created');
        $this->db->where('EmailId',$CurrentEmailId);
        $this->db->where('Type',$type);
        //$this->db->where('Status','disabled');
        $this->db->order_by('Title', 'ASC');
        $query = $this->db->get('MacantaStripoEmail');
        $Data = [];
        if ($query->num_rows() > 0){
            foreach ($query->result() as $Details)
            {
                $Details->Updated = date("Y-m-d H:i",strtotime($Details->Updated));
                $Details->Created = date("Y-m-d H:i",strtotime($Details->Created));
                $Data[] = $Details;
            }
        }
        $this->ajaxResults['data'] = $Data;
        $this->ajaxResults['message'] = "Total Template: ".$query->num_rows();
        $this->ajaxResults['script'] = '';
        $this->ajaxResults['status'] = 1;
        return $this->ajaxResults;
    }
    public function useEmailTempalteModal($data){
        macanta_check_macantastripo_email_table();
        $TemplateEmailId = $data['TemplateEmailId'];
        $CurrentEmailId = $data['CurrentEmailId'];
        $TemplateName = $data['TemplateName'];
        $Version = $data['Version'];

        // disable current active template
        $DBdata = [
                'Status' => 'disabled'
        ];
        $this->db->where('EmailId',$CurrentEmailId);
        $this->db->where('Status','active');
        $this->db->update('MacantaStripoEmail',$DBdata);

        //Duplicate $TemplateEmailId data
        $this->db->where('EmailId',$TemplateEmailId);
        $this->db->where('Title',$TemplateName);
        $this->db->where('Version',$Version);
        $query = $this->db->get('MacantaStripoEmail');
        $DetailsOfChosen = $query->row(0,'array');

        //Change columns value
        $DetailsOfChosen['Type'] = 'custom';
        $DetailsOfChosen['Status'] = 'active';
        $DetailsOfChosen['EmailId'] = $CurrentEmailId;
        unset($DetailsOfChosen['Id']);
        unset($DetailsOfChosen['Updated']);
        unset($DetailsOfChosen['Created']);

        //Check for duplicate, if there is, change version
        $this->db->where('EmailId',$CurrentEmailId);
        $this->db->where('Title',$DetailsOfChosen['Title']);
        $this->db->where('Type','custom');
        $query = $this->db->get('MacantaStripoEmail');
        $Id = 0;
        $message = "";
        if ($query->num_rows() > 0){

            foreach ($query->result() as $DetailsOfDuplicate)
            {
                if($DetailsOfDuplicate->TemplateHTML == $DetailsOfChosen['TemplateHTML'] &&
                    $DetailsOfDuplicate->TemplateCSS == $DetailsOfChosen['TemplateCSS']){
                    //theres existing template so use it
                    $Id = $DetailsOfDuplicate->Id;
                    break;
                }else{
                    $Version++;
                }

            }
            if($Id == 0){
                $message .= "New Version";
                $DetailsOfChosen['Version'] = $Version;
                $this->db->insert('MacantaStripoEmail', $DetailsOfChosen);
            }else{
                //use the last existing duplicate
                $message .= "Existing Template";
                $DBdata = [
                    'Status' => 'active'
                ];
                $this->db->where('Id',$Id);
                $this->db->update('MacantaStripoEmail',$DBdata);
            }

        }else{
            $message .= "New Template";
            $DetailsOfChosen['Version'] = $Version;
            $this->db->insert('MacantaStripoEmail', $DetailsOfChosen);
        }
        $this->ajaxResults['data'] = '';
        $this->ajaxResults['message'] = $message;
        $this->ajaxResults['script'] = '';
        $this->ajaxResults['status'] = 1;
        return $this->ajaxResults;
    }
    public function changeMacantaTagCategory($data){
        $catId =  $data['catId'];
        $OtherTags = infusionsoft_get_tags_by_cat_id($catId);
        $DBPermissionTags = json_decode($this->config->item('access_level'));
        $Prams['DBPermissionTags'] = $DBPermissionTags;
        $Prams['OtherTags'] = $OtherTags;
        $HTML =  $this->load->view('core/tab_admin_permissions-selections-admin-user', $Prams, true);
        $this->ajaxResults['message'] = 'changeMacantaTagCategory results';
        $this->ajaxResults['html'] = $HTML;
        $this->ajaxResults['script'] = "$('.admintagpicker').selectpicker('refresh')";
        return $this->ajaxResults;
    }
    public function saveSearchFilterTagPairs($data){
        $Pairs = $data["pairs"];
        $FilterNameArr = array();
        $SavePairsArr = array();
        $SavedSearch = array();
        //Restructure the saved search array
        $Filters = infusionsoft_get_all_saved_search();
        $Filters = sortMultiArray($Filters,'ReportStoredName');
        foreach($Filters as $SavedSearchItem){
            $SavedSearch[$SavedSearchItem->FilterName] = $SavedSearchItem;
        }
        foreach ($Pairs as $Pair){
            if($Pair['value']){
                $FilterNameArr[] = $Pair['value'];
                $TempFilter = $SavedSearch[$Pair['value']];
                $theTag = $this->getcreateTagByName(
                    $Pair['value'],
                    "savedsearch_permission_cat",
                    "macanta saved search permission",
                    "macanta >> saved search >> "
                );
                $SavePairsArr[$TempFilter->Id] = $theTag->Id; // this will be save in the database
            }
        }
        // update Databse
        $DBdata = array();
        $DBdata['value'] = json_encode($SavePairsArr);
        if(false == $this->DbExists('key','saved_search_restriction','config_data')){
            $DBdata['key'] = 'saved_search_restriction';
            $this->db->insert('config_data', $DBdata);
        }else{
            $this->db->where('key','saved_search_restriction');
            $this->db->update('config_data',$DBdata);
        }
        if(sizeof($SavePairsArr) > 0 && isset($data['session_name'])){
            $session_name = $data['session_name'];
            $user_seession_data = macanta_get_user_seession_data($session_name);
            $Session_data = unserialize($user_seession_data->session_data);
            //$this->ajaxResults['user_success'] = applyFn('set_macanta_user_success',$Session_data['email']);
        }
        $this->ajaxResults['message'] = '';
        $this->ajaxResults['data'] = $SavePairsArr;
        $this->ajaxResults['html'] = '';
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function saveNoteEditingPermissions($data){
        $Values = $data["Values"];
        // update Databse
        $DBdata = array();
        $DBdata['value'] = trim($Values);
        if(false == $this->DbExists('key','note_editing_permission','config_data')){
            $DBdata['key'] = 'note_editing_permission';
            $this->db->insert('config_data', $DBdata);
        }else{
            $this->db->where('key','note_editing_permission');
            $this->db->update('config_data',$DBdata);
        }
    }

    public function saveMacantaStripoEmail($data){
        macanta_check_macantastripo_email_table();
        $Result = false;
        $Action = '';
        if(isset($data["EmailId"])){
            $EmailId = $data["EmailId"];
            if(false == $this->DbExists('EmailId',$EmailId,'MacantaStripoEmail')){
                $Result = $this->db->insert('MacantaStripoEmail', $data);
                $Action = 'Insert';
            }else{
                $this->db->where('EmailId',$EmailId);
                $this->db->where('Status','active');
                $Result = $this->db->update('MacantaStripoEmail',$data);
                $Action = 'Update';
            }
        }else{
            $this->ajaxResults['message'] = 'Missing Required Field';
        }
        $this->ajaxResults['data'] = $data;
        $this->ajaxResults['result'] = $Result;
        $this->ajaxResults['action'] = $Action;
        return $this->ajaxResults;
    }
    public function macantaFieldaction($data)
    {
       $actionType = $data['actionType']; 
       $QueryId = $data['queryId'];
       $this->db->select('*');
       $this->db->where('QueryId',$QueryId);
       $fieldsdata = $this->db->get('ConnectedDataAutomation')->result_object();
       //echo '<pre>'; print_r(json_decode($fieldsdata[0]->Data)); exit;
       $fields = json_decode($fieldsdata[0]->Data);
       $GroupName = $fieldsdata[0]->ConnectedDataGroupName;
       //echo '<pre>'; print_r($fields); exit;
        $Params=[
            'actionType' => $actionType,
            'GroupName' => $GroupName,
            'fields' => $fields
        ];
       $fieldaction = $this->load->view('core/tab_admin_automation_field_actions', $Params, true);
       $this->ajaxResults['data'] = $data;
       $this->ajaxResults['queryCDFieldNameResult'] = $fields->queryCDFieldNameResult;
       $this->ajaxResults['queryCDFieldName1'] = $fields->queryCDFieldName1;
       $this->ajaxResults['queryCDFieldName2'] = $fields->queryCDFieldName2;
       $this->ajaxResults['queryFormatting'] = $fields->queryFormatting;
       $this->ajaxResults['action'] =  $fieldaction;
       return $this->ajaxResults;
    }
    public function convertMarkdown($data)
    {
        $helperText = $data['helpertext'];
        $Parsedown = new Parsedown();
        $returnText = $Parsedown->text($helperText);
        $this->ajaxResults['returnText'] = $returnText;
        return $this->ajaxResults;
    }
    public function macantaSendTestEmail($data){
        macanta_check_macantastripo_email_table();
        if(!empty($data["EmailId"]) && !empty($data["theContactId"])){
            $EmailId = $data["EmailId"];
            $ContactItemId = $data["theContactId"];
            $IdsArr = explode("|",$ContactItemId);
            $ContactId = $IdsArr[0];
            $ItemId = $IdsArr[1];
            $EmailDetails = macanta_db_record_exist('QueryId',$EmailId,'ConnectedDataAutomation', true);
            $EmailDetailsData = json_decode($EmailDetails->Data, true);
            //$Templates = macanta_db_record_exist('EmailId',$EmailId,'MacantaStripoEmail', true);
            $this->db->select('CompiledEmailHTML');
            $this->db->where('EmailId',$EmailId);
            $this->db->where('Status','active');
            $query = $this->db->get('MacantaStripoEmail');
            $Templates = $query->row();
            if($Templates == false){
                $this->ajaxResults['message'] = 'Please Create Email Template';
            }else{
                $Contact = infusionsoft_get_contact_by_id($ContactId, array('all'));
                if($Contact){
                    $toEmails = [
                        $Contact->Email =>  $Contact->FirstName." ".$Contact->LastName
                    ];
                    $CompiledEmailHtml = base64_decode($Templates->CompiledEmailHTML);
                    $CompiledEmailHtml = str_replace('~Email.Preview~',$EmailDetailsData['queryPreviewText'], $CompiledEmailHtml);
                    if($ItemId){
                        $ItemId = trim($ItemId);
                        $CDItem = macanta_db_record_exist('id',$ItemId,'connected_data', true);
                        if($CDItem){
                            $groupId = $CDItem->group;
                            $ConnectedInfosEncoded = macanta_get_config('connected_info');
                            if (!$ConnectedInfosEncoded) $ConnectedInfosEncoded = '[]';
                            $ConnectedInfos = json_decode($ConnectedInfosEncoded, true);
                            if(isset($ConnectedInfos[$groupId])){
                                $GroupName = $ConnectedInfos[$groupId]['title'];
                            }
                        }
                    }
                    else{
                        $ItemId = '';
                        $GroupName = '';
                    }
                    $EmailResults = macanta_send_email(
                            $EmailDetailsData['querySubject'],
                            $CompiledEmailHtml,
                            $toEmails,
                            $EmailDetailsData['queryFromAddress'],
                            $EmailDetailsData['queryFromName'],
                            $ContactId,
                            $GroupName,
                            $ItemId,
                        false

                    );
                    $this->ajaxResults['message'] = "Email Sent to ".$Contact->FirstName." ".$Contact->LastName." ({$Contact->Email})";
                    }else{
                    $this->ajaxResults['message'] = 'Contact Not Found: '.$ContactId;
                }
                }
        }else{
            $this->ajaxResults['message'] = 'Missing EmailId or Contact Id';
        }
        $this->ajaxResults['data'] = $data;
        $this->ajaxResults['result'] = $EmailResults;
        return $this->ajaxResults;
    }

    public function getcreateTagByName($Name, $CatKey ="savedsearch_permission_cat", $TagDesc = "macanta saved search permission" , $prefix = ''){
        $CatId =  $this->config->item($CatKey);
        $theTag = infusionsoft_get_tags_by_groupname_and_catId(trim($Name),$CatId);
        if(isset($theTag[0])){
            return $theTag[0];
        }else{
            $theTag = infusionsoft_get_tags_by_groupname_and_catId($prefix.trim($Name),$CatId);
            if(isset($theTag[0])){
                return $theTag[0];
            }
            $newTag = array();
            $newTag['GroupCategoryId'] = $CatId;
            $newTag['GroupName'] = $prefix.trim($Name);
            $newTag['GroupDescription']  = $TagDesc;
            $result  = infusionsoft_create_tag($newTag);
            $newTag['Id'] = $result->message;

            return json_decode(json_encode($newTag));
        }

    }
    public function saveMacantaAccess($data){
        $MacantaTagCat = $data['MacantaTagCat'];
        $MacantaAdminTag = $data['MacantaAdminTag'];
        $MacantaUserTag = $data['MacantaUserTag'];
        $MacantaContactViewPermissionCat = $data['MacantaContactViewPermissionCat'];
        $MacantaSavedSearchPermissionCat = $data['MacantaSavedSearchPermissionCat'];
        $values = array("administrator"=>"$MacantaAdminTag","staff"=>"$MacantaUserTag","member"=>"0");
        $DBdata = array();
        $DBdata['value'] = json_encode($values);
        $this->db->where('key','access_level');
        $this->db->update('config_data',$DBdata);

        $DBdata['value'] = $MacantaContactViewPermissionCat;
        $this->db->where('key','contactview_permission_cat');
        $this->db->update('config_data',$DBdata);

        $DBdata['value'] = $MacantaSavedSearchPermissionCat;
        $this->db->where('key','savedsearch_permission_cat');
        $this->db->update('config_data',$DBdata);


        $this->ajaxResults['message'] = 'config_data updated';
        $this->ajaxResults['html'] = "";
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function addContactTag($data){
        $tagId =  $data['tagId'];
        $ConId =  $data['ContactId'];
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $results = infusionsoft_apply_tag($ConId, $tagId);
        $searched_cache = manual_cache_loader('searched_cache' . $session_data['InfusionsoftID'],true);
        if ($searched_cache) {
            $searched_cache = json_decode($searched_cache, true);
            foreach ($searched_cache as $key => $Contact) {
                if ($Contact['Id'] == $session_data['InfusionsoftID']) {
                    //$OldContact = $Contact;
                    $TagArr = explode(',',$Contact['Groups']);
                    $TagArr[] = $tagId;
                    $Tags = implode(',',$TagArr);
                    $Contact['Groups'] = $Tags;
                    $searched_cache[$key] = $Contact;
                    break;
                }
            }
            manual_cache_writer('searched_cache' . $session_data['InfusionsoftID'], json_encode($searched_cache), 86400,true);
        }
        infusionsoft_get_tags_applied($ConId, true);
        $this->ajaxResults['message'] = $results;
        return $this->ajaxResults;
    }
    public function removeContactTag($data){
        $tagId =  $data['tagId'];
        $ConId =  $data['ContactId'];
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $results = infusionsoft_remove_tag($ConId, $tagId);
        $searched_cache = manual_cache_loader('searched_cache' . $session_data['InfusionsoftID'],true);
        if ($searched_cache) {
            $searched_cache = json_decode($searched_cache, true);
            foreach ($searched_cache as $key => $Contact) {
                if ($Contact['Id'] == $session_data['InfusionsoftID']) {
                    //$OldContact = $Contact;
                    $TagArr = explode(',',$Contact['Groups']);
                    $key = array_search($tagId,$TagArr);
                    unset($TagArr[$key]);
                    $Tags = implode(',',$TagArr);
                    $Contact['Groups'] = $Tags;
                    $searched_cache[$key] = $Contact;
                    break;
                }
            }
            manual_cache_writer('searched_cache' . $session_data['InfusionsoftID'], json_encode($searched_cache), 86400,true);
        }

        $this->ajaxResults['message'] = $results;
        return $this->ajaxResults;
    }
    public function refreshFileBox($data){
        $this->ajaxResults['script'] = '';
        $this->ajaxResults['message'] = infusionsoft_refresh_files($data);
        return $this->ajaxResults;
    }
    public function getInfusionsoftcontact($data)
    {
    	$searchKeyTemp = $data['query'];
    	$this->db->select('*');
        $this->db->like('FirstName',$searchKeyTemp);
        $this->db->or_like('LastName',$searchKeyTemp);
        $this->db->or_like('Email',$searchKeyTemp);
        $InfusionsoftContactName = $this->db->get('InfusionsoftContact')->result_object();
        $output = '<ul class="list-unstyled">';   
        if(!empty($InfusionsoftContactName))
        {
	        foreach($InfusionsoftContactName as $Contact){
	        
	         	$output .= '<li data-contactId='.$Contact->Id.'>'.$Contact->FirstName.' - '.$Contact->Email.'</li>'; 
	         }
        }
        else
        {
        	$output .= '<li>ContactName Not found.</li>';  
        }
        $output .= '</ul>';  
        echo $output;
    }
    public function getMacantauserPermissiontemplate()
    {
      $this->db->select('*');
      $this->db->where('Status', 'Active');
      $PermissionTemplatequery = $this->db->get('MacantaUserPermissionTemplates')->result_object();
      if(!empty($PermissionTemplatequery))
      {
      	$option = '<option value="None">Select User Permission Template</option>';
        foreach($PermissionTemplatequery as $template)
        {
          $option .= '<option value='.$template->TemplateId.'>'.$template->TemplateName.'</option>';
        }
      }
      else
      {
        $option = '<option value="None">First Add User Permission Template</option>';
      }
      echo $option;
      //echo '<pre>'; print_r($PermissionTemplatequery); exit;
      
    }
    public function saveuser($data)
    {
       $DBdata['ContactId'] = $data['ContactId'];
       $DBdata['ContactLevel'] = $data['UserLevel'];
       $DBdata['TemplateId'] = $data['UserTemplate'];
       $DBdata['Status'] = 'Active';
       $DBdata['Created'] = date('Y-m-d H:i:s');
       $DBdata['Updated'] = date('Y-m-d H:i:s');

       $this->db->select('*');
       $this->db->where('ContactId',$data['ContactId']);
       $checkmacantauserexistquery = $this->db->get('MacantaUser')->result_object();
       if(!empty($checkmacantauserexistquery))
       {
       	 $this->ajaxResults['status'] = 'false';
       	 $this->ajaxResults['script'] = '';
       	 $this->ajaxResults['message'] = 'This MacantaUser is already exist.';
       }
       else
       {
	       $macantauser = $this->db->insert('MacantaUser', $DBdata);
	       //$this->ajaxResults['script'] = '';
	       //$this->ajaxResults['message'] = macanta_get_user_data();

           //Send Webhook
           $this->db->select('FirstName, LastName, Email, Phone1, Company','Password');
           $this->db->where('Id',$data['ContactId']);
           $query = $this->db->get('InfusionsoftContact');
           $Details = $query->row(0,'array');
           if(empty($Details['Password'])){
               //Generate Password
               $GeneratedPassword = $this->generate_adjective() . "-" . $this->generate_noun();
               $GeneratedPassword = substr($GeneratedPassword, 0, 20);
               $this->db->where('Id',$data['ContactId']);
               $DBData = [
                   'Password' => $GeneratedPassword
               ];
               $this->db->update('InfusionsoftContact', $DBData);
           }
           $NewUsersLoginDetailsURL = "https://hooks.zapier.com/hooks/catch/6634434/omml0uz/";
           $NewUser = [
               'FirstName' => $Details['FirstName'],
               'Email' => $Details['Email'],
               'Password' => $GeneratedPassword,
               'App ID' => $this->config->item('MacantaAppName'),
               'Company' => $Details['Company'],
           ];
           $WebHookResults =  $this->sendWebHook($NewUsersLoginDetailsURL,$NewUser);
	       $this->ajaxResults['status'] = $macantauser;
           $this->ajaxResults['WebHookResults'] = $WebHookResults;
	       $this->ajaxResults['message'] = "saveMacantaQuery Results: ".json_encode($macantauser);
	       $this->ajaxResults['script'] = "<tr><td><label>".$data['ContactName']."</label></td><td><label>".$data['UserLevel']."</label></td><td><label>".$data['UserTemplateName']."</label></td><td><i class='fa fa-pencil-square-o EditMacantaUser' aria-hidden='true' data-usercontact-id='".$data['ContactId']."' data-template-id='".$data['UserTemplate']."'></i><i class='fa fa-trash-o DeleteMacantaUser' aria-hidden='true' data-usercontact-id='".$data['ContactId']."' data-template-id='".$data['UserTemplate']."'></i></td></tr>";
	       
	   }
       return $this->ajaxResults;
       //return $macantauser;
    }
    public function sendWebHook($URL,$DataPassed){
        $PostData = http_build_query($DataPassed);
        $opts = array(
            'http' => array(
                'method' => 'POST',
                'header' => "Accept-language: en\r\n" .
                    "Content-type: application/x-www-form-urlencoded\r\n"
            ,
                'content' => $PostData
            )
        );
        $context  = stream_context_create($opts);
        $Return = file_get_contents($URL, false, $context);
        return $Return;
    }
    public function generate_adjective()
    {

        $data = file_get_contents(dirname(__FILE__)."/adjective.json");
        $data = json_decode($data, true);
        $random_nr = rand(0, count($data["data"]));
        return $data["data"][$random_nr]['adjective'];

    }
    public function generate_noun()
    {

        $data = file_get_contents(dirname(__FILE__)."/noun.json");
        $data = json_decode($data, true);
        $random_nr = rand(0, count($data["data"]));
        return $data["data"][$random_nr]['noun'];

    }
    public function saveUserTemplate($data)
    {
      //echo "TemplateID=>".$data['templateId'];
      $this->db->select('*');
      if($data['templateId'] != '')
      {
        $this->db->where('TemplateId',$data['templateId']);
      }
      else if($data['templateName'] != '')
      {
        $this->db->where('TemplateName',$data['templateName']);
      }
      
      $checkmacantatemplateexistquery = $this->db->get('MacantaUserPermissionTemplates')->result_object();
      //echo '<pre>'; print_r($checkmacantatemplateexistquery);
      if(!empty($checkmacantatemplateexistquery))
      {
         $DBupdatedata = array();
      	 $DBupdatedata['TemplateName'] = $data['templateName'];
	     $DBupdatedata['TemplateDescription'] = $data['templateDesc'];
	     if(!empty($data['ParsedValues']))
	     {
	      	$DBupdatedata['TabAccess'] = $data['ParsedValues'];
	     }
	     
	     if(!empty($data['SectionParsedValues']))
	     {
	      	$DBupdatedata['SectionAccess'] = $data['SectionParsedValues'];
	     }
	     
	     if(!empty($data['MQBParsedValues']))
	     {
	      	$DBupdatedata['MQBAccess'] = $data['MQBParsedValues'];
	     }
	     
      	 $DBupdatedata['Updated'] = date('Y-m-d H:i:s');
         if($data['templateId'] != '')
         {
            $this->db->where('TemplateId',$data['templateId']);
         }
         //echo "UpdateData".'<pre>'; print_r($DBupdatedata); exit;
      	 $macantauserupdatetemplate = $this->db->update('MacantaUserPermissionTemplates', $DBupdatedata);
         //get User permission Template data
         $userPermisisontemplatgetquery = $this->db->get('MacantaUserPermissionTemplates')->result_object();
         
            //get uset details from MacantaUser table
            $macantaUsergetquery = $this->db->get('MacantaUser')->result_object();
            $DataMacantaUsermanagement = array();
            $usertr = "";
            foreach ($macantaUsergetquery as $row) 
            {
                //get FirstName of Infusionsoftcontact using Id 
                $ContactId = $row->ContactId;
                $UserLevel = $row->ContactLevel;
                $this->db->select('*');
                $this->db->where('Id', $ContactId);
                $contactquery = $this->db->get('InfusionsoftContact')->result_object();
                $contactName = $contactquery[0]->FirstName." - ".$contactquery[0]->Email;

                //get templateName of MacantaUserPermissionTemplates using TemplateId of MacantaUser
                $TemplateId =  $row->TemplateId;
                $this->db->select('TemplateName');
                $this->db->where('TemplateId', $TemplateId);
                $PermissionTemplatequery = $this->db->get('MacantaUserPermissionTemplates')->result_object();
                $templateName = $PermissionTemplatequery[0]->TemplateName; 

                $usertr .= "<tr><td><label>".$contactName."</label></td><td><label>".$UserLevel."</label></td><td><label>".$templateName."</label></td><td><i class='fa fa-pencil-square-o EditMacantaUser' aria-hidden='true' data-usercontact-id='".$ContactId."' data-template-id='".$TemplateId."'></i><i class='fa fa-trash-o DeleteMacantaUser' aria-hidden='true' data-usercontact-id='".$ContactId."' data-template-id='".$TemplateId."'></i></td></tr>";
          }
         //get uset details from MacantaUser table
         if(!empty($userPermisisontemplatgetquery))
         {
             $templatetr = "";
             foreach($userPermisisontemplatgetquery as $templateData) 
             {
                $templatetr .= '<tr><td><label>'.$templateData->TemplateName.'</label></td><td><label>'.$templateData->TemplateDescription.'</label></td><td><i class="fa fa-pencil-square-o EditUsertemplate" aria-hidden="true" data-template-id="'.$templateData->TemplateId.'"></i><i class="fa fa-trash-o DeleteUserTemplate" aria-hidden="true" 
                data-template-id="'.$templateData->TemplateId.'"></i></td></tr>';
             }
          }
          $this->ajaxResults['status'] = 'True';
          $this->ajaxResults['message'] = json_encode($userPermisisontemplatgetquery);
          $this->ajaxResults['templatescript'] = $templatetr;
          $this->ajaxResults['userscript'] = $usertr;
          return $this->ajaxResults;
      }
      else
      {
         $DBdata = array();
         $DBdata['TemplateName'] = $data['templateName'];
         $DBdata['TemplateDescription'] = $data['templateDesc'];
      	 if(!empty($data['ParsedValues']))
	     {
	      	$DBdata['TabAccess'] = $data['ParsedValues'];
	     }
	     else
	     {
	      	$DBdata['TabAccess'] = '';
	     }
	     if(!empty($data['SectionParsedValues']))
	     {
	      	$DBdata['SectionAccess'] = $data['SectionParsedValues'];
	     }
	     else
	     {
	      	$DBdata['SectionAccess'] = '';
	     }
	     if(!empty($data['MQBParsedValues']))
	     {
	      	$DBdata['MQBAccess'] = $data['MQBParsedValues'];
	     }
	     else
	     {
	      	$DBdata['MQBAccess'] = '';
	     }
	      
	     $DBdata['Status'] = 'Active';
	     $DBdata['Created'] = date('Y-m-d H:i:s');
	     $DBdata['Updated'] = date('Y-m-d H:i:s');
         //echo "InsertData=>".'<pre>'; print_r($DBdata);
	     $macantausertemplate = $this->db->insert('MacantaUserPermissionTemplates', $DBdata);
	     $template_id = $this->db->insert_id();
	     //get User permission Template data
         $userPermisisontemplatgetquery = $this->db->get('MacantaUserPermissionTemplates')->result_object();
         
         if(!empty($userPermisisontemplatgetquery))
         {
             $templatetr = "";
             foreach($userPermisisontemplatgetquery as $templateData) 
             {
                $templatetr .= '<tr><td>'.$templateData->TemplateName.'</td><td>'.$templateData->TemplateDescription.'</td><td><i class="fa fa-pencil-square-o EditUsertemplate" aria-hidden="true" data-template-id="'.$templateData->TemplateId.'"></i><i class="fa fa-trash-o DeleteUserTemplate" aria-hidden="true" 
                data-template-id="'.$templateData->TemplateId.'"></i></td></tr>';
             }
          }
          $this->ajaxResults['status'] = 'True';
          $this->ajaxResults['message'] = json_encode($userPermisisontemplatgetquery);
          $this->ajaxResults['templatescript'] = $templatetr;
         // $this->ajaxResults['userscript'] = $usertr;

	     /*$this->ajaxResults['status'] = $macantausertemplate;
	     $this->ajaxResults['message'] = "UpdateMacantaQuery Results: ".json_encode($macantausertemplate);
	     $this->ajaxResults['script'] = "<tr><td><label>".$data['templateName']."</label></td><td><label>".$data['templateDesc']."</label></td><td><i class='fa fa-pencil-square-o EditMacantaUser' aria-hidden='true' data-template-id='".$template_id."'></i>
          <i class='fa fa-trash-o DeleteMacantaUser' aria-hidden='true' data-template-id='".$template_id."'></i></td></tr>"; */
          return $this->ajaxResults;  
	  }
      
      
    }
    public function saveUserNewTemplate($data)
    {
      $DBdata['TemplateName'] = $data['templateName'];
      $DBdata['TemplateDescription'] = $data['templateDesc'];
      if(!empty($data['TabParsedValues']))
      {
         $DBdata['TabAccess'] = $data['TabParsedValues'];
      }
      else
      {
        $DBdata['TabAccess'] = '';
      }
      if(!empty($data['SectionParsedValues']))
      {
        $DBdata['SectionAccess'] = $data['SectionParsedValues'];
      }
      else
      {
        $DBdata['SectionAccess'] = '';
      }
      if(!empty($data['MQBParsedValues']))
      {
        $DBdata['MQBAccess'] = $data['MQBParsedValues'];
      }
      else
      {
        $DBdata['MQBAccess'] = '';
      }
          
      $DBdata['Status'] = 'Active';
      $DBdata['Created'] = date('Y-m-d H:i:s');
      $DBdata['Updated'] = date('Y-m-d H:i:s');

      $macantausertemplate = $this->db->insert('MacantaUserPermissionTemplates', $DBdata);
      $template_id = $this->db->insert_id();
     
      //get user details from MacantaUser table
      $this->ajaxResults['status'] = $macantausertemplate;
      $this->ajaxResults['message'] = "UpdateMacantaQuery Results: ".json_encode($macantausertemplate);
      $this->ajaxResults['templatescript'] = "<tr><td><label>".$data['templateName']."</label></td><td><label>".$data['templateDesc']."</label></td><td><i class='fa fa-pencil-square-o EditUsertemplate' aria-hidden='true' data-template-id='".$template_id."'></i>
          <i class='fa fa-trash-o DeleteUserTemplate' aria-hidden='true' data-template-id='".$template_id."'></i></td></tr>";  
      return $this->ajaxResults;

    }
    public function updateMacantaUser($data)
    {
        $DBdata['TemplateId'] = $data['templateId'];
        $contactId = $data['contactId'];
        $DBdata['ContactLevel'] = $data['UserLevel'];
        $DBdata['Updated'] = date('Y-m-d H:i:s');

        $this->db->where('ContactId', $contactId);
        $results = $this->db->update('MacantaUser',$DBdata);

        //get all MacantaUser
        $macantaUsergetquery = $this->db->get('MacantaUser')->result_object();
        $usertr = "";
        foreach ($macantaUsergetquery as $row) 
        {
            //get FirstName of Infusionsoftcontact using Id 
             $ContactId = $row->ContactId;
             $UserLevel = $row->ContactLevel;
             $this->db->select('*');
             $this->db->where('Id', $ContactId);
             $contactquery = $this->db->get('InfusionsoftContact')->result_object();
             $contactName = $contactquery[0]->FirstName." - ".$contactquery[0]->Email; 

             //get templateName of MacantaUserPermissionTemplates using TemplateId of MacantaUser
             $TemplateId =  $row->TemplateId;
             $this->db->select('TemplateName');
             $this->db->where('TemplateId', $TemplateId);
             $PermissionTemplatequery = $this->db->get('MacantaUserPermissionTemplates')->result_object();
             $templateName = $PermissionTemplatequery[0]->TemplateName; 

             $usertr .= "<tr><td><label>".$contactName."</label></td><td><label>".$UserLevel."</label></td><td><label>".$templateName."</label></td><td><i class='fa fa-pencil-square-o EditMacantaUser' aria-hidden='true' data-usercontact-id='".$ContactId."' data-template-id='".$TemplateId."'></i><i class='fa fa-trash-o DeleteMacantaUser' aria-hidden='true' data-usercontact-id='".$ContactId."' data-template-id='".$TemplateId."'></i></td></tr>";
        }   

        //get user details from MacantaUser table
        if(!empty($results))
        {
          $this->ajaxResults['status'] = $results;
          $this->ajaxResults['message'] = 'MacantaUser Updated Successfully.';
          $this->ajaxResults['userscript'] = $usertr;
          return $this->ajaxResults;
        }
    }
    public function editUserDataTemplatedata($data)
    {
      $templateId = $data['templateId'];
      $contactId = $data['contactId'];

      //get contactName from table InfusionsoftContact
      $this->db->select('*');
      $this->db->where('Id', $contactId);
      $contactquery = $this->db->get('InfusionsoftContact')->result_object();
      $contactName = $contactquery[0]->FirstName." - ".$contactquery[0]->Email; 

      //get templateName from table MacantaUserPermissionTemplates 
      $this->db->select('TemplateName');
      $this->db->where('TemplateId', $templateId);
      $templatenamequery = $this->db->get('MacantaUserPermissionTemplates ')->result_object();
      $TemplateName = $templatenamequery[0]->TemplateName; 

      $this->db->select('*');
      $this->db->where('ContactId',$data['contactId']);
      $results = $this->db->get('MacantaUser')->result_object();
     
      //get all templatelist
      $this->db->select('*');
      $this->db->where('Status', 'Active');
      $PermissionTemplatequery = $this->db->get('MacantaUserPermissionTemplates')->result_object();
      if(!empty($PermissionTemplatequery))
      {
        $option = '<option value="None">Select User Permission Template</option>';
        foreach($PermissionTemplatequery as $template)
        {
            if($template->TemplateId == $templateId)
            {
                $option .= '<option value='.$template->TemplateId.' selected="selected">'.$template->TemplateName.'</option>';
            }
            else
            {
                $option .= '<option value='.$template->TemplateId.'>'.$template->TemplateName.'</option>'; 
            }
        }
      }
      else
      {
        $option = '<option value="None">First Add User Permission Template</option>';
      }
     
      if(!empty($results))
      {
        $userlevel = $results[0]->ContactLevel;
        $userleveloption = '<option value="None">Select User Level</option>';
        if($userlevel != '')
        {   
             if($userlevel == 'Admin')
             {
                $userleveloption .=   '<option value="Admin" selected>Admin</option>'; 
                $userleveloption .=   '<option value="User">User</option>'; 
             }
             else if($userlevel == 'User')
             {
                $userleveloption .=   '<option value="User" selected>User</option>';
                $userleveloption .=   '<option value="Admin">Admin</option>';   
             }
             else
             {
                $userleveloption .=   '<option value="Admin">Admin</option> <option value="User">User</option>'; 
             }
        }
        else
        {
          $userleveloption .=   '<option value="Admin">Admin</option> <option value="User">User</option>';   
        }
        $this->ajaxResults['status'] = $results;
        $this->ajaxResults['message'] = json_encode($results);
        $this->ajaxResults['script'] = $option;
        $this->ajaxResults['username'] = $contactName;
        $this->ajaxResults['userlevelscript'] = $userleveloption;
         return $this->ajaxResults;
      }
      

    }
    public function getUserTemplate($data)
    {
      $templateId = $data['templateId'];
      $this->db->select('*');
      $this->db->where('TemplateName',$data['templateName']);
      if($templateId !="")
        $this->db->where('TemplateId != ',$templateId);
      $results = $this->db->get('MacantaUserPermissionTemplates')->result_object();
     // echo $this->db->last_query();
      
      if(!empty($results))
      {
        $this->ajaxResults['status'] = $results;
        $this->ajaxResults['message'] = 'This User Template is already exist.';
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
      }
      else
      {
        $this->ajaxResults['status'] = $results;
        $this->ajaxResults['message'] = '';
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
      }
    }
    public function editUserTemplate($data)
    {
      $DBdata['TemplateId'] = $data['templateId'];
 
      $this->db->select('*');
      $this->db->where('TemplateId',$data['templateId']);
      $results = $this->db->get('MacantaUserPermissionTemplates')->result_object();
      if(!empty($results))
      {
      	$this->ajaxResults['status'] = $results;
        $this->ajaxResults['message'] = json_encode($results);
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
      }
    }
    public function deleteMacantaUser($data)
    {
    	$contactId = $data['contactId'];
    	$templateId = $data['templateId'];
    	
    	$this->db->where('ContactId',$contactId);
    	$this->db->where('TemplateId',$templateId);
        $results = $this->db->delete('MacantaUser');

        $this->ajaxResults['status'] = $results;
        $this->ajaxResults['message'] = "saveMacantaQuery Results: ".json_encode($results);
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function deleteUserTemplate($data)
    {
    	$templateId = $data['templateId'];
    	
    	$this->db->where('TemplateId',$templateId);
        $results = $this->db->delete('MacantaUserPermissionTemplates');
        //get uset details from MacantaUser table
        $macantaUsergetquery = $this->db->get('MacantaUser')->result_object();
        $DataMacantaUsermanagement = array();
        $usertr = "";
        foreach ($macantaUsergetquery as $row) 
        {
           //get FirstName of Infusionsoftcontact using Id 
           $ContactId = $row->ContactId;
           $UserLevel = $row->ContactLevel;
           $this->db->select('*');
           $this->db->where('Id', $ContactId);
           $contactquery = $this->db->get('InfusionsoftContact')->result_object();
           $contactName = $contactquery[0]->FirstName." - ".$contactquery[0]->Email; 

           //get templateName of MacantaUserPermissionTemplates using TemplateId of MacantaUser
           $TemplateId =  $row->TemplateId;
           $this->db->select('TemplateName');
           $this->db->where('TemplateId', $TemplateId);
           $PermissionTemplatequery = $this->db->get('MacantaUserPermissionTemplates')->result_object();
           $templateName = $PermissionTemplatequery[0]->TemplateName; 

           $usertr .= "<tr><td><label>".$contactName."</label></td><td><label>".$UserLevel."</label></td><td><label>".$templateName."</label></td><td><i class='fa fa-pencil-square-o EditMacantaUser' aria-hidden='true' data-usercontact-id='".$ContactId."' data-template-id='".$TemplateId."'></i><i class='fa fa-trash-o DeleteMacantaUser' aria-hidden='true' data-usercontact-id='".$ContactId."' data-template-id='".$TemplateId."'></i></td></tr>";
        }    
        $this->ajaxResults['status'] = $results;
        $this->ajaxResults['message'] = "saveMacantaQuery Results: ".json_encode($results);
        $this->ajaxResults['userscript'] =  $usertr;
        return $this->ajaxResults;
    }
    public function refreshEmail($data){
        $this->ajaxResults['script'] = '';
        $this->ajaxResults['message'] = infusionsoft_refresh_emails();
        return $this->ajaxResults;
    }
    public function refreshSavedSearchTag(){
        $ForAutocompleteArr = array();
        $DBFilterTagPair = json_decode($this->config->item('saved_search_restriction'),true);
        $Filters = infusionsoft_get_all_saved_search(true);
        $Filters = sortMultiArray($Filters,'ReportStoredName');
        foreach($Filters as $SavedSearch){
            if(!isset($DBFilterTagPair[$SavedSearch->Id])){
                $ForAutocompleteArr[]='"'.$SavedSearch->FilterName.'"';
            }
        }
        $ForAutocomplete = "[".implode(',',$ForAutocompleteArr)."]";
        $this->ajaxResults['message'] = 'refreshSavedSearchTag results';
        $this->ajaxResults['script'] = "validOptions = $ForAutocomplete; SaveSearchAutoCompleteDestroy(); SaveSearchAutoComplete();";
        return $this->ajaxResults;
    }
    public function getSavedSearchTag($data){
        $PairNames = array();
        $ForAutocompleteArr = array();
        $DBFilterTagPair = json_decode($this->config->item('saved_search_restriction'),true);
        $Filters = infusionsoft_get_all_saved_search();
        $Filters = sortMultiArray($Filters,'ReportStoredName');
        foreach($Filters as $SavedSearch){
            if(isset($DBFilterTagPair[$SavedSearch->Id])){
                //$tempname = str_replace('macanta_','',$SavedSearch->FilterName);
                //$tempname = str_replace('macanta','',$tempname);
                $PairNames[] = array("name"=>trim($SavedSearch->FilterName),"filterid"=>$SavedSearch->Id, "tagid"=>$DBFilterTagPair[$SavedSearch->Id]);

            }else{
                $ForAutocompleteArr[]=$SavedSearch->FilterName;
            }
        }
        //$ForAutocomplete = "[".implode(',',$ForAutocompleteArr)."]";
        $ForAutocomplete = json_encode($ForAutocompleteArr);
        //$HTML =  $this->load->view('core/tab_admin_permissions-selections', $Prams, true);
        $this->ajaxResults['message'] = 'getSavedSearchTag results';
        $Prams['PairNames'] = $PairNames;
        $Prams['ForAutocomplete'] = $ForAutocomplete;
        $HTML =  $this->load->view('core/tab_admin_permissions-savedsearch', $Prams, true);
        $this->ajaxResults['html'] = $HTML;
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function getTagCatSelection(){

        $DBPermissionTags = json_decode($this->config->item('access_level'));
        $AdminTag = $DBPermissionTags->administrator;
        $AdminTagDetails = infusionsoft_get_tags($AdminTag);
        $CurrentMacantaTagCategoryId = $AdminTagDetails[0]->GroupCategoryId;
        $UserTag = $DBPermissionTags->staff;

        $UserTagDetails = infusionsoft_get_tags($UserTag);
        $OtherTags = infusionsoft_get_tags_by_cat_id($CurrentMacantaTagCategoryId);
        $TagCategories = infusionsoft_get_tags_category();
        $Prams['DBPermissionTags'] = $DBPermissionTags;
        $Prams['CurrentMacantaTagCategoryId'] = $CurrentMacantaTagCategoryId;
        $Prams['TagCategories'] = $TagCategories;
        $Prams['UserTagDetails'] = $UserTagDetails;
        $Prams['OtherTags'] = $OtherTags;
        $HTML =  $this->load->view('core/tab_admin_permissions-selections', $Prams, true);
        $this->ajaxResults['message'] = 'getTagCatSelection results';
        $this->ajaxResults['html'] = $HTML;
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function savePipelines($data){
        $DBdata['value'] = $data['Pipelines'];
        if($this->DbExists("key","OpportunityPipeline","config_data")){
            $this->db->where('key','OpportunityPipeline');
            $this->db->update('config_data',$DBdata);
        }else{
            $DBdata['key'] = 'OpportunityPipeline';
            $this->db->insert('config_data', $DBdata);
        }
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['data'] = $data;
        $this->ajaxResults['message'] = "saveCustomTabs Results Saved";
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function saveRelationships($data){
        $Temp = json_decode($data['Relationships'], true);
        $New = $Temp;
        foreach ($Temp as $index => $Relationships){
            if($Relationships['Id'] == '') $New[$index]['Id'] = macanta_generate_key('re_');
        }
        $DBdata['value'] = json_encode($New);
        if($this->DbExists("key","ConnectorRelationship","config_data")){
            $this->db->where('key','ConnectorRelationship');
            $this->db->update('config_data',$DBdata);
        }else{
            $DBdata['key'] = 'ConnectorRelationship';
            $this->db->insert('config_data', $DBdata);
        }
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['data'] = $data;
        $this->ajaxResults['message'] = "saveRelationships Saved";
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function setLanguage($data){
        $DBdata = array();
        $DBdata['value'] = $data['language'];
        $this->db->where('key','macanta_lang');
        $this->db->update('config_data',$DBdata);
        $this->ajaxResults['message'] = $data['language'];
        $this->lang->load('macanta', $data['language']);
        $all_lang_array = $this->lang->language;
        $JSscript = '';
        foreach($all_lang_array as $key=>$value){
            $value = json_encode(str_replace("'",'',$value));
            $JSscript .= '$("span.'.$key.'").html('.$value.');';
        }
        $this->ajaxResults['script']=$JSscript;
        //print_r($all_lang_array); // shows all the langauage line
        return $this->ajaxResults;
    }
    public function saveCustomTabs($data){

        // will add script tp create tag under access_permission_cat
        $DBdata = array();
        $DBdata['value'] = json_encode($data['CustomTabs']);
        $data['CustomTabsOffTagId'] = trim($data['CustomTabsOffTagId']);
        $CustomTabsOffTagId = $data['CustomTabsOffTagId'] ? $data['CustomTabsOffTagId']:"";
        $CustomTabOnTagId = $data['CustomTabOnTagId'] ? $data['CustomTabOnTagId']:"";
        $DBdata2['value'] = $CustomTabsOffTagId;
        $DBdata3['value'] = $CustomTabOnTagId;
        $oldCustomTabs = macanta_get_config('custom_tabs');
        $beforeProc =  false;
        // convert to array with key id
        $oldCustomTabs = json_decode($oldCustomTabs,true);
        $oldCustomTabsArr = array();
        $newCustomTabsArr = array();
        foreach($oldCustomTabs as $oldCustomTabKey => $oldCustomTabParam){
            $oldCustomTabsArr[$oldCustomTabParam['id']] = $oldCustomTabParam;
        }
        $newCustomTabs = json_decode($DBdata['value'],true);
        foreach($newCustomTabs as $newCustomTabKey => $newCustomTabParam){
            $newCustomTabsArr[$newCustomTabParam['id']] = $newCustomTabParam;
        }

        foreach($newCustomTabsArr as $tabId => $tabParam){
            if($newCustomTabsArr[$tabId]['permission']!=="ShowToAll"){
                if(array_key_exists($tabId,$oldCustomTabsArr)){

                    if(isset($oldCustomTabsArr[$tabId]['permission_tag']) && $oldCustomTabsArr[$tabId]['permission_tag'] != ""){
                        $newCustomTabsArr[$tabId]['permission_tag'] = $oldCustomTabsArr[$tabId]['permission_tag'];
                    }else{
                        // retrieve or create permission tag for this custom tab
                        if($beforeProc) sleep(1);
                        $theTag = $this->getcreateTagByName(
                            $newCustomTabsArr[$tabId]['title'],
                            "access_cat_id",
                            "macanta custom tab permission",
                            "macanta >> custom tab >> ");
                        $newCustomTabsArr[$tabId]['permission_tag'] = $theTag->Id;
                        $beforeProc = true;
                    }

                }else{
                    // this is a new custom tab with permission
                    // retrieve or create permission tag for this custom tab
                    if($beforeProc) sleep(1);
                    $theTag = $this->getcreateTagByName(
                        $newCustomTabsArr[$tabId]['title'],
                        "access_cat_id",
                        "macanta custom tab permission",
                        "macanta >> custom tab >> ");
                    $newCustomTabsArr[$tabId]['permission_tag'] = $theTag->Id;
                    $beforeProc = true;

                }
            }else{
                if(array_key_exists($tabId,$oldCustomTabsArr)){

                    if(isset($oldCustomTabsArr[$tabId]['permission_tag']) && $oldCustomTabsArr[$tabId]['permission_tag'] != ""){
                        $newCustomTabsArr[$tabId]['permission_tag'] = $oldCustomTabsArr[$tabId]['permission_tag'];
                    }else{
                        // retrieve or create permission tag for this custom tab
                        $newCustomTabsArr[$tabId]['permission_tag']="";
                    }

                }
            }
        }
        // Update the database
        $DBdata['value'] = json_encode($newCustomTabsArr);
        if($this->DbExists("key","custom_tabs","config_data")){
            $this->db->where('key','custom_tabs');
            $this->db->update('config_data',$DBdata);
        }else{
            $DBdata['key'] = 'custom_tabs';
            $this->db->insert('config_data', $DBdata);
        }
        if($this->DbExists("key","custom_tabs_tag","config_data")){
            $this->db->where('key','custom_tabs_tag');
            $this->db->update('config_data',$DBdata2);
        }else{
            $DBdata2['key'] = 'custom_tabs_tag';
            $this->db->insert('config_data', $DBdata2);
        }
        if($this->DbExists("key","custom_tabs_on_tag","config_data")){
            $this->db->where('key','custom_tabs_on_tag');
            $this->db->update('config_data',$DBdata3);
        }else{
            $DBdata3['key'] = 'custom_tabs_on_tag';
            $this->db->insert('config_data', $DBdata3);
        }
        if(sizeof($newCustomTabsArr) > 0 && isset($data['session_name'])){
            $session_name = $data['session_name'];
            $user_seession_data = macanta_get_user_seession_data($session_name);
            $Session_data = unserialize($user_seession_data->session_data);
            //$this->ajaxResults['user_success'] = applyFn('set_macanta_user_success',$Session_data['email']);
        }
        //get cd custom data from config_data on key condition
        if($this->DbExists("key","custom_tabs","config_data")){
          $this->db->select('*');
          $this->db->where('key', 'custom_tabs');
          $cd_data = $this->db->get('config_data')->result_object();
          
         
          $li = "";
          $cddata = json_decode($cd_data[0]->value);
          foreach($cddata as $k=>$val)
          {
            $li .= '<li class="form-group-item ui-sortable-handle" data-guid="'.$k.'">
             <div class="bullet-item"><span class="CustomTabListTitle" data-guid="'.$k.'">
             '.$val->title.'</span><button type="button" class="nb-btn btn-default removeButton"><i class="fa fa-trash-o" title="Delete Custom Tab"></i></button></div></li>';
          }
        }
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['data'] = $data;
        $this->ajaxResults['message'] = "saveCustomTabs Results: ".json_encode($theTag);
        $this->ajaxResults['script'] = $li;
        return $this->ajaxResults;
    }
    public function saveCdCustomTabs($data){
        //echo '<pre>'; print_r($data); exit;
        // will add script tp create tag under access_permission_cat
        $DBdata = array();
        $DBdata['value'] = json_encode($data['CustomTabs']);
        $data['CustomTabsOffTagId'] = trim($data['CustomTabsOffTagId']);
        $CustomTabsOffTagId = $data['CustomTabsOffTagId'] ? $data['CustomTabsOffTagId']:"";
        $CustomTabOnTagId = $data['CustomTabOnTagId'] ? $data['CustomTabOnTagId']:"";
        $DBdata2['value'] = $CustomTabsOffTagId;
        $DBdata3['value'] = $CustomTabOnTagId;
        $oldCustomTabs = macanta_get_config('cd_custom_tabs');
        $beforeProc =  false;
        // convert to array with key id
        $oldCustomTabs = json_decode($oldCustomTabs,true);
        $oldCustomTabsArr = array();
        $newCustomTabsArr = array();
        foreach($oldCustomTabs as $oldCustomTabKey => $oldCustomTabParam){
            $oldCustomTabsArr[$oldCustomTabParam['id']] = $oldCustomTabParam;
        }
        $newCustomTabs = json_decode($DBdata['value'],true);
        foreach($newCustomTabs as $newCustomTabKey => $newCustomTabParam){
            $newCustomTabsArr[$newCustomTabParam['id']] = $newCustomTabParam;
        }

        foreach($newCustomTabsArr as $tabId => $tabParam){
            if($newCustomTabsArr[$tabId]['permission']!=="ShowToAll"){
                if(array_key_exists($tabId,$oldCustomTabsArr)){

                    if(isset($oldCustomTabsArr[$tabId]['permission_tag']) && $oldCustomTabsArr[$tabId]['permission_tag'] != ""){
                        $newCustomTabsArr[$tabId]['permission_tag'] = $oldCustomTabsArr[$tabId]['permission_tag'];
                    }else{
                        // retrieve or create permission tag for this custom tab
                        if($beforeProc) sleep(1);
                        $theTag = $this->getcreateTagByName(
                            $newCustomTabsArr[$tabId]['title'],
                            "access_cat_id",
                            "macanta cd custom tab permission",
                            "macanta >> cd custom tab >> ");
                        $newCustomTabsArr[$tabId]['permission_tag'] = $theTag->Id;
                        $beforeProc = true;
                    }

                }else{
                    // this is a new custom tab with permission
                    // retrieve or create permission tag for this custom tab
                    if($beforeProc) sleep(1);
                    $theTag = $this->getcreateTagByName(
                        $newCustomTabsArr[$tabId]['title'],
                        "access_cat_id",
                        "macanta cd custom tab permission",
                        "macanta >> cd custom tab >> ");
                    $newCustomTabsArr[$tabId]['permission_tag'] = $theTag->Id;
                    $beforeProc = true;

                }
            }else{
                if(array_key_exists($tabId,$oldCustomTabsArr)){

                    if(isset($oldCustomTabsArr[$tabId]['permission_tag']) && $oldCustomTabsArr[$tabId]['permission_tag'] != ""){
                        $newCustomTabsArr[$tabId]['permission_tag'] = $oldCustomTabsArr[$tabId]['permission_tag'];
                    }else{
                        // retrieve or create permission tag for this custom tab
                        $newCustomTabsArr[$tabId]['permission_tag']="";
                    }

                }
            }
        }
        // Update the database
        $DBdata['value'] = json_encode($newCustomTabsArr);
        if($this->DbExists("key","cd_custom_tabs","config_data")){
            $this->db->where('key','cd_custom_tabs');
            $this->db->update('config_data',$DBdata);
        }else{
            $DBdata['key'] = 'cd_custom_tabs';
            $this->db->insert('config_data', $DBdata);
        }
        if($this->DbExists("key","cd_custom_tabs_tag","config_data")){
            $this->db->where('key','cd_custom_tabs_tag');
            $this->db->update('config_data',$DBdata2);
        }else{
            $DBdata2['key'] = 'cd_custom_tabs_tag';
            $this->db->insert('config_data', $DBdata2);
        }
        if($this->DbExists("key","cd_custom_tabs_on_tag","config_data")){
            $this->db->where('key','cd_custom_tabs_on_tag');
            $this->db->update('config_data',$DBdata3);
        }else{
            $DBdata3['key'] = 'cd_custom_tabs_on_tag';
            $this->db->insert('config_data', $DBdata3);
        }
        if(sizeof($newCustomTabsArr) > 0 && isset($data['session_name'])){
            $session_name = $data['session_name'];
            $user_seession_data = macanta_get_user_seession_data($session_name);
            $Session_data = unserialize($user_seession_data->session_data);
            //$this->ajaxResults['user_success'] = applyFn('set_macanta_user_success',$Session_data['email']);
        }
        //get cd custom data from config_data on key condition
        if($this->DbExists("key","cd_custom_tabs","config_data")){
          $this->db->select('*');
          $this->db->where('key', 'cd_custom_tabs');
          $cd_data = $this->db->get('config_data')->result_object();
          
         
          $li = "";
          $cddata = json_decode($cd_data[0]->value);
          foreach($cddata as $k=>$val)
          {
            $li .= '<li class="form-group-item ui-sortable-handle" data-guid="'.$k.'">
             <div class="bullet-item"><span class="CustomTabListTitle" data-guid="'.$k.'">
             '.$val->title.'</span><button type="button" class="nb-btn btn-default removeButton"><i class="fa fa-trash-o" title="Delete Custom Tab"></i></button></div></li>';
          }
        }
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['data'] = $data;
        $this->ajaxResults['message'] = "saveCdCustomTabs Results: ".json_encode($theTag);
        $this->ajaxResults['script'] = $li;
        return $this->ajaxResults;
    }
    public function removeDuplicatedFields($Fields){
        $ExistingFieldIds = [];
        $NewFields = $Fields;
        foreach ($Fields as $Index=>$Field){
            if(!in_array($Field['fieldId'],$ExistingFieldIds))
            {
                $ExistingFieldIds[] = $Field['fieldId'];
            }
            else
            {
                unset($NewFields[$Index]);
            }

        }
        return $NewFields;
    }
    public function saveConnectedInfos($data){

        $SystemConnectedInfos = json_decode($data['ConnectedInfos'], true);

        //echo '<pre>'; print_r($SystemConnectedInfos); exit;

        $oldCustomTabs = macanta_get_config('connected_info');
        $beforeProc =  false;
        // convert to array with key id
        $oldCustomTabs = json_decode($oldCustomTabs,true);
        $oldCustomTabsArr = array();
        $newCustomTabsArr = array();
        foreach($oldCustomTabs as $oldCustomTabKey => $oldCustomTabParam){
            $oldCustomTabsArr[$oldCustomTabParam['id']] = $oldCustomTabParam;
        }
        foreach($SystemConnectedInfos as $newCustomTabKey => $newCustomTabParam){
            $newCustomTabsArr[$newCustomTabParam['id']] = $newCustomTabParam;
        }

        foreach($newCustomTabsArr as $tabId => $tabParam){
            $newCustomTabsArr[$tabId]['fields'] = $this->removeDuplicatedFields($newCustomTabsArr[$tabId]['fields']);
            if($newCustomTabsArr[$tabId]['visibility']!=="ShowCDToAll"){
                if(array_key_exists($tabId,$oldCustomTabsArr)){

                    if(isset($oldCustomTabsArr[$tabId]['permission_tag']) && $oldCustomTabsArr[$tabId]['permission_tag'] != ""){
                        $newCustomTabsArr[$tabId]['permission_tag'] = $oldCustomTabsArr[$tabId]['permission_tag'];
                    }else{
                        // retrieve or create permission tag for this custom tab
                        if($beforeProc) sleep(1);
                        $theTag = $this->getcreateTagByName(
                            $newCustomTabsArr[$tabId]['title'],
                            "access_cat_id",
                            "macanta custom tab permission",
                            "macanta >> connected info >> ");
                        $newCustomTabsArr[$tabId]['permission_tag'] = $theTag->Id;
                        $beforeProc = true;
                    }

                }else{
                    // this is a new custom tab with permission
                    // retrieve or create permission tag for this custom tab
                    if($beforeProc) sleep(1);
                    $theTag = $this->getcreateTagByName(
                        $newCustomTabsArr[$tabId]['title'],
                        "access_cat_id",
                        "macanta custom tab permission",
                        "macanta >> connected info >> ");
                    $newCustomTabsArr[$tabId]['permission_tag'] = $theTag->Id;
                    $beforeProc = true;

                }
            }
            else{
                if(array_key_exists($tabId,$oldCustomTabsArr)){

                    if(isset($oldCustomTabsArr[$tabId]['permission_tag']) && $oldCustomTabsArr[$tabId]['permission_tag'] != ""){
                        $newCustomTabsArr[$tabId]['permission_tag'] = $oldCustomTabsArr[$tabId]['permission_tag'];
                    }else{
                        // retrieve or create permission tag for this custom tab
                        $newCustomTabsArr[$tabId]['permission_tag']="";
                    }

                }else{
                    $newCustomTabsArr[$tabId]['permission_tag']="";
                }
            }
        }



        $DBdata = array();
        $DBdata['value'] = json_encode($newCustomTabsArr);
        if($DBdata['value'] != '[]'){
            if($this->DbExists("key","connected_info","config_data")){
                $this->db->where('key','connected_info');
                $this->db->update('config_data',$DBdata);
            }else{
                $DBdata['key'] = 'connected_info';
                $this->db->insert('config_data', $DBdata);
            }
        }








        if(sizeof($data['ConnectedInfos']) > 0 && isset($data['session_name'])){
            $session_name = $data['session_name'];
            $user_seession_data = macanta_get_user_seession_data($session_name);
            $Session_data = unserialize($user_seession_data->session_data);
            //$this->ajaxResults['user_success'] = applyFn('set_macanta_user_success',$Session_data['email']);
        }



        //Update Contacts Connected Info
        $GUID = [];
        $ConInfo = [];
        foreach ($SystemConnectedInfos as $index => $SystemConnectedInfo){
            $GUID[] = $SystemConnectedInfo['id'];
            foreach ($SystemConnectedInfo['fields'] as $FieldIndex => $Field){
                $ConInfo[$SystemConnectedInfo['id']][] = $Field['fieldId'];
            }

        }
        $DBdata = array();
        $this->db->where('meta_key','connected_info');
        $query = $this->db->get('users_meta');
        if ($query->num_rows() > 0){

            foreach ($query->result() as $row)
            {
                $values =  json_decode($row->meta_value, true);
                $user_id = $row->user_id;
                foreach ($values as $theGUID => $Items){
                    if(!in_array($theGUID, $GUID)){
                        unset($values[$theGUID]);
                    }else{
                        foreach ($Items as $ItemId=>$Fields){
                            foreach ($Fields as $FieldId => $FieldValue){
                                if(!in_array($FieldId, $ConInfo[$theGUID])) unset($values[$theGUID][$ItemId][$FieldId]);
                                if(sizeof($values[$theGUID][$ItemId]) == 0) unset($values[$theGUID][$ItemId]);
                                if(sizeof($values[$theGUID]) == 0) unset($values[$theGUID]);
                            }
                        }
                    }
                }
                $NewValue = json_encode($values);
                $this->db->where('user_id',$user_id);
                $this->db->where('meta_key','connected_info');
                $DBdata['meta_value'] = $NewValue;
                $this->db->update('users_meta', $DBdata);
            }
        }


        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['GUID'] = $GUID;
        $this->ajaxResults['ConInfo'] = $ConInfo;
        $this->ajaxResults['data'] = json_decode($data['ConnectedInfos']);
        $this->ajaxResults['message'] = "Saved";
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function saveUsersCallerId($data){
        $UsersCallerIds = $data['UsersCallerIds'];
        $UsersCallerIds = json_decode($UsersCallerIds, true);
        $CallerIds = [];
        $ContactId = 0;
        foreach ($UsersCallerIds as $field){
            if($field['name'] == "UserId"){
                $ContactId = $field['value'];
            }elseif ($field['name'] == "CallerId"){
                $CallerIds[] = $field['value'];
            }
        }
        $this->db->where('user_id',$ContactId);
        $this->db->where('meta_key','caller_id');
        $query = $this->db->get('users_meta');
        $row = $query->row();
        if (isset($row))
        {
            $this->db->where('user_id',$ContactId);
            $this->db->where('meta_key','caller_id');
            $DBdata['meta_value'] = json_encode($CallerIds);
            $this->db->update('users_meta', $DBdata);
        }else{
            $DBdata['user_id'] = $ContactId;
            $DBdata['meta_key'] = 'caller_id';
            $DBdata['meta_value'] = json_encode($CallerIds);
            $this->db->insert('users_meta', $DBdata);
        }
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['data'] = json_encode(getUsersCallerIds());
        $this->ajaxResults['message'] = "saveUsersCallerId Results: ".json_encode($CallerIds);
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function saveCustomJS($data){
        $content = $data['content'];
        $this->db->where('key','CustomJS');
        $query = $this->db->get('config_data');
        $row = $query->row();
        $DBdata = [];
        if (isset($row))
        {
            $this->db->where('key','CustomJS');
            $DBdata['value'] = $content;
            $result = $this->db->update('config_data', $DBdata);
        }else{
            $DBdata['key'] = 'CustomJS';
            $DBdata['value'] = $content;
            $result = $this->db->insert('config_data', $DBdata);
        }
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['message'] = $result;
        return $this->ajaxResults;
    }
    public function saveCustomCSS($data){
        $content = $data['content'];
        $this->db->where('key','CustomCSS');
        $query = $this->db->get('config_data');
        $row = $query->row();
        $DBdata = [];
        if (isset($row))
        {
            $this->db->where('key','CustomCSS');
            $DBdata['value'] = $content;
            $result = $this->db->update('config_data', $DBdata);
        }else{
            $DBdata['key'] = 'CustomCSS';
            $DBdata['value'] = $content;
            $result = $this->db->insert('config_data', $DBdata);
        }
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['message'] = $result;
        return $this->ajaxResults;
    }
    public function insert_update($table,$keyName,$keyValue,$fieldName,$fieldValue, $valueToJson = false){
        //todo:finish this function
        $this->db->where($keyName,$keyValue);
        $query = $this->db->get($table);
        $row = $query->row();
        if (isset($row))
        {
            $existingDevices = json_decode($row->value,true);
            if(!in_array($deviceNumber,$existingDevices)){
                $existingDevices[] = $deviceNumber;
                $this->db->where($keyName,$keyValue);
                $DBdata['value'] = json_encode($existingDevices);
                $this->db->update($table, $DBdata);
            }

        }else{
            $existingDevices[] = $deviceNumber;
            $DBdata['key'] = 'outbound_devices';
            $DBdata['value'] = json_encode($existingDevices);
            $this->db->insert($table, $DBdata);
        }
    }
    public function deleteCallerId($data){
        $Twilio_Account_SID = $this->config->item('Twilio_Account_SID');
        $Twilio_TOKEN = $this->config->item('Twilio_TOKEN');

        try {
            $client = new Twilio\Rest\Client($Twilio_Account_SID, $Twilio_TOKEN);
            $client->outgoingCallerIds($data['SID'])->delete();
            $this->db->where('key','outbound_devices');
            $query = $this->db->get('config_data');
            $row = $query->row();
            if (isset($row))
            {
                if($row->value !== "[]" && $row->value !== ""){
                    $existingDevices = json_decode($row->value,true);
                    $temp = [];
                    foreach ($existingDevices as $existingDevice){
                        if($existingDevice == $data['phone']) continue;
                        $temp[] = $existingDevice;
                    }
                    $existingDevices = $temp;
                    $this->db->where('key','outbound_devices');
                    $DBdata['value'] = json_encode($existingDevices);;
                    $this->db->update('config_data', $DBdata);
                }
            }
            $this->ajaxResults['message'] = "deleted";

        } catch (Twilio\Exceptions\RestException $e) {
            $this->ajaxResults['message'] = $e->getMessage();
        }

        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['data'] = "";
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function saveCallerIdLabel($data){
        $Twilio_Account_SID = $this->config->item('Twilio_Account_SID');
        $Twilio_TOKEN = $this->config->item('Twilio_TOKEN');
        try {
            $client = new Twilio\Rest\Client($Twilio_Account_SID, $Twilio_TOKEN);
            $caller_id = $client
                ->outgoingCallerIds($data['SID'])
                ->update(
                    array("FriendlyName" => $data['Label'])
                );
            //echo $caller_id->phone_number;
            $deviceNumber = $caller_id->phoneNumber;
            $this->db->where('key','outbound_devices');
            $query = $this->db->get('config_data');
            $row = $query->row();
            $existingDevices = [];
            if (isset($row))
            {
                $row->value = $row->value == "" ? "[]":$row->value;
                $existingDevices = json_decode($row->value,true);
                if($data['SetAsDevice'] === 'yes') {
                    if(!in_array($deviceNumber,$existingDevices)){
                        $existingDevices[] = $deviceNumber;
                    }
                }else{
                    $temp = [];
                    foreach ($existingDevices as $existingDevice){
                        if($existingDevice == $deviceNumber) continue;
                        $temp[] = $existingDevice;
                    }
                    $existingDevices = $temp;
                }
                $this->db->where('key','outbound_devices');
                $DBdata['value'] = json_encode($existingDevices);
                $this->db->update('config_data', $DBdata);
            }else{
                if($data['SetAsDevice'] == 'yes') {
                    $existingDevices[] = $deviceNumber;
                    $DBdata['key'] = 'outbound_devices';
                    $DBdata['value'] = json_encode($existingDevices);
                    $this->db->insert('config_data', $DBdata);
                }

            }

        } catch (Twilio\Exceptions\RestException $e) {
            $this->ajaxResults['message'] = $e->getMessage();;
        }
        $this->ajaxResults['existingDevices'] = $existingDevices;
        $this->ajaxResults['deviceNumber'] = $deviceNumber;
        $this->ajaxResults['json_decode'] = json_decode($row->value);
        $this->ajaxResults['script'] = '';
        return $this->ajaxResults;
    }
    public function makeDefaultCallerId($data){
        $PhoneNumber = $data['PhoneNumber'];
        $this->config->set_item('macanta_caller_id',$PhoneNumber);
        $macanta_caller_id = macanta_db_record_exist('key','macanta_caller_id','config_data');
        $DBData = ['value' => $PhoneNumber];
        if($macanta_caller_id == false){
            $DBData['key'] = 'macanta_caller_id';
            $this->db->insert('config_data', $DBData);

        }else{
            $this->db->where('key','macanta_caller_id');
            $this->db->update('config_data', $DBData);
        }
        ob_start();
        //Get All Available Company Caller ID
        $account_sid = $this->config->item('Twilio_Account_SID');
        $auth_token = $this->config->item('Twilio_TOKEN');
        try {
            $client = new Twilio\Rest\Client($account_sid, $auth_token);
            $CompanyCallerIds = $client->outgoingCallerIds->read();

        } catch (Twilio\Exceptions\RestException $e) {
            $CompanyCallerIds = [];
            echo '<span style="    color: red; font-size: 14px; text-align: left; width: 100%; line-height: 16px !important; display: inherit;">Sorry, Twilio Communication System Down, Details:'.$e->getMessage().'</span>';

        }
        $CompanyCallerIdsTemp = [];
        foreach ($CompanyCallerIds as $CallerId){
            $CompanyCallerIdsTemp[$CallerId->friendlyName] = $CallerId;
        }
        ksort($CompanyCallerIdsTemp);
        $CompanyCallerIds = [];
        foreach ($CompanyCallerIdsTemp as $friendly_name => $CallerId){
            $CompanyCallerIds[] = $CallerId;
        }
        $DefaultCallerId = $this->config->item('macanta_caller_id') ;
        ?>
        <div class="CallerIdItem">
            <div class="form-group default">
                <input class="AllowUserCallerId  "  type="checkbox" id="CallerIdDefault" name="CallerIdDefault"   autocomplete="off" disabled checked />
                <div class="btn-group">
                    <label for="CallerIdDefault" class="btn btn-default ">
                        <span class="fa fa-check"></span>
                        <span> </span>
                    </label>
                    <label for="CallerIdDefault" class="btn btn-default checkbox-label active ">
                        <div class="MacantaCallerIdTitle">
                            System Default
                        </div>
                        <div class="MacantaCallerIdNumber">
                            <?php echo $DefaultCallerId ?  $DefaultCallerId:"No Default Caller ID"?>
                        </div>
                    </label>
                </div>
            </div>
        </div>
        <form id="MacantaCallerIdList" method="post" class="form-horizontal MacantaCallerIdList dynamic"
              _lpchecked="1">
            <input type="hidden" name="UserId"  class="UserId">
            <ul class="itemContainer">
                <?php
                if($this->config->item('outbound_devices')){
                    $SystemOuboundDevices = json_decode($this->config->item('outbound_devices'), true);
                }else{
                    $SystemOuboundDevices = [];
                }
                foreach ($CompanyCallerIds as $CallerId) {
                    if($DefaultCallerId == $CallerId->phoneNumber) continue;
                    $friendly_name = $CallerId->friendlyName;
                    $friendly_name = $CallerId->phoneNumber == $friendly_name ? "<em>No Label</em>":$friendly_name;
                    $Id = $CallerId->sid;
                    $data =  'data-callerid="'.$CallerId->phoneNumber.'"';
                    $info = "hide";
                    if(in_array($CallerId->phoneNumber, $SystemOuboundDevices)) $info = "";
                    $isdevice = $info == "hide" ? 'no':'yes';
                    $fwidth = $info == "hide" ? 'fwidth':'';
                    ?>
                    <li class="form-group-item" data-calleridtype="user-defined"  data-sid="<?php echo $CallerId->sid; ?>" data-guid="<?php echo $CallerId->phoneNumber; ?>">
                        <div class="CallerIdItem">
                            <div class="form-group success">
                                <input class="AllowUserCallerId UserCallerIdToEnable" value="<?php echo $CallerId->phoneNumber; ?>" data-callerid="<?php echo $CallerId->phoneNumber; ?>" type="checkbox" name="CallerId" id="callerId-<?php echo $Id; ?>" autocomplete="off" disabled   />
                                <div class="btn-group">
                                    <label for="callerId-<?php echo $Id; ?>" class="btn btn-success toggleCallerId">
                                        <span class="fa fa-check"></span>
                                        <span> </span>
                                    </label>
                                    <label class="btn btn-default checkbox-label active ">
                                        <div class="MacantaCallerIdTitle <?php echo $Id; ?> <?php echo $fwidth; ?>" id="label<?php echo $Id; ?>" title="<?php echo $friendly_name ?>">
                                            <?php echo $friendly_name ?>
                                        </div>
                                        <div class="MacantaCallerIdNumber  <?php echo $Id; ?> <?php echo $fwidth; ?>">
                                            <div class="the-number">
                                                <?php echo $CallerId->phoneNumber ?>
                                            </div>
                                            <div class="the-info">
                                                            <span class="info-as-device-<?php echo $Id; ?>">
                                                                    <a class="info-as-device <?php echo $info; ?>">Can be used as a call device</a>
                                                                </span>
                                            </div>
                                        </div>
                                    </label>
                                    <i class="fa fa-pencil-square-o EditCallerIdLabelIcon CallerIDAction" aria-hidden="true"
                                       data-sid="<?php echo $CallerId->sid ?>"
                                       data-callerid="<?php echo $CallerId->phoneNumber ?>"
                                       data-label="<?php echo $CallerId->friendlyName ?>"
                                       data-isdevice="<?php echo $isdevice; ?>"></i>
                                    <i class="fa fa-trash-o DeleteCallerIdIcon CallerIDAction" aria-hidden="true"
                                       data-sid="<?php echo $CallerId->sid ?>"
                                       data-phone="<?php echo $CallerId->phoneNumber ?>"
                                    ></i>
                                </div>
                            </div>


                        </div>

                    </li>
                    <?php
                }
                ?>

            </ul>

        </form>
        <?php
        $html = ob_get_contents();
        ob_end_clean();
        $this->ajaxResults['html_content'] = $html;
        $this->ajaxResults['script'] = '';
        return $this->ajaxResults;

    }
    public function addCallerId($data)
    {   //"ExtNumber":ExtNumber, "PhoneNumber":PhoneNumber, 'Label':LabelToVerify
        $Twilio_Account_SID = $this->config->item('Twilio_Account_SID');
        $Twilio_TOKEN = $this->config->item('Twilio_TOKEN');
        try {
            $client = new Twilio\Rest\Client($Twilio_Account_SID, $Twilio_TOKEN);
            $file_random_hash = time() . rand(10000, 99990);
            $this->ajaxResults['file_id'] = $file_random_hash;

        } catch (Twilio\Exceptions\RestException $e) {
            $client = false;
            $this->ajaxResults['message'] = $e->getMessage();
        }
        if($client != false){
            try {
                $validationRequest = $client->validationRequests->create(
                    $data['PhoneNumber'],
                    array(
                        "friendlyName" => $data['Label'],
                        "extension" => $data['ExtNumber'],
                        "statusCallback" => "https://macanta.org/twilio/post.php?from=$file_random_hash"
                    )
                );
                $this->ajaxResults['message'] = [
                    "validationCode"=>$validationRequest->__get('validationCode'),
                    'friendlyName' =>$validationRequest->__get('friendlyName')
                ];
            } catch (Exception $e) {
                $this->ajaxResults['message'] = 'already_in';
            }
        }

        return $this->ajaxResults;
    }
    public function checkAddCallerId($data)
    {   //"ExtNumber":ExtNumber, "PhoneNumber":PhoneNumber, "file_id":e.file_id, 'Label':LabelToVerify
        $found = false;
        $file_path = "https://macanta.org/twilio/" . $data['file_id'] . ".txt";
        $count = 0;
        $HTML = '';
        $SetAsDevice = $data['SetAsDevice'];
        if($this->config->item('outbound_devices')){
            $SystemOuboundDevices = json_decode($this->config->item('outbound_devices'), true);
        }else{
            $SystemOuboundDevices = [];
        }
        if($SetAsDevice == "yes") $SystemOuboundDevices[] = $data['PhoneNumber'];
        while (!$found) {
            $fileData = @file_get_contents($file_path);
            if ($fileData) {
                $found = true;
                $existingDevices = [];
                if($SetAsDevice == 'yes') {
                    $deviceNumber = $data['PhoneNumber'];
                    $this->db->where('key','outbound_devices');
                    $query = $this->db->get('config_data');
                    $row = $query->row();
                    if (isset($row))
                    {
                        $existingDevices = json_decode($row->value,true);
                        if(!in_array($deviceNumber,$existingDevices)){
                            $existingDevices[] = $deviceNumber;
                            $this->db->where('key','outbound_devices');
                            $DBdata['value'] = json_encode($existingDevices);
                            $this->db->update('config_data', $DBdata);
                        }

                    }else{
                        $existingDevices[] = $deviceNumber;
                        $DBdata['key'] = 'outbound_devices';
                        $DBdata['value'] = json_encode($existingDevices);
                        $this->db->insert('config_data', $DBdata);
                    }
                }
                $message = json_decode($fileData);
                if($message->VerificationStatus == 'success'){
                    //Get All Available Company Caller ID
                    $account_sid = $this->config->item('Twilio_Account_SID');
                    $auth_token = $this->config->item('Twilio_TOKEN');
                    $client = new Twilio\Rest\Client($account_sid, $auth_token);
                    $CompanyCallerIds = $client->outgoingCallerIds->read();

                    foreach ($CompanyCallerIds as $CallerId){
                        $CompanyCallerIdsTemp[$CallerId->friendlyName] = $CallerId;
                    }
                    ksort($CompanyCallerIdsTemp);
                    $CompanyCallerIds = [];
                    foreach ($CompanyCallerIdsTemp as $friendly_name => $CallerId){
                        $CompanyCallerIds[] = $CallerId;
                    }
                    ob_start();
                    $DefaultCallerId = $this->config->item('macanta_caller_id');

                    foreach ($CompanyCallerIds as $CallerId) {
                        if($DefaultCallerId == $CallerId->phoneNumber) continue;
                        $friendly_name = $CallerId->friendlyName;
                        $friendly_name = $CallerId->phoneNumber == $friendly_name ? "<em>No Label</em>":$friendly_name;
                        $Id = $CallerId->sid;
                        $info = "hide";
                        if(in_array($CallerId->phoneNumber, $SystemOuboundDevices)) $info = "";
                        $isdevice = $info == "hide" ? 'no':'yes';
                        $fwidth = $info == "hide" ? 'fwidth':'';
                        echo '<li class="form-group-item" data-calleridtype="user-defined"  data-sid="'.$CallerId->sid.'"  data-guid="'.$CallerId->phoneNumber.'">
                            <div class="CallerIdItem">
                                <div class="form-group success">
                                    <input class="AllowUserCallerId UserCallerIdToEnable" value="'.$CallerId->phoneNumber.'" data-callerid="'.$CallerId->phoneNumber.'"  type="checkbox" name="CallerId" id="callerId-'.$Id.'" autocomplete="off" disabled   />
                                    <div class="btn-group">
                                        <label for="callerId-'.$Id.'" class="btn btn-success toggleCallerId">
                                            <span class="fa fa-check"></span>
                                            <span> </span>
                                        </label>
                                        <label  class="btn btn-default checkbox-label active">
                                            <div class="MacantaCallerIdTitle '.$Id.' '.$fwidth.'" id="label'.$Id.'"  title="'.$friendly_name.'">
                                                '.$friendly_name.'
                                            </div>
                                            <div class="MacantaCallerIdNumber '.$Id.' '.$fwidth.'">
                                            <div class="the-number">
                                                 '.$CallerId->phoneNumber.'
                                             </div>
                                                <div class="the-info">
                                                     <span class="info-as-device-'.$Id.'">
                                                     <a class="info-as-device '.$info.'">Can be used as a call device`</a>
                                                    </span>
                                                 </div>
                                            </div>
                                        </label>
                                        <i class="fa fa-pencil-square-o EditCallerIdLabelIcon CallerIDAction" aria-hidden="true"
                                            data-sid="'.$CallerId->sid.'"
                                            data-callerid="'.$CallerId->phoneNumber.'"
                                            data-label="'.$CallerId->friendlyName.'" 
                                            data-isdevice="'.$isdevice.'"></i>
                                        <i class="fa fa-trash-o DeleteCallerIdIcon CallerIDAction" aria-hidden="true"
                                           data-sid="'.$CallerId->sid.'"
                                           data-phone="'.$CallerId->phoneNumber.'" ></i>
                                    </div>
                                </div>
                            </div>
                        </li>';
                    }
                    $HTML = ob_get_contents();
                    ob_end_clean();

                }
                return array("message" => $message,"html"=>$HTML);
            } else {
                $count++;
                sleep(5);
            }
            if ($count > 20) {
                $found = true;
            }
        }
        return array("message" => "error");
    }
    public function setLogo(){
        $DBdata = array();
        $this->ajaxResults['data'] = $_FILES['adminlogofile'];
        $tmp_name = $_FILES["adminlogofile"]["tmp_name"];
        $uploads_dir = manual_cache_location();
        if (!file_exists($uploads_dir)) {
            mkdir($uploads_dir, 0777, true);
        }
        // basename() may prevent filesystem traversal attacks;
        // further validation/sanitation of the filename may be appropriate
        $name = 'macanta-'.time().str_replace(' ','-',basename($_FILES["adminlogofile"]["name"]));
        $status = move_uploaded_file($tmp_name, $uploads_dir.$name);

        $imageData =  base64_encode(file_get_contents($uploads_dir.$name));
        $mime_content_type = mime_content_type($uploads_dir.$name);
        $value = [
            'imageData' => $imageData,
            'filename' => $name,
            'type' => $mime_content_type
        ];
        $DBdata['value'] = json_encode($value);

        // update Databse
        if(false == $this->DbExists('key','macanta_custom_logo','config_data')){
            $DBdata['key'] = 'macanta_custom_logo';
            $this->db->insert('config_data', $DBdata);
        }else{
            $this->db->where('key','macanta_custom_logo');
            $this->db->update('config_data',$DBdata);
        }
        unlink($uploads_dir.$name);
        $this->ajaxResults['status'] = $status;
        $this->ajaxResults['script'] = 'if(typeof serviceWorkerRegistration !== "undefined"){ serviceWorkerRegistration.unregister();}caches.delete(\'macanta-cache\');$("img.loginLogo").attr("src","data:'.$mime_content_type.';base64,'.$imageData.'");';
        $this->ajaxResults['script'] .= '$("img.adminloginLogo").attr("src","data:'.$mime_content_type.';base64,'.$imageData.'");';

        return $this->ajaxResults;
    }
    public function DbExists($field,$value,$table)
    {
        $this->db->where($field,$value);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0){

            foreach ($query->result() as $row)
            {
                return true;
            }
            return false;
        }
        else{

            return false;
        }
    }
    public function saveMacantaQuery($data){
        $ParsedValues = $data['ParsedValues'];
        $theTag = $this->getcreateTagByName(
            $ParsedValues['queryName'],
            "savedsearch_permission_cat",
            "macanta saved search permission",
            "macanta >> saved search >> "
        );
        $ParsedValues['tagId'] = $theTag->Id;
        $Data = [
            'QueryId' => $ParsedValues['queryId'],
            'Data' => json_encode($ParsedValues),
            'Status' => isset($ParsedValues['queryStatus']) ? 'active':'inactive',
        ];
        if(isset($theTag->Id)) $Data['TagId'] = $theTag->Id;
        $Results = macanta_save_cd_query($Data);
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['message'] = "saveMacantaQuery Results: ".json_encode($Results);
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function deleteCDAutomation($data){
        $QueryId = $data['queryId'];
        $this->db->where('QueryId',$QueryId);
        $results = $this->db->delete('ConnectedDataAutomation');
        $this->ajaxResults['status'] = $results;
        $this->ajaxResults['message'] = "saveMacantaQuery Results: ".json_encode($results);
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function saveCDAutomation($data){
        $ParsedValues = $data['ParsedValues'];
        //echo '<pre>'; print_r($ParsedValues); exit;
        $Data = [
            'QueryId' => $ParsedValues['queryId'],
            'QueryLabel' => $ParsedValues['TabTitleName'],
            'QueryType' => $ParsedValues['queryType'],
            'ConnectedDataGroupName' => isset($ParsedValues['queryConnectedDataType']) ? $ParsedValues['queryConnectedDataType']:'',
            'Data' => json_encode($ParsedValues,  JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK),
            'Status' => isset($ParsedValues['GroupStatus']) ? $ParsedValues['GroupStatus']:'',
        ];
        $Results = macanta_save_cd_automation($Data);
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['message'] = "saveMacantaQuery Results: ".json_encode($Results);
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function updateMacantaQueryList($data){
        $query = $this->db->get('ConnectedDataQuery');
        $Data = [];
        foreach ($query->result() as $row) {
            $Data[] = json_decode($row->Data);
        }
        ob_start();
        foreach ($Data as $MacantaQuery){
            ?>
            <div class="macanta-query-item query-item-container-<?php echo $MacantaQuery->queryId; ?>">
                <div class="query-item">
                    <div class="query-item-head">
                        <h3 class=""><?php echo $MacantaQuery->queryName; ?> <small>( <?php echo $MacantaQuery->queryStatus ? "active":"inactive"; ?> )</small></h3>
                        <h5 class=""><?php echo $MacantaQuery->queryDescription; ?></h5>
                        <div class="nb-btn-group" role="group" aria-label="...">
                            <button  data-queryid="<?php echo $MacantaQuery->queryId; ?>" type="button" class="btn editMacantaQuery">
                                <i class="fa fa-pencil-square-o"></i>
                            </button>
                            <button   data-queryid="<?php echo $MacantaQuery->queryId; ?>" type="button" class="btn deleteMacantaQuery">
                                <i class="fa fa-trash-o"></i>
                            </button>
                            <button type="button" class="nb-btn icon-btn nb-btn-icon-only" disabled>
                                <i class="fa fa-ban"></i>
                            </button>
                        </div>
                        <span class="tag-info">Tag Id: <?php echo $MacantaQuery->tagId; ?></span>
                    </div><!-- /.query-item-head -->
                    <div class="macanta-query-item-short-info">
                        <div class="">
                            <strong>Connected Data Type: </strong><?php echo $MacantaQuery->queryConnectedDataType; ?>
                        </div>
                        <div class="">
                            <strong>Contact Relationship: </strong>
                            <?php
                            $ContactRelationshipArr = [];
                            foreach ($MacantaQuery->queryContact as $queryContact){
                                $ContactRelationshipArr[] = $queryContact->queryContactRelationship;
                            }
                            echo implode(', ',$ContactRelationshipArr);
                            ?>
                        </div>
                        <div class="">
                            <strong>User Relationship: </strong>
                            <?php
                            $UserRelationshipArr = [];
                            foreach ($MacantaQuery->queryUser as $queryUser){
                                $UserRelationshipArr[] = $queryUser->queryUserRelationship;
                            }
                            echo implode(', ',$UserRelationshipArr);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                bindEditDeleteQuery();
            </script>
        <?php }
        $HTML = ob_get_contents();
        ob_end_clean();
        $this->ajaxResults['html'] = $HTML;
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function getMacantaQueryItem($data){
        $queryId = $data['queryId'];
        $this->db->where('QueryId',$queryId);
        $query = $this->db->get('ConnectedDataQuery');
        $row = $query->row();
        $this->ajaxResults['data'] = $row;
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function deleteMacantaQueryItem($data){
        $queryId = $data['queryId'];
        $this->db->where('QueryId',$queryId);
        $result = $this->db->delete('ConnectedDataQuery');
        $this->ajaxResults['data'] = $result;
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }


}
