// Macanta Main Functions
(function (factory) {
    if (typeof define === "function" && define.amd) {
        define(["jquery", "moment", "datatables.net"], factory);
    } else {
        factory(jQuery, moment);
    }
}(function ($, moment) {

    $.fn.dataTable.moment = function (format, locale) {
        var types = $.fn.dataTable.ext.type;

        // Add type detection
        types.detect.unshift(function (d) {
            if (d) {
                // Strip HTML tags and newline characters if possible
                if (d.replace) {
                    d = d.replace(/(<.*?>)|(\r?\n|\r)/g, '');
                }

                // Strip out surrounding white space
                d = $.trim(d);
            }

            // Null and empty values are acceptable
            if (d === '' || d === null) {
                return 'moment-' + format;
            }

            return moment(d, format, locale, true).isValid() ?
                'moment-' + format :
                null;
        });

        // Add sorting method - use an integer for the sorting
        types.order['moment-' + format + '-pre'] = function (d) {
            if (d) {
                // Strip HTML tags and newline characters if possible
                if (d.replace) {
                    d = d.replace(/(<.*?>)|(\r?\n|\r)/g, '');
                }

                // Strip out surrounding white space
                d = $.trim(d);
            }

            return !moment(d, format, locale, true).isValid() ?
                Infinity :
                parseInt(moment(d, format, locale, true).format('x'), 10);
        };
    };

}));
$.fn.dataTable.moment('HH:mm MMM D, YY');
$.fn.dataTable.moment('dddd, MMMM Do, YYYY');
$.fn.dataTable.moment('YYYY-MM-DD');
$.fn.dataTable.moment('DD MMM YYYY');
$.fn.dataTable.moment('MMM DD YYYY');

var Macanta = function (origin) {
    origin = origin || 'Manually';
    //console.log('Macanta run from '+ origin);
    //renderContactSearch();
    $(document)
        ._once("change", "select.ConnectedContactRelationshipsBulkAdd", function () {
            var toDisabled = true;
            $("table#UserConnectorInfoTableFilterResult tbody tr").each(function () {
                var theTR = $(this);
                var theIdsStr = theTR.attr("data-contactids");
                var itemid = theTR.attr("data-itemid");
                var theIdsArr = theIdsStr.split(',');
                var show = false;
                if (theTR.hasClass('hideThis')) return true;
                if (theTR.hasClass('hideThisSub')) return true;
                var theSelect = theTR.find("select").val();
                if (!theSelect) {
                    return true;
                } else {
                    toDisabled = false;
                    return false;
                }

            });
            if (toDisabled === false) {
                $("button.addBulkConnectedInfo").removeAttr('disabled');
            } else {
                $("button.addBulkConnectedInfo").attr('disabled', true);
            }
        })
        ._once("click", "button.addBulkConnectedInfo", function () {
            var GUID = $(this).attr("data-guid");
            addBulkConnectedInfo(GUID);
        })
        ._once("click", "li.filter-option-item", function () {
            $(this).toggleClass("active");
            $("input#AllConnectedData").prop("checked", false);
            $("input#ByConnectedContact").prop("checked", true);
            var showWithIds = [];

            $(".ul-filter-contact-list li").each(function () {
                var theLI = $(this);
                if (theLI.hasClass('active')) {
                    showWithIds.push(theLI.attr("data-contactid"));
                }
            });
            FilteredContactId = showWithIds;
            UserConnectorInfoTableFilterResult.ajax.reload();
            //onlyShowWithIds(showWithIds);

        })
        ._once("click", ".apply-connected-data-filter", function () {
            var theParent = $(this).parents("div.ByConnectedContactContainer");
            var hasFilter = theParent.find("input[type=radio]:checked").length;
            console.log(hasFilter);
            if (hasFilter > 0) {
                $("input#AllConnectedData").prop("checked", false);
            } else {
                $("input#AllConnectedData").prop("checked", true)
            }
        })
        ._once("click", ".remove-connected-data-filter", function () {
            $("input.remove-connected-data-filter").prop("checked", true);
            $("input.apply-connected-data-filter[type=radio]").each(function () {
                $(this).prop("checked", false);
            });
            $(".ByConnectedContactContainer li.filter-option-item").each(function () {
                $(this).removeClass("active");
            })
            onlyShowWithIds([]);
        })
        ._once("click", "button.link-connect-contact", function () {
            var FormParent = $(this).parents("form.FormUserConnectedInfo");
            var theItem = FormParent.attr("data-itemid");

            renderContactsPreloaderDataTable([]);
            $('#ConnectOtherContact').modal('show');
            $("input.search-contact-to-connect").val('').attr("data-itemid", theItem);
        })
        ._once("click", "button.btn-add-agent", function () {
            renderAgentsPreloaderDataTable([]);
            $('#AddCallCenterAgent').modal('show');
        })
        ._once('click', "button.link-connect-data", function () {
            var GroupId = $(this).attr("data-guid");
            $('#ConnectOtherData').modal('show');
            $('#ConnectOtherData button.addBulkConnectedInfo').attr("data-guid", GroupId);
            getConnectedDataByGroup(GroupId);
            $(".remove-connected-data-filter").trigger('click');
        })
        ._once('submit', "form.macantaLogin", function (evt) {
            evt.preventDefault();
            login($(this), 'login-body');
        })
        ._once('click',"button.saveUserConnectedInfo", function (evt) {
            var theForm = $(this).parents("form.FormUserConnectedInfo");
            //============== Aarti's Modification Start Date: 2020-03-23 =============/
            $('#macanta-body').find('.FormUserConnectedInfo .changed-input').removeClass('changed-input');
            $("a[data-toggle='tab']").prop('disabled', false);
            //============== Aarti's Modification Start Date: 2020-03-23 =============/
            var emptyRequiredFields = $('input,textarea,select').filter('[required]:hidden');
            emptyRequiredFields.each(function() {
                if($(this).val() === ''){
                    $(this).parents("fieldset").find("div.form-group").each(function () {
                        $(this).show();
                    });
                }
            });


        })
        ._once('click',".collapsible.CDSection", function (evt) {
            $(".saveUserConnectedInfo").addClass('floating-save-btn');

        })
        ._once('submit', "form.FormUserConnectedInfo", function (evt) {
            evt.preventDefault();
            var theForm = $(this);
            var GUID = $(this).attr('data-guid');
            var data = {};
            var itemId = $(this).attr('data-itemid');
            var values = $(this).serializeArray();
            var newValue = {};

            var ContactConenctedInfoStr = localStorage.getItem(GUID) || {};
            var ContactConenctedInfoObj = JSON.parse(ContactConenctedInfoStr);


            var ConnectedContacts = ContactConenctedInfoObj[itemId]['connected_contact'];
            var CheckBoxes = [];
            $.each(values, function (index, field) {
                var FieldName = field['name'];
                if (FieldName.indexOf('checkbox_') !== -1) {
                    CheckBoxes.push(field['value']);
                }
                if (FieldName.indexOf('field_') !== -1) {
                    if ($("[name='" + FieldName + "']", theForm).attr('data-jsonvalue')) {
                        var oldValue = JSON.parse($("[name='" + FieldName + "']", theForm).attr('data-jsonvalue'));
                        oldValue["id_" + ContactId.toString()] = $.trim(field['value']);
                        if (typeof newValue[FieldName] === "undefined") {
                            newValue[FieldName] = oldValue;
                        } else {
                            oldValue = typeof newValue[FieldName] === "object" ? newValue[FieldName]:JSON.parse(newValue[FieldName]);
                            oldValue["id_" + ContactId.toString()] = oldValue["id_" + ContactId.toString()] + "|" + $.trim(field['value']);
                            newValue[FieldName] = oldValue;
                        }
                    } else {
                        if (typeof newValue[FieldName] === "undefined") {
                            newValue[FieldName] = $.trim(field['value']);
                        } else {
                            newValue[FieldName] = newValue[FieldName] + "|" + $.trim(field['value']);
                        }
                    }

                }
            });
            $(CheckBoxes).each(function (index,CheckBox) {
                if(typeof newValue[CheckBox] === 'undefined'){
                    if ($("[name='" + CheckBox + "']", theForm).attr('data-jsonvalue')) {
                        var oldValue = JSON.parse($("[name='" + CheckBox + "']", theForm).attr('data-jsonvalue'));
                        oldValue["id_" + ContactId.toString()] = '';
                        newValue[CheckBox] = oldValue;
                    }else{
                        newValue[CheckBox] = '';
                    }
                }
            });

            /*ContactConenctedInfoObj[itemId]['value']  = newValue;*/
            $.each(newValue, function (FieldName, FieldValue) {
                ContactConenctedInfoObj[itemId]['value'][FieldName] = FieldValue;
            });
            ContactConenctedInfoStr = JSON.stringify(ContactConenctedInfoObj);
            localStorage.setItem(GUID,ContactConenctedInfoStr);
            $(this).trigger('reset');
            $(this).parents("div.col-md-5").removeClass('lightYellow');
            saveUserConnectedInfo(itemId, JSON.stringify(newValue), GUID, JSON.stringify(ConnectedContacts));

        })
        ._once('click', "button.deleteUserConnectedInfo", function (evt) {
            evt.preventDefault();
            var ConnectorDeleteDialog = "\n<div id=\"dialog-confirm\" title=\"Unlink Information Item.\">\n    <p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:12px 12px 20px 0;\"><\/span>This Informaion will be removed from this contact.<br> Are you sure?<\/p>\n<\/div>\n";
            var theForm = $(this).parents("form.FormUserConnectedInfo");
            var itemId = theForm.attr('data-itemid');
            var GUID = $(this).attr('data-guid');
            $(ConnectorDeleteDialog).appendTo("body");
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 500,
                modal: true,
                buttons: {
                    Cancel: function () {
                        $(this).dialog("close");
                        $("#dialog-confirm").remove();
                        toDel = false;
                    },
                    "Yes!": function () {
                        $(this).dialog("close");
                        $("#dialog-confirm").remove();
                        theForm.trigger('reset');
                        $("fieldset.cd-fields",theForm).removeClass('lightYellow');
                        $("fieldset.cd-fields",theForm).removeClass('lightGreen');
                        theForm.attr("data-itemid", "");
                        $("ul.relationship-list", theForm).html("");
                        $("select.ConnectedContactRelationships", theForm)
                            .multiselect({
                                //includeSelectAllOption: true
                                numberDisplayed: 5,
                                maxHeight: 200,
                                allSelectedText: false
                            })
                            .multiselect('select', []);
                        $(".addGroup", theForm).attr('disabled', true);
                        deleteUserConnectedInfo(itemId, GUID);
                    }

                }
            });
        })
        ._once('click', '.main-search-bar .search', function () {
            var source = $(this).data('source');
            search(source);
        })
        ._once('keydown', '.main-search-bar .searchKey', function (event) {
            if (event.keyCode == 13) {
                var source = $(this).data('source');
                search(source);
            }
        })
        ._once('keyup', '.main-search-bar .searchKey', function (event) {
            if ($(this).val().trim() != '') {
                $("#searchTag_tag").css('display', 'none');
                $('.searchTag').importTags('');
            } else {
                $("#searchTag_tag").removeAttr('style');
                $("#searchTag_tag").css('color', 'rgb(102, 102, 102)');
                $("#searchTag_tag").val(' or by note #tags');

            }
        })
        ._once('click', '.inside-search .search', function () {
            var source = $(this).data('source');
            if ($('#macanta-body').find('.changed-input').length) {
             alert("You have unsaved changes on this page!");
             return false;
            }
            search(source);
        })
        ._once('keydown', '.inside-search .InAppSearch', function (event) {
            if (event.keyCode == 13) {
                var source = $(this).data('source');
                search(source);
            }
        })
        ._once("update", "#ContactsTable", function () {
            $(this).DataTable({
                data: searchToDataTable(),
                responsive: true,
                "paging": true,
                "search": true,
                "info": false,
                "destroy": true,
                "createdRow": function (row, data, index) {
                    //$(row).attr('data-contactid',data[3]);
                }
            });
        })
        ._once("click", ".SaveNote", function () {
            var toSave = confirm('Save Notes?');
            if (toSave) {
                addNote(ContactId, 'call_notes', 'callNotes', 'CurrentCallNotes', 'PreviousCallNotes');
            } else {
                return false;
            }


        })
        ._once("click", ".SaveQuickNote", function () {
            //var toSave = confirm('Save Notes?');
            var toSave = true;
            if (toSave) {
                addNote(ContactId, 'quick_notes', 'personal-notes', 'CurrentPersonalNotes', 'PreviousQuickNotes', 'PreviousQuickNotes', 'prev-notes');
            } else {
                return false;
            }


        })
        ._once("click", ".SaveOppNote", function () {
            //var toSave = confirm('Save Notes?');
            var shortcodeid = $(this).attr("data-shortcodeid");
            var toSave = true;
            if (toSave) {
                addNote(ContactId, 'quick_notes', 'opp-notes', 'CurrentOpportunityNotesContainer' + shortcodeid, 'PreviousQuickNotes', 'NoteDescriptionContainer' + shortcodeid, 'NoteDescriptionContainer' + shortcodeid);
            } else {
                return false;
            }


        })
        ._once("click", ".SaveQuickTask", function () {
            //var toSave = confirm('Save Notes?');
            var toSave = true;
            if (toSave) {
                addTask(ContactId, 'personal-notes', 'CurrentPersonalNotes', 'PreviousQuickNotes');
            } else {
                return false;
            }


        })
        ._once("keyup", "#TaskActionDescription", function () {
            var theVal = $(this).val();
            if (theVal.trim() !== '') {
                $(this).removeClass('error');
            }
        })
        ._once("keyup", "#NoteTitle", function () {
            var theVal = $(this).val();
            if (theVal.trim() !== '') {
                $(this).removeClass('error');
                //$('.SaveQuickNote').attr('disabled',true);
                $('.SaveQuickNote').removeAttr('disabled');
            } else {
                $('.SaveQuickNote').attr('disabled', true);
            }
        })
        ._once("change", "#TaskActionDate", function () {
            var theVal = $(this).val();
            if (theVal.trim() !== '') $(this).removeClass('error');
        })
        ._once("click", ".updateStat i.fa-refresh", function () {
            $(".updateStat").html('Updating, please wait..');
            refreshNote(ContactId, 'quick_notes', 'PreviousQuickNotes');

        })
        ._once("click", ".contacts-connected a", function () {
            var thisHash = $(this).attr('href').substring(1).split('/');
            ContactId = thisHash[1];
            console.log(thisHash);
            $("#AddContact").remove();
            processHash(thisHash);

        })
        ._once("click", "a.rowContactResults", function () {

            if ($(this).hasClass('TaskSearch') || $(this).hasClass('MacantaTaskSearch')) {
                var href = $(this).attr('data-href');
                var thisHash = href.substring(1).split('/');
                if (thisHash[1]) {
                    console.log(thisHash);
                    //var dashboardWindow = window.open(href, "macantaTab");
                    //dashboardWindow.location.reload(true);
                    window.location = href;
                    var theDelay = setTimeout(function () {
                        window.location.reload(true);
                    }, 300);
                } else {
                    alert("Oops! This saved search doesn't contain a Contact Id and/or Note Id, so I don't know which contact or note to display!\\n\\rPlease add the Id's to the columns of the saved search in Infusionsoft, save it and re-select the saved search on this page.\\n\\rThanks.");
                }

            } else {
                var thisHash = $(this).attr('href').substring(1).split('/');
                ContactId = thisHash[1];
                console.log(thisHash);
                $("#AddContact").remove();
                processHash(thisHash);
            }

        })
        ._once("click", "a.showNextResult", function (e) {
            //============== Aarti's Modification Start Date: 2020-03-23 =============/
            if ($('#macanta-body').find('.changed-input').length) {
                  e.preventDefault();   
            } 
            //============== Aarti's Modification Start Date: 2020-03-23 =============/
            var theDelay = setTimeout(function () {
                //============== Aarti's Modification Start Date: 2020-03-23 =============/
                if ($('#macanta-body').find('.changed-input').length) {
                  alert("You have unsaved changes on this page!");
                  return false;
                } 
                else
                {
                   window.location.reload(true); 
                }
                //============== Aarti's Modification Start Date: 2020-03-23 =============/
                
            }, 600);


        })
        ._once('click', '.editContact', function () {
            $('#ContactDetails').modal('show');
        })
        ._once('click', '.showFullContact', function () {
            $('#FullContactCard').modal('show');
        })
        ._once('click', '.addContact', function () {
            $("form#ContactDetailsFormAdd").trigger("reset");
            $("form#ContactDetailsFormAdd input").val('');
            if($('#macanta-body').find('.changed-input').length) {
             alert('You have unsaved changes on this page!');
             return false;
            }
            else
            {
              $('#AddContact').modal('show');
            }
        })
        ._once('click', ".switchAddress span.show-other-address", function () {
            switchAddress()
        })
        ._once('click', ".switchAddressEdit span.show-other-address", function () {
            switchAddressType('Edit');
        })
        ._once('click', ".switchAddressAdd span.show-other-address", function () {
            switchAddressType('Add');
        })
        ._once('click', "a.ShowAddressEditLoqate, a.ShowAddressAddLoqate", function () {
            var addressType =  $(this).attr('data-addresstype');
            console.log("."+addressType+"LoqateContainer");
            $("."+addressType+"LoqateContainer").slideDown('slow', function () {
                $("input#"+addressType+"Loqate").focus();
            });
            $("."+addressType+" ul.user-info.contactView").slideUp('fast');
            $("."+addressType+" .btn-toolbar.address-edit-btn").slideUp('fast');

        })
        ._once('click', "a.AddressEditManual, a.AddressAddManual", function () {
            var addressType =  $(this).attr('data-addresstype');
            $("."+addressType+"LoqateContainer").slideUp('fast', function () {
                $("."+addressType+" .DVE-alert").fadeOut('fast');
                $("."+addressType+" ul.user-info.contactView").slideDown();
                $("."+addressType+" .btn-toolbar.address-edit-btn").slideDown();
                $("."+addressType+" span.BlockMode").slideUp(100, function () {
                    $("."+addressType+" li.hideThis").removeClass('hideThis');
                    $("."+addressType+" span.EditMode").slideDown(300);
                });

            });

        })
        ._once('click',"a.AddressVerifyAlert",function () {
            var addressType =  $(this).attr('data-addresstype');
            $("."+addressType+" .DVE-alert").fadeIn('slow');
        })
        ._once('click', "a.AddressBillingEditVerify", function () {
            var EditContainer = $("div.AddressBillingEdit ul.contactView");
            var AddressObj = {
                "StreetAddress1":$("input.StreetAddress1", EditContainer).val(),
                "StreetAddress2":$("input.StreetAddress2", EditContainer).val(),
                "City":$("input.City", EditContainer).val(),
                "State":$("input.State", EditContainer).val(),
                "PostalCode":$("input.PostalCode", EditContainer).val(),
                "Country":$("select.Country", EditContainer).val()
            };
            var AddressArr = [
                $("input.StreetAddress1", EditContainer).val(),
                $("input.StreetAddress2", EditContainer).val(),
                $("input.City", EditContainer).val(),
                $("input.State", EditContainer).val(),
                $("input.PostalCode", EditContainer).val(),
                $("select.Country", EditContainer).val()
            ];
            var Country = $("select.Country", EditContainer).val();
            var Container = "";
            validateAddress(AddressObj,AddressArr, Container, "AddressBillingEdit", Country);
        })
        ._once('click', "a.AddressShippingEditVerify", function () {
            var EditContainer = $("div.AddressShippingEdit ul.contactView");
            var AddressObj = {
                "Address2Street1":$("input.Address2Street1", EditContainer).val(),
                "Address2Street2":$("input.Address2Street2", EditContainer).val(),
                "City2":$("input.City2", EditContainer).val(),
                "State2":$("input.State2", EditContainer).val(),
                "PostalCode2":$("input.PostalCode2", EditContainer).val(),
                "Country2":$("select.Country2", EditContainer).val()
            };
            var AddressArr = [
                $("input.Address2Street1", EditContainer).val(),
                $("input.Address2Street2", EditContainer).val(),
                $("input.State2", EditContainer).val(),
                $("input.State", EditContainer).val(),
                $("input.PostalCode2", EditContainer).val(),
                $("select.Country2", EditContainer).val()
            ];
            var Country = $("select.Country2", EditContainer).val();
            var Container = "";
            validateAddress(AddressObj,AddressArr, Container, "AddressShippingEdit", Country);
        })
        ._once('keyup',"input.LoqateField", function (evt) {
            if (evt.keyCode == 32) { // dont include space
                return false;
            }
            var AddressSection = $(this).attr('data-addresstype');
            var Address = $(this).val();
            var searchId = "";
            validateAddressTypeIn(Address, searchId, AddressSection)
        })
        ._once('click', "li.address-item.group span.grouplabel", function () {
            var theParentLi = $(this).parents("li.address-item.group");
            var AllAddressSublist = $("ul.AddressSublist",theParentLi);
            var addressType = theParentLi.attr('data-addresstype');
            AllAddressSublist.removeClass("lastClicked");

            $("ul.AddressSublist", theParentLi).addClass("lastClicked");
            AllAddressSublist.not(".lastClicked").slideUp().removeClass("ulOpened");
            if ($(this).hasClass('haveClicked')) {
                /*if($("ul.AddressSublist",theParentLi).hasClass("ulOpened")){
                    $("ul.AddressSublist",theParentLi).slideUp().removeClass("ulOpened");
                }else{*/
                $("ul.AddressSublist", theParentLi).slideToggle();
                //}
                return false;
            }
            $(this).addClass('haveClicked');
            var EditContainer = $("div."+addressType+" ul.contactView");
            var Address = [""];
            if(addressType === 'AddressBillingEdit'){
                Address = {
                    "StreetAddress1":$("input.StreetAddress1", EditContainer).val(),
                    "StreetAddress2":$("input.StreetAddress2", EditContainer).val(),
                    "City":$("input.City", EditContainer).val(),
                    "State":$("input.State", EditContainer).val(),
                    "PostalCode":$("input.PostalCode", EditContainer).val(),
                    "Country":$("select.Country", EditContainer).val()
                };
            }
            if(addressType === 'AddressShippingEdit'){
                Address = {
                    "Address2Street1":$("input.Address2Street1", EditContainer).val(),
                    "Address2Street2":$("input.Address2Street2", EditContainer).val(),
                    "City2":$("input.City2", EditContainer).val(),
                    "State2":$("input.State2", EditContainer).val(),
                    "PostalCode2":$("input.PostalCode2", EditContainer).val(),
                    "Country2":$("select.Country2", EditContainer).val()
                };
            }
            var Container = theParentLi.attr('data-searchid');
            getAddresss(Address, Container, theParentLi)
        })
        ._once('click', 'li.address-item.item', function () {
            $("input[type=radio]", this).prop('checked', true);
        })
        ._once('change',".AddressBillingEdit ul.user-info.contactView li.info-item input, .AddressBillingEdit ul.user-info.contactView li.info-item select", function () {
            if($("input[name=AddressBillingEdit]").val() === 'verified' || BillingAddressValidated === 'verified' ){
                BillingAddressValidated = 'verified';
                var OriginalVal = $(this).attr('data-originalvalue');
                var NewVal = $(this).val();
                if(NewVal !== OriginalVal){
                    $("input[name=AddressBillingEdit]").val('');
                    $(".AddressBillingEdit i.AddressStatus").removeClass('ValidatedAddress').addClass('InValidatedAddress').attr('title','Address Not Verified');
                }else{
                    $("input[name=AddressBillingEdit]").val('verified');
                    $(".AddressBillingEdit i.AddressStatus").removeClass('InValidatedAddress').addClass('ValidatedAddress').attr('title','Address Verified');
                }

            }

        })
        ._once('change',".AddressShippingEdit ul.user-info.contactView li.info-item input, .AddressShippingEdit ul.user-info.contactView li.info-item select", function () {
            if($("input[name=AddressShippingEdit]").val() === 'verified' || ShippingAddressValidated === 'verified'){
                ShippingAddressValidated = 'verified';
                var OriginalVal = $(this).attr('data-originalvalue');
                var NewVal = $(this).val();
                if(NewVal !== OriginalVal){
                    $("input[name=AddressShippingEdit]").val('');
                    $(".AddressShippingEdit i.AddressStatus").removeClass('ValidatedAddress').addClass('InValidatedAddress').attr('title','Address Not Verified');
                }else{
                    $("input[name=AddressShippingEdit]").val('verified');
                    $(".AddressShippingEdit i.AddressStatus").removeClass('InValidatedAddress').addClass('ValidatedAddress').attr('title','Address Verified')
                }

            }
        })
        ._once('click', '.AddressSliderContainerEdit li.address-item.item, .AddressSliderContainerAdd li.address-item.item', function () {
            var addressType = $(this).attr('data-addresstype');
            var AddressSearchId = $(this).attr('data-searchid');
            var addressStr = $("#"+addressType+"Loqate").val();
            var Address = [addressStr];
            EditContainer = $("div."+addressType+" ul.contactView");
            applyAddresss(Address, AddressSearchId, EditContainer,addressType, true)
        })
        ._once('click', 'button.VerifySelectedAddressItem', function () {
            var parentItem = $(this).parents("li.address-item.group");
            var EditContainer,Container,Address;
            var SelectedAddressItem = $("ul.AddressMainList").find("input[type=radio][name=SelectedAddressItem]:checked");
            if (SelectedAddressItem.length > 0) {
                console.log(SelectedAddressItem.val());
                if($(this).hasClass('AddressBillingEdit')){
                    EditContainer = $("div.AddressBillingEdit ul.contactView");
                    Container = 'AddressBillingEdit';
                    Address = [
                        $("input.StreetAddress1", EditContainer).val(),
                        $("input.StreetAddress2", EditContainer).val(),
                        $("input.City", EditContainer).val(),
                        $("input.State", EditContainer).val(),
                        $("input.PostalCode", EditContainer).val(),
                        $("select.Country", EditContainer).val()
                    ];
                }
                if($(this).hasClass('AddressShippingEdit')){
                    EditContainer = $("div.AddressShippingEdit ul.contactView");
                    Container = 'AddressShippingEdit';
                    Address = [
                        $("input.Address2Street1", EditContainer).val(),
                        $("input.Address2Street2", EditContainer).val(),
                        $("input.City2", EditContainer).val(),
                        $("input.State2", EditContainer).val(),
                        $("input.PostalCode2", EditContainer).val(),
                        $("select.Country2", EditContainer).val()
                    ];
                }
                applyAddresss(Address, SelectedAddressItem.val(), EditContainer,Container)
            } else {
                alert("Oopps!, Please Select Address!");
            }
        })
        ._once('click','body', function (event) {
            if(!$(event.target).closest('.macanta-notice-info').length) {
                if($('.macanta-notice-info').is(":visible")) {
                    $('.macanta-notice-info').hide();
                }
            }
        })
        ._once('click',".macanta-notice-info", function (event) {
            event.stopPropagation();
        })
        ._once('click',".show-notice", function (event) {
            event.stopPropagation();
            var target = $(this).attr('data-show');
            $('.macanta-notice-info').not('.'+target).hide();
            var noticeInfo = $('.macanta-notice-info.'+target);
            noticeInfo.fadeToggle();
        })
        ._once('click','.btn-macanta-confirm-info', function () {
            var chosen = $(this);
            var container = chosen.parents('.macanta-confirm-info');
            var target = container.attr('data-target');
            var theValue = container.attr('data-value');
            if(chosen.hasClass('yes')){
                $("input."+target).val(theValue).trigger('change');
                container.hide();
            }else{
                container.hide();
            }
        })
        ._once('click', '.updateContactDetails', function () {
            var contactDetails = $('form#ContactDetailsFormEdit').serializeArray();
            $.each(contactDetails, function (theIndex, ArrItem) {
                if(ArrItem.name === 'EmailSignature'){
                    contactDetails[theIndex]['value'] = tinyMCE.get('EmailSignature').getContent();
                }
            });
            console.log(contactDetails);
            console.log('Requesting...');
            var optin = "No";
            if ($("form#ContactDetailsForm input.Email").attr("data-original") !== $("form#ContactDetailsForm input.Email").val()) {
                optin = "Yes";
            }
            updateIScontact(contactDetails, ContactId, optin);
            //}

        })
        ._once("click", "a.callrecording", function () {
            var URL = $(this).attr('href');
            URL = URL.replace('.mp3', '');
            console.log(URL);
            var ogg = URL + '.ogg';
            var mp3 = URL + '.mp3';
            $("source.crecording_ogg").attr('src', ogg);
            $("source.crecording_mp3").attr('src', mp3);
            var audio = document.getElementById('callrecord');
            audio.load(); //call this to just preload the audio without playing
            audio.play();
            $("#CallRecording").modal('show');
            return false;

        })
        ._once('hidden.bs.modal', "#CallRecording", function (e) {
            var audio = document.getElementById('callrecord');
            audio.pause();
        })
        ._once('click.bs.dropdown.data-api', '.dropdown-menu form', function (e) {
            e.stopPropagation()
        })
        ._once('click', ".show-editor", function () {
            var theParent = $(this).parents(".NoteItem");
            $Id = $(theParent).find(".noteTextRich").attr('Id');
            initTinymceById($Id);
            $(theParent).find(".mce-tinymce").show();
            $(this).hide();
            $(".noteTextRich", theParent).hide();
            $(".close-editing", theParent).show();
            $(".cancel-editing", theParent).show();
            $(".NoteTitleEditorContainer", theParent).show();
            $(".NoteTypeEditorContainer", theParent).show();
            $(".NoteDescriptionEditorLabel", theParent).show();
            $("#noteTitle", theParent).hide();
            var theArticle = $(this).parents("article.noteCollapsible");
            theArticle.readmore('destroy');
        })
        ._once('click', ".close-editing", function () {
            var theParent = $(this).parents(".NoteItem");
            $(this).hide();
            $(".cancel-editing", theParent).hide();
            $(".NoteTypeEditorContainer", theParent).hide();
            $(".NoteTitleEditorContainer", theParent).hide();
            $(".NoteDescriptionEditorLabel", theParent).hide();
            $("#noteTitle", theParent).show();
            savePrevNoteRichText(theParent);
            var theArticle = $(this).parents("article.noteCollapsible");
            theArticle.readmore({
                speed: 300,
                embedCSS: false,
                collapsedHeight: 115,
                lessLink: '<a href="#">Show less</a>',
                moreLink: '<a href="#">Read more</a>'
            })

        })
        ._once('click', ".cancel-editing", function () {
            var theParent = $(this).parents(".NoteItem");
            var oldNote = $(theParent).find(".noteTextRich").html();
            var $Id = $(theParent).find(".noteTextRich").attr('Id');
            $(this).hide();
            $(".close-editing", theParent).hide();
            $(theParent).find(".mce-tinymce").hide();
            $(".NoteTypeEditorContainer", theParent).hide();
            $(".NoteTitleEditorContainer", theParent).hide();
            $(".NoteDescriptionEditorLabel", theParent).hide();
            $(".show-editor", theParent).show();
            $("#noteTitle", theParent).show();
            $(theParent).find(".noteTextRich").show();
            tinyMCE.get($Id, theParent).setContent(oldNote);
            var theArticle = $(this).parents("article.noteCollapsible");
            theArticle.readmore({
                speed: 300,
                embedCSS: false,
                collapsedHeight: 115,
                lessLink: '<a href="#">Show less</a>',
                moreLink: '<a href="#">Read more</a>'
            })
        })
        ._once('click', '.filterNotes', function () {
            var thePanel = $(this).parents('.panel-body.filter-notes');
            var staffSelected = $(".selectpicker", thePanel).selectpicker('val'); // array of ID, null if none
            var tagInserted = $(".tags_filter", thePanel).val(); // comma separated tags
            var dateRagne = $("#note_daterange", thePanel).val(); // "2016-06-28 to 2016-06-30"
            var searchStr = $(".filterSearchKey", thePanel).val(); // string
            filterNotes(staffSelected, tagInserted, dateRagne, searchStr);
        })
        ._once('click', '.search_filter', function () {
            var StrIds = $(".search_filter").selectpicker('val'); // array of ID, null if none
            var Source = $(this).find("select").attr('Id');
            var select = $("select#" + Source);
            var selected = $(':selected', select);
            var Group = selected.attr('data-group');
            var GlobalAccess = selected.attr('data-access');
            savedSearchReport(StrIds, Source, Group, GlobalAccess);
        })
        ._once('click', '.languagepicker', function () {
            var SlectedLanguage = $(".languagepicker").selectpicker('val'); // array of ID, null if none
            console.log(SlectedLanguage);
            setLanguage(SlectedLanguage);
        })
        ._once('click', '.applytags', function () {
            applyGoals();
        })
        ._once('click', '.newActionPlans', function () {
            newActionPlans();
        })
        ._once('click', '.noContactId', function () {
            alert(MacantaLanguages['text_no_contact_id']);
        })
        ._once('click', 'h4.orDial', function () {
            $(".DialContainer").slideToggle();
        })
        ._once('click', '.macanta-tag-category-select', function () {
            var Val = $('.macanta-tag-category-select').selectpicker('val');
            console.log(Val);
            changeMacantaTagCategory(Val);
        })
        ._once('click', '.save-tags-categories', function () {
            saveMacantaAccess();
        })
        ._once('change', '.EmailAdd', function () {
            $emailStatus = verifyEmail($(this));
            var Email = $(this).val();
            var theClass =$(this).attr('class');
            var theType =$(this).attr('data-type');
            if(Email !== ''){
                validateEmailAddress(Email,theClass,theType)
            }else{
                $(".Is"+theClass+"Valid"+theType).fadeOut();
            }
        })
        ._once('change', 'input.Phone1, input.Phone2', function () {
            //alert('Changed');
            var PhoneNumber = $(this).val();
            var theClass =$(this).attr('class');
            var theType =$(this).attr('data-type');
            if(PhoneNumber !== ''){
                validatePhoneNumber(PhoneNumber,theClass,theType)
            }else{
                $(".Is"+theClass+"Valid"+theType).fadeOut();
            }
        })
        ._once('change', 'input.Email', function () {
            //alert('Changed');
            var Email = $(this).val();
            var theClass =$(this).attr('class');
            var theType =$(this).attr('data-type');
            if(Email !== ''){
                validateEmailAddress(Email,theClass,theType)
            }else{
                $(".Is"+theClass+"Valid"+theType).fadeOut();
            }
        })
        ._once('click', '.permissionInfo', function () {
            $("#PermissionDetails").modal('show');
        })
        ._once('click', '.copyAutoLogin', function () {
            var theInput = $('input.AutoLoginCopyText');
            theInput.select();
            document.execCommand("copy");
            var theDelay = setTimeout(function () {
                theInput.blur();
            }, 500);
        })
        ._once('click', '.copyPostKey', function () {
            var theInput = $('input.PostKey');
            theInput.select();
            document.execCommand("copy");
            var theDelay = setTimeout(function () {
                theInput.blur();
            }, 500);
        })
        ._once('click', '.gotoAdmin', function (e) {
            var btn = $(this);
            var href = btn.attr('data-href');
            console.log(href);
            //============== Aarti's Modification Start Date: 2020-03-24 =============/
            if ($('#macanta-body').find('.changed-input').length) {
              e.preventDefault();
              alert("You have unsaved changes on this page!");
              return false;
            }
            else
            {
              window.location = href;   
            }
            //============== Aarti's Modification Start Date: 2020-03-24 =============/
            
            var theDelay = setTimeout(function () {
                window.location.reload(true);
            }, 1000);
        })
        ._once('keypress', '[data-type=WholeNumber]', function (event) {
            var allowedChars = '0123456789,';
            var entered = event.key;
            if (allowedChars.indexOf(entered) < 0) return false;
        })
        ._once('keypress', '[data-type=DecimalNumber]', function (event) {
            var allowedChars = '0123456789.';
            var entered = event.key;
            if (allowedChars.indexOf(entered) < 0) return false;
        })
        ._once('change', '[data-type=DecimalNumber]', function () {
            var value = $(this).val();
            if (!$.isNumeric(value)) {
                alert('Oopss!, Please check the Decimal Number value you entered.');
                $(this).focus();
            }
        })
        ._once('click', 'button.ShowVerifyPhoneWindow', function () {
            var Modal = $("#CallerIdWindow");
            Modal.modal('show');
        })
        ._once('click', 'i.EditCallerIdLabelIcon', function () {
            var CallerIdSID = $(this).attr("data-sid");
            var CallerId = $(this).attr("data-callerid");
            var CallerIdLabel = $(this).attr("data-label");
            var CallerIdIsDevice = $(this).attr("data-isdevice") === 'yes';
            var Modal = $("#EditCallerIdWindow");
            $("input.LabelToVerify", Modal)
                .val(CallerIdLabel)
                .attr("data-sid", CallerIdSID)
                .attr("data-callerid", CallerId);
            $("input#LabelToCallDeviceEdit", Modal).prop("checked", CallerIdIsDevice);
            Modal.modal('show');
        })
        ._once('click', 'button#SaveCallerIdLabel', function () {
            var Modal = $("#EditCallerIdWindow");
            saveCallerIdLabel(Modal);
        })
        ._once('click', 'button#MakeDefaultCallerId', function () {
            var Modal = $("#EditCallerIdWindow");
            makeDefaultCallerId(Modal);
        })
        ._once('click', 'i.DeleteCallerIdIcon', function () {
            var sid = $(this).attr("data-sid");
            var phone = $(this).attr("data-phone");
            var sure = confirm("Delete This Caller Id?");
            if (!sure) return false;
            deleteCallerId(sid, phone);
        })
        ._once('hidden.bs.modal', '#CallerIdWindow', function () {
            $(".phone-progress").html('Initializing, Please wait..').fadeOut('fast', function () {
                $(".validate-phone-div").fadeIn('fast');
                $("input.NumberToVerify").val('');
                $("input.ExtToVerify").val('');
                $("input.LabelToVerify").val('');
            });
        })
        ._once('click', 'button#ButtonToVerify', function () {
            verifyCallerId();
        })
        ._once('mousedown', "span.CallPhone", function (e) {
            $(this).addClass('mouseDown');
        })
        ._once('mouseup mouseout', "span.CallPhone", function (e) {
            $(this).removeClass('mouseDown');
        })
        ._once('click', "span.CallPhone", function (e) {
            console.log('CallPhone');
            var Field = $(this).attr('data-phone');
            var $this = $("a." + Field + "Twilio");

            var theId = $($this).attr('Id');
            var href = $($this).data('href');
            var PhoneField = $("#" + theId).attr('data-phonefield');
            if (PhoneField === 'DialedPhone') {
                PhoneField = $("#DialedPhone").intlTelInput("getNumber");
            }
            console.log("PhoneField: " + PhoneField);
            console.log("Id: " + theId);
            var Mobile = $('input.Phone1').val();
            var LandLine = $('input.Phone2').val();
            e.preventDefault();
            $('[data-toggle="tab"][href="' + href + '"]').trigger('click');
            var UserCallerId = $("select#UserCallerIds").val();
            var FirstName = ContactInfo.FirstName;
            var LastName = ContactInfo.LastName;
            console.log('CallerId: ' + UserCallerId);
            var device = $("select#UserOutboundDevices").val();
            if (device === "") {
                DeviceUsed = $("select#UserOutboundDevices option[value='" + device + "']").html();
                CallerIdUsed = $("select#UserCallerIds option[value='" + UserCallerId + "']").html();
                params = {"PhoneNumber": PhoneField, "From": UserCallerId};
                $('.phoneCall')
                    .attr('disabled', true)
                    .hide();
                $('.EndphoneCall').show();
                var TwilioConnection = Twilio.Device.connect(params);
                console.log('Twilio Connection');
                console.log(TwilioConnection);
                $('.SaveNote').removeAttr('disabled');
            } else {
                DeviceUsed = $("select#UserOutboundDevices option[value='" + device + "']").html();
                CallerIdUsed = $("select#UserCallerIds option[value='" + UserCallerId + "']").html();
                console.log('Divice Connection');
                $('.phoneCall')
                    .attr('disabled', true)
                    .hide();
                $('.DevicephoneCall').show();
                deviceConnect(PhoneField, UserCallerId, device, FirstName, LastName);
            }

        })
        ._once('click', "button.user-settings", function () {
            $('#UserSettingsWindow').modal('show');
        })
        ._once('click', "label.toggleCallerId", function () {
            var UserContactId = $("form#MacantaCallerIdList input.UserId").val();
            if (UserContactId === '') {
                alert("Opps.. Please select User from the Macanta Users List");
                return false;
            }
            var putdelay = setTimeout(function () {
                var CallerIDFormValues = $("form#MacantaCallerIdList").serializeArray();
                CallerIdFormJSON = JSON.stringify(CallerIDFormValues);
                console.log(CallerIdFormJSON);
                saveUsersCallerId(CallerIdFormJSON);
            }, 10);

        })
        ._once('click', '#MacantaUsersList ul.itemContainer li', function () {
            $('#MacantaUsersList ul.itemContainer li').removeClass('active');
            $(this).addClass('active');
            var UserContactId = $(this).attr('data-guid');
            //console.log('User Id: '+ UserContactId);
            if (typeof UsersCallerIds[UserContactId] !== 'undefined') {
                UserCallerIds = UsersCallerIds[UserContactId];
                renderUserCallerIds(UserCallerIds, UserContactId);
                //console.log(UsersCallerIds[UserContactId]);
            } else {
                UserCallerIds = [];
                renderUserCallerIds(UserCallerIds, UserContactId);
            }

        })
        ._once('change', "select#UserCallerIds", function () {
            var theCallerId = $(this).val();
            $("select#UserOutboundDevices option.reg-item").prop("disabled", false);
            if (theCallerId !== "") $("select#UserOutboundDevices option[value='" + theCallerId + "']").prop("disabled", true);
            saveDefaultCallerId(theCallerId);
        })
        ._once('change', "select#UserOutboundDevices", function () {
            var theOutboundDevice = $(this).val();
            $("select#UserCallerIds option.reg-item").prop("disabled", false);
            if (theOutboundDevice !== "") $("select#UserCallerIds option[value='" + theOutboundDevice + "']").prop("disabled", true);
            saveOutboundDevice(theOutboundDevice);

        })
        ._once('click', "button.addUserConnectedInfo", function () {

            var GUID = $(this).attr('data-guid');
            $("table#UserConnectorInfoTable[data-guid=" + GUID + "] tr.infoItem").removeClass("activeItem");
            $(this).attr('disabled', true);
            var ItemId = 'item_' + new Date().getTime().toString(36);
            var theTableList = $("table#UserConnectorInfoTable[data-guid=" + GUID + "]");
            $('tbody', theTableList).append('<tr class="infoItem" data-itemid="' + ItemId + '" data-connectedcontacts ="' + ('{"' + ContactId + '":{"relationships":[],"ContactId":"' + ContactId + '","FirstName":"' + ContactInfo.FirstName + '","LastName":"' + ContactInfo.LastName + '","Email":"' + ContactInfo.Email + '"}}').b64encode() + '"></tr>');
            var theForm = $("form.FormUserConnectedInfo[data-guid=" + GUID + "]");

            $("fieldset.cd-fields",theForm).addClass('lightYellow');
            $("fieldset.cd-fields",theForm).removeClass('lightGreen');
            theForm.trigger('reset');

            $("[data-jsonvalue]", theForm).each(function () {
                $(this).attr("data-jsonvalue", "{}");
            });

            $("select.addGroup", theForm).each(function () {
                var defaultValue = $(this).attr('data-default');
                $(this).val(defaultValue);
            });
            theForm.attr('data-itemid', ItemId);
            $("button.cancelUserConnectedInfo", theForm).removeClass("hideThis");
            $("button.deleteUserConnectedInfo", theForm).addClass("hideThis");
            $("ul.relationship-list", theForm).html("");
            $("ul.file-attachments-list", theForm).html("");
            $("select.ConnectedContactRelationships", theForm)
                .multiselect({
                    //includeSelectAllOption: true
                    numberDisplayed: 5,
                    maxHeight: 200,
                    allSelectedText: false
                })
                .multiselect('select', []);
            $(".ConnectedContactRelationships", theForm).multiselect('enable');
            $(".addGroup", theForm).removeAttr('disabled');
            $("div.form-group", theForm).first().find("input").focus();
        })
        ._once('click', "button.cancelUserConnectedInfo", function () {
            var GUID = $(this).attr('data-guid');
            var theForm = $("form.FormUserConnectedInfo[data-guid=" + GUID + "]");
            var itemId = theForm.attr('data-itemid');
            if (itemId !== '') {
                var toremove = $("tr[data-itemid=" + itemId + "]");
                if (typeof toremove.attr("data-raw") === "undefined") toremove.remove();
            }
            theForm.trigger('reset');
            theForm.attr('data-itemid', '');
            $("fieldset.cd-fields",theForm).removeClass('lightYellow');
            $("fieldset.cd-fields",theForm).removeClass('lightGreen');
            $("select.ConnectedContactRelationships")
                .multiselect({
                    //includeSelectAllOption: true
                    numberDisplayed: 5,
                    maxHeight: 200,
                    allSelectedText: false
                })
                .multiselect('select', []);
            $(this).addClass('hideThis');
            $("button.deleteUserConnectedInfo", theForm).removeClass("hideThis");
            $("button.addUserConnectedInfo[data-guid=" + GUID + "]").removeAttr('disabled');
            $("input#itemId", theForm).val('');
            $(".ConnectedContactRelationships", theForm).multiselect('disable');
            $(".addGroup", theForm).attr('disabled', true);
        })
        ._once('click', "tr.OppItem", function () {
            var opp = $(this);
            var parentTable = opp.parents('table#OppTable');
            $("tr.OppItem", parentTable).removeClass('active');
            opp.addClass('active');
            var tabId = parentTable.attr('data-shortcodeid');
            console.log("the id: " + tabId);
            var oppid = opp.attr('data-oppid');
            var allowedmoves = $.parseJSON(opp.attr('data-allowedmove'));
            var nextactiondate = opp.attr('data-nextactiondate');
            var closedate = opp.attr('data-closedate');
            var nextactionnote = opp.attr('data-nextactionnote');
            var stageid = opp.attr('data-stageid');
            var oppTab = $("div#" + tabId);
            var customfields = $.parseJSON(opp.attr('data-customfields').b64decode());
            console.log(customfields);
            $('.CurrentOpportunityNotesContainer' + tabId + ' .quick_notes_tags-' + tabId).importTags('#opp_' + oppid + ',#opportunity');
            allowedmoves.id.push(parseInt(stageid));
            console.log(allowedmoves.id);
            $("input#Id", oppTab).val(oppid);
            $("input#NextActionDate", oppTab).val(nextactiondate);
            $("input#CloseActionDate", oppTab).val(closedate);
            $("textarea#NextActionNote", oppTab).val(nextactionnote.b64decode());
            /*
            Reset Custom Fields Value
            */
            //$(".OppCustomFIeldsContainer input:not([type=radio], [type=checkbox])", oppTab).val('');
            //$('.OppCustomFIeldsContainer input[type="radio"]', oppTab).prop('checked', false);
            //$('.OppCustomFIeldsContainer select', oppTab).val('');
            //$('.OppCustomFIeldsContainer select[multiple="MULTIPLE"]', oppTab).val([]);
            $.each(customfields, function (Id, Value) {
                if (typeof Value === 'object') {
                    Value = Value.date || '';
                    if (Value !== '') {
                        var theDate = Value.split(' ');
                        Value = theDate[0];
                    }
                }
                $("." + Id+":not([type=radio], [type=checkbox])", oppTab).val(Value);
                $('input.' + Id + '[type="radio"][value="' + Value + '"]').prop('checked', true);
                if (typeof Value === 'string') {
                    var multiSelectVal = Value.split(",");
                    $('select.' + Id + '[multiple="MULTIPLE"]', oppTab).val(multiSelectVal);
                }
            })
            $("select#OppStage option", oppTab).each(function () {
                var theOption = $(this);
                var theVal = $(this).attr('value');
                if ($.inArray("ALL", allowedmoves.id) !== -1) {
                    theOption.removeClass('hideThis');
                } else {
                    if ($.inArray(parseInt(theVal), allowedmoves.id) !== -1) {
                        theOption.removeClass('hideThis');
                    } else {
                        theOption.addClass('hideThis');
                    }
                }

            });
            $("select#OppStage optgroup", oppTab).each(function () {
                var theOptgroup = $(this);
                if ($("option", theOptgroup).not(".hideThis").length === 0) {
                    theOptgroup.addClass("hideThis");
                } else {
                    theOptgroup.removeClass("hideThis");
                }
            });
            $("select#OppStage", oppTab).val(stageid);

        })
        ._once('submit', "form#updateopp", function (evt) {
            evt.preventDefault();
            var formVal = $(this).serializeArray();
            var Identifier = $(this).attr('data-identifier');
            updateOpp(JSON.stringify(formVal), Identifier);

        })
        ._once('click', "tr.infoItem", function (e) {
            if($(e.target).is("button")) return false;
            if($(e.target).is("i")) return false;
            var theTR = $(this);
            if(theTR.hasClass('activeItem')) {return false;}
            var parentTable = theTR.parents("table#UserConnectorInfoTable");
            var GUID = parentTable.attr("data-guid");
            var ItemId = theTR.attr("data-itemid");
            /*
            var itemRaw = theTR.attr("data-raw");
            if (typeof itemRaw === "undefined") return false;
            var values = JSON.parse(itemRaw.b64decode());
            var itemConnectedContacts = theTR.attr("data-connectedcontacts");
            var ConnectedContacts = JSON.parse(itemConnectedContacts.b64decode());
            */

            var ContactConenctedInfoStr = localStorage.getItem(GUID) || {};
            var ContactConenctedInfoObj = JSON.parse(ContactConenctedInfoStr);
            var values = ContactConenctedInfoObj[ItemId]['value'];
            var ConnectedContacts = ContactConenctedInfoObj[ItemId]['connected_contact'];

            $("tr.infoItem", parentTable).removeClass("activeItem");
            theTR.addClass("activeItem");

            var theForm = $("form.FormUserConnectedInfo[data-guid=" + GUID + "]");
            theForm.attr('data-itemid', ItemId);
            //UserConnectorInfoTable[GUID].draw(false);
            theForm.css("opacity", 0.1);
            var thedelayTimer = setTimeout(() => {
                $("button.cancelUserConnectedInfo", theForm).trigger('click');
                /*console.log(values);*/
                theForm.attr('data-itemid', ItemId);
                $("fieldset.cd-fields",theForm).addClass('lightGreen');
                $(".addGroup", theForm).removeAttr('disabled');
                $('form[data-itemid="'+ItemId+'"] input.CDFileAttachments').fileinput('enable');

                $('form[data-itemid="'+ItemId+'"] input.url-attachment').removeAttr('disabled');
                $('form[data-itemid="'+ItemId+'"] a.url-attachment-btn').removeAttr('disabled');
                $(".ConnectedContactRelationships", theForm).multiselect('enable');
                //UserConnectorInfoTable[GUID].draw(false);
                $.each(values, function (fieldName, fieldValue) {
                    fieldValue = fieldValue === null ? "" : fieldValue;
                    if (typeof fieldValue === 'object') {
                        if(typeof $("[name='" + fieldName + "']", theForm).attr('data-jsonvalue') !== "undefined"){
                            $("[name='" + fieldName + "']", theForm).attr('data-jsonvalue', JSON.stringify(fieldValue));
                            if (typeof fieldValue["id_" + ContactId] !== 'undefined') {
                                fieldValue = fieldValue["id_" + ContactId];
                            } else {
                                fieldValue = '';
                            }
                        }else{
                            fieldValue = '';
                        }
                    } else {
                        //$("[name='"+fieldName+"']",theForm).removeAttr('data-jsonvalue');
                    }
                    //console.log(fieldName+" : "+fieldValue);

                    if (typeof fieldValue === "string") {
                        var fieldValueLowercase = fieldValue.toLowerCase();
                        var htmlDecoded = $("<div />").html(fieldValue).text();
                    }

                    /* HANDLE INPUT TEXT FIELD */
                    $("input.input[type='text'][name='" + fieldName + "']", theForm).val(fieldValue);

                    /* HANDLE INPUT URL FIELD */
                    $("input.input[type='url'][name='" + fieldName + "']", theForm).val(fieldValue);

                    /* HANDLE INPUT EMAIL FIELD */
                    $("input.input[type='email'][name='" + fieldName + "']", theForm).val(fieldValue);

                    /* HANDLE INPUT PASSWORD FIELD */
                    $("input.input[type='password'][name='" + fieldName + "']", theForm).val(fieldValue);

                    /* HANDLE INPUT NUMBER FIELD */
                    $("input.input[type='number'][name='" + fieldName + "']", theForm).val(fieldValue);

                    /* HANDLE TEXTAREA FIELD */
                    $("textarea[name='" + fieldName + "']", theForm).val(htmlDecoded);

                    /* HANDLE SELECT FIELD */
                    $("select[name='" + fieldName + "'] option", theForm).each(function () {
                        $(this).removeAttr('selected');
                        $(this).prop('selected', false)
                    });
                    $("select[name='" + fieldName + "'] option", theForm).each(function () {
                        var dataValue = $(this).attr('data-value');
                        if (fieldValueLowercase === dataValue) $(this).attr('selected', true);
                        if (fieldValueLowercase === dataValue) $(this).prop('selected', true);
                    });
                    var aliasesArr;
                    var fullValue;
                    /* HANDLE INPUT CHECKBOX FIELD */
                    $("input.checkbox[name='" + fieldName + "']", theForm).removeAttr('checked');
                    if ($("input.checkbox[name='" + fieldName + "']", theForm).length > 0) {
                        var valArr;
                        if (fieldValueLowercase.indexOf("|") >= 0) {
                            valArr = fieldValueLowercase.split("|");
                            $("input.checkbox[name='" + fieldName + "']", theForm).each(function () {
                                fullValue = $(this).attr('data-fullvalue');
                                aliasesArr = fullValue.split(";");
                                $.each(valArr, function (index, value) {
                                    $.each(aliasesArr, function (index,alias) {
                                        var aliasValueLowercase = alias.toLowerCase();
                                        if (aliasValueLowercase === value){
                                            $("input.checkbox[data-value=\"" + aliasesArr[0] + "\"][name='" + fieldName + "']", theForm).prop("checked", true);
                                            return false
                                        }
                                    });
                                });
                            });
                        } else if (fieldValueLowercase.indexOf(",") >= 0) {
                            valArr = fieldValueLowercase.split(",");
                            $("input.checkbox[name='" + fieldName + "']", theForm).each(function () {
                                fullValue = $(this).attr('data-fullvalue');
                                aliasesArr = fullValue.split(";");
                                $.each(valArr, function (index, value) {
                                    $.each(aliasesArr, function (index,alias) {
                                        var aliasValueLowercase = alias.toLowerCase();
                                        if (aliasValueLowercase === value){
                                            $("input.checkbox[data-value=\"" + aliasesArr[0] + "\"][name='" + fieldName + "']", theForm).prop("checked", true);
                                            return false
                                        }
                                    });
                                });
                            });
                        } else {
                            $("input.checkbox[name='" + fieldName + "']", theForm).each(function () {
                                fullValue = $(this).attr('data-fullvalue');
                                aliasesArr = fullValue.split(";");
                                $.each(aliasesArr, function (index,alias) {
                                    var aliasValueLowercase = alias.toLowerCase();
                                    if (aliasValueLowercase === fieldValueLowercase){
                                        $("input.checkbox[data-value=\"" + aliasesArr[0] + "\"][name='" + fieldName + "']", theForm).prop("checked", true);
                                        return false
                                    }
                                });
                            });
                        }
                    }

                    /* HANDLE INPUT RADIO FIELD */
                    if ($("input.radio[name=\"" + fieldName + "\"]", theForm).length > 0) {

                        $("input.radio[name=\"" + fieldName + "\"]", theForm).each(function () {
                            fullValue = $(this).attr('data-fullvalue');
                            aliasesArr = fullValue.split(";");
                            $.each(aliasesArr, function (index,alias) {
                                var aliasValueLowercase = alias.toLowerCase();
                                if (aliasValueLowercase === fieldValueLowercase){
                                    $("input.radio[data-value=\"" + aliasesArr[0] + "\"][name='" + fieldName + "']", theForm).prop("checked", true);
                                    return false
                                }
                            });
                        });
                    }

                });
                theForm.css("opacity", 1);
                //UserConnectorInfoTable[GUID].draw(false);
                getCDFileAttachments(ItemId);
                getConnectedContactNotes(ConnectedContacts, GUID);

                $("select.ConnectedContactRelationships").multiselect('select', []);
                var template = $("li.HTML-Template.relationship-list-item");
                var theList = $("ul.relationship-list", theForm);
                var theGroup = theForm.attr("data-guid");
                var availableRelationship = relationshipRules[theGroup];
                /*console.log(ConnectedContacts);*/
                $("ul.relationship-list").html('');
                $.each(ConnectedContacts, function (theId, theRelationships) {
                    if (theId === ContactId) {
                        $("select.ConnectedContactRelationships" + theId).multiselect('select', theRelationships.relationships);
                    } else {
                        var LastName = theRelationships.LastName || '';
                        var FullName = theRelationships.FirstName + " " + LastName;
                        template
                            .clone()
                            .removeClass("HTML-Template")
                            .attr("data-contactid", theId)
                            .appendTo(theList)
                            .find("h3.connected-contact-name").html(FullName + ' <small class="footnote">Contact Id: ' + theId + '</small><small>' + theRelationships.Email + '</small>')
                            .on('click', function () {
                                //============== Aarti's Modification Start Date: 2020-03-24 =============/
                                 if ($('#macanta-body').find('.changed-input').length) {
                                   alert("You have unsaved changes on this page!");
                                   return false;
                                 }
                                 else
                                 {
                                   window.location = '/#contact/' + theId;
                                 }
                                 //============== Aarti's Modification Start Date: 2020-03-24 =============/
                                
                                var theDelay = setTimeout(function () {
                                    window.location.reload(true);
                                }, 300);
                            })
                            .parents("li.relationship-list-item")
                            .find("i.DeleteConenctedContactItem")
                            .attr("data-contactid", theId)
                            .attr("data-itemid", ItemId)
                            .attr("data-groupid", theGroup)
                            .on("click", function () {
                                var ContactIdTobeDeleted = $(this).attr("data-contactid");
                                var _theGroup = $(this).attr("data-groupid");
                                var _theItem = $(this).attr("data-itemid");


                                var _ContactConenctedInfoStr = localStorage.getItem(_theGroup) || {};
                                var _ContactConenctedInfoObj = JSON.parse(_ContactConenctedInfoStr);
                                var _currentConnectedContacts = _ContactConenctedInfoObj[_theItem]['connected_contact'];

                                var theParent = $(this).parents("li.relationship-list-item");
                                var theConfirm = confirm("Are you sure to remove this connected contact?");
                                if (theConfirm) {
                                    theParent.slideUp('fast', function () {
                                        theParent.remove();
                                        delete _currentConnectedContacts[ContactIdTobeDeleted];
                                        _ContactConenctedInfoObj[_theItem]['connected_contact'] = _currentConnectedContacts;
                                        _ContactConenctedInfoStr = JSON.stringify(_ContactConenctedInfoObj);
                                        localStorage.setItem(_theGroup,_ContactConenctedInfoStr);
                                    })
                                } else {
                                    return false;
                                }
                            })
                            .parents("li.relationship-list-item")
                            .find("select")
                            .attr("data-contactid", theId)
                            .attr("data-email", theRelationships.Email)
                            .attr("data-firstname", theRelationships.FirstName)
                            .attr("data-lastname", theRelationships.LastName)
                            .attr("id", "ConnectedContactRelationships" + theId)
                            .addClass("ConnectedContactRelationships" + theId)
                            .find("option").each(function () {
                            var theOption = $(this);
                            var allowed = [];
                            $.each(availableRelationship, function (index, theRules) {
                                allowed.push(theRules.Id)
                            })
                            if ($.inArray(theOption.attr("value"), allowed) === -1) theOption.remove();

                        });

                        $("li.relationship-list-item select[data-contactid=" + theId + "]").multiselect({
                            //includeSelectAllOption: true
                            numberDisplayed: 5,
                            maxHeight: 200,
                            allSelectedText: false
                        }).multiselect('select', theRelationships.relationships);

                    }
                    //console.log(theId);
                    //console.log(theRelationships);
                });
                checkItemsToDisable(theGroup, ItemId);
                UserConnectorInfoTable[GUID].draw(false);
            }, 300);

        })
        ._once('click', "i.connected-data-url", function () {
            var fieldId = $(this).attr("data-field");
            var url = $("input[name=" + fieldId + "]").val();
            if (url === "") return false;
            var new_window = window.open(url, "macantaTab");
        })
        ._once('change', "select#infusionsoftCustomField", function () {
            var value = $(this).val();
            var parentLi = $(this).parents("li.field-item");
            parentLi.find("div.field-item span.is-custom-field").html(value)
        })
        ._once('click', ".showContact", function () {
            $(".ContactInfoHideMobile").slideToggle("slow", function () {
                if ($(this).is(":hidden")) {
                    $(".showContact").html("Show Contact Details");
                } else {
                    $(".showContact").html("Hide Contact Details");
                }
            });
        })
        ._once('shown.bs.tab', '#macanta-tabs .nav-tabs a', function () {
            if(TabMenuHeight === ''){
                TabMenuHeight = $('#MainContent .tab-menu').outerHeight();
            }
            var theHTML = $(this).html();
            var theId = $(this).attr("href");
            var TabPane = $(theId);
            $('a.navbar-brand').html(theHTML);
            if ($('.navbar-collapse').hasClass('in')) {
                $('button.navbar-toggle').trigger('click');
            }
            if (OppTable !== {}) {
                $.each(OppTable, function (theClass, theObj) {
                    theObj.columns.adjust().responsive.recalc();
                });
            }
            var CustomTabPanel = $("div.CustomTabPanel", TabPane);
            var ConnectedInfoPanel = $("div.ConnectedInfoPanel", TabPane);
            var ContentId;
            var Title;
            if (CustomTabPanel.length !== 0){
                ContentId = CustomTabPanel.attr('data-contentid');
                Title  = CustomTabPanel.attr('data-title');
                if($.inArray(Title,LoadedTabs) === -1){
                    LoadedTabs.push(Title);
                    lazy_load(ContentId,Title,"tab_custom");
                }
            }
            if (ConnectedInfoPanel.length !== 0){
                ContentId = ConnectedInfoPanel.attr('data-contentid');
                Title  = ConnectedInfoPanel.attr('data-title');
                if($.inArray(Title,LoadedTabs) === -1){
                    LoadedTabs.push(Title);
                    lazy_load("ConnectedInfoPanel"+ContentId,Title,"tab_cd");
                }

            }
            if ($("tr.activeItem", TabPane).length === 0)
                $("tr.infoItem:first-child", TabPane).trigger("click");

            var theHeight;
            var theBodyHeight = $('#MainContent .tab-body').outerHeight();
            if(theBodyHeight > TabMenuHeight){
                theHeight = TabMenuHeight
            }else if(theBodyHeight > $(window).height()) {
                theHeight = theBodyHeight;
            }else{
                theHeight = $(window).height();
            }
            $('ul.nav.nav-tabs.tabs-left').css('height',theHeight+'px');
            if (theId === "#Admin"){
                if(CustomEditorInit === false) CustomEditor();
            }
        })
        ._once('click','input[name=customTabPermission]',function () {
            var theSuffix;
            if($(this).val() === 'ContactHaveTag'){
                theSuffix = $(this).attr('data-suffix');
                $("input.ContactHaveTag."+theSuffix).slideDown();
            }else{
                theSuffix = $(this).attr('data-suffix');
                $("input.ContactHaveTag."+theSuffix).slideUp();
            }
        })
        ._once('click','a.url-attachment-btn', function () {
            var theForm = $(this).parents("form.FormUserConnectedInfo");
            var theURL = $("input.url-attachment", theForm).val();
            var theItemId = theForm.attr('data-itemid');

            // check if valid file
            if($.trim(theURL) !== ''){
                putCDURLAttachments(theURL,theItemId)
            }else {
                alert('Please Enter File URL.');
            }
        })
        ._once('click','.tagsChangeOrder', function () {
            console.log('tagsChangeOrder Clicked!');
            var theContactId = $(this).attr('data-contactid');
            var theCatId = $(this).attr('data-catid');
            var theReadOnly = $(this).attr('data-readonly');
            var theCurrentOrder = $(this).attr('data-currentorder');
            var nextOrder = theCurrentOrder === 'Tag Name' ? 'Date Applied':'Tag Name';
            $(this).attr('data-currentorder',nextOrder);
            $(this).html('ordered by '+nextOrder);
            tagsChangeOrder(theContactId,theCatId,theReadOnly,theCurrentOrder);
        })
        ._once('submit','form.StripoPluginModalForm', function (event) {
            event.preventDefault();
            var EmailModal = $("#StripoPluginModal");
            var EmailId =  EmailModal.attr("data-emailid");
            saveMacantaStripoEmail(EmailModal,EmailId);
        })
        ._once('click','.sendTestEmailButton', function () {
            var sendTestEmail = $(this).parents(".sendTestEmail");
            var EmailId =  sendTestEmail.attr("data-emailid");
            var theContactId = sendTestEmail.find("input.sendTestEmailValue").val();
            if(theContactId){
                macantaSendTestEmail(EmailId,theContactId);
            }else{
                return false;
            }
        })
        ._once('change', '#StripoPluginModal input.TemplateName', function (event) {
            var theTitle = $(this).val();
            var emailId  = $("#StripoPluginModal").attr("data-emailid");
            if(typeof window.Stripo !== "undefined" && window.Stripo.loaded){
                var setTitle = theTitle+"::"+emailId;
                var currentTitle = window.StripoApi.getTitle();
                var EmailTitleIdArr = currentTitle.split("::");
                var EmailId = EmailTitleIdArr[1];
                if(EmailId === emailId){
                    window.StripoApi.setTitle(setTitle);
                    saveMacantaStripoEmail();
                }

            }

        })
        ._once('click','button.deleteContact', function () {
            var theContactId =  $(this).attr("data-id");
            deleteMacantaContactFromSearchResults(theContactId,$(this));
        })
        ._once('click','span.deleteContact', function () {
            var theContactId =  $(this).attr("data-id");
            deleteMacantaContactFromDashboard(theContactId,$(this));
        })
        ._once('click','button.deleteDataObjectItem', function () {
            var theItemId =  $(this).attr("data-id");
            var theGroupId =  $(this).attr("data-groupid");
            var theType =  $(this).attr("data-type");
            deleteMacantaDataObjectItem(theItemId,theGroupId, $(this),theType);
        })
       /* ._once('mouseover', '.template-thumb', function (event) {
            var parentForm =  $(this).parents("form.queryForm");
            var OrigEmailId = parentForm.find("input.queryId").val();
            var ConstantEmailId = $(this).attr("data-emailid");
            //activateChosenTemplate(OrigEmailId,ConstantEmailId);
            $(".template-options[data-emailid="+ConstantEmailId+"]").show();

        })
        ._once('mouseout', '.template-thumb', function (event) {
            var ConstantEmailId = $(this).attr("data-emailid");
            $(".template-options[data-emailid="+ConstantEmailId+"]").hide();

        })*/
    ;


    $('.selectpicker').selectpicker({
        style: 'btn-default',
        size: 4
    });
    $('.languagepicker').selectpicker({
        style: 'btn-default',
        size: 4
    });
    $('.search_filter').selectpicker({
        style: 'btn-default'
        /*size: 4*/
    });
    $('.search_filter').on('loaded.bs.select', function (e) {
        if (SelectedSearchCache != '') {
            var label = $("option[value='" + SelectedSearchCache + "']").html();
            $('.bootstrap-select button.dropdown-toggle[data-id="MainSavedSearch"] span.filter-option').html('Saved Search Filter : <span class=" selectedFilter">' + label + '</span>');
            $('.search-results .panel-title').html(label + ' Results');

        }
    });

    $('.search_str_options').selectpicker({
        style: 'btn-default'
    });

    $("#CallRecording").prependTo("body");
    //https://local.macanta.org/config/notetags json url
    $('input[name="note_daterange"]').daterangepicker({
        autoUpdateInput: false,
        locale: {
            format: 'YYYY-MM-DD'
        }
    });
    $('input[name="note_daterange"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD') + ' to ' + picker.endDate.format('YYYY-MM-DD'));
    });

    $('input[name="note_daterange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });


    $('tr.NoteItem')
        ._once('click', 'i.fa-tags', function () {
            var tagContainer = $(this).closest('td.noteDate').find('.tagsinput');
            var existingtagContainer = $(this).closest('td.noteDate').find('.tagDisplay');
            if ($(this).attr('style')) {
                $(this).removeAttr('style');
                tagContainer.slideUp("fast");
                existingtagContainer.slideDown();
            } else {
                $(this).css('display', "block");
                tagContainer.hide().slideDown().find('input').focus();
                existingtagContainer.slideUp("fast");
            }


        });
    $(".DialedPhone").on('change', function () {
        $(this).attr('data-phone', $(this).val());
    });
    $(document)._once("click", '.EndphoneCall', function (e) {
        Twilio.Device.disconnectAll();

    });







    tagIt();
    autoCompleteIt();
    startDialer();
    relationshipIt()


};
//Initialise macanta scripts
var availableTags = []; // for auto complete
var availableRelationships = []; // for auto complete
var relationshipRules = {};
var TwilioToken = '';
var CallCenterAdmin = false;
var CallCenterAgent = false;
var SyncToken = {};
var WorkerActivities = {};
var CallCenterWorker;
var CallCenterWorkers = {};
var CallCenterWorkspace;
var CallcenterTaskQueues = {};
var WorkspaceToken = '';
var TaskQueueTokens = {};
var WorkersTokens = {};
var TwilioParams = {};
var ContactsPreloaderDataTable;
var AgentsPreloaderDataTable;
var contactToConnectTimeOut;
var contactToBeAgentTimeOut;
var theThis;
var agentSid;
var workspaceSid;
var CallCenterHeartbeat;
var McantaTaskRouter = {};
var initAgentSwitchToggle;
/*TWILIO CONTORLS*/

$(function () {


    window.on = function (event) {
        alert('Close or refresh');
        return true;
    }
    ContactsPreloaderDataTable = $("#ConnectOtherContact .contact-preloading-list").DataTable({
        "pageLength": 10,
        responsive: true,
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "info": false,
        "createdRow": function (row, data, index) {
            //$(row).attr('data-contactid',data[3]);
        }
    });
    AgentsPreloaderDataTable = $("#AddCallCenterAgent .contact-preloading-list").DataTable({
        "pageLength": 10,
        responsive: true,
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "info": false,
        "createdRow": function (row, data, index) {
            //$(row).attr('data-contactid',data[3]);
        }
    });

    $(document).on('hidden.bs.modal', "#ConnectOtherContact", function (e) {
        $("input.search-contact-to-connect").val('').attr("data-itemid", '');
    });
    $("input.search-contact-to-connect").on("keyup", function () {
        theThis = $(this);
        if (contactToConnectTimeOut) clearTimeout(contactToConnectTimeOut);
        contactToConnectTimeOut = setTimeout(function () {
            if (ajax_requests['contact_search'] && ajax_requests['contact_search'].readyState != 4) {
                ajax_requests['contact_search'].abort();
                $(".contact-preloading").removeClass("loading-overlay loading-center");
                $(".contact-preloading-list").css("opacity", 1);
            }
            searchKey = theThis.val();
            if (!searchKey) return false;
            jsonData = {
                "controler": "core/common",
                "action": "quick_search",
                "session_name": session_name,
                "data": {"searchKey": searchKey, "session_name": session_name,"assetsVersion":assetsVersion}
            };
            console.log(jsonData);
            $(".contact-preloading").addClass("loading-overlay loading-center");
            $(".contact-preloading-list").css("opacity", 0.1);

            ajax_requests['contact_search'] = $.ajax({
                url: ajax_url,
                type: "POST",
                data: jsonData,
                success: function (e) {
                    console.log(e);
                    $(".contact-preloading").removeClass("loading-overlay loading-center");
                    $(".contact-preloading-list").css("opacity", 1);
                    var ItemDatatable = {};
                    ItemDatatable['Column'] = [];
                    var theItem = $("input.search-contact-to-connect").attr("data-itemid");
                    var theForm = $("form.FormUserConnectedInfo[data-itemid=" + theItem + "]");
                    var theList = $("ul.relationship-list li", theForm);
                    var ContactIdArr = [parseInt(ContactId)];
                    $(theList).each(function () {
                        ContactIdArr.push(parseInt(theThis.attr("data-contactid")));
                    });
                    console.log("Existing Id's");
                    console.log(ContactIdArr);
                    $(e.data).each(function (index, theContact) {
                        //var theContact = $(this);
                        if ($.inArray(theContact.Id, ContactIdArr) !== -1) return true;
                        var FirstName = theContact.FirstName || '';
                        var LastName = theContact.LastName || '';
                        var theEmail = theContact.Email || 'No Email';
                        var itemFootnote = '<small class=\'footnote\'>Contact Id: ' + theContact.Id + '</small>'
                        ItemDatatable['Column'].push([FirstName + ' ' + LastName + itemFootnote, theEmail]);
                        ItemDatatable[theEmail] = {
                            FirstName: FirstName,
                            LastName: LastName,
                            Email: theEmail,
                            ContactId: theContact.Id
                        };

                    });
                    renderContactsPreloaderDataTable(ItemDatatable);
                    bindPreloadingListTR();
                }
            });
        }, 1000);

    });
    $("input.search-contact-tobe-agent").on("keyup", function () {
        theThis = $(this);
        if (contactToBeAgentTimeOut) clearTimeout(contactToBeAgentTimeOut);
        contactToBeAgentTimeOut = setTimeout(function () {
            if (ajax_requests['contact_search'] && ajax_requests['contact_search'].readyState != 4) {
                ajax_requests['contact_search'].abort();
                $(".contact-preloading").removeClass("loading-overlay loading-center");
                $(".contact-preloading-list").css("opacity", 1);
            }
            searchKey = theThis.val();
            if (!searchKey) return false;
            jsonData = {
                "controler": "core/common",
                "action": "quick_search",
                "session_name": session_name,
                "data": {"searchKey": searchKey, "session_name": session_name,"assetsVersion":assetsVersion}
            };
            console.log(jsonData);
            $("#AddCallCenterAgent .contact-preloading").addClass("loading-overlay loading-center");
            $("#AddCallCenterAgent .contact-preloading-list").css("opacity", 0.1);
            var ContactIdArr = [];
            $("table.callcenter-agents tbody tr").each(function () {
                ContactIdArr.push(parseInt($(this).attr('data-contactid')));
            });
            ajax_requests['contact_search'] = $.ajax({
                url: ajax_url,
                type: "POST",
                data: jsonData,
                success: function (e) {
                    console.log(e);
                    $("#AddCallCenterAgent .contact-preloading").removeClass("loading-overlay loading-center");
                    $("#AddCallCenterAgent .contact-preloading-list").css("opacity", 1);
                    var ItemDatatable = {};
                    ItemDatatable['Column'] = [];
                    /*var theItem = $("input.search-contact-to-connect").attr("data-itemid");
                    var theForm = $("form#FormUserConnectedInfo[data-itemid="+theItem+"]");
                    var theList = $("ul.relationship-list li", theForm);
                    var ContactIdArr = [parseInt(ContactId)];
                    $(theList).each(function(){
                        ContactIdArr.push(parseInt($(this).attr("data-contactid")));
                    });*/

                    console.log("Existing Id's");
                    console.log(ContactIdArr);
                    $(e.data).each(function (index, theContact) {
                        if ($.inArray(theContact.Id, ContactIdArr) !== -1) return true;
                        var FirstName = theContact.FirstName || '';
                        var LastName = theContact.LastName || '';
                        var theEmail = theContact.Email || 'No Email';
                        var itemFootnote = '<small class=\'footnote\'>Contact Id: ' + theContact.Id + '</small>'
                        ItemDatatable['Column'].push([FirstName + ' ' + LastName + itemFootnote, theEmail]);
                        ItemDatatable[theEmail] = {
                            FirstName: FirstName,
                            LastName: LastName,
                            Email: theEmail,
                            ContactId: theContact.Id
                        };

                    });
                    renderAgentsPreloaderDataTable(ItemDatatable);
                }
            });
        }, 1000);

    });
    // close tag editor hen click outside
    $('.front-page')._once('click', function (event) {
        var clicked = $(event.target);
        var tdParent = clicked.parents('td.noteDate');
        var tagContainer = clicked.parents("div.tagsinput");
        if (typeof tagContainer.attr('class') === 'undefined') {

            if (clicked.hasClass('fa-tags') || clicked.hasClass('tagsinput')) {
                var instancePaternt = clicked.parents('td.noteDate');
                return false;
            } else {
                $('tr.NoteItem i.fa-tags').removeAttr('style');
                $('div.prev-notes .tagsinput').slideUp("fast");
                $('div.prev-notes .tagDisplay').slideDown();
            }
        }
    });
    $('.toggleThisCallerID').toggles({
        text: {
            on: 'Done', // text for the ON position
            off: 'Not Done' // and off
        },
        on: false, // is the toggle ON on init
        animate: 150, // animation time (ms)
        easing: 'easeOutQuint', // animation transition easing function
        width: 80, // width used if not set in css
        height: 25 // height if not set in css
    });
    var hash = window.location.hash.substring(1);
    if (hash) {
        var thisHash = hash.split('/');
        ContactId = thisHash[1];
        //console.log(thisHash);
        processHash(thisHash);
    } else {
        console.log("You Are At Homepage");
        getFrontPage()
    }

    Macanta('On Load');

});
//============== Aarti's Modification Start Date: 2020-03-23 =============/
 $('#macanta-body').on('change keyup keydown', '.theConnectedInfoPanelBody input, .theConnectedInfoPanelBody textarea, .theConnectedInfoPanelBody select', function (e) {
     $(this).addClass('changed-input');
     $("a[data-toggle='tab']").prop('disabled', true);               
 });
 $('#macanta-body').on('change keyup keydown', '.FormFieldDetails input, .FormFieldDetails textarea, .FormFieldDetails select', function (e) {
     $(this).addClass('changed-input');
     $("a[data-toggle='tab']").prop('disabled', true);            
 });

$(document).on('click','.nav-tabs li', function(){
   if ($('#macanta-body').find('.changed-input').length) {
    alert("You have unsaved changes on this page!");
    return false;
   } 
});

$(document).on('click','.showRecentResults, .connected-contact-name-trash-container',function(){
  if ($('#macanta-body').find('.changed-input').length) {
      alert("You have unsaved changes on this page!");
      return false;
  }
});

/*
$(document).on('click','#connectorsTable li .field-content, #connectorsTable .preview_content, .undo_markdown_preview, .markdown_preview', function(e){
         return false;
});
*/

$(document).on('click','.ConnectedInfoListContainer li, .ConnectedInfoListTitle', function(e){
    if ($('#macanta-body').find('.changed-input').length) {
     alert("You have unsaved changes on this page!");
     return false;
    }
});
//============== Aarti's Modification Start Date: 2020-03-23 =============/