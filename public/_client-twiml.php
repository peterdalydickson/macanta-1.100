<?php
header('Content-type: text/xml');
file_put_contents(dirname(__FILE__)."/client-twiml.txt",json_encode($_REQUEST)."\n\n",FILE_APPEND);
$theURL = $_SERVER['HTTP_HOST'];
$parsedTheURL = parse_url($theURL);
$theHost = explode('.', $parsedTheURL['path']);
$theSubdomain = $theHost[0];
$CallerId = $this->config->item('macanta_caller_id');
// put your default Twilio Client name here, for when a phone number isn't given
// $number   = "jenny";

// get the phone number from the page request parameters, if given
if (isset($_REQUEST['PhoneNumber'])) {
    $number = htmlspecialchars($_REQUEST['PhoneNumber']);
    file_put_contents('php://stderr', print_r($number, TRUE));
} else {
    file_put_contents('php://stderr', print_r("PhoneNumber not set", TRUE));
    // $number="+442476276203";
}

// wrap the phone number or client name in the appropriate TwiML verb
// by checking if the number given has only digits and format symbols
if (preg_match("/^[\d\+\-\(\) ]+$/", $number)) {
    $numberOrClient = "<Number>" . $number . "</Number>";
} else {
    $numberOrClient = "<Client>" . $number . "</Client>";
}
?>
<Response>
    <Dial record="record-from-ringing" callerId="<?php echo $CallerId ?>">
        <?php echo $numberOrClient ?>
    </Dial>
</Response>