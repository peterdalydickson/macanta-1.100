<script>
    $(".taskOption").parents(".panel-heading").addClass("has-task-option");
    $(".headnote").parents(".panel-heading").addClass("has-head-note");
</script>
<div class="theNotePanel">
<small class="dev-only php">tab_notes.php</small>
    <div class="flex-row mainbox-top">
        <div class="col-half tab-panel">
            <div class="panel panel-primary callNotes PreviousQuickNotes">
                <div class="panel-heading otherNotes">
                    <h3 class="panel-title NoteName"><div><i class="fa fa-sticky-note"></i> <?php echo $this->lang->line('text_contact_notes');?></div><small class="headnote"><em>(all times currently shown in <?php echo $session_data['TimeZone']; ?> timezone)</em></small></h3><!--<span class="updateStat"><i class="fa fa-refresh"></i></span>-->
                </div>
                <div class="panel-body prev-notes Lazy-prev-notes">
                    <div class="btn-group note-loader">
                        <button type="button" class="nb-btn nb-secondary icon-btn" onclick='lazy_load("prev-notes","Contact Notes","loadNote","core/tabs/note","note-loader");'><i class="fa fa-list"></i> Load Contact Notes</button>
                    </div>
                    <?php
                    //echo mb_convert_encoding($PrevNotes, "ISO-8859-1", "UTF-8");
                    ?>
                </div>
            </div>

        </div>
        <div class="col-half tab-panel">
            <div class="panel panel-primary CurrentPersonalNotes callNotes">
                <div class="panel-heading otherNotes">
                    <h3 class="panel-title NoteName"><div><i class="fa fa-sticky-note-o"></i> <?php echo $this->lang->line('text_quick_note');?></div>
                        <div class="taskOption">Task? <div data-guid=""  class="toggleThisTaskNote toggle-iphone <?php echo !$this->config->item('TaskCustomFields') ? "disabled":""; ?>" ></div></div>
                    </h3>
                </div>
                <div class="panel-body filter-notes">
                    <div class="flex-row">
                        <div class="col-third TaskActionDescriptionContainer">
                            <h3 class="label-lg">Action Description:</h3>
                            <input name="TaskActionDescription" id="TaskActionDescription" class="form-control TaskActionDescription">
                        </div>
                        <div class="col-third ISUsersContainer">
                            <h3 class="label-lg">Assign To: (macanta user)</h3>
                            <select name="MacantaUser" id="MacantaUser" class="form-control  MacantaUser">
                                <?php
                                $Macanta_Users = macanta_get_users();
                                foreach ($Macanta_Users as $Users){
                                    $selected = $session_data['InfusionsoftID'] == $Users->Id ? "selected":'';
                                    echo '<option value="'.$Users->FirstName.' '.$Users->LastName.'" '.$selected.'>'.$Users->FirstName.' '.$Users->LastName.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-third TaskActionTypeContainer">
                            <h3 class="label-lg">Action Type:</h3>
                            <?php
                            $Types = infusionsoft_get_settings('ContactAction', 'optionstype');
                            $Types =  explode(',', $Types->message);
                            ?>
                            <select name="TaskActionType" id="TaskActionType" class="form-control  TaskActionType">
                                <option value="">Please select an action type</option>
                                <?php
                                foreach ($Types as $Type){
                                    echo "<option value='$Type'>$Type</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div><!-- /.flex-row -->
                    <div class="flex-row">
                        <div class="col-half TaskActionPriorityContainer">
                            <h3 class="label-lg">Priority:</h3>
                            <select name="TaskActionPriority" id="TaskActionPriority" class="form-control  TaskActionPriority">
                                <option value="1">1. Critical</option>
                                <option value="2">2. Essential</option>
                                <option value="3" selected>3. Non-Essential</option>
                            </select>
                        </div>
                        <div class="col-half TaskActionDateContainer">
                            <h3 class="label-lg">Action Date:</h3>
                            <input name="TaskActionDate" id="TaskActionDate" class="form-control TaskActionDate">
                        </div>
                    </div><!-- /.flex-row -->
                    <div class="flex-row note-title-type-container">
                        <div class="col-half NoteTitleContainer">
                            <h3 class="label-lg">Title:</h3>
                            <input name="NoteTitle" id="NoteTitle" class="form-control NoteTitle">
                        </div>
                        <div class="col-half NoteTypeContainer">
                            <h3 class="label-lg">Type:</h3>
                            <select name="NoteType" id="NoteType" class="form-control  NoteType">
                                <option value="">Please select an action type</option>
                                <?php
                                foreach ($Types as $Type){
                                    echo "<option value='$Type'>$Type</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div><!-- /.note-title-type-container -->
                    <div class="NoteDescriptionContainer">
                        <h3 class="label-lg">Description:</h3>
                        <textarea name="" id="personal-notes" class="personal-notes" placeholder="<?php echo $this->lang->line('text_type_your_note_here');?>"></textarea>
                        <script>
                            initTinymcePersonalNotes();
                        </script>
                    </div>
                    <input id='quick_notes_tags' type='text' class='quick_notes_tags' value='' />
                    <div class="note-action">
                        <div class="SaveQuickNoteContainer">
                            <button data-action="" class="nb-btn nb-primary icon-btn SaveQuickNote" disabled><i class="fa fa-sticky-note-o" aria-hidden="true"></i> Create Note </button>

                        </div>
                        <div class="SaveQuickTaskContainer">
                            <button data-action="" class="nb-btn nb-primary icon-btn SaveQuickTask"><i class="fa fa-tasks" aria-hidden="true"></i> Create Task </button>

                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-primary CurrentPersonalNotes callNotes search-and-filter">
                <div class="panel-heading otherNotes">
                    <h3 class="panel-title NoteName"><i class="fa fa-sticky-note-o"></i> <?php echo $this->lang->line('text_search_and_filter');?></h3>
                </div>
                <div class="panel-body filter-notes">
                    <div class="input-group">
                        <input type="text" class="form-control filterSearchKey " placeholder="<?php echo $this->lang->line('text_search_note_content');?>" style="height: 30px;">
                        <span class="input-group-btn">
                                <button class="btn btn-default filterNotes" type="button"><?php echo $this->lang->line('text_go');?>!</button>
                              </span>
                    </div>
                    <div class="flex-row">
                        <div class="col-third by-date">
                            <h3 class="label-lg"><?php echo $this->lang->line('text_by_date');?>:</h3>
                            <input id="note_daterange" rows="2" name="note_daterange">
                        </div><!-- /.by-date -->
                        <div class="col-third by-note">
                            <h3 class="label-lg"><?php echo $this->lang->line('text_by_note_tag');?>:</h3>
                            <input id='tags_filter' type='text' class='tags_filter' value='' />
                        </div><!-- /.by-note -->
                        <div class="col-third by-staff">
                            <h3 class="label-lg"><?php echo $this->lang->line('text_by_staff_member');?>:</h3>
                            <select class="selectpicker" multiple title="<?php echo $this->lang->line('text_choose_staff_member');?>">
                                <?php
                                foreach($StaffMembers as $StaffMember){
                                    $StaffMember->CompanyID = isset($StaffMember->CompanyID) ? $StaffMember->CompanyID : 0;
                                    if($StaffMember->CompanyID != $StaffMember->Id)
                                        echo "<option value='".$StaffMember->Id."'>".$StaffMember->FirstName." ".$StaffMember->LastName."</option>";
                                }
                                ?>
                            </select>
                        </div><!-- /.by-staff -->
                    </div><!-- /.flex-row -->

                </div>
            </div>


        </div>
    </div>
    <div class="row">
    </div>
</div>
<?php
$DisabledAddTaskDialog = "
<div id=\"dialog-confirm\" title=\"Oops.. Missing Custom Field!\">
    <p>In order to create a task inside macanta, please follow <a href=\"http://help.macanta.org/knowledge_base/topics/create-a-note-custom-field-in-infusionsoft\" target=\"_blank\">these 4 steps</a> to create a task/note custom field Tab and Header in Infusionsoft.</p>
    <br>
    <p>We can then create the relevant custom field. Thanks!</p>
</div>
";
?>
<script>
    var DisabledAddTaskDialog = <?php echo json_encode($DisabledAddTaskDialog); ?>;
    $('.toggleThisTaskNote').toggles({
        text: {
            on: 'Yes', // text for the ON position
            off: 'No' // and off
        },
        on: false, // is the toggle ON on init
        animate: 150, // animation time (ms)
        easing: 'easeOutQuint', // animation transition easing function
        width: 60, // width used if not set in css
        height: 25 // height if not set in css
    })
        .on('toggle', function(e, active) {
            var toggle = $(this);
            if (active) {
                $('div.TaskActionDateContainer').fadeIn('fast');
                $('div.TaskActionDescriptionContainer').fadeIn('fast');
                $('div.TaskActionPriorityContainer').fadeIn('fast');
                $('div.ISUsersContainer').fadeIn('fast');
                $('div.TaskActionTypeContainer').fadeIn('fast');
                $('div.NoteTitleContainer').fadeOut('fast');
                $('div.NoteTypeContainer').fadeOut('fast');
                $('.CurrentPersonalNotes div#quick_notes_tags_tagsinput').fadeOut('fast');
                $('div.SaveQuickNoteContainer').fadeOut('fast', function () {
                    $('div.SaveQuickTaskContainer').fadeIn('fast');
                });
                $('div.NoteDescriptionContainer h3').html('Creation Notes:');

            } else {
                $('div.NoteDescriptionContainer h3').html('Description:');
                $('div.TaskActionDateContainer').fadeOut('fast');
                $('div.TaskActionDescriptionContainer').fadeOut('fast');
                $('div.TaskActionPriorityContainer').fadeOut('fast');
                $('div.ISUsersContainer').fadeOut('fast');
                $('div.TaskActionTypeContainer').fadeOut('fast');
                $('.CurrentPersonalNotes div#quick_notes_tags_tagsinput').fadeIn('fast');
                $('div.NoteTitleContainer').fadeIn('fast');
                $('div.NoteTypeContainer').fadeIn('fast');
                $('div.SaveQuickTaskContainer').fadeOut('fast', function () {
                    $('div.SaveQuickNoteContainer').fadeIn('fast');
                });
            }
        });
    $("input.TaskActionDate").daterangepicker({
        autoUpdateInput: false,
        showDropdowns: true,
        timePicker:true,
        locale: {
            format: "YYYY-MM-DD HH:mm:ss"
        },
        singleDatePicker:true
    }).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format("YYYY-MM-DD HH:mm:ss"));
        $("#TaskActionDate").removeClass('error');
    });
    $(document)
        ._once('click',"div.toggleThisTaskNote.disabled", function () {
        $(DisabledAddTaskDialog).appendTo("body");
        $( "#dialog-confirm" ).dialog({
            resizable: false,
            height: "auto",
            width: 500,
            modal: true,
            buttons: {
                Ok: function() {
                    $( this ).dialog( "close" );
                    $( "#dialog-confirm" ).remove();
                }
            }
        });


    });
</script>