<div class="CustomTabPanel" data-contentid="<?php echo $contentid; ?>" data-title="<?php echo $title; ?>">
    <div class="mainbox-top">
        <div class="panel panel-primary theCustomTabPanel theCustomTabPanel-<?php echo $contentid; ?>">
            <div class="panel-heading ">
                <h3 class="panel-title"><i class="fa fa-newspaper-o"></i> <?php echo $title;?></h3>
                <?php
                if($title == 'FileBox'){
                    echo '<button type="button" data-contentid="'.$contentid.'" class="nb-btn nb-secondary icon-btn refreshFileBox"><i class="fa fa-refresh"></i> Refresh </button>';
                }
                ?>
            </div>
            <div class="panel-body theCustomTabPanelBody  Lazy-<?php echo $contentid; ?> <?php echo $GlobalClass;?>">
                <script>
                    //setTimeout(function(){ lazy_load("<?php echo $contentid; ?>","<?php echo $title; ?>","tab_custom"); }, 1000);
                </script>
            </div>
        </div>
    </div>
</div>
<?php
if($title == 'FileBox'){ ?>
    <script>
        $(function () {
            $(document)
                .on('click', 'button.refreshFileBox', function () {
                    var jsonData = {
                        "controler": "core/tabs/admin",
                        "action": "refreshFileBox",
                        "session_name": session_name,
                        "data": {}
                    };
                    var successFn = function (e) {
                        lazy_load("<?php echo $contentid; ?>","<?php echo $title; ?>","tab_custom");
                        if (typeof e === 'object') {
                            console.log('Refresh FileBox');
                            eval(e.script);
                        }
                    }
                    ajaxRequester('theCustomTabPanel-<?php echo $contentid; ?>', 'theCustomTabPanel-<?php echo $contentid; ?> .theCustomTabPanelBody', jsonData, successFn);
                })
        });

    </script>
<?php }
?>