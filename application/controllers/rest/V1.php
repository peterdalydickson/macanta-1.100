<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/InfusionsoftHooks.php';
//require APPPATH . '/libraries/Format.php';
//require BASEPATH . '/libraries/Session/Session.php';
/*if ( ! class_exists('CI_Session'))
{
    load_class('Session', 'libraries/Session');
}*/
class V1 extends REST_Controller {

    private $access_key;
    public function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->access_key = $this->config->item('macanta_api_key');
        $this->load->dbforge();
    }
    public function index_get(){
        $data = $this->get();
        $Response = [];
        if(!isset($data['access_key']))
            $this->response('Forbidden', REST_Controller::HTTP_FORBIDDEN);
        if($data['access_key'] != $this->access_key)
            $this->response('Invalid Access Key', REST_Controller::HTTP_FORBIDDEN);
        unset($data['access_key']);
        $Response = macanta_get_connected_info_group_fields_map("","",false);
        $this->response($Response, REST_Controller::HTTP_OK);
    }
    public function contact_post(){
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        $TablePrefix = 'Infusionsoft';
        $PostData = $this->post();
        $api_key = $PostData['api_key'];
        $api_key = isset($PostData['access_key']) ? $PostData['access_key']:$api_key;
        if($this->checkAPIkey($api_key) == false)  $this->response('Forbidden: Missing or Invalid API KEY', REST_Controller::HTTP_FORBIDDEN);

        unset($PostData['api_key']);
        unset($PostData['access_key']);

        if(!isset($PostData['Email']) || !isset($PostData['FirstName'])){
            $this->response("Error: Required Contact Parameter Missing (Email and FirstName)", REST_Controller::HTTP_OK);
        }

        $RegularField = ['Address1Type', 'Address2Street1', 'Address2Street2', 'Address2Type', 'Address3Street1', 'Address3Street2', 'Address3Type',
            'AssistantName', 'AssistantPhone', 'Anniversary', 'BillingInformation', 'City', 'City2', 'City3', 'Company', 'CompanyID', 'ContactNotes', 'ContactType', 'Country',
            'Country2', 'Country3', 'Email', 'EmailAddress2', 'EmailAddress3', 'Fax1', 'Fax1Type', 'Fax2', 'Fax2Type', 'FirstName', 'JobTitle', 'Language', 'LastName', 'Leadsource', 'MiddleName', 'Nickname',
            'Password', 'Phone1', 'Phone1Ext', 'Phone1Type', 'Phone2', 'Phone2Ext', 'Phone2Type', 'Phone3', 'Phone3Ext', 'Phone3Type', 'Phone4', 'Phone4Ext',
            'Phone4Type', 'Phone5', 'Phone5Ext', 'Phone5Type', 'PostalCode', 'PostalCode2', 'PostalCode3', 'ReferralCode', 'SpouseName', 'State', 'State2', 'State3',
            'StreetAddress1', 'StreetAddress2', 'Suffix', 'TimeZone', 'Title', 'Username', 'Validated', 'Website', 'ZipFour1', 'ZipFour2', 'ZipFour3', 'Birthday'
        ];
        $DBData = [];

        foreach ($RegularField as $FieldName){
            if(isset($PostData[$FieldName]) && !empty($PostData[$FieldName])){
                $DBData[$FieldName] = html_entity_decode($PostData[$FieldName]);
            }
        }

        $Existing = macanta_db_record_exist('Email',$DBData['Email'],$TablePrefix.'Contact', true);
        if($Existing){
            $DBData['LastUpdated'] = '{"date":"'.date("Y-m-d H:i:s").'","timezone_type":3,"timezone":"UTC"}';
            $this->db->where('Email', $DBData['Email']);
            $result = $this->db->update($TablePrefix.'Contact', $DBData);
            $SQLLOG = $this->db->last_query();
            $Response = ['action' => 'update','status' => $result, 'contactId' => $Existing->Id];
        }else{
            $DBData['DateCreated'] = '{"date":"'.date("Y-m-d H:i:s").'","timezone_type":3,"timezone":"UTC"}';
            $DBData['Id'] = $DBData['IdLocal'] = $this->get_last_id($TablePrefix.'Contact')+3;
            $result = $this->db->insert($TablePrefix.'Contact', $DBData);
            $SQLLOG = $this->db->last_query();
            $Response = ['action' => 'insert','status' => $result, 'contactId' => $DBData['Id']];
        }
        $this->response($Response, REST_Controller::HTTP_OK);
    }
    public function get_last_id($Table, $orderBy='Id'){
        $last_row=$this->db->select($orderBy)->order_by($orderBy,"DESC")->limit(1)->get($Table)->row();
        return isset($last_row) ? $last_row->$orderBy:'0';
    }
    public function receive_hook_post(){
        $CI = &get_instance();
        $ExcludedApps = ['cs375','dr453','yl682'];
        if(in_array($CI->config->item('MacantaAppName'),$ExcludedApps))  $this->response("Skipped", REST_Controller::HTTP_OK);
        $headers = apache_request_headers();
        $data = $this->post();
        $response = ['headers' => $headers, 'data' => $data];

        //check if verification: verify hook if verification_key is present
        if(isset($data['verification_key'])){
            header('X-Hook-Secret: '.$data['verification_key']);
            macanta_logger('RegisterHook',json_encode($response));
            //file_put_contents(dirname(__FILE__).'/register_hook.txt', "POST: ".json_encode($response)."\n\n");
            $this->response($data, REST_Controller::HTTP_OK);
        }

        //check if this is the actual rest hook
        if (isset($data['event_key']) && isset($data['object_keys'])){
            $this->create_resthook_table();


            $message = json_encode($data);
            /*Store Messages*/
            $DBData = [
                "message" => $message,
                "date_received" => date("Y-m-d H:i:s"),
                "status" => "ready"
            ];
            $this->db->insert('RestHookRequest', $DBData);
            $this->response("InfusionsoftHook Listed", REST_Controller::HTTP_OK);

            /*$CI = &get_instance();
            $HookAction = str_replace('.','_',$data['event_key']);
            $InfusionsoftHooks = new InfusionsoftHooks($CI);
            if(method_exists($InfusionsoftHooks, $HookAction)){
                $return = $InfusionsoftHooks->$HookAction($data);
                macanta_logger($data['event_key'],json_encode($return));
                //file_put_contents(dirname(__FILE__).'/'.$data['event_key'].'.txt', json_encode($return)."\n\n");
                $this->response($return, REST_Controller::HTTP_OK);
            }else{
                macanta_logger($data['event_key'],'No Method Existing-> '.$HookAction ." in InfusionsoftHooks");
                //file_put_contents(dirname(__FILE__).'/'.$data['event_key'].'.txt', 'No Method Existing-> '.$HookAction ." in InfusionsoftHooks"."\n\n", FILE_APPEND);
                $this->response('No Method Existing-> '.$HookAction ." in InfusionsoftHooks", REST_Controller::HTTP_OK);
            }*/
        }
    }
    public function process_resthook_exe_get(){
        $PID = $this->get_pids('rest/v1/process_resthook');
        if($PID){
            $this->response('Terminated! Existing Process Detected.. ', REST_Controller::HTTP_OK);
        }
        $output = '';
        while (true) {
            $exec_string = '/usr/bin/php /var/www/macanta/shared/services/_cli.php rest/process_resthook';
            exec($exec_string, $output);
            sleep(5);
        }
    }
    public function process_automation_get(){
        //set_time_limit(180);
        $return['triggers'] = macanta_execute_valid_triggers();
        $return['automation'] .= macanta_check_execute_automation_with_wait();
        echo json_encode($return)."\n=================================================\n";
    }
    public function process_resthook_get(){
        //ini_set('display_errors', 1);
        //error_reporting(E_ALL);
        set_time_limit(180);
        $query = $this->db->get('RestHookRequest');
        if (sizeof($query->result()) > 0){
            $CI = &get_instance();
            $InfusionsoftHooks = new InfusionsoftHooks($CI);
            foreach ($query->result() as $row) {
                $data = json_decode($row->message,true);
                $HookAction = str_replace('.','_',$data['event_key']);
                if(method_exists($InfusionsoftHooks, $HookAction)){
                    if($CI->config->item('MacantaAppName') == 'al347' && $HookAction == 'contact_delete'){
                        $return = 'Skipped';

                    }elseif($CI->config->item('MacantaAppName') != 'dw379' && $HookAction == 'opportunity_stage_move'){
                        $return = 'Skipped';
                    }else{
                        if($HookAction == 'contactGroup_applied' || $HookAction == 'contactGroup_removed'){
                            //$return = 'Skipped'; // this is covered with contact_edit
                            $return = $InfusionsoftHooks->$HookAction($data);
                        }else{
                            $return = $InfusionsoftHooks->$HookAction($data);
                        }

                    }
                    macanta_logger($data['event_key'],json_encode($return));
                    //file_put_contents(dirname(__FILE__).'/'.$data['event_key'].'.txt', json_encode($return)."\n\n");
                    // delete this item
                    $this->db->where('id',$row->id);
                    $this->db->delete('RestHookRequest');
                    if($HookAction == 'contactGroup_add' || $HookAction == 'contactGroup_edit' || $HookAction == 'contactGroup_delete'){
                        infusionsoft_get_tag_categories_menu(true);
                        infusionsoft_refresh_get_tags_by_catId();
                    }
                    if($HookAction == 'contactGroup_applied' || $HookAction == 'contactGroup_removed'){
                        $exec_string = '/usr/bin/php /var/www/macanta/shared/services/sync.php '.$CI->config->item('MacantaAppName').' 4 > /dev/null & ';
                        exec($exec_string);
                    }

                    //$this->response($return, REST_Controller::HTTP_OK);
                }else{
                    macanta_logger($data['event_key'],'No Method Existing-> '.$HookAction ." in InfusionsoftHooks");
                    //file_put_contents(dirname(__FILE__).'/'.$data['event_key'].'.txt', 'No Method Existing-> '.$HookAction ." in InfusionsoftHooks"."\n\n", FILE_APPEND);
                    // delete this item
                    $this->db->where('id',$row->id);
                    $this->db->delete('RestHookRequest');
                    //$this->response('No Method Existing-> '.$HookAction ." in InfusionsoftHooks", REST_Controller::HTTP_OK);
                }

            }
        }
        exit;
    }
    public function connected_data_get(){
        $data = $this->get();
        $Response = [];
        $this->access_key = $this->config->item('macanta_api_key');
        if(!isset($data['access_key']))
            $this->response('Forbidden', REST_Controller::HTTP_FORBIDDEN);
        if($data['access_key'] != $this->access_key)
            $this->response('Invalid Access Key', REST_Controller::HTTP_OK);
        unset($data['access_key']);

        $QueryParam = [];
        $QueryParam['Limit'] = isset($data['limit']) ? $data['limit']:false;
        $QueryParam['Offset'] = isset($data['offset']) ? $data['offset']:false;

        $QueryParam['Group'] = isset($data['group']) ? urldecode($data['group']):false;
        $QueryParam['Group'] = isset($data['type']) ? urldecode($data['type']):false;
        $QueryParam['GroupId'] = isset($data['group_id']) ? $data['group_id']:false;
        $QueryParam['Field'] = isset($data['field']) ? urldecode($data['field']):false;
        $QueryParam['Value'] = isset($data['value']) ? urldecode($data['value']):false;
        $QueryParam['ContactId'] = isset($data['contactId']) ? $data['contactId']:"";
        if($QueryParam['Group'] == false && $QueryParam['GroupId'] == false){
            $Response = macanta_get_connected_info_group_fields_map("","",false);
            $this->response($Response, REST_Controller::HTTP_OK);
        }

        $ConnectedData = macanta_api_get_connected_info($QueryParam);


        if(is_array($ConnectedData['message'])){
            $Response['connected_data'] = $ConnectedData['message'];
            $Response['count'] = sizeof($ConnectedData['message']);
            $Response['next'] = '';
            $Response['previous'] = '';
        }else{
            $Response['message'] = $ConnectedData['message'];
        }

        $this->response($Response, REST_Controller::HTTP_OK);

    }
    public function connected_contact_get(){
        $data = $this->get();
        $this->access_key = $this->config->item('macanta_api_key');
        if(!isset($data['access_key']))
            $this->response('Forbidden', REST_Controller::HTTP_FORBIDDEN);
        if($data['access_key'] != $this->access_key)
            $this->response('Invalid Access Key', REST_Controller::HTTP_FORBIDDEN);
        unset($data['access_key']);

        if(isset($data['ContactId'])){
            $Response = macanta_get_connected_info_by_groupname($data['GroupName'], $data['ContactId']);
            $this->response($Response, REST_Controller::HTTP_OK);
        }else{
            $this->response('ContactId Required', REST_Controller::HTTP_OK);
        }

    }
    public function add_post(){
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        $PostData = $this->post();
        $data = [];
        $api_key = $PostData['api_key'];
        $api_key = isset($PostData['access_key']) ? $PostData['access_key']:$api_key;
        if(
            !isset($api_key) ||
            !isset($PostData['contactId']) ||
            !isset($PostData['connected_group'])
        ) $this->response('Forbidden: Missing Required Field - '. json_encode($PostData), REST_Controller::HTTP_FORBIDDEN);
        $connected_relationship = $PostData['connected_relationship'];
        $connected_relationship = isset($PostData['relationship']) ?  trim($PostData['relationship']):$connected_relationship;

        if(empty($connected_relationship)) $this->response('Forbidden: Missing Required Field - '. json_encode($PostData), REST_Controller::HTTP_FORBIDDEN);

        if($this->checkAPIkey($api_key) == false)  $this->response('Forbidden: Missing or Invalid API KEY', REST_Controller::HTTP_FORBIDDEN);

        $connected_groups = strtolower($PostData['connected_group']);
        $contactId = $PostData['contactId'];

        //Unset non-macanta fields parameters
        unset($PostData['api_key']);
        unset($PostData['access_key']);
        unset($PostData['connected_group']);
        unset($PostData['contactId']);
        unset($PostData['cd_guid']);
        unset($PostData['connecteddata_id']);
        unset($PostData['connected_relationship']);
        unset($PostData['relationship']);
        $connected_contacts = [$contactId=>$connected_relationship];
        foreach ($PostData as $FieldName => $FieldValue){
            $data[$FieldName] = $FieldValue;
        }

        $connected_groupsArr = explode(" or ",$connected_groups);
        $Added = [];
        foreach ($connected_groupsArr as $connected_group){
            $group_details = macanta_get_connected_info_group_fields_map($connected_group);
            $group_id = isset($group_details['id']) ? $group_details['id']:false;
            if($group_id === false) $this->response($PostData['connected_group']." group does not exist!.", REST_Controller::HTTP_OK);;

            $values = array_change_key_case($data,CASE_LOWER);
            $Added[$connected_group] = macanta_add_connected_data($group_id, $group_details, $values, $connected_contacts, false,'');

        }
        $this->response($Added, REST_Controller::HTTP_OK);
    }
    public function edit_post(){
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        $PostData = $this->post();
        $data = [];
        $api_key = $PostData['api_key'];
        $api_key = isset($PostData['access_key']) ? $PostData['access_key']:$api_key;
        if(
            !isset($api_key) ||
            !isset($PostData['contactId']) ||
            !isset($PostData['connected_group'])
        ) $this->response('Forbidden: Missing Required Field - '. json_encode($PostData), REST_Controller::HTTP_FORBIDDEN);
        $connected_relationship = $PostData['connected_relationship'];
        $connected_relationship = isset($PostData['relationship']) ?  trim($PostData['relationship']):$connected_relationship;
        if(empty($connected_relationship)) $this->response('Forbidden: Missing Required Field - '. json_encode($PostData), REST_Controller::HTTP_FORBIDDEN);
        if( !isset($PostData['cd_guid']) && !isset($PostData['connecteddata_id']) )
            $this->response('Forbidden: Missing Required Field - cd_guid | connecteddata_id', REST_Controller::HTTP_FORBIDDEN);


        if($this->checkAPIkey($api_key) == false)  $this->response('Forbidden: Missing or Invalid API KEY', REST_Controller::HTTP_FORBIDDEN);

        $connected_groups = $PostData['connected_group'];
        $contactId = $PostData['contactId'];
        $cd_guid = isset($PostData['cd_guid']) ? $PostData['cd_guid']:'';
        $duplicate_option = isset($PostData['connecteddata_id']) ? $PostData['connecteddata_id']:false;

        $connected_contacts = [$contactId=>$connected_relationship];

        //Unset non-macanta fields parameters
        unset($PostData['api_key']);
        unset($PostData['access_key']);
        unset($PostData['connected_group']);
        unset($PostData['contactId']);
        unset($PostData['cd_guid']);
        unset($PostData['connecteddata_id']);
        unset($PostData['connected_relationship']);
        unset($PostData['relationship']);
        foreach ($PostData as $FieldName => $FieldValue){
            $data[$FieldName] = $FieldValue;
        }

        $connected_groupsArr = explode(" or ",$connected_groups);
        $Edited = [];
        foreach ($connected_groupsArr as $connected_group){
            $FieldValue = false;
            $FieldId = false;
            $group_details = macanta_get_connected_info_group_fields_map($connected_group);
            $group_id = isset($group_details['id']) ? $group_details['id']:false;
            if($group_id === false){
                $Edited[$connected_group] = $PostData['connected_group']." group does not exist!.";
                continue;
            }
            if($duplicate_option){
                if(isset($data[$duplicate_option])){
                    foreach ($group_details['fields'] as $field_id => $field_details ){
                        if(strtolower($duplicate_option) == $field_details['title']){
                            $FieldValue = $data[$duplicate_option];
                            $FieldId = $field_id;
                            break;
                        }
                    }
                }else{
                    $Edited[$connected_group] = "ERROR: connecteddata_id does not have any given value in the POST.";
                    continue;
                }
            }
            $Edited[$connected_group] = macanta_update_connected_data($contactId, $group_id, $data,$connected_contacts,false, $FieldId, $FieldValue, true,'',$cd_guid);
            if ($Edited[$connected_group] == false) {
                // Add Connected Data
                $Edited[$connected_group] = macanta_add_connected_data($group_id, $group_details, $data, $connected_contacts, false,'');
            }
        }

        $this->response($Edited, REST_Controller::HTTP_OK);
    }
    public function connected_data_attach_file_post(){
        ini_set('max_execution_time', 3600);
        $post_data = $this->post();
        $Debug = [];
        $this->access_key = $this->config->item('macanta_api_key');
        if(!isset($_GET['access_key']))
            $this->response('Forbidden', REST_Controller::HTTP_FORBIDDEN);
        if($_GET['access_key'] != $this->access_key)
            $this->response('Invalid Access Key', REST_Controller::HTTP_FORBIDDEN);
        if(isset($post_data['ItemId']) && isset($post_data['FileURL'])){
            $itemId = $post_data['ItemId'];
            $theFileURL = $post_data['FileURL'];
            //$theFileData = file_get_contents($theFileURL);
            $path_parts = pathinfo($theFileURL);
            if(empty($path_parts['extension'])){
                $content = get_headers($theFileURL,1);
                $content = array_change_key_case($content, CASE_LOWER);
                if ($content['content-disposition']) {
                    $tmp_name = explode('=', $content['content-disposition']);
                    if ($tmp_name[1]) {
                        $realfilename = trim($tmp_name[1],'";\'');
                        $path_parts = pathinfo($realfilename);
                    }
                }
            }
            $opts = array(
                'http'=>array(
                    'method'=>"GET",
                    'header'=>"Accept-language: en\r\n" .
                        "Access-Control-Allow-Origin: *\r\n".
                        "Cookie: foo=bar\r\n"
                )
            );

            $context = stream_context_create($opts);
            $file_contents = file_get_contents($theFileURL, false, $context);
            $db_record = [];
            $db_insert = '';
            if($file_contents != false){
                $filesize = strlen($file_contents);
                $thumb_info = createThumbnail($theFileURL, $itemId,false, $file_contents );
                $friendly_filename = empty($path_parts['extension']) ? $thumb_info['ThumbFileName']:$path_parts['filename'].".".$path_parts['extension'];

                $DBdata = [
                    'item_id' => $itemId,
                    'filename' => $friendly_filename,
                    'thumbnail' => $thumb_info['data'],
                    'download_url' => $theFileURL,
                    'file_size' => $filesize,
                    'timestamp' => time(),
                    'b2_filename' => '',
                    'b2_file_id' => '',
                    'b2_timestamp' => '',
                    'meta' => "{}"
                ];
                $db_insert = $this->db->insert('connected_data_file_attachment', $DBdata);


                $this->db->where('item_id',$itemId);
                $query = $this->db->get('connected_data_file_attachment');
                if (sizeof($query->result()) > 0){
                    foreach ($query->result() as $row) {
                        $db_record[$itemId][]=$row;
                    }
                }
                $Debug['path_parts'] = $path_parts;
                $Debug['DBdata'] = $DBdata;
                $Debug['status'] = 'success';
                $Debug['message'] = 'File Successfully Attached';
            }
            else{
                $Debug['status'] = 'failed';
                $Debug['message'] = 'Invalid/Not Found File URL';
            }

            //echo $this->db->last_query();
            $Debug['db_record'] = $db_record;

            $Debug['db_insert'] = $db_insert;
            $this->response($Debug, REST_Controller::HTTP_OK);
        }else{
            $this->response("Oops Missing FileURL and ItemId Parameters", REST_Controller::HTTP_OK);
        }
    }
    public function connected_data_add(){
        // URL: https://qj311.macanta.org/rest/v1/connected_data/?access_key=1492505713264569
        ini_set('max_execution_time', 3600);
        $post_data = $this->post();
        $Debug = [];
        $this->access_key = $this->config->item('macanta_api_key');
        if(!isset($_GET['access_key']))
            $this->response('Forbidden', REST_Controller::HTTP_FORBIDDEN);
        if($_GET['access_key'] != $this->access_key)
            $this->response('Invalid Access Key', REST_Controller::HTTP_FORBIDDEN);

        if(isset($post_data['group_id']) || isset($post_data['group_name'])){
            //Perform  Add/Updating Bulk Connected Data
            $group_id = isset($post_data['group_id']) ? $post_data['group_id']:false;
            $group_name = isset($post_data['group_name']) ? $post_data['group_name']:false;
            $duplicate_option = isset($post_data['duplicate_option']) ? $post_data['duplicate_option']:false;
            foreach ($post_data['items'] as $item){
                $data = isset($item['data']) ? $item['data']:[];
                $connected_contacts = isset($item['connected_contacts']) ? $item['connected_contacts']:[];

                $Debug[] = $this->add_update_connected_data($group_id,$group_name, $data,$connected_contacts, $duplicate_option);
            }
        }

        $this->response(["Response"=>"Ok", 'Debug'=>$Debug], REST_Controller::HTTP_OK);


        // For Add/Updating Bulk Connected Data
        /*{"group_name":"Children","duplicate_option":"Firstname","items":[{"data":{"Firstname":"Laira","Lastname":"Ortega3","Date of Birth":"2012-02-03"},"connected_contacts":{"364":"Tito","462":"Tita"}}]}*/
        $SamplePostContent = [
            '{group_id|group_name}'=>'string',
            'duplicate_option'=>'string',// Optional: Default to ''. Value can be `Email` etc..
            'items' =>[
                [
                    'data'=>['{field_name1}'=>'string','{field_name2}'=>'string','{field_name3}'=>'string'],
                    'connected_contacts' =>[ '{contact_id1}' => '{relation_name}', '{contact_id2}' => '{relation_name}']
                ],
                [
                    'data'=>['{field_name1}'=>'string','{field_name2}'=>'string','{field_name3}'=>'string'],
                    'connected_contacts' =>[ '{contact_id1}' => '{relation_name}', '{contact_id2}' => '{relation_name}' ]
                ]

            ]
        ];
        /*
        {
          "{group_id|group_name}": "string",
          "duplicate_option": "string",
          "items": [
            {
              "data": {
                "{field_name1}": "string",
                "{field_name2}": "string",
                "{field_name3}": "string"
              },
              "connected_contacts": {
                "{contact_id1}": "{relation_name}",
                "{contact_id2}": "{relation_name}"
              }
            },
            {
              "data": {
                "{field_name1}": "string",
                "{field_name2}": "string",
                "{field_name3}": "string"
              },
              "connected_contacts": {
                "{contact_id1}": "{relation_name}",
                "{contact_id2}": "{relation_name}"
              }
            }
          ]
        }
        */

        // For Updating Particular Item
        $SamplePostContent = [
            '{item_id}'=>'string',
            'data'=>['{field_name1}'=>'string','{field_name2}'=>'string','{field_name3}'=>'string'],
            'connected_contacts' =>[ '{contact_id1}' => '{relation_name}', '{contact_id2}' => '{relation_name}']
        ];
    }
    public function connected_data_post(){
        // URL: https://qj311.macanta.org/rest/v1/connected_data/?access_key=1492505713264569
        ini_set('max_execution_time', 3600);
        $post_data = $this->post();
        $Debug = [];
        $this->access_key = $this->config->item('macanta_api_key');
        if(!isset($_GET['access_key']))
            $this->response('Forbidden', REST_Controller::HTTP_FORBIDDEN);
        if($_GET['access_key'] != $this->access_key)
            $this->response('Invalid Access Key', REST_Controller::HTTP_FORBIDDEN);

        if(isset($post_data['item_id'])){
            // For Updating Particular Item

            $SamplePostContent = [
                'item_id'=>'{string}',
                'value'=>['{field_id1}'=>'string','{field_id2}'=>'string','{field_id3}'=>'string'],

                'connected_contacts' =>[ '{contact_id1}' => '{relation_name}', '{contact_id2}' => '{relation_name}'],
                'meta' => ['availed_offer'=>["KidsDXB"=>['golf']]]
            ];
            $item_id = $post_data['item_id'];
            $value = isset($post_data['value']) ? $post_data['value']:false;
            $connected_contacts = isset($post_data['connected_contacts']) ? $post_data['connected_contacts']:false;
            $meta = isset($post_data['meta']) ? $post_data['meta']:false;
            $Debug[] = $this->update_connected_data($item_id,$value,$connected_contacts, $meta);

            $this->response(["Response"=>"Ok", 'Debug'=>$Debug], REST_Controller::HTTP_OK);
        }


        if(isset($post_data['group_id']) || isset($post_data['group_name'])){
            //Perform  Add/Updating Bulk Connected Data
            // For Add/Updating Bulk Connected Data
            /*{"group_name":"Children","duplicate_option":"Firstname","items":[{"data":{"Firstname":"Laira","Lastname":"Ortega3","Date of Birth":"2012-02-03"},"connected_contacts":{"364":"Tito","462":"Tita"}}]}*/
            $SamplePostContent = [
                'group_name'=>'Children',
                'duplicate_option'=>'AccountId',// Optional: Default to ''. Value can be `Email` etc..
                'items' =>[
                    [
                        'data'=>['AccountId'=>'12345abcd','FirstName'=>'Sam','LastName'=>'Smith']
                    ],
                    [
                        'data'=>['AccountId'=>'54321abcd','FirstName'=>'Michaela','LastName'=>'Wood']
                    ]

                ]
            ];
            /*
            {
              "{group_id|group_name}": "string",
              "duplicate_option": "string",
              "items": [
                {
                  "data": {
                    "{field_name1}": "string",
                    "{field_name2}": "string",
                    "{field_name3}": "string"
                  },
                  "connected_contacts": {
                    "{contact_id1}": "{relation_name}",
                    "{contact_id2}": "{relation_name}"
                  }
                },
                {
                  "data": {
                    "{field_name1}": "string",
                    "{field_name2}": "string",
                    "{field_name3}": "string"
                  },
                  "connected_contacts": {
                    "{contact_id1}": "{relation_name}",
                    "{contact_id2}": "{relation_name}"
                  }
                }
              ]
            }
            */
            $group_id = isset($post_data['group_id']) ? $post_data['group_id']:false;
            $group_name = isset($post_data['group_name']) ? $post_data['group_name']:false;
            $duplicate_option = isset($post_data['duplicate_option']) ? $post_data['duplicate_option']:false;
            $duplicate_option = $duplicate_option == "" ? false:$duplicate_option;
            foreach ($post_data['items'] as $item){
                $data = isset($item['data']) ? $item['data']:[];
                $connected_contacts = isset($item['connected_contacts']) ? $item['connected_contacts']:[];
                $meta = isset($item['meta']) ? $item['meta']:false;
                $Debug[] = macanta_add_update_connected_data(false, $group_id, $group_name, $data, $connected_contacts,$meta, $duplicate_option);
                //$Debug[] = $this->add_update_connected_data($group_id,$group_name, $data,$connected_contacts, $duplicate_option);
            }
            $this->response(["Response"=>"Ok", 'Debug'=>$Debug], REST_Controller::HTTP_OK);
        }
        $this->response(["Response"=>"Oops Missing Group ID | Group Name | Item Id", 'Debug'=>$Debug], REST_Controller::HTTP_OK);

    }
    public function update_array_value($data, $value, &$modification, &$modified, $parent=''){
        $NewValue = $value;
        foreach ($data as $propName => $propDetails){

            if(isset($value[$propName])){
                if($value[$propName] == $propDetails) continue;
                if($parent == ''){
                    $parent = $propName;
                }else{
                    $parent = $parent.'->'.$propName;
                }
                if(is_array($propDetails)){
                    // check if multi dimensional array
                    if(count($propDetails) != count($propDetails, 1)){
                        $NewValue[$propName] = $this->update_array_value($propDetails, $value[$propName],$modification,$modified,$parent);
                    }
                    // if not, just add it or merge to the array value
                    else{
                        $NewValue[$propName] = array_merge($NewValue[$propName], $propDetails);
                        $modification[$parent] = ['from'=>$value[$propName],'to'=>$NewValue[$propName]];
                        $modified = true;
                        $parent = '';
                    }
                }else{
                    $modification[$parent] = ['from'=>$value[$propName],'to'=>$propDetails];
                    $NewValue[$propName] = $propDetails;
                    $modified = true;
                    $parent = '';
                }
            }else{
                if($parent == ''){
                    $parent = $propName;
                }else{
                    $parent = $parent.'->'.$propName;
                }
                $modification[$parent] = ['from'=>$value[$propName],'to'=>$propDetails];
                $NewValue[$propName] = $propDetails;
                $modified = true;
                $parent = '';
            }
        }
        return $NewValue;
    }
    public function cd_record_history($Id, $Modification ,$Type='connected_data'){

        //Create Table If Not Exist
        $table = "connected_data_history";
        $fields = array(
            'id'        => array( 'type' => 'INT', 'constraint' => 9, 'auto_increment' => TRUE ),
            'item_id'   => array( 'type' => 'TEXT', 'null' => TRUE, ),
            'time'      => array( 'type' => 'TEXT', 'null' => TRUE, ),
            'update_by' => array( 'type' => 'TEXT', 'null' => TRUE, ),
            'type'      => array( 'type' => 'TEXT', 'null' => TRUE, ),
            'data'      => array( 'type' => 'TEXT', 'null' => TRUE, )
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->create_table($table, true);

        if(isset($_SESSION['InfusionsoftID'])){
            $LoggedInUser = json_decode($_SESSION['Details']);
            $updated_by = json_encode(["ContactId"=>$_SESSION['InfusionsoftID'],"Email"=>$_SESSION['Email'],"FirstName"=>$LoggedInUser->FirstName,"LastName"=>$LoggedInUser->LastName]);
        }else{
            $updated_by = 'System API';
        }
        $DBData = array(
            "item_id"=>$Id,
            "time"=>date("Y-m-d H:i:s"),
            "update_by" =>$updated_by,
            "type"=>$Type,
            "data"=>json_encode($Modification)
        );
        $this->db->insert($table, $DBData);
    }
    public function update_connected_data($item_id,$value=false,$connected_contacts=false, $meta=false){
        $DBData = [];
        $Results = '';
        $this->db->where('id',$item_id);
        $query = $this->db->get('connected_data');
        if (sizeof($query->result()) > 0){
            foreach ($query->result() as $row) {
                $history = json_decode($row->history, true);
                if($value && is_array($value)){
                    $Old = json_decode($row->value, true);
                    $modified = false;
                    $modification = [];
                    $New = $this->update_array_value($value, $Old, $modification, $modified);
                    if($New!=$Old && $modified === true) {
                        $DBData['value'] = json_encode($New);
                        $this->cd_record_history($item_id, $modification, 'connected_data');
                    }
                }
                if($connected_contacts && is_array($connected_contacts)){
                    $Old = json_decode($row->connected_contact, true);
                    $modified = false;
                    $modification = [];
                    $New = $this->update_array_value($connected_contacts, $Old, $modification, $modified);
                    if($New!=$Old && $modified === true) {
                        $DBData['connected_contact'] = json_encode($New);
                        $this->cd_record_history($item_id, $modification, 'connected_contact');
                    }

                }
                if($meta && is_array($meta)){
                    $Old = json_decode($row->meta, true);
                    $modified = false;
                    $modification = [];
                    $New = $this->update_array_value($meta, $Old, $modification, $modified);
                    if($New!=$Old && $modified === true) {
                        $DBData['meta'] = json_encode($New);
                        $this->cd_record_history($item_id, $modification, 'meta');
                    }
                }
            }

            if(sizeof($DBData) > 0){
                $this->db->where('id',$item_id);
                $Results = $this->db->update('connected_data', $DBData);
            }


        }
        return ["DBData"=>$DBData, "QueryResult"=>$Results];


    }
    public function add_update_connected_data($group_id,$group_name, $values,$connected_contacts, $duplicate_option){
        $Results = '';
        $AllItemIds = [];
        if($group_id == false){
            $group_details = macanta_get_connected_info_group_fields_map($group_name);
            $group_id = isset($group_details['id']) ? $group_details['id']:false;
        }else{
            $group_details = macanta_get_connected_info_group_fields_map('',$group_id);
        }

        if($group_id === false) return false;

        $relationships_map = macanta_get_connected_info_relationships_map();
        if(isset($group_details['message'])) return $group_details['message'];
        $data['values'] = [];
        $values = array_change_key_case($values,CASE_LOWER);

        if($duplicate_option !== false){

            $check_field_name = strtolower($duplicate_option);
            $check_field_id = '';
            $check_value = '';
            // Determine the field value to check
            if(isset($values[$check_field_name])) $check_value = $values[$check_field_name];
            // Determine the field id to check
            foreach ($group_details['fields'] as $field_id => $field_details ){

                if(strtolower($check_field_name) == $field_details['title']){
                    $check_field_id = $field_id;
                }
            }

            $this->db->where('group',$group_id);
            $this->db->like('value','"'.$check_field_id.'":"'.$check_value.'"');

            $query = $this->db->get('connected_data');
            if (sizeof($query->result()) > 0) {

                foreach ($query->result() as $row) {
                    $DBdata = [];
                    $history = json_decode($row->history, true);
                    $OldValuesArr = json_decode($row->value, true);
                    $OldContactArr = json_decode($row->connected_contact, true);
                    $ItemId = $row->id;
                    $AllItemIds[] = $ItemId;
                    foreach ($group_details['fields'] as $field_id => $field_details ){
                        foreach ($values as $title=>$value){
                            if($title == strtolower($field_details['title'])){
                                $OldValuesArr[$field_id] = $value;
                            }
                        }
                    }
                    //364":{"relationships":["re_5b92474e"],"ContactId":"364","FirstName":"Geover","LastName":"Zamora","Email":"geover@gmail.com"}
                    foreach ($connected_contacts as $contact_id=>$relation_name){
                        $Contact = infusionsoft_get_contact_by_id_simple($contact_id);
                        if(isset($Contact[0]->Email) || isset($Contact[0]->FirstName) || isset($Contact[0]->LastName)){
                            $relation_id_arr = [];
                            $relation_name_arr = explode(',', $relation_name);
                            foreach ($relation_name_arr as $relation){
                                $relationship_id = array_search(strtolower(trim($relation)),$relationships_map);
                                if($relationship_id !== false){
                                    $relation_id_arr[] = $relationship_id;
                                }else{
                                    //create relationship if not existing and return the id
                                    $NewId = macanta_create_relationship($relation,'',$group_id);
                                    $relation_id_arr[] = $NewId;
                                    $relationships_map[$NewId] = $relation;
                                }


                            }
                            $NewConnectedContactInfo = [
                                "relationships"=>$relation_id_arr,
                                "ContactId"=>$contact_id,
                                "FirstName"=>isset($Contact[0]->FirstName) ? $Contact[0]->FirstName:"-no first name-",
                                "LastName"=>isset($Contact[0]->LastName) ? $Contact[0]->LastName:"-no last name-",
                                "Email"=>isset($Contact[0]->Email) ? $Contact[0]->Email:"-no email name-"

                            ];
                            if(isset($OldContactArr[$contact_id])){
                                if($OldContactArr[$contact_id] != $NewConnectedContactInfo ){
                                    $From[$contact_id] = $OldContactArr[$contact_id];
                                    $To[$contact_id] = $NewConnectedContactInfo;
                                    $history["connection_history"][] = array(
                                        "date"=>date("Y-m-d H:i:s"),
                                        "from"=>$From,
                                        "to"=>$To
                                    );
                                    $DBdata['history'] = json_encode($history);
                                }
                                $OldContactArr[$contact_id]=$NewConnectedContactInfo;
                            }else{
                                $OldContactArr[$contact_id]=$NewConnectedContactInfo;
                            }

                        }

                    }
                    $DBdata['value'] = json_encode($OldValuesArr);
                    $DBdata['connected_contact'] = json_encode($OldContactArr);
                    $this->db->where('id',$ItemId);
                    $Results = $this->db->update('connected_data', $DBdata);

                }
            }
            else{
                //Add if not existing

                $DBData = [];
                $OldValuesArr = [];
                $OldContactArr = [];
                foreach ($group_details['fields'] as $field_id => $field_details ){
                    foreach ($values as $title=>$value){
                        if($title == strtolower($field_details['title'])){
                            $OldValuesArr[$field_id] = $value;
                        }
                    }
                }
                $history_to = [];
                foreach ($connected_contacts as $contact_id=>$relation_name){
                    $Contact = infusionsoft_get_contact_by_id_simple($contact_id);
                    if(isset($Contact[0]->Email) || isset($Contact[0]->FirstName) || isset($Contact[0]->LastName)){
                        $relation_id_arr = [];
                        $relation_name_arr = explode(',', $relation_name);
                        foreach ($relation_name_arr as $relation){
                            $relationship_id = array_search(strtolower(trim($relation)),$relationships_map);
                            if($relationship_id !== false){
                                $relation_id_arr[] = $relationship_id;
                            }else{
                                //create relationship if not existing and return the id
                                $NewId = macanta_create_relationship($relation,'',$group_id);
                                $relation_id_arr[] = $NewId;
                                $relationships_map[$NewId] = $relation;
                            }


                        }

                        $OldContactArr[$contact_id]=[
                            "relationships"=>$relation_id_arr,
                            "ContactId"=>$contact_id,
                            "FirstName"=>isset($Contact[0]->FirstName) ? $Contact[0]->FirstName:"-no first name-",
                            "LastName"=>isset($Contact[0]->LastName) ? $Contact[0]->LastName:"-no last name-",
                            "Email"=>isset($Contact[0]->Email) ? $Contact[0]->Email:"-no email name-"

                        ];
                        $history_to[$contact_id] = [
                            "Email"=>$Contact[0]->Email,
                            "FirstName"=>isset($Contact[0]->FirstName) ? $Contact[0]->FirstName:"-no first name-",
                            "LastName"=>isset($Contact[0]->LastName) ? $Contact[0]->LastName:"-no last name-",
                            "relationships"=>$relation_id_arr
                        ];

                    }

                }
                $DBData['id'] = macanta_generate_key('item_');

                $DBData['group'] = $group_id;
                $DBData['value'] = json_encode($OldValuesArr);
                $DBData['connected_contact'] = json_encode($OldContactArr);
                $DBData['history'] = '{"update_history":[],"connection_history":[{"date":"'.date('Y-m-d H:i:s').'","to":'.json_encode($history_to).'}]}';
                $DBData['meta'] = '{"editable":"yes","searchable":"yes","multiple_link":"yes"}';
                $DBData['created'] = date('Y-m-d H:i:s');
                $DBData['status'] = 'active';
                $DBResults = $this->db->insert('connected_data', $DBData);
            }
        }else{
            //Always Add if $duplicate_option is not set
            $DBData = [];
            $OldValuesArr = [];
            $OldContactArr = [];
            foreach ($group_details['fields'] as $field_id => $field_details ){
                foreach ($values as $title=>$value){
                    if($title == strtolower($field_details['title'])){
                        $OldValuesArr[$field_id] = $value;
                    }
                }
            }
            $history_to = [];
            foreach ($connected_contacts as $contact_id=>$relation_name){
                $Contact = infusionsoft_get_contact_by_id_simple((int) $contact_id);
                if(isset($Contact[0]->Email)){
                    $relation_id_arr = [];
                    $relation_name_arr = explode(',', $relation_name);
                    foreach ($relation_name_arr as $relation){
                        $relationship_id = array_search(strtolower(trim($relation)),$relationships_map);
                        if($relationship_id !== false){
                            $relation_id_arr[] = $relationship_id;
                        }else{
                            //create relationship if not existing and return the id
                            $NewId = macanta_create_relationship($relation,'',$group_id);
                            $relation_id_arr[] = $NewId;
                            $relationships_map[$NewId] = $relation;
                        }


                    }

                    $OldContactArr[$contact_id]=[
                        "relationships"=>$relation_id_arr,
                        "ContactId"=>$contact_id,
                        "FirstName"=>$Contact[0]->FirstName,
                        "LastName"=>$Contact[0]->LastName,
                        "Email"=>$Contact[0]->Email

                    ];
                    $history_to[$contact_id] = [
                        "Email"=>$Contact[0]->Email,
                        "FirstName"=>$Contact[0]->FirstName,
                        "LastName"=>$Contact[0]->LastName,
                        "relationships"=>$relation_id_arr
                    ];
                }

            }
            $DBData['id'] = macanta_generate_key('item_');
            $AllItemIds[] = $DBData['id'];
            $DBData['group'] = $group_id;
            $DBData['value'] = json_encode($OldValuesArr);
            $DBData['connected_contact'] = json_encode($OldContactArr);
            $DBData['history'] = '{"update_history":[],"connection_history":[{"date":"'.date('Y-m-d H:i:s').'","to":'.json_encode($history_to).'}]}';
            $DBData['meta'] = '{"editable":"yes","searchable":"yes","multiple_link":"yes"}';
            $DBData['created'] = date('Y-m-d H:i:s');
            $DBData['status'] = 'active';
            $DBResults = $this->db->insert('connected_data', $DBData);
        }
        $Results = ['DB'=>$DBResults, 'items_ids'=>$AllItemIds];


        return $Results;
    }
    public function checkAPIkey($Key){
        $password = $this->config->item('macanta_api_key');
        return  $Key == $password ? true:false;
    }
    public function create_resthook_table($Table = 'RestHookRequest'){
        /*Crate Table If not exist*/
        $fields = array(
            'message' => array(
                'type' => 'LONGTEXT',
                'null' => TRUE,
            ),
            'date_received' => array(
                'type' => 'DATETIME',
                'null' => TRUE,
            ),
            'status' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'default' => 'ready',
            ),
        );
        $this->dbforge->add_field("id");
        $this->dbforge->add_field($fields);
        $this->dbforge->create_table($Table, true);
    }
}
