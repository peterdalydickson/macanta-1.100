<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
header("Content-Type: text/plain");

define('uDBHOST', '10.138.104.32');
define('uDBUSER', 'macanta_root');
define('uDBPASS', 'hyacinth-illusion-turnpike-wardrobe-habitue-vinyl-prowl-plus-negus-fulness');


$MariaDB = new PDO('mysql:host='.uDBHOST.';', uDBUSER, uDBPASS);
$MariaDB->query("USE MacantaRecords;");

$param = getopt(null, ["date:"]);
$Yesterday = (empty($param)) ? date('Y-m-d', strtotime("-1 day")) : $param['date'];
// echo $Yesterday;
// die();
$Query = "SELECT DISTINCT x.AppName, DATE(x.LoginDate) as LoginDate  
        FROM macanta_user_track_records as x 
        JOIN (SELECT DISTINCT b.AppName, DATE(b.Login) FROM (SELECT DISTINCT c.AppName, DATE(c.LoginDate) as Login 
            FROM macanta_user_track_records as c
            WHERE DATE(c.LoginDate) LIKE '{$Yesterday}'
            ORDER BY c.AppName) as b
            GROUP BY b.AppName
            HAVING count(AppName) > 0) as a 
            ON a.AppName = x.AppName
        WHERE DATE(x.LoginDate)  LIKE '{$Yesterday}'
        ORDER BY x.AppName, DATE(x.LoginDate)";

$results = $MariaDB->prepare($Query);
$results->execute();

foreach ($results as $LoginRecord) {

    $userId = $LoginRecord['AppName'].".macanta.org";
    $timestamp = date('c', strtotime($LoginRecord['LoginDate']));

    // POST to https://api.segment.io/v1/identify
    $DataIdentifyPassed = [
        'timestamp' => $timestamp,
        'userId' => $userId
    ];
    $IdentifyPostDataJson = json_encode($DataIdentifyPassed);
    //$IdentifyPostData = http_build_query($DataIdentifyPassed);
    $IdentifyOpts = array(
        'http' => array(
            'method' => 'POST',
            'header' => "Accept-language: en\r\n" .
                /*"Content-type: application/x-www-form-urlencoded\r\n"*/
                "Authorization: Basic aml6Y0Y0QlM1U3BzRWlSNmpiNW4wVHZVc3dHNFlhMEs6\r\n".
                "Content-type: application/json\r\n"
        ,
            /*'content' => $IdentifyPostData*/
            'content' => $IdentifyPostDataJson
        )
    );
    $IdentifyContext  = stream_context_create($IdentifyOpts);
    // echo $IdentifyContext . "\n";
    echo $LoginRecord['AppName']."-".$LoginRecord['LoginDate']." Posting to Identify... \n";
    $IdentifyResults = file_get_contents('https://api.segment.io/v1/identify', false, $IdentifyContext);
    // echo "file_get_contents('https://api.segment.io/v1/identify', false, $IdentifyContext)\n\n";
    // echo "Done\n";
    /*======================================================================*/

    // sleep(1);

    // POST to https://api.segment.io/v1/track
    $DataTrackPassed = [
        'timestamp' => $timestamp,
        'userId' => $userId,
        "event"=> "Active App",
		"context" => ["groupId"=>$LoginRecord['AppName']]
    ];
    $TrackPostDataJson = json_encode($DataTrackPassed);
    //$TrackPostData = http_build_query($DataTrackPassed);
    $TrackOpts = array(
        'http' => array(
            'method' => 'POST',
            'header' => "Accept-language: en\r\n" .
                /*"Content-type: application/x-www-form-urlencoded\r\n"*/
                "Authorization: Basic aml6Y0Y0QlM1U3BzRWlSNmpiNW4wVHZVc3dHNFlhMEs6\r\n".
                "Content-type: application/json\r\n"
        ,
            /*'content' => $TrackPostData*/
            'content' => $TrackPostDataJson
        )
    );
    $TrackContext  = stream_context_create($TrackOpts);
    // echo $TrackContext . "\n";
    echo $LoginRecord['AppName']."-".$LoginRecord['LoginDate']." Posting to Track ... \n\n";
    $TrackResults = file_get_contents('https://api.segment.io/v1/track ', false, $TrackContext);
    // echo "file_get_contents('https://api.segment.io/v1/track ', false, $TrackContext)\n\n\n\n";
    // break;
    // echo "Done\n";
    /*======================================================================*/
}