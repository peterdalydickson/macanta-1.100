<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'macanta';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;

/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/
//$route['core/common/autologin/(:num)'] = 'autologin/id/$1'; // Example 4
$route['check'] = 'check';
$route['consumer/(:any)'] = 'daemon_amqp/consumer/$1';
$route['trigger/(:any)'] = 'daemon_amqp/sequence_trigger';
$route['sync/(:any)/(:any)'] = 'offline/cli_sync/$1/$2';
$route['sync/(:any)'] = 'offline/cli_sync/$1';
$route['resthook/(:any)'] = 'rest/v1/process_resthook';
$route['automation/(:any)'] = 'rest/v1/process_automation';
$route['token/(:any)'] = 'token/test';
$route['resync/(:any)/(:any)/(:any)'] = 'offline/resync/$1/$2/$3';
$route['thread/(:num)/(:any)/(:any)'] = 'daemon_amqp/thread/$1/$2/$3'; // Example 8
$route['connector/start-campaign'] = 'daemon_amqp/start_campaign';
$route['connector/set-next-sequence'] = 'daemon_amqp/set_next_sequence';
$route['createmacantauser'] = 'ajax/genpass';
$route['expired_app'] = 'ajax/expired_app';
$route['disable_app'] = 'ajax/disable_app';
$route['enable_app'] = 'ajax/enable_app';
$route['connecteddata/add'] = 'ajax/connecteddata_add';
$route['unsubscribe'] = 'ajax/unsubscribe';
$route['connecteddata/task'] = 'ajax/connecteddata_task';
$route['connecteddata/edit'] = 'ajax/connecteddata_edit';
$route['connecteddata/field'] = 'ajax/connecteddata_field';
$route['connecteddata/export'] = 'ajax/connecteddata_export';
$route['contacts'] = 'ajax/query_contact';
$route['user/add'] = 'ajax/add_user';
$route['user/(:any)'] = 'ajax/pre_login/$1';
$route['connecteddata'] = 'ajax/connecteddata';
$route['connecteddata/(:any)/(:any)/(:any)/(:any)'] = 'ajax/connecteddata/$1/$2/$3/$4';
$route['filebox/(:any)/(:any)'] = 'ajax/filebox/$1/$2';
$route['download/(:any)'] = 'ajax/download/$1';
$route['client-twiml.php'] = 'macanta/clientTwiml/';
$route['macanta-sw.js'] = 'macanta/serviceWorker/';
$route['autologin/id/(:num)'] = 'core/common/autologinold/';
$route['autologin/(:num)/(:any)'] = 'core/common/autologin/$1/$2'; // Example 8
$route['autologin/confirmed/(:num)/(:any)'] = 'core/common/autologinConfirmed/$1/$2';
$route['reconnect/token/(:any)'] = 'reconnect/token/$1';
$route['token/test/(:any)'] = 'token/test/$1';
$route['voice/(:any)'] = 'twilio/voice/$1';
$route['events'] = 'twilio/events/';
$route['api/example/users/(:num)'] = 'api/example/users/id/$1'; // Example 4
$route['api/example/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4'; // Example 8
