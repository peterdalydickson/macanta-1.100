<!--MACANTA CUSTOM CSS-->
<style>
    <?php echo $this->config->item('CustomCSS') ? base64_decode($this->config->item('CustomCSS')):"";?>
</style>
<script>
    var UserInfo = <?php echo isset($Details) ? $Details:"{}" ?>;
    <?php if (isset($AutoLoginKey)) { ?>
    var autoLoginInfo =  '<span class="auto-login-label nb-primary">Autologin URL</span><input type="text" class="AutoLoginCopyText" readonly value="<?php echo $this->config->item('base_url') . "autologin/" . $InfusionsoftID . "/" . $AutoLoginKey ?>"> <span class="copyAutoLogin">copy</span>';
    $(".AutoLoginCopy").html(autoLoginInfo);
    <?php } ?>
</script>
<script>
    $("body.main-search").removeClass("main-search");
    $("body.macanta-body").addClass("admin");
    $(".main-header").hide();
    $(".footer-content").show();
</script>
<?php
echo "<!--";
//print_r($user_meta);
echo "-->";
?>
<div class="DashboardPage">
<!-- Page Content -->
<div class="container">
    <div id="Header" class="dashboard-contact-header">
	    
	<!-- ⬇︎⬇︎⬇︎ CONTENT LOCATION: views > core > contact_index.php -->
       <small class="dev-only php">
	        <span>dashboard.php</span><br>
	        <span>CONTENT LOCATION: views > core > contact_index.php</span>
        </small>	
        <div class="core-contact-index"></div>
 

    </div><!-- End of dashboard-contact-header -->
    <!--<div class="row">
        <nav id="MainMenu" class="navbar col-lg-12" role="navigation">
            <ul class="core-menu-index nav col-lg-12 navbar-nav navigation-menus">

            </ul>
        </nav>
    </div>-->
    <div id="MainContent" class="">
        <!-- TABS CONTENT STARTED HERE-->
        <div class="core-tabs-index"></div>
        <!-- TABS CONTENT ENDED HERE-->
    </div>
    <!-- /.row -->

</div>
<!-- /.container -->
</div>
<!--MODALS HERE-->

<!--Modal For User Settings-->
<div id="UserSettingsWindow" class="modal fade UserSettingsWindow" tabindex="-1" role="dialog"
     aria-labelledby="UserSettingsWindowLabel">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">User Preferences</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="panel panel-primary right-LookAndFeel">
                    <div class="panel-heading">
                        <h3 class="panel-title "><i class="glyphicon glyphicon-phone-alt"></i> Call Settings</h3>
                    </div>
                    <div class="panel-body admin-panelBody">
                        <h3>Verified Caller Id's: </h3>
                        <ul class="CallerIdList">
                            <?php
                            $this->db->where('user_id',$InfusionsoftID);
                            $this->db->where('meta_key','caller_id');
                            $query = $this->db->get('users_meta');
                            $row = $query->row();
                            //$callerId = $data['PhoneNumber'];
                            if (isset($row))
                            {
                                $NewTwilio_info =  array();
                                $Twilio_info =  json_decode($row->meta_value, true);
                                foreach ($Twilio_info as $Phone=>$status){
                                    $cheched = $status['active'] == 'yes' ? 'checked':'';
                                    echo "<li class='CallerIdItem'>$Phone <span class='CallerIdOption'>Delete</span><span class='CallerIdOption'><input type='radio' name='ActiveCallerID' $cheched></span></li>";
                                }

                            }else{
                                echo "<li>No Caller ID configured, System Default Caller ID will be use or Add a Caller Id.</li>";
                            }

                            ?>

                        </ul>
                        <div class="toggleThisCallerID toggle-iphone"></div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal"><?php echo $this->lang->line('text_close'); ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!--Modal For Email History-->
<div id="EmailHistoryItem" class="modal fade EmailHistoryItem" tabindex="-1" role="dialog"
     aria-labelledby="EmailHistoryItem">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Email Record</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="panel panel-primary right-LookAndFeel">
                    <div class="panel-heading">
                        <h3 class="panel-title "><i class="fa fa-envelope-open"></i><span>EmailDetails</span> </h3>
                    </div>
                    <div class="panel-body admin-panelBody">



                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<?php
if(!$this->config->item('NoteCustomFields') && isset($userlevel) && $userlevel == "administrator" ){?>
    <!--Modal For Tip Window-->
    <div id="TipWindow" class="modal fade TipWindow" tabindex="-1" role="dialog" aria-labelledby="TipWindowLabel">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title">ADMIN ACTION REQUIRED</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <p>macanta adds extra data to Infusionsoft contact notes.</p>
                    <p>In order for us to store this data in Note custom fields, please follow <a href="http://help.macanta.org/knowledge_base/topics/create-a-note-custom-field-in-infusionsoft" target="_blank">these 4 steps</a> to create a Tab and Header in Infusionsoft.
                        Thanks!. </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('text_close');?></button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <script>
        var macanta_tips_shown = localStorage.getItem('macanta_tips_shown') || false;
        if(macanta_tips_shown === false){
            //$('#TipWindow').modal('show');
            //localStorage.setItem('macanta_tips_shown', 'yes');
        }

    </script>

<?php }
?>
<?php

$ConnectorRelationship = $this->config->item('ConnectorRelationship') ? json_decode($this->config->item('ConnectorRelationship')):[];
$availableRelationships = [];
foreach ($ConnectorRelationship as $RelationshipItem){
    $availableRelationships[$RelationshipItem->Id] = ["RelationshipName"=>$RelationshipItem->RelationshipName,"RelationshipDescription"=>$RelationshipItem->RelationshipDescription];
}
?>
<!--Template for added connected data-->
<li class="HTML-Template relationship-list-item">
    <div class="connected-contact-name-trash-container">
        <h3 class="connected-contact-name"></h3>
        <i class="fa fa-trash-o DeleteConenctedContactItem" aria-hidden="true"></i>
    </div><!-- /.connected-contact-name-trash-container -->
    <div class="form-group">
        <div class="connected-contact-select">
            <select  data-contactid=""
                     class="multiselect-ui field-relationships form-control addGroup"
                     multiple="multiple"
                     onchange="get_multi_select_values(this)"
                     required>
                <?php
                foreach ($availableRelationships as $Id => $availableRelationship){
                    echo '<option value="'.$Id.'" >'.$availableRelationship["RelationshipName"].'</option>';
                }
                ?>
            </select>
        </div>
    </div>
</li>
<!--MACANTA CUSTOM JS-->
<script>
    try {
        <?php echo $this->config->item('CustomJS') ? base64_decode($this->config->item('CustomJS')):"";?>
    }catch (e) {
        console.error("MACANTA CUSTOM JS ERROR: "+e);
        console.log("MACANTA CUSTOM JS ERROR: "+e);
    }

 </script>


