CREATE TABLE IF NOT EXISTS `InfusionsoftCache` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Created` varchar(100) DEFAULT NULL,
  `LastAccess` varchar(100) DEFAULT NULL,
  `TTL` int(10) DEFAULT NULL,
  `Content` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `InfusionsoftCompany` (
  `AccountId` int(11) NOT NULL,
  `Address1Type` text NOT NULL,
  `Address2Street1` text NOT NULL,
  `Address2Street2` text NOT NULL,
  `Address2Type` text NOT NULL,
  `Address3Street1` text NOT NULL,
  `Address3Street2` text NOT NULL,
  `Address3Type` text NOT NULL,
  `Anniversary` date NOT NULL,
  `AssistantName` text NOT NULL,
  `AssistantPhone` text NOT NULL,
  `BillingInformation` text NOT NULL,
  `Birthday` text NOT NULL,
  `City` text NOT NULL,
  `City2` text NOT NULL,
  `City3` text NOT NULL,
  `Company` text NOT NULL,
  `CompanyID` int(11) NOT NULL,
  `ContactNotes` longtext NOT NULL,
  `ContactType` text NOT NULL,
  `Country` text NOT NULL,
  `Country2` text NOT NULL,
  `Country3` text NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `DateCreated` text NOT NULL,
  `Email` text NOT NULL,
  `EmailAddress2` text NOT NULL,
  `EmailAddress3` text NOT NULL,
  `Fax1` text NOT NULL,
  `Fax1Type` text NOT NULL,
  `Fax2` text NOT NULL,
  `Fax2Type` text NOT NULL,
  `FirstName` text NOT NULL,
  `Groups` text NOT NULL,
  `Id` int(11) NOT NULL,
  `IdLocal` int(11) NOT NULL,
  `JobTitle` text NOT NULL,
  `LastName` text NOT NULL,
  `LastUpdated` text NOT NULL,
  `LastUpdatedBy` int(11) NOT NULL,
  `Password` text NOT NULL,
  `Phone2` text NOT NULL,
  `Phone1Ext` text NOT NULL,
  `Phone1` text NOT NULL,
  `MiddleName` text NOT NULL,
  `OwnerID` int(11) NOT NULL,
  `Nickname` text NOT NULL,
  `Phone2Type` text NOT NULL,
  `Phone2Ext` text NOT NULL,
  `PostalCode` text NOT NULL,
  `Phone1Type` text NOT NULL,
  `Phone3Ext` text NOT NULL,
  `PostalCode2` text NOT NULL,
  `Phone3` text NOT NULL,
  `Phone5Ext` text NOT NULL,
  `Phone5` text NOT NULL,
  `Phone5Type` text NOT NULL,
  `Phone4Ext` text NOT NULL,
  `Phone4` text NOT NULL,
  `Phone3Type` text NOT NULL,
  `Phone4Type` text NOT NULL,
  `PostalCode3` text NOT NULL,
  `ReferralCode` text NOT NULL,
  `SpouseName` text NOT NULL,
  `State` text NOT NULL,
  `State2` text NOT NULL,
  `State3` text NOT NULL,
  `StreetAddress1` text NOT NULL,
  `StreetAddress2` text NOT NULL,
  `Suffix` text NOT NULL,
  `Title` text NOT NULL,
  `Username` text NOT NULL,
  `Validated` text NOT NULL,
  `Website` text NOT NULL,
  `ZipFour1` text NOT NULL,
  `ZipFour2` text NOT NULL,
  `ZipFour3` text NOT NULL,
  `Origin` varchar(15) NOT NULL DEFAULT 'Infusionsoft'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `InfusionsoftContact` (
  `AccountId` int(11) NOT NULL,
  `Address1Type` text NOT NULL,
  `Address2Street1` text NOT NULL,
  `Address2Street2` text NOT NULL,
  `Address2Type` text NOT NULL,
  `Address3Street1` text NOT NULL,
  `Address3Street2` text NOT NULL,
  `Address3Type` text NOT NULL,
  `Anniversary` text NOT NULL,
  `AssistantName` text NOT NULL,
  `AssistantPhone` text NOT NULL,
  `BillingInformation` text NOT NULL,
  `Birthday` text NOT NULL,
  `City` text NOT NULL,
  `City2` text NOT NULL,
  `City3` text NOT NULL,
  `Company` text NOT NULL,
  `CompanyID` int(11) NOT NULL,
  `ContactNotes` longtext NOT NULL,
  `ContactType` text NOT NULL,
  `Country` text NOT NULL,
  `Country2` text NOT NULL,
  `Country3` text NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `DateCreated` text NOT NULL,
  `Email` text NOT NULL,
  `EmailAddress2` text NOT NULL,
  `EmailAddress3` text NOT NULL,
  `Fax1` text NOT NULL,
  `Fax1Type` text NOT NULL,
  `Fax2` text NOT NULL,
  `Fax2Type` text NOT NULL,
  `FirstName` text NOT NULL,
  `Groups` text NOT NULL,
  `Id` int(11) NOT NULL,
  `IdLocal` int(11) NOT NULL,
  `JobTitle` text NOT NULL,
  `LastName` text NOT NULL,
  `LastUpdated` text NOT NULL,
  `LastUpdatedBy` int(11) NOT NULL,
  `Password` text NOT NULL,
  `Phone2` text NOT NULL,
  `Phone1Ext` text NOT NULL,
  `Phone1` text NOT NULL,
  `MiddleName` text NOT NULL,
  `OwnerID` int(11) NOT NULL,
  `Nickname` text NOT NULL,
  `Phone2Type` text NOT NULL,
  `Leadsource` text NOT NULL,
  `LeadSourceId` int(11) NOT NULL,
  `Phone2Ext` text NOT NULL,
  `PostalCode` text NOT NULL,
  `Phone1Type` text NOT NULL,
  `Phone3Ext` text NOT NULL,
  `PostalCode2` text NOT NULL,
  `Phone3` text NOT NULL,
  `Phone5Ext` text NOT NULL,
  `Phone5` text NOT NULL,
  `Phone5Type` text NOT NULL,
  `Phone4Ext` text NOT NULL,
  `Phone4` text NOT NULL,
  `Phone3Type` text NOT NULL,
  `Phone4Type` text NOT NULL,
  `PostalCode3` text NOT NULL,
  `ReferralCode` text NOT NULL,
  `SpouseName` text NOT NULL,
  `State` text NOT NULL,
  `State2` text NOT NULL,
  `State3` text NOT NULL,
  `StreetAddress1` text NOT NULL,
  `StreetAddress2` text NOT NULL,
  `TimeZone` text NOT NULL,
  `Title` text NOT NULL,
  `Suffix` text NOT NULL,
  `Username` text NOT NULL,
  `Validated` text NOT NULL,
  `Website` text NOT NULL,
  `ZipFour1` text NOT NULL,
  `ZipFour2` text NOT NULL,
  `ZipFour3` text NOT NULL,
  `Origin` varchar(15) NOT NULL DEFAULT 'Infusionsoft',
  `CustomField` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `InfusionsoftContactAction` (
  `Accepted` int(11) NOT NULL,
  `ActionDate` text NOT NULL,
  `ActionDescription` text NOT NULL,
  `ActionType` text NOT NULL,
  `CompletionDate` text NOT NULL,
  `ContactId` int(11) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreationDate` text NOT NULL,
  `CreationNotes` text NOT NULL,
  `EndDate` text NOT NULL,
  `Id` int(11) NOT NULL,
  `IdLocal` int(11) NOT NULL,
  `IsAppointment` int(11) NOT NULL,
  `LastUpdated` text NOT NULL,
  `LastUpdatedBy` int(11) NOT NULL,
  `Location` text NOT NULL,
  `ObjectType` enum('Note','Task','Appointment') NOT NULL,
  `OpportunityId` int(11) NOT NULL,
  `PopupDate` text NOT NULL,
  `Priority` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Origin` varchar(15) NOT NULL DEFAULT 'Infusionsoft',
  `CustomField` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `InfusionsoftContactGroup` (
  `GroupCategoryId` int(11) NOT NULL,
  `GroupDescription` text NOT NULL,
  `GroupName` text NOT NULL,
  `Id` int(11) NOT NULL,
  `IdLocal` int(11) NOT NULL,
  `Origin` varchar(15) NOT NULL DEFAULT 'Infusionsoft',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `InfusionsoftContactGroupAssign` (
  `ContactId` int(11) NOT NULL,
  `ContactGroup` text NOT NULL,
  `GroupId` int(11) NOT NULL,
  `DateCreated` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `InfusionsoftContactGroupCategory` (
  `CategoryDescription` text NOT NULL,
  `CategoryName` text NOT NULL,
  `Id` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `InfusionsoftDataFormField` (
  `DataType` int(11) NOT NULL,
  `DefaultValue` text NOT NULL,
  `FormId` int(11) NOT NULL,
  `GroupId` int(11) NOT NULL,
  `Id` int(11) NOT NULL,
  `Label` text NOT NULL,
  `ListRows` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Values` text NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `InfusionsoftDataFormGroup` (
  `Id` int(11) NOT NULL,
  `Name` text NOT NULL,
  `TabId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `InfusionsoftDataFormTab` (
  `FormId` int(11) NOT NULL,
  `Id` int(11) NOT NULL,
  `TabName` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `InfusionsoftEmailAddStatus` (
  `DateCreated` text NOT NULL,
  `Email` text NOT NULL,
  `Id` int(11) NOT NULL,
  `LastClickDate` text NOT NULL,
  `LastOpenDate` text NOT NULL,
  `LastSentDate` text NOT NULL,
  `Type` text NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `InfusionsoftFileBox` (
  `ContactId` int(11) NOT NULL,
  `Extension` text NOT NULL,
  `FileName` text NOT NULL,
  `FileSize` longtext NOT NULL,
  `Id` int(11) NOT NULL,
  `Public` int(11) NOT NULL,
  `FileData` longtext NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `InfusionsoftInvoice` (
  `AffiliateId` int(11) NOT NULL,
  `ContactId` int(11) NOT NULL,
  `CreditStatus` int(11) NOT NULL,
  `DateCreated` text NOT NULL,
  `Description` text NOT NULL,
  `Id` int(11) NOT NULL,
  `InvoiceTotal` double NOT NULL,
  `InvoiceType` text NOT NULL,
  `JobId` int(11) NOT NULL,
  `LastUpdated` text NOT NULL,
  `LeadAffiliateId` int(11) NOT NULL,
  `PayPlanStatus` int(11) NOT NULL,
  `PayStatus` int(11) NOT NULL,
  `ProductSold` text NOT NULL,
  `PromoCode` text NOT NULL,
  `RefundStatus` int(11) NOT NULL,
  `Synced` int(11) NOT NULL,
  `TotalDue` double NOT NULL,
  `TotalPaid` double NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `InfusionsoftLead` (
  `AffiliateId` int(11) NOT NULL,
  `ContactID` int(11) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `DateCreated` text NOT NULL,
  `DateInStage` text NOT NULL,
  `EstimatedCloseDate` text NOT NULL,
  `Id` int(11) NOT NULL,
  `IncludeInForecast` int(11) NOT NULL,
  `LastUpdated` text NOT NULL,
  `LastUpdatedBy` int(11) NOT NULL,
  `Leadsource` text NOT NULL,
  `MonthlyRevenue` double NOT NULL,
  `NextActionDate` text NOT NULL,
  `NextActionNotes` text NOT NULL,
  `Objection` text NOT NULL,
  `OpportunityNotes` text NOT NULL,
  `OpportunityTitle` text NOT NULL,
  `OrderRevenue` double NOT NULL,
  `ProjectedRevenueHigh` double NOT NULL,
  `ProjectedRevenueLow` double NOT NULL,
  `StageID` int(11) NOT NULL,
  `StatusID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `CustomField` longtext NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `InfusionsoftLeadSource` (
  `CostPerLead` text NOT NULL,
  `Description` text NOT NULL,
  `EndDate` text NOT NULL,
  `Id` int(11) NOT NULL,
  `LeadSourceCategoryId` int(11) NOT NULL,
  `Medium` text NOT NULL,
  `Message` text NOT NULL,
  `Name` text NOT NULL,
  `StartDate` text NOT NULL,
  `Status` text NOT NULL,
  `Vendor` text NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `InfusionsoftSavedFilter` (
  `FilterName` text NOT NULL,
  `Id` int(11) NOT NULL,
  `ReportStoredName` text NOT NULL,
  `UserId` text NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `InfusionsoftStage` (
  `Id` int(11) NOT NULL,
  `StageName` text NOT NULL,
  `StageOrder` int(11) NOT NULL,
  `TargetNumDays` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `InfusionsoftTemplate` (
  `Categories` text NOT NULL,
  `Id` int(11) NOT NULL,
  `PieceTitle` text NOT NULL,
  `PieceType` text NOT NULL,
  `PieceContent` longtext NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `InfusionsoftEmailSent` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `subject` text,
  `headers` text,
  `contact_id` int(11) NOT NULL,
  `sent_to_address` text NOT NULL,
  `sent_to_cc_addresses` text,
  `sent_to_bcc_addresses` text,
  `sent_from_address` text,
  `sent_from_reply_address` text,
  `sent_date` varchar(30) DEFAULT NULL,
  `received_date` varchar(30) DEFAULT NULL,
  `opened_date` varchar(30) DEFAULT NULL,
  `clicked_date` varchar(30) DEFAULT NULL,
  `original_provider` varchar(100) DEFAULT NULL,
  `original_provider_id` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE  IF NOT EXISTS `InfusionsoftEmailContent` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `subject` text,
  `headers` text,
  `contact_id` int(11) NOT NULL,
  `sent_to_address` text NOT NULL,
  `sent_to_cc_addresses` text,
  `sent_to_bcc_addresses` text,
  `sent_from_address` text,
  `sent_from_reply_address` text,
  `sent_date` varchar(30) DEFAULT NULL,
  `received_date` varchar(30) DEFAULT NULL,
  `opened_date` varchar(30) DEFAULT NULL,
  `clicked_date` varchar(30) DEFAULT NULL,
  `original_provider` varchar(100) DEFAULT NULL,
  `original_provider_id` varchar(30) DEFAULT NULL,
  `plain_content` text,
  `html_content` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `InfusionsoftUser` (
  `City` text NOT NULL,
  `Email` text NOT NULL,
  `EmailAddress2` text NOT NULL,
  `EmailAddress3` text NOT NULL,
  `FirstName` text NOT NULL,
  `GlobalUserId` int(11) NOT NULL,
  `HTMLSignature` text NOT NULL,
  `Id` int(11) NOT NULL,
  `LastName` text NOT NULL,
  `MiddleName` text NOT NULL,
  `Nickname` text NOT NULL,
  `Partner` text NOT NULL,
  `Phone1` text NOT NULL,
  `Phone1Ext` text NOT NULL,
  `Phone1Type` text NOT NULL,
  `Phone2` text NOT NULL,
  `Phone2Ext` text NOT NULL,
  `Phone2Type` text NOT NULL,
  `PostalCode` text NOT NULL,
  `Signature` text NOT NULL,
  `SpouseName` text NOT NULL,
  `State` text NOT NULL,
  `StreetAddress1` text NOT NULL,
  `StreetAddress2` text NOT NULL,
  `Suffix` text NOT NULL,
  `Title` text NOT NULL,
  `ZipFour1` text NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `SyncProcess` (
  `TableName` varchar(32) NOT NULL,
  `TotalItems` int(11) NOT NULL,
  `CurrentItem` int(11) NOT NULL,
  `Operation` varchar(32) NOT NULL,
  `Status` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `SyncRecord` (
  `TableName` text NOT NULL,
  `LastSynced` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;