-- Adminer 4.7.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `ConnectedDataAutomation`;
CREATE TABLE `ConnectedDataAutomation` (
  `QueryId` varchar(30) NOT NULL,
  `QueryLabel` text NOT NULL,
  `QueryType` varchar(30) NOT NULL,
  `ConnectedDataGroupName` varchar(100) NOT NULL,
  `Data` longtext NOT NULL,
  `Status` varchar(30) DEFAULT NULL,
  `Created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `QueryId` (`QueryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `ConnectedDataAutomationTriggerRecord`;
CREATE TABLE `ConnectedDataAutomationTriggerRecord` (
  `QueryId` varchar(30) NOT NULL,
  `LastTriggered` varchar(30) NOT NULL,
  KEY `QueryId` (`QueryId`),
  CONSTRAINT `ConnectedDataAutomationTriggerRecord_ibfk_2` FOREIGN KEY (`QueryId`) REFERENCES `ConnectedDataAutomation` (`QueryId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `ConnectedDataQuery`;
CREATE TABLE `ConnectedDataQuery` (
  `Id` int(15) NOT NULL AUTO_INCREMENT,
  `QueryId` varchar(30) NOT NULL,
  `Data` longtext NOT NULL,
  `TagId` int(15) DEFAULT NULL,
  `Status` varchar(30) NOT NULL,
  `CreatedBy` varchar(100) NOT NULL DEFAULT 'system',
  `Created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `QueryId` (`QueryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `MacantaField`;
CREATE TABLE `MacantaField` (
  `FieldId` varchar(100) NOT NULL,
  `GroupId` varchar(100) NOT NULL,
  `Label` text NOT NULL,
  `Type` varchar(100) NOT NULL DEFAULT 'Text',
  `Required` varchar(15) DEFAULT 'no',
  `ReadOnly` enum('yes','no') DEFAULT 'no',
  `DefaultValue` text,
  `PlaceHolderText` text,
  `HelperText` text,
  `UseAsListHeader` varchar(15) DEFAULT 'no',
  `ListHeaderOrder` int(5) DEFAULT '0',
  `Created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FieldId`),
  KEY `GroupId` (`GroupId`),
  CONSTRAINT `MacantaField_ibfk_1` FOREIGN KEY (`GroupId`) REFERENCES `MacantaGroup` (`GroupId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `MacantaFieldMeta`;
CREATE TABLE `MacantaFieldMeta` (
  `MetaId` varchar(100) NOT NULL,
  `FieldId` varchar(100) NOT NULL,
  `MetaName` varchar(100) NOT NULL COMMENT 'Custom Field Assign, Tag Assign, Data Reference, Contact Specific Etc..',
  `MetaValue` longtext NOT NULL,
  UNIQUE KEY `MetaId` (`MetaId`),
  KEY `FieldId` (`FieldId`),
  CONSTRAINT `MacantaFieldMeta_ibfk_1` FOREIGN KEY (`FieldId`) REFERENCES `MacantaField` (`FieldId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `MacantaFieldValue`;
CREATE TABLE `MacantaFieldValue` (
  `ItemId` varchar(100) NOT NULL,
  `FieldId` varchar(100) NOT NULL,
  `FieldValue` longtext,
  UNIQUE KEY `ItemId_FieldId` (`ItemId`,`FieldId`),
  KEY `ItemId` (`ItemId`),
  KEY `FieldId` (`FieldId`),
  CONSTRAINT `MacantaFieldValue_ibfk_2` FOREIGN KEY (`FieldId`) REFERENCES `MacantaField` (`FieldId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `MacantaFieldValue_ibfk_3` FOREIGN KEY (`ItemId`) REFERENCES `MacantaItem` (`ItemId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `MacantaFileAttachment`;
CREATE TABLE `MacantaFileAttachment` (
  `Id` int(15) NOT NULL AUTO_INCREMENT,
  `ItemId` varchar(100) NOT NULL,
  `Filename` varchar(150) NOT NULL,
  `Thumbnail` longtext NOT NULL,
  `DownloadURL` text NOT NULL,
  `FileSize` int(30) NOT NULL,
  `B2Filename` varchar(300) NOT NULL,
  `B2FileId` varchar(150) NOT NULL,
  `Meta` longtext NOT NULL,
  `Created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `ItemId_B2FileId` (`ItemId`,`B2FileId`),
  KEY `ItemId` (`ItemId`),
  CONSTRAINT `MacantaFileAttachment_ibfk_3` FOREIGN KEY (`ItemId`) REFERENCES `MacantaItem` (`ItemId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `MacantaGroup`;
CREATE TABLE `MacantaGroup` (
  `GroupId` varchar(100) NOT NULL,
  `Label` varchar(150) NOT NULL,
  `Type` enum('regular','custom') NOT NULL DEFAULT 'custom' COMMENT 'Regular:  Contact, Company, ContactGroup, Invoice, Notes,  Task . Irregular: Others e.i. CD''s',
  `Status` varchar(100) NOT NULL DEFAULT 'active',
  `Created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`GroupId`),
  UNIQUE KEY `Label_Type` (`Label`,`Type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `MacantaGroupMeta`;
CREATE TABLE `MacantaGroupMeta` (
  `MetaId` varchar(100) NOT NULL,
  `GroupId` varchar(100) NOT NULL,
  `MetaName` varchar(100) NOT NULL COMMENT 'Permission, Tag Permision etc..',
  `MetaValue` longtext NOT NULL,
  UNIQUE KEY `MetaId` (`MetaId`),
  KEY `GeoupId` (`GroupId`),
  CONSTRAINT `MacantaGroupMeta_ibfk_2` FOREIGN KEY (`GroupId`) REFERENCES `MacantaGroup` (`GroupId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `MacantaGroupRelationship`;
CREATE TABLE `MacantaGroupRelationship` (
  `GroupId` varchar(100) NOT NULL,
  `RelationshipId` varchar(100) NOT NULL,
  `Type` enum('contact','data') NOT NULL,
  `Exclusive` varchar(100) NOT NULL DEFAULT 'yes',
  `Limit` int(15) DEFAULT NULL,
  UNIQUE KEY `GroupId_RelationshipId_Type` (`GroupId`,`RelationshipId`,`Type`),
  KEY `RelationshipId` (`RelationshipId`),
  CONSTRAINT `MacantaGroupRelationship_ibfk_1` FOREIGN KEY (`GroupId`) REFERENCES `MacantaGroup` (`GroupId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `MacantaGroupRelationship_ibfk_2` FOREIGN KEY (`RelationshipId`) REFERENCES `MacantaRelationship` (`RelationshipId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `MacantaHistory`;
CREATE TABLE `MacantaHistory` (
  `Id` int(15) NOT NULL AUTO_INCREMENT,
  `ItemId` varchar(100) NOT NULL,
  `UpdatedBy` varchar(300) NOT NULL,
  `Type` varchar(100) NOT NULL,
  `Data` longtext NOT NULL,
  `Created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `ItemId` (`ItemId`),
  CONSTRAINT `MacantaHistory_ibfk_3` FOREIGN KEY (`ItemId`) REFERENCES `MacantaItem` (`ItemId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `MacantaItem`;
CREATE TABLE `MacantaItem` (
  `ItemId` varchar(100) NOT NULL,
  `GroupId` varchar(100) NOT NULL,
  `Status` varchar(100) NOT NULL DEFAULT 'active',
  `CreatedBy` varchar(100) NOT NULL DEFAULT 'system_api',
  `Created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ItemId`),
  KEY `GroupId` (`GroupId`),
  CONSTRAINT `MacantaItem_ibfk_1` FOREIGN KEY (`GroupId`) REFERENCES `MacantaGroup` (`GroupId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `MacantaItemMeta`;
CREATE TABLE `MacantaItemMeta` (
  `MetaId` varchar(100) NOT NULL,
  `ItemId` varchar(100) NOT NULL,
  `MetaName` varchar(100) NOT NULL COMMENT 'Can be UserLevel, CustomField, InfusionsoftId, TagId, Status etc..',
  `MetaValue` longtext NOT NULL,
  UNIQUE KEY `MetaId` (`MetaId`),
  UNIQUE KEY `ItemId_MetaName` (`ItemId`,`MetaName`),
  KEY `ItemId` (`ItemId`),
  CONSTRAINT `MacantaItemMeta_ibfk_2` FOREIGN KEY (`ItemId`) REFERENCES `MacantaItem` (`ItemId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `MacantaRelationship`;
CREATE TABLE `MacantaRelationship` (
  `RelationshipId` varchar(100) NOT NULL,
  `Label` text NOT NULL,
  `Description` text,
  `Created` datetime DEFAULT CURRENT_TIMESTAMP,
  `Updated` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`RelationshipId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `MacantaSequence`;
CREATE TABLE `MacantaSequence` (
  `ContactItemId` varchar(100) NOT NULL,
  `ItemId` varchar(100) NOT NULL,
  `GroupId` varchar(100) NOT NULL,
  `InitialSequence` varchar(15) NOT NULL,
  `NextSequence` int(15) DEFAULT NULL,
  `RestartAfter` text,
  `MoveAfter` text,
  `MoveWhen` text,
  `Parsed` text,
  `Status` varchar(100) DEFAULT 'active',
  `Meta` text,
  `Created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `ItemId_GroupId_InitialSequence_ContactItemId` (`ItemId`,`GroupId`,`InitialSequence`,`ContactItemId`),
  KEY `GroupId` (`GroupId`),
  KEY `ItemId` (`ItemId`),
  KEY `ContactItemId` (`ContactItemId`),
  CONSTRAINT `MacantaSequence_ibfk_1` FOREIGN KEY (`GroupId`) REFERENCES `MacantaGroup` (`GroupId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `MacantaSequence_ibfk_2` FOREIGN KEY (`ItemId`) REFERENCES `MacantaItem` (`ItemId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `MacantaSequence_ibfk_3` FOREIGN KEY (`ContactItemId`) REFERENCES `MacantaItem` (`ItemId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- 2019-11-11 09:21:43