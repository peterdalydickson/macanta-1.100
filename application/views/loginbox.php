<div class="form-box login-box">



    <div class="form-top">
    <h5 class="login-message <?php echo isset($login_disabled) ? "in_arrears":"" ?> <?php echo isset($_SESSION['token_generated']) ? "successful":"" ?>"
        <?php echo isset($_SESSION['token_generated']) ? 'style="display: block !important;"':"" ?>>
        <?php
        if(isset($_SESSION['token_generated'])){
            echo 'TOKEN SUCCESSFULLY GENERATED!.<br> Please Login.';
            unset($_SESSION['token_generated']);
        }
        if(isset($login_disabled)){
            echo $login_disabled;
        }
        ?>
    </h5>

</div>
    <div class="form-bottom login-body">
    <div class="login-form-header">
        <h3><?php echo $this->lang->line('text_login');?><!--Login--></h3>
        <p><?php echo $this->lang->line('text_please_fill_in');?><!--Please fill in the form below:--></p>
    </div>	    
    <div class="">
        <form role="form"   class="registration-form macantaLogin">
            <div class="form-group">
                <label class="sr-only" for="identity"><?php echo $this->lang->line('text_email');?><!--Email--></label>
                <input type="email" name="email" placeholder="<?php echo $this->lang->line('text_email');?>..." class="form-email form-control" id="email" required <?php echo isset($login_disabled) ? "disabled":"" ?>>
            </div>
            <div class="form-group">
                <label class="sr-only" for="password"><?php echo $this->lang->line('text_password');?><!--Password--></label>
                <input type="password" name="password" placeholder="<?php echo $this->lang->line('text_password');?>..." class="form-last-name form-control" id="password" required <?php echo isset($login_disabled) ? "disabled":"" ?>>
            </div>
            <div class="form-group">

              <!--  <input type="checkbox" name="keepmeloggedin" class="form-last-name form-control" id="keepmeloggedin"  <?php /*echo isset($login_disabled) ? "disabled":"" */?> >
                <label class="keepmeloggedin_label" for="password">Keep me logged in?</label> -->
            </div>
            <button type="submit" class="submit nb-btn nb-primary" <?php echo isset($login_disabled) ? "disabled":"" ?>><?php echo $this->lang->line('text_log_me_in');?><!--Log me in-->...</button>
        </form>
    </div>
</div>
</div>
<script>
    var LoginMessage = localStorage.getItem('LoginMessage');
    if(LoginMessage){
        $(".login-message").html(LoginMessage).fadeIn();
        localStorage.removeItem('LoginMessage');
    }
</script>