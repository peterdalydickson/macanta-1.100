<?php
//This is the main macanta template wrapper.
ob_start("ob_gzhandler");
?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv='cache-control' content='no-cache'>
        <meta http-equiv='expires' content='0'>
        <meta http-equiv='pragma' content='no-cache'>
        <title><?php echo $this->config->item('sitename'); ?></title>
        <meta content="<?php echo $this->config->item('sitename'); ?>" property="og:title">
        <meta content="crm as unique as your business" name="description">
        <meta content="Macanta" property="og:site_name">
        <meta content="product" property="og:type">
        <meta content="<?php echo $this->config->item('base_url'); ?>" property="og:url">
        <meta content="<?php echo $this->config->item('base_url'); ?>assets/img/macantaicon.png" property="og:image">
        <meta content="<?php echo $this->config->item('base_url'); ?>assets/img/macantaicon.png" name="twitter:image">
        <link rel='mask-icon' href='<?php echo $this->config->item('base_url'); ?>assets/img/macantaicon.png'
              color='#7BBE4A'>
        <?php echo assets_css($stylesheets, array('v' => $this->config->item('macanta_verson'))); ?>
        <link rel="icon"
              href="<?php echo $this->config->item('base_url'); ?>assets/ico/favicon.png?v=<?php echo $this->config->item('macanta_verson'); ?>"/>
        <link rel="shortcut icon"
              href="<?php echo $this->config->item('base_url'); ?>assets/ico/favicon.png?v=<?php echo $this->config->item('macanta_verson'); ?>">
        <script type="application/javascript">

            var ajax_url = "<?php echo base_url() . 'index.php/ajax'?>";
            var session_name = localStorage.getItem("session_name") || '';
            var assetsVersion = localStorage.getItem("asset_version");
            var serverAssetsVersion = "<?php echo $this->config->item('macanta_verson');?>";
            if (assetsVersion === null) {
                localStorage.setItem("asset_version", serverAssetsVersion);
                assetsVersion = serverAssetsVersion;
            }
            var UserConnectorInfoTable = [];
            var ajax_requests = {};
            var SearchCache = {};
            var SelectedSearchCache = '';
            var DataTableColumn = {};
            var OppTableData = [];
            var OppTable = {};
            var ContactsTable;
            var ContactsDataTable;
            var ContactId; // Infustionsoft ContactId
            var ContactInfo = {};
            var UserCallerIds = {};
            /*Javascript Text Language replacement*/
            var MacantaLanguages = {};
            var myDataTable = {};
            var ZuoraDataObj = {};
            var allFilters = [];
            var allFiltersStr = '';
            var CustomEditorInit = false;
            var FilterResults = {};
            var LoqateRequest = '';
            var PhoneValidation = {};
            var FilteredContactId = [];
            var UserConnectorInfoTableFilterResult;
            var EmailValidation = {};
            var BillingAddressValidated = '';
            var ShippingAddressValidated = '';
            var LoadedTabs = [];
            var TabMenuHeight = '';
            var ISCustomFieldsArr = {};
            var UsersCallerIds = {};
            var ContactCustomFields = {};
            var ConnectedInfoSettings = localStorage.getItem("cd_settings") || "{}";
                ConnectedInfoSettings = JSON.parse(ConnectedInfoSettings);
            var ConnectorRelationshipArr = {};
            var MacantaUsers = {};
            var ContactGroups = {};
            var ConnectedInfoGroupFieldsMap = {};
            var ConnectedDataAutomationSettings = {};
            var MacantaDialogMessages = {};
            MacantaLanguages['please_wait_loading'] = '<?php echo $this->lang->line('text_please_wait_loading'); ?>';
            MacantaLanguages['text_add_note_tag'] = '<?php echo $this->lang->line('text_add_note_tag'); ?>';
            MacantaLanguages['text_add_tag_filter'] = '<?php echo $this->lang->line('text_add_tag_filter'); ?>';
            MacantaLanguages['text_words'] = '<?php echo $this->lang->line('text_words'); ?>';
            MacantaLanguages['text_show'] = '<?php echo $this->lang->line('text_show'); ?>';
            MacantaLanguages['text_entries'] = '<?php echo $this->lang->line('text_entries'); ?>';
            MacantaLanguages['text_previous'] = '<?php echo $this->lang->line('text_previous'); ?>';
            MacantaLanguages['text_next'] = '<?php echo $this->lang->line('text_next'); ?>';
            MacantaLanguages['text_no_contact_id'] = "<?php echo $this->lang->line('text_no_contact_id'); ?>";

        </script>
        <?php
        if ($mobile == 'yes') echo '<meta name="viewport" content="width=device-width">';
        ?>
        <!-- Segment Code -->
        <script>
            !function () {
                var analytics = window.analytics = window.analytics || [];
                if (!analytics.initialize) if (analytics.invoked) window.console && console.error && console.error("Segment snippet included twice."); else {
                    analytics.invoked = !0;
                    analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "reset", "group", "track", "ready", "alias", "debug", "page", "once", "off", "on"];
                    analytics.factory = function (t) {
                        return function () {
                            var e = Array.prototype.slice.call(arguments);
                            e.unshift(t);
                            analytics.push(e);
                            return analytics
                        }
                    };
                    for (var t = 0; t < analytics.methods.length; t++) {
                        var e = analytics.methods[t];
                        analytics[e] = analytics.factory(e)
                    }
                    analytics.load = function (t, e) {
                        var n = document.createElement("script");
                        n.type = "text/javascript";
                        n.async = !0;
                        n.src = "https://cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js";
                        var a = document.getElementsByTagName("script")[0];
                        a.parentNode.insertBefore(n, a);
                        analytics._loadOptions = e
                    };
                    analytics.SNIPPET_VERSION = "4.1.0";
                    analytics.load("F8cK3qsAJgYANQgFBwg5k0xjBPWZRlYn");
                    analytics.page();
                }
            }();
        </script>
        <!--<script>
            (function (u, s, e, r, g) {
                u[r] = u[r] || [];
                u[r].push({
                    'ug.start': new Date().getTime(), event: 'embed.js',
                });
                var f = s.getElementsByTagName(e)[0],
                    j = s.createElement(e);
                j.async = true;
                j.src = 'https://static.userguiding.com/media/user-guiding-'
                    + g + '-embedded.js';
                j.onload = function(){ userGuiding.track("segment", { AppAdmin: '.$Admin.', AppUser: '.$User.', }); };
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'userGuidingLayer', '04844493ID');
        </script>-->
        <!-- Segment Code End-->
    </head>
    <body id="macanta-body" class="macanta-body">
    <div id="fountainG">
        <div class='fountainText'><?php echo $this->lang->line('text_loading'); ?>...</div>
        <div id="fountainG_1" class="fountainG"></div>
        <div id="fountainG_2" class="fountainG"></div>
        <div id="fountainG_3" class="fountainG"></div>
        <div id="fountainG_4" class="fountainG"></div>
    </div>
    <div class="body-container">
        <div class="body-content-container">
	        <!-- Content Starts -->
            <?php echo $content; ?>
        </div>
    </div>
    <!--<script type="text/javascript" src="//static.twilio.com/libs/twiliojs/1.2/twilio.min.js"></script>-->
    <!--<script type="text/javascript" src="//media.twiliocdn.com/sdk/js/client/v1.3/twilio.min.js"></script>-->
    <script type="text/javascript" src="//media.twiliocdn.com/sdk/js/client/v1.8/twilio.min.js"></script>
    <script type="text/javascript" src="//media.twiliocdn.com/taskrouter/js/v1.12/taskrouter.min.js"></script>
    <script type="text/javascript" src="//media.twiliocdn.com/sdk/js/sync/v0.7/twilio-sync.min.js"></script>
    <?php echo assets_js($javascripts, array('v' => $this->config->item('macanta_verson'))) ?>
    <script>
        /* Hopscotch - to be implemented*/
        var calloutMgr = hopscotch.getCalloutManager();

        /*FROM macanta_utilty.js.txt*/
        <?php
        if (file_exists(FCPATH . "assets/custom_js/macanta_utility.js.txt")) {
            $theScript = file_get_contents(FCPATH . "assets/custom_js/macanta_utility.js.txt");
            echo $theScript;
        }
        ?>
        <?php
        $write_code = is_hosted();
        if ($write_code){  ?>
        <!-- Begin of Chaport Live Chat code -->
        /*(function (w, d, v3) {
            w.chaportConfig = {appId: '5b17b4927e05df3a611bf674'};

            if (w.chaport) return;
            v3 = w.chaport = {};
            v3._q = [];
            v3._l = {};
            v3.q = function () {
                v3._q.push(arguments)
            };
            v3.on = function (e, fn) {
                if (!v3._l[e]) v3._l[e] = [];
                v3._l[e].push(fn)
            };
            var s = d.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = 'https://app.chaport.com/javascripts/insert.js';
            var ss = d.getElementsByTagName('script')[0];
            ss.parentNode.insertBefore(s, ss)
        })(window, document);*/
        <!-- End of Chaport Live Chat code -->
        <!-- BEGIN zohostatic CODE -->
        /*var zsFeedbackTabPref = {};
        zsFeedbackTabPref.tabTitle = "Click Here For Help";
        zsFeedbackTabPref.tabColor = "#78bf42";
        zsFeedbackTabPref.tabFontColor = "#FFFFFF";
        zsFeedbackTabPref.tabPosition = "Right";
        zsFeedbackTabPref.tabOffset = "526";
        zsFeedbackTabPref.display = "popout";
        zsFeedbackTabPref.srcDiv = "zsfeedbackwidgetdiv";
        zsFeedbackTabPref.defaultDomain = "https://desk.zoho.com";
        zsFeedbackTabPref.feedbackId = "3c4ce33b05207de385e99c47a0352837afb6719abeecaa48";
        zsFeedbackTabPref.fbURL = "https://desk.zoho.com/support/fbw?formType=AdvancedWebForm&fbwId=3c4ce33b05207de385e99c47a0352837afb6719abeecaa48&xnQsjsdp=bLSN-BymkY1vJE5CPjqIkA$$&mode=showWidget&displayType=popout";
        zsFeedbackTabPref.jsStaticUrl = "https://js.zohostatic.com/support/fbw_v8/js";
        zsFeedbackTabPref.cssStaticUrl = "https://css.zohostatic.com/support/fbw_v8/css";

        var feedbackInitJs = document.createElement("script");
        feedbackInitJs.type = "text/javascript";
        feedbackInitJs.src = "https://js.zohostatic.com/support/fbw_v8/js/zsfeedbackinit.js";
        window.jQueryAndEncoderUrl = "https://js.zohostatic.com/support/app/js/jqueryandencoder.ffa5afd5124fbedceea9.js";
        window.loadingGifUrl = "https://img.zohostatic.com/support/app/images/loading.8f4d3630919d2f98bb85.gif";
        document.head.appendChild(feedbackInitJs);
        var feedbackWidgetCss = document.createElement("link");
        feedbackWidgetCss.setAttribute("rel", "stylesheet");
        feedbackWidgetCss.setAttribute("type", "text/css");
        feedbackWidgetCss.setAttribute("href", "https://css.zohostatic.com/support/app/css/ZSFeedbackPopup.40fce22de60367cedd8b.css");
        document.head.appendChild(feedbackWidgetCss);*/
        <!-- END zohostatic CODE -->
        <?php }
        ?>
    </script>
    <!--<script src="https://desk.zoho.com/portal/api/feedbackwidget/355258000000115286?orgId=681773543&displayType=popout"></script>-->
    <div id="ytWidget"></div>
    <script src="https://translate.yandex.net/website-widget/v1/widget.js?widgetId=ytWidget&pageLang=en&widgetTheme=light&trnslKey=trnsl.1.1.20170324T105515Z.cd22b0b89d711c43.893727e9ddd730237b7eed44a266acbf77c5b81c&autoMode=true"
            type="text/javascript"></script>
    <script>
        //Load All Javascript Variables
        if (localStorage.getItem("session_name") !== null) {
            ISCustomFieldsArr = JSON.parse(getPHPCache('infusionsoft_get_custom_fields', false));
            ContactCustomFields = JSON.parse(getPHPCache('infusionsoft_contact_custom_fields', false));
            UsersCallerIds = <?php echo json_encode(getUsersCallerIds()) ?>;
            if ($("[name=infusionsoftCustomField]").length > 0) {
                var txtSelectHTML = '<option value=""> -- Select One --</option>';
                $.each(ContactCustomFields, function (GroupName, CustomField) {
                    txtSelectHTML += '<optgroup label="' + GroupName + '">';
                    $.each(CustomField, function (Label, Items) {
                        txtSelectHTML += '<option data-subtext="Type: ' + Items.DataType + '" value="' + Items.Name + '" title="' + Items.Label + '">' + Items.Label + '</option>';
                    });
                    txtSelectHTML += '</optgroup>';
                    $("[name=infusionsoftCustomField]").html(txtSelectHTML);
                });
            }
        }


        /* SERVICE WORKER */
        <?php
        if( $this->config->item('MacantaAppName') != 'staging' )
        {
        ?>
        var serviceWorkerRegistration;
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.register('macanta-sw.js')
                .then(function (registration) {
                    console.log('Service Worker registration successful with scope: ', registration.scope);
                    serviceWorkerRegistration = registration;
                    window.addEventListener("keydown", function (event) {
                        if ((event.shiftKey && event.ctrlKey && event.keyCode === 82) || (event.shiftKey && event.metaKey && event.keyCode === 82)) {
                            // 82 = r
                            event.preventDefault();
                            console.log('Updating Serivce Worker!');
                            registration.unregister();
                            caches.delete('macanta-cache');
                            setTimeout(function () {
                                window.location.reload(true);
                            }, 2000);

                        } else if ((event.ctrlKey && event.keyCode === 82) || (event.metaKey && event.keyCode === 82)) {
                            // 82 = r
                            window.location.reload(true);
                        }

                    }, true);
                })
                .catch(function (err) {
                    console.log('Service Worker registration failed: ', err);
                });
        }
        <?php
        }
        ?>
    </script>
    <?php
    //$ShowNewSupportButton = $this->config->item('MacantaAppName') == 'tr410' ? true:false;
    $ShowNewSupportButton = true;
    if ($ShowNewSupportButton) {

?>
    <a class="nb-btn nb-primary icon-btn btn-helpsupport" data-toggle="modal" href="#helpsupport"><i class="fa fa-life-ring"></i>Click Here For Help</a>
    <div class="modal fade helpsupport" id="helpsupport">
        <div class="modal-dialog">
            <form id="supportform"  role="form">
                <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Macanta Help</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div><!-- /.modal-header -->
                <div class="modal-body">
                    <p id="returnmessage"></p>
                    <div class="form-group flex-row">
                        <div class="col-half">
                            <label class="label-sm">Name: <span>*</span></label>
                            <input type="text" name="name" placeholder="Name" class="form-control" required/>
                        </div>
                        <div class="col-half">
                            <label class="label-sm">Email: <span>*</span></label>
                            <input type="email" name="email" placeholder="Email" class="form-control" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="field-group block">
                            <label class="label-sm">Subject: <span>*</span></label>
                            <input type="text" name="subject"  class="form-control" placeholder="If you were sending an email, what would the subject be?" required/>
                        </div>
                        <div class="field-group block">
                            <label class="label-sm">Description:</label>
                            <textarea name="message"  class="form-control"  placeholder="How can we help you? Please describe the issue in as much detail as possible." required></textarea>
                        </div>
                    </div>
                </div><!-- /.modal-body -->
                <div class="modal-footer">
                    <button type="button" class="nb-btn nb-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="nb-btn nb-primary">Submit</button>
                </div><!-- /.modal-footer -->
            </div><!-- /.modal-content -->
            </form>
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <script>
        macantaHelpSupportInit();
    </script>
<?php
}
?>


    </body>
    </html>
<?php
ob_end_flush();
?>
