<div class="tab-menu-admin">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
        <?php
        $replaceArr = array(' ','#','!','@','$','%','&','*','(',')','?',',','.','/','\\',':',';','"',"'",'[',']','-','=','+','|','{','}','<','>');
        foreach($menu as $MenuId => $MenuName){ $MenuId = str_replace($replaceArr,'_',$MenuId); ?>

            <li class="tab<?php echo $MenuId;?> <?php echo $status[$MenuId]? $status[$MenuId]:"";?>">
                <a href="#<?php echo $MenuId;?>" data-toggle="tab" onclick="<?php echo $MenuId;?>(this);"> <?php echo $MenuName;?></a>
            </li>
        <?php }
        ?>

    </ul>
</div>
<div class="tab-body">
    <!-- Tab panes -->
    <div class="tab-content">
        <?php
        foreach($menu as $MenuId => $MenuName ){  $MenuId = str_replace($replaceArr,'_',$MenuId); ?>
        <div class="tab-pane   <?php echo $status[$MenuId]? $status[$MenuId]:"";?> Lazy-<?php echo $MenuId;?>" id="<?php echo $MenuId;?>">
            <div class="adminTabContent target-<?php echo $MenuId;?>" style="min-height: 100px;width: 100%;float: left; ">
                <?php echo $content[$MenuId];?>
            </div>


        </div>        <?php }
    ?>

</div>
</div>
<?php
$ConnectedDataAutomationSettings = macanta_get_connected_data_automations();
$MacantaUsers = macanta_get_users();
$MacantaUserArr = [];
foreach ($MacantaUsers as $MacantaUser){
    $MacantaUserArr[$MacantaUser->Id] = $MacantaUser->FirstName." ".$MacantaUser->LastName;
}
?>
<script>
    MacantaUsers = <?php echo json_encode($MacantaUserArr) ?>;
    ContactGroups = <?php echo json_encode($MacantaUserArr) ?>;
    ConnectorRelationshipArr = <?php echo macanta_get_config('ConnectorRelationship',true); ?>;
    ConnectedDataAutomationSettings = <?php echo json_encode($ConnectedDataAutomationSettings); ?>;
    ConnectorRelationshipArr = <?php echo macanta_get_config('ConnectorRelationship',true); ?>;
</script>