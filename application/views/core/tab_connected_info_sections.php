<?php
 $Parsedown = new Parsedown();
$Sections = [];
$theFields = [];
$USerPermissions = macanta_get_user_permission($session_data['InfusionsoftID']);
foreach ($ConnectorTab['fields'] as $field) {
    $sectionTagId = trim($field['sectionTagId']);
    $GroupName = $sectionTagId == '' ? 'General':$sectionTagId;
    if ($GroupName !== $SectionTitle) continue;
    $theFields[] = $field;

}
echo "<legend>$SectionTitle</legend><div class=\"flex-row sub-group-container\">";
foreach ($theFields as $field) {
    $jsonData = $field['contactspecificField'] == 'yes' ? 'data-jsonvalue="{}"' : '';
    $domId = str_replace("=", "", base64_encode($field['fieldLabel']));
    $required = $field['requiredField'] == 'yes' ? 'required' : "";
    $asterisk = $field['requiredField'] == 'yes' ? '<span style="color: red;">*</span>' : "";
    if($field['helperText'] != '')
    {
        $helperButton = "<span class='helper-btn btnShowPopup' data-help-id='". $domId ."'><i class='fa fa-question-circle' aria-hidden='true'></i></span>";
    }
    else
    {
         $helperButton = '';
    }
    $Helper = trim($field['helperText']) == '' ? '' : "<small id=\"txt" . $domId . "\" class=\"form-text text-muted\">" . $Parsedown->text($field['helperText']) . "</small>";
    $SubGrouped = !empty($field['fieldSubGroup']) ? "SubGrouped" : "";
    $AddDivider = !empty($field['addDivider']) ? $field['addDivider'] : "no";
    $AddDivider = $AddDivider == 'yes' ? 'AddDivider' : '';
    $FieldHeadingTextHTML = '';
    $FieldHeadingText = !empty($field['fieldHeadingText']) ? $field['fieldHeadingText'] : "";
    if (trim($FieldHeadingText) != '') {
        $FieldHeadingTextHTML = '<h3 class="fieldHeadingText">' . $FieldHeadingText . '</h3>';
    }
    $fieldSubGroup = "";
    if (isset($field['fieldSubGroup']) && !empty($field['fieldSubGroup'])) {
        $fieldSubGroup = $field['fieldSubGroup'];
    }
    if (strstr($field['fieldType'], 'field_')) {
        $defaultValue = '';
        $PrimaryFieldData = macanta_get_data_reference_values($field['fieldType']);
        sort($PrimaryFieldData);
        $options = "<option  value>Please Select One</option>";
        foreach ($PrimaryFieldData as $choice) {
            $choiceVal = str_replace(['<', '>', '&'], ['&lt;', '&gt;', '&amp;'], $choice);
            $choiceVal = html_entity_decode($choiceVal);
            if (empty($choiceVal)) continue;
            $selected = trim($field['defaultValue']) == trim($choice) ? "selected" : "";
            $options .= "<option data-value='" . strtolower($choiceVal) . "' value='" . $choiceVal . "' $selected>$choice</option>";
        }
        echo "
									<div class=\"form-group   " . $SubGrouped . " " . $AddDivider . "\"  data-subgroup=\"" . $fieldSubGroup . "\">
										" . $FieldHeadingTextHTML . "
										 <div class=\"label-field-container\"><label class=\"label-sm\" for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                         <i class='fa fa-expand'></i>
										<select    class=\"addGroup select input-select form-control\" 
												id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
												aria-describedby=\"txt" . $domId . "\" 
												data-default=\"" . $defaultValue . "\" 
												name=\"" . $field['fieldId'] . "\"
												" . $jsonData . "
												placeholder=\"" . $field['placeHolder'] . "\" disabled 
												$required>
										  " . $options . "
										</select>

										<small id=\"txt" . $domId . "\" class=\"form-text text-muted\">" . $Parsedown->text($field['helperText']) . "</small>
									 </div></div>									 
									";
    } else {
        switch ($field['fieldType']) {
            case 'Currency':
                echo "
                                        <div class=\"form-group   " . $SubGrouped . " " . $AddDivider . "\" data-subgroup=\"" . $fieldSubGroup . "\">
                                            " . $FieldHeadingTextHTML . "
										 <div class=\"label-field-container\"><label class=\"label-sm\" for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                         <i class='fa fa-expand'></i>
                                            <input  type='number'
                                                    step='any'
                                                    class=\"addGroup input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\"
                                                    disabled 
                                                    $required
                                                    >
                                           <small id=\"txt" . $domId . "\" class=\"form-text text-muted\">" . $Parsedown->text($field['helperText']) . "</small>
                                         </div></div>
                                        ";
                break;

            case 'Text':
                echo "
                                        <div class=\"form-group   " . $SubGrouped . " " . $AddDivider . "\" data-subgroup=\"" . $fieldSubGroup . "\">
                                            " . $FieldHeadingTextHTML . "
										 <div class=\"label-field-container\"><label class=\"label-sm\" for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                         
                                          $helperButton
                                            <input  type=\"text\" 
                                                    class=\"addGroup input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\"
                                                    disabled 
                                                    $required
                                                    >
                                                    <small id=\"txt" . $domId . "\" class=\"form-text text-muted\">" . $Parsedown->text($field['helperText']) . "</small>
                                         </div></div>
                                        ";
                break;

            case 'TextArea':
                echo "
                                        <div class=\"form-group   " . $SubGrouped . " " . $AddDivider . "\" data-subgroup=\"" . $fieldSubGroup . "\">
                                            " . $FieldHeadingTextHTML . "
										 <div class=\"label-field-container\"><label class=\"label-sm\" for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                         $helperButton
                                            <textarea 
                                                    class=\"addGroup input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\" rows=\"3\" 
                                                    disabled  
                                                    $required>" . $field['defaultValue'] . "</textarea>
                                            <small id=\"txt" . $domId . "\" class=\"form-text text-muted\">" . $Parsedown->text($field['helperText']) . "</small>
                                         </div></div>
                                        ";
                break;

            case 'Number':
                echo "
                                        <div class=\"form-group   " . $SubGrouped . " " . $AddDivider . "\" data-subgroup=\"" . $fieldSubGroup . "\">
                                            " . $FieldHeadingTextHTML . "
										 <div class=\"label-field-container\"><label class=\"label-sm\" for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                         $helperButton
                                           <input  type='number'
                                                    class=\"addGroup input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\" disabled 
                                                    $required>
                                            <small id=\"txt" . $domId . "\" class=\"form-text text-muted\">" . $Parsedown->text($field['helperText']) . "</small>
                                         </div></div>
                                        ";
                break;

            case 'Email':
                echo "
                                        <div class=\"form-group   " . $SubGrouped . " " . $AddDivider . "\" data-subgroup=\"" . $fieldSubGroup . "\">
                                            " . $FieldHeadingTextHTML . "
										 <div class=\"label-field-container\"><label class=\"label-sm\" for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                         $helperButton
                                            <input  type='email'
                                                    class=\"addGroup input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\" disabled  
                                                    $required>
                                           <small id=\"txt" . $domId . "\" class=\"form-text text-muted\">" . $Parsedown->text($field['helperText']) . "</small>
                                         </div></div>
                                        ";
                break;

            case 'Password':
                echo "
                                        <div class=\"form-group   " . $SubGrouped . " " . $AddDivider . "\" data-subgroup=\"" . $fieldSubGroup . "\">
                                            " . $FieldHeadingTextHTML . "
										 <div class=\"label-field-container\"><label class=\"label-sm\" for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                           $helperButton
                                            <input  type='password'
                                                    class=\"addGroup input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\" disabled 
                                                    $required>
                                            <small id=\"txt" . $domId . "\" class=\"form-text text-muted\">" . $Parsedown->text($field['helperText']) . "</small>
                                         </div></div>
                                        ";
                break;

            case 'Select':
                $defaultValue = $field['defaultValue'];
                $choices = explode("\r\n", $field['fieldChoices']);
                $options = "<option  value>Please Select One</option>";
                foreach ($choices as $choice) {
                    $choiceVal = str_replace(['<', '>', '&'], ['&lt;', '&gt;', '&amp;'], $choice);
                    $choiceVal = html_entity_decode($choiceVal);
                    $selected = trim($field['defaultValue']) == trim($choice) ? "selected" : "";
                    $options .= "<option data-value='" . strtolower($choiceVal) . "' value='" . $choiceVal . "' $selected>$choice</option>";
                }
                echo "
                                        <div class=\"form-group   " . $SubGrouped . " " . $AddDivider . "\" data-subgroup=\"" . $fieldSubGroup . "\">
                                            " . $FieldHeadingTextHTML . "
										 <div class=\"label-field-container\"><label class=\"label-sm\" for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                           $helperButton
                                            <select    class=\"addGroup select input-select form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    data-default=\"" . $defaultValue . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\" disabled 
                                                    $required>
                                              " . $options . "
                                            </select>
                                            <small id=\"txt" . $domId . "\" class=\"form-text text-muted\">" . $Parsedown->text($field['helperText']) . "</small>
                                         </div></div>
                                         
                                        ";
                break;

            case 'Checkbox':
                $choices = explode("\r\n", $field['fieldChoices']);
                $checkbox = '';
                $count = 0;
                foreach ($choices as $choice) {
                    $FullValue = $choice;
                    $choice = explode(';', $choice);
                    $choice = $choice[0];
                    $choiceVal = str_replace(['<', '>', '&'], ['&lt;', '&gt;', '&amp;'], $choice);
                    $choiceVal = html_entity_decode($choiceVal);
                    $choice = trim($choice);
                    $Checked = $field['defaultValue'] == $choice ? "checked" : "";
                    $count++;
                    $checkbox .= "<div class=\"form-check\">
                                                          <span class=\"form-check-label\">
                                                            <input type=\"checkbox\" 
                                                            class=\"addGroup checkbox form-check-input\" 
                                                            name=\"$field[fieldId]\" 
                                                            id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                            value=\"" . $choiceVal . "\" 
                                                            data-fullvalue=\"" . strtolower($FullValue) . "\" 
                                                            data-value=\"" . strtolower($choiceVal) . "\"
                                                            " . $jsonData . "
                                                             disabled
                                                             >
                                                                $choice
                                                          </span>
                                                        </div>";
                }
                echo "
                                        <div class=\"form-group   " . $SubGrouped . " " . $AddDivider . "\" data-subgroup=\"" . $fieldSubGroup . "\">
                                        " . $FieldHeadingTextHTML . "
										<div class=\"label-field-container\"><label>$field[fieldLabel]</label>
                                        <input type=\"hidden\" name=\"checkbox_" . str_replace('field_', '', $field['fieldId']) . "\" value=\"" . $field['fieldId'] . "\">
                                        $checkbox
                                       <small id=\"txt" . $domId . "\" class=\"form-text text-muted\">" . $Parsedown->text($field['helperText']) . "</small>
                                      </div></div>
                                        ";
                break;

            case 'Radio':
                $choices = explode("\r\n", $field['fieldChoices']);
                $checkbox = '';
                foreach ($choices as $choice) {
                    $FullValue = $choice;
                    $choice = trim($choice);
                    $choice = explode(';', $choice);
                    $choice = $choice[0];
                    $choiceVal = str_replace(['<', '>', '&'], ['&lt;', '&gt;', '&amp;'], $choice);
                    $choiceVal = html_entity_decode($choiceVal);
                    $Checked = $field['defaultValue'] == $choice ? "checked" : "";
                    $checkbox .= "<div class=\"form-check\">
                                                          <span class=\"form-check-label\">
                                                            <input type=\"radio\"
                                                             class=\"addGroup radio form-check-input\" 
                                                             id=\"" . macanta_generate_key('radio_', 5) . "\"
                                                                " . $jsonData . "
                                                            name=\"$field[fieldId]\" id=\"$domId\"   data-fullvalue=\"" . strtolower($FullValue) . "\"  data-value=\"" . strtolower($choiceVal) . "\" value=\"" . $choiceVal . "\" {$Checked} disabled>
                                                                $choice
                                                          </span>
                                                        </div>";
                }
                echo "
                                        <div class=\"form-group   " . $SubGrouped . " " . $AddDivider . "\" data-subgroup=\"" . $fieldSubGroup . "\">
                                        " . $FieldHeadingTextHTML . "
										 <div class=\"label-field-container\"><label class=\"label-sm\">$field[fieldLabel]</label>
                                        $checkbox
                                        <small id=\"txt" . $domId . "\" class=\"form-text text-muted\">" . $Parsedown->text($field['helperText']) . "</small>
                                      </div></div>
                                        ";

                break;

            case 'Date':
                $placeholder = $field['placeHolder'] == "" ? "YYYY-MM-DD" : $field['placeHolder'];
                echo "
                                        <div class=\"form-group   " . $SubGrouped . " " . $AddDivider . "\" data-subgroup=\"" . $fieldSubGroup . "\">
                                            " . $FieldHeadingTextHTML . "
										 <div class=\"label-field-container\"><label class=\"label-sm\" for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                           $helperButton
                                            <input  type='text'
                                                    class=\"addGroup infoDate input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $placeholder . "\" disabled 
                                                    $required>
                                            <small id=\"txt" . $domId . "\" class=\"form-text text-muted\">" . $Parsedown->text($field['helperText']) . "</small>
                                         </div></div>
                                        ";
                break;

            case 'DateTime':
                $placeholder = $field['placeHolder'] == "" ? "YYYY-MM-DD HH:mm:ss" : $field['placeHolder'];
                echo "
                                        <div class=\"form-group   " . $SubGrouped . " " . $AddDivider . "\" data-subgroup=\"" . $fieldSubGroup . "\">
                                            " . $FieldHeadingTextHTML . "
										 <div class=\"label-field-container\"><label class=\"label-sm\" for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . "</label>
                                          $helperButton
                                            <input  type='text'
                                                    class=\"addGroup infoDateTime input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $placeholder . "\" disabled 
                                                    $required>
                                            <small id=\"txt" . $domId . "\" class=\"form-text text-muted\">" . $Parsedown->text($field['helperText']) . "</small>
                                         </div></div>
                                        ";
                break;

            case 'URL':
                echo "
                                        <div class=\"form-group   " . $SubGrouped . " " . $AddDivider . "\" data-subgroup=\"" . $fieldSubGroup . "\">
                                            " . $FieldHeadingTextHTML . "
										 <div class=\"label-field-container\"><label class=\"label-sm\" for=\"" . $domId . "\">" . $field['fieldLabel'] . $asterisk . " <i class=\"fa fa-external-link connected-data-url\" data-field=\"" . $field['fieldId'] . "\" aria-hidden=\"true\"></i></label>
                                            $helperButton
                                            <input  type='url'
                                                    class=\"addGroup input form-control\" 
                                                    id=\"" . macanta_generate_key($domId, $length = 5) . "\" 
                                                    aria-describedby=\"txt" . $domId . "\" 
                                                    name=\"" . $field['fieldId'] . "\"
                                                    value=\"" . $field['defaultValue'] . "\"
                                                    " . $jsonData . "
                                                    placeholder=\"" . $field['placeHolder'] . "\" disabled 
                                                    $required>
                                           <small id=\"txt" . $domId . "\" class=\"form-text text-muted\">" . $Parsedown->text($field['helperText']) . "</small>
                                         </div></div>
                                        ";
                break;

            case 'Repeater':

                break;
        }
    }
}
echo "</div>";
?>
<!-- /.flex-row -->
<script>
    $(document).ready(function () {
        $('.text-muted').hide();
        $("input.infoDate").daterangepicker({
            autoUpdateInput: false,
            locale: {
                format: 'YYYY-MM-DD'
            },
            singleDatePicker: true,
            showDropdowns: true
        }).on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD'));
        });

        $("input.infoDateTime").daterangepicker({
            timePicker: true,
            autoUpdateInput: false,
            locale: {
                format: 'YYYY-MM-DD HH:mm:ss'
            },
            singleDatePicker: true,
            startDate: moment().startOf('hour'),
            endDate: moment().startOf('hour').add(32, 'hour'),
            showDropdowns: true
        }).on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD HH:mm:ss'));
        });

        var fieldsets_<?php echo $ConnectorTab['id'] ?> = $("form.FormUserConnectedInfo[data-guid=<?php echo $ConnectorTab['id'] ?>]").find("fieldset[data-sectionname='<?php echo $SectionTitle ?>']");
        fieldsets_<?php echo $ConnectorTab['id'] ?>.each(function () {
            var theFieldSet = $(this);
            var SubGrouped = theFieldSet.find("div.SubGrouped");
            var SubGroups = [];
            if (SubGrouped.length > 0) {
                SubGrouped.each(function () {
                    var GroupName = $(this).attr('data-subgroup');
                    if (SubGroups.includes(GroupName) === false) {
                        SubGroups.push(GroupName);
                    }

                });
                if (SubGroups.length > 0) {
                    $.each(SubGroups, function (index, theGroupName) {
                        var first = $("div.SubGrouped[data-subgroup='" + theGroupName + "']:first", theFieldSet);
                        if (first.hasClass('AddDivider')) {
                            first.append("<hr class=\"divider\">");
                        }
                        var others = $("div.SubGrouped[data-subgroup='" + theGroupName + "']:not(:first)", theFieldSet);
                        others.appendTo(first).removeClass('SubGrouped').removeClass(' ');
                        var appendedNotLast = $("div.form-group[data-subgroup='" + theGroupName + "']:not(:last)", first);
                        var appendedLast = $("div.form-group[data-subgroup='" + theGroupName + "']:last", first);
                        appendedNotLast.each(function () {
                            if ($(this).hasClass('AddDivider')) {
                                $("<hr class=\"divider\">").insertAfter($(this));
                            }
                        });
                        if (appendedLast.hasClass('AddDivider')) {
                            $("<hr class=\"divider\">").insertAfter(first);
                        }
                        $("<h3 class='SubGroupLabel'>" + theGroupName + "</h3>").prependTo(first);
                    })
                }
            }
        });
        fieldsets_<?php echo $ConnectorTab['id'] ?>.collapsible({
            animation: true,
            speed: "medium"
        });
        $('#MainContent').append("<div id='myModal' class='modal fade helpermodal' role='dialog' aria-hidden='true'><div class='modal-dialog' role='document'><div class='modal-content'><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button><h4 class='modal-title' id='myModalLabel'>Helper Text</h4></div><div class='modal-body'></div><div class='modal-footer'><button type='button' class='btn btn-default' data-dismiss='modal'>Close</button></div></div></div></div>");
        /*Get Active infoItem if theres one*/
        $("table.UserConnectorInfoTable<?php echo $ConnectorTab['id'] ?>").find("tr.activeItem").each(function () {
            var ItemId = $(this).attr('data-itemid');
            var GUID = "<?php echo $ConnectorTab['id'] ?>";
            var ContactConenctedInfoStr = localStorage.getItem(GUID) || {};
            var ContactConenctedInfoObj = JSON.parse(ContactConenctedInfoStr);
            var values = ContactConenctedInfoObj[ItemId]['value'];

            var theForm = fieldsets_<?php echo $ConnectorTab['id'] ?>;

            $(".addGroup", theForm).removeAttr('disabled');

            $.each(values, function (fieldName, fieldValue) {
                fieldValue = fieldValue === null ? "" : fieldValue;
                if (typeof fieldValue === 'object') {


                    if (typeof $("[name='" + fieldName + "']", theForm).attr('data-jsonvalue') !== "undefined") {
                        $("[name='" + fieldName + "']", theForm).attr('data-jsonvalue', JSON.stringify(fieldValue));
                        if (typeof fieldValue["id_" + ContactId] !== 'undefined') {
                            fieldValue = fieldValue["id_" + ContactId];
                        } else {
                            fieldValue = '';
                        }
                    } else {
                        fieldValue = '';
                    }
                } else {
                    //$("[name='"+fieldName+"']",theForm).removeAttr('data-jsonvalue');
                }
                //console.log(fieldName+" : "+fieldValue);

                if (typeof fieldValue === "string") {
                    var fieldValueLowercase = fieldValue.toLowerCase();
                    var htmlDecoded = $("<div />").html(fieldValue).text();
                }

                /* HANDLE INPUT TEXT FIELD */
                $("input.input[type='text'][name='" + fieldName + "']", theForm).val(fieldValue);

                /* HANDLE INPUT URL FIELD */
                $("input.input[type='url'][name='" + fieldName + "']", theForm).val(fieldValue);

                /* HANDLE INPUT EMAIL FIELD */
                $("input.input[type='email'][name='" + fieldName + "']", theForm).val(fieldValue);

                /* HANDLE INPUT PASSWORD FIELD */
                $("input.input[type='password'][name='" + fieldName + "']", theForm).val(fieldValue);

                /* HANDLE INPUT NUMBER FIELD */
                $("input.input[type='number'][name='" + fieldName + "']", theForm).val(fieldValue);

                /* HANDLE TEXTAREA FIELD */
                $("textarea[name='" + fieldName + "']", theForm).val(htmlDecoded);

                /* HANDLE SELECT FIELD */
                $("select[name='" + fieldName + "'] option", theForm).each(function () {
                    $(this).removeAttr('selected');
                    $(this).prop('selected', false)
                });
                $("select[name='" + fieldName + "'] option", theForm).each(function () {
                    var dataValue = $(this).attr('data-value');
                    if (fieldValueLowercase === dataValue) $(this).attr('selected', true);
                    if (fieldValueLowercase === dataValue) $(this).prop('selected', true);
                });
                var aliasesArr;
                var fullValue;
                /* HANDLE INPUT CHECKBOX FIELD */
                $("input.checkbox[name='" + fieldName + "']", theForm).removeAttr('checked');
                if ($("input.checkbox[name='" + fieldName + "']", theForm).length > 0) {
                    var valArr;
                    if (fieldValueLowercase.indexOf("|") >= 0) {
                        valArr = fieldValueLowercase.split("|");
                        $("input.checkbox[name='" + fieldName + "']", theForm).each(function () {
                            fullValue = $(this).attr('data-fullvalue');
                            aliasesArr = fullValue.split(";");
                            $.each(valArr, function (index, value) {
                                $.each(aliasesArr, function (index, alias) {
                                    var aliasValueLowercase = alias.toLowerCase();
                                    if (aliasValueLowercase === value) {
                                        $("input.checkbox[data-value=\"" + aliasesArr[0] + "\"][name='" + fieldName + "']", theForm).prop("checked", true);
                                        return false
                                    }
                                });
                            });
                        });
                    } else if (fieldValueLowercase.indexOf(",") >= 0) {
                        valArr = fieldValueLowercase.split(",");
                        $("input.checkbox[name='" + fieldName + "']", theForm).each(function () {
                            fullValue = $(this).attr('data-fullvalue');
                            aliasesArr = fullValue.split(";");
                            $.each(valArr, function (index, value) {
                                $.each(aliasesArr, function (index, alias) {
                                    var aliasValueLowercase = alias.toLowerCase();
                                    if (aliasValueLowercase === value) {
                                        $("input.checkbox[data-value=\"" + aliasesArr[0] + "\"][name='" + fieldName + "']", theForm).prop("checked", true);
                                        return false
                                    }
                                });
                            });
                        });
                    } else {
                        $("input.checkbox[name='" + fieldName + "']", theForm).each(function () {
                            fullValue = $(this).attr('data-fullvalue');
                            aliasesArr = fullValue.split(";");
                            $.each(aliasesArr, function (index, alias) {
                                var aliasValueLowercase = alias.toLowerCase();
                                if (aliasValueLowercase === fieldValueLowercase) {
                                    $("input.checkbox[data-value=\"" + aliasesArr[0] + "\"][name='" + fieldName + "']", theForm).prop("checked", true);
                                    return false
                                }
                            });
                        });
                    }
                }

                /* HANDLE INPUT RADIO FIELD */
                if ($("input.radio[name=\"" + fieldName + "\"]", theForm).length > 0) {

                    $("input.radio[name=\"" + fieldName + "\"]", theForm).each(function () {
                        fullValue = $(this).attr('data-fullvalue');
                        aliasesArr = fullValue.split(";");
                        $.each(aliasesArr, function (index, alias) {
                            var aliasValueLowercase = alias.toLowerCase();
                            if (aliasValueLowercase === fieldValueLowercase) {
                                $("input.radio[data-value=\"" + aliasesArr[0] + "\"][name='" + fieldName + "']", theForm).prop("checked", true);
                                return false
                            }
                        });
                    });
                }

            });
        });
       verifyTabPermission("<?php echo $ConnectorTab['id'] ?>");
       
    });
    $('.btnShowPopup').click(function(){

       $('#myModal').modal('show');
       var domId = $(this).attr('data-help-id');
       var helperText = $('#txt'+domId).html();
       if(helperText != '')
       {
          $('#myModal').find('.modal-body').html(helperText);
       }
    });
</script>
