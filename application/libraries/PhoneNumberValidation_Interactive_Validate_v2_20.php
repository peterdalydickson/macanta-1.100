<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 16/08/2018
 * Time: 1:39 PM
 */

class PhoneNumberValidation_Interactive_Validate_v2_20
{

    //Credit: Thanks to Stuart Sillitoe (http://stu.so/me) for the original PHP that these samples are based on.

    private $Key; //The key to use to authenticate to the service.
    private $Phone; //The mobile/cell phone number to verify. This must be in international format (+447528471411 or 447528471411) if no country code is provided or national format with a Country parameter provided (07528471411 and GB as the Country parameter).
    private $Country; //The ISO2 country code of the number you are trying to validate (if provided in national format).
    private $Data; //Holds the results of the query

    function __construct($Key, $Phone, $Country)
    {
        $this->Key = $Key;
        $this->Phone = $Phone;
        $this->Country = $Country;
   }

    function MakeRequest()
    {
        $url = "https://api.addressy.com/PhoneNumberValidation/Interactive/Validate/v2.20/xmla.ws?";
        $url .= "&Key=" . urlencode($this->Key);
        $url .= "&Phone=" . urlencode($this->Phone);
        $url .= "&Country=" . urlencode($this->Country);

      //Make the request to Postcode Anywhere and parse the XML returned
      //$file = simplexml_load_file($url);
      $feed = file_get_contents($url);
      $file = simplexml_load_string($feed);
      //Check for an error, if there is one then throw an exception
      if ($file->Columns->Column->attributes()->Name == "Error")
      {
          throw new Exception("[ID] " . $file->Rows->Row->attributes()->Error . " [DESCRIPTION] " . $file->Rows->Row->attributes()->Description . " [CAUSE] " . $file->Rows->Row->attributes()->Cause . " [RESOLUTION] " . $file->Rows->Row->attributes()->Resolution);
      }

      //Copy the data
      if ( !empty($file->Rows) )
      {
          foreach ($file->Rows->Row as $item)
         {
             $this->Data[] = array('PhoneNumber'=>$item->attributes()->PhoneNumber,'RequestProcessed'=>$item->attributes()->RequestProcessed,'IsValid'=>$item->attributes()->IsValid,'NetworkCode'=>$item->attributes()->NetworkCode,'NetworkName'=>$item->attributes()->NetworkName,'NetworkCountry'=>$item->attributes()->NetworkCountry,'NationalFormat'=>$item->attributes()->NationalFormat,'CountryPrefix'=>$item->attributes()->CountryPrefix,'NumberType'=>$item->attributes()->NumberType);
         }
      }
   }

    function HasData()
    {
        if ( !empty($this->Data) )
      {
          return $this->Data;
      }
      return false;
   }

}

//Example usage
//-------------
//$pa = new PhoneNumberValidation_Interactive_Validate_v2_20 ("AA11-AA11-AA11-AA11","+447528471411","GB");
//$pa->MakeRequest();
//if ($pa->HasData())
//{
//   $data = $pa->HasData();
//   foreach ($data as $item)
//   {
//      echo $item["PhoneNumber"] . "&lt;br/>";
//      echo $item["RequestProcessed"] . "&lt;br/>";
//      echo $item["IsValid"] . "&lt;br/>";
//      echo $item["NetworkCode"] . "&lt;br/>";
//      echo $item["NetworkName"] . "&lt;br/>";
//      echo $item["NetworkCountry"] . "&lt;br/>";
//      echo $item["NationalFormat"] . "&lt;br/>";
//      echo $item["CountryPrefix"] . "&lt;br/>";
//      echo $item["NumberType"] . "&lt;br/>";
//   }
//}