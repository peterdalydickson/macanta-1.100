<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 07/03/16
 * Time: 3:00 PM
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Offline extends CI_Controller
{
    //protected $local_stylesheets = array('mystylesheet.css');
    public $Timezone = 'Europe/London';
    public $content = array();
    public $TableMap = [
        "DataFormField" => "InfusionsoftDataFormField",
        "DataFormGroup" => "InfusionsoftDataFormGroup",
        "DataFormTab" => "InfusionsoftDataFormTab",
        "Company" => "InfusionsoftCompany",
        "Contact" => "InfusionsoftContact",
        "ContactAction" => "InfusionsoftContactAction",
        "ContactGroupAssign" => "InfusionsoftContactGroupAssign",
        "ContactGroup" => "InfusionsoftContactGroup",
        "ContactGroupCategory" => "InfusionsoftContactGroupCategory",
        "EmailAddStatus" => "InfusionsoftEmailAddStatus",
        "FileBox" => "InfusionsoftFileBox",
        "Invoice" => "InfusionsoftInvoice",
        "Lead" => "InfusionsoftLead",
        "LeadSource" => "InfusionsoftLeadSource",
        "SavedFilter" => "InfusionsoftSavedFilter",
        "Stage" => "InfusionsoftStage",
        "Template" => "InfusionsoftTemplate",
        "User" => "InfusionsoftUser",
        "EmailSent" => "InfusionsoftEmailSent",
        "StageMove" => "InfusionsoftStageMove",
        //"EmailContent" => "InfusionsoftEmailContent"


    ];
    public $HasLocalIds = ["Company", "Contact", "ContactAction", "ContactGroup"];

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $Params['tables'] = $this->TableMap;
        $this->load->view('macanta-offline', $Params); // return the view
    }

    public function sync($Table, $Params)
    {
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        ini_set('memory_limit', '2048M');
        error_reporting(E_ERROR);
        set_time_limit(0);
        if (!isset($Table)) {
            echo "\nSyncing All Tables";
        } else {
            echo "\nSyncing $Table\n";
            print_r($this->db->list_fields($this->TableMap[$Table]));
        }
        if (!isset($Params)) {
            echo "\nNo Prams";
        } else {
            echo "\n$Params";
        }

    }

    public function monitorSync()
    {
        error_reporting(E_ERROR);
        ini_set('memory_limit', '2048M');
        $query = $this->db->get('SyncProcess');
        $results = [];
        foreach ($this->TableMap as $ISTable => $MacantaTable) {
            $results[$ISTable] = new stdClass();
            $results[$ISTable]->details = 0;
            $results[$ISTable]->count = $this->get_record_count($MacantaTable);
            $results[$ISTable]->local_count = $this->get_record_count($MacantaTable, true);
        }
        if ($query->num_rows() > 0) {

            foreach ($query->result() as $row) {

                $results[$row->TableName]->details = $row;

            }
            return $results;

        } else {

            return $results;
        }
    }

    public function get_record_count($Table, $Local = false)
    {
        $WithOrigin = ['InfusionsoftCompany', 'InfusionsoftContact', 'InfusionsoftContactAction', 'InfusionsoftContactGroup'];
        if ($Local == true && in_array($Table, $WithOrigin)) {
            $this->db->where('Origin', 'Local');
            $this->db->where('Id', 0);
            $query = $this->db->get($Table);
            $num_rows = $query->num_rows();
            unset($query);
            return $num_rows;
        } elseif ($Local == false) {
            $query = $this->db->get($Table);
            $num_rows = $query->num_rows();
            unset($query);
            return $num_rows;
        }
        return 0;

    }

    public function cli_sync($Table,$ContactId=""){
        $ContactId = strpos($ContactId, '.') === false ? $ContactId:"";
        ini_set('display_errors', 1);
        ini_set('memory_limit', '2048M');
        error_reporting(E_ERROR);
        set_time_limit(0);
        if($Table == 'DBPrepare'){
            $app_name = $this->config->item('MacantaAppName');
            echo gmdate('Y-m-d H:i:s')." Preparing $app_name old Database, please wait...\n";
            $Get = "https://".$app_name.".macanta.".EXT."/ajax/adddbconfig";
            $restuls = file_get_contents($Get);
            echo $restuls."\n";
            exit(gmdate('Y-m-d H:i:s')." Preparing $app_name Done! Database Ready To Copy! \n");
        }
        else{
            $Data['table'] = ucfirst($Table);
            $Data['ContactId'] = $ContactId;

            if($ContactId){
                echo gmdate('Y-m-d H:i:s')." Syncing $Table Table For Contact $ContactId, please wait...\n";
            }else{
                echo gmdate('Y-m-d H:i:s')." Syncing $Table Table, please wait...\n";
            }
            $this->initialize_sync($Data);
        }

    }

    public function resync($Id,$AppName,$Groups){
        echo "$Id,$AppName,$Groups";
        infusionsoft_sync_new_contact_from_local($Id,$Groups);
    }
    public function initialize_sync($Data)
    {
        $Table = $Data['table'];
        $ContactId = $Data['ContactId'] ? $Data['ContactId']:'';
        //Check for existing table processing, this make sure no overlapping process
        if($Table == 'ContactGroupAssign' && $ContactId != ''){
            //$this->db->where('TableName', $Table);
            //$query = $this->db->get('SyncProcess');
            //$row = $query->row();
            //$this->db->where('TableName','ContactGroupAssign');
            //$this->db->delete('SyncProcess');
            unset($row);
        }
        if (!isset($row)) {
            $DBData = [
                'TableName' => $Table,
                'TotalItems' => 1,
                'CurrentItem' => 0,
                'Operation' => 'Waiting',
                'Status' => 'Initialising'
            ];
            $HasSyncing = $this->db_exists('SyncProcess', 'TableName', $Table);
            if ($HasSyncing !== false) {
                $this->db->where('TableName', $Table);
                $this->db->update('SyncProcess', $DBData);
            } else {
                $this->db->insert('SyncProcess', $DBData);
            }

            $LastSynced = false;
            $this->db->where('TableName', $Table);
            $row = $this->db->get('SyncRecord')->row();
            if (isset($row)) {
                $LastSynced = $row->LastSynced;
            }

            $TotalItems = (int)$this->get_is_record_count($Table,$ContactId,$LastSynced);
            echo "\033[44D";
            echo 'Total Items Detected: '.str_pad($TotalItems, 7,' ',STR_PAD_LEFT)."              \n";
            echo 'Total Items Recorded: '.str_pad('0', 7,' ',STR_PAD_LEFT);

            //check updates from Infusionsoft
            $this->check_infusionsoft_update($Table,$ContactId,$LastSynced);
            // check updates from local
            //$this->check_local_update($Table); // updates will be directly to infusionsoft
            //export new Items
            $this->check_local_new_record($Table);

            $returnFields = $this->db->list_fields($this->TableMap[$Table]);
            if (in_array('LastUpdated', $returnFields)) {
                $HasSyncRecord = $this->db_exists('SyncRecord', 'TableName', $Table);
                $DBData = ['LastSynced'=>$this->generate_date()];
                if ($HasSyncRecord !== false) {
                    $this->db->where('TableName', $Table);
                    $this->db->update('SyncRecord', $DBData);
                } else {
                    $DBData['TableName'] = $Table;
                    $this->db->insert('SyncRecord', $DBData);
                }
            }

            //sleep(2);
            $this->db->where('TableName', $Table);
            $this->db->delete('SyncProcess');
            gc_collect_cycles();
            exit(gmdate('Y-m-d H:i:s')." Syncing $Table Done!\n\n");
        }
        else{
            exit(gmdate('Y-m-d H:i:s')." Terminated: Sync Already In Progress For $Table\n\n");
        }
    }
    public function get_last_id($Table, $orderBy){
        $last_row=$this->db->select($orderBy)->order_by($orderBy,"DESC")->limit(1)->get($this->TableMap[$Table])->row();
        return isset($last_row) ? $last_row->$orderBy:'0';
    }

    public function get_is_record_count($Table,$ContactId="",$LastSynced=false)
    {
        $action = "query_is";

        if($Table == 'ContactGroupAssign'){
            if(empty($ContactId)){
                $query = '{"GroupId":"%"}';
            }else{
                //Delete All Current Group Assigned
                $this->db->where('ContactId',$ContactId);
                $this->db->delete('InfusionsoftContactGroupAssign');
                $query = '{"ContactId":"'.$ContactId.'"}';

                $this->db->where('ContactId',$ContactId);
            }
            $this->db->order_by('DateCreated', 'DESC');
            $this->db->limit(1);
            $GroupAssign = $this->db->get($this->TableMap[$Table])->row();
            if ($GroupAssign) {
                $LastDateCreatedJson = $GroupAssign->DateCreated;
                $LastDateCreated = json_decode($LastDateCreatedJson);
                if(empty($ContactId)){
                    $query = '{"DateCreated":"~>~ '.$LastDateCreated->date.'"}';
                }else{
                    $query = '{"DateCreated":"~>~ '.$LastDateCreated->date.'","ContactId":"'.$ContactId.'"}';
                }
            }
            $action_details = '{"table":"' . $Table . '","query":'.$query.'}';
            $temp = applyFn('rucksack_request', 'countRecord', $action_details, false);
            if(isset($temp->message)){
                $size = $temp->message;
            }
            else{
                $size = 0;
            }
            $DBData = [
                'TotalItems' => (int)$size
            ];
            echo "\033[44D";
            echo 'Total Items Detected: '.str_pad($size, 7,' ',STR_PAD_LEFT)." please wait..";
            $this->db->where('TableName', $Table);
            $this->db->update('SyncProcess', $DBData);
            return $size;
        }
        elseif($Table == 'EmailSent'){
            $action = "restAllEmailHistory";
            $action_details = '{"Limit":1,"Offset":0}';
            $EmailHistory = applyFn('rucksack_request',$action, $action_details);
            $Emails = $EmailHistory->message;
            $ISCount = $Emails->count;
            $LocalCount  = $this->db->count_all($this->TableMap[$Table]);;
            $size = $ISCount - $LocalCount;
            $DBData = [
                'TotalItems' => $size
            ];
            echo "\033[44D";
            echo 'Total Items Detected: '.str_pad($size, 7,' ',STR_PAD_LEFT)." please wait..";
            $this->db->where('TableName', $Table);
            $this->db->update('SyncProcess', $DBData);
            return $size;
        }
        elseif($Table == 'EmailContent'){
            $EmailContent  = $this->db->count_all($this->TableMap[$Table]);
            $EmailSentCount  = $this->db->count_all('InfusionsoftEmailSent');
            $size = $EmailSentCount - $EmailContent;
            $DBData = [
                'TotalItems' => $size
            ];
            $this->db->where('TableName', $Table);
            $this->db->update('SyncProcess', $DBData);
            echo "\033[44D";
            echo 'Total Items Detected: '.str_pad($size, 7,' ',STR_PAD_LEFT)." please wait..";
            return $size;
        }
        else{
            if($Table == 'DataFormField'){
                $this->db->empty_table('InfusionsoftDataFormField');
            }
            if($Table == 'ContactGroup'){
                $this->db->empty_table('InfusionsoftContactGroup');
            }
            $orderBy = 'Id';
            $last_id = $this->get_last_id($Table, $orderBy);
            $HasSyncRecord = $this->db_exists('SyncRecord', 'TableName', $Table);
            if ($HasSyncRecord !== false) {
                $queryId = '%';
            }else{
                $queryId = '~>~ '.$last_id;
            }
            if(empty($ContactId)){
                $query = '{"Id":"'.$queryId.'"}';
            }elseif($Table != 'Contact'){
                $query = '{"Id":"'.$queryId.'","ContactId":"'.$ContactId.'"}';
            }else{
                $query = '{"Id":"'.$ContactId.'"}';
            }

            $returnFields = array('"Id"');
            if($Table == 'Contact') $returnFields = array('"Id"','"CompanyID"');
            if ($Table == 'Template') {
                $query = '{"Id":"'.$queryId.'","PieceTitle":"~<>~null","Categories":"%"}';
            }
            if ($Table == 'FileBox') {
                if(empty($ContactId)){
                    $query = '{"Id":"'.$queryId.'","ContactId":"~<>~0"}';
                }else{
                    $last_row=$this->db->select('Id')->where('ContactId',$ContactId)->order_by('Id',"DESC")->limit(1)->get('InfusionsoftFileBox')->row();
                    $last_id = isset($last_row) ? $last_row->Id:'0';
                    $queryId = '~>~ '.$last_id;
                    $query = '{"Id":"'.$queryId.'","ContactId":"'.$ContactId.'"}';
                }

            }
        }

        $TableFields = $this->db->list_fields($this->TableMap[$Table]);
        if (in_array('LastUpdated', $TableFields)) {
            if ($LastSynced) {
                if(empty($ContactId)){
                    $query = '{"LastUpdated":"~>~ ' . $LastSynced . '"}';
                }elseif($Table != 'Contact'){
                    $query = '{"LastUpdated":"~>~ ' . $LastSynced . '","ContactId":"'.$ContactId.'"}';
                }else{
                    $query = '{"Id":"'.$ContactId.'"}';
                }
            }
        }
        $returnFieldsStr = implode(', ', $returnFields);
        echo gmdate('Y-m-d H:i:s')." Query:  $query\n";
        $action_details = '{"table":"' . $Table . '","query":'.$query.'}';
        $temp = applyFn('rucksack_request', 'countRecord', $action_details, false);
        if(isset($temp->message)){
            $size = $temp->message;
        }
        else{
            $size = 0;
        }
        $DBData = [
            'TotalItems' => $size
        ];
        echo "\033[44D";
        echo 'Total Items Detected: '.str_pad($size, 7,' ',STR_PAD_LEFT)." please wait..";
        $this->db->where('TableName', $Table);
        $this->db->update('SyncProcess', $DBData);
        return $size;
    }

    public function db_exists($table, $field, $value)
    {
        $this->db->where($field, $value);
        $query = $this->db->get($table);
        if ($query && $query->num_rows() > 0) {

            foreach ($query->result() as $row) {
                return $row;
            }
            return false;
        } else {

            return false;
        }
    }
    public function check_infusionsoft_update($Table,$ContactId="",$LastSynced=false)
    {
        $returnFields = $this->db->list_fields($this->TableMap[$Table]);
        // For tables that has data originated from macanta
        $theKey = array_search('Origin', $returnFields);
        if ($theKey !== false) unset($returnFields[$theKey]);
        //For Filebox Table
        $theKey = array_search('FileData', $returnFields);
        if ($theKey !== false) unset($returnFields[$theKey]);

        //For Contact Table
        $theKey = array_search('CustomField', $returnFields);
        if ($theKey !== false) unset($returnFields[$theKey]);

        //For Template Table
        $theKey = array_search('PieceContent', $returnFields);
        if ($theKey !== false) unset($returnFields[$theKey]);

        //For Template Table
        $theKey = array_search('IdLocal', $returnFields);
        if ($theKey !== false) unset($returnFields[$theKey]);
        $RecordsCount = 1000;
        $page = 0;
        $Recorded = 0;
        $last_Id = 0;
        if($Table != 'ContactGroupAssign' && $Table != 'EmailSent')
            $last_Id = $this->get_last_id($Table, 'Id');
        $Offset = $this->db->count_all($this->TableMap[$Table]);
        while ($RecordsCount > 0) {

            $Records = $this->get_is_record($Table, $returnFields, $page,$last_Id,$Offset,$ContactId,$LastSynced);
            $RecordsCount = sizeof($Records);
            if($Table == 'Contact') $Records = removeCompany($Records);
            $Offset = $Offset + $RecordsCount;
            $this->process_data($Table, $Records, $page ,$Recorded);
            $page++;
            unset($Records);
        }
        echo "\n".'Last Batch Count : '.$RecordsCount."\n";
        unset($Records);// free up memory
        unset($returnFields);
    }
    public function get_is_record($Table, $_returnFields = ['Id'], $page = 0, $last_Id=0, $Offset=0,$ContactId="",$LastSynced=false)
    {
        $action = "query_is";
        $FormIdsArr = [
            'Contact' => -1,
            'Opportunity' => -4,
            'Lead' => -4,
            'Company' => -6,
            'ContactAction' => -5
        ];
        if ($Table == 'ContactGroupAssign') {
            if(empty($ContactId)){
                $query = '{"GroupId":"%"}';
            }else{
                $query = '{"ContactId":"'.$ContactId.'"}';
            }
            $orderBy = 'DateCreated';
            $returnFields = array('"GroupId"');
            if(!empty($ContactId))  $this->db->where('ContactId',$ContactId);
            $this->db->order_by('DateCreated', 'DESC');
            $this->db->limit(1);
            $GroupAssign = $this->db->get($this->TableMap[$Table])->row();
            if ($GroupAssign) {
                $page = 0; // this should be zero when DateCreated is referenced
                $LastDateCreatedJson = $GroupAssign->DateCreated;
                $LastDateCreated = json_decode($LastDateCreatedJson);
                if(empty($ContactId)){
                    $query = '{"DateCreated":"~>~ '.$LastDateCreated->date.'"}';
                }else{
                    $query = '{"DateCreated":"~>~ '.$LastDateCreated->date.'","ContactId":"'.$ContactId.'"}';
                }


            }
            unset($GroupAssign);

        }
        elseif($Table == 'EmailSent'){
            return infusionsoft_get_all_email_history(1000,$Offset);
        }
        else{
            $orderBy = 'Id';
            $HasSyncRecord = $this->db_exists('SyncRecord', 'TableName', $Table);
            if ($HasSyncRecord !== false) {
                $queryId = '%';
            }else{
                $queryId = '~>~ '.$last_Id;
            }

            if(empty($ContactId)){
                $query = '{"'.$orderBy.'":"'.$queryId.'"}';
            }elseif($Table != 'Contact'){
                $query = '{"Id":"'.$queryId.'","ContactId":"'.$ContactId.'"}';
            }else{
                $query = '{"'.$orderBy.'":"'.$ContactId.'"}';
            }
            if ($Table == 'Template') {
                $query = '{"'.$orderBy.'":"'.$queryId.'","PieceTitle":"~<>~null","Categories":"%"}';
            }
            if ($Table == 'FileBox') {

                if(empty($ContactId)){
                    $query = '{"Id":"'.$queryId.'","ContactId":"~<>~0"}';
                }else{
                    $last_row=$this->db->select('Id')->where('ContactId',$ContactId)->order_by('Id',"DESC")->limit(1)->get('InfusionsoftFileBox')->row();
                    $last_id = isset($last_row) ? $last_row->Id:'0';
                    $queryId = '~>~ '.$last_id;
                    $query = '{"Id":"'.$queryId.'","ContactId":"'.$ContactId.'"}';
                }
            }
        }
        $TableFields = $this->db->list_fields($this->TableMap[$Table]);
        if (in_array('LastUpdated', $TableFields)) {
            $orderBy = 'LastUpdated';
            if ($LastSynced) {
                if(empty($ContactId)){
                    $query = '{"LastUpdated":"~>~ ' . $LastSynced . '"}';
                }elseif($Table != 'Contact'){
                    $query = '{"LastUpdated":"~>~ ' . $LastSynced . '","ContactId":"'.$ContactId.'"}';
                }else{
                    $query = '{"Id":"'.$ContactId.'"}';
                }
            }
        }
        $returnFields = [];
        foreach ($_returnFields as $returnField) {
            $returnFields[] = '"' . $returnField . '"';
        }

        if (array_key_exists($Table, $FormIdsArr)) {
            //Lets Add Custom Fields
            $CFObjs = infusionsoft_get_custom_fields("%", true, $FormIdsArr[$Table]);
            foreach ($CFObjs as $CFObj) {
                $returnFields[] = '"_' . $CFObj->Name . '"';
            }
        }
        $returnFieldsStr = implode(', ', $returnFields);

        $action_details = '{"table":"' . $Table . '","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":' . $query . ',"orderby":"' . $orderBy . '","ascending":true}';
        //file_put_contents(dirname(__FILE__)."/log_".$this->config->item('MacantaAppName').".txt",$action_details);
        $temp = applyFn('rucksack_request', $action, $action_details, false);
        $Records = isset($temp->message) ? $temp->message:[];
        unset($CFObjs);
        unset($temp);
        unset($action_details);
        unset($returnFieldsStr);
        return $Records;
    }

    public function process_data($Table, $Records, $page, &$Recorded)
    {
        $stopper = 0;
        if (is_array($Records) && sizeof($Records) > 0) {

            if ($Table == 'ContactGroupAssign' || $Table == 'EmailSent' || $Table == 'EmailContent') {
                //$this->db->truncate($this->TableMap[$Table]);
                $Operation = 'INSERT';
            }
            foreach ($Records as $key => $Record) {
                //$Initiated = $this->db_exists('SyncProcess', 'TableName', $Table);
                $Initiated = true;
                if ($Initiated !== false) {
                    $stopper++;
                    $CurrentItem = ($key + 1) + ($page * 1000);
                    if ($Table != 'ContactGroupAssign' && $Table != 'EmailSent' && $Table != 'EmailContent') $Operation = $this->compare_data_record($Table, $Record);
                    $Record = $this->formatToDB($Record);
                    if(sizeof($Records) == 1) {
                        $HasRecord = $this->db_exists($this->TableMap[$Table], 'Id', $Record->Id);
                        if ($HasRecord !== false) {
                            $Operation = 'UPDATE';
                        } else {
                            $Operation = 'INSERT';
                        }

                    }
                    if ($Table == "FileBox" && $Operation != 'SKIP') {
                        $ObjData = new stdClass();
                        $FileData = infusionsoft_get_file_by_id($Record->Id,false,false)->message;

                        if(!isset($FileData->file_descriptor)) continue;

                        $ObjData->infusionsoft = $FileData->file_descriptor;
                        // Add here the B2 Script
                        if(!empty($FileData)){
                            $ObjData->b2_upload = macanta_b2_upload(
                                $FileData->file_descriptor->file_name,
                                $FileData->file_descriptor->contact_id,
                                "",
                                "FILEBOX",
                                base64_decode($FileData->file_data));

                            //file_put_contents(dirname(__FILE__)."/upload_".$this->config->item('MacantaAppName').".txt",json_encode($ObjData));
                        }
                        $Record->FileData = json_encode($ObjData);

                    }
                    if($Table == "EmailContent"){
                        $Content = infusionsoft_get_email_item($Record->id,false);
                        //print_r($Record);
                        //print_r($Content);
                        $Record->plain_content = $Content->plain_content;
                        $Record->html_content = $Content->html_content;
                    }
                    if ($Table == "Template" && $Operation != 'SKIP') {
                        if ($Record->PieceType == "Email") {
                            $PieceContent = $this->get_email_piece_content($Record->Id);
                            $Record->PieceContent = json_encode($PieceContent);
                        }
                    }
                    $CustomFields = [];
                    foreach ($Record as $FieldName => $FieldValue) {
                        if ($FieldName[0] == "_") {
                            $CustomFields[$FieldName] = html_entity_decode($FieldValue);
                            $CustomFields[$FieldName] = isJson($CustomFields[$FieldName]) ? json_decode($CustomFields[$FieldName]) : $CustomFields[$FieldName];
                            unset($Record->$FieldName);
                        }
                    }
                    if($Table == 'ContactAction' || $Table == 'Lead'  || $Table == 'Opportunity' || $Table == 'ContactAction'){
                        unset($Record->CreatedBy);
                        if($Record->LastUpdatedBy == -1)
                            unset($Record->LastUpdatedBy);

                        $this->db->where('Id', $Record->Id);
                        $query = $this->db->get($this->TableMap[$Table]);
                        $row = $query->row();
                        if($row){
                            $OldCustomFields = json_decode($row->CustomField,true);
                            $NewCustomFields = array_merge($OldCustomFields,$CustomFields);
                            $Record->CustomField = $NewCustomFields;
                        }
                    }
                    switch ($Operation) {
                        case 'UPDATE':
                            // make sure table has custom field column
                            $TableFields = $this->db->list_fields($this->TableMap[$Table]);
                            if ($Table != 'ContactAction' && in_array('CustomField', $TableFields)) $Record->CustomField = json_encode($CustomFields);
                            $this->db->where('Id', $Record->Id);
                            $this->db->update($this->TableMap[$Table], $Record);
                            $Recorded++;
                            echo "\033[30D";
                            echo 'Total Items Recorded: '.str_pad($Recorded, 7,' ',STR_PAD_LEFT);
                            break;
                        case 'INSERT':
                            //$local_id = $this->generate_local_id($this->TableMap[$Table]);
                            if (in_array($Table, $this->HasLocalIds))
                                $Record->IdLocal = $Record->Id;

                            // make sure table has custom field column
                            $TableFields = $this->db->list_fields($this->TableMap[$Table]);
                            if ($Table != 'ContactAction' && in_array('CustomField', $TableFields)) $Record->CustomField = json_encode($CustomFields);
                            $this->db->insert($this->TableMap[$Table], $Record);
                            $Recorded++;
                            echo "\033[30D";
                            echo 'Total Items Recorded: '.str_pad($Recorded, 7,' ',STR_PAD_LEFT);
                            break;
                        case 'SKIP':
                            $Recorded++;
                            echo "\033[30D";
                            echo 'Total Items Recorded: '.str_pad($Recorded, 7,' ',STR_PAD_LEFT);
                            break;
                        default:
                            break;
                    }
                    $DBData = [
                        'CurrentItem' => $CurrentItem,
                        'Operation' => $Operation,
                        'Status' => 'Running'
                    ];

                    $HasSyncing = $this->db_exists('SyncProcess', 'TableName', $Table);
                    if ($HasSyncing !== false) {
                        $this->db->where('TableName', $Table);
                        $this->db->update('SyncProcess', $DBData);
                    } else {
                        $DBData['TableName'] = $Table;
                        $this->db->insert('SyncProcess', $DBData);
                    }

                    if(isset($Record->LastUpdated)){
                        $LastSynced = json_decode($Record->LastUpdated);
                        $DBData = [
                            'LastSynced' => date("Y-m-d H:i:s", strtotime($LastSynced->date))
                        ];
                        $HasRecord = $this->db_exists('SyncRecord', 'TableName', $Table);
                        if ($HasRecord !== false) {
                            $this->db->where('TableName', $Table);
                            $this->db->update('SyncRecord', $DBData);
                        } else {
                            $DBData['TableName'] = $Table;
                            $this->db->insert('SyncRecord', $DBData);
                        }
                    }




                    if ($stopper == 20) {
                        $stopper = 0;
                        //sleep(1);
                    }
                } else {
                    return false;
                }
            }
        } else {
            $DBData = [
                'CurrentItem' => 0,
                'Operation' => 'No Updates',
                'Status' => 'Running'
            ];
            $HasSyncing = $this->db_exists('SyncProcess', 'TableName', $Table);
            if ($HasSyncing !== false) {
                $this->db->where('TableName', $Table);
                $this->db->update('SyncProcess', $DBData);
            } else {
                $DBData['TableName'] = $Table;
                $this->db->insert('SyncProcess', $DBData);
            }



            return false;
        }
    }

    public function compare_data_record($Table, $Record)
    {
        // $Operation can be UPDATE,INSERT,IS_UPDATE,IS_INSERT
        $FoundRecord = $this->db_exists($this->TableMap[$Table], 'Id', $Record->Id);
        if ($FoundRecord !== false) {
            if (!empty($Record->LastUpdated)) {
                $ISLastUpdated = $Record->LastUpdated->date;
                $ISTimeZone = $Record->LastUpdated->timezone;
                $LastUpdated = json_decode($FoundRecord->LastUpdated);
                if(!$LastUpdated) return 'UPDATE';
                $MacantaLastUpdated = $LastUpdated->date;
                $MacantaTimeZone = $LastUpdated->timezone;
                $ISDate = new DateTime($ISLastUpdated, new DateTimeZone($ISTimeZone));
                $MacantaDate = new DateTime($MacantaLastUpdated, new DateTimeZone($MacantaTimeZone));
                //$MacantaDate->setTimezone(new DateTimeZone($ISTimeZone));
                if ($ISDate > $MacantaDate) {
                    $Operation = 'UPDATE';
                } elseif ($ISDate == $MacantaDate) {
                    $Operation = 'SKIP';
                } elseif ($ISDate < $MacantaDate) {
                    $Operation = 'IS_UPDATE';
                }
            } else {
                $Operation = 'UPDATE';
                if($Table == 'FileBox') $Operation = 'SKIP';
            }

        } else {
            $Operation = 'INSERT';
        }
        return $Operation;
    }

    public function formatToDB($Data)
    {
        $Formated = new stdClass();
        foreach ($Data as $FieldName => $FieldValue) {
            $FieldValue = is_object($FieldValue) ? json_encode($FieldValue) : is_array($FieldValue) ? json_encode($FieldValue) : $FieldValue;
            $Formated->$FieldName = $FieldValue;

        }
        return $Formated;
    }

    public function get_email_piece_content($Id)
    {
        $action = "email_template_is";
        $action_details = '{"Id":"' . (string)$Id . '"}';
        $results = applyFn('rucksack_request', $action, $action_details, false);
        $Templates = $results->message;
        return $Templates;
    }

    public function  check_local_update($Table)
    {
        $Operation = 'IS_UPDATE';
        $stopper = 0;
        $TableFields = $this->db->list_fields($this->TableMap[$Table]);
        if (in_array('LastUpdated', $TableFields)) {
            // Get Last Sync Date
            $HasSyncRecord = $this->db_exists('SyncRecord', 'TableName', $Table);
            if ($HasSyncRecord !== false) {
                $this->db->where('TableName', $Table);
                $row = $this->db->get('SyncRecord')->row();
                if (isset($row)) {
                    $LastSynced = $row->LastSynced;
                }

            } else {
                $LastSynced = false;
            }
            // Get All Record to compare from Lastsync date
            $ContactsToBeUpdated = [];
            $this->db->where('Id !=', 0);
            $query = $this->db->get($this->TableMap[$Table]);
            foreach ($query->result() as $row) {
                $LastUpdated = json_decode($row->LastUpdated);
                if ($LastSynced != false && strtotime($LastUpdated->date) > strtotime($LastSynced))
                    $ContactsToBeUpdated[] = $row;

            }
            $DBData = [
                'TotalItems' => sizeof($ContactsToBeUpdated),
                'CurrentItem' => 0,
                'Operation' => $Operation,
                'Status' => 'Running'
            ];
            $this->db->where('TableName', $Table);
            $this->db->update('SyncProcess', $DBData);
            if (sizeof($ContactsToBeUpdated) > 0) {
                foreach ($ContactsToBeUpdated as $key => $Record) {
                    $CurrentItem = $key + 1;
                    $Initiated = $this->db_exists('SyncProcess', 'TableName', $Table);
                    if ($Initiated !== false) {
                        $stopper++;
                        $this->db->where('TableName', $Table);
                        $DBData = [
                            'CurrentItem' => $CurrentItem,
                        ];
                        $this->db->update('SyncProcess', $DBData);
                        $this->IS_Update($Table, $Record);
                        if ($stopper == 5) {
                            $stopper = 0;
                           // sleep(1);
                        }
                    }
                }
            } else {
                $this->db->where('TableName', $Table);
                $DBData = [
                    'CurrentItem' => 0,
                    'Operation' => 'No Updates',
                    'Status' => 'Running'
                ];
                $this->db->update('SyncProcess', $DBData);
            }
        }
    }

    public function IS_Update($Table, $Record)
    {
        //Id, CustomField in Contact Table should not be included to get custom field values
        $fields_to_remove = [
            'Id',
            'Origin',
            'FileData',
            'PieceContent',
            'CreatedBy',
            'DateCreated',
            'Groups',
            'CustomField',
            'LastUpdated',
            'LastUpdatedBy',
            'Validated',
            'ObjectType',
            'IdLocal'
        ];
        $tables_to_import = [
            'Company',
            'Contact',
            'ContactAction',
            'ContactGroup',
        ];
        $DateFields = ['CreationDate', 'LastUpdated', 'EndDate', 'CompletionDate', 'Birthday', 'DateCreated'];
        $NoteCustomFields = json_decode($this->config->item('NoteCustomFields'), true);
        $CF_MacantaNotes = '_' . $NoteCustomFields['JSON'];
        $action = "update_is";
        if (in_array($Table, $tables_to_import)) {
            $this->db->where('Id', $Record->Id);
            $query = $this->db->get($this->TableMap[$Table]);
            if ($query->num_rows() > 0) {

                foreach ($query->result() as $row) {
                    $DBData = [];
                    $IdLocal = $row->IdLocal;
                    $fields = [];
                    $Groups = '';
                    foreach ($row as $FieldName => $FiledValue) {
                        if (trim($FiledValue) == '') continue;
                        if ($FieldName == 'Groups') $Groups = $FiledValue;
                        if (in_array($FieldName, $fields_to_remove)) continue;
                        if (in_array($FieldName, $DateFields)) {
                            $FiledValue = json_decode($FiledValue);
                            $FiledValue = $FiledValue->date;
                        }
                        $fields[] = '"' . $FieldName . '":' . json_encode($FiledValue);
                    }
                    //Add CustomFields Value if any
                    if (isset($row->CustomField) && trim($row->CustomField) != '') {
                        $CustomFields = json_decode($row->CustomField);
                        foreach ($CustomFields as $CustomFieldName => $CustomFieldValue) {
                            if ($CustomFieldName == $CF_MacantaNotes) {
                                $CustomFieldValue = json_encode($CustomFieldValue);
                            }
                            $fields[] = '"' . $CustomFieldName . '":' . json_encode($CustomFieldValue);
                        }
                    }
                    $fields = '{' . implode(',', $fields) . '}';
                    $action_details = '{"table":"' . $Table . '","id":"' . $Record->Id . '","fields":' . $fields . '}';
                    $result = applyFn('rucksack_request', $action, $action_details, false);
                    //file_put_contents(dirname(__FILE__) . "/UpdateResults_".$this->config->item('MacantaAppName').".txt", $action_details . "\n" . json_encode($result) . "\n\n", FILE_APPEND);
                    if ($Table == 'Contact') {
                        $GroupsArr = explode(',', $Groups);
                        $GroupsArrOld = isset($Record->Groups) ? explode(',', $Record->Groups) : [];
                        foreach ($GroupsArr as $GroupId) {
                            if (!in_array($GroupId, $GroupsArrOld)) {
                                $action_details = '{"tag":' . $GroupId . ',"id":' . $Record->Id . '}';
                                applyFn('rucksack_request', 'apply_tag', $action_details, false);
                            }
                        }
                        foreach ($GroupsArrOld as $GroupIdOld) {
                            if (!in_array($GroupIdOld, $GroupsArr)) {
                                $action_details = '{"tag":' . $GroupIdOld . ',"id":' . $Record->Id . '}';
                                $Tags = applyFn('rucksack_request', 'removeTag', $action_details, false);
                            }

                        }
                    }


                }
            }
        }
    }

    public function check_local_new_record($Table)
    {
        //Id, CustomField in Contact Table should not be included to get custom field values
        $Operation = 'IS_ADD_RECORD';
        $stopper = 0;
        $fields_to_remove = [
            'Id',
            'Origin',
            'FileData',
            'PieceContent',
            'CreatedBy',
            'DateCreated',
            'Groups',
            'CustomField',
            'LastUpdated',
            'LastUpdatedBy',
            'Validated',
            'ObjectType',
            'IdLocal'
        ];
        $tables_to_import = [
            'Company',
            'Contact',
            'ContactAction',
            'ContactGroup',
        ];
        $DateFields = ['CreationDate', 'LastUpdated', 'EndDate', 'CompletionDate', 'Birthday', 'DateCreated','ActionDate'];
        if ($this->config->item('NoteCustomFields')) {
            $NoteCustomFields = json_decode($this->config->item('NoteCustomFields'), true);
            $CF_MacantaNotes = '_' . $NoteCustomFields['JSON'];
        }else{
            $CF_MacantaNotes = '_JSON';
        }
        $action = "create_is";
        if (in_array($Table, $tables_to_import)) {
            $this->db->where('Origin', 'Local');
            $this->db->where('Id', 0);
            $query = $this->db->get($this->TableMap[$Table]);
            if ($query->num_rows() > 0) {

                $DBData = [
                    'TotalItems' => $query->num_rows(),
                    'CurrentItem' => 0,
                    'Operation' => $Operation,
                    'Status' => 'Running'
                ];
                $this->db->where('TableName', $Table);
                $this->db->update('SyncProcess', $DBData);

                foreach ($query->result() as $key => $row) {
                    $CurrentItem = $key + 1;
                    $Initiated = $this->db_exists('SyncProcess', 'TableName', $Table);
                    if ($Initiated !== false) {
                        $stopper++;
                        $this->db->where('TableName', $Table);
                        $DBData = [
                            'CurrentItem' => $CurrentItem,
                        ];
                        $this->db->update('SyncProcess', $DBData);
                        $DBData = [];
                        $IdLocal = $row->IdLocal;
                        $fields = [];
                        $Groups = '';
                        foreach ($row as $FieldName => $FiledValue) {
                            if (trim($FiledValue) == '') continue;
                            if ($FieldName == 'Groups') $Groups = $FiledValue;
                            if (in_array($FieldName, $fields_to_remove)) continue;
                            if (in_array($FieldName, $DateFields)) {
                                $FiledValue = json_decode($FiledValue);
                                $FiledValue = $FiledValue->date;
                                if($FieldName == 'ActionDate') $FiledValue = infuDate($FiledValue);
                            }
                            $fields[] = '"' . $FieldName . '":' . json_encode($FiledValue);
                        }
                        //Add CustomFields Value if any
                        if ($Table != 'ContactAction' && isset($row->CustomField) && trim($row->CustomField) != '') {
                            $CustomFields = json_decode($row->CustomField);
                            foreach ($CustomFields as $CustomFieldName => $CustomFieldValue) {
                                if ($CustomFieldName == $CF_MacantaNotes) {
                                    $CustomFieldValue = json_encode($CustomFieldValue);
                                }
                                $fields[] = '"' . $CustomFieldName . '":' . json_encode($CustomFieldValue);
                            }
                        }
                        $fields = '{' . implode(',', $fields) . '}';
                        $action_details = '{"table":"' . $Table . '","fields":' . $fields . '}';
                        $result = applyFn('rucksack_request', $action, $action_details, false);
                        //file_put_contents(dirname(__FILE__) . "/ExportResults_".$this->config->item('MacantaAppName').".txt", $action_details . "\n" . json_encode($result) . "\n\n", FILE_APPEND);
                        $Id = $result->message;
                        $DBData['Id'] = $Id;
                        $DBData['IdLocal'] = $Id;
                        $this->db->where('IdLocal', $IdLocal);
                        $this->db->update($this->TableMap[$Table], $DBData);
                        if (is_numeric($Id)) {
                            //if $Table is Contact Check Groups and apply manually
                            if ($Table == 'Contact') {
                                $GroupsArr = explode(',', $Groups);
                                foreach ($GroupsArr as $GroupId) {
                                    $action_details = '{"tag":' . $GroupId . ',"id":' . $Id . '}';
                                    applyFn('rucksack_request', 'apply_tag', $action_details, false);
                                }
                            }
                        }
                        if ($stopper == 5) {
                            $stopper = 0;
                           // sleep(1);
                        }
                    }
                }
            }
        }
    }

    public function generate_date(){
        $defaultTimeZone = $this->Timezone;
        if (date_default_timezone_get() != $defaultTimeZone) date_default_timezone_set($defaultTimeZone);
        $UTC_date = gmdate('Y-m-d H:i:s');
        $UTC_date = date('Y-m-d H:i:s', strtotime($UTC_date) - 18000);
        return $UTC_date;
    }

    public function abort_sync($Table)
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $this->db->where('TableName', $Table);
        $this->db->delete('SyncProcess');
        echo "$Table Sync Aborted";
    }

    public function get_contact_custom_fields_value($ContactId)
    {
        //Lets Add Custom Fields
        $returnFields = [];
        $CFObjs = infusionsoft_get_custom_fields("%", true, -1);
        foreach ($CFObjs as $CFObj) {
            $returnFields[] = '"_' . $CFObj->Name . '"';
        }
        $returnFieldsStr = implode(', ', $returnFields);
        $action_details = '{"table":"Contact","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"Id":"' . $ContactId . '"}}';
        $action = "query_is";
        $ContactArr = applyFn('rucksack_request', $action, $action_details, false);
        return $ContactArr->message[0];
    }
    public function countISRecord($Table){
        $query = '{"Id":"%"}';
        if($Table == 'ContactGroupAssign'){
            $query = '{"GroupId":"%"}';
            $this->db->order_by('DateCreated', 'DESC');
            $this->db->limit(1);
            $GroupAssign = $this->db->get($this->TableMap[$Table])->row();

            if (isset($GroupAssign)) {
                $LastDateCreatedJson = $GroupAssign->DateCreated;
                $LastDateCreated = json_decode($LastDateCreatedJson);
                $query = '{"DateCreated":"~>~ '.$LastDateCreated->date.'"}';
            }
        }
        $action = "countRecord";
        $action_details = '{"table":"'.$Table.'","query":'.$query;
        //echo $action_details;
        $Records = rucksack_request($action, $action_details);
        //print_r($Records);
        return $Records->message;
    }
    public function generate_local_id($Table)
    {
        $LastId = $this->db->select_max('IdLocal')->get($Table)->row();
        return (int)$LastId->Id + 1;
    }
    public function time_check(){
        echo $this->generate_date();
    }

}
