<?php
$MacantaUsers = macanta_get_user_data();
$MacantaUserPermissionTempalte = macanta_get_usertemplate_data();
$Macantamqbinfo = get_macanta_cd_query_data();
$MacantacustomTabinfo = macanta_get_config('custom_tabs');
$MacantaconnectorTabinfo = macanta_get_config('connected_info');
//tabdataobject array create
if (!empty($MacantacustomTabinfo)) {
    $Tabinfo = json_decode($MacantacustomTabinfo, true);
    $tabcustomdata = array();
    foreach ($Tabinfo as $key => $tabcustom) {
        $tabcustomdata[$key] = $tabcustom['title'];
    }
}
if (!empty($MacantaconnectorTabinfo)) {
    $Tabconnectorinfo = json_decode($MacantaconnectorTabinfo, true);
    $tabconnectordata = array();
    foreach ($Tabconnectorinfo as $key => $tabconnector) {
        $tabconnectordata[$key] = $tabconnector['title'];
    }
}
$Tabaccess = array_merge($tabcustomdata, $tabconnectordata);
//tabdataobject array create
//sectiondataobject array create
if (!empty($MacantaconnectorTabinfo)) {
    $CdCustomTabs = json_decode($MacantaconnectorTabinfo, true);

    $sectionaccess = array();

    foreach($CdCustomTabs as $key=>$Customtab)
    {
      
      	foreach($Customtab['fields'] as $field)
      	 {
      	 	if($Customtab['title'] != '')
      	 	{
      	 	  if($field['sectionTagId'] != '')
      	 	  {

      	 	  	if(!in_array($field['sectionTagId'],$sectionaccess[$key][$Customtab['title']]))
      	 	  	{
      	 	  	  $sectionaccess[$key][$Customtab['title']][] = $field['sectionTagId'];	
      	 	  	}
      	 	  	
      	 	  }
      	 	  
      	 	}
      	 }
   }
}
//sectiondataobject array create
//MQBsection array create
if (!empty($Macantamqbinfo)) {
    $mqbaccess = array();
    foreach ($Macantamqbinfo as $mqbinfo) {
        $mqbaccess[$mqbinfo->queryId] = $mqbinfo->queryName;
    }
}
//MQBsection array create
//echo '<pre>'; print_r($sectionaccess); exit;
?>
<small class="dev-only php">tab_admin_user_management.php</small>
<div class="theNotePanel">
    <div class="mainbox-top">
        <div class="tab-panel">
            <div class="panel panel-primary left-ConnectedInfos">
                <div class="panel-heading">

                    <h3 class="panel-title "><i class="fa fa-cogs"></i> User Management</h3>
                </div>
                <div class="panel-body admin-panelBody UsermanagementContainer">
                    <div class="UsermanagementListContainer">
                        <ul class="nav nav-tabs itemContainer UsermanagementList">
                            <li class="form-group-item ui-sortable-handle active">
                                <div class="bullet-item">
                                    <a href="#Users" data-toggle="tab" aria-expanded="false">
                                        <h3 class="UsermanagementListTitle" data-content="UserGroups">
                                            <i class="fa fa-cubes"></i>
                                            Users
                                        </h3>
                                    </a>

                                </div>
                            </li>
                            <li class="form-group-item ui-sortable-handle">
                                <div class="bullet-item">
                                    <a href="#UserPermissionTemplates" data-toggle="tab" aria-expanded="false">
                                        <h3 class="UsermanagementListTitle" data-content="TriggerConditions">
                                            <i class="fa fa-microchip"></i>
                                            User Permission Templates
                                        </h3>
                                    </a>

                                </div>
                            </li>
                            <li class="form-group-item ui-sortable-handle">
                                <div class="bullet-item">
                                    <a href="#ContactViewPermission" data-toggle="tab" aria-expanded="false">
                                        <h3 class="UsermanagementListTitle" data-content="TriggerActions">
                                            <i class="fa fa-cog"></i>
                                            Contact View Permission
                                        </h3>
                                    </a>

                                </div>
                            </li>
                            <li class="form-group-item ui-sortable-handle hide">
                                <div class="bullet-item">
                                    <a href="#CallSettings" data-toggle="tab" aria-expanded="false">
                                        <h3 class="UsermanagementListTitle" data-content="ActionWebhooks">
                                            <i class="fa fa-id-card"></i>
                                            Call Settings
                                        </h3>
                                    </a>

                                </div>
                            </li>
                        </ul>
                        <div class="tab-body UsermanagementContainer">
                            <div class="tab-content">
                            	<!--modal for add new permissiontemplate-->
			                    <div id="AddTemplateModal" class="modal fade" role="dialog" aria-hidden="true">
								  <div class="modal-dialog modal-lg">
									  <!-- Modal content-->
									  <div class="modal-content">
										 <div class="modal-header">
										 	<h3 class="modal-title">New User Permission Template</h3>
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									      </div>
										  <div class="modal-body"> 
										  	<form method="post">
										  	<div id="MacantaTemplateError"></div>
										  	<div class="flex-row">
											  	  <div class="form-group">
		                            				   <label class="label-lg">Template Name</label>
		                            				   <input type="text" class="form-control col-md-6 TemplateName" name="TemplateName" id="TemplateName">
		                            				   <span id="templateNameError"></span>
	                            				  </div>
	                            				  <div class="form-group">
		                            				   <label class="label-lg">Template Description</label>
		                            				   <textarea name="TemplateDesc" class="form-control col-md-6 TemplateDesc" id="TemplateDesc"></textarea>
	                            				  </div>
                            				</div>
										  	  <ul class="nav nav-tabs itemContainer UsermanagementList">
					                            <li class="form-group-item ui-sortable-handle active">
					                                <div class="bullet-item">
					                                    <a href="#TabAccess" data-toggle="tab" aria-expanded="false">
					                                        <h3 class="UsermanagementListTitle" data-content="UserGroups">
					                                            Tab Access
					                                        </h3>
					                                    </a>

					                                </div>
					                            </li>
					                            <li class="form-group-item ui-sortable-handle">
					                                <div class="bullet-item">
					                                    <a href="#SectionAccess" data-toggle="tab" aria-expanded="false">
					                                        <h3 class="UsermanagementListTitle" data-content="SectionAccess">
					                                           Data Object Section Access
					                                        </h3>
					                                    </a>
					                                </div>
					                            </li>
					                            <li class="form-group-item ui-sortable-handle">
					                                <div class="bullet-item">
					                                    <a href="#MQBAccess" data-toggle="tab" aria-expanded="false">
					                                        <h3 class="UsermanagementListTitle" data-content="MQBAccess">
					                                           MQB Access
					                                        </h3>
					                                    </a>
					                                </div>
					                            </li>
					                      </ul>

					                      <div class="tab-body UsermanagementContainer">
                            				<div class="tab-content">
                            				  <!-- Tab access -->
                                			  <div id="TabAccess" class="tab-pane active UsermanagementContent ">
                                			  	<div class="panel-body admin-panelBody DynamicTabContainer "
								                     data-key="TabAccess"
								                     data-label="Tab Access"
								                     data-template="TabAccessContentTemplate"
								                     data-dbprefix="sa">
												  <table class="table table-hover table-striped TabAccessTable" width="100%;" style="width: 100%;">
											       <thead>
											       	   <tr>
								                            <th>
								                               <label></label>
								                            </th>
															<th>
								                               <label>Read-Only Access</label>
								                            </th>
								                            <th>
								                               <label>Read/write Access</label>	 
								                            </th>
								                            <th>
								                               <label>Global Access</label> 
								                            </th>
								                        </tr>
											          <tr> 
											         	<th><label>Data Object Title</label></th>
											         	<th><label>Tab Access</label></th>
											         	<th></th>
											         	<th></th>
													 </tr>
											       </thead>	
											       <tbody> 
										           <?php
												   foreach($Tabaccess as $key=>$data)
												   {
												    ?>
												     <tr class="TabAccessItem" data-section-name="<?php echo $data; ?>" data-tab-id="<?php echo $key; ?>">
											         	<td><label><?php echo $data; ?></label></td>
											         	<td>
											         	<div data-state="inactive" class="ItemStatus toggle-iphone"></div>
									                     <input type="hidden" name="Tabreadonly" value="inactive" class="Tabreadonly">
									                    </td>
											         	<td><div data-state="inactive" class="ItemStatus toggle-iphone"></div>
									                      <input type="hidden" name="Tabreadwrite" value="inactive" class="Tabreadwrite"></td>
											         	<td><div data-state="inactive" class="ItemStatus toggle-iphone"></div>
									                      <input type="hidden" name="Tabglobal" value="inactive" class="Tabglobal"></td>
										         	    </tr>
										         	 <?php
										         	 }
										         	 ?>
										         	 </tbody>
										             </table>
										             </div>
													<div class="save-btn-container">
										            <button type="button" class="nb-btn nb-primary icon-btn saveTabAccess" data-contact-id="" data-template-id=""><i class="fa fa-save"></i> Save <span class="TabButtonLabel"> Tab Access</span></button>
										       		</div>
										       		
											  </div>
											  <!-- Tab access -->
											<!-- Section Access -->
                                			<div id="SectionAccess" class="tab-pane UsermanagementContent ">
                                			<?php     
								            foreach($sectionaccess as $key=>$sectiondata)
											{
											  foreach($sectiondata as $k=>$section)
											  {
											  ?>
												  <div class="panel-body admin-panelBody DynamicTabContainer "
								                     data-key="SectionAccess"
								                     data-label="Section Access"
								                     data-template="SectionAccessContentTemplate"
								                     data-dbprefix="sa">
												  <table class="table table-hover SectionAccessTable" width="100%;" style="width: 100%;">
											       <thead>
											       	   <tr>
								                            <th>
								                               <label></label>
								                            </th>
															<th>
								                               <label>Read-Only Access</label>
								                            </th>
								                            <th>
								                               <label>Read/write Access</label>	 
								                            </th>
								                            <th>
								                               <label>Global Access</label> 
								                            </th>
								                        </tr>
											          <tr> 
											         	<th><label>Data Object Title</label></th>
											         	<th><label><?php echo $k; ?></label></th>
											         	<th></th>
											         	<th></th>
													 </tr>
											       </thead>	
											       <tbody> 
										           <?php
												   foreach($section as $data)
												   {
												    ?>
												     <tr class="SectionAccessItem" data-section-name="<?php echo $data; ?>" data-tab-id="<?php echo $key; ?>">
											         	<td><label><?php echo $data; ?></label></td>
											         	<td>
											         	<div data-state="inactive" class="ItemStatus toggle-iphone"></div>
									                     <input type="hidden" name="Sectionreadonly" value="inactive" class="Sectionreadonly">
									                    </td>
											         	<td><div data-state="inactive" class="ItemStatus toggle-iphone"></div>
									                      <input type="hidden" name="Sectionreadwrite" value="inactive" class="Sectionreadwrite"></td>
											         	<td><div data-state="inactive" class="ItemStatus toggle-iphone"></div>
									                      <input type="hidden" name="Sectionglobal" value="inactive" class="Sectionglobal"></td>
										         	    </tr>
										         	 <?php
										         	 }
										         	 ?>
										         	 </tbody>
										             </table>
										             </div>
													<div class="save-btn-container">
										            <button type="button" class="nb-btn nb-primary icon-btn saveSectionAccess" data-contact-id="" data-template-id=""><i class="fa fa-save"></i> Save <span class="TabButtonLabel">  Section Access</span></button>
										       		</div>
										       		<?php
										            }
										        }
								            ?>
								            </div>
                                			<!-- Section Access -->
                                		
                                			<!-- MQB Access -->
                                			<div id="MQBAccess" class="tab-pane  UsermanagementContent ">
                                			
												  <div class="panel-body admin-panelBody DynamicTabContainer "
								                     data-key="MQBAccess"
								                     data-label="MQB Access"
								                     data-template="MQBAccessContentTemplate"
								                     data-dbprefix="sa">
												  <table class="table table-hover MQBAccessTable" width="100%;" style="width: 100%;">
											       <thead>
											       	   <tr>
								                            <th>
								                               <label></label>
								                            </th>
															<th>
								                               <label>Read-Only Access</label>
								                            </th>
								                            <th>
								                               <label>Read/write Access</label>	 
								                            </th>
								                            <th>
								                               <label>Global Access</label> 
								                            </th>
								                        </tr>
											          <tr> 
											         	<th><label>Data Object Title</label></th>
											         	<th><label>MQB Access</label></th>
											         	<th></th>
											         	<th></th>
													 </tr>
											       </thead>	
											       <tbody> 
										           <?php
												   foreach($mqbaccess as $key=>$data)
												   {
												    ?>
												     <tr class="MQBAccessItem" data-tab-id="<?php echo $key; ?>" data-section-name="<?php echo $data; ?>">
											         	<td><label><?php echo $data; ?></label></td>
											         	<td>
											         	<div data-state="inactive" class="ItemStatus toggle-iphone"></div>
									                     <input type="hidden" name="MQBreadonly" value="inactive" class="MQBreadonly">
									                    </td>
											         	<td><div data-state="inactive" class="ItemStatus toggle-iphone"></div>
									                      <input type="hidden" name="MQBreadwrite" value="inactive" class="MQBreadwrite"></td>
											         	<td><div data-state="inactive" class="ItemStatus toggle-iphone"></div>
									                      <input type="hidden" name="MQBglobal" value="inactive" class="MQBglobal"></td>
										         	    </tr>
										         	 <?php
										         	 }
										         	 ?>
										         	 </tbody>
										             </table>
										             </div>
													<div class="save-btn-container">
										            <button type="button" class="nb-btn nb-primary icon-btn saveMQBAccess" data-contact-id=""  data-template-id=""><i class="fa fa-save"></i> Save <span class="TabButtonLabel"> MQB Access</span></button>
										       		</div>
										       		
                                			</div>
                                			<!-- MQBAccess -->
                                			</div>
                                		   </div>
                                		   </form>
                                		  
								  	        <button type="button" class="nb-btn nb-primary icon-btn saveAsNewTemplate" id="saveAsNewTemplate"><i class="fa fa-save"></i> Save <span class="TabButtonLabel"> As New Template</span></button>
								          
										  </div>
										  <div class="modal-footer">
											<div class="modal-footer"> <button type="button" class="nb-btn nb-secondary" data-dismiss="modal">Close</button> </div>
											</div>
									  </div>
								  </div>								 
								</div>
								<!-- Modal -->
								<div id="UserNewTemplate" class="modal fade" role="dialog" aria-hidden='true'>
								  <div class="modal-dialog modal-lg">
							       <!-- Modal content-->
								    <div class="modal-content">
								      <div class="modal-header">
								        <button type="button" class="close" data-dismiss="modal">&times;</button>
								      </div>
								      <div class="modal-body">
								        <form method="post">
								          <div class="form-group"> 
								          	<label>Template Name</label>  <input type="text" class="form-control col-md-6 NewTemplateName" name="NewTemplateName" id="NewTemplateName">
								           <span id="templateNameError"></span> 
								          </div>
								          <div class="form-group"> 
								          	<label>Template Description </label> <textarea name="NewTemplateDesc" class="form-control col-md-6 NewTemplateDesc" id="NewTemplateDesc"></textarea> 
								          </div>
								          <button type="button" class="nb-btn nb-primary icon-btn saveUserNewTemplate" id="saveUserNewTemplate"><i class="fa fa-save"></i> Save <span class="TabButtonLabel"> As New Template</span></button>
								        </form>
								      </div>
								      <div class="modal-footer">
								        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								      </div>
								    </div>

								  </div>
								</div>

                                <div id="Users" class="tab-pane active UsermanagementContent ">
                                    <div class="bs-callout bs-callout-info" id="callout-alerts-no-default"><h4>
                                            Users</h4>
                                        <p>Simple!, Create User with user permission tetmplate so need to create user
                                            permission template first.</p></div>

                                    <table class="table table-hover UserManagementTable SettingsContainer" width="100%;"
                                           style="width: 100%;">

                                        <thead>
                                        <tr>

                                            <th>
                                                <h3 class="label-lg"></h3>
                                            </th>
                                            <th>
                                                <h3 class="label-lg">User Level</h3>
                                            </th>
                                            <th>
                                                <h3 class="label-lg">Active User Template</h3>
                                            </th>
                                            <th>
                                                <h3 class="label-lg">Action</h3>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody id="UserManagementTable">
                                        <?php

                                        foreach ($MacantaUsers as $user) {

                                            ?>
                                            <tr>

                                                <td>
                                                    <label><?php echo $user['ContactName']; ?></label>

                                                </td>
                                                <td>
                                                    <label><?php echo $user['ContactLevel']; ?></label>

                                                </td>
                                                <td>
                                                    <label><?php echo $user['TemplateName']; ?></label>
                                                </td>
                                                <td>
                                                    <i class="fa fa-pencil-square-o EditMacantaUser" aria-hidden="true"
                                                       data-usercontact-id="<?php echo $user['ContactId']; ?>"
                                                       data-template-id="<?php echo $user['TemplateId']; ?>"></i>
                                                    <i class="fa fa-trash-o DeleteMacantaUser" aria-hidden="true"
                                                       data-usercontact-id="<?php echo $user['ContactId']; ?>"
                                                       data-template-id="<?php echo $user['TemplateId']; ?>"></i>
                                                </td>
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                        ?></tbody>
                                    </table>

                                    <button type="button" class="nb-btn nb-secondary icon-btn AddNewUser"
                                            id="AddNewUser"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Add
                                        New User
                                    </button>
                                    <!--modal for add new user-->
                                    <div id="AddUserModal" class="modal fade" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                	<h3 class="label-lg">Add/Edit User</h3>
                                                    <button type="button" class="close" data-dismiss="modal">
                                                    	<span>&times;</span>
                                                    </button>

                                                </div>
                                                <div class="modal-body">
                                                    <div id="MacantaUserError"></div>
                                                    <form method="post">
                                                        <div class="form-group">
                                                            <label class="label-sm">ContactName:</label>
                                                            <input type="text" name="ContactId" value=""
                                                                   class="ContactId form-control"
                                                                   data-infusion-contact-id="" id='autocomplete'>
                                                            <div id="contactIdList"></div>
                                                            <div id="ContactId_error"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="label-sm">UserLevel:</label>
                                                            <select name="UserLevel" id="UserLevel"
                                                                    class="UserLevel form-control">
                                                                <option value="None">Select User Level</option>
                                                                <option value="Admin">Admin</option>
                                                                <option value="User">User</option>
                                                            </select>
                                                            <div id="UserLevel_error"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="label-sm">UserTemplate:</label>
                                                            <select name="UserTemplate" id="UserTemplate"
                                                                    class="UserTemplate form-control">
                                                                <option value="None">Select User Permission Template
                                                                </option>
                                                                <!--load from user permission table-->
                                                            </select>
                                                            <div id="UserTemplate_error"></div>
                                                        </div>
                                                        <div class="btn-container">
	                                                        <button type="button"
	                                                                class="nb-btn nb-primary icon-btn SaveUser"
	                                                                id="SaveUser" name="SaveUser"><i class="fa fa-save"></i>
	                                                            Save User <span></span></button>
	                                                    	<button type="button" class="nb-btn nb-primary icon-btn UpdateUser" id="UpdateUser" name="UpdateUser"><i class="fa fa-save"></i> Update User <span></span></button>
	                                                        <button type="button" class="nb-btn nb-secondary"
	                                                                data-dismiss="modal">Close
	                                                        </button>
	                                                    </div>    
                                                    </form>
                                                </div>
                                                <div class="modal-footer">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="UserPermissionTemplates"
                                     class="tab-pane UsermanagementContent ">
                                    <div class="bs-callout bs-callout-info" id="callout-alerts-no-default"><h4>User
                                            Permission Template</h4>
                                        <p>Simple!, Create User Permission Template which defines User Template
                                            access.</p></div>

                                    <table class="table table-hover UserManagementTable SettingsContainer" width="100%;"
                                           style="width: 100%;">
                                        <thead>
                                        <tr>

                                            <th>
                                                <h3 class="label-lg">User Permission Template</h3>
                                            </th>
                                            <th>
                                                <h3 class="label-lg">Template Description</h3>
                                            </th>
                                            <th>
                                                <h3 class="label-lg">Action</h3>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody id="TemplateManagementTable">
                                        <?php
                                        foreach ($MacantaUserPermissionTempalte as $template) {
                                            ?>
                                            <tr>
                                                <td>
                                                    <label><?php echo $template['TemplateName']; ?></label>

                                                </td>
                                                <td>
                                                    <label><?php echo $template['TemplateDescription']; ?></label>
                                                </td>
                                                <td>
                                                    <i class="fa fa-pencil-square-o EditUsertemplate" aria-hidden="true"
                                                       data-template-id="<?php echo $template['TemplateId']; ?>"></i>
                                                    <i class="fa fa-trash-o DeleteUserTemplate" aria-hidden="true"
                                                       data-template-id="<?php echo $template['TemplateId']; ?>"></i>
                                                </td>
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                        ?></tbody>
                                    </table>

                                    <button type="button" class="nb-btn nb-secondary icon-btn AddNewTemplate"
                                            id="AddNewTemplate"><i class="fa fa-plus-square-o" aria-hidden="true"></i>
                                        Add New Template
                                    </button>
                                </div>
                                <div id="ContactViewPermission"
                                     class="tab-pane UsermanagementContent ">

                                </div>
                                <div id="CallSettings"
                                     class="tab-pane UsermanagementContent">
                                    <?php
                                    $MacantaUsers = macanta_get_users();
                                    ?>
                                    <div class="theNotePanel">
                                        <div class="mainbox-top">
                                            <div class="tab-panel">
                                                <div class="panel panel-primary left-CustomTabs">
                                                    <div class="panel-heading">

                                                        <h3 class="panel-title "><i class="fa fa-id-card"></i> Users
                                                            Caller ID</h3>
                                                        <button type="button"
                                                                class="nb-btn icon-btn  saveCallerIDs ShowVerifyPhoneWindow">
                                                            <i class="fa fa-save"></i>
                                                            Add Caller ID
                                                        </button>
                                                    </div>
                                                    <div class="panel-body admin-panelBody MacantaUsersContainer ">
                                                        <div class="MacantaUsersListContainer">
                                                            <h4>Macanta Users List</h4>
                                                            <form id="MacantaUsersList" method="post"
                                                                  class="MacantaUsersList dynamic"
                                                                  _lpchecked="1">
                                                                <ul class="itemContainer">

                                                                    <div class="saved"></div>
                                                                    <?php

                                                                    foreach ($MacantaUsers as $MacantaUser) {
                                                                        $UsersCallerId[] = array()
                                                                        ?>
                                                                        <li class="form-group-item"
                                                                            data-guid="<?php echo $MacantaUser->Id ?>">
                                                                            <div class="UserItem">
																				<span class="MacantaUserListTitle">
																					<?php echo $MacantaUser->FirstName ?> <?php echo $MacantaUser->LastName ?>
																					<br><small>Id: <?php echo $MacantaUser->Id ?> | <?php echo $MacantaUser->Email ?></small>
																				</span>
                                                                            </div>

                                                                        </li>
                                                                        <?php
                                                                    }
                                                                    ?>

                                                                </ul>
                                                            </form>
                                                        </div>

                                                        <div class="CallSettingsContainer">
                                                            <h4>Available Caller IDs</h4>
                                                            <?php
                                                            $account_sid = $this->config->item('Twilio_Account_SID');
                                                            $auth_token = $this->config->item('Twilio_TOKEN');
                                                            $CompanyCallerIds = [];
                                                            $CompanyCallerIdsTemp = [];
                                                            /*try {
                                                                $client = new Twilio\Rest\Client($account_sid, $auth_token);
                                                                $CompanyCallerIds = $client->outgoingCallerIds->read();

                                                            } catch (Twilio\Exceptions\RestException $e) {
                                                                echo '<span>Sorry, Twilio Communication System Down, Details:' . $e->getMessage() . '</span>';

                                                            }
                                                            foreach ($CompanyCallerIds as $CallerId) {
                                                                $CompanyCallerIdsTemp[$CallerId->friendlyName] = $CallerId;
                                                            }
                                                            ksort($CompanyCallerIdsTemp);
                                                            $CompanyCallerIds = [];
                                                            foreach ($CompanyCallerIdsTemp as $friendly_name => $CallerId) {
                                                                $CompanyCallerIds[] = $CallerId;
                                                            }*/

                                                            $DefaultCallerId = $this->config->item('macanta_caller_id');
                                                            ?>
                                                            <div class="CallerIdItem">
                                                                <div class="form-group default">
                                                                    <input class="AllowUserCallerId  " type="checkbox"
                                                                           id="CallerIdDefault" name="CallerIdDefault"
                                                                           autocomplete="off" disabled checked/>
                                                                    <div class="btn-group">
                                                                        <label for="CallerIdDefault"
                                                                               class="nb-btn nb-base-color ">
                                                                            <span class="fa fa-check"></span>
                                                                            <span> </span>
                                                                        </label>
                                                                        <label for="CallerIdDefault"
                                                                               class="nb-btn btn-default checkbox-label active ">
                                                                            <div class="MacantaCallerIdTitle">
                                                                                System Default
                                                                            </div>
                                                                            <div class="MacantaCallerIdNumber">
                                                                                <?php echo $DefaultCallerId ? $DefaultCallerId : "No Default Caller ID" ?>
                                                                            </div>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <form id="MacantaCallerIdList" method="post"
                                                                  class="MacantaCallerIdList dynamic"
                                                                  _lpchecked="1">
                                                                <input type="hidden" name="UserId" class="UserId">
                                                                <ul class="itemContainer">
                                                                    <?php
                                                                    if ($this->config->item('outbound_devices')) {
                                                                        $SystemOuboundDevices = json_decode($this->config->item('outbound_devices'), true);
                                                                    } else {
                                                                        $SystemOuboundDevices = [];
                                                                    }
                                                                    foreach ($CompanyCallerIds as $CallerId) {
                                                                        if ($DefaultCallerId == $CallerId->phoneNumber) continue;
                                                                        $friendly_name = $CallerId->friendlyName;
                                                                        $friendly_name = $CallerId->phoneNumber == $friendly_name ? "<em>No Label</em>" : $friendly_name;
                                                                        $Id = $CallerId->sid;
                                                                        $data = 'data-callerid="' . $CallerId->phoneNumber . '"';
                                                                        $info = "hide";
                                                                        if (in_array($CallerId->phoneNumber, $SystemOuboundDevices)) $info = "";
                                                                        $isdevice = $info == "hide" ? 'no' : 'yes';
                                                                        $fwidth = $info == "hide" ? 'fwidth' : '';
                                                                        ?>
                                                                        <li class="form-group-item"
                                                                            data-calleridtype="user-defined"
                                                                            data-sid="<?php echo $CallerId->sid; ?>"
                                                                            data-guid="<?php echo $CallerId->phoneNumber; ?>">
                                                                            <div class="CallerIdItem">
                                                                                <div class="form-group success">
                                                                                    <input class="AllowUserCallerId UserCallerIdToEnable"
                                                                                           value="<?php echo $CallerId->phoneNumber; ?>"
                                                                                           data-callerid="<?php echo $CallerId->phoneNumber; ?>"
                                                                                           type="checkbox"
                                                                                           name="CallerId"
                                                                                           id="callerId-<?php echo $Id; ?>"
                                                                                           autocomplete="off" disabled/>
                                                                                    <div class="btn-group">
                                                                                        <label for="callerId-<?php echo $Id; ?>"
                                                                                               class="nb-btn nb-primary toggleCallerId">
                                                                                            <span class="fa fa-check"></span>
                                                                                            <span> </span>
                                                                                        </label>
                                                                                        <label class="nb-btn btn-default checkbox-label active ">
                                                                                            <div class="MacantaCallerIdTitle <?php echo $Id; ?> <?php echo $fwidth; ?>"
                                                                                                 id="label<?php echo $Id; ?>"
                                                                                                 title="<?php echo $friendly_name ?>">
                                                                                                <?php echo $friendly_name ?>
                                                                                            </div>
                                                                                            <div class="MacantaCallerIdNumber  <?php echo $Id; ?> <?php echo $fwidth; ?>">
                                                                                                <div class="the-number">
                                                                                                    <?php echo $CallerId->phoneNumber ?>
                                                                                                </div>
                                                                                                <div class="the-info">
																								<span class="info-as-device-<?php echo $Id; ?>">
																										<a class="info-as-device <?php echo $info; ?>">Can be used as a call device</a>
																									</span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </label>
                                                                                        <i class="fa fa-pencil-square-o EditCallerIdLabelIcon CallerIDAction"
                                                                                           aria-hidden="true"
                                                                                           data-sid="<?php echo $CallerId->sid ?>"
                                                                                           data-callerid="<?php echo $CallerId->phoneNumber ?>"
                                                                                           data-label="<?php echo $CallerId->friendlyName ?>"
                                                                                           data-isdevice="<?php echo $isdevice; ?>"></i>
                                                                                        <i class="fa fa-trash-o DeleteCallerIdIcon CallerIDAction"
                                                                                           aria-hidden="true"
                                                                                           data-sid="<?php echo $CallerId->sid ?>"
                                                                                           data-phone="<?php echo $CallerId->phoneNumber ?>"
                                                                                        ></i>
                                                                                    </div>
                                                                                </div>


                                                                            </div>

                                                                        </li>
                                                                        <?php
                                                                    }
                                                                    ?>

                                                                </ul>

                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                        </div>
                                    </div>
                                    <!--Modal For Caller Id Verification-->
                                    <div id="CallerIdWindow" class="modal fade CallerIdWindow" tabindex="-1"
                                         role="dialog" aria-labelledby="CallerIdWindowLabel">
                                        <div class="modal-dialog">
                                            <small class="dev-only php">tab_admin_call_settings.php - Line 174</small>
                                            <div class="modal-content">

                                                <div class="modal-header">
                                                    <h4 class="modal-title">Caller Id Verification</h4>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close"><span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">

                                                    <div class="form-group validate-phone-div">
                                                        <div class="field-group">
                                                            <label for="NumberToVerify"><span
                                                                        class="hidden-xs">Phone*</span></label>
                                                            <input type="tel" id="NumberToVerify" class="NumberToVerify"
                                                                   placeholder="e.g. +1 702 123 4567" required>
                                                            <input type="text" id="ExtToVerify" class="ExtToVerify"
                                                                   placeholder="Ext" style='width:100px'>
                                                        </div>
                                                        <div class="field-group">
                                                            <label for="LabelToVerify"><span
                                                                        class="hidden-xs">Label*</span></label>
                                                            <input type="text" class="LabelToVerify"
                                                                   placeholder="Company Office Phone" required>
                                                        </div>
                                                        <div class="field-group">
                                                            <label for="LabelToCallDevice"><span class="hidden-xs">Can be used as an outbound call device</span></label>
                                                            <div class="material-switch ">
                                                                <input id="LabelToCallDevice" name="LabelToCallDevice"
                                                                       type="checkbox"/>
                                                                <label for="LabelToCallDevice"
                                                                       class="label-success"></label>
                                                            </div>
                                                        </div>

                                                        <button class="nb-btn btn-info" id="ButtonToVerify"
                                                                class="ButtonToVerify">
                                                            Verify Caller ID
                                                        </button>
                                                        <div class='phone-status'></div>
                                                        <div class="twilio-notice">
                                                            <p>Please enter the phone number, <strong>without a leading
                                                                    '0'</strong>,
                                                                you would like to use to call people from within
                                                                macanta. When you click
                                                                'Verify Caller ID', we'll call the number you provide
                                                                and ask you to
                                                                enter a 6-digit verification code. <a target='_blank'
                                                                                                      href='https://conquerthechaos.groovehq.com/knowledge_base/topics/verifying-caller-ids-behind-an-ivr-or-extension'>How
                                                                    to verify caller ids behind an IVR or extension.</a>
                                                            </p>
                                                            <a href='https://www.twilio.com/' target='_blank'><img
                                                                        src='<?php echo base_url(); ?>assets/img/logos_powerdby_twilio.png'
                                                                        style="display:block;margin:20px auto 0 auto;width:135px"></a>
                                                        </div>
                                                    </div>
                                                    <div class="form-group phone-progress">
                                                        Initializing, Please wait..
                                                    </div>

                                                    <script>
                                                        var PhoneInput = $('#NumberToVerify');
                                                        PhoneInput.intlTelInput({
                                                            utilsScript: "<?php echo base_url();?>assets/format-phone/build/js/utils.js",
                                                            nationalMode: false,
                                                            preferredCountries: ["gb", "us"]
                                                        });

                                                        PhoneInput.keyup(function () {
                                                            var inputVal = $(this).val();
                                                            if (inputVal.charAt(0) !== "+") {
                                                                $(this).val("+" + inputVal);
                                                            }
                                                        })


                                                    </script>


                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="nb-btn nb-secondary"
                                                            data-dismiss="modal"><?php echo $this->lang->line('text_close'); ?></button>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->

                                    <!--Modal For Caller Id Edit-->
                                    <div id="EditCallerIdWindow" class="modal fade EditCallerIdWindow" tabindex="-1"
                                         role="dialog" aria-labelledby="EditCallerIdWindowLabel">
                                        <div class="modal-dialog">
                                            <small class="dev-only php">tab_admin_call_settings.php - Line 256</small>
                                            <div class="modal-content EditCallerIdWindowContainer">

                                                <div class="modal-header">
                                                    <h4 class="modal-title">Edit Caller Id</h4>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close"><span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body EditCallerIdWindowBody">

                                                    <div class="form-group validate-phone-div">
                                                        <div class="field-group">
                                                            <label for="LabelToVerify"><span
                                                                        class="hidden-xs">Label*</span></label>
                                                            <input data-sid="" data-callerid="" type="text"
                                                                   class="LabelToVerify" required>
                                                        </div>
                                                        <div class="field-group">
                                                            <label for="LabelToCallDeviceEdit"><span class="hidden-xs">Can be used as an outbound call device</span></label>
                                                            <div class="material-switch ">
                                                                <input id="LabelToCallDeviceEdit"
                                                                       name="LabelToCallDevice" type="checkbox"/>
                                                                <label for="LabelToCallDeviceEdit"
                                                                       class="label-success"></label>
                                                            </div>
                                                        </div>
                                                        <div class="field-group">
                                                            <button class="nb-btn btn-info" id="SaveCallerIdLabel"
                                                                    class="SaveCallerIdLabel">
                                                                Save Caller ID
                                                            </button>
                                                            <button class="nb-btn btn-info" id="MakeDefaultCallerId"
                                                                    class="MakeDefaultCallerId">
                                                                Make Default Caller ID
                                                            </button>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="nb-btn nb-secondary"
                                                            data-dismiss="modal"><?php echo $this->lang->line('text_close'); ?></button>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
