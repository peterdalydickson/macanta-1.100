<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Sei extends REST_Controller
{
    public $DBHOST;
    public $DBUSER;
    public $DBNAME;
    public $DBPASS;
    public $MariaDB;

    public function __construct()
    {
        // Construct the parent class
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '2000M');
        parent::__construct();
        $this->load->database();
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
        $this->load->library('gravatar');
        $this->load->helper('url');
        $this->load->dbforge();
        $this->DBHOST = '159.203.80.134';
        $this->DBUSER = 'sei_filemaker';
        $this->DBNAME = 'sei_filemaker';
        $this->DBPASS = 'wouldest-bawdy-yogi-euphony-cyclone-impress';
        $this->MariaDB = new PDO('mysql:host=' . $this->DBHOST . ';', $this->DBUSER, $this->DBPASS);

    }

    public function index_get()
    {
        $this->response('Forbidden!');
    }

    public function avatar_exe_stop_get()
    {
        $PID = $this->get_pids('sei/avatar');
        if($PID){
            exec('kill -9 ' . $PID);
            echo "sei/avatar stopped!\n";
        }
    }
    public function avatar_b_exe_stop_get()
    {
        $PID = $this->get_pids('sei/avatar_b');
        if($PID){
            exec('kill -9 ' . $PID);
            echo "sei/avatar stopped!\n";
        }
    }
    public function avatar_stop_get()
    {
        $StopFile = dirname(__FILE__) . "/avatar-stop.txt";
        echo file_put_contents($StopFile,'1');
    }
    public function avatar_b_stop_get()
    {
        $StopFile = dirname(__FILE__) . "/avatar-stop-b.txt";
        echo file_put_contents($StopFile,'1');
    }
    public function avatar_b_exe_get()
    {
        $PID = $this->get_pids('sei/avatar_b');
        if($PID){
            $this->response('Terminated! Existing Process Detected.. ', REST_Controller::HTTP_OK);
        }
        $output = '';
        $exec_string = '/usr/bin/php /var/www/macanta/shared/services/_cli.php sei/avatar_b & ';
        exec($exec_string,$output);
        $this->response($output, REST_Controller::HTTP_OK);
    }
    public function avatar_exe_get()
    {
        $PID = $this->get_pids('sei/avatar');
        if($PID){
            $this->response('Terminated! Existing Process Detected.. ', REST_Controller::HTTP_OK);
        }
        $output = '';
        while (true) {
            $exec_string = '/usr/bin/php /var/www/macanta/shared/services/_cli.php sei/avatar';
            exec($exec_string, $output);
        }
        $this->response($output, REST_Controller::HTTP_OK);
    }
    public function avatar_get()
    {
        header("Content-Type: text/plain");
        $LogProfix = date('Y-m-d');
        $StopFile = dirname(__FILE__) . "/avatar-stop.txt";
        $IdNumFile = dirname(__FILE__) . "/idnum.txt";
        $IdNumStr = file_get_contents($IdNumFile);
        $IdNumArr = $IdNumStr ? explode(',', $IdNumStr) : [];
        $OffsetFile = dirname(__FILE__) . "/offset-avatar.txt";
        $OffsetText = file_get_contents($OffsetFile);
        $Offset = $OffsetText ? $OffsetText : 0;
        //Check if other program is editing the offset
        sleep(5);
        $OffsetTextNew = file_get_contents($OffsetFile);
        if ($OffsetText != $OffsetTextNew) die('Terminated! Existing Process Detected.. ');

        $Avatar = [
            'idnum' => '_SEIAccountID',
            'alias' => 'Nickname',
            'avatarmod' => ['note', ' #FileMaker,#Avatar_avatarmod'],
            'AVCOMDT' => '_AvatarSection1CompletionDate',
            'BIRTHDATE' => 'Birthday',
            'M1IDNUM' => '_MastersID',
            'M2IDNUM' => '_2ndMastersID',
            'NeedTranslation' => '_VerbalTranslationNeeded,_WrittenTranslationNeeded', // apply to both Custom fields Y - Yes F - No
            'S2COMDT' => '_AvatarSection1CompletionDate1',
            'SecLanguage' => ['tag', 'Avatar-SecLanguage-{value}'], // Create a tag - Apply Tag if has a proper language,
        ];
        $ExistingTags['FIleMaker Import - Added'] = 287;
        $ExistingTags['FIleMaker Import - Updated'] = 289;
        $Limit = $RowCount = 50;
        $LastId = $this->db->select('Id')->order_by('Id', "desc")->limit(1)->get('InfusionsoftContact')->row()->Id;
        $Query = "SELECT  idnum, alias, avatarmod, AVCOMDT, BIRTHDATE, M1IDNUM, M2IDNUM, NeedTranslation, S2COMDT, SecLanguage 
			   FROM `" . $this->DBNAME . "`.`Avatar` LIMIT $Limit OFFSET $Offset";
        $TableContent = $this->MariaDB->query(
            $Query,
            PDO::FETCH_ASSOC
        );
        while ($Content = $TableContent->fetch(PDO::FETCH_ASSOC)) {
            //print_r($Content);

            if(empty($Content['idnum'])) continue;
            $Contact = $this->GetContactByCustomField('_SEIAccountID', $Content['idnum']);
            if ($Contact) {
                //print_r($Contact);
                $ContactId = $Contact->Id;
                $Log = '';
                $Log .= "Date/Time: " . date('Y-m-d H:i:s') . "\n";
                $Log .= "Offset: $Offset\n";
                $conDat = [];
                $conNotes = [];
                $conTags = [];
                foreach ($Content as $FieldName => $FieldValue) {
                    if(empty($FieldValue)) continue;
                    if (!is_array($Avatar[$FieldName])) {
                        $FieldValue = $FieldValue == 'T' ? 'Yes' : $FieldValue;
                        $FieldValue = $FieldValue == 'F' ? 'No' : $FieldValue;
                        $AvatarFieldname = explode(',', $Avatar[$FieldName]);
                        if (sizeof($AvatarFieldname) > 1) {
                            foreach ($AvatarFieldname as $AvatarFieldnameItem) {
                                $conDat[] = ['name'=>$AvatarFieldnameItem,'value'=>$FieldValue];
                            }
                        } else {
                            $conDat[] = ['name'=>$Avatar[$FieldName],'value'=>$FieldValue];
                        }

                    } else {
                        if ($Avatar[$FieldName][0] == 'tag') {
                            if (!empty(trim($FieldValue)) && trim($FieldValue) != '0') {
                                $Tag = $Avatar[$FieldName][1];
                                $FieldValue = $FieldValue == 'M' ? 'Male' : $FieldValue;
                                $FieldValue = $FieldValue == 'F' ? 'Female' : $FieldValue;
                                $Tag = str_replace('{value}', $FieldValue, $Tag);
                                $this->GetCreateTagId($Tag, $ExistingTags);
                                $conTags[$Tag] = $ExistingTags[$Tag];
                            }

                        }
                        if ($Avatar[$FieldName][0] == 'note') {
                            if (!empty(trim($FieldValue))) {
                                $conNotes[] = [$FieldValue, $Avatar[$FieldName][1]];
                            }
                        }
                    }
                }
                //print_r($conDat);
                infusionsoft_update_contact($conDat, $ContactId, '',[],true,'no');
                $Log .= "Contact Update Result ID: $ContactId \n";
                if (is_numeric($ContactId)) {
                    if ($LastId > $ContactId) {
                        $conTags['FIleMaker Import - Updated'] = 289;
                    } else {
                        $conTags['FIleMaker Import - Added'] = 287;
                    }
                    $Log .= "Applying Tags: \n";
                    foreach ($conTags as $TagName => $TagId) {
                        $result = infusionsoft_apply_tag($ContactId, $TagId, true);
                        $result = $result ? 'Ok' : 'Failed';
                        $Log .= "  Applying $TagName($TagId) : " . $result . "\n";
                    }
                    $Log .= "Creating Notes: \n";
                    $action = "create_is";
                    foreach ($conNotes as $Notes) {
                        $theNote = json_encode($Notes[0]);
                        $hashTag = $Notes[1];

                        $MacantaNotes = '{"note_type":"Import Note","user":"", "note":' . $theNote . '}';

                        if ($this->config->item('NoteCustomFields')) {
                            $NoteCustomFields = json_decode($this->config->item('NoteCustomFields'), true);
                            $CF_MacantaNotes = '_' . $NoteCustomFields['JSON'];
                            //$MacantaNotes = json_encode($MacantaNotes);
                            $PlainNotes = json_encode($theNote);
                            $action_details = '{"table":"ContactAction","fields":{"' . $CF_MacantaNotes . '":' . $MacantaNotes . ',"ContactId":' . $ContactId . ', "CreationNotes":' . $PlainNotes . ',"ActionType":"Other","ActionDescription":"FileMaker-Avatar-AvatarMod","IsAppointment":0}}';

                        } else {
                            $MacantaNotes = base64_encode($MacantaNotes);
                            $CombinedNotes = json_encode($theNote . "\n\n== macanta data: do not edit below this line ==\n" . $MacantaNotes);
                            $action_details = '{"table":"ContactAction","fields":{"ContactId":' . $ContactId . ',"CreationNotes":' . $CombinedNotes . ',"ActionType":"Other","ActionDescription":"FileMaker-Avatar-AvatarMod","IsAppointment":0}}';
                        }
                        /*Save Notes*/
                        $SaveResult = applyFn('rucksack_request',$action, $action_details);
                        $tag_data['note_id'] = $SaveResult->message;
                        $tag_data['tags'] = $hashTag;
                        $tag_data['conId'] = $ContactId;
                        $this->updateTagNotes($tag_data);
                        //$exec_string = '/usr/bin/php /var/www/macanta/shared/services/sync.php '.$this->config->item('MacantaAppName').' 3 & ';
                        //exec($exec_string,$output);
                        $Log .= "  Crating Note Result Id: " . $SaveResult->message . "\n";
                        $Log .= "==============================\n";
                    }

                }
                file_put_contents(dirname(__FILE__) . "/import_log/" . $LogProfix . "_sei-avatar-import.log", $Log, FILE_APPEND);
                //echo $Log;


                //IdNumFile
                $IdNumArr = array_diff($IdNumArr, [$Content['idnum']]);
                $IdNumStr = implode(',', $IdNumArr);
                file_put_contents($IdNumFile, $IdNumStr);
            }
            else {
                //store idnum to continue syncing
                $IdNumArr[] = $Content['idnum'];
                $IdNumStr = implode(',', $IdNumArr);
                file_put_contents($IdNumFile, $IdNumStr);

            }
            $Offset++;
            file_put_contents($OffsetFile, $Offset);
            if (is_file($StopFile)) {
                unlink($StopFile);
                break;
            }

        }
        exit;
    }
    public function avatar_b_get()
    {
        header("Content-Type: text/plain");
        $LogProfix = date('Y-m-d');
        $StopFile = dirname(__FILE__) . "/avatar-stop-b.txt";
        $IdNumFile = dirname(__FILE__) . "/idnum.txt";
        $IdNumStr = file_get_contents($IdNumFile);
        $IdNumArr = $IdNumStr ? explode(',', $IdNumStr) : [];

        $Avatar = [
            'idnum' => '_SEIAccountID',
            'alias' => 'Nickname',
            'avatarmod' => ['note', ' #FileMaker,#Avatar_avatarmod'],
            'AVCOMDT' => '_AvatarSection1CompletionDate',
            'BIRTHDATE' => 'Birthday',
            'M1IDNUM' => '_MastersID',
            'M2IDNUM' => '_2ndMastersID',
            'NeedTranslation' => '_VerbalTranslationNeeded,_WrittenTranslationNeeded', // apply to both Custom fields Y - Yes F - No
            'S2COMDT' => '_AvatarSection1CompletionDate1',
            'SecLanguage' => ['tag', 'Avatar-SecLanguage-{value}'], // Create a tag - Apply Tag if has a proper language,
        ];
        $ExistingTags['FIleMaker Import - Added'] = 287;
        $ExistingTags['FIleMaker Import - Updated'] = 289;
        $LastId = $this->db->select('Id')->order_by('Id', "desc")->limit(1)->get('InfusionsoftContact')->row()->Id;
        foreach ($IdNumArr as $IdNum){
            $Query = "SELECT  idnum, alias, avatarmod, AVCOMDT, BIRTHDATE, M1IDNUM, M2IDNUM, NeedTranslation, S2COMDT, SecLanguage 
			   FROM `" . $this->DBNAME . "`.`Avatar` WHERE idnum = '$IdNum'";
            $TableContent = $this->MariaDB->query(
                $Query,
                PDO::FETCH_ASSOC
            );
            //print_r($this->MariaDB->errorInfo());
            $RowCount = $TableContent->rowCount();
            while ($Content = $TableContent->fetch(PDO::FETCH_ASSOC)) {
                //print_r($Content);

                if(empty($Content['idnum'])) continue;
                $Contact = $this->GetContactByCustomField('_SEIAccountID', $Content['idnum']);
                if ($Contact) {
                    //print_r($Contact);
                    $ContactId = $Contact->Id;
                    $Log = '';
                    $Log .= "Date/Time: " . date('Y-m-d H:i:s') . "\n";
                    $conDat = [];
                    $conNotes = [];
                    $conTags = [];
                    foreach ($Content as $FieldName => $FieldValue) {
                        if(empty($FieldValue)) continue;
                        if (!is_array($Avatar[$FieldName])) {
                            $FieldValue = $FieldValue == 'T' ? 'Yes' : $FieldValue;
                            $FieldValue = $FieldValue == 'F' ? 'No' : $FieldValue;
                            $AvatarFieldname = explode(',', $Avatar[$FieldName]);
                            if (sizeof($AvatarFieldname) > 1) {
                                foreach ($AvatarFieldname as $AvatarFieldnameItem) {
                                    $conDat[] = ['name'=>$AvatarFieldnameItem,'value'=>$FieldValue];
                                }
                            } else {
                                $conDat[] = ['name'=>$Avatar[$FieldName],'value'=>$FieldValue];
                            }

                        } else {
                            if ($Avatar[$FieldName][0] == 'tag') {
                                if (!empty(trim($FieldValue)) && trim($FieldValue) != '0') {
                                    $Tag = $Avatar[$FieldName][1];
                                    $FieldValue = $FieldValue == 'M' ? 'Male' : $FieldValue;
                                    $FieldValue = $FieldValue == 'F' ? 'Female' : $FieldValue;
                                    $Tag = str_replace('{value}', $FieldValue, $Tag);
                                    $this->GetCreateTagId($Tag, $ExistingTags);
                                    $conTags[$Tag] = $ExistingTags[$Tag];
                                }

                            }
                            if ($Avatar[$FieldName][0] == 'note') {
                                if (!empty(trim($FieldValue))) {
                                    $conNotes[] = [$FieldValue, $Avatar[$FieldName][1]];
                                }
                            }
                        }
                    }
                    //print_r($conDat);
                    infusionsoft_update_contact($conDat, $ContactId, '',[],true,'no');
                    $Log .= "Contact Update Result ID: $ContactId \n";
                    if (is_numeric($ContactId)) {
                        if ($LastId > $ContactId) {
                            $conTags['FIleMaker Import - Updated'] = 289;
                        } else {
                            $conTags['FIleMaker Import - Added'] = 287;
                        }
                        $Log .= "Applying Tags: \n";
                        foreach ($conTags as $TagName => $TagId) {
                            $result = infusionsoft_apply_tag($ContactId, $TagId);
                            $result = $result ? 'Ok' : 'Failed';
                            $Log .= "  Applying $TagName : " . $result . "\n";
                        }
                        $Log .= "Creating Notes: \n";
                        $action = "create_is";
                        foreach ($conNotes as $Notes) {
                            $theNote = json_encode($Notes[0]);
                            $hashTag = $Notes[1];

                            $MacantaNotes = '{"note_type":"Import Note","user":"", "note":' . $theNote . '}';

                            if ($this->config->item('NoteCustomFields')) {
                                $NoteCustomFields = json_decode($this->config->item('NoteCustomFields'), true);
                                $CF_MacantaNotes = '_' . $NoteCustomFields['JSON'];
                                //$MacantaNotes = json_encode($MacantaNotes);
                                $PlainNotes = json_encode($theNote);
                                $action_details = '{"table":"ContactAction","fields":{"' . $CF_MacantaNotes . '":' . $MacantaNotes . ',"ContactId":' . $ContactId . ', "CreationNotes":' . $PlainNotes . ',"ActionType":"Other","ActionDescription":"FileMaker-Avatar-AvatarMod","IsAppointment":0}}';

                            } else {
                                $MacantaNotes = base64_encode($MacantaNotes);
                                $CombinedNotes = json_encode($theNote . "\n\n== macanta data: do not edit below this line ==\n" . $MacantaNotes);
                                $action_details = '{"table":"ContactAction","fields":{"ContactId":' . $ContactId . ',"CreationNotes":' . $CombinedNotes . ',"ActionType":"Other","ActionDescription":"FileMaker-Avatar-AvatarMod","IsAppointment":0}}';
                            }
                            /*Save Notes*/
                            $SaveResult = applyFn('rucksack_request',$action, $action_details);
                            $tag_data['note_id'] = $SaveResult->message;
                            $tag_data['tags'] = $hashTag;
                            $tag_data['conId'] = $ContactId;
                            $this->updateTagNotes($tag_data);
                            //$exec_string = '/usr/bin/php /var/www/macanta/shared/services/sync.php '.$this->config->item('MacantaAppName').' 3 & ';
                            //exec($exec_string,$output);
                            $Log .= "  Crating Note Result Id: " . $SaveResult->message . "\n";
                            $Log .= "==============================\n";
                        }

                    }
                    file_put_contents(dirname(__FILE__) . "/import_log/" . $LogProfix . "_sei-avatar-import-b.log", $Log, FILE_APPEND);
                    //echo $Log;
                    //IdNumFile
                    $IdNumArr = array_diff($IdNumArr, [$Content['idnum']]);
                    $IdNumStr = implode(',', $IdNumArr);
                    file_put_contents($IdNumFile, $IdNumStr);
                }
                if (is_file($StopFile)) {
                    unlink($StopFile);
                    break 2;
                }

            }
        }

    }
    public function avatar_offset_get()
    {
        $OffsetFile = dirname(__FILE__) . "/offset-avatar.txt";
        echo file_get_contents($OffsetFile);
    }


    public function master_get()
    {
        header("Content-Type: text/plain");
        $LogProfix = date('Y-m-d');
        $StopFile = dirname(__FILE__) . "/master-stop.txt";
        $IdNumFile = dirname(__FILE__) . "/idnum-master.txt";
        $IdNumStr = file_get_contents($IdNumFile);
        $IdNumArr = $IdNumStr ? explode(',', $IdNumStr) : [];
        $OffsetFile = dirname(__FILE__) . "/offset-master.txt";
        $OffsetText = file_get_contents($OffsetFile);
        $Offset = $OffsetText ? $OffsetText : 0;
        //Check if other program is editing the offset
        sleep(5);
        $OffsetTextNew = file_get_contents($OffsetFile);
        if ($OffsetText != $OffsetTextNew) die('Terminated! Existing Process Detected.. ');

        $Avatar = [
            'IDNUM' => '_SEIAccountID',
            'FRSTLICN' => '_OriginalLicenseDate',
            'LicenseLapse' => '_LicenseStatus',
            'LICEXPR' => '_LicenseValidUntil',
            'LICTYPE' => '_LicenseType',
            'MasterMod' => ['note', '#FileMaker,#Master_MasterMod'],
            'MCCOMDT' => '_DateofAvatarCompletion',
            'MSTRID' => '_cbmasterid',
            'Networks' => ['tag', 'Master-Networks-{value}'],
            'PathStatus' => ['tag', '{value}'],
            //'PrintLicFlag' => ['cd', 'Requires Printed Licenses'], // empty data
            'QM_Pod_One' => ['tag', 'Master-QM_Pod_One-{value}'],
            'QM_Pod_two' => ['tag', 'Master-QM_Pod_two-{value}'],
            'SECLANGU' => ['tag', 'Master-SECLANGU-{value}'],
            'SSNUM' => 'SSN',
            'WIZARD' => ['tag', 'Master-Wizard'] // Create a tag if T or True
        ];
        $ExistingTags['FIleMaker Import - Added'] = 287;
        $ExistingTags['FIleMaker Import - Updated'] = 289;
        $Limit = $RowCount = 50;
        $LastId = $this->db->select('Id')->order_by('Id', "desc")->limit(1)->get('InfusionsoftContact')->row()->Id;
        $Query = "SELECT  IDNUM, FRSTLICN, LicenseLapse, LICEXPR, LICTYPE, MasterMod, MCCOMDT, MSTRID, Networks, PathStatus, PrintLicFlag, QM_Pod_One, QM_Pod_two, SECLANGU, SSNUM, WIZARD 
			   FROM `" . $this->DBNAME . "`.`Master` LIMIT $Limit OFFSET $Offset";
        $TableContent = $this->MariaDB->query(
            $Query,
            PDO::FETCH_ASSOC
        );
        while ($Content = $TableContent->fetch(PDO::FETCH_ASSOC)) {
            //print_r($Content);

            if(empty($Content['IDNUM'])) continue;
            $Contact = $this->GetContactByCustomField('_SEIAccountID', $Content['IDNUM']);
            if ($Contact) {
                //print_r($Contact);
                $ContactId = $Contact->Id;
                $Log = '';
                $Log .= "Date/Time: " . date('Y-m-d H:i:s') . "\n";
                $Log .= "Offset: $Offset\n";
                $conDat = [];
                $conNotes = [];
                $conTags = [];
                foreach ($Content as $FieldName => $FieldValue) {
                    if(empty($FieldValue)) continue;
                    if (!is_array($Avatar[$FieldName])) {
                        if($FieldName == 'LicenseLapse'){
                            $FieldValue = $FieldValue == 'T' ? 'Expired' : $FieldValue;
                            $FieldValue = $FieldValue == 'F' ? 'Active' : $FieldValue;
                        }

                        $AvatarFieldname = explode(',', $Avatar[$FieldName]);
                        if (sizeof($AvatarFieldname) > 1) {
                            foreach ($AvatarFieldname as $AvatarFieldnameItem) {
                                $conDat[] = ['name'=>$AvatarFieldnameItem,'value'=>$FieldValue];
                            }
                        } else {
                            $conDat[] = ['name'=>$Avatar[$FieldName],'value'=>$FieldValue];
                        }

                    } else {
                        if ($Avatar[$FieldName][0] == 'tag') {
                            if (!empty(trim($FieldValue)) && trim($FieldValue) != '0' && trim($FieldValue) != 'F') {
                                $Tag = $Avatar[$FieldName][1];
                                $Tag = str_replace('{value}', $FieldValue, $Tag);
                                $this->GetCreateTagId($Tag, $ExistingTags);
                                $conTags[$Tag] = $ExistingTags[$Tag];
                            }

                        }
                        if ($Avatar[$FieldName][0] == 'note') {
                            if (!empty(trim($FieldValue))) {
                                $conNotes[] = [$FieldValue, $Avatar[$FieldName][1]];
                            }
                        }
                    }
                }
                //print_r($conDat);
                infusionsoft_update_contact($conDat, $ContactId, '',[],true,'no');
                $Log .= "Contact Update Result ID: $ContactId \n";
                if (is_numeric($ContactId)) {
                    if ($LastId > $ContactId) {
                        $conTags['FIleMaker Import - Updated'] = 289;
                    } else {
                        $conTags['FIleMaker Import - Added'] = 287;
                    }
                    $Log .= "Applying Tags: \n";
                    foreach ($conTags as $TagName => $TagId) {
                        $result = infusionsoft_apply_tag($ContactId, $TagId, true);
                        $result = $result ? 'Ok' : 'Failed';
                        $Log .= "  Applying $TagName($TagId) : " . $result . "\n";
                    }
                    $Log .= "Creating Notes: \n";
                    $action = "create_is";
                    foreach ($conNotes as $Notes) {
                        $theNote = json_encode($Notes[0]);
                        $hashTag = $Notes[1];

                        $MacantaNotes = '{"note_type":"Import Note","user":"", "note":' . $theNote . '}';

                        if ($this->config->item('NoteCustomFields')) {
                            $NoteCustomFields = json_decode($this->config->item('NoteCustomFields'), true);
                            $CF_MacantaNotes = '_' . $NoteCustomFields['JSON'];
                            //$MacantaNotes = json_encode($MacantaNotes);
                            $PlainNotes = json_encode($theNote);
                            $action_details = '{"table":"ContactAction","fields":{"' . $CF_MacantaNotes . '":' . $MacantaNotes . ',"ContactId":' . $ContactId . ', "CreationNotes":' . $PlainNotes . ',"ActionType":"Other","ActionDescription":"FileMaker-Master-MasterMod","IsAppointment":0}}';

                        } else {
                            $MacantaNotes = base64_encode($MacantaNotes);
                            $CombinedNotes = json_encode($theNote . "\n\n== macanta data: do not edit below this line ==\n" . $MacantaNotes);
                            $action_details = '{"table":"ContactAction","fields":{"ContactId":' . $ContactId . ',"CreationNotes":' . $CombinedNotes . ',"ActionType":"Other","ActionDescription":"FileMaker-Master-MasterMod","IsAppointment":0}}';
                        }
                        /*Save Notes*/
                        $SaveResult = applyFn('rucksack_request',$action, $action_details);
                        $tag_data['note_id'] = $SaveResult->message;
                        $tag_data['tags'] = $hashTag;
                        $tag_data['conId'] = $ContactId;
                        $this->updateTagNotes($tag_data);
                        //$exec_string = '/usr/bin/php /var/www/macanta/shared/services/sync.php '.$this->config->item('MacantaAppName').' 3 & ';
                        //exec($exec_string,$output);
                        $Log .= "  Crating Note Result Id: " . $SaveResult->message . "\n";
                        $Log .= "==============================\n";
                    }

                }
                file_put_contents(dirname(__FILE__) . "/import_log/" . $LogProfix . "_sei-master-import.log", $Log, FILE_APPEND);
                //echo $Log;


                //IdNumFile
                $IdNumArr = array_diff($IdNumArr, [$Content['IDNUM']]);
                $IdNumStr = implode(',', $IdNumArr);
                file_put_contents($IdNumFile, $IdNumStr);
            }
            else {
                //store idnum to continue syncing
                $IdNumArr[] = $Content['IDNUM'];
                $IdNumStr = implode(',', $IdNumArr);
                file_put_contents($IdNumFile, $IdNumStr);

            }
            $Offset++;
            file_put_contents($OffsetFile, $Offset);
            if (is_file($StopFile)) {
                unlink($StopFile);
                break;
            }

        }
        exit;
    }
    public function master_offset_get()
    {
        $OffsetFile = dirname(__FILE__) . "/offset-master.txt";
        echo file_get_contents($OffsetFile);
    }
    public function master_exe_get()
    {
        $PID = $this->get_pids('sei/master');
        if($PID){
            $this->response('Terminated! Existing Process Detected.. ', REST_Controller::HTTP_OK);
        }
        $output = '';
        while (true){
            $exec_string = '/usr/bin/php /var/www/macanta/shared/services/_cli.php sei/master';
            exec($exec_string,$output);
        }
        $this->response($output, REST_Controller::HTTP_OK);
    }
    public function master_stop_get()
    {
        $StopFile = dirname(__FILE__) . "/master-stop.txt";
        echo file_put_contents($StopFile,'1');
    }
    public function master_exe_stop_get()
    {
        $PID = $this->get_pids('sei/master');
        if($PID){
            exec('kill -9 ' . $PID);
            echo "sei/master stopped!\n";
        }
    }

    public function public_exe_get()
    {
        $PID = $this->get_pids('sei/public');
        if($PID){
            $this->response('Terminated! Existing Process Detected.. ', REST_Controller::HTTP_OK);
        }
        $output = '';
        while (true){
            $exec_string = '/usr/bin/php /var/www/macanta/shared/services/_cli.php sei/public';
            exec($exec_string,$output);
        }
        $this->response($output, REST_Controller::HTTP_OK);
    }
    public function public_offset_get()
    {
        $OffsetFile = dirname(__FILE__) . "/offset-public.txt";
        echo file_get_contents($OffsetFile);
    }
    public function public_get()
    {
        //!!! SELECT command denied to user 'sei_filemaker'@'138.197.197.92' for table 'Public'
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        $LogProfix = date('Y-m-d');
        $StopFile = dirname(__FILE__) . "/public-stop.txt";
        $OffsetFile = dirname(__FILE__) . "/offset-public.txt";
        $OffsetText = file_get_contents($OffsetFile);
        $Offset = $OffsetText ? $OffsetText : 0;
        $Avatar = [
            'IDNUM'=> '_SEIAccountID',
            'FNAME'=> 'FirstName',
            'LNAME'=> 'LastName',
            'ADDRS'=> 'StreetAddress1',
            'CITY'=> 'City',
            'STATE'=> 'State',
            'COUNTRY'=> 'Country',
            'MAILCODE'=> 'PostalCode',
            'ZIPCODE'=> 'ZipFour1',
            'LOCALE'=> 'State',
            'EMAIL'=> 'Email',
            'Backup Email'=> 'EmailAddress2',
            'HOMEPHONE'=> 'Phone1',
            'CellPhone'=> 'Phone2',
            'OfficePhone'=> 'Phone3',
            'Skype'=> 'Fax2',
            'FAX'=> 'Fax1',
            'WebsiteURL'=> 'Website',
            'NOMAIL'=> ['tag','Public_NOMAIL'], // Create a tag - Apply Tag if T
            //'Instant Message'=> ['tag','Public-InstantMessage'], // Create a tag - Apply Tag if True, only 5 record has value.. and its not T or F, its Language
            'Digital Only'=> ['tag','Public-DigitalOnly'], // Create a tag - Apply Tag if T
            //'ENDATE'=> '', // Will be part of Avatar, Date Part 3 Of Avatar Course is Completed. Part of the Delivery Progress Report CD
            'Gender'=> ['tag','Public-Gender-{value}'], // Create a Tag `Public_Gender_Male` and `Public_Gender_Female`- Apply Tag if theres a value
            'LANGUAGE'=> '_LanguageSpoken',
            'MINTIAL'=> 'MiddleName',
            'PublicMod'=> ['note','#FileMaker,#Public_PublicMod'], //Create Contact Note: #FileMaker #Public_PublicMod
            'STATUS'=> '_Status',
        ];
        $action = "create_is";
        $action_details_temp = '{"table":"Contact","fields":~fields~}';

        $ExistingTags['FIleMaker Import - Added'] = 287;
        $ExistingTags['FIleMaker Import - Updated'] = 289;
        $Limit = $RowCount = 50;
        $Query = "SELECT  IDNUM, FNAME, LNAME, ADDRS, CITY, STATE, COUNTRY, MAILCODE, ZIPCODE, LOCALE, EMAIL, `Backup Email`,HOMEPHONE, CellPhone, OfficePhone, Skype, FAX, WebsiteURL, NOMAIL, `Digital Only`, Gender, `LANGUAGE`, MINTIAL, PublicMod, STATUS  
			   FROM `".$this->DBNAME."`.`Public` LIMIT $Limit OFFSET $Offset";
        $TableContent = $this->MariaDB->query(
            $Query,
            PDO::FETCH_ASSOC
        );
        //print_r($this->MariaDB->errorInfo());
        while ($Content = $TableContent->fetch(PDO::FETCH_ASSOC)) {
            //print_r($Content);
            if(empty($Content['IDNUM']))
            {
                $Offset++;
                file_put_contents($OffsetFile, $Offset);
                continue;
            }
            $Contact = $this->GetContactByCustomField('_SEIAccountID', $Content['IDNUM']);

            if($Contact){
                $Offset++;
                file_put_contents($OffsetFile, $Offset);
                continue; // skip existing ones
            }

            $Log = '';
            $Log .=   "Date/Time: ".date('Y-m-d H:i:s')."\n";
            $Log .=   "Offset: $Offset\n";
            $Log .=   "IDNUM: $Content[IDNUM]\n";
            $conDat = [];
            $conNotes = [];
            $conTags = [];
            foreach($Content as $FieldName => $FieldValue){
                if(!is_array($Avatar[$FieldName])){

                    $conDat[$Avatar[$FieldName]] = mb_convert_encoding($FieldValue, 'UTF-8');
                }
                else{
                    if($Avatar[$FieldName][0] == 'tag'){
                        if(!empty(trim($FieldValue))){
                            $Tag = $Avatar[$FieldName][1];
                            $FieldValue = $FieldValue == 'M' ? 'Male' : $FieldValue;
                            $FieldValue = $FieldValue == 'F' ? 'Female' : $FieldValue;
                            $Tag = str_replace('{value}', $FieldValue,$Tag);
                            $this->GetCreateTagId($Tag,$ExistingTags);
                            $conTags[$Tag] = $ExistingTags[$Tag];
                        }

                    }
                    if($Avatar[$FieldName][0] == 'note'){
                        if(!empty(trim($FieldValue))){
                            $conNotes[] =  [$FieldValue,$Avatar[$FieldName][1]];
                        }
                    }
                }
            }


            if($conDat['PostalCode'] == '' && $conDat['ZipFour1'] != ''){
                $conDat['PostalCode'] = $conDat['ZipFour1'];
                $conDat['ZipFour1'] = '';
            }
            if(empty($conDat['Email'])){
                $conDat['Email'] = $Content['IDNUM']."@SEIAccountID.com";
            }
            switch ($conDat['Country']){
                case 'USA':
                    $conDat['Country'] = 'United States';
                    break;
                case 'ArabEmirates':
                    $conDat['Country'] = 'United Arab Emirates (the)';
                    break;
                case 'Kingdom of Bahrain':
                    $conDat['Country'] = 'Bahrain';
                    break;
                case 'UK':
                    $conDat['Country'] = 'United Kingdom';
                    break;
                case 'Iran':
                    $conDat['Country'] = 'Iran (Islamic Republic of)';
                    break;
                case 'Korea':
                    $conDat['Country'] = 'Korea (the Republic of)';
                    break;
                case 'Macedonia':
                    $conDat['Country'] = 'Macedonia (the former Yugoslav Republic of)';
                    break;
                case 'Venezuela':
                    $conDat['Country'] = 'Venezuela (Bolivarian Republic of)';
                    break;
                case 'Taiwan':
                    $conDat['Country'] = 'Taiwan (Province of China)';
                    break;
                case 'Russia':
                    $conDat['Country'] = 'Russian Federation (the)';
                    break;
                case 'Palestine':
                    $conDat['Country'] = 'Palestine, State of';
                    break;
                case 'Netherlands':
                    $conDat['Country'] = 'Netherlands (the)';
                    break;
                case 'Moldova':
                    $conDat['Country'] = 'Moldova (the Republic of)';
                    break;
                case 'moldova':
                    $conDat['Country'] = 'Moldova (the Republic of)';
                    break;
                case 'Czech Republic':
                    $conDat['Country'] = 'Czech Republic (the)';
                    break;
                case 'Rep. Democratic du Congo':
                    $conDat['Country'] = 'Congo (the Democratic Republic of the)';
                    break;
                case 'Cayman Islands':
                    $conDat['Country'] = 'Cayman Islands (the)';
                    break;
                case 'Bolivia':
                    $conDat['Country'] = 'Bolivia (Plurinational State of)';
                    break;
                default:
                    break;
            }
            //print_r($conDat);
            $TableFields = json_encode($conDat);
            $action_details = str_replace('~fields~', $TableFields, $action_details_temp);
            $Log .=   "Contact Action Details: $action_details \n";
            //echo $Log;
            $SaveResult = applyFn('rucksack_request',$action, $action_details);
            $SyncedContact = $SaveResult->message;
            $Log .=   "Contact AddUpdate Result ID: $SyncedContact \n";
            if(is_numeric($SyncedContact)){
                $conTags['FIleMaker Import - Updated'] = 289;
                $conTags['FIleMaker Import - Added'] = 287;
                $ContactId = $SyncedContact;
                $Log .=   "Applying Tags: \n";
                foreach ($conTags as $TagName => $TagId){

                    $result = infusionsoft_apply_tag($ContactId, $TagId, true);
                    $result = $result ? 'Ok' : 'Failed';
                    $Log .= "  Applying $TagName($TagId) : " . $result . "\n";
                }
                $Log .=   "Creating Notes: \n";
                foreach ($conNotes as $Notes) {
                    $theNote = json_encode($Notes[0]);
                    $hashTag = $Notes[1];

                    $MacantaNotes = '{"note_type":"Import Note","user":"", "note":' . $theNote . '}';

                    if ($this->config->item('NoteCustomFields')) {
                        $NoteCustomFields = json_decode($this->config->item('NoteCustomFields'), true);
                        $CF_MacantaNotes = '_' . $NoteCustomFields['JSON'];
                        //$MacantaNotes = json_encode($MacantaNotes);
                        $PlainNotes = json_encode($theNote);
                        $action_details = '{"table":"ContactAction","fields":{"' . $CF_MacantaNotes . '":' . $MacantaNotes . ',"ContactId":' . $ContactId . ', "CreationNotes":' . $PlainNotes . ',"ActionType":"Other","ActionDescription":"FileMaker-Avatar-AvatarMod","IsAppointment":0}}';

                    } else {
                        $MacantaNotes = base64_encode($MacantaNotes);
                        $CombinedNotes = json_encode($theNote . "\n\n== macanta data: do not edit below this line ==\n" . $MacantaNotes);
                        $action_details = '{"table":"ContactAction","fields":{"ContactId":' . $ContactId . ',"CreationNotes":' . $CombinedNotes . ',"ActionType":"Other","ActionDescription":"FileMaker-Avatar-AvatarMod","IsAppointment":0}}';
                    }
                    /*Save Notes*/
                    $SaveResult = applyFn('rucksack_request',$action, $action_details);
                    $tag_data['note_id'] = $SaveResult->message;
                    $tag_data['tags'] = $hashTag;
                    $tag_data['conId'] = $ContactId;
                    $this->updateTagNotes($tag_data);
                    //$exec_string = '/usr/bin/php /var/www/macanta/shared/services/sync.php '.$this->config->item('MacantaAppName').' 3 & ';
                    //exec($exec_string,$output);
                    $Log .= "  Crating Note Result Id: " . $SaveResult->message . "\n";
                    $Log .= "==============================\n";
                }

            }
            $Offset++;
            file_put_contents(dirname(__FILE__) . "/import_log/" . $LogProfix . "_sei-public-import.log", $Log, FILE_APPEND);
            file_put_contents($OffsetFile, $Offset);
            //echo $Log;
            if(is_file($StopFile)){
                unlink($StopFile);
                break;
            }
        }
        //$this->response($Log, REST_Controller::HTTP_OK);
        exit;
    }

    public function avatar_registrations_get(){
        header("Content-Type: text/plain");
        // if it has _cbmasterid it should be Master
        // if it has _MastersID,_2ndMastersID it should be Avatar
        // if it has _MastersID and _cbmasterid should be Master and Avatar
        $StopFile = dirname(__FILE__) . "/cd-avatar-stop.txt";
        $OffsetFile = dirname(__FILE__) . "/offset-cd-avatar.txt";
        $OffsetText = file_get_contents($OffsetFile);
        $LogProfix = date('Y-m-d_H-i');
        $Offset = $OffsetText ? $OffsetText : 0;
        $Limit = $RowCount = 10;
        $InfusionsoftContactFields = 'Id, IdLocal, FirstName, LastName, Email, CustomField';
        $CustomFields = [];
        $TitleMap = [];
        $Debug = [];
        $ConnectedGroup = 'Avatar Registrations';
        $ConnectedGroupFieldsMap = macanta_get_connected_info_group_fields_map($ConnectedGroup,'', true, false);
        $ConnectedGroupFieldsMapFields = $ConnectedGroupFieldsMap['fields'];
        //print_r($ConnectedGroupFieldsMapFields);
        foreach ($ConnectedGroupFieldsMapFields as $Fields){
            $CustomFields[] = '_'.$Fields['custom-field'];
            $TitleMap['_'.$Fields['custom-field']] = $Fields['title'];
        }
        while ($RowCount >= $Limit){
            $this->db->select($InfusionsoftContactFields);
            $this->db->like('CustomField','_MastersID');
            $this->db->or_like('CustomField','_2ndMastersID');
            $this->db->limit($Limit, $Offset);
            $query = $this->db->get('InfusionsoftContact');
            $RowCount = $query->result();
            foreach ($query->result() as $row) {
                $ToCD = [];
                $ContactId = !empty($row->Id) ? $row->Id:$row->IdLocal;
                $CDCustomFields = json_decode($row->CustomField, true);
                $Log = '';
                $Log .= "Date/Time: " . date('Y-m-d H:i:s') . "\n";
                $Log .= "Offset: $Offset\n";
                foreach ($CustomFields as $CustomField){
                    if(isset($CDCustomFields[$CustomField])){
                        $ToCD[$TitleMap[$CustomField]] = $CDCustomFields[$CustomField];
                    }
                }
                $Log.= "CD Data: ".json_encode($ToCD) . "\n";
                //Get Connected Contacts
                $ConnectedContacts[$ContactId] = 'Avatar';
                if(isset($ToCD["Master's ID"])){
                    $_Master = $this->GetContactByCustomField('_SEIAccountID', $ToCD["Master's ID"]);
                    //echo $ToCD["Master's ID"]."\n";
                    //print_r($_Master);
                    if ($_Master) {
                        $_MasterContactId = $_Master->Id;
                        $ConnectedContacts[$_MasterContactId] = 'Master';
                    }
                }
                if(isset($ToCD["2nd Master's ID"])){
                    $_2ndMaster = $this->GetContactByCustomField('_SEIAccountID', $ToCD["2nd Master's ID"]);
                    //echo $ToCD["Master's ID"]."\n";
                    //print_r($_2ndMaster);
                    if ($_2ndMaster) {
                        $_2ndMasterContactId = $_2ndMaster->Id;
                        $ConnectedContacts[$_2ndMasterContactId] = '2nd Master';
                    }
                }
                $Log.= "CD Contacts: ".json_encode($ConnectedContacts) . "\n";
                $Debug = macanta_add_update_connected_data($ContactId, false,$ConnectedGroup, $ToCD, $ConnectedContacts,false, false, true, "", false, true );
                if(isset($Debug['Added']['DBData']['id'])){
                    $ItemId = $Debug['Added']['DBData']['id'];
                    $Log .= "CD Created with Id: " . $ItemId . "\n";
                    $Log .= "==============================\n";
                }else{
                    $Log .= "CD Failed!\n";
                    $Log .= "==============================\n";
                }
                file_put_contents(dirname(__FILE__) . "/cd_log/" . $LogProfix . "_sei-avatar-cd.log", $Log, FILE_APPEND);

                //print_r($ToCD);
                //print_r($ConnectedContacts);
                $Offset++;
                //file_put_contents($OffsetFile, $Offset);
                if (is_file($StopFile)) {
                    unlink($StopFile);
                    break 2;
                }
                break 2;
            }
        }

        $this->response($Log, REST_Controller::HTTP_OK);
    }
    public function delivery_progress_reports_get(){
        header("Content-Type: text/plain");
        // if it has _cbmasterid it should be Master
        // if it has _MastersID,_2ndMastersID it should be Avatar
        // if it has _MastersID and _cbmasterid should be Master and Avatar
        $StopFile = dirname(__FILE__) . "/cd-avatar-stop.txt";
        $OffsetFile = dirname(__FILE__) . "/offset-cd-avatar.txt";
        $OffsetText = file_get_contents($OffsetFile);
        $LogProfix = date('Y-m-d_H-i');
        $Offset = $OffsetText ? $OffsetText : 0;
        $Limit = $RowCount = 10;
        $InfusionsoftContactFields = 'Id, IdLocal, FirstName, LastName, Email, CustomField';
        $CustomFields = [];
        $TitleMap = [];
        $Debug = [];
        $ConnectedGroup = 'Avatar Registrations';
        $ConnectedGroupFieldsMap = macanta_get_connected_info_group_fields_map($ConnectedGroup,'', true, false);
        $ConnectedGroupFieldsMapFields = $ConnectedGroupFieldsMap['fields'];
        //print_r($ConnectedGroupFieldsMapFields);
        foreach ($ConnectedGroupFieldsMapFields as $Fields){
            $CustomFields[] = '_'.$Fields['custom-field'];
            $TitleMap['_'.$Fields['custom-field']] = $Fields['title'];
        }
        while ($RowCount >= $Limit){
            $this->db->select($InfusionsoftContactFields);
            $this->db->like('CustomField','_MastersID');
            $this->db->or_like('CustomField','_2ndMastersID');
            $this->db->limit($Limit, $Offset);
            $query = $this->db->get('InfusionsoftContact');
            $RowCount = $query->result();
            foreach ($query->result() as $row) {
                $ToCD = [];
                $ContactId = !empty($row->Id) ? $row->Id:$row->IdLocal;
                $CDCustomFields = json_decode($row->CustomField, true);
                $Log = '';
                $Log .= "Date/Time: " . date('Y-m-d H:i:s') . "\n";
                $Log .= "Offset: $Offset\n";
                foreach ($CustomFields as $CustomField){
                    if(isset($CDCustomFields[$CustomField])){
                        $ToCD[$TitleMap[$CustomField]] = $CDCustomFields[$CustomField];
                    }
                }
                $Log.= "CD Data: ".json_encode($ToCD) . "\n";
                //Get Connected Contacts
                $ConnectedContacts[$ContactId] = 'Avatar';
                if(isset($ToCD["Master's ID"])){
                    $_Master = $this->GetContactByCustomField('_SEIAccountID', $ToCD["Master's ID"]);
                    //echo $ToCD["Master's ID"]."\n";
                    //print_r($_Master);
                    if ($_Master) {
                        $_MasterContactId = $_Master->Id;
                        $ConnectedContacts[$_MasterContactId] = 'Master';
                    }
                }
                if(isset($ToCD["2nd Master's ID"])){
                    $_2ndMaster = $this->GetContactByCustomField('_SEIAccountID', $ToCD["2nd Master's ID"]);
                    //echo $ToCD["Master's ID"]."\n";
                    //print_r($_2ndMaster);
                    if ($_2ndMaster) {
                        $_2ndMasterContactId = $_2ndMaster->Id;
                        $ConnectedContacts[$_2ndMasterContactId] = '2nd Master';
                    }
                }
                $Log.= "CD Contacts: ".json_encode($ConnectedContacts) . "\n";
                $Debug = macanta_add_update_connected_data($ContactId, false,$ConnectedGroup, $ToCD, $ConnectedContacts,false, false, true, "", false, true );
                if(isset($Debug['Added']['DBData']['id'])){
                    $ItemId = $Debug['Added']['DBData']['id'];
                    $Log .= "CD Created with Id: " . $ItemId . "\n";
                    $Log .= "==============================\n";
                }else{
                    $Log .= "CD Failed!\n";
                    $Log .= "==============================\n";
                }
                file_put_contents(dirname(__FILE__) . "/cd_log/" . $LogProfix . "_sei-avatar-cd.log", $Log, FILE_APPEND);

                //print_r($ToCD);
                //print_r($ConnectedContacts);
                $Offset++;
                //file_put_contents($OffsetFile, $Offset);
                if (is_file($StopFile)) {
                    unlink($StopFile);
                    break 2;
                }
                break 2;
            }
        }

        $this->response($Log, REST_Controller::HTTP_OK);
    }

    public function GetContactByCustomField($FieldName, $FieldValue)
    {
        $this->db->like('CustomField', '"' . $FieldName . '":"' . $FieldValue . '"');
        $query = $this->db->get('InfusionsoftContact');
        $row = $query->row();
        return $row;
    }
    public function GetCreateTagId($TagName, &$ExistingTags)
    {
        if (array_key_exists($TagName, $ExistingTags)) return $ExistingTags[$TagName];

        $TagsFound = infusionsoft_get_tags_by_groupname_and_catId($TagName, 53);
        if (sizeof($TagsFound) > 0) {
            $ExistingTags[$TagName] = $TagsFound[0]->Id;
        } else {
            $newTag = array();
            $newTag['GroupCategoryId'] = 53;
            $newTag['GroupName'] = trim($TagName);
            $newTag['GroupDescription'] = '';
            $result = infusionsoft_create_tag($newTag);
            $Id = $result->message;

            $ExistingTags[$TagName] = $Id;
        }
        return $ExistingTags;
    }
    public function updateTagNotes($data)
    {
        $NoteId = $data['note_id'];
        $tagsStr = $data['tags'];
        $contactId = $data['conId'];
        //$SelectedContactResult = storeTaggedSelectedContact($data);
        $DBdata = array(
            'tag_slugs' => $tagsStr
        );
        if (false == $this->DbExists('note_id', $NoteId, 'note_tags')) {
            $DBdata['note_id'] = $NoteId;
            $DBdata['contact_id'] = $contactId;
            $this->db->insert('note_tags', $DBdata);
        } else {
            $this->db->where('note_id', $NoteId);
            $this->db->update('note_tags', $DBdata);
        }
        $tagsArr = explode(',', $tagsStr);
        foreach ($tagsArr as $tag) {
            $tagSlug = strtolower($tag);
            $tagName = $tag;
            $tagData = $this->DbExists('tag_slug', $tagSlug, 'tags');
            if (!$tagData) {
                /*Save Unique Tag To DB*/
                $DBdata = array(
                    'tag_slug' => $tagSlug,
                    'tag_name' => $tagName
                );
                $this->db->insert('tags', $DBdata);
            }

        }
    }
    public function DbExists($field, $value, $table)
    {
        $this->db->where($field, $value);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {

            foreach ($query->result() as $row) {
                return $row;
            }
            return false;
        } else {

            return false;
        }
    }
    public function get_pids($service){
        $ConsumerOutput = [];
        exec('ps -ef | awk \'$NF=="'.$service.'"  {print $2}\'',$ConsumerOutput);
        $PID = sizeof($ConsumerOutput) > 0 ? $ConsumerOutput[0]:0;
        return $PID;
    }
}
