<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 07/03/16
 * Time: 3:00 PM
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Basic_info extends MY_Controller
{

    private $Contact;
    public $ClientToken;
    public $AccessToken;
    public $TaskQueueTokens;
    public $WorkspaceToken;
    public $WorkersTokens;
    public $CallCenterScript;
    public $WorkerActivities;
    public $ajaxResults = array(
        "status" => 0, // true or false
        "message" => "",// any massage
        "data" => array(), // returned data
        "script" => '' // javascript need to execute // returned data
    );

    public function __construct($Prams=[],$session_data=[])
    {
        parent::__construct();
        $this->Contact = $Prams;
        //$this->load->database(); this is already loaded in My_Controller
        //$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file')); this is already loaded in My_Controller
        $Twilio_App_SID = $this->config->item('Twilio_App_SID');
        $Twilio_Account_SID = $this->config->item('Twilio_Account_SID');
        $Twilio_TOKEN = $this->config->item('Twilio_TOKEN');
        $Twilio_API_Key = $this->config->item('Twilio_API_Key');
        $Twilio_API_Secret = $this->config->item('Twilio_API_Secret');
        $this->CallCenterScript = '';
        try {
            $ClientToken = new Twilio\Jwt\ClientToken($Twilio_Account_SID, $Twilio_TOKEN);
            $ClientToken->allowClientOutgoing($Twilio_App_SID);
            $this->ClientToken = $ClientToken->generateToken(86400);



            /*Genereate Tokens for TaskRouters*/
            $CallCenterData = $this->config->item('CallCenterData');
            $CallCenter = $this->config->item('CallCenter');
            if($CallCenterData && $CallCenter == 'enabled'){

                $SyncGrant = new Twilio\Jwt\Grants\SyncGrant;
                $SyncGrant->setServiceSid('default');
                $AccessToken = new Twilio\Jwt\AccessToken($Twilio_Account_SID,$Twilio_API_Key,$Twilio_API_Secret,86400, $session_data['Email'] );
                $AccessToken->addGrant($SyncGrant);
                $this->AccessToken = [ 'identity' => $session_data['Email'], 'token' => $AccessToken->toJWT() ];


                $CallCenterData = json_decode($CallCenterData, true);
                //Generate Token for Workspace
                if(isset($CallCenterData["Workspaces"])){
                    $WorkspaceSid = $CallCenterData["Workspaces"]['sid'];
                    $WorkspaceCapability = new Twilio\Jwt\TaskRouter\WorkspaceCapability($Twilio_Account_SID, $Twilio_TOKEN, $WorkspaceSid);
                    $WorkspaceCapability->allowFetchSubresources();
                    $WorkspaceCapability->allowUpdatesSubresources();
                    $WorkspaceCapability->allowDeleteSubresources();
                    $this->WorkspaceToken = $WorkspaceCapability->generateToken(86400);
                    //Generate Tokens for TaskQues
                    if(isset($CallCenterData["TaskQueues"])){
                        foreach ($CallCenterData["TaskQueues"] as $TaskQueueName => $TaskQueueDetails){
                            $TaskQueueCapability = new Twilio\Jwt\TaskRouter\TaskQueueCapability($Twilio_Account_SID, $Twilio_TOKEN, $WorkspaceSid, $TaskQueueDetails['sid']);
                            $TaskQueueCapability->allowFetchSubresources();
                            $TaskQueueCapability->allowUpdates();
                            $this->TaskQueueTokens[$TaskQueueName] = $TaskQueueCapability->generateToken(86400);
                        }
                    }
                    if(isset($CallCenterData["Workers"])){
                        foreach ($CallCenterData["Workers"] as $WorkerName => $WorkerDetails){
                            $WorkerCapability = new Twilio\Jwt\TaskRouter\WorkerCapability($Twilio_Account_SID, $Twilio_TOKEN, $WorkspaceSid, $WorkerDetails['sid']);
                            $WorkerCapability->allowActivityUpdates();
                            $WorkerCapability->allowReservationUpdates();
                            $this->WorkersTokens[$WorkerDetails['sid']] = $WorkerCapability->generateToken(86400);  // 60 * 60 * 8
                        }
                    }
                    if(isset($CallCenterData["Activities"])){
                        $this->WorkerActivities = json_encode($CallCenterData["Activities"]);
                    }
                }
                $this->CallCenterScript = "<script>WorkerActivities = ".$this->WorkerActivities.";TaskQueueTokens = ".json_encode($this->TaskQueueTokens).";WorkersTokens = ".json_encode($this->WorkersTokens).";WorkspaceToken = '".$this->WorkspaceToken."';initAgentSwitch();initWorkspace();initTaskQueues();</script>";
            }
        } catch (Twilio\Exceptions\RestException $e) {
            $this->ClientToken = 0;
        }
    }

    public function index($Items,$session_name,$session_data)
    {
        if ($session_data['InfusionsoftID'] == $this->Contact->Id) {
            $this->db->where('user_id',$this->Contact->Id);
            $this->db->where('meta_key','caller_id');
            $query = $this->db->get('users_meta');
            $row = $query->row();
            $tipA = "Add this phone as your Caller ID";
            $tipB = "Current Caller ID";
            $tipC = "Listed as Caller Id";
            $Button1 = isset($this->Contact->Phone1Twilio) ? '<a class="verifyPhone PhoneTooltip" data-tip="'.$tipA.'" data-phone="Phone1">verify</a>':'';
            $Button2 = isset($this->Contact->Phone2Twilio) ? '<a class="verifyPhone PhoneTooltip" data-tip="'.$tipA.'" data-phone="Phone2">verify</a>':'';
            if (isset($row))
            {

                $Twilio_info =  $row->meta_value;
                $infoArr = json_decode($Twilio_info,true);

                $FirstPhone = isset($infoArr[$this->Contact->Phone1Twilio]) ? $this->Contact->Phone1Twilio:false;
                $SecondPhone = isset($infoArr[$this->Contact->Phone2Twilio]) ? $this->Contact->Phone2Twilio:false;
                //$Button1 .= $this->Contact->Phone1Twilio;
                if($FirstPhone !== false){
                    if($infoArr[$FirstPhone]['verified'] == 'yes'){
                        $statClass = $infoArr[$FirstPhone]['active'] == 'yes' ? 'activeCallerId':'';
                        $theTip = $statClass == 'activeCallerId' ? $tipB:$tipC;
                        $Button1 = '<a class="verifiedPhone PhoneTooltip '.$statClass.'" data-tip="'.$theTip.'" data-phone="'.$FirstPhone.'">verified</a>';
                    }
                }
                if($SecondPhone !== false){
                    if($infoArr[$SecondPhone]['verified'] == 'yes'){
                        $statClass = $infoArr[$SecondPhone]['active'] == 'yes' ? 'activeCallerId':'';
                        $theTip = $statClass == 'activeCallerId' ? $tipB:$tipC;
                        $Button1 = '<a class="verifiedPhone PhoneTooltip '.$statClass.'" data-tip="'.$theTip.'" data-phone="'.$FirstPhone.'">verified</a>';
                    }
                }

            }
            $this->Contact->Button1 = $Button1;
            $this->Contact->Button2 = $Button2;
        }
        $this->Contact->session_data = $session_data;
        $HTML = $this->load->view('core/contact_info', $this->Contact, true);
        return
            $HTML
            . "<script>TwilioToken = '" . $this->ClientToken . "';SyncToken=".json_encode($this->AccessToken).";initiateTwilio();showLastSearchKey();overideNextContactbySearchResultsFilter('".$this->Contact->Id."', '".base_url()."');</script>"
            . $this->CallCenterScript;
    }
    public function form_edit_contact($Items,$session_name,$session_data)
    {
        $this->Contact->session_data = $session_data;
        $HTML = $this->load->view('core/contact_edit', $this->Contact, true);
        return $HTML;
    }
    public function eCommerce($Items,$session_name,$session_data)
    {
        //$HTML = $this->load->view('core/contact_ecommerce', $this->Contact, true);
        return '';
    }
    public function linked($Items,$session_name,$session_data)
    {
        // $HTML = $this->load->view('core/contact_linked', $this->Contact, true);
        return '';
    }
    public function search($Items,$session_name,$session_data)
    {
        //$HTML = $this->load->view('core/contact_search', $this->Contact, true);
        return '';
    }
    public function twillioCall($Items,$session_name,$session_data)
    {
        $OptOutTag = infusionsoft_get_opt_out_tag('Phone Opt-out');
        $twillioFeatures = array('PhoneCall', 'CallRecord');
        if (in_array('all', $Items)) {
            macanta_update_user_seession_data($session_name,'twillioFeatures',$twillioFeatures);
        } else {
            $availableTwillioFeatures = [];
            foreach ($Items as $Item) {
                $availableTwillioFeatures[] = $Item;
            }
            macanta_update_user_seession_data($session_name,'twillioFeatures',$availableTwillioFeatures);
        }

        $Param['Contact'] = $this->Contact;
        $Param['Items'] = $Items;
        $Param['OptOutTag'] = $OptOutTag;
        $Param['session_data'] = $session_data;
        $HTML = $this->load->view('core/contact_call', $Param, true);
        return $HTML;
    }
    public function lazyTwillioCall($Data){
        $session_name = $Data['session_name'];
        $user_seession_data = macanta_get_user_seession_data($session_name);
        $session_data = unserialize($user_seession_data->session_data);

        //Get All Available Company Caller ID
        $account_sid = $this->config->item('Twilio_Account_SID');
        $auth_token = $this->config->item('Twilio_TOKEN');
        try {
            $client = new Twilio\Rest\Client($account_sid, $auth_token);
            $CompanyCallerIds  = $client->outgoingCallerIds->read();

        } catch (Twilio\Exceptions\RestException $e) {
            $CompanyCallerIds = [];
            $Param['ERROR'] = $e->getMessage();
        }

        $CompanyCallerIdsTemp = [];
        foreach ($CompanyCallerIds as $CallerId){
            $CompanyCallerIdsTemp[$CallerId->friendlyName] = $CallerId;
        }
        ksort($CompanyCallerIdsTemp);
        $CompanyCallerIds = [];
        foreach ($CompanyCallerIdsTemp as $friendly_name => $CallerId){
            $CompanyCallerIds[] = $CallerId;
        }
        $DefaultCallerId = $this->config->item('macanta_caller_id');
        $Param['DefaultCallerId'] = $DefaultCallerId;
        if($DefaultCallerId == "" || $DefaultCallerId == null){
            $Param['ERROR'] = "No Default Caller ID";
        }
        $Param['session_data'] = $session_data;
        $Param['CompanyCallerIds'] = $CompanyCallerIds;
        $HTML = $this->load->view('core/lazy_contact_call', $Param, true);
        $this->ajaxResults['data'] = $HTML;
        $this->ajaxResults['status'] = 1;
        return $this->ajaxResults;
    }
    public function deleteUserConnectedInfo($data){
        $ContactId = $data['ContactId'];
        $itemId = $data['itemId'];
        $GUID = $data['GUID'];

        $this->db->where('id',$itemId);
        $query = $this->db->get('connected_data');
        $row = $query->row();

        if (isset($row))
        {
            $history = json_decode($row->history, true);
            /*======check fields for differences========*/
            $OldValuesArr = json_decode($row->value, true);

            /*=========================*/
            /*======check connection for differences========*/
            $OldValuesArr = json_decode($row->connected_contact, true);
            $NewValuesArr = $OldValuesArr;
            unset($NewValuesArr[$ContactId]);
            $history["connection_history"][] = array(
                "date"=>date("Y-m-d H:i:s"),
                "from"=>$OldValuesArr,
                "to"=>$NewValuesArr
            );

            /*=========================*/
            $DBdata['connected_contact'] = json_encode($NewValuesArr);
            $DBdata['history'] = json_encode($history);
            $this->db->where('id',$itemId);
            $this->db->update('connected_data', $DBdata);
        }
        $this->ajaxResults['data'] = "";
        $this->ajaxResults['message'] = "Info Deleted";
        $this->ajaxResults['status'] = 1;

        unset($ConnectorTabsEncoded);
        unset($ConnectorTabs);
        return $this->ajaxResults;
    }
    public function addBulkConnectedInfo($data){
        $value = $data['Value'];
        $ContactId = $data['ContactId'];
        $GUID = '';
        foreach ($value as $itemId => $ConnectedContact){
            $this->db->where('id',$itemId);
            $query = $this->db->get('connected_data');
            $row = $query->row();
            if (isset($row))
            {
                $history = json_decode($row->history, true);
                $GUID = $row->group;
                /*======check connection for differences========*/
                $UpdateHistory = true;
                $OldValuesArr = json_decode($row->connected_contact, true);
                $NewValuesArr = $OldValuesArr;
                foreach ($ConnectedContact as $ContactId => $ContactInfo){
                    $NewValuesArr[$ContactId] = $ContactInfo;
                }
                $history["connection_history"][] = array(
                    "date"=>date("Y-m-d H:i:s"),
                    "from"=>[],
                    "to"=>$ConnectedContact
                );

                /*=========================*/
                $DBdata['connected_contact'] = json_encode($NewValuesArr);
                if($UpdateHistory === true)
                    $DBdata['history'] = json_encode($history);

                $this->db->where('id',$itemId);
                $this->db->update('connected_data', $DBdata);
            }
        }
        $ConnectorTabsEncoded = macanta_get_config('connected_info');
        $ConnectorTabs = json_decode($ConnectorTabsEncoded, true);

        $UserConnectedInfo = macanta_get_connected_info($ContactId);
        $Order = [];
        $OrderId = [];
        $UserValue = [];
        foreach ($ConnectorTabs as $key => $ConnectorTab){
            if($ConnectorTab['id'] == $GUID) {
                $UserValue = isset($UserConnectedInfo[$ConnectorTab['id']]) ? $UserConnectedInfo[$ConnectorTab['id']]:[];
                foreach ($ConnectorTab['fields'] as $field) {
                    if ($field["showInTable"] == "yes") {
                        $key = (int)$field["showOrder"];
                        if (isset($Order[$key])) {
                            while (isset($order[$key])) {
                                $key++;
                            }
                            $Order[$key] = $field["fieldLabel"];
                            $OrderId[$key] = $field["fieldId"];
                        } else {
                            $Order[$key] = $field["fieldLabel"];
                            $OrderId[$key] = $field["fieldId"];
                        }
                    }
                }
                ksort($Order);
                ksort($OrderId);

                if(sizeof($Order) == 0){
                    $count = 0;
                    foreach ($ConnectorTab['fields'] as $field){
                        $count++;
                        $OrderId[] = $field["fieldId"];
                        if($count == 3) break;
                    }
                }
            }
        }

        $html = '';
        foreach ($UserValue as $itemId => $ValuesArr){
            $UserField = $ValuesArr['value'];
            $ConnectedContacts = $ValuesArr['connected_contact'];
            $html .=  "<tr class='infoItem' data-itemid='$itemId' >";
            foreach ($OrderId as $fieldName){
                if(is_array($UserField[$fieldName])){
                    $Val = isset($UserField[$fieldName]["id_".$ContactId]) ? $UserField[$fieldName]["id_".$ContactId]:'';
                }else{
                    $Val = $UserField[$fieldName];
                }
                $html .=  "<td>$Val</td>";
            }
            $html .=  "</tr>";
        }

        $this->ajaxResults['tbody'] = $html;
        $this->ajaxResults['data'] = $UserValue;
        $this->ajaxResults['message'] = "Info Saved";
        $this->ajaxResults['status'] = 1;
        unset($ConnectorTabsEncoded);
        unset($ConnectorTabs);
        return $this->ajaxResults;
    }
    public function saveUserConnectedInfo($data){
        $ToLog = $data;
        $ToLog['values']  = json_decode($ToLog['values'], true);
        $ToLog['ConnectedContacts']  = json_decode($ToLog['ConnectedContacts'], true);
        macanta_logger('saveUserConnectedInfo',json_encode($ToLog));
        $ContactId = $data['ContactId'];
        $itemId = $data['itemId'];
        $GUID = $data['GUID'];
        $session_name = $data['session_name'];
        $user_seession_data = macanta_get_user_seession_data($session_name);
        $session_data = unserialize($user_seession_data->session_data);
        $DBData = [];
        $this->db->where('id',$itemId);
        $query = $this->db->get('connected_data');
        $row = $query->row();


        //{"update_history":[{"date":"","from":"","to":""}],"connection_history":[{"date":"","contact_id":"","relationship_id":""}]}

        if (isset($row))
        {
            /*======check fields for differences========*/
            $DBdata = [];
            $DataOld = json_decode($row->value, true);
            $NewValuesArr = json_decode($data['values'], true);
            $DataModification = [];
            $ToDB = array_merge($DataOld, $NewValuesArr);
            $DataModified = false;
            $DataNew = macanta_array_update($ToDB, $DataOld, $DataModification, $DataModified);

            if($ToDB != $DataOld) {
                $DBData['value'] = json_encode($ToDB);
                macanta_cd_record_history($itemId, $DataModification, 'connected_data',$session_data);
            }

            /*=========================*/
            /*======check connection for differences========*/
            if($data['ConnectedContacts'] != ''){
                $OldValuesArr = json_decode($row->connected_contact, true);
                $NewValuesArr = json_decode($data['ConnectedContacts'], true);
                $ContactsModification = [];
                $ContactsModified = false;
                $ContactsNew = macanta_array_update($NewValuesArr, $OldValuesArr, $ContactsModification, $ContactsModified,'',true);
                if($ContactsNew!=$OldValuesArr) {
                    $DBData['connected_contact'] = json_encode($NewValuesArr);
                    macanta_cd_record_history($itemId, $ContactsModification, 'connected_contact',$session_data);
                }
            }
            /*=========================*/
            if(sizeof($DBData) >0 ){
                $this->db->where('id',$itemId);
                $this->db->update('connected_data', $DBData);

                if(sizeof($DataModification) > 0){
                    $group_details = macanta_get_connected_info_group_fields_map('',$GUID);
                    $ItemId = $itemId;
                    $queryConnectedDataType =  $group_details['title'];
                    foreach ($DataModification as $FieldId => $DataMods){
                        $queryCDFieldName = $group_details['fields'][$FieldId]['title'];//lowercase
                        if(isset($DataMods['from']) and isset($DataMods['to'])){
                            if(is_array($DataMods['from']) && is_array($DataMods['to'])){
                                foreach ($DataMods['from'] as $Contact => $FieldVal){
                                    if(isset($DataMods['to'][$Contact]) && $FieldVal != $DataMods['to'][$Contact]){
                                        //trigger this
                                        $ContactId = str_replace('id_','',$Contact);
                                        $ToLogs = macanta_trigger_automation_cd_amended($queryConnectedDataType,$queryCDFieldName,$ContactId,$ItemId);
                                        macanta_logger('CDAmendedAction',json_encode($ToLogs));
                                    }
                                }
                            }
                            elseif (!is_array($DataMods['from']) && !is_array($DataMods['to'])){
                                if($DataMods['from'] != $DataMods['to']){
                                    //trigger this
                                    $ToLogs = macanta_trigger_automation_cd_amended($queryConnectedDataType,$queryCDFieldName,$ContactId,$ItemId);
                                    macanta_logger('CDAmendedAction',json_encode($ToLogs));
                                }
                            }
                        }
                    }
                }
            }


        }else{
            $history["update_history"] = [];
            $history["connection_history"][] = array(
                "date"=>date("Y-m-d H:i:s"),
                "contact_id"=>$data['ContactId'],
                "relationships"=>$data['ConnectedContacts']
            );
            $DBdata = array(
                'id' => $data['itemId'],
                'group' => $data['GUID'],
                'value' => $data['values'],
                'connected_contact' => $data['ConnectedContacts'],
                'history' => json_encode($history),
                'meta' => '{"editable":"yes","searchable":"yes","multiple_link":"yes"}',
                'created' => date("Y-m-d H:i:s"),
                'status' => 'active'
            );
            $this->db->insert('connected_data', $DBdata);
        }
        $UserConnectedInfoToLocalStorage = macanta_get_connected_info($ContactId,$data['GUID']);
        $UserValue =  isset($UserConnectedInfoToLocalStorage[$data['GUID']]) ? $UserConnectedInfoToLocalStorage[$data['GUID']] : [];
        if(sizeof($UserValue) == 0){
            $stringify = "{}";
        }else{
            $stringify = json_encode($UserValue);
        }
        $ToStoreScript = 'localStorage.setItem("'.$data['GUID'].'", JSON.stringify('.$stringify.'));';

        $ConnectorTabsEncoded = macanta_get_config('connected_info');
        $ConnectorTabs = json_decode($ConnectorTabsEncoded, true);

        $Order = [];
        $OrderId = [];
        foreach ($ConnectorTabs as $key => $ConnectorTab){
            if($ConnectorTab['id'] == $GUID) {
                foreach ($ConnectorTab['fields'] as $field) {
                    if ($field["showInTable"] == "yes") {
                        $key = (int)$field["showOrder"];
                        if (isset($Order[$key])) {
                            while (isset($order[$key])) {
                                $key++;
                            }
                            $Order[$key] = $field["fieldLabel"];
                            $OrderId[$key] = $field["fieldId"];
                        } else {
                            $Order[$key] = $field["fieldLabel"];
                            $OrderId[$key] = $field["fieldId"];
                        }
                    }
                }
                ksort($Order);
                ksort($OrderId);

                if(sizeof($Order) == 0){
                    $count = 0;
                    foreach ($ConnectorTab['fields'] as $field){
                        $count++;
                        $OrderId[] = $field["fieldId"];
                        if($count == 3) break;
                    }
                }
            }
        }

        $html = '';
        /*foreach ($UserValue as $itemId => $ValuesArr){
            $UserField = $ValuesArr['value'];
            $ConnectedContacts = $ValuesArr['connected_contact'];
            $html .=  "<tr class='infoItem' data-itemid='$itemId'  data-connectedcontacts ='".str_replace("=","",base64_encode(json_encode($ConnectedContacts)))."' data-raw ='".str_replace("=","",base64_encode(json_encode($UserField)))."'>";
            $ShowItemId = true;
            foreach ($OrderId as $fieldName){
                $ItemId = '';
                if(is_array($UserField[$fieldName])){
                    $Val = isset($UserField[$fieldName]["id_".$ContactId]) ? $UserField[$fieldName]["id_".$ContactId]:'';
                }else{
                    $Val = $UserField[$fieldName];
                }
                if($ShowItemId) $ItemId = '<div class="cd-item-id">ID:<span>'.$itemId.'</span></div>';
                $html .=  "<td>$Val $ItemId</td>";
                $ShowItemId = false;
            }
            $html .=  "</tr>";
        }*/

        foreach ($UserValue as $itemId => $ValuesArr){
            $UserField = $ValuesArr['value'];
            $ConnectedContact = $ValuesArr['connected_contact'];
            foreach ($ConnectedContact as $ContactId => $ContactDetails){
                $this->db->select('Groups');
                $this->db->where('Id',$ContactId);
                $query = $this->db->get('InfusionsoftContact');
                $row = $query->row();
                if (isset($row))
                {
                    $Groups = $row->Groups;
                    if ($Groups != '' && checkContactRestriction($Groups,$session_data)) unset($ConnectedContact[$ContactId]); // skip this contact when restricted
                }
            }
            $html .=  "<tr class='infoItem' data-itemid='$itemId' >";
            $ShowItemId = true;
            foreach ($OrderId as $fieldName){
                $ItemId = '';
                if(is_array($UserField[$fieldName])){
                    $Val = isset($UserField[$fieldName]["id_".$ContactId]) ? $UserField[$fieldName]["id_".$ContactId]:'';
                    $Val = str_replace('|','<br>',$Val);
                }else{
                    $Val = $UserField[$fieldName];
                }
                if($ShowItemId) $ItemId = '<div class="cd-item-id">ID:<span>'.$itemId.'</span></div><div class="deleteDataObjectItemContainer"><button class=" nb-btn nb-primary icon-btn deleteDataObjectItem" data-id="'.$itemId.'" data-groupid="'. $ConnectorTab['id'] .'" data-type="dashboard"><i class="fa fa-trash-o" aria-hidden="true"></i></button></div>';
                $html .=   "<td><div class='DOtitle'>$Val</div>$ItemId</td>";
                $ShowItemId = false;
            }
            $html .=   "</tr>";
        }




        $this->ajaxResults['tbody'] = $html;
        $this->ajaxResults['ContactsNew'] = $ContactsNew;
        $this->ajaxResults['data'] = $UserValue;
        $this->ajaxResults['DataModification'] = $DataModification;
        $this->ajaxResults['message'] = "Info Saved";
        $this->ajaxResults['script'] = $ToStoreScript;
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['toLogs'] = $ToLogs;
        unset($ConnectorTabsEncoded);
        unset($ConnectorTabs);
        return $this->ajaxResults;
    }
    public function getCDFileAttachements($data){
        //$data = json_decode($data, true);
        macanta_create_table_connected_data_file_attachment();
        $itemId = $data['ItemId'];
        $db_record = [];
        $this->db->where('item_id',$itemId);
        $this->db->order_by('filename', 'ASC');
        $query = $this->db->get('connected_data_file_attachment');
        if (sizeof($query->result()) > 0){
            foreach ($query->result() as $row) {
                $db_record[$itemId][]=$row;
            }
        }
        //echo $this->db->last_query();
        $this->ajaxResults['db_record'] = $db_record;
        $this->ajaxResults['script'] = '';
        return $this->ajaxResults;
    }
    public function delCDFileAttachements($data){
        //$data = json_decode($data, true);
        $itemId = $data['ItemId'];
        $itemId = $data['ItemId'];
        $db_record = [];
        $this->db->where('item_id',$itemId);
        $this->db->order_by('filename', 'ASC');
        $query = $this->db->get('connected_data_file_attachment');
        if (sizeof($query->result()) > 0){
            foreach ($query->result() as $row) {
                $db_record[$itemId][]=$row;
            }
        }
        //echo $this->db->last_query();
        $this->ajaxResults['db_record'] = $db_record;
        $this->ajaxResults['script'] = '';
        return $this->ajaxResults;
    }
    public function putCDURLAttachments($data){
        //$data = json_decode($data, true);
        $itemId = $data['ItemId'];
        $theFileURL = $data['theFileURL'];
        //$theFileData = file_get_contents($theFileURL);
        $path_parts = pathinfo($theFileURL);
        $opts = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>"Accept-language: en\r\n" .
                    "Access-Control-Allow-Origin: *\r\n".
                    "Cookie: foo=bar\r\n"
            )
        );

        $context = stream_context_create($opts);
        $file_contents = file_get_contents($theFileURL, false, $context);
        $db_record = [];
        $db_insert = '';
        if($file_contents != false ){
            $filesize = strlen($file_contents);
            $thumb_info = createThumbnail($theFileURL, $itemId, false, $file_contents );
            $friendly_filename = empty($path_parts['extension']) ? $thumb_info['ThumbFileName']:$path_parts['filename'].".".$path_parts['extension'];
            $DBdata = [
                'item_id' => $itemId,
                'filename' => $friendly_filename,
                'thumbnail' => $thumb_info['data'],
                'download_url' => $theFileURL,
                'file_size' => $filesize,
                'timestamp' => time(),
                'b2_filename' => '',
                'b2_file_id' => '',
                'b2_timestamp' => '',
                'meta' => "{}"
            ];
            $db_insert = $this->db->insert('connected_data_file_attachment', $DBdata);


            $this->db->where('item_id',$itemId);
            $this->db->order_by('filename', 'ASC');
            $query = $this->db->get('connected_data_file_attachment');
            if (sizeof($query->result()) > 0){
                foreach ($query->result() as $row) {
                    $db_record[$itemId][]=$row;
                }
            }
            $this->ajaxResults['script'] = '$(".url-attachment").val("")';
            $this->ajaxResults['status'] = 'success';
        }
        else{
            $this->ajaxResults['script'] = 'alert("The File URL Entered Is Invalid!")';
            $this->ajaxResults['status'] = 'failed';
        }

        //echo $this->db->last_query();
        $this->ajaxResults['db_record'] = $db_record;

        $this->ajaxResults['db_insert'] = $db_insert;
        return $this->ajaxResults;
    }
    public function saveCDFileAttachement($data){
        $itemId = $data['itemId'];
        $uploads_dir = FCPATH . "assets/custom_img/";
        $tmp_name = $_FILES["CDFileAttachments"]["tmp_name"];
        $FriendlyName = $_FILES["CDFileAttachments"]["name"];
        if (!file_exists($uploads_dir)) {
            mkdir($uploads_dir, 0777, true);
        }
        $SafeName = str_replace(' ','-',basename($_FILES["CDFileAttachments"]["name"]));
        $FileSourcePath = "{$uploads_dir}{$SafeName}";

        $status = move_uploaded_file($tmp_name, $FileSourcePath);

        $thumb_info = createThumbnail($FileSourcePath, $itemId );
        $b2_upload_param = ['FileSourcePath'=>$FileSourcePath,'itemId'=>$itemId,'SafeName'=>$SafeName];
        $b2_upload = macanta_b2_upload($FileSourcePath, $itemId, $SafeName,"CD_ATTACHMENT");
        $b2_upload = json_decode($b2_upload,true);
        unlink($FileSourcePath);
        $b2_filename = empty($b2_upload['fileName']) ? "":$b2_upload['fileName'];
        $b2_file_id = empty($b2_upload['fileId']) ? "":$b2_upload['fileId'];
        $b2_timestamp = empty($b2_upload['uploadTimestamp']) ? "":$b2_upload['uploadTimestamp'];

        $DBdata = [
            'item_id' => $itemId,
            'filename' => $FriendlyName,
            'thumbnail' => $thumb_info['data'],
            'download_url' => 'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/'.$this->config->item('MacantaAppName').'/'.$itemId.'/'.$SafeName,
            'file_size' => $_FILES['CDFileAttachments']['size'],
            'timestamp' => time(),
            'b2_filename' => $b2_filename,
            'b2_file_id' => $b2_file_id,
            'b2_timestamp' =>$b2_timestamp,
            'meta' => "{}"
        ];
        $db_insert = $this->db->insert('connected_data_file_attachment', $DBdata);

        $db_record = [];
        $this->db->where('item_id',$itemId);
        $this->db->order_by('filename', 'ASC');
        $query = $this->db->get('connected_data_file_attachment');
        if (sizeof($query->result()) > 0){
            foreach ($query->result() as $row) {
                $db_record[$itemId][]=$row;
            }
        }
        $this->ajaxResults['data'] = $data;
        $this->ajaxResults['file_data'] = $_FILES['CDFileAttachments'];
        $this->ajaxResults['db_record'] = $db_record;
        $this->ajaxResults['status'] = $status;
        $this->ajaxResults['thumb_info'] = $thumb_info;
        $this->ajaxResults['b2_info'] = $b2_upload;
        $this->ajaxResults['b2_upload_param'] = $b2_upload_param;
        $this->ajaxResults['itemId'] = $itemId;
        $this->ajaxResults['friendly_name'] = $FriendlyName;
        $this->ajaxResults['db_insert'] = $db_insert;
        $this->ajaxResults['script'] = '';

        return $this->ajaxResults;
    }
    public function getConnectedDataByGroup($data){
        $GroupId = $data['GroupId'];
        /*For connector tab contents*/
        $ConnectorTabsEncoded = macanta_get_config('connected_info');
        $ConnectorTabs = json_decode($ConnectorTabsEncoded, true);
        $ConnectorTab = [];
        foreach ($ConnectorTabs as $key => $_ConnectorTab){
            if($_ConnectorTab['id'] === $GroupId){
                $ConnectorTab = $_ConnectorTab;
                break;
            }
        }
        ob_start();
        ?>
        <table id="UserConnectorInfoTableFilterResult" data-guid="<?php echo $GroupId ?>" class="table table-striped table-bordered <?php echo $GroupId ?>" cellspacing="0" width="100%"  style="width: 100%;">
            <thead>
            <?php
            $Order = [];
            $OrderId = [];
            foreach ($ConnectorTab['fields'] as $field){
                if($field["showInTable"] == "yes"){
                    $key = (int) $field["showOrder"];
                    if(isset($Order[$key])){
                        while (isset($order[$key])){
                            $key++;
                        }
                        $Order[$key] = $field["fieldLabel"];
                        $OrderId[$key] = $field["fieldId"];
                    }else{
                        $Order[$key] = $field["fieldLabel"];
                        $OrderId[$key] = $field["fieldId"];
                    }
                }
            }
            ksort($Order);
            ksort($OrderId);
            ?>
            <tr>
                <th>Add As</th>
                <?php
                if(sizeof($Order) > 0){
                    foreach ($Order as $field){
                        echo "<th>$field</th>";
                    }
                }else{
                    $count = 0;
                    foreach ($ConnectorTab['fields'] as $field){
                        $count++;
                        $OrderId[] = $field["fieldId"];
                        echo "<th>$field[fieldLabel]</th>";
                        if($count == 3) break;
                    }
                }
                ?>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
        <?php
        $HTML = ob_get_contents();
        ob_end_clean();
        $AllContact = [];
        $ContainedIds = [];
        $ConnectedData = macanta_get_connected_info_by_group_id($GroupId,$data['ContactId']);
        $UserValue = $ConnectedData[$GroupId];
        foreach ($UserValue as $itemId => $ValuesArr){
            $ConnectedContact = $ValuesArr['connected_contact'];
            foreach ($ConnectedContact as $ContactId=>$theRalation){
                if($theRalation['ContactId'] === $data['ContactId']) continue;
                if(in_array($theRalation['ContactId'],$ContainedIds)) continue;
                $ContainedIds[] = $theRalation['ContactId'];
                $AllContact[$theRalation['FirstName']] = $theRalation;
            }


        }
        ksort($AllContact);
        $this->ajaxResults['html'] = $HTML;
        $this->ajaxResults['allcontact'] = $AllContact;
        $this->ajaxResults['message'] = "Connected Data Returned";
        $this->ajaxResults['status'] = 1;
        unset($ConnectorTabsEncoded);
        unset($ConnectorTabs);
        unset($ConnectedData);
        return $this->ajaxResults;
    }
    public function saveDefaultCallerId($data){
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $this->db->where('user_id',$session_data['InfusionsoftID']);
        $this->db->where('meta_key','caller_id_default');
        $query = $this->db->get('users_meta');
        $row = $query->row();
        $callerId = $data['PhoneNumber'];

        if (isset($row))
        {
            $this->db->where('user_id',$session_data['InfusionsoftID']);
            $this->db->where('meta_key','caller_id_default');
            $DBdata['meta_value'] = trim($callerId);
            $this->db->update('users_meta', $DBdata);
        }else{
            $DBdata['user_id'] = $session_data['InfusionsoftID'];
            $DBdata['meta_key'] = 'caller_id_default';
            $DBdata['meta_value'] = trim($callerId);
            $this->db->insert('users_meta', $DBdata);
        }
        return $this->ajaxResults;
    }
    public function saveDefaultOutboundDevice($data){
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $this->db->where('user_id',$session_data['InfusionsoftID']);
        $this->db->where('meta_key','outbound_device_default');
        $query = $this->db->get('users_meta');
        $row = $query->row();
        $OutboundDevice = $data['OutboundDevice'];
        if (isset($row))
        {
            $this->db->where('user_id',$session_data['InfusionsoftID']);
            $this->db->where('meta_key','outbound_device_default');
            $DBdata['meta_value'] = trim($OutboundDevice);
            $this->db->update('users_meta', $DBdata);
        }else{
            $DBdata['user_id'] = $session_data['InfusionsoftID'];
            $DBdata['meta_key'] = 'outbound_device_default';
            $DBdata['meta_value'] = trim($OutboundDevice);
            $this->db->insert('users_meta', $DBdata);
        }
        return $this->ajaxResults;
    }

    public function addNote($data)
    {
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $UserInfo = json_decode($session_data['Details']);
        //check if call recording is enabled or included
        if (in_array('CallRecord', $session_data['twillioFeatures'])) {
            $CallId = trim($data['call_id']);
            $account_sid = $this->config->item('Twilio_Account_SID');
            $auth_token = $this->config->item('Twilio_TOKEN');
            $client = new Twilio\Rest\Client($account_sid, $auth_token);
            $RecordingsId = array();
            // Loop till it get the recording Id, because twillio has a delay of returning the Id
            $counter = 0;
            $callInfo = '';
            while (sizeof($RecordingsId) == 0) {
                sleep(1);
                $call = $client->calls($CallId)->fetch();
                $recordings = $client->recordings->read(
                    array(
                        "callSid" => $CallId,
                    )
                );
                foreach ($recordings as $recording) {
                    $callInfo = $call;
                    $RecordingsId[] = $recording->sid;
                }
                $counter++;
                if ($counter > 5) break;
            }

            if (sizeof($RecordingsId) == 0) return false;

            $recordings_sid = $RecordingsId[0];
            $data['plainNotes'] = '';
            if(is_object($callInfo)){
                $data['plainNotes'] = "<p class='call-note'>Caller Id Used: ".$data['CallerIdUsed']."</p><p class='call-note'>Device Used: ".$data['DeviceUsed']."</p><p class='call-note'>Status: ".$callInfo->status."</p><p class='call-note'>Duration: ".$callInfo->duration."</p>";
            }
            $data['notes']['Notes'] = base64_encode($data['plainNotes']);
            $data['acct_id'] = $account_sid;
            $data['rec_id'] = $recordings_sid;
        } else {
            unset($data['call_id']);
        }

        $SaveResult = infusionsoft_add_note($data, $UserInfo);
        if(!empty($data['tags'])){
            $tag_data['note_id'] = $SaveResult->message;
            $tag_data['tags'] = $data['tags'];
            $tag_data['conId'] = $data['conId'];
            $this->updateTagNotes($tag_data);
        }
        $Notes = infusionsoft_get_contact_notes_by_contact_id($data['conId']);
        $availableTags = $this->notetags_get();
        $tabNotes['PrevNotes'] = $this->parseNoteNoteTab($Notes,$data['conId'],$session_data);
        $EditorNoteTab = $this->load->view('core/call_add_quick_note', $tabNotes, true);
        $this->ajaxResults['data']['note'] = $EditorNoteTab;
        $this->ajaxResults['data']['request'] = $data;
        $this->ajaxResults['message'] = $SaveResult;
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['script'] = 'noteCollapsible();'.'availableTags=' . $availableTags . '; Macanta("tabs/Call addnote"); toggleThisTaskInside();';
        return $this->ajaxResults;
    }
    public function updateTagNotes($data)
    {
        $NoteId = $data['note_id'];
        $tagsStr = $data['tags'];
        $contactId = $data['conId'];
        //$SelectedContactResult = storeTaggedSelectedContact($data);
        $DBdata = array(
            'tag_slugs' => $tagsStr
        );
        if (false == $this->DbExists('note_id', $NoteId, 'note_tags')) {
            $DBdata['note_id'] = $NoteId;
            $DBdata['contact_id'] = $contactId;
            $this->db->insert('note_tags', $DBdata);
        } else {
            $this->db->where('note_id', $NoteId);
            $this->db->update('note_tags', $DBdata);
        }
        $tagsArr = explode(',', $tagsStr);
        $tags_display = '';
        if ($tagsStr && $tagsStr !== '') {
            foreach ($tagsArr as $tag_item) {
                $tags_display .= "<span>$tag_item</span>";
            }
        } else {
            $tags_display = '- ' . $this->lang->line('text_no_tags') . ' -';
        }
        $tag_script = '$("tr[data-noteid=' . $NoteId . '] .tagDisplay").html("' . $tags_display . '")';
        foreach ($tagsArr as $tag) {
            $tagSlug = strtolower($tag);
            $tagName = $tag;
            $tagData = $this->DbExists('tag_slug', $tagSlug, 'tags');
            if (!$tagData) {
                /*Save Unique Tag To DB*/
                $DBdata = array(
                    'tag_slug' => $tagSlug,
                    'tag_name' => $tagName
                );
                $this->db->insert('tags', $DBdata);
            }

        }
        $availableTags = $this->notetags_get();
        //update available tags
        $this->ajaxResults['script'] = 'availableTags=' . $availableTags . '; ' . $tag_script . ';';
        $this->ajaxResults['data'] = $data;
        $this->ajaxResults['message'] = 'Tag Saved, ';
        $this->ajaxResults['status'] = 1;


        return $this->ajaxResults;
    }
    public function parseNoteNoteTab($Notes,$ContactId,$session_data)
    {
        $TotalNotesArr = array();
        $NoteCustomFields = json_decode($this->config->item('NoteCustomFields'), true);
        $CF_RecordingURL = '_' . $NoteCustomFields['Call Recording URL'];
        $CF_MacantaNotes = '_' . $NoteCustomFields['JSON'];
        $text['text_edit_note'] = $this->lang->line('text_edit_note');
        $text['text_save_notes'] = $this->lang->line('text_save_notes');
        $text['text_cancel'] = $this->lang->line('text_cancel');
        $Types = infusionsoft_get_settings('ContactAction', 'optionstype');
        $Types = explode(',', $Types->message);


        $NoteEditingPermissions = $this->config->item('note_editing_permission');
        $NoteEditingPermissionsArr = $NoteEditingPermissions ? explode(',',$NoteEditingPermissions):[];
        if(isset($session_data['Groups']) && trim($session_data['Groups']) != ''){
            $UserGroupsArr = explode(',',$session_data['Groups']);

        }else{
            $UserGroupsArr = [];
        }
        $HideEditNote = "hideThis";
        if(sizeof($NoteEditingPermissionsArr) > 0){
            foreach ($NoteEditingPermissionsArr as $Tag){
                if(in_array($Tag,$UserGroupsArr)){
                    $HideEditNote = "";
                    break;
                }
            }
        }else{
            $HideEditNote = "";
        }

        $IS_Users = macanta_get_users();
        foreach ($Notes as $Note) {
            $TypesOption = '';
            $NativeNotice = '';
            if($Note->Origin == 'Native'){
                $NativeNotice = '(Macanta Native Note)';
            }
            foreach ($Types as $Type) {
                $TypeSelected = $Note->ActionType == $Type ? 'selected' : '';
                $TypesOption .= "<option value='$Type' $TypeSelected>$Type</option>";
            }
            $noteId = $Note->Id;
            if(!empty($Note->CreationDate)){
                $Note->CreationDate->date = date('Y-m-d H:i:s', strtotime($Note->CreationDate->date) + 18000);
                $Note->CreationDate->date = _date('Y-m-d H:i:s', strtotime($Note->CreationDate->date), $session_data['TimeZone']);
            }else{
                $Note->CreationDate = new stdClass();
                $Note->CreationDate->date = date('Y-m-d H:i:s');;
            }
            if(isset($Note->ActionDate)){
                $Note->ActionDate->date = date('Y-m-d', strtotime($Note->ActionDate->date) + 18000);
                $Note->ActionDate->date = _date('Y-m-d', strtotime($Note->ActionDate->date), $session_data['TimeZone']);
            }else{
                $Note->ActionDate = new stdClass();
                $Note->ActionDate->date = '';
            }
            // Get Tags
            $this->db->where('note_id', $noteId);
            $this->db->where('tag_slugs !=', '');
            $query = $this->db->get('note_tags');
            $ret = $query->row();
            if($ret){
                $tags = $ret->tag_slugs;
                $tags_displayArr = explode(',', $tags);
                $tags_display = '';
                if ($tags && $tags !== '') {
                    foreach ($tags_displayArr as $tag_item) {
                        $tags_display .= "<span>$tag_item</span>";
                    }
                } else {
                    $tags_display = '- ' . $this->lang->line('text_no_tags') . ' -';
                }
            }else{
                $tags_display = '- ' . $this->lang->line('text_no_tags') . ' -';
            }

            $ObjectType = strtolower($Note->ObjectType);
            $Note->ActionDescription = mb_convert_encoding($Note->ActionDescription, "HTML-ENTITIES", "UTF-8");
            $ActionDescription = $Note->ActionDescription; // not used

            $MacantaNotes = json_encode($Note->$CF_MacantaNotes);
            /*if (isset($Note->$CF_MacantaNotes) && trim($Note->$CF_MacantaNotes) != '') {
                $MacantaNotes = trim(html_entity_decode($Note->$CF_MacantaNotes));
            }*/
            $RecordingURL = '';
            if (isset($Note->$CF_RecordingURL) && trim($Note->$CF_RecordingURL) != '' && trim($Note->$CF_RecordingURL) != "null") {
                $RecordingURL = trim($Note->$CF_RecordingURL);
            }
            $note = mb_convert_encoding($Note->CreationNotes, "HTML-ENTITIES", "UTF-8");
            //$note = trim(html_entity_decode($Note->CreationNotes));
            // Check if Old Data Format
            if (strpos($note, "== macanta data: do not edit below this line ==") !== false) {
                $noteArr = explode("== macanta data: do not edit below this line ==", $note);
                $MacantaNotes = trim($noteArr[1]);
            }
            if ($Note->ObjectType != 'Task' && ($MacantaNotes != '' || $RecordingURL != '')) {
                //$MacantaNotes = trim(html_entity_decode($MacantaNotes));
                $noteObj = json_decode($MacantaNotes);
                $CallType = isset($noteObj->note_type) ? $noteObj->note_type : '';
                $theVal = is_base64_encoded($noteObj->note->Notes) ? base64_decode($noteObj->note->Notes) : $noteObj->note->Notes;
                $theVal = str_replace("\n", "<br>", $theVal);
                $theVal = mb_convert_encoding($theVal, "HTML-ENTITIES", "UTF-8");
                $Identity = $CallType == 'call_notes' ? '<i class="fa fa-volume-control-phone" aria-hidden="true"></i>' : '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>';
                $rows = '
                            <article class="col-md-12  noteCollapsible">
                            <span class="noteLabel"><small>
                            <span class="show-editor  '.$HideEditNote.'   btn-link">' . $Identity . ' ' . $text['text_edit_note'] . '</span>
                            <span class="close-editing  btn-link"><i class="fa fa-floppy-o" aria-hidden="true"></i> ' . $text['text_save_notes'] . '</span>
                            <span class="cancel-editing  btn-link"><i class="fa fa-stop-circle-o" aria-hidden="true"></i> ' . $text['text_cancel'] . '</span></small></span>
                            <div id="noteTitle">' . $Note->ActionDescription . '</div>
                            <div class="col-md-12  no-pad-right no-pad-left">
                            <div class="col-md-6 no-pad-left NoteTitleEditorContainer">
                                <h3 class="NoteTitleEditorLabel">Title:</h3>
                                <input name="NoteTitleEditor" class="form-control NoteTitleEditor" value="' . $Note->ActionDescription . '">
                            </div>
                            <div class="col-md-6  no-pad-right no-pad-left NoteTypeEditorContainer">
                                <h3>Type:</h3>
                                <select name="NoteTypeEditor" id="NoteTypeEditor" class="form-control  NoteTypeEditor">
                                <option value="">Please select an action type</option>
                                ' . $TypesOption . '
                                </select>
                            </div>
                            </div>
                            
                    
                            <div class="col-md-12  no-pad-right no-pad-left NoteDescriptionEditorContainer">
                            <h3 class="NoteDescriptionEditorLabel">Description:</h3>
                            <div id="noteTextRich' . rand() . '" class=" noteTextRich" data-tinymce="true">' . $theVal . '</div>
                            </div>
                            </article>';
                $TheNote['rows'] = $rows;
                //if ($noteObj->rec_id != "") {
                if ($RecordingURL != "") {
                    //$URL = "https://api.twilio.com/2010-04-01/Accounts/" . $noteObj->acct_id . "/Recordings/" . $noteObj->rec_id . ".mp3";
                    // use this because when note is updated rec_id is omitted and no Recoding url will be seen
                    $URL = $RecordingURL;
                    $callrecording = '<a class="callrecording" target="_blank" href="' . $URL . '"><i class="glyphicon glyphicon-earphone"></i> ' . $this->lang->line('text_call_recording') . '</a>';
                } else {
                    $callrecording = $this->lang->line('text_no_call_record');
                }
                $staffArr = explode(' ', $noteObj->user);

                if ($CallType == "call_notes" || $CallType == 'quick_notes') {
                    $callrecording = $CallType == 'quick_notes' ? $this->lang->line('text_quick_note') : $callrecording;

                    $TotalNotesArr[] = "<tr class='NoteItem' data-noteid='" . $noteId . "'  data-staffid='" . $Note->CreatedBy . "'  data-timestamp='" . strtotime($Note->CreationDate->date) . "'   data-tags='" . $tags . "' data-notetype='" . $CallType . "'>
                <td class='noteDate'  data-callid='" . $noteObj->call_id . "' colspan='2' >
                <span  class='noteDate'  data-isdate='" . $Note->CreationDate->date . "'   data-isdatedetailed='" . json_encode($Note->CreationDate) . "'>" . date('D d M Y h:iA', strtotime($Note->CreationDate->date)) . "<small> " . $callrecording . " &nbsp;&nbsp;</small><small class='staffname'>" . $staffArr[0] . ' ' . substr($staffArr[1], 0, 1) . ".</small></span>"
                        . $TheNote['rows'] .
                        "<i class='fa fa-tags' aria-hidden='true'></i>" . "
                <input id='tags_" . rand() . "' type='text' data-noteid='" . $noteId . "' class='note-tags' value='" . $tags . "' />" .
                        '<div class="tagDisplay">' . $tags_display . '</div></td></tr>';
                }


            }
            elseif ($this->config->item('TurboDial_CallRecordingURL_Field'))
            {
                /*Save Note if not existing*/
                if(!isset($ret)){
                    $ExistingTagStr = '';
                    $tags = $this->getNoteTag($noteId);
                    $ExistingTagArr = $tags == '' ? []:explode(',',$tags);
                    if(!in_array('#inbound_call',$ExistingTagArr)){
                        $ExistingTagArr[] = '#inbound_call';
                        $ExistingTagStr = implode(',',$ExistingTagArr);
                        $data['note_id'] = $noteId;
                        $data['tags'] = $ExistingTagStr;
                        $data['conId'] = $ContactId;
                        $this->updateTagNotes($data);
                    }
                    foreach ($ExistingTagArr as $tag_item) {
                        $tags_display .= "<span>$tag_item</span>";
                    }
                }
                $CallType ='call_notes';
                $theVal = $Note->CreationNotes;
                $Identity = '<i class="fa fa-volume-control-phone" aria-hidden="true"></i>';
                $rows = '
                            <article class="col-md-12  noteCollapsible">
                            <span class="noteLabel"><small>
                            <span class="show-editor   '.$HideEditNote.'  btn-link">' . $Identity . ' ' . $text['text_edit_note'] . '</span>
                            <span class="close-editing  btn-link"><i class="fa fa-floppy-o" aria-hidden="true"></i> ' . $text['text_save_notes'] . '</span>
                            <span class="cancel-editing  btn-link"><i class="fa fa-stop-circle-o" aria-hidden="true"></i> ' . $text['text_cancel'] . '</span></small></span>
                            <div id="noteTitle">' . $Note->ActionDescription . '</div>
                            <div class="col-md-12  no-pad-right no-pad-left">
                            <div class="col-md-6 no-pad-left NoteTitleEditorContainer">
                                <h3 class="NoteTitleEditorLabel">Title:</h3>
                                <input name="NoteTitleEditor"   class="form-control NoteTitleEditor" value="' . $Note->ActionDescription . '">
                            </div>
                            <div class="col-md-6  no-pad-right no-pad-left NoteTypeEditorContainer">
                                <h3>Type:</h3>
                                <select name="NoteTypeEditor" id="NoteTypeEditor" class="form-control  NoteTypeEditor">
                                <option value="">Please select an action type</option>
                                ' . $TypesOption . '
                                </select>
                            </div>
                            </div>
                            
                            <div class="col-md-12  no-pad-right no-pad-left NoteDescriptionEditorContainer">
                            <h3 class="NoteDescriptionEditorLabel">Description:</h3>
                            <div id="noteTextRich' . rand() . '" class=" noteTextRich" data-tinymce="true">' . $theVal . '</div>
                            </div>
                            </article>';
                $TheNote['rows'] = $rows;
                $TurboDial_CallRecordingURL_Field = $this->config->item('TurboDial_CallRecordingURL_Field');
                $URL = $Note->$TurboDial_CallRecordingURL_Field;
                $callrecording = '<a class="callrecording" target="_blank" href="' . $URL . '"><i class="glyphicon glyphicon-earphone"></i> ' . $this->lang->line('text_call_recording') . '</a>';
                $staffArr = "Created By User Id: ".$Note->CreatedBy;

                $TotalNotesArr[] = "<tr class='NoteItem' data-noteid='" . $noteId . "'  data-staffid='" . $Note->CreatedBy . "'  data-timestamp='" . strtotime($Note->CreationDate->date) . "'   data-tags='" . $tags . "' data-notetype='" . $CallType . "'>
                    <td class='noteDate'  colspan='2' >
                    <span  class='noteDate'  data-isdate='" . $Note->CreationDate->date . "'   data-isdatedetailed='" . json_encode($Note->CreationDate) . "'>" . date('D d M Y h:iA', strtotime($Note->CreationDate->date)) . "<small> " . $callrecording . " &nbsp;&nbsp;</small><small class='staffname'>" . $staffArr. ".</small></span>"
                    . $TheNote['rows'] .
                    "<i class='fa fa-tags' aria-hidden='true'></i>" . "
                    <input id='tags_" . rand() . "' type='text' data-noteid='" . $noteId . "' class='note-tags' value='".$tags."' />" .
                    '<div class="tagDisplay"><span>' . $tags_display . '</span></div></td></tr>';
            }
            else {
                //$CallType = 'Legacy notes';
                $CallType = 'Infusionsoft ' . $this->lang->line('text_' . $ObjectType);
                $ActionDate = '';
                if(strtolower($ObjectType) == 'task'){
                    $ActionDate = "<span class='action-date'>Action Date: ".$Note->ActionDate->date."</span>";
                }
                if (strstr(strtolower($ActionDescription), 'changed info for')) continue;
                $note = str_replace("\n", "<br>", $note);
                $AssignedToText = '';
                $Toggles = '';

                if ($this->config->item('TaskCustomFields')) {
                    $TaskCustomFields = json_decode($this->config->item('TaskCustomFields'), true);
                    $TaskAssignedTo = '_' . $TaskCustomFields['Task assigned to'];
                    if (isset($Note->$TaskAssignedTo) && $Note->$TaskAssignedTo !== ''){
                        $AssignedToText = '<div class="col-md-12 assigned-to">Assigned To: <strong>' . $Note->$TaskAssignedTo . '</strong></div>';
                    }else{
                        foreach ($IS_Users as $IS_User){
                            if($IS_User->Id == $Note->UserID){
                                $AssignedToText = '<div class="col-md-12 assigned-to">Assigned To: <strong>' . $IS_User->FirstName .' '. $IS_User->LastName. '</strong></div>';
                                break;
                            }
                        }
                    }

                }
                if($Note->ObjectType == 'Task'){
                    //add complete toggle
                    $AssignBy = 'System '.$NativeNotice;
                    foreach ($IS_Users as $IS_User){
                        if($IS_User->Id == $Note->UserID){
                            $AssignBy = $IS_User->FirstName .' '. $IS_User->LastName;
                            break;
                        }
                    }
                    //$CallType = 'Assign by ' . $this->lang->line('text_' . $ObjectType);
                    $CallType = 'Assign by: ' . $AssignBy;
                    $CompletionDate = isset($Note->CompletionDate) ? $Note->CompletionDate->date:"";
                    $Toggles = '<div data-contactid="'.$ContactId.'" data-completiondate="'.$CompletionDate.'" data-noteid="'.$noteId.'" class="toggleThisTaskInside toggle-iphone"></div>';
                }
                $rows = '<div class="col-md-12"><div class="Notes noteTextRich" >' . $note . '</div></div>'.$Toggles.$AssignedToText;
                $TotalNotesArr[] = "<tr class='NoteItem' data-noteid='" . $noteId . "'  data-staffid='" . $Note->CreatedBy . "'  data-timestamp='" . strtotime($Note->CreationDate->date) . "'   data-tags='" . $tags . "' data-notetype='" . $CallType . "'>
                <td class='noteDate'  data-callid='' colspan='2' >
                <span  class='noteDate'  data-isdate='" . $Note->CreationDate->date . "'   data-isdatedetailed='" . json_encode($Note->CreationDate) . "'>" . date('D d M h:iA', strtotime($Note->CreationDate->date)) . " <small>" . $CallType . ' - ' . $ActionDescription .  $ActionDate."</small></span>"
                    . $rows .
                    "<i class='fa fa-tags' aria-hidden='true'></i>" . "
                <input id='tags_" . rand() . "' type='text' data-noteid='" . $noteId . "' class='note-tags' value='" . $tags . "' />" .
                    '<div class="tagDisplay">' . $tags_display . '</div></td></tr>';
            }
        }

        $TotalNotesArr = array_reverse($TotalNotesArr);
        $TotalNotes = implode("\r\n", $TotalNotesArr);
        $PrevNotes = '<table>' . $TotalNotes . '</table>';
        return $PrevNotes;
    }
    public function notetags_get()
    {
        $tags = array();
        $tagsStr = array();
        $query = $this->db->get('tags');
        foreach ($query->result() as $row) {
            $tagsStr[] = $row->tag_name;
            $tags[] = array(
                'id' => $row->tag_slug,
                'label' => $row->tag_name,
                'value' => $row->tag_name
            );
        }
        //echo  json_encode($tags);
        return json_encode($tagsStr);
    }
    public function DbExists($field, $value, $table)
    {
        $this->db->where($field, $value);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {

            foreach ($query->result() as $row) {
                return $row;
            }
            return false;
        } else {

            return false;
        }
    }
}
