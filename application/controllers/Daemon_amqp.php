<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Daemon_amqp extends CI_Controller
{
    public $BUILD;
    public $IDEN;
    public $SERVER_ID;
    public $SERVICE_NAME;
    public $SERVER_LOG;
    public $AppName;
    public $LastProcIdFile;
    public $StdoutFile;
    public $ProcessLog;
    public $PROCS = [];
    public $MessageTable = 'campaign_requests';
    private static $instance;
    public function __construct(){
        parent::__construct();
        ini_set('memory_limit', '2048M');
        $CI =& get_instance();
        foreach($CI->Siteconfig->get_all()->result() as $site_config)
        {
            $CI->config->set_item($site_config->key,$site_config->value);
        }
        $this->load->database();
        $this->load->dbforge();

        /*Crate Table If not exist*/
        $fields = array(
            'message' => array(
                'type' => 'TEXT',
                'null' => TRUE,
            ),
            'date_received' => array(
                'type' => 'DATETIME',
                'null' => TRUE,
            ),
            'status' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'default' => 'ready',
            ),
        );
        $this->dbforge->add_field("id");
        $this->dbforge->add_field($fields);
        $this->dbforge->create_table($this->MessageTable, true);

        $this->config->load('version', FALSE, TRUE);
        self::$instance =& $this;

        $this->BUILD = $this->config->item('macanta_verson');
        $this->IDEN = "macanta";
        $this->SERVER_ID = $this->config->item('MacantaAppName');;
        $this->SERVICE_NAME = 'consumer';
        $this->SERVER_LOG = SERVICEPATH.'apps';
    }
    public static function &get_instance()
    {
        return self::$instance;
    }

    public function start_campaign(){
        $request = "start-campaign";
        $this->producer($request);
    }
    public function set_next_sequence(){
        $request = "set-next-sequence";
        $this->producer($request);
    }
    public function producer($request){
        header("Content-Type: text/plain");
        error_reporting(E_ALL);
        set_time_limit(0);

        $RequiredParam = [
            "start-campaign"=>
                [
                    "campaignId",
                    "connected_data",
                    "relationship"
                ],
            "set-next-sequence"=>
                [
                    "contactId",
                    "currentSequence",
                    "nextSequence"
                ]
        ];

        $MessageStr = $this->input->raw_input_stream;
        $MessageArr = [];

        $MessageArrTemp = [];
        if($this->isJson($MessageStr)){
            $MessageArr = json_decode($MessageStr, true);
        }else{
            $MessageStr = str_replace('.','_dot_',$MessageStr);
            parse_str($MessageStr,$MessageArr);
        }
        foreach ($MessageArr as $theKey => $theValue){
            $theKey =  str_replace('_dot_','.',$theKey);
            $theValue =  str_replace('_dot_','.',$theValue);
            $MessageArrTemp[$theKey] = $theValue;
        }
        $MessageArr=$MessageArrTemp;
        $RequiredItems = $RequiredParam[$request];
        $Error = [];
        foreach ($RequiredItems as $RequiredItem){
            if(!array_key_exists($RequiredItem,$MessageArr)) {
                $Error[] = "Bad Request: missing '$RequiredItem'";
            }
        }
        if(sizeof($Error ) == 0){
            $Work = ['request'=>str_replace('-','_',$request),'data'=>$MessageArr];
            $message = json_encode($Work);
            /*Store Messages*/
            $DBData = [
                "message" => $message,
                "date_received" => date("Y-m-d H:i:s"),
                "status" => "ready"
            ];
            if(false == macanta_db_record_exist('message',$message,$this->MessageTable))
                $this->db->insert($this->MessageTable, $DBData);
            $this->output->set_status_header(200); // ok;
            $this->output->set_output($message);
        }else{
            $Error = implode("\n", $Error);
            $this->output->set_status_header(400); // ok;
            $this->output->set_output($Error."\nPassed Data: \n". print_r($MessageArr, true));
        }

        //$this->output->_display();
    }
    private function consumer_init($config){

        echo '['.gmdate('Y-m-d H:i:s').'] >> Script Version: '.$this->BUILD."\n";
        if(defined('CMD') AND (CMD == 1) ){
            $this->as_service = true;
            $this->last_db_log = 0;
            $this->verbose	=	(isset($config['verbose']))?$config['verbose']:false;
            if(is_array($_SERVER['argv']) AND in_array('--verbose', $_SERVER['argv']) ) $this->verbose = true;

            echo '['.gmdate('Y-m-d H:i:s').'] >> Service Started in '.ENVIRONMENT.' PID: '.getmypid()."\n";
            $this->logit_init(true);

        }else{
            //If ran from web browser ONLY ALLOW IN NON PRODUCTION
            if(ENVIRONMENT == 'production'){
                $this->logit('SERVICE', 'WARNING: Access DENIED for IP Address: '.$_SERVER['REMOTE_ADDR'].' in PRODUCTION');
                die('Not allowed in PRODUCTION');
            }
            $this->as_service = false;
            $this->verbose	  = true;
            echo '['.gmdate('Y-m-d H:i:s').'] Service Started in '.ENVIRONMENT.' w/ PID: '.getmypid();
            $this->logit('SERVICE', 'WARNING: WEB Access granted in ['.ENVIRONMENT.'] enviroment for IP Address: '.$_SERVER['REMOTE_ADDR'].', access will be denied in PRODUCTION');

        }
        $this->logit('SERVICE', '-------------------------------------------------');
        $this->logit('SERVICE', 'Running Macanta Consumer in version: '.$this->BUILD);
        $this->logit('SERVICE', 'Environment '.strtoupper(ENVIRONMENT));
        $this->debug	=	(isset($config['debug']))?$config['debug']:false;
    }
    public function consumer($appname){
        $this->AppName = $appname;
        $this->LastProcIdFile = $this->SERVER_LOG.'/' . $appname . '/LastProcId.log';
        $this->StdoutFile = $this->SERVER_LOG."/".$appname."/stdout.log";
        $this->ProcessLog = $this->SERVER_LOG."/".$appname."/process.log";
        $config = array();
        $config['name'] = $this->config->item('DAEMON_SERVICE_NAME');
        $config['verbose'] = $this->config->item('DAEMON_SERVICE_VERBOSE');
        $config['MacantaAppName'] = $this->config->item('MacantaAppName');
        $this->consumer_init($config);
        $this->logit('SERVICE', 'Macanta Consumer Service Started sucessfully w PID: '.getmypid());
        $this->get_message();
        exit;
    }
    function getProcId(){
        $ProcId = 100001;
        if (!file_exists($this->LastProcIdFile)){
            file_put_contents($this->LastProcIdFile, $ProcId);
        }else{
            $ProcId = file_get_contents($this->LastProcIdFile);
            $ProcId++;
            file_put_contents($this->LastProcIdFile, $ProcId);
        }
        return $ProcId;
    }
    function get_message(){
        global $servicepleasestop;
        if((isset($servicepleasestop) && $servicepleasestop === true)) return false;

        $this->db->where('status','ready');
        $query = $this->db->get($this->MessageTable);
        if (sizeof($query->result()) > 0){
            $StdoutMessage =  "[".date("Y-m-d H:i:s")."] >> Batch Process Running In The Background\n";
            file_put_contents($this->StdoutFile, $StdoutMessage, FILE_APPEND);
            foreach ($query->result() as $row) {
                $message = $row->message;
                $this->process_message($message,$this->getProcId());
                // Remove consumed messages
                $this->db->where('id', $row->id);
                $this->db->delete ($this->MessageTable);
            }
            $StdoutMessage =  "[".date("Y-m-d H:i:s")."] >> TaskManager: Batch Process Done!\n\n";
            file_put_contents($this->StdoutFile, $StdoutMessage, FILE_APPEND);
        }
        return true;
    }
    public function sequence_trigger(){
        set_time_limit(180);
        echo "[".date('Y-m-d H:i:s')."]".' >> Run: infusionsoft_trigger_move_to_next_sequence'."\n";
        $pidfile = fopen($this->SERVER_LOG.'/'.$this->SERVER_ID.'/trigger.pid', 'w');
        fwrite($pidfile, getmypid());
        fclose($pidfile);
        infusionsoft_trigger_move_to_next_sequence();
        gc_collect_cycles();
        exit;
    }
    public function process_message($message,$ProcId)
    {
        if($this->as_service){
            $this->logit_init();
            $encodedBody =  base64_encode($message);
            $Log = "";
            $Log .= "Script Version: ".$this->BUILD."\n";
            $Log .= "App Name: ".$this->AppName."\n";
            $Log .= "Environment: ".ENVIRONMENT."\n";
            $Log .= "CI_ENV: ".$_SERVER['CI_ENV']."\n";
            $Log .= "Date Processed: ".date("Y-m-d H:i:s")."\n";
            $Log .= "ProcId: ".$ProcId."\n";
            $Log .= "Data: \n";
            $Log .= $message."\n";
            try {
                $ProcResults = infusionsoft_connected_data_process($encodedBody);

            } catch (Exception $e) {
                $ProcResults = 'Caught exception: '.  $e->getMessage(). "\n";
                $ProcResults .= 'Error From Post: '.$message."\n";
                }
            $Log .= "Process Results: \n";
            $Log .= json_encode($ProcResults)."\n";
            $Log .= "======================================\n";
            file_put_contents($this->ProcessLog, $Log, FILE_APPEND);
        }
    }
    public function logit_init($init = false){
        if($init OR ($this->day != gmdate('Ymd')) ){
            $this->day	=	gmdate('Ymd');
            $dir = $this->SERVER_LOG.'/'.$this->SERVER_ID.'/';
            if(!is_dir($dir)){
                mkdir($dir, 0777, true );
                sleep(1);// preven lock before continue
                $wasdir = false;
            }else{
                $wasdir = true;
            }
            if(isset($this->logfile)){fclose($this->logfile);}
            $this->logfile = fopen($dir.'daemon.log', 'a');
            if(!$wasdir){
                //log that new folder wsa created
                $this->logit('SERVICE', 'New Folder Created.('.$dir.')');

                //Remove old current link if exists
                if( file_exists( $this->SERVER_LOG.'/'.$this->SERVER_ID.'/logs/current' ) ){
                    $link_command	=	'unlink '.$this->SERVER_LOG.'/'.$this->SERVER_ID.'/logs/current';
                    exec($link_command, $link_output, $link_result);
                }
                //Link CURRENT folder to most recent folder
                $link_command	=	'ln -s '.$this->SERVER_LOG.'/'.$this->SERVER_ID.'/logs/'. $this->day.'/'.' '.$this->SERVER_LOG.'/'.$this->SERVER_ID.'/logs/current';
                exec($link_command, $link_output, $link_result);
                $this->logit('SERVICE', '+++++++++++++++++++++++++++++++++++++++++++++++++');
                //Log result of the link if directory was created, ie.was not a dir before
                if($link_output){
                    $this->logit('SERVICE', 'Relinked to current directory ('.$dir.')');
                }else{
                    $this->logit('SERVICE', 'FAILED to relink to current directory ('.$dir.')');
                }

                $this->logit('SERVICE', 'Code: '.$this->BUILD.' in '.strtoupper(ENVIRONMENT).' ENV');
                $this->logit('SERVICE', '+++++++++++++++++++++++++++++++++++++++++++++++++');

            }

        }

        // This only runs if its the FIRST TIME not every day
        if($init){
            //Write pid file ONLY RAN ON First loop execution
            $pidfile = fopen($this->SERVER_LOG.'/'.$this->SERVER_ID.'/consumer.pid', 'w');
            fwrite($pidfile, getmypid());
            fclose($pidfile);
        }

    }
    public function logit($who, $string){

        if($this->as_service){
            // Log to file
            fwrite($this->logfile, '['.gmdate('Y-m-d H:i:s').'] '.$who.' >> '.$string."\n");

            // If verbose throw to STDOUT too
            if($this->verbose){
                echo '['.gmdate('H:i:s').'] '.$who.' >> '.$string.chr(10);
            }
        }else{
            // Web browser
            echo $who.' >> '.$string.'<br>';
        }

    }
    public function isJson($string) {
        json_decode(trim($string));
        return (json_last_error() == JSON_ERROR_NONE);
    }
}