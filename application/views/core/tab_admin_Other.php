<div class="theNotePanel">
<small class="dev-only php">tab_admin_Other.php</small>
    <div class="mainbox-top">
        <div class="tab-panel left-tab-panel">
            <div class="panel panel-primary left-LookAndFeel OpportunitySettingsContainer">
                <div class="panel-heading">
                    <h3 class="panel-title "><div class="panel-title-inner"><i class="fa fa-road"></i> Opportunity Pipeline</div></h3><!--<span class="updateStat"><i class="fa fa-refresh"></i></span>-->
                </div>
                <div class="panel-body admin-panelBody">
                    <div class="field-container">
                        <?php
                        $HasPipeline = false;
                        $OpportunityPipeline = $this->config->item('OpportunityPipeline');
                        ?>
                        <div class="OpportunityContent" >
                            <div class="">
                                <div class="form-group ">
                                    <label class="control-label" for="OpportunityContent">
                                        Available Pipeline Description
                                    </label>
                                    <ul id="opportunityTable" >
                                        <?php
                                        if($OpportunityPipeline && trim($OpportunityPipeline) !== ''){
                                            $HasPipeline = true;
                                            $OpportunityPipelineArr = json_decode($OpportunityPipeline, true);
                                            foreach ($OpportunityPipelineArr as $Pipeline){ ?>
                                            <li class="field-item">
                                                <div class="field-item">
                                                    <span class="field-label"><?php echo $Pipeline['PipelineName'] != "" ? $Pipeline['PipelineName']:"Please Enter Code and Label" ?></span>
													<i class="fa fa-trash-o DeleteFieldItem" aria-hidden="true" ></i>
                                                </div>

                                                <form class="FormFieldDetails">
                                                    <ul class="opportunityFieldDetails">
                                                        <li class="field-details">
                                                            <div class="field-container special">
                                                                <div class="field-group">
                                                                    <h3>Two Letter Code</h3>
                                                                    <input  type="text"   name="PipelineCode" maxlength="2"
                                                                            onkeydown="upperCaseF(this);" onkeyup="enableAddStage(this);"
                                                                            class="PipelineCode form-control field-input" value="<?php echo $Pipeline['PipelineCode'] ?>" title=""
                                                                            required="required">
                                                                </div>
                                                                <div class="field-group">
                                                                    <h3>Pipeline Name</h3>
                                                                    <input type="text"  name="PipelineName"
                                                                           onkeyup="enableAddStage(this);"
                                                                           maxlength="20"
                                                                           class="PipelineName form-control field-input" value="<?php echo $Pipeline['PipelineName'] ?>" title=""
                                                                           required="required">
                                                                </div>
                                                            </div><!-- /.field-container -->
                                                            <div class="addstagesContainer">
                                                                <p class="addstagesContainerInfo  <?php echo $HasPipeline===false ? "hideThis":"" ?>">Your Stage Name Prefix and the respective Allowed Next Move, separated by comma.</p>
                                                                <?php
                                                                    foreach ($Pipeline['Stages'] as $Stage){ ?>
                                                                        <div class="addstages field-container">
                                                                            <div class="stagePrefix"><?php echo $Stage['stagePrefix'] ?></div>
                                                                            <div class="stageMoveTo">
                                                                                <input type="text" name="StageCanMove"
                                                                                       class="form-control" value="<?php echo $Stage['stageMoveTo'] ?>"
                                                                                       placeholder="AA02, AA03, AA05"
                                                                                       title="" required="required">
                                                                               <i class="fa fa-trash-o DeleteStageItem" aria-hidden="true"></i>
                                                                            </div>
                                                                        </div>
                                                                   <?php  }
                                                                ?>
                                                            </div>
                                                            <div class="add-field-container">
                                                                <button type="button" class="nb-btn nb-secondary icon-btn addStage" ><i class="fa fa-plus-square-o" aria-hidden="true"></i> Add Stage and Rules</button>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </form>
                                            </li>

                                      <?php     }
                                        }
                                        ?>
                                    </ul>
                                </div>
                                <div class="add-field-container pipeline-save-add">
                                        <button type="button" class="nb-btn nb-secondary icon-btn addPipelines <?php echo $HasPipeline===false ? "hideThis":"" ?>"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Add Pipeline</button>
                                        <button type="button" class="nb-btn nb-primary icon-btn savePipelines  <?php echo $HasPipeline===false ? "hideThis":"" ?>"><i class="fa fa-save"></i>
                                            Save Pipelines
                                        </button>
                                </div>
                                </div>
                            </div>
                        <div class="ContentHeader"></div>
                        <div class="OpportunitySettingsContainerPlaceholder <?php echo $HasPipeline===false ? "":"hideThis" ?>"><i class="glyphicon glyphicon-edit"></i>
                            Click Here To Create Pipeline
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=" tab-panel">
            <div class="panel panel-primary left-LookAndFeel RelationshipSettingsContainer">
                <div class="panel-heading">

                    <h3 class="panel-title "><div class="panel-title-inner"><i class="fa fa-external-link"></i> Connection Relationship Management</div></h3><!--<span class="updateStat"><i class="fa fa-refresh"></i></span>-->

                </div>
                <div class="panel-body admin-panelBody">
                    <div class="field-container">
                        <?php
                        $HasRelationship = false;
                        $ConnectorRelationship = $this->config->item('ConnectorRelationship');
                        ?>
                        <div class="RelationshipContent" >
                            <div class="field-container">
                                <div class="form-group ">
                                    <label class="control-label" for="RelationshipContent">
                                        Relationships
                                    </label>
                                    <ul id="relationshipTable" >
                                        <?php
                                        if($ConnectorRelationship && trim($ConnectorRelationship) !== ''){
                                            $HasRelationship = true;
                                            $ConnectorRelationshipArr = json_decode($ConnectorRelationship, true);
                                            foreach ($ConnectorRelationshipArr as $Relationship){ ?>
                                                <li class="field-item" data-relationid="<?php echo $Relationship['Id']; ?>">
                                                    <div class="field-item">
                                                        <span class="field-label"><?php echo $Relationship['RelationshipName'] != "" ? $Relationship['RelationshipName']:"Please Enter Relationship Name" ?></span>
                                                        <i class="fa fa-trash-o DeleteFieldItem" aria-hidden="true" ></i>
                                                    </div>

                                                    <form class="FormFieldDetails">
                                                        <ul class="relationshipFieldDetails">
                                                            <li class="field-details">

                                                                <div class="detail-container">
                                                                    <h3>Name*</h3>
                                                                    <input type="hidden" name="RelationshipId" class="RelationshipId" value="<?php echo $Relationship['Id']; ?>">
                                                                    <input type="text"  name="RelationshipName"
                                                                           maxlength="20"
                                                                           class="RelationshipName form-control field-input" value="<?php echo $Relationship['RelationshipName'] ?>" title=""
                                                                           required="required">
                                                                </div>
                                                                <div class="detail-container">
                                                                    <h3>Description</h3>
                                                                    <textarea title="Relationship Description" name="RelationshipDescription" class="RelationshipDescription form-control field-input" ><?php echo $Relationship['RelationshipDescription'] ?></textarea>
                                                                </div>
                                                            </li>




                                                        </ul>
                                                    </form>
                                                </li>

                                            <?php     }
                                        }
                                        ?>



                                    </ul>
                                </div>
                                <div class="add-field-container">
                                    <button type="button" class="nb-btn nb-secondary icon-btn addRlationship <?php echo $HasRelationship===false ? "hideThis":"" ?>"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Add Relationship</button>
                                    <button type="button" class="nb-btn nb-primary icon-btn saveRelationships  <?php echo $HasRelationship===false ? "hideThis":"" ?>"><i class="fa fa-save"></i>
                                        Save Relationships
                                    </button>


                                </div>
                            </div>
                        </div>
                        <div class="ContentHeader"></div>

                        <div class="RelationshipSettingsContainerPlaceholder <?php echo $HasRelationship===false ? "":"hideThis" ?>"><i class="glyphicon glyphicon-edit"></i>
                            Click Here To Add Connector Relationship
                        </div>


                    </div>



                </div>
            </div>


        </div>
    </div>
    <div class="">
    </div>
</div>

<li class="HTML-Template relationship field-item">
    <div class="field-item">
        <span class="field-label">Please Enter Relationship Name</span>
        <i class="fa fa-trash-o DeleteFieldItem" aria-hidden="true" ></i>
    </div>

    <form class="FormFieldDetails">
        <ul class="relationshipFieldDetails">
            <li class="field-details">

                <div class="detail-container">
                    <h3>Name*</h3>
                    <input type="hidden" name="RelationshipId" class="RelationshipId" value="">
                    <input type="text"  name="RelationshipName"
                           placeholder="e.g. Owner, Buyer etc.."
                           class="RelationshipName form-control field-input" value="" title=""
                           required="required">
                </div>
                <div class="detail-container">
                    <h3>Description</h3>
                    <textarea title="Relationship Description" name="RelationshipDescription" class="RelationshipDescription form-control field-input" ></textarea>
                </div>
            </li>
        </ul>
    </form>
</li>
<li class="HTML-Template pipeline field-item ">
    <div class="field-item">
        <i class="fa fa-trash-o DeleteFieldItem" aria-hidden="true" ></i>
        <span class="field-label">Please Enter Code and Label</span>

    </div>

    <form class="FormFieldDetails">
        <ul class="opportunityFieldDetails">
            <li class="field-details">
                <div class="field-container">
                    <h3>Two Letter Code</h3>
                    <input  type="text"   name="PipelineCode" maxlength="2"
                            onkeydown="upperCaseF(this);" onkeyup="enableAddStage(this);"
                            class="PipelineCode form-control field-input" value="" title=""
                            required="required">
                </div>
                <div class="field-container">
                    <h3>Pipeline Name</h3>
                    <input type="text"  name="PipelineName"
                           onkeyup="enableAddStage(this);"
                           maxlength="20"
                           class="PipelineName form-control field-input" value="" title=""
                           required="required">
                </div>
                <div class="addstagesContainer">
                    <p class="addstagesContainerInfo hideThis">Your Stage Name Prefix and the respective Allowed Next Move, separated by comma.</p>


                </div>
                <div class="add-field-container">
                    <button type="button" class="nb-btn nb-secondary icon-btn addStage" disabled><i class="fa fa-plus-square" aria-hidden="true"></i> Add Stage and Rules</button>

                </div>

            </li>




        </ul>
    </form>
</li>
<div class="HTML-Template addstages">
    <div class="stagePrefix"></div>
    <div class="stageMoveTo">
        <input type="text" name="StageCanMove"
               class="form-control" value=""
               placeholder="AA02, AA03, AA05"
               title="" required="required"><i class="fa fa-trash-o DeleteStageItem" aria-hidden="true"></i>

    </div>
</div>
<?php
$StageDeleteDialog = "
<div id=\"dialog-confirm\" title=\"Deleting Stage\">
    <p><span class=\"ui-icon ui-icon-alert\"></span>This Stage will be deleted.<br> Are you sure?</p>
</div>
";
$PipelineDeleteDialog = "
<div id=\"dialog-confirm\" title=\"Deleting Pipeline\">
    <p><span class=\"ui-icon ui-icon-alert\"></span>This Pipeline Description will be deleted.<br> Are you sure?</p>
</div>
";
$RelationshipDeleteDialog = "
<div id=\"dialog-confirm\" title=\"Deleting Relationship\">
    <p><span class=\"ui-icon ui-icon-alert\"></span>This Relationship will be deleted.<br> Are you sure?</p>
</div>
";
?>
<script>


    var StageDeleteDialog = <?php echo json_encode($StageDeleteDialog); ?>;
    var PipelineDeleteDialog = <?php echo json_encode($PipelineDeleteDialog); ?>;
    var RelationshipDeleteDialog = <?php echo json_encode($RelationshipDeleteDialog); ?>;
    renderAccordion('opportunity', PipelineDeleteDialog);
    renderAccordion('relationship', RelationshipDeleteDialog);

    $(".OpportunitySettingsContainer")
        ._once('click',".OpportunitySettingsContainerPlaceholder", function(){
            $(this).fadeOut('fast', function(){
                $(".addPipelines").removeClass("hideThis").trigger("click");
                $(".savePipelines").removeClass("hideThis");
            })
        })
        ._once('click', "button.addPipelines", function(){
            console.log('addPipelines');
            var ulParent = $(this).parents("div.OpportunityContent");
            var destination =  ulParent.find("ul#opportunityTable");
            var tempalte = $("li.HTML-Template.pipeline");
            tempalte
                .clone()
                .appendTo(destination)
                .removeClass('HTML-Template')
                .find('div.field-item')
                .trigger('click');

        })
        ._once('click', "button.addStage", function(){

            var liParent = $(this).parents("li.field-details");
            $("p.addstagesContainerInfo",liParent).slideDown();
            var Container = liParent.find("div.addstagesContainer");
            var stagenumer = liParent.find("div.addstagesContainer div.addstages").length || 0;
            stagenumer++;
            stagenumer = stagenumer < 10 ? '0'+stagenumer:stagenumer;
            var pipelinecode = liParent.find("input[name=PipelineCode]").val();
            var tempalte = $("div.HTML-Template.addstages");
            tempalte
                .clone()
                .appendTo(Container)
                .removeClass('HTML-Template')
                .find('div.stagePrefix')
                .html(pipelinecode+stagenumer);

        })._once('click', "i.DeleteStageItem", function(){
        var liParent = $(this).parents("li.field-details");
        var Container = $(this).parents("div.addstagesContainer");
        var addstagesParent = $(this).parents("div.addstages");
        var pipelinecode = liParent.find("input[name=PipelineCode]").val();
        addstagesParent.remove();
        resetCodePrefix(Container,pipelinecode)

        })
        ._once('keyup', "input.PipelineCode", function(){
        var theVal =  $(this).val();
        var liParent = $(this).parents("li.field-details");
        var Container = $("div.addstagesContainer", liParent);
        resetCodePrefix(Container,theVal)

    })
        ._once('keyup', "input.PipelineName", function(){
        var theVal =  $(this).val() || "Please Enter Code and Label";
        var liParent = $(this).parents("li.field-item");
        liParent.find("span.field-label").html(theVal);

    })
        ._once('click', "button.savePipelines", function(){
            var Pipelines = [];
            var toContinue = true;
            $("ul#opportunityTable li.field-item").each(function(){
                var PipelineItem = $(this);
                var PipelineCode = PipelineItem.find("input.PipelineCode").val();
                var PipelineName = PipelineItem.find("input.PipelineName").val();
                if(PipelineCode === '' || PipelineName  === ''){
                    alert("Please make sure all Pipeline Code and Pipeline Name are filled up.");
                    toContinue = false;
                    return false;
                }
                var Stages = [];
                $("div.addstagesContainer div.addstages", PipelineItem).each(function(){
                    var StageItem = $(this);
                    var stagePrefix = StageItem.find("div.stagePrefix").html();
                    var stageMoveTo = StageItem.find("input[name=StageCanMove]").val();
                    Stages.push({stagePrefix:stagePrefix,stageMoveTo:stageMoveTo});
                });
                var ObjectItem = {PipelineCode:PipelineCode,PipelineName:PipelineName,Stages:Stages};
                Pipelines.push(ObjectItem);
            });
            if(toContinue === true) {
                console.log(JSON.stringify(Pipelines));
                console.log("Saving Pipeline");
                savePipelines(JSON.stringify(Pipelines));
            }
        });

    $(".RelationshipSettingsContainer")
        ._once('click',".RelationshipSettingsContainerPlaceholder", function(){
            $(this).fadeOut('fast', function(){
                $(".addRlationship").removeClass("hideThis").trigger("click");
                $(".saveRelationships").removeClass("hideThis");
            })
        })
        ._once('click', "button.addRlationship", function(){
            console.log('addRlationship');
            var ulParent = $(this).parents("div.RelationshipContent");
            var destination =  ulParent.find("ul#relationshipTable");
            var tempalte = $("li.HTML-Template.relationship");
            tempalte
                .clone()
                .appendTo(destination)
                .removeClass('HTML-Template')
                .find('div.field-item')
                .trigger('click');

        })
        ._once('keyup', "input.RelationshipName", function(){
            var theVal =  $(this).val() || "Please Enter Code and Label";
            var liParent = $(this).parents("li.field-item");
            liParent.find("span.field-label").html(theVal);

        })
        ._once('click', "button.saveRelationships", function(){
            var Relationships = [];
            var toContinue = true;
            $("ul#relationshipTable li.field-item").each(function(){
                var RelationshipItem = $(this);
                var RlationshipId = RelationshipItem.find("input.RelationshipId").val();
                var RelationshipName = RelationshipItem.find("input.RelationshipName").val();
                var RelationshipDescription = RelationshipItem.find("textarea.RelationshipDescription").val();
                if($.trim(RelationshipName) === ''){
                    alert("Please make sure all Relationship Name are filled up.");
                    toContinue = false;
                    return false;
                }
                var ObjectItem = {Id:RlationshipId,RelationshipName:RelationshipName,RelationshipDescription:RelationshipDescription};
                Relationships.push(ObjectItem);
            });
            if(toContinue === true) {
                console.log(JSON.stringify(Relationships));
                console.log("Saving Relationships");
                saveRelationships(JSON.stringify(Relationships));
            }
        })
        ._once("change","input.RelationshipName", function(){
            var theField = $(this);
            var Relationships = [];
            $("ul#relationshipTable li.field-item").each(function(){
                var RelationshipItem = $(this);
                var RelationshipName = RelationshipItem.find("input.RelationshipName").val();
                if (RelationshipName === '') return false;
                if($.inArray( RelationshipName.toLowerCase(), Relationships ) === -1){
                    Relationships.push(RelationshipName.toLowerCase());
                }else{
                    alert('Please make the Relationship Name unique.');
                    theField.val('').focus().trigger('keyup');
                }


            });
        });


    $(function() {
        $("ul#opportunityTable li.field-item").first().find("div.field-item").trigger('click');
        $("ul#relationshipTable li.field-item").first().find("div.field-item").trigger('click');
    });
</script>