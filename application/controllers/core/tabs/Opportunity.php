<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 07/03/16
 * Time: 3:00 PM
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Opportunity extends MY_Controller
{
    private $Contact;
    public $ajaxResults = array(
        "status"=> 0, // true or false
        "message"=>"",// any massage
        "data" => array(), // returned data
        "script" => '' // javascript need to execute // returned data
    );
    public function __construct($Prams = []){
        parent::__construct();
        $this->Contact = $Prams;
        //$this->load->database(); this is already loaded in My_Controller
        //$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file')); this is already loaded in My_Controller

    }

    public function index($Data)
    {

        $Params = array();
        $HTML = $this->load->view('core/tab_sbsm', $Params, true);
        return $HTML;
    }
    public function update($data){
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        $values = json_decode($data['values']);
        $session_name = $data['session_name'];
        $user_seession_data = macanta_get_user_seession_data($session_name);
        $session_data = unserialize($user_seession_data->session_data);
        $Id = 0;
        $Fields = [];
        $StageID = '';
        $OppsCustom = [];
        $SetCustomFields = [];
        $opps_custom_fields = infusionsoft_get_custom_fields("%",false, -4, false,false);
        foreach ($opps_custom_fields as $opps_custom_field){
            $Name = $opps_custom_field->Name;
            $OppsCustom[$Name]['value'] = [];
            $FieldName = '';
            $value = '';
            foreach ($values as $key => $Pair){
                if($Pair->name == $Name){
                    $value = $Pair->value;
                    if($opps_custom_field->DataType == 13 || $opps_custom_field->DataType == 14){
                        if($value !== '') $value = infuDateCustom(str_replace(['/','\\'],'-',$value));
                    }
                    $FieldName = '_'.$Pair->name;
                    $OppsCustom[$Name]['value'][] = $value;
                    unset($values[$key]);
                }
            }
            $value = implode(',',$OppsCustom[$Name]['value']);
            if($FieldName) $Fields[] = '"'.$FieldName.'":"'.$value.'"';
        }
        foreach ($values as $Pair){
            if($Pair->name !== 'Id'){
                $value = $Pair->value;
                if($Pair->name === 'NextActionDate' || $Pair->name === 'EstimatedCloseDate'){
                    if($value !== '') $value = infuDate($value);
                }
                if($Pair->name === 'StageID'){
                    $StageID = $value;
                }
                if($Pair->name === 'NextActionNotes'){
                    $value = str_replace('"','',json_encode($value));
                }
                $FieldName = $Pair->name;
                $Fields[] = '"'.$FieldName.'":"'.$value.'"';
            }else{
                $Id =  $Pair->value;
            }
        }
        $StageNames = [];
        $Stages = get_opp_stages();
        foreach ($Stages as $Stage){
            $StageNames[$Stage->Id] = $Stage->StageName;
        }
        $PipelineName = getPipelineName($StageNames[$StageID]);
        $AllowedMove = getAllwedMove($StageNames[$StageID],$Stages);
        $fields = '{'.implode(',',$Fields).'}';
        $action = "update_is";
        $action_details = '{"table":"Lead","id":"'.$Id.'","fields":'.$fields.'}';
        $Results['Local'] =  applyFn('rucksack_request',$action, $action_details );
        //$Results['IS'] =  applyFn('rucksack_request',$action, $action_details, false );
        $this->ajaxResults['data'] = $values;
        $this->ajaxResults['message'] = "Opportunity Saved";
        $this->ajaxResults['action_details'] = $action_details;
        $this->ajaxResults['result'] = $Results;
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['script'] = '';
        $this->ajaxResults['PipelineName'] = $PipelineName;
        infusionsoft_refresh_get_opportunities($data['ContactId'],$session_data);
        $NewOppsValue = get_opportunities_by_id($Id);
        $OppsCustomFields =  infusionsoft_opps_custom_fields();
        $customfields = [];
        foreach ($OppsCustomFields as $GroupName => $OppsCustomFieldObj){
            foreach ($OppsCustomFieldObj as $OppsCustomFieldName =>$OppsCustomFieldDetails){
                $field = '_'.$OppsCustomFieldDetails->Name;
                if(isset($NewOppsValue[0]->$field)){
                    $customfields[$OppsCustomFieldDetails->Name] = $NewOppsValue[0]->$field;
                }
            }
        }
        $data_customfields =  str_replace("=","",base64_encode(json_encode($customfields)));
        $this->ajaxResults['new_data'] = $NewOppsValue;
        $this->ajaxResults['data_customfields'] = $data_customfields;
        $this->ajaxResults['allowed_move'] = $AllowedMove;
        $this->ajaxResults['dataoppid'] = $Id;
        $this->ajaxResults['passed_fields'] = $fields;
        return $this->ajaxResults;
    }

}
