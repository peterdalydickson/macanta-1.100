<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 07/03/16
 * Time: 3:00 PM
 */
// This is not used due to call tab SIMPLIFICATION, this features has been merged in Basic Info controller
if (!defined('BASEPATH')) exit('No direct script access allowed');
use Twilio\Jwt\ClientToken;
class Call extends MY_Controller
{
    private $Contact;
    public $ajaxResults = array(
        "status"=> 0, // true or false
        "message"=>"",// any massage
        "data" => array(), // returned data
        "script" => '' // javascript need to execute // returned data
    );
    public $TwilioToken;
    public function __construct($Prams){
        parent::__construct();
        $this->Contact = $Prams;
        //$this->load->database(); this is already loaded in My_Controller
        //$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file')); this is already loaded in My_Controller
        $Twilio_App_SID  = $this->config->item('Twilio_App_SID');
        $Twilio_Account_SID = $this->config->item('Twilio_Account_SID');
        $Twilio_TOKEN = $this->config->item('Twilio_TOKEN');

        $this->TwilioCapability = new ClientToken($Twilio_Account_SID, $Twilio_TOKEN);
        $this->TwilioCapability->allowClientOutgoing($Twilio_App_SID);
        $this->TwilioToken= $this->TwilioCapability->generateToken(86400);
    }
    public function index($Items)
    {
        //$Items is use as option if we need to restrict part of this method to display
        //We dont Need $ajaxResults, this is not called by ajax

        $Notes = infusionsoft_get_contact_notes_by_contact_id($this->Contact->Id);
        $ContactNotes['PrevNotes'] = $this->parseNote($Notes);
        $Editor = $this->load->view('core/call_index', $ContactNotes, true);
        return $Editor."<script>TwilioToken = '".$this->TwilioToken."'; initiateTwilio();</script>";
    }
    public function twilio($Items)
    {
        //todo: insert here the twillio crendentials from config files
        //$Items is use as option if we need to restrict part of this method to display
        //We dont Need $ajaxResults, this is not called by ajax
        $Contact['Contact'] = $this->Contact;
        $ContactInfo = array();
        $Editor = $this->load->view('core/call_twilio', $Contact, true);
        return $Editor;
    }
    public function getCallRecord($data){
        $RecodingsId = array();
        $CallId = trim($data['CallId']);
        $account_sid = $this->config->item('Twilio_Account_SID');
        $auth_token = $this->config->item('Twilio_TOKEN');
        $client = new Twilio\Rest\Client($account_sid, $auth_token);
        $recordings = $client->recordings->read(
            array(
                "callSid" => $CallId
            )
        );
        foreach($recordings as $recording) {
            $RecodingsId[]  = $recording->sid;
        }
        $URL = "https://api.twilio.com/2010-04-01/Accounts/".$account_sid."/Recordings/".$RecodingsId[0].".mp3";
        $recordings_sid = $RecodingsId[0];
        $Scrpts = "
        TwilioParams['call_id'] = '$CallId';
        TwilioParams['acct_id'] = '$account_sid';
        TwilioParams['rec_id'] = '$recordings_sid';
        ";
        $Scrpts = str_replace("\n","",$Scrpts);
        $this->ajaxResults['data'] = $URL;
        $this->ajaxResults['message'] = 'URL Returned';
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['script'] = $Scrpts;
        return $this->ajaxResults;
    }
    public function addNote($data){
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $UserInfo = json_decode($session_data['Details']);
        $SaveResult = infusionsoft_add_note($data, $UserInfo);
        $tag_data['note_id'] = $SaveResult->message;
        $tag_data['tags'] = $data['tags'];
        $tag_data['conId'] = $data['conId'];
        $this->updateTagNotes($tag_data);
        $Notes = infusionsoft_get_contact_notes_by_contact_id($data['conId']);
        $availableTags = $this->notetags_get();
        $ContactNotes['PrevNotes'] = $this->parseNote($Notes);
        $tabNotes['PrevNotes'] = $this->parseNoteNoteTab($Notes);

        $Editor = $this->load->view('core/call_add_note', $ContactNotes, true);
        $EditorNoteTab = $this->load->view('core/call_add_quick_note', $tabNotes, true);
        $this->ajaxResults['data']['call'] = $Editor;
        $this->ajaxResults['data']['note'] = $EditorNoteTab;
        $this->ajaxResults['message'] = $SaveResult;
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['script'] = 'availableTags='.$availableTags.'; Macanta("tabs/Call addnote");';
        return $this->ajaxResults;
    }
    public function updateNote($data){
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $UserInfo = json_decode($session_data['Details']);
        $UpdateResult = infusionsoft_update_note($data, $UserInfo);
        $this->ajaxResults['data'] = $UpdateResult;
        $this->ajaxResults['message'] = 'Note Updated';
        $this->ajaxResults['status'] = 1;
        return $this->ajaxResults;

    }

    public function updateTagNotes($data){
        $NoteId = $data['note_id'];
        $tagsStr = $data['tags'];
        $contactId = $data['conId'];
        //$SelectedContactResult = storeTaggedSelectedContact($data);
        $DBdata = array(
            'tag_slugs' => $tagsStr
        );
        if(false == $this->DbExists('note_id',$NoteId,'note_tags')){
            $DBdata['note_id'] = $NoteId;
            $DBdata['contact_id'] = $contactId;
            $this->db->insert('note_tags', $DBdata);
        }else{
            $this->db->where('note_id',$NoteId);
            $this->db->update('note_tags',$DBdata);
        }
        $tagsArr = explode(',',$tagsStr);
        $tags_display = '';
        if($tagsStr && $tagsStr !== ''){
            foreach($tagsArr as $tag_item){
                $tags_display.="<span>$tag_item</span>";
            }
        }else{
            $tags_display = '- '.$this->lang->line('text_no_tags').' -';
        }
        $tag_script = '$("tr[data-noteid='.$NoteId.'] .tagDisplay").html("'.$tags_display.'")';
        foreach($tagsArr as $tag){
            $tagSlug = strtolower($tag);
            $tagName = $tag;
            $tagData = $this->DbExists('tag_slug',$tagSlug,'tags');
            if(!$tagData){
                /*Save Unique Tag To DB*/
                $DBdata = array(
                    'tag_slug' => $tagSlug,
                    'tag_name' => $tagName
                );
                $this->db->insert('tags', $DBdata);
            }

        }
        $availableTags = $this->notetags_get();
        //update available tags
        $this->ajaxResults['script'] = 'availableTags='.$availableTags.'; '.$tag_script.';';
        $this->ajaxResults['data'] = $data;
        $this->ajaxResults['message'] = 'Tag Saved, ';
        $this->ajaxResults['status'] = 1;


        return $this->ajaxResults;
    }
    public function parseNote($Notes){
        $TotalNotesArr = array();
        foreach($Notes as $Note){
            $noteId = $Note->Id;

            // Get Tags
            $this->db->where('note_id',$noteId);
            $query = $this->db->get('note_tags');
            $ret = $query->row();
            $tags =  $ret->tag_slugs;
            $tags_displayArr = explode(',',$tags);
            $tags_display = '';
            if($tags && $tags !== ''){
                foreach($tags_displayArr as $tag_item){
                    $tags_display.="<span>$tag_item</span>";
                }
            }else{
                $tags_display = '- '.$this->lang->line('text_no_tags').' -';
            }
            $ObjectType = strtolower($Note->ObjectType);
            $ActionDescription = $Note->ActionDescription; // not used
            $note = html_entity_decode($Note->CreationNotes);
            if(isJson($note)){
                $noteObj = json_decode($note);
                if($noteObj->rec_id != ""){
                    $URL = "https://api.twilio.com/2010-04-01/Accounts/".$noteObj->acct_id."/Recordings/".$noteObj->rec_id.".mp3";
                    $callrecording = '<a class="callrecording" target="_blank" href="'.$URL.'"><i class="glyphicon glyphicon-earphone"></i> '.$this->lang->line('text_call_recording').'</a>';
                }else{
                    $callrecording = $this->lang->line('text_no_call_record');
                }

                $staffArr  = explode(' ',$noteObj->user);
                $CallType = isset($noteObj->note_type) ? $noteObj->note_type:'';
                if ($CallType == "call_notes"){
                    $text['text_edit_note'] = $this->lang->line('text_edit_note');
                    $text['text_save_notes'] = $this->lang->line('text_save_notes');
                    $text['text_cancel'] = $this->lang->line('text_cancel');
                    $TheNote = infusionsoft_parse_call_notes($noteObj->note,$CallType,$text);
                    //$TotalNotesArr[] = "<tr class='NoteItem' data-noteid='".$noteId."' data-notetype='".$CallType."'><td class='noteDate'  data-callid='".$noteObj->call_id."' colspan='2' ><i class='fa fa-tags' aria-hidden='true'></i><input id='tags_".rand()."' type='text' data-noteid='".$noteId."' class='note-tags' value='".$tags."' /></p><span  class='noteDate'>".date('D d M', strtotime($Note->CreationDate->date))." <small>(".ucfirst(str_replace('_',' ',$CallType)).")</small></span><span  class='noteTopic'>".$noteObj->Topic."</span>".$TheNote.'<div class="tagDisplay">'.$tags_display.'</div></td></tr>';
                    $TotalNotesArr[] = "<tr class='NoteItem' data-noteid='".$noteId."' data-notetype='".$CallType."'><td class='noteDate'  data-callid='".$noteObj->call_id."' colspan='2' ><span  class='noteDate'  data-isdate='".$Note->CreationDate->date."'  data-isdatedetailed='".json_encode($Note->CreationDate)."'>".date('D d M h:iA', strtotime($Note->CreationDate->date))."<small> ".$callrecording." &nbsp;&nbsp;</small><small class='staffname'>".$staffArr[0].' '.substr($staffArr[1],0,1).".</small> </span>".$TheNote['rows']."".'<div class="tagDisplay">'.$tags_display.'</div></td></tr>';

                }
            }
        }
        $TotalNotesArr = array_reverse($TotalNotesArr);
        $TotalNotes = implode("\r\n",$TotalNotesArr);
        $PrevNotes = '<table>'.$TotalNotes.'</table>';
        return $PrevNotes;
    }
    public function parseNoteNoteTab($Notes){
        $TotalNotesArr = array();
        foreach($Notes as $Note){
            $noteId = $Note->Id;

            // Get Tags
            $this->db->where('note_id',$noteId);
            $query = $this->db->get('note_tags');
            $ret = $query->row();
            $tags =  $ret->tag_slugs;
            $tags_displayArr = explode(',',$tags);
            $tags_display = '';
            if($tags && $tags !== ''){
                foreach($tags_displayArr as $tag_item){
                    $tags_display.="<span>$tag_item</span>";
                }
            }else{
                $tags_display = '- '.$this->lang->line('text_no_tags').' -';
            }
            $ObjectType = strtolower($Note->ObjectType);
            $ActionDescription = $Note->ActionDescription; // not used
            $note = html_entity_decode($Note->CreationNotes);
            if(isJson($note)){
                $noteObj = json_decode($note);
                if($noteObj->rec_id != ""){
                    $URL = "https://api.twilio.com/2010-04-01/Accounts/".$noteObj->acct_id."/Recordings/".$noteObj->rec_id.".mp3";
                    $callrecording = '<a class="callrecording" target="_blank" href="'.$URL.'"><i class="glyphicon glyphicon-earphone"></i> '.$this->lang->line('text_call_recording').'</a>';
                }else{
                    $callrecording = $this->lang->line('text_no_call_record');
                }
                $staffArr  = explode(' ',$noteObj->user);
                $CallType = isset($noteObj->note_type) ? $noteObj->note_type:'';
                if ($CallType == "call_notes" ||  $CallType == 'quick_notes'){
                    $callrecording = $CallType == 'quick_notes'?  $this->lang->line('text_quick_note'):$callrecording;
                    $text['text_edit_note'] = $this->lang->line('text_edit_note');
                    $text['text_save_notes'] = $this->lang->line('text_save_notes');
                    $text['text_cancel'] = $this->lang->line('text_cancel');
                    $TheNote = infusionsoft_parse_call_notes($noteObj->note,$CallType,$text);
                    $TotalNotesArr[] = "<tr class='NoteItem' data-noteid='".$noteId."'  data-staffid='".$Note->CreatedBy."'  data-timestamp='".strtotime($Note->CreationDate->date)."'   data-tags='".$tags."' data-notetype='".$CallType."'>
                    <td class='noteDate'  data-callid='".$noteObj->call_id."' colspan='2' >
                    <span  class='noteDate'  data-isdate='".$Note->CreationDate->date."'   data-isdatedetailed='".json_encode($Note->CreationDate)."'>>".date('D d M h:iA', strtotime($Note->CreationDate->date))."<small> ".$callrecording." &nbsp;&nbsp;</small><small class='staffname'>".$staffArr[0].' '.substr($staffArr[1],0,1).".</small></span>"
                        .$TheNote['rows'].
                        "<i class='fa fa-tags' aria-hidden='true'></i>"."
                    <input id='tags_".rand()."' type='text' data-noteid='".$noteId."' class='note-tags' value='".$tags."' />".
                        '<div class="tagDisplay">'.$tags_display.'</div></td></tr>';
                }
            }else{
                //$CallType = 'Legacy notes';
                $CallType = 'Infusionsoft '.$this->lang->line('text_'.$ObjectType);
                $rows ='<div class="col-md-12"><span class="noteLabel"><small><span class="show-editor  btn-link"><i class="fa fa-sticky-note" aria-hidden="true"></i> Edit Note</span> <span class="close-editing  btn-link"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save note</span><span class="cancel-editing  btn-link"><i class="fa fa-stop-circle-o" aria-hidden="true"></i> Cancel</span></small></span><div id="noteTextRich'.rand().'" class="Notes noteTextRich" data-tinymce="true">'.$note.'</div></div>';
                $TotalNotesArr[] = "<tr class='NoteItem' data-noteid='".$noteId."'  data-staffid='".$Note->CreatedBy."'  data-timestamp='".strtotime($Note->CreationDate->date)."'   data-tags='".$tags."' data-notetype='".$CallType."'>
                    <td class='noteDate'  data-callid='".$noteObj->call_id."' colspan='2' >
                    <span  class='noteDate'  data-isdate='".$Note->CreationDate->date."'   data-isdatedetailed='".json_encode($Note->CreationDate)."'>>".date('D d M h:iA', strtotime($Note->CreationDate->date))." <small>".$CallType.' - '.$ActionDescription."</small></span>"
                    .$rows.
                    "<i class='fa fa-tags' aria-hidden='true'></i>"."
                    <input id='tags_".rand()."' type='text' data-noteid='".$noteId."' class='note-tags' value='".$tags."' />".
                    '<div class="tagDisplay">'.$tags_display.'</div></td></tr>';


            }
        }
        $TotalNotesArr = array_reverse($TotalNotesArr);
        $TotalNotes = implode("\r\n",$TotalNotesArr);
        $PrevNotes = '<table>'.$TotalNotes.'</table>';
        return $PrevNotes;
    }
    public function notetags_get(){
        $tags = array();
        $tagsStr = array();
        $query = $this->db->get('tags');
        foreach ($query->result() as $row)
        {
            $tagsStr[] = $row->tag_name;
            $tags[] = array(
                'id'=>$row->tag_slug,
                'label'=>$row->tag_name,
                'value'=>$row->tag_name
            );
        }
        //echo  json_encode($tags);
        return json_encode($tagsStr);
    }
    public function DbExists($field,$value,$table)
    {
        $this->db->where($field,$value);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0){

            foreach ($query->result() as $row)
            {
                return $row;
            }
            return false;
        }
        else{

            return false;
        }
    }
}
