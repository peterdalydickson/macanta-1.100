<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 07/03/16
 * Time: 3:00 PM
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once (APPPATH."libraries/Capture_Interactive_Find_v1_00.php");
require_once (APPPATH."libraries/Capture_Interactive_Retrieve_v1_00.php");
require_once (APPPATH."libraries/PhoneNumberValidation_Interactive_Validate_v2_20.php");
class Contact extends MY_Controller
{
    public $content = array();
    public $ajaxResults = array(
        "status"=> 0, // true or false
        "message"=>"",// any massage
        "data" => array(), // returned data
        "script" => '' // javascript need to execute // returned data
    );
    private $MacantaIgniter;
    public function __construct(){
        parent::__construct();
        //$this->load->database(); this is already loaded in My_Controller
        //$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file')); this is already loaded in My_Controller
        //$this->load->library('gravatar'); this is already loaded in My_Controller

    }
    public function index($Data)
    {
        $CurrentDir =  dirname(__FILE__)."/";
        $TimeStarted = time();
        $ToRecord = 'Time Started: '.date('Y-m-d H:i:s',$TimeStarted)."\n";
        $Sections = json_decode($this->config->item('macanta_contact'),true);
        $AllowedSections = $Data['Items'];
        $session_name = $Data['session_name'];
        $user_seession_data = macanta_get_user_seession_data($session_name);
        $session_data = unserialize($user_seession_data->session_data);
        $ContactInfo = infusionsoft_get_contact_by_id($Data['Id'], array('all'));
        $ContactInfo->Gravatar = $this->gravatar->get($ContactInfo->Email);
        $this->MacantaIgniter =& get_instance();
        $SectionContent['Sections'] = $this->_processSubControllers($Sections,$AllowedSections, $ContactInfo, $this->MacantaIgniter,$session_name,$session_data );
        $HTML =  $this->load->view('core/contact_index', $SectionContent, true);
        $this->ajaxResults['data'] = $HTML;
        $this->ajaxResults['message'] = $ContactInfo;
        $this->ajaxResults['status'] = 1;
        $TimeEnded = time();
        $TimeLapse = $TimeEnded-$TimeStarted;
        $ToRecord .= "Contact: ".$TimeLapse."\n";
        //file_put_contents($CurrentDir."_ContactTimeLapse_".$this->config->item('MacantaAppName').".txt",$ToRecord."\n", FILE_APPEND);
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function checkContactDOConnection($Data){
        $ContactId = $Data['ContactId'];
        $result = false;
        if($ContactId != ''){

            //GetContactInfo
            $this->db->where('Id', $ContactId);
            $query = $this->db->get('InfusionsoftContact');
            $row = $query->row();
            $ExtraInfo = "Details: {$row->FirstName} {$row->LastName} | {$row->Email}";

            //Check if has DO connected
            $this->db->like('connected_contact', '"' . $ContactId . '":{','both',false);
            $query = $this->db->get('connected_data');
            $row = $query->row();
            if (isset($row))
            {
                $result = true;
            }
        }
        $this->ajaxResults['data'] = $result;
        $this->ajaxResults['message'] = $ExtraInfo;
        return $this->ajaxResults;
    }
    public function deleteMacantaDataObjectItem($Data){
        $ItemId = $Data['ItemId'];
        $GroupId = $Data['GroupId'];
        $this->db->where('id', $ItemId);
        $this->db->delete('connected_data');
        $this->ajaxResults['message'] = 'ok';

        //Delete from search cache
        $InfusionsoftID = macanta_get_user_seession_data($Data['session_name'], "user_id");
        $connected_searched_cache = manual_cache_loader('connected_searched_cache' . $InfusionsoftID, true);
        if ($connected_searched_cache){
            $UserConnectedInfo = json_decode($connected_searched_cache, true);
            unset($UserConnectedInfo[$GroupId][$ItemId]);
            manual_cache_writer('connected_searched_cache'.$InfusionsoftID, json_encode($UserConnectedInfo),86400, true);
        }
        return $this->ajaxResults;
    }
    public function deleteMacantaContact($Data){
        // remove from search cache
        $ContactId = $Data['ContactId'];
        $InfusionsoftID = macanta_get_user_seession_data($Data['session_name'], "user_id");
        $searched_cache = manual_cache_loader('searched_cache' . $InfusionsoftID, true);
        if($searched_cache){
            $searched_cache = json_decode($searched_cache, true);
            $new_searched_cache = [];
            foreach ($searched_cache as $ContactDetails){
                if($ContactDetails['Id'] != $ContactId) $new_searched_cache[] = $ContactDetails;
            }
            manual_cache_writer('searched_cache'.$InfusionsoftID, json_encode($new_searched_cache),86400,true);
        }

        // remove from CD relationship
        $this->db->like('connected_contact', '"' . $ContactId . '":{','both',false);
        $query = $this->db->get('connected_data');
        if (sizeof($query->result()) > 0) {
            foreach ($query->result() as $row) {
                $connected_contact = json_decode($row->connected_contact, true);
                unset($connected_contact[$ContactId]);
                $DBData = [
                    'connected_contact' => json_encode($connected_contact)
                ];
                $this->db->where('id', $row->id);
                $this->db->update('connected_data', $DBData);
            }
        }

        // remove from contact table
        $this->db->where('Id', $ContactId);
        $this->db->delete('InfusionsoftContact');
        // remove from contact action table
        $this->db->where('ContactId', $ContactId);
        $this->db->delete('InfusionsoftContactAction');

        $this->ajaxResults['message'] = 'ok';
        return $this->ajaxResults;
    }
    public function ContactInvoice($Data){
        $session_name = $Data['session_name'];
        $ContactId = $Data['ContactId'];
        $Invoices = infusionsoft_get_invoice_by_contact_id($ContactId);
        $ContactInfo = infusionsoft_get_contact_by_id($ContactId, array('all'));
        $IS_Currency = infusionsoft_get_settings("Order", "currency");
        $total_due = 0;
        $total_invoice = 0;
        $total_owed = 0;
        if (is_array($Invoices) && sizeof($Invoices > 0)) {
            foreach ($Invoices as $Invoice) {
                $total_invoice += $Invoice->InvoiceTotal;
                if ($Invoice->TotalPaid < $Invoice->InvoiceTotal) {
                    $total_due += $Invoice->TotalDue;
                    $total_owed += ($Invoice->InvoiceTotal - $Invoice->TotalPaid);
                }

            }
        }
        $Params['Invoices'] = $Invoices;
        $Params['Id'] = $ContactId;
        $Params['Currency'] = $IS_Currency->message;
        $Params['OwnerId'] = $ContactInfo->OwnerID;
        $Params['ContactOwner'] = macanta_get_users($ContactInfo->OwnerID);
        $Params['InvoiceTotal'] = $IS_Currency->message . ' ' . number_format($total_invoice, 2);
        $Params['InvoiceOwed'] = $IS_Currency->message . ' ' . number_format($total_owed, 2);
        $Content = $this->load->view('core/contact-invoice', $Params, true);
        $this->ajaxResults['data'] = $Content;
        $this->ajaxResults['script'] = '';
        $this->ajaxResults['status'] = 1;
        return $this->ajaxResults;
    }
    public function FullContact($data){
        $appname = $this->config->item('MacantaAppName');
        $ExcludedApps = ['ryankelley'];
        if(in_array($appname,$ExcludedApps)){
            $this->ajaxResults['data'] = '';
            $this->ajaxResults['script'] = '';
            $this->ajaxResults['status'] = 1;
            return $this->ajaxResults;
        }
        $ContactId = $data['ContactId'];
        $theId = $data['theId'];
        $ContactInfo = infusionsoft_get_contact_by_id($ContactId, array('all'));
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $Params['FullContact'] = macanta_full_contact($ContactId, $ContactInfo->Email, $session_data);
        $FullContact = $this->load->view('core/contact_full_contact', $Params, true);
        $this->ajaxResults['data'] = $FullContact;
        $this->ajaxResults['script'] = '';
        $this->ajaxResults['status'] = 1;
        return $this->ajaxResults;
    }
    public function sendISEmailTemplate($data){
        $conDetails = $data['conDetails'];
        $theId = $data['theId'];
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $subject = '';
        $htmlBody = '';
        $contactList = array();
        $fromAddress = "noreply@".$_SERVER['HTTP_HOST'];
        $toAddress = $conDetails['Email'];
        $bccAddresses = $session_data['Email'];
        $contentType = "HTML";
        $ccAddresses = "";
        $textBody = strip_tags($htmlBody);;
        $result = infusionsoft_send_email($subject,$htmlBody, $contactList,$fromAddress,$toAddress,$bccAddresses,$contentType,$ccAddresses,$textBody, $theId);
        $this->ajaxResults['data'] = $conDetails;
        $this->ajaxResults['message'] = "sendISEmailTemplate";
        $this->ajaxResults['status'] = $result;
        $this->ajaxResults['script'] = "";
        return $this->ajaxResults;
    }
    public function sendISEmail($data){
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $LoggedInUser = json_decode($session_data['Details']);
        $ContactNoteBody = '';

        $conDetails = $data['conDetails'];
        $subject = $data['Subject'];
        $htmlBody = $data['Message'];
        $createNote =  $data['createnote'];
        if(!isset($conDetails['Id']) && isset($conDetails['Email'])){
            $ContactIds = [];
            $Contacts = infusionsoft_get_contact_by_email($conDetails['Email']);
            foreach ($Contacts as $Contact){
                $ContactIds[] = $Contact->Id;
            }
            $contactList = implode(',',$ContactIds);
        }else{
            $contactList = $conDetails['Id'];
        }

        //$fromAddress = "noreply@".$_SERVER['HTTP_HOST'];
        $fromAddress = $LoggedInUser->FirstName." ".$LoggedInUser->LastName." <".$session_data['Email'].">";
        $toAddress = $conDetails['Email'];
        $bccAddresses = $data['BCC'];
        $contentType = "HTML";
        $ccAddresses = "";
        $textBody = strip_tags($htmlBody);
        $Parameters = [$subject,$htmlBody, $contactList,$fromAddress,$toAddress,$bccAddresses,$contentType,$ccAddresses,$textBody];
        $result = infusionsoft_send_email($subject,$htmlBody, $contactList,$fromAddress,$toAddress,$bccAddresses,$contentType,$ccAddresses,$textBody);
        $ContactNoteBody .= "Subject: ".base64_decode($subject)."\n";
        $ContactNoteBody .= "From: ".$LoggedInUser->FirstName." ".$LoggedInUser->LastName." (".$session_data['Email'].")"."\n";
        $ContactNoteBody .= "Message:\n";
        $ContactNoteBody .= base64_decode($textBody)."\n";
        $ContactNoteBody = json_encode($ContactNoteBody);
        $AddToScript = $createNote == 'yes' ? "createQuickNoteAfterFormSubmission($ContactNoteBody,'Email Sent','#email-sent','Email');":"";
        $this->ajaxResults['data'] = $conDetails;
        $this->ajaxResults['message'] = "Subject:".$subject." Message:".$htmlBody;
        $this->ajaxResults['status'] = $result;
        $this->ajaxResults['script'] = $AddToScript;
        return $this->ajaxResults;
    }
    public function AddContact($data){
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $tagsArr = $session_data['UserContactRestrictionTags'];
        $AddedContactURL = $this->config->item('base_url');
        $values = $data['conDetails'];
        $AddedContact = infusionsoft_add_contact($values);
        foreach($values as $index=>$isField){
            if($isField['name'] == 'AddressBillingAdd'){
                //Process here
                if($isField['value'] == 'verified'){
                    $this->addVerifiedAddress($AddedContact->message,"AddressBillingEdit","verified");
                }else{
                    $this->addVerifiedAddress($AddedContact->message,"AddressBillingEdit","unverified");
                }

            }
            if($isField['name'] == 'AddressShippingAdd'){
                //Process here
                if($isField['value'] == 'verified'){
                    $this->addVerifiedAddress($AddedContact->message,"AddressShippingEdit","verified");
                }else{
                    $this->addVerifiedAddress($AddedContact->message,"AddressShippingEdit","unverified");
                }
            }
        }
        //$AddedContactURL .="#contact/".$AddedContact->message;
        $ConId = $AddedContact->message;
        if(sizeof($tagsArr) > 0){
            foreach($tagsArr as $TagId){
                infusionsoft_apply_tag($ConId, $TagId);
            }
        }
        $this->ajaxResults['data'] = $AddedContact;
        $this->ajaxResults['message'] = 'Contact Added/Updated';
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['script'] = 'hashInjectId("'.$AddedContactURL.'","'.$AddedContact->message.'");';
        return $this->ajaxResults;
    }
    public function UpdateContact($data){
        $optin = $data['optin'];
        $userId = $data['conId'];
        $values = $data['conDetails'];
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $Script = '';
        $d_code = $this->config->item('country_code');
        foreach($values as $index=>$isField){
            //$$isField['name'] = $isField['value'];
            if($isField['name'] == 'Company' && empty($isField['value'])){
                unset($values[$index]);
            }
            if($isField['name'] == 'Country'){
                $d_code = getCountryCode($isField['value']);
            }
            if($isField['name'] == 'Phone1'){
                $Phone1 = $isField['value'];
            }
            if($isField['name'] == 'Phone2'){
                $Phone2 = $isField['value'];
            }
            if($isField['name'] == 'AddressBillingEdit'){
                //Process here
                if($isField['value'] == 'verified'){
                    $this->addVerifiedAddress($userId,"AddressBillingEdit","verified");
                }else{
                    $this->addVerifiedAddress($userId,"AddressBillingEdit","unverified");
                }
                unset($values[$index]);

            }
            if($isField['name'] == 'AddressShippingEdit'){
                //Process here
                if($isField['value'] == 'verified'){
                    $this->addVerifiedAddress($userId,"AddressShippingEdit","verified");
                }else{
                    $this->addVerifiedAddress($userId,"AddressShippingEdit","unverified");
                }
                unset($values[$index]);
            }
            if($isField['name'] == 'AddressShippingEditLoqate'){
                unset($values[$index]);
            }
            if($isField['name'] == 'AddressBillingEditLoqate'){
                unset($values[$index]);
            }
            if($isField['name'] == 'SelectedAddressItem'){
                unset($values[$index]);
            }
            if($isField['name'] == 'EmailSignature'){
                $EmailSignature = $isField['value'];
                $this->db->where('user_id',$userId);
                $this->db->where('meta_key','EmailSignature');
                $query = $this->db->get('users_meta');
                $row = $query->row();
                if (isset($row))
                {
                    $this->db->where('user_id',$userId);
                    $this->db->where('meta_key','EmailSignature');
                    $DBdata['meta_value'] = $EmailSignature;
                    $this->db->update('users_meta', $DBdata);
                }else{
                    $DBdata['user_id'] = $userId;
                    $DBdata['meta_key'] = 'EmailSignature';
                    $DBdata['meta_value'] = $EmailSignature;
                    $this->db->insert('users_meta', $DBdata);
                }
                unset($values[$index]);
            }

            $fieldsArr[] = '"'.$isField['name'].'":"'.$isField['value'].'"';
        }
        if(isset($Phone1) && $Phone1 != ''){
            $Phone1Info = SanitizePhone($Phone1);
            $Thecode = $Phone1Info["Code"] != '' ? $Phone1Info["Code"]:$d_code;
            $Phone1Twilio = $Thecode.$Phone1Info["Phone"];
            $Script .= '$("a.Phone1Twilio").attr("data-phonefield","'.$Phone1Twilio.'");';
        }
        if(isset($Phone2) && $Phone2 != ''){
            $Phone2Info = SanitizePhone($Phone2);
            $Thecode = $Phone2Info["Code"] != '' ? $Phone2Info["Code"]:$d_code;
            $Phone2Twilio = $Thecode.$Phone2Info["Phone"];
            $Script .= '$("a.Phone2Twilio").attr("data-phonefield","'.$Phone2Twilio.'");';
        }
        $this->ajaxResults['data'] = $userId;
        $message = infusionsoft_update_contact($values,$userId,$optin,$session_data); // this already updates the infusionsoft
        $this->ajaxResults['message'] = $message;
        $reloadScript = '';
        macanta_logger('RestoreContactId',json_encode($message->message));
        if(isset($message->message['new_id'])){
            $href = "/#contact/".$message->message['new_id'];
            $reloadScript = 'window.location = "'.$href.'"; var theDelay = setTimeout(function () { window.location.reload(true); }, 800);';
        }
        $ContactInfo = infusionsoft_get_contact_by_id($userId, array('all'));
        //Make a javascript variable of each contact info
        $Script .="ContactInfo={};";
        foreach($ContactInfo as $FieldName => $FieldValue){
            $Script .= "ContactInfo['{$FieldName}'] = ".json_encode($FieldValue).";";
        }
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['script'] = $Script."showAddresses(ContactInfo,'AddressBilling');showAddresses(ContactInfo,'AddressShipping');updatePhoneNumbersUI();updateEmailUI();$('.user-info-b li.info-item:visible:odd').addClass('odd-item');".$reloadScript;

        return $this->ajaxResults;
    }
    public function addVerifiedAddress($ContactId,$AddressType,$Value){
        $this->db->where('user_id',$ContactId);
        $this->db->where('meta_key',$AddressType);
        $query = $this->db->get('users_meta');
        $row = $query->row();
        if (isset($row))
        {
            $this->db->where('user_id',$ContactId);
            $this->db->where('meta_key',$AddressType);
            $DBdata['meta_value'] = $Value;
            $this->db->update('users_meta', $DBdata);
        }else{
            $DBdata['user_id'] = $ContactId;
            $DBdata['meta_key'] = $AddressType;
            $DBdata['meta_value'] = $Value;
            $this->db->insert('users_meta', $DBdata);
        }
    }
    public function validateEmailAddress($data){
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        $Email = $data['Email'];
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $EmailInfo = macanta_validate_email($Email,$session_data);
        if($EmailInfo['format_valid'] == true){
            $Format = 'Valid Format';
        }elseif ($EmailInfo['format_valid'] == false){
            $Format = 'Invalid Format';
        }else{
            $Format = $EmailInfo['format_valid'];
        }
        if($EmailInfo['smtp_check'] == true){
            $SMTPCheck = 'Passed';
        }else{
            $SMTPCheck = 'Failed';
        }
        $doYouMean = $EmailInfo['did_you_mean'] != '' ?  '<li><span>Do You Mean:</span> '. $EmailInfo['did_you_mean'] .'</li>':'';
        switch ($EmailInfo['format_valid']){
            case true:
                $html = '<ul class="macanta-notice-ul">
                                       <li><span>Domain:</span> '.$EmailInfo['domain'].'</li>
                                        <li><span>Format:</span> '. $Format .'</li>
                                        '.$doYouMean.'
                                        <li><span>SMTP Check:</span> '. $SMTPCheck .'</li>
                                        <li><span>Score:</span> '.$EmailInfo['score'].'</li>
                                      </ul>';
                break;
            case false:
                $html = '<ul class="macanta-notice-ul">
                                        <li><span>Status:</span> Not A Valid Email Address</li>
                                      </ul>';
                break;
            case 'Unable':
                $html = '<ul class="macanta-notice-ul">
                                       <li><span style="color: #fe7b7f;font-weight: 600;text-shadow: 0px 0px 1px #000;">Verification Disabled</span></li>
                                      </ul>';
                break;
        }
        $this->ajaxResults['data'] = $EmailInfo;
        $this->ajaxResults['html'] = $html;
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['script'] = '';
        return $this->ajaxResults;
    }
    public function validatePhoneNumber($data){
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        $PhoneNumber = $data['PhoneNumber'];
        $ContactId = $data['ContactId'];
        $Country = $data['Country'];
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $Info = macanta_validate_phone_number($ContactId,$PhoneNumber,$Country,$session_data);
        $Stat = $Info['IsValid'];
        switch ($Stat){
            case 'Yes':
                $html = '<ul class="macanta-notice-ul">
                                        <li><span>Status:</span> Valid Phone Number</li>
                                        <li><span>PhoneNumber:</span> '.$Info['PhoneNumber'].'</li>
                                        <li><span>NetworkName:</span> '.$Info['NetworkName'].'</li>
                                        <li><span>NetworkCode:</span> '.$Info['NetworkCode'].'</li>
                                        <li><span>NetworkCountry:</span> '.$Info['NetworkCountry'].'</li>
                                        <li><span>NationalFormat:</span> '.$Info['NationalFormat'].'</li>
                                        <li><span>CountryPrefix:</span> '.$Info['CountryPrefix'].'</li>
                                        <li><span>NumberType:</span> '.$Info['NumberType'].'</li>
                                      </ul>';
                break;
            case 'No':
                $html = '<ul class="macanta-notice-ul">
                                        <li><span>Status:</span> Not A Valid Phone Number</li>
                                      </ul>';
                break;
            case 'Unable':
                $html = '<ul class="macanta-notice-ul">
                                        <li><span style="color: #fe7b7f;font-weight: 600;text-shadow: 0px 0px 1px #000;">Verification Disabled</span></li>
                                      </ul>';
                break;
        }
        $this->ajaxResults['data'] = $Info;
        $this->ajaxResults['html'] = $html;
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['script'] = '';
        return $this->ajaxResults;
    }
    public function validateAddress($data){
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        $Address = $data['Address'];
        //macanta_get_state_format( $input, $format = 'abbr' )
        $Country = $data['Country'];
        $Container = $data['Container'];
        if(is_array($Address)){
            $Text = [];
            unset($Address['Country2']);
            unset($Address['Country']);
            foreach ($Address as $FieldName=>$FieldValue){
                if($FieldName == 'PostalCode'){
                    if($Address['StreetAddress1'] != "" || $Address['StreetAddress2'] != "") continue;
                }
                if($FieldName == 'PostalCode2'){
                    if($Address['Address2Street1'] != "" || $Address['Address2Street2'] != "") continue;
                }
                if($FieldName == 'State' || $FieldName == 'State2'){
                    $FieldValue = macanta_get_state_format( $FieldValue, 'abbr' );
                }
                $Text[] = $FieldValue;
            }
            $Text = implode(' ', $Text);
        }else{
            $Text = $Address;
        }


        //Get All FirstBachId
        $Addresses = macanta_get_valid_addresses($Text,$Container,$Country);
        $this->ajaxResults['data'] = $Addresses;
        $this->ajaxResults['size'] = sizeof($Addresses);
        $this->ajaxResults['message'] = '';
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['script'] = '';
        return $this->ajaxResults;
    }
    public function retrieveFullAddress($data){
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $SearchId = $data['SearchId'];
        //Get All FirstBachId
        $Address =  macanta_get_valid_address($SearchId);
        macanta_record_api_call("address_retrieve",$SearchId,$session_data);
        $this->ajaxResults['data'] = $Address;
        $this->ajaxResults['message'] = '';
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['script'] = '';
        return $this->ajaxResults;
    }
    public function getEmailHistoryItem($data){
        $EmailHistoryItem = infusionsoft_get_email_item($data['Id']);
        $this->ajaxResults['data'] = $EmailHistoryItem;
        $this->ajaxResults['message'] = 'Ok';
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['script'] = '';
        return $this->ajaxResults;
    }
    public function _processSubControllers($Sections,$AllowedSections, $ContactInfo=array(), $MacantaIgniter,$session_name,$session_data ){
        $SectionContent = array();
        foreach ($Sections as $Section => $Params){
            if(!in_array('all', $AllowedSections)){
                if(!in_array($Section, $AllowedSections)) continue;
            }
            $SectionContent[$Section]['class'] = $Params['class'];
            foreach($Params['controllers'] as $Controller => $Methods ){
                $loadedController = _loadMacantaController($MacantaIgniter, $Controller, $ContactInfo, $session_data);
                if(isset($this->$loadedController)){
                    foreach($Methods as $Method => $Items){
                        if(method_exists($this->$loadedController, $Method)){
                            $SectionContent[$Section]['content'][$Method] = $this->$loadedController->$Method($Items,$session_name,$session_data);
                        }else{
                            $SectionContent[$Section]['content'][$Method] = 'No "'.$Method .'" Method Existing in "'. $loadedController . '" controller locating in('.$Controller.')';
                        }
                    }
                }else{
                    $SectionContent[$Section]['content'][] = $loadedController;
                }

            }
        }
        return $SectionContent;
    }

}