<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 07/03/16
 * Time: 3:00 PM
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Note extends MY_Controller
{
    public $ajaxResults = array(
        "status" => 0, // true or false
        "message" => "",// any massage
        "data" => array(), // returned data
        "script" => '' // javascript need to execute // returned data
    );
    private $Contact;

    public function __construct($Prams = [])
    {
        parent::__construct();
        $this->Contact = $Prams;
        //$this->load->database(); this is already loaded in My_Controller
        //$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file')); this is already loaded in My_Controller
    }

    public function index($Items, $session_name)
    {
        $WriteLog = $this->config->item('MacantaAppName') == 'no' ? true:false;
        $CurrentDir = dirname(__FILE__) . "/";
        header('Content-Type: text/html; charset=UTF-8');

        if($this->config->item('MacantaAppName') == 'tt381'){
            /*ini_set('display_errors', 1);
            error_reporting(E_ALL);*/
        }
        $start = time();
        $user_seession_data = macanta_get_user_seession_data($session_name);
        $session_data = unserialize($user_seession_data->session_data);
        $Duration = time()-$start;
        $ToRecordContactNotes = "GetUserSession: " . $Duration . "\n----\n";
        if($WriteLog)
            file_put_contents($CurrentDir . "_NoteTimeLapse_".$this->config->item('MacantaAppName').".txt", $ToRecordContactNotes , FILE_APPEND);


        //$Notes['Notes'] = infusionsoft_get_contact_notes_by_contact_id($this->Contact->Id);
        $start = time();
        $Notes = infusionsoft_get_contact_notes_by_contact_id($this->Contact->Id);
        $Duration = time()-$start;
        $ToRecordContactNotes = "GetNotes: " . $Duration . "\n----\n";
        if($WriteLog)
            file_put_contents($CurrentDir . "_NoteTimeLapse_".$this->config->item('MacantaAppName').".txt", $ToRecordContactNotes , FILE_APPEND);


        $start = time();
        $ContactNotes['StaffMembers'] = macanta_get_users();
        $Duration = time()-$start;
        $ToRecordContactNotes = "GetStaffMembers: " . $Duration . "\n----\n";
        if($WriteLog)
            file_put_contents($CurrentDir . "_NoteTimeLapse_".$this->config->item('MacantaAppName').".txt", $ToRecordContactNotes , FILE_APPEND);

        //file_put_contents(dirname(__FILE__)."/".$this->config->item('MacantaAppName')."-notes.txt",json_encode($Notes));
        $start = time();
        $ContactNotes['PrevNotes'] = $this->parseNote($Notes,$session_data);
        $Duration = time()-$start;
        $ToRecordContactNotes = "ParseNote: " . $Duration . "\n----\n";
        if($WriteLog)
            file_put_contents($CurrentDir . "_NoteTimeLapse_".$this->config->item('MacantaAppName').".txt", $ToRecordContactNotes , FILE_APPEND);

        $ContactNotes['session_data'] = $session_data;
        //echo $ContactNotes['PrevNotes'];
        $HTML = $this->load->view('core/tab_notes', $ContactNotes, true);
        return $HTML;
    }
    public function loadNote($Data){
        $WriteLog = $this->config->item('MacantaAppName') == 'no' ? true:false;
        $CurrentDir = dirname(__FILE__) . "/";
        header('Content-Type: text/html; charset=UTF-8');
        $session_name = $Data['session_name'];
        $start = time();
        $user_seession_data = macanta_get_user_seession_data($session_name);
        $session_data = unserialize($user_seession_data->session_data);
        $ContactNotes['session_data'] = $session_data;
        $Duration = time()-$start;
        $ToRecordContactNotes = "GetUserSession: " . $Duration . "\n----\n";
        if($WriteLog)
            file_put_contents($CurrentDir . "_NoteTimeLapse_".$this->config->item('MacantaAppName').".txt", $ToRecordContactNotes , FILE_APPEND);


        //$Notes['Notes'] = infusionsoft_get_contact_notes_by_contact_id($this->Contact->Id);
        $start = time();
        $Notes = infusionsoft_get_contact_notes_by_contact_id($Data['ContactId']);
        $Duration = time()-$start;
        $ToRecordContactNotes = "GetNotes: " . $Duration . "\n----\n";
        if($WriteLog)
            file_put_contents($CurrentDir . "_NoteTimeLapse_".$this->config->item('MacantaAppName').".txt", $ToRecordContactNotes , FILE_APPEND);

        $start = time();
        $ContactNotes['PrevNotes'] = $this->parseNote($Notes,$session_data);
        $Duration = time()-$start;
        $ToRecordContactNotes = "ParseNote: " . $Duration . "\n----\n";
        if($WriteLog)
            file_put_contents($CurrentDir . "_NoteTimeLapse_".$this->config->item('MacantaAppName').".txt", $ToRecordContactNotes , FILE_APPEND);

        $availableTags = $this->notetags_get();
        //echo $ContactNotes['PrevNotes'];
        $this->ajaxResults['data'] = $this->load->view('core/tab_notes_c', $ContactNotes, true);
        $this->ajaxResults['message'] = "Contacts Notes";
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['script'] = '$(\'tr.NoteItem\')
        ._once(\'click\', \'i.fa-tags\', function () {
            var tagContainer = $(this).closest(\'td.noteDate\').find(\'.tagsinput\');
            var existingtagContainer = $(this).closest(\'td.noteDate\').find(\'.tagDisplay\');
            if ($(this).attr(\'style\')) {
                $(this).removeAttr(\'style\');
                tagContainer.slideUp("fast");
                existingtagContainer.slideDown();
            } else {
                $(this).css(\'display\', "block");
                tagContainer.hide().slideDown().find(\'input\').focus();
                existingtagContainer.slideUp("fast");
            }


        });
        tagIt();autoCompleteIt();
        noteCollapsible();'.'availableTags=' . $availableTags . '; Macanta("tab/Note addNote");toggleThisTaskInside();';
        return $this->ajaxResults;
    }
    public function loadNoteBtn($session_name){
        header('Content-Type: text/html; charset=UTF-8');
        $user_seession_data = macanta_get_user_seession_data($session_name);
        $session_data = unserialize($user_seession_data->session_data);
        $ContactNotes['session_data'] = $session_data;
        $ContactNotes['StaffMembers'] = macanta_get_users();
        $HTML = $this->load->view('core/tab_notes_b', $ContactNotes, true);
        return $HTML;
    }
    public function getContactsNotes($data){

        $this->ajaxResults['data'] =infusionsoft_get_contact_notes_by_multiple_contact_id($data['ContactIds']);
        $this->ajaxResults['message'] = "Contacts Notes";
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['script'] = '';
        $this->ajaxResults['json'] = '_';
        if ($this->config->item('NoteCustomFields')) {
            $NoteCustomFields = json_decode($this->config->item('NoteCustomFields'), true);
            $CF_RecordingURL = '_' . $NoteCustomFields['Call Recording URL'];
            $CF_MacantaNotes = '_' . $NoteCustomFields['JSON'];
            $this->ajaxResults['jsonNote'] = $CF_MacantaNotes;
        }

        return $this->ajaxResults;
    }
    public function RemoveBS($Str) {
        $StrArr = str_split($Str); $NewStr = '';
        foreach ($StrArr as $Char) {
            $CharNo = ord($Char);
            if ($CharNo == 163) { $NewStr .= $Char; continue; } // keep £
            if ($CharNo > 31 && $CharNo < 127) {
                $NewStr .= $Char;
            }
        }
        return $NewStr;
    }
    public function parseNote($Notes,$session_data)
    {
        $TotalNotesArr = array();
        $NoteCustomFields = json_decode($this->config->item('NoteCustomFields'), true);
        $CF_RecordingURL = '_' . $NoteCustomFields['Call Recording URL'];
        $CF_MacantaNotes = '_' . $NoteCustomFields['JSON'];
        $text['text_edit_note'] = $this->lang->line('text_edit_note');
        $text['text_save_notes'] = $this->lang->line('text_save_notes');
        $text['text_cancel'] = $this->lang->line('text_cancel');
        $Types = infusionsoft_get_settings('ContactAction', 'optionstype');
        $Types = explode(',', $Types->message);


        $NoteEditingPermissions = $this->config->item('note_editing_permission');
        $NoteEditingPermissionsArr = $NoteEditingPermissions ? explode(',',$NoteEditingPermissions):[];
        if(isset($session_data['Groups']) && trim($session_data['Groups']) != ''){
            $UserGroupsArr = explode(',',$session_data['Groups']);

        }else{
            $UserGroupsArr = [];
        }
        $HideEditNote = "hideThis";
        if(sizeof($NoteEditingPermissionsArr) > 0){
            foreach ($NoteEditingPermissionsArr as $Tag){
                if(in_array($Tag,$UserGroupsArr)){
                    $HideEditNote = "";
                    break;
                }
            }
        }else{
            $HideEditNote = "";
        }

        $IS_Users = macanta_get_users();
        foreach ($Notes as $Note) {
            $TypesOption = '';
            $NativeNotice = '';
            if($Note->Origin == 'Native'){
                $NativeNotice = '(Macanta Native Note)';
            }
            foreach ($Types as $Type) {
                $TypeSelected = isset($Note->ActionType) ?  $Note->ActionType == $Type ? 'selected' : '':'';
                $TypesOption .= "<option value='$Type' $TypeSelected>$Type</option>";
            }
            $noteId = $Note->IdLocal;
            //if($Note->Id == 2747) continue; //swampschool error debugging
            $UserInfo = json_decode($session_data['Details']);
            if(!empty($Note->CreationDate)){
                //$Note->CreationDate->date = date('Y-m-d H:i:s', strtotime($Note->CreationDate->date));
                //$Note->CreationDate->date = date('Y-m-d H:i:s', strtotime($Note->CreationDate->date) + 18000);
                $Note->CreationDate->date = _date('Y-m-d H:i:s', strtotime($Note->CreationDate->date), $session_data['TimeZone']);
            }else{
                $Note->CreationDate = new stdClass();
                $Note->CreationDate->date = date('Y-m-d H:i:s');
            }
            if(isset($Note->ActionDate->date)){
                //$Note->ActionDate->date = date('Y-m-d', strtotime($Note->ActionDate->date));
                //$Note->ActionDate->date = date('Y-m-d', strtotime($Note->ActionDate->date) + 18000);
                $Note->ActionDate->date = _date('Y-m-d', strtotime($Note->ActionDate->date), $session_data['TimeZone']);
            }else{
                $Note->ActionDate = new stdClass();
                $Note->ActionDate->date = '';
            }
            // tobe continued, theres an error in updateTagNotes with cache (storeTaggedSelectedContact($data))
            /*preg_match_all("/(#\w+)/u", $Note->CreationNotes, $matches);
            if ($matches) {
                $hashtagsArray = array_count_values($matches[0]);
                $hashtags = array_keys($hashtagsArray);
            }
            $ExistingTag = $this->getNoteTag($noteId);
            $ExistingTagArr = $ExistingTag == '' ? [] : explode(',', $ExistingTag);
            $ExistingTagArr = array_merge($ExistingTagArr,$hashtags);
            $ExistingTagStr = implode(',', $ExistingTagArr);
            $data['note_id'] = $noteId;
            $data['tags'] = $ExistingTagStr;
            $data['conId'] = $UserInfo->Id;
            $this->updateTagNotes($data);
			*/
            // Get Tags
            $this->db->where('note_id', $noteId);
            $query = $this->db->get('note_tags');
            $LastQuery = $this->db->last_query();
            $ret = $query->row();
            $tags = isset($ret) ? $ret->tag_slugs:'';
            $tags_displayArr = explode(',', $tags);
            $tags_display = '';
            if ($tags && $tags !== '') {
                foreach ($tags_displayArr as $tag_item) {
                    $tags_display .= "<span>$tag_item</span>";
                }
            } else {
                $tags_display = '- ' . $this->lang->line('text_no_tags') . ' -';
            }
            $ObjectType = strtolower($Note->ObjectType);
            $Note->ActionDescription = mb_convert_encoding($Note->ActionDescription, "HTML-ENTITIES", "UTF-8");
            //$Note->ActionDescription = trim(html_entity_decode($Note->ActionDescription));
            $ActionDescription = $Note->ActionDescription; // not used
            $MacantaNotes = empty($Note->$CF_MacantaNotes) ? false : json_encode($Note->$CF_MacantaNotes);
            /*if (isset($Note->$CF_MacantaNotes) && trim($Note->$CF_MacantaNotes) != '') {
                $MacantaNotes = trim(html_entity_decode($Note->$CF_MacantaNotes));
            }*/
            $RecordingURL = '';
            if (isset($Note->$CF_RecordingURL) && trim($Note->$CF_RecordingURL) != '') {
                $RecordingURL = trim($Note->$CF_RecordingURL);
            }
            $note = mb_convert_encoding($Note->CreationNotes, "HTML-ENTITIES", "UTF-8");
            //$note = trim(html_entity_decode($note));
            // Check if Old Data Format
            $UpdateNotes = true;
            if (strpos($note, "== macanta data: do not edit below this line ==") !== false) {
                $UpdateNotes = false;
                if ($this->config->item('NoteCustomFields')) $UpdateNotes = true;
                $noteArr = explode("== macanta data: do not edit below this line ==", trim($note));
                $note = trim(str_replace("\n",'',$noteArr[1]));
            }

            if (isJson($note) && $note != '') {
                $noteObj = json_decode($note);
                if (isset($noteObj->note)) {
                    //This is 100% Macanta note in CreationNotes field, so lets put it in Macanta Custom field
                    $CallType = isset($noteObj->note_type) ? $noteObj->note_type : '';
                    $MacantaNotes = $note;
                    $theVal = is_base64_encoded($noteObj->note->Notes) ? base64_decode($noteObj->note->Notes) : $noteObj->note->Notes;
                    $theVal = mb_convert_encoding($theVal, "HTML-ENTITIES", "UTF-8");
                    $PlainNotes = strip_tags($theVal);
                    $theVal = str_replace("\n", "<br>", $theVal);
                    $Identity = $CallType == 'call_notes' ? '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>' : '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>';
                    $rows = '
                            <article class=" noteCollapsible">
                            <small class="dev-only php">Code moved to Note.php - Line 325</small>
                            
                            <div id="noteTitle">' . $Note->ActionDescription . '</div>
                            <div class="">
                            <div class="NoteTitleEditorContainer">
                                <h3 class="NoteTitleEditorLabel">Title:</h3>
                                <input name="NoteTitleEditor"  class="form-control NoteTitleEditor" value="' . $Note->ActionDescription . '">
                            </div>
                            <div class=" NoteTypeEditorContainer">
                                <h3>Type:</h3>
                                <select name="NoteTypeEditor" id="NoteTypeEditor" class="form-control  NoteTypeEditor">
                                <option value="">Please select an action type</option>
                                ' . $TypesOption . '
                                </select>
                            </div>
                            </div>
                            
                            <div class=" NoteDescriptionEditorContainer">
                            <h3 class="NoteDescriptionEditorLabel">Description:</h3>
                            <div id="noteTextRich' . rand() . '" class=" noteTextRich" data-tinymce="true">' . $theVal . '</div>
                            </div>
                            </article>';
                    $TheNote['rows'] = $rows;
                    if ($noteObj->rec_id != "") {
                        $PlainNotes = $PlainNotes == '' ? 'Outbound Call' : $PlainNotes;
                        $URL = "https://api.twilio.com/2010-04-01/Accounts/" . $noteObj->acct_id . "/Recordings/" . $noteObj->rec_id . ".mp3";
                        $RecordingURL = $URL;
                        $callrecording = '<a class="callrecording" target="_blank" href="' . $RecordingURL . '"><i class="fa fa-phone"></i> ' . $this->lang->line('text_call_recording') . '</a>';
                    } else {
                        $callrecording = $this->lang->line('text_no_call_record');
                    }
                    $staffArr = explode(' ', $noteObj->user);

                    if ($CallType == "call_notes" || $CallType == 'quick_notes') {
                        $callrecording = $CallType == 'quick_notes' ? $this->lang->line('text_quick_note') : $callrecording;

                        $TotalNotesArr[] = "<tr class='NoteItem' data-noteid='" . $noteId . "'  data-staffid='" . $Note->CreatedBy . "'  data-timestamp='" . strtotime($Note->CreationDate->date) . "'   data-tags='" . $tags . "' data-notetype='" . $CallType . "'>
                    <td class='noteDate' data-tags='".$tags."'  data-callid='" . $noteObj->call_id . "' colspan='2' >
                    <div class='note-header'>
	                    <span  class='noteDate'  data-isdate='" . $Note->CreationDate->date . "'   data-isdatedetailed='" . json_encode($Note->CreationDate) . "'>"
		                    ."<span class='cal-date'>" . date('D d M Y h:iA', strtotime($Note->CreationDate->date)) . "</span>"
		                    ."<small class='note-action'> " . $callrecording . " </small>"
		                    ."<small class='staffname'>" . $staffArr[0] . ' ' . substr($staffArr[1], 0, 1) . "</small>"
	                    ."</span><!-- /.noteDate -->"
                    ."</div><!-- /.note-header -->"
                            . $TheNote['rows'] .
                            "<div class='note-footer'><div class='flex-row'>" . "
                    <input id='tags_" . rand() . "' type='text' data-noteid='" . $noteId . "' class='note-tags' value='" . $tags . "' />" .
                            '<div class="tagDisplay">' . $tags_display . '</div><i class="fa fa-tags" aria-hidden="true"></i></div></div></td></tr>';
                    }

                    $data = array(
                        'noteid' => $noteId,
                        'NoteTitle' => $ActionDescription,
                        'notesType' => $CallType,
                        'notes' => $PlainNotes,
                        'macantaNotes' => $MacantaNotes,
                        'callRecordingUrl' => $RecordingURL,
                    );
                    // Update Note to the new Format
                    if ($UpdateNotes == true) infusionsoft_update_note($data, $UserInfo);
                }
                else {

                }


            } else {
                if ($Note->ObjectType != 'Task' && $MacantaNotes ) {
                    //$MacantaNotes = trim(html_entity_decode($MacantaNotes));
                    $noteObj = json_decode($MacantaNotes);
                    $CallType = isset($noteObj->note_type) ? $noteObj->note_type : '';
                    $theVal = is_base64_encoded($noteObj->note->Notes) ? base64_decode(trim($noteObj->note->Notes)) : $noteObj->note->Notes;
                    $theVal = str_replace("\n", "<br>", trim($theVal));
                    $theVal = mb_convert_encoding($theVal, "HTML-ENTITIES", "UTF-8");
                    $Identity = $CallType == 'call_notes' ? '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>' : '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>';
                    $rows = '
                            <article class="  noteCollapsible">
                            <small class="dev-only php">Code moved to Note.php - Line 325</small>
                            
                            <div id="noteTitle">' . $Note->ActionDescription . '</div>
                            <div class="">
                            <div class="NoteTitleEditorContainer">
                                <h3 class="label-sm NoteTitleEditorLabel">Title:</h3>
                                <input name="NoteTitleEditor"   class="form-control NoteTitleEditor" value="' . $Note->ActionDescription . '">
                            </div>
                            <div class=" NoteTypeEditorContainer">
                                <h3 class="label-sm">Type:</h3>
                                <select name="NoteTypeEditor" id="NoteTypeEditor" class="form-control  NoteTypeEditor">
                                <option value="">Please select an action type</option>
                                ' . $TypesOption . '
                                </select>
                            </div>
                            </div>
                            
                    
                            <div class=" NoteDescriptionEditorContainer">
                            <h3 class="label-sm NoteDescriptionEditorLabel">Description:</h3>
                            <div id="noteTextRich' . rand() . '" class=" noteTextRich" data-tinymce="true">' . $theVal . '</div>
                            </div>
                            </article>';
                    $TheNote['rows'] = $rows;
                    //if ($noteObj->rec_id != "") {
                    if ($RecordingURL != "") {
                        //$URL = "https://api.twilio.com/2010-04-01/Accounts/" . $noteObj->acct_id . "/Recordings/" . $noteObj->rec_id . ".mp3";
                        // use this because when note is updated rec_id is omitted and no Recoding url will be seen
                        $URL = $RecordingURL;
                        $callrecording = '<a class="callrecording" target="_blank" href="' . $URL . '"><i class="fa fa-phone"></i> ' . $this->lang->line('text_call_recording') . '</a>';
                    } else {
                        $callrecording = $this->lang->line('text_no_call_record');
                    }
                    $staffArr = explode(' ', $noteObj->user);

                    if ($CallType == "call_notes" || $CallType == 'quick_notes') {
                        $callrecording = $CallType == 'quick_notes' ? $this->lang->line('text_quick_note') : $callrecording;

                        $TotalNotesArr[] = "<tr class='NoteItem' data-noteid='" . $noteId . "'  data-staffid='" . $Note->CreatedBy . "'  data-timestamp='" . strtotime($Note->CreationDate->date) . "'   data-tags='" . $tags . "' data-notetype='" . $CallType . "'>
                    <!--" . $LastQuery . "-->
                    <td class='noteDate'  data-tagsb='' data-callid='" . $noteObj->call_id . "' colspan='2' >
                    <div class='note-header'>
	                    <span  class='noteDate'  data-isdate='" . $Note->CreationDate->date . "'   data-isdatedetailed='" . json_encode($Note->CreationDate) . "'>"
	                    ."<span class='cal-date'>" . date('D d M Y h:iA', strtotime($Note->CreationDate->date)) . "</span>"
	                    ."<small class='note-action'>" . $callrecording . " </small>"
	                    ."<small class='staffname'>" . $staffArr[0] . ' ' . substr($staffArr[1], 0, 1) . ".</small>"
	                    ."</span><!-- /.noteDate -->"
		                    
	                    ."<span class='noteLabel'>
		                    <small>
	                            <span class='show-editor ".$HideEditNote." nb-btn icon-btn txt-btn'>" . $Identity . ' ' . $text['text_edit_note'] . "</span>
	                            <span class='close-editing  nb-btn icon-btn txt-btn'>
		                            <i class='fa fa-floppy-o' aria-hidden='true'></i> "
		                            . $text['text_save_notes']
	                            ."</span>"
	                            ."<span class='cancel-editing  nb-btn icon-btn txt-btn'>
		                            <i class='fa fa-stop-circle-o' aria-hidden='true'></i> "
		                            . $text['text_cancel']
	                            ."</span>
                            </small>
                        </span>
                    </div><!-- /.note-header -->"
		                    . $TheNote['rows']
		                    ."<div class='note-footer'>"
			                    ."<div class='flex-row'>"
                                    ."<input id='tags_" . rand() . "' type='text' data-noteid='" . $noteId . "' class='note-tags' value='" . $tags . "' />"
				                    .'<div class="tagDisplay">' . $tags_display . '</div>'
				                    ."<i class='fa fa-tags' aria-hidden='true'></i>"
			                    ."</div>"
                            ."</div><!-- /.note-footer -->"
                        .'</td></tr>';
                    }


                }
                elseif ($this->config->item('TurboDial_CallRecordingURL_Field')) {
                    $CustomField = $this->config->item('TurboDial_CallRecordingURL_Field');
                    if (!isset($Note->$CustomField)) continue;
                    $tags_display = '';
                    $tags = $this->getNoteTag($noteId);
                    $ExistingTagArr = $tags == '' ? [] : explode(',', $tags);
                    if (!in_array('#inbound_call', $ExistingTagArr)) {
                        $ExistingTagArr[] = '#inbound_call';
                        $tags = implode(',', $ExistingTagArr);
                    }
                    foreach ($ExistingTagArr as $tag_item) {
                        $tags_display .= "<span>$tag_item</span>";
                    }


                    $CallType = 'call_notes';

                    $theVal = $Note->CreationNotes;
                    $theVal = str_replace("\n", "<br>", $theVal);
                    $Identity = '<i class="fa fa-volume-control-phone" aria-hidden="true"></i>';
                    $rows = '
                            <article class="  noteCollapsible">
                            <small class="dev-only php">Code moved to Note.php - Line 325</small>
                            <div id="noteTitle">' . $Note->ActionDescription . '</div>
                            <div class="">
                            <div class="NoteTitleEditorContainer">
                                <h3 class="NoteTitleEditorLabel">Title:</h3>
                                <input name="NoteTitleEditor"   class="form-control NoteTitleEditor" value="' . $Note->ActionDescription . '">
                            </div>
                            <div class=" NoteTypeEditorContainer">
                                <h3>Type:</h3>
                                <select name="NoteTypeEditor" id="NoteTypeEditor" class="form-control  NoteTypeEditor">
                                <option value="">Please select an action type</option>
                                ' . $TypesOption . '
                                </select>
                            </div>
                            </div>
                            
                            <div class=" NoteDescriptionEditorContainer">
                            <h3 class="NoteDescriptionEditorLabel">Description:</h3>
                            <div id="noteTextRich' . rand() . '" class=" noteTextRich" data-tinymce="true">' . $theVal . '</div>
                            </div>
                            </article>';
                    $TheNote['rows'] = $rows;
                    $URL = $Note->$CustomField;
                    //$TurboDial = file_get_contents($URL);
                    $xmlDoc = new DOMDocument();

                    $html = file_get_contents($URL);

                    @$xmlDoc->loadHTML($html);
                    $searchNodes = $xmlDoc->getElementsByTagName("source");
                    foreach ($searchNodes as $searchNode) {
                        $CallURLID = str_replace(".wav", ".mp3", $searchNode->getAttribute('src'));

                    }
                    $callrecording = '<a class="callrecording" target="_blank" href="' . $CallURLID . '" ><i class="fa fa-phone"></i> ' . $this->lang->line('text_call_recording') . '</a>';
                    $staffArr = "Created By Turbo Dial";

                    $TotalNotesArr[] = "<tr class='NoteItem' data-noteid='" . $noteId . "'  data-staffid='" . $Note->CreatedBy . "'  data-timestamp='" . strtotime($Note->CreationDate->date) . "'   data-tags='" . $tags . "' data-notetype='" . $CallType . "'>
                    <td class='noteDate'  data-tags='".$tags."' colspan='2' >
	                    <span  class='noteDate'  data-isdate='" . $Note->CreationDate->date . "'   data-isdatedetailed='" . json_encode($Note->CreationDate) . "'>"
		                    ."<span class='cal-date'>" . date('D d M Y h:iA', strtotime($Note->CreationDate->date)) . "</span>"
		                    ."<small class='note-action'>" . $callrecording . " </small>"
		                    ."<small class='staffname'>" . $staffArr . "</small>"
	                    ."</span><!-- /.noteDate -->"
                        
                        . $TheNote['rows']
                        ."<div class='note-footer'><i class='fa fa-tags' aria-hidden='true'></i>" . "
                    <input id='tags_" . rand() . "' type='text' data-noteid='" . $noteId . "' class='note-tags' value='" . $tags . "' />" .
                        '<div class="tagDisplay">' . $tags_display . '</div></div></td></tr>';
                }
                else {
                    //$CallType = $this->lang->line('text_legacy').' '.$this->lang->line('text_'.$ObjectType);
                    $CallType = 'Infusionsoft ' . $this->lang->line('text_' . $ObjectType);
                    $ActionDate = '';
                    if(strtolower($ObjectType) == 'task'){
                        $ActionDate = "<span class='action-date'>Action Date: ".$Note->ActionDate->date."</span>";
                    }
                    $ActionDescription = mb_convert_encoding($ActionDescription, "HTML-ENTITIES", "UTF-8");
                    if (strstr(strtolower($ActionDescription), 'changed info for')) continue;
                    // removed the ability to edit non-macanta notes
                    //$rows ='<div class=""><span class="noteLabel"><small><span class="show-editor  nb-btn icon-btn txt-btn"><i class="fa fa-sticky-note" aria-hidden="true"></i> '.$this->lang->line('text_edit').' '.$this->lang->line('text_'.$ObjectType).'</span> <span class="close-editing  nb-btn icon-btn txt-btn"><i class="fa fa-floppy-o" aria-hidden="true"></i> '.$this->lang->line('text_save').' '.$this->lang->line('text_'.$ObjectType).'</span><span class="cancel-editing  nb-btn icon-btn txt-btn"><i class="fa fa-stop-circle-o" aria-hidden="true"></i> '.$this->lang->line('text_cancel').'</span></small></span><div id="noteTextRich'.rand().'" class="Notes noteTextRich" data-tinymce="true">'.$note.'</div></div>';
                    $note = str_replace("\n", "<br>", $note);
                    $note = mb_convert_encoding($note, "HTML-ENTITIES", "UTF-8");
                    $AssignedToText = '';
                    $Toggles = '';

                        if ($this->config->item('TaskCustomFields')) {
                            $TaskCustomFields = json_decode($this->config->item('TaskCustomFields'), true);
                            $TaskAssignedTo = '_' . $TaskCustomFields['Task assigned to'];
                            if (isset($Note->$TaskAssignedTo) && $Note->$TaskAssignedTo !== ''){
                                $AssignedToText = '<div class=" assigned-to">Assigned to: <strong>' . $Note->$TaskAssignedTo . '</strong></div>';
                            }else{
                                foreach ($IS_Users as $IS_User){
                                    if($IS_User->Id == $Note->UserID){
                                        $AssignedToText = '<div class=" assigned-to">Assigned to:<strong>' . $IS_User->FirstName .' '. $IS_User->LastName. '</strong></div>';
                                        break;
                                    }
                                }
                            }

                        }
                    if ($Note->ObjectType == 'Task') {
                        //add complete toggle
                        $AssignBy = 'System '.$NativeNotice;
                        foreach ($IS_Users as $IS_User){
                            if($IS_User->Id == $Note->UserID){
                                $AssignBy = $IS_User->FirstName .' '. $IS_User->LastName;
                                break;
                            }
                        }
                        //$CallType = 'Assign by ' . $this->lang->line('text_' . $ObjectType);
                        $CallType = '<span class="assigned-by">Assigned by:<strong>' . $AssignBy . '</strong></span>';
                        $CompletionDate = isset($Note->CompletionDate) ? $Note->CompletionDate->date : "";
                        $Toggles = '<div data-contactid="' . $UserInfo->Id . '" data-completiondate="' . $CompletionDate . '" data-noteid="' . $noteId . '" class="toggleThisTaskInside toggle-iphone"></div>';
                    }
                    $rows = '<div class=""><div  class="Notes noteTextRich" >' . $note . '</div></div>' . $Toggles . $AssignedToText;
                    $TotalNotesArr[] = "<tr class='NoteItem' data-noteid='" . $noteId . "'  data-staffid='" . $Note->CreatedBy . "'  data-timestamp='" . strtotime($Note->CreationDate->date) . "'   data-tags='" . $tags . "' data-notetype='" . $CallType . "'>
                    <td class='noteDate'  data-callid='' colspan='2' >
	                    <div class='note-header'>
		                    <span  class='noteDate'  data-isdate='" . $Note->CreationDate->date . "'   data-isdatedetailed='" . json_encode($Note->CreationDate) . "'>"
			                    ."<span class='cal-date'>" . date('D d M Y h:iA', strtotime($Note->CreationDate->date)) ."</span>"
			                    ."<small class='legacyNote'>" . $CallType . '</small>
			                         <span class="actionTitle legacyNoteTitle">' . $ActionDescription . $ActionDate."</span>
		                    </span><!-- /.noteDate -->
	                    </div><!-- /.note-header -->"
                        . $rows .
                        "<div class='note-footer'>" . "
                    <div class='flex-row'><input id='tags_" . rand() . "' type='text' data-noteid='" . $noteId . "' class='note-tags' value='" . $tags . "' />" .
                        '<div class="tagDisplay">' . $tags_display . '</div><i class="fa fa-tags" aria-hidden="true"></i></div></td></tr>';
                }


            }
        }
        //$TotalNotesArr = array_reverse($TotalNotesArr);
        $TotalNotes = implode("\r\n", $TotalNotesArr);
        $PrevNotes = '<table>' . $TotalNotes . '</table>';
        return $PrevNotes;
    }

    public function getNoteTag($NoteId)
    {
        $this->db->where('note_id', trim($NoteId));
        $query = $this->db->get('note_tags');
        $row = $query->row();
        if (isset($row)) {
            return $row->tag_slugs;
        }
        return '';
    }

    public function filterNote($data)
    {
        $conId = $data['conId'];
        $searchStr = $data['searchStr'];
        $dateRange = $data['dateRagne'];
        $tagInserted = $data['tagInserted'];
        $staffSelected = $data['staffSelected'];
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $Notes = infusionsoft_get_contact_notes_by_contact_id($conId);
        $hasSearchStr = $this->noteSearchStr($Notes, $searchStr);
        $hasTagInserted = $this->noteSearchTag($Notes, $tagInserted, $hasSearchStr, $conId);
        $hasDateRagne = $this->noteSearchDate($Notes, $dateRange, $hasTagInserted);
        $FinalArray = $this->noteSearchStaff($Notes, $staffSelected, $hasDateRagne);
        $FilteredNotes = $this->finalFilter($FinalArray, $Notes);
        $ContactNotes['PrevNotes'] = $this->parseNote($FilteredNotes,$session_data);
        $Editor = $this->load->view('core/call_add_quick_note', $ContactNotes, true);
        $this->ajaxResults['data'] = $Editor;
        $this->ajaxResults['message'] = $FilteredNotes;
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['script'] = 'Macanta("tab/Note filterNote");';
        return $this->ajaxResults;
    }

    public function noteSearchStr($Notes, $searchStr)
    {

        $searchStrArr = explode(" ", $searchStr);
        foreach ($Notes as $Note) {
            $noteId = $Note->Id;
            $ActionDescription = $Note->ActionDescription; // not used
            $note = html_entity_decode($Note->CreationNotes);
            if (isJson($note)) {
                $noteObj = json_decode($note);

                foreach ($noteObj->note as $key => $value) {
                    $note = base64_decode($value) != false ? base64_decode($value) : $value;
                }
            }
            if ($searchStr == '') {
                $FoundNote[$noteId]['str'] = 1;

            } else {
                foreach ($searchStrArr as $key) {
                    if (stristr($note, $key) != false) {
                        $FoundNote[$noteId]['str'] = 1;
                        break;
                    }
                }

            }
        }
        return $FoundNote;
    }

    public function noteSearchTag($Notes, $tagInserted, $FoundNote, $conId)
    {

        $tagInsertedArr = explode(",", $tagInserted);
        foreach ($Notes as $Note) {
            $noteId = $Note->Id;
            $found = false;
            $CreationNotes = $Note->CreationNotes;
            $tags = $this->getNoteTag($noteId);
            $tagsArr = explode(",", $tags);
            if ($tagInserted == '') {
                $FoundNote[$noteId]['tag'] = 1;
            } else {
                foreach ($tagsArr as $tag) {
                    if (in_array($tag, $tagInsertedArr)) {
                        $FoundNote[$noteId]['tag'] = 1;
                        $found = true;
                        break;
                    }
                }
                if ($found === false) {
                    foreach ($tagInsertedArr as $INFtag) {
                        if (strpos($CreationNotes, $INFtag) !== false) {
                            $FoundNote[$noteId]['tag'] = 1;
                            $ExistingTag = $this->getNoteTag($noteId);
                            $ExistingTagArr = $ExistingTag == '' ? [] : explode(',', $ExistingTag);
                            $ExistingTagArr[] = $INFtag;
                            $ExistingTagStr = implode(',', $ExistingTagArr);
                            $data['note_id'] = $noteId;
                            $data['tags'] = $ExistingTagStr;
                            $data['conId'] = $conId;
                            $this->updateTagNotes($data);
                            break;
                        }
                    }
                }

            }


        }
        return $FoundNote;
    }

    public function updateTagNotes($data)
    {
        $NoteId = $data['note_id'];
        $tagsStr = $data['tags'];
        $contactId = $data['conId'];
        //$SelectedContactResult = storeTaggedSelectedContact($data);
        $DBdata = array(
            'tag_slugs' => $tagsStr
        );
        if (false == $this->DbExists('note_id', $NoteId, 'note_tags')) {
            $DBdata['note_id'] = $NoteId;
            $DBdata['contact_id'] = $contactId;
            $this->db->insert('note_tags', $DBdata);
        } else {
            $this->db->where('note_id', $NoteId);
            $this->db->update('note_tags', $DBdata);
        }
        $tagsArr = explode(',', $tagsStr);
        $tags_display = '';
        if ($tagsStr && $tagsStr !== '') {
            foreach ($tagsArr as $tag_item) {
                $tags_display .= "<span>$tag_item</span>";
            }
        } else {
            $tags_display = '- ' . $this->lang->line('text_no_tags') . ' -';
        }
        $tag_script = '$("tr[data-noteid=' . $NoteId . '] .tagDisplay").html("' . $tags_display . '");$("input[data-noteid=' . $NoteId . ']").val("' . $tagsStr . '")';
        foreach ($tagsArr as $tag) {
            $tagSlug = strtolower($tag);
            $tagName = $tag;
            $tagData = $this->DbExists('tag_slug', $tagSlug, 'tags');
            if (!$tagData) {
                /*Save Unique Tag To DB*/
                $DBdata = array(
                    'tag_slug' => $tagSlug,
                    'tag_name' => $tagName
                );
                $this->db->insert('tags', $DBdata);
            }

        }
        $availableTags = $this->notetags_get();
        //update available tags
        $this->ajaxResults['script'] = 'availableTags=' . $availableTags . '; ' . $tag_script . ';$( ".tagsinput input").val("");';
        $this->ajaxResults['data'] = $data;
        $this->ajaxResults['message'] = 'Tag Saved, ';
        $this->ajaxResults['status'] = 1;


        return $this->ajaxResults;
    }

    public function DbExists($field, $value, $table)
    {
        $this->db->where($field, $value);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {

            foreach ($query->result() as $row) {
                return $row;
            }
            return false;
        } else {

            return false;
        }
    }

    public function notetags_get()
    {
        $tags = array();
        $tagsStr = array();
        $query = $this->db->get('tags');
        foreach ($query->result() as $row) {
            $tagsStr[] = $row->tag_name;
            $tags[] = array(
                'id' => $row->tag_slug,
                'label' => $row->tag_name,
                'value' => $row->tag_name
            );
        }
        //echo  json_encode($tags);
        return json_encode($tagsStr);
    }

    public function noteSearchDate($Notes, $dateRange, $FoundNote)
    {
        foreach ($Notes as $Note) {
            $noteId = $Note->Id;
            if ($dateRange == '') {
                $FoundNote[$noteId]['date'] = 1;
            } else {
                $dateRangeArr = explode(" to ", $dateRange);
                $fromStamp = strtotime($dateRangeArr[0]);
                $toStamp = strtotime($dateRangeArr[1]) + 86400;
                $noteDateStamp = strtotime($Note->CreationDate->date);
                if ($noteDateStamp >= $fromStamp && $noteDateStamp <= $toStamp) {
                    $FoundNote[$noteId]['date'] = 1;
                }
            }
        }
        return $FoundNote;
    }

    public function noteSearchStaff($Notes, $staffSelected, $FoundNote)
    {
        foreach ($Notes as $Note) {
            $noteId = $Note->Id;
            if ($staffSelected == '') {
                $FoundNote[$noteId]['staff'] = 1;
            } else {
                //$staffSelectedArr = explode(",",$staffSelected);
                if (in_array($Note->CreatedBy, $staffSelected)) {
                    $FoundNote[$noteId]['staff'] = 1;
                }
            }
        }
        return $FoundNote;
    }

    public function finalFilter($StatusArr, $Notes)
    {
        $NewNotes = $Notes;
        foreach ($Notes as $key => $Note) {
            $noteId = $Note->Id;
            if (!isset($StatusArr[$noteId]['staff']) ||
                !isset($StatusArr[$noteId]['date']) ||
                !isset($StatusArr[$noteId]['tag']) ||
                !isset($StatusArr[$noteId]['str'])
            ) unset($NewNotes[$key]);
        }
        return $NewNotes;
    }

    public function refreshNote($data)
    {
        $Notes = infusionsoft_get_contact_notes_by_contact_id($data['conId']);
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $ContactNotes['PrevNotes'] = $this->parseNote($Notes,$session_data);
        $Editor = $this->load->view('core/call_add_quick_note', $ContactNotes, true);
        $this->ajaxResults['data'] = $Editor;
        $this->ajaxResults['message'] = "Rereshed Note";
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['script'] = 'Macanta("tab/Note refreshNote");';
        return $this->ajaxResults;
    }

    public function addNote($data)
    {
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $UserInfo = json_decode($session_data['Details']);
        $SaveResult = infusionsoft_add_note($data, $UserInfo);
        $tag_data['note_id'] = $SaveResult->message;
        $tag_data['conId'] = $data['conId'];
        $NoteType = '#'.strtolower($data['NoteType']);
        if(!empty($data['tags']) || $data['tags'] != ''){
            $tag_data['tags'] = $NoteType.','.$data['tags'];

        }else{
            $tag_data['tags'] = $NoteType;
        }
        $this->updateTagNotes($tag_data);
        $Notes = infusionsoft_get_contact_notes_by_contact_id($data['conId']);
        $availableTags = $this->notetags_get();
        $ContactNotes['PrevNotes'] = $this->parseNote($Notes,$session_data);
        $Editor = $this->load->view('core/call_add_quick_note', $ContactNotes, true);
        $this->ajaxResults['data'] = $Editor;
        $this->ajaxResults['message'] = $SaveResult;
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['script'] = 'noteCollapsible();'.'availableTags=' . $availableTags . '; Macanta("tab/Note addNote");toggleThisTaskInside();';
        return $this->ajaxResults;
    }

    public function addTask($data)
    {
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $UserInfo = json_decode($session_data['Details']);
        $SaveResult = infusionsoft_add_task($data, $UserInfo,$session_data);
        $noteId = $SaveResult['result'];
        $tags = $this->getNoteTag($noteId);
        $ExistingTagArr = $tags == '' ? [] : explode(',', $tags);
        if (!in_array('#task', $ExistingTagArr)) {
            $ExistingTagArr[] = '#task';
            $ExistingTagStr = implode(',', $ExistingTagArr);
            $Param['note_id'] = $noteId;
            $Param['tags'] = $ExistingTagStr;
            $Param['conId'] = $data['conId'];
            $this->updateTagNotes($Param);
        }
        $Notes = infusionsoft_get_contact_notes_by_contact_id($data['conId']);
        $availableTags = $this->notetags_get();
        $ContactNotes['PrevNotes'] = $this->parseNote($Notes,$session_data);
        $Editor = $this->load->view('core/call_add_quick_note', $ContactNotes, true);
        $this->ajaxResults['data'] = $Editor;
        $this->ajaxResults['message'] = $SaveResult;
        $this->ajaxResults['status'] = 1;
        $this->ajaxResults['script'] = 'noteCollapsible();'.'availableTags=' . $availableTags . '; Macanta("tab/Note addNote"); toggleThisTaskInside();';
        return $this->ajaxResults;
    }

    public function compeleteTask($data)
    {

        $noteContactId = $data['noteContactId'];
        $noteId = $data['noteId'];
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $UpdateResult = infusionsoft_compeleteTask($noteContactId, $noteId,$data['session_name']);
        $theData = array('note_id' => $noteId, 'conId' => $noteContactId);

        //Get Existing note tag if any
        $this->db->where('note_id', $noteId);
        $query = $this->db->get('note_tags');
        $ret = $query->row();
        $tags_display = '#task_completed';
        if (isset($ret)) {
            $tags = $ret->tag_slugs;
            $tags_displayArr = explode(',', $tags);
            if (!in_array($tags_display, $tags_displayArr)) {
                $tags_displayArr[] = $tags_display;
                $theData['tags'] = implode(",", $tags_displayArr);
                $this->updateTagNotes($theData);
                $this->ajaxResults['data'] = [$UpdateResult, $this->ajaxResults['data']];
            }
        } else {
            $theData['tags'] = $tags_display;
            $this->updateTagNotes($theData);
            $this->ajaxResults['data'] = [$UpdateResult];
        }
        $searched_cache = manual_cache_loader('searched_cache'.$session_data['InfusionsoftID'],true);
        $searched_cache = $searched_cache ? $searched_cache: '{}';
        //; SearchCache = $searched_cache;
        $this->ajaxResults['script'] .= "SearchCache = $searched_cache;";
        $this->ajaxResults['message'] = 'Task Completed., Loading: '.'searched_cache'.$session_data['InfusionsoftID'];
        $this->ajaxResults['status'] = 1;
        return $this->ajaxResults;
    }

    public function updateNote($data)
    {
        $user_seession_data = macanta_get_user_seession_data($data['session_name']);
        $session_data = unserialize($user_seession_data->session_data);
        $UserInfo = json_decode($session_data['Details']);
        $NoteType = $data['notesType'];
        $Notes = json_encode($data['macantaNotes']);
        $MacantaNote = '{"note_type":"' . $NoteType . '","user":"' . $UserInfo->FirstName . " " . $UserInfo->LastName . '", "note":' . $Notes . '}';
        $data['macantaNotes'] = $MacantaNote;
        $UpdateResult = infusionsoft_update_note($data, $UserInfo);
        $this->ajaxResults['data'] = $UpdateResult;
        $this->ajaxResults['message'] = 'Note Updated';
        $this->ajaxResults['status'] = 1;
        return $this->ajaxResults;

    }


}
