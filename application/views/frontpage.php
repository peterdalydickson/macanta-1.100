<script>
    var DevicePlatform = "<?php echo $browser['platform']; ?>";
    console.log("Platform: " + DevicePlatform + ", Welcome to macanta console!");
</script>
<style type="text/css">
    .logoutautologin {
        display: none;
    }
</style>

<?php
if ($browser['platform'] == 'iOS' || $browser['platform'] == "Android") {
    ?>
    <style>
        .or, .other-login-method {
            display: none;
        }
    </style>
    <?php
}
//$DVE = macanta_validate_feature('DVE');
$DVE = 'enabled';
$appname = $this->config->item('MacantaAppName');
if (empty($DVE) || $DVE != "enabled") {
    $CSS_status = "status-off";
    $DVE_status = "OFF";
    $LeadMsg = '<a href="https://macanta.org/add-data-validation-enrichment/?app_name=' . $appname . '" target="_blank">Click here to find out more and enable.</a>';

} else {
    $CSS_status = "status-on";
    $DVE_status = "ON";
    $LeadMsg = '';
}
$ConnectedInfosEncoded = macanta_get_config('connected_info');
if (!$ConnectedInfosEncoded) $ConnectedInfosEncoded = '[]';
$ConnectedInfos = json_decode($ConnectedInfosEncoded, true);
?>
<div class="front-page-logo main-header <?php echo isset($class_alignleft) ? $class_alignleft : ""; ?>>
    <a href="<?php echo $this->config->item('base_url'); ?>" class="loginLogo">
        <?php
        $SiteLogo = macanta_db_record_exist('key', 'macanta_custom_logo', 'config_data', true);
        if ($SiteLogo) {
            $SiteLogoArr = json_decode($SiteLogo->value)
            ?>
            <img class="loginLogoImg"
                 src="data:<?php echo $SiteLogoArr->type; ?>;base64,<?php echo $SiteLogoArr->imageData; ?>" style=" "
                 alt="">
        <?php } else { ?>
            <img class="loginLogoImg" src="<?php echo $this->config->item('base_url'); ?>assets/img/Macanta-Logo_Colour.png"
                 style=" " alt="">
        <?php }
        ?>

    </a>

    <div class="description">
        <!--Welcome to  <strong>macanta</strong>-->
        <div class="logoutautologin">
            <div class="toggleThisAgentContainer">
                <div class="toggleThisAgentContent">
                    <h4>Agent Status: </h4>
                    <div class="toggleThisAgent toggle-iphone"></div>
                </div>
                <div id="inboundactivity" class="inbound-activity   "></div>
                <div id="inboundlog" class="inbound-log  "></div>
            </div>
            
            <div class="sec2">
<!--                 <div class="AutoLoginCopy">
                </div> -->
            </div>
            <div class="sec1">
                <!-- ⬇︎⬇︎⬇︎ CONTENT LOCATION: views > core > contact_info.php -->
                <small class="dev-only php">
                    <span>dashboard.php</span>
                    <span>CONTENT LOCATION: views > core > contact_info.php</span>
                </small>

            </div>


        </div>


    </div>
</div>


<div class="front-page page-content">

    <div class="front-page-body">
        <?php echo $FrontPage; ?>

    </div>
    <div class="page-footer">
        <div class="footer-content" style="display: none;">
            <div class="footer-logo">
               <a href="<?php echo $this->config->item('base_url'); ?>" class="loginLogo">
                    <?php
                    $SiteLogo = macanta_db_record_exist('key', 'macanta_custom_logo', 'config_data', true);
                    if ($SiteLogo) {
                        $SiteLogoArr = json_decode($SiteLogo->value)
                        ?>
                        <img class="loginLogoImg"
                             src="data:<?php echo $SiteLogoArr->type; ?>;base64,<?php echo $SiteLogoArr->imageData; ?>" style=" "
                             alt="">
                    <?php } else { ?>
                        <img class="loginLogoImg" src="<?php echo $this->config->item('base_url'); ?>assets/img/Macanta-Logo_Colour.png"
                             style=" " alt="">
                    <?php }
                    ?>

                </a> 
            </div>
            <div class="footer-buttons">
                    <a class="nb-btn icon-btn nb-primary knowledge-base" href="//docs.macanta.org/encyclopedia" title="Find answers on the Macanta Knowledge Base" target="_blank">
                        <i class="fa fa-university"></i>
                        <span>Macanta Knowledge Base</span>
                    </a>
                <a class="nb-btn icon-btn nb-base-color gotoAdmin "><i class="fa fa-cogs" aria-hidden="true"></i> Goto Admin</a>
                <button class="nb-btn icon-btn nb-base-color nb-muted logout logout-from-dashboard text_logout" type="button"
                        onclick="logout(); return false;"><i class="fa fa-sign-out"></i>
                    <?php echo $this->lang->line('text_logout'); ?></button>
            </div>
                <div class="AutoLoginCopy">
                </div>
        </div>
        <div class="footnote"><span class="text_fed_by"><?php echo $this->lang->line('text_fed_by'); ?></span> <!--fed by-->
            <img class="macantaicon" src="<?php echo $this->config->item('base_url'); ?>assets/img/macantaicon.png"
                 style=" " alt=""><strong>macanta</strong>
        </div>
        <div class="app_version">
            <?php
    
            //$FileLocVersion = basename(dirname(dirname(dirname(__FILE__))));
            //echo $FileLocVersion;
            echo $this->config->item('macanta_verson');
            echo "<!--".dirname(__FILE__)."-->";
            ?>
        </div>
        <div class="credits">
            <a href="//www.onlinewebfonts.com">fonts supplied by oNline Web Fonts</a>
        </div>
    </div>
</div>


<!--Modal For Adding Call Center Agent-->
<div id="AddCallCenterAgent" class="modal fade AddCallCenterAgent " tabindex="-1" role="dialog"
     aria-labelledby="AddCallCenterAgent">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Choose Contact to Add as Agent</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3>Type Contact Name</h3>
                <input type="text" name="search-contact-tobe-agent" class="search-contact-tobe-agent form-control"
                       value="" title="" required="required">
                <div class="">
                    <table class="contact-preloading-list table table-striped table-bordered table-hover" width="100%" style="width: 100%;">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="nb-btn nb-base-color"
                            data-dismiss="modal"><?php echo $this->lang->line('text_close'); ?></button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>


<!--Modal For Editing ConnectOtherContact-->
<div id="ConnectOtherContact" class="modal fade ConnectOtherContact " tabindex="-1" role="dialog"
     aria-labelledby="ConnectOtherContact">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Search For Contact</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3>Type Contact Name</h3>
                <input type="text" name="search-contact-to-connect" class="search-contact-to-connect form-control"
                       value="" title="" required="required">
                <div class="contact-preloading">
                    <table class="contact-preloading-list table table-striped table-bordered table-hover" width="100%" style="width: 100%;">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="nb-btn nb-secondary"
                            data-dismiss="modal"><?php echo $this->lang->line('text_close'); ?></button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>

<!--Modal For Add Conencted Data-->
<div id="ConnectOtherData" class="modal fade ConnectOtherData " tabindex="-1" role="dialog"
     aria-labelledby="ConnectOtherData">
    <div class="modal-dialog connect-other-data">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Choose your connected data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3 class="label-sm">*Note: ONLY connected data with a relationship will be added</h3>
                <div class="flex-row">
                    <div class="connected-data-sidebar-filter">
                        <div class="AllConnectedDataContainer">
                            <div class="radio">
                                <input type="radio" name="AllConnectedData" id="AllConnectedData" value="AllConnectedData" checked="" class="remove-connected-data-filter">
                                <label class="label-sm" for="radio1" class="remove-connected-data-filter">
                                    All Connected Data
                                </label>
                            </div>

                        </div>
    
                        <div class="ByConnectedContactContainer">
                            <div class="filter-contact-list">
    
                                <div class="radio">
                                    <input type="radio" name="ByConnectedContact" id="ByConnectedContact" value="AllConnectedData" class="apply-connected-data-filter">
                                    <label class="label-sm" for="ByConnectedContact">
                                        Filter By Contacts
                                    </label>
                                </div>
                                <ul class="ul-filter-contact-list">
    
                                </ul>
                            </div>
                            <!--<div class="filter-relation-list">
                                <div class="checkbox">
                                    <input id="ByRelationship" type="checkbox"  class="apply-connected-data-filter">
                                    <label for="ByRelationship">
                                        Filter By Relationship
                                    </label>
                                </div>
                                <select title="Filter Relation List" multiple>
                                    <option class="filter-option-item">
                                        Relation Here
                                    </option>
                                </select>
                            </div>-->
                        </div>
    
                    </div>
                    <div class="connected-data-sidebar-results">
    
    
                    </div>
                </div><!-- /.flex-row -->    
                <div class="btn-container">
                    <button type="button" class="nb-tn nb-primary icon-btn  addBulkConnectedInfo" disabled>
                        <i class="fa fa-plus-square-o"></i> Connect Contact
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
<!--Modal For Permission Info-->
<div id="PermissionDetails" class="modal fade PermissionDetails" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Here's how to know if you have permission to market to an address</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul>

                    <li> This email address was not purchased, rented, borrowed, or harvested.</li>
                    <li> This is not an old, non-responsive email from another list.</li>
                    <li> I can show proof of consent to receive marketing for this email address.</li>
                    <li> I have recently - as in, the last few months - contacted this person.</li>
                    <li> They are expecting the content I intend to send.</li>
                    <li> I have let them know how frequently I will be emailing them.</li>
                    <li> I trust that they provided me with correct information. (offline sign-up forms that require
                        email addresses are often faked or entered incorrectly)
                    </li>

                </ul>
                <div class="modal-footer">
                    <button type="button" class="nb-btn nb-secondary" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
<!--Modal For Address List-->
<div id="ContactAddressList" class="modal fade ContactAddressList" tabindex="-1" role="dialog"
     aria-labelledby="ContactAddressList">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Found Address</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h4 class="currentAddress hideThis">Current Address:
                    <small></small>
                </h4>
                <h4>Please select correct address and confirm.</h4>
                <div class="AddressContainer">
                    <ul class="AddressMainList">

                    </ul>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="nb-btn nb-secondary"
                        data-dismiss="modal">Cancel
                </button>
                <button type="button"
                        class="btn btn-primary VerifySelectedAddressItem">Confirm
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!--Modal For Connected Data File Attachment-->
<div id="CDFileAttachment" class="modal fade CDFileAttachment" tabindex="-1" role="dialog"
     aria-labelledby="CDFileAttachment">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">File Attachment</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="file-container">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="nb-btn nb-secondary"
                        data-dismiss="modal">Close
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!--Modal For Connected Data Add/Edit Field-->
<div id="CDField" class="modal fade CDField" tabindex="-1" role="dialog" aria-labelledby="CDField">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Field Properties</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="FormFieldDetails">
                    <input type="hidden" name="fieldId" value="">
                    <ul class="connectorsFieldDetails">
                        <li class="field-details">
                            <div class="">
                                <h3>Label</h3>
                                <small>This is the name which will appear in before the field.</small>
                            </div>
                            <div class="">
                                <input type="text" id="<?php echo macanta_generate_key('fieldLabel_', 5); ?>" name="fieldLabel"
                                       class="form-control field-input" value="" title=""
                                       required="required">
                            </div>
                        </li>

                        <li class="field-details">
                            <div class="">
                                <h3>Type</h3>
                            </div>
                            <div class="">
                                <select name="fieldType" id="fieldType" class="form-control">
                                    <option value=""> -- Select One --</option>
                                    <optgroup label="Basic">
                                        <option value="Text">Text</option>
                                        <option value="TextArea">Text Area</option>
                                        <option value="Number">Whole Number</option>
                                        <option value="Currency">Decimal Number</option>
                                        <option value="Email">Email</option>
                                        <option value="Password">Password</option>
                                    </optgroup>
                                    <optgroup label="Choice">
                                        <option value="Select">Select</option>
                                        <option value="Checkbox">Checkbox</option>
                                        <option value="Radio">Radio Button</option>
                                    </optgroup>
                                    <optgroup label="Other">
                                        <option value="Date">Date Picker</option>
                                        <option value="DateTime">Date/Time Picker</option>
                                        <option value="URL">URL</option>
                                        <option value="Repeater" disabled="disabled">Repeater (coming soon)</option>
                                    </optgroup>
                                    <optgroup label="Data Reference">
                                    <?php
                                    foreach($ConnectedInfos as $ShKey=>$SharedConnector)
                                    {
                                        foreach($SharedConnector['fields'] as $SharedFields)
                                        {
                                            if($SharedFields['useAsPrimaryKey']=='yes')
                                            {
                                                ?>
                                                <option data-cdkey="<?php echo $ShKey;?>" value="<?php echo $SharedFields['fieldId'];?>">
                                                    <?php echo $SharedConnector['title'];?> (<?php echo $SharedFields['fieldLabel'];?>)
                                                </option>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                    </optgroup>
                                </select>
                            </div>
                        </li>
                        <li class="field-details">
                            <div class="">
                                <h3>Data Reference?</h3>
                            </div>
                            <div class="">
                                <label>
                                    <input type="radio"
                                           name="useAsPrimaryKey" value="no" checked>
                                    No
                                </label>
                                <label>
                                    <input type="radio"
                                           name="useAsPrimaryKey" value="yes" >
                                    Yes
                                </label>
                            </div>
                        </li>
                        <li class="field-details">
                            <div class="">
                                <h3>Required?</h3>
                            </div>
                            <div class="">
                                <label>
                                    <input type="radio" name="requiredField" value="no" checked="checked">
                                    No
                                </label>
                                <label>
                                    <input type="radio"  name="requiredField" value="yes">
                                    Yes
                                </label>
                            </div>
                        </li>
                        <li class="field-details">
                            <div class="">
                                <h3>Contact Specific?</h3>
                            </div>
                            <div class="">
                                <label>
                                    <input type="radio" class="contactspecificField"
                                           name="contactspecificField" value="no"
                                           checked="checked"
                                    >
                                    No
                                </label>
                                <label>
                                    <input type="radio" class="contactspecificField"
                                           name="contactspecificField" value="yes"
                                    >
                                    Yes
                                </label>
                            </div>
                        </li>
                        <li class="field-details">
                            <div class="">
                                <h3>Default Value</h3>
                                <small>Predefined value for this field</small>
                            </div>
                            <div class="">
                                <input type="text" id="defaultValue" name="defaultValue"
                                       class="form-control  field-input" value=""
                                       title="" required="required">
                            </div>
                        </li>
                        <li class="field-details">
                            <div class="">
                                <h3>Place Holder Text</h3>
                                <small>Text to show when the field is blank</small>
                            </div>
                            <div class="">
                                <input type="text" id="placeHolder" name="placeHolder" class="form-control field-input"
                                       value=""
                                       title="" required="required">
                            </div>
                        </li>

                        <li class="field-details">
                            <div class="">
                                <h3>Helper Text</h3>
                                <small>Some information regarding this field if needed.</small>
                            </div>
                            <div class="">
                                 <textarea name="helperText" onkeyup="textAreaAdjust()" class="form-control field-input helperText" required="required"></textarea></div>
                                <div class="preview_content" style="display:none;"></div>
                                <a class="fa fa-eye markdown_preview" aria-hidden="true" title="Preview"></a><a class="fa fa-edit undo_markdown_preview" aria-hidden="true" title="Write" style="display:none;"></a> <a href="https://www.markdownguide.org/cheat-sheet" target="_blank" class="helperResourceurl">Markdown Guide</a>
                        </li>

                        <li class="field-details">
                            <div class="">
                                <h3>Infusionsoft Custom Field</h3>
                                <small>Field to be updated in Infusionsoft</small>
                            </div>
                            <div class="">
                                <select name="infusionsoftCustomField" id="infusionsoftCustomField" class=" form-control"
                                        data-size="false">
                                </select>
                                <script>

                                </script>
                            </div>
                        </li>
                        <li class="field-details">
                            <div class="">
                                <h3>Use As List Header</h3>
                            </div>
                            <div class="">
                                <label>
                                    <input type="radio" name="showInTable" value="no" checked="checked">
                                    No
                                </label>
                                <label>
                                    <input type="radio" name="showInTable" value="yes">
                                    Yes
                                </label>
                                <label>
                                    <input type="number" name="showOrder" value="0" class="ordernumber">
                                    Order
                                </label>
                            </div>
                        </li>
                        <li class="field-details">
                            <div class="">
                                <h3>Section Tag ID</h3>
                                <small>Some information regarding this field if needed.</small>
                            </div>
                            <div class="">
                                <input type="text" id="sectionTagId" name="sectionTagId" class="form-control field-input"
                                       value=""
                                       title="" required="required"></div>
                        </li>
                        <li class="field-details">
                            <div class="">
                                <h3>Sub-Section</h3>
                                <small>Group-name to group fields inside sections.</small>
                            </div>
                            <div class="">
                                <input type="text" id="fieldSubGroup" name="fieldSubGroup" class="form-control field-input"
                                       placeholder="Group A"
                                       value=""
                                       title=""></div>

                        </li>
                        <li class="field-details">
                            <div class="">
                                <h3>Add Divider After This Field</h3>
                            </div>
                            <div class="">
                                <label>
                                    <input type="radio" name="addDivider" value="no" checked="checked">
                                    No
                                </label>
                                <label>
                                    <input type="radio" name="addDivider" value="yes">
                                    Yes
                                </label>
                            </div>
                        </li>
                        <li class="field-details">
                            <div class="">
                                <h3>Add Heading Text Above This Field</h3>
                            </div>
                            <div class="">
                                <input type="text" name="fieldHeadingText" class="form-control field-input"
                                       value=""
                                       title="" >
                            </div>
                        </li>
                    </ul>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="nb-btn nb-secondary"
                        data-dismiss="modal">Close
                </button>
                <button type="button"
                        class="nb-btn nb-primary CDSaveField">Save
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- Modal -->
<?php
/*$MediaPresentation = $this->config->item('MediaPresentation');
if(isset($MediaPresentation['Type']) && $MediaPresentation['Type'] != 'wistia'){
    */ ?><!--
    <div class="modal fade MediaPresentation" id="MediaPresentation" tabindex="-1" role="dialog" aria-labelledby="MediaPresentationLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><?php /*echo $MediaPresentation['Title']; */ ?></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <iframe id="iframeYoutube" width="560" height="315"  src="<?php /*echo $MediaPresentation['URL']; */ ?>" frameborder="0" allowfullscreen></iframe>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
--><?php /*}
*/ ?>


<select class="CountryTeplate hide" id="CountryTeplate" name="CountryTeplate">
    <option value="">Please select a country</option>
    <option value="Afghanistan">Afghanistan</option>
    <option value="Albania">Albania</option>
    <option value="Algeria">Algeria</option>
    <option value="American Samoa">American Samoa</option>
    <option value="Andorra">Andorra</option>
    <option value="Angola">Angola</option>
    <option value="Anguilla">Anguilla</option>
    <option value="Antarctica">Antarctica</option>
    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
    <option value="Argentina">Argentina</option>
    <option value="Armenia">Armenia</option>
    <option value="Aruba">Aruba</option>
    <option value="Australia">Australia</option>
    <option value="Austria">Austria</option>
    <option value="Åland Islands">Åland Islands</option>
    <option value="Azerbaijan">Azerbaijan</option>
    <option value="Bahamas">Bahamas</option>
    <option value="Bahrain">Bahrain</option>
    <option value="Bangladesh">Bangladesh</option>
    <option value="Barbados">Barbados</option>
    <option value="Belarus">Belarus</option>
    <option value="Belgium">Belgium</option>
    <option value="Belize">Belize</option>
    <option value="Benin">Benin</option>
    <option value="Bermuda">Bermuda</option>
    <option value="Bhutan">Bhutan</option>
    <option value="Bolivia">Bolivia</option>
    <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
    <option value="Botswana">Botswana</option>
    <option value="Bouvet Island">Bouvet Island</option>
    <option value="Brazil">Brazil</option>
    <option value="British Indian Ocean Territory">British Indian Ocean
        Territory
    </option>
    <option value="Brunei Darussalam">Brunei Darussalam</option>
    <option value="Bulgaria">Bulgaria</option>
    <option value="Burkina Faso">Burkina Faso</option>
    <option value="Burundi">Burundi</option>
    <option value="Cambodia">Cambodia</option>
    <option value="Cameroon">Cameroon</option>
    <option value="Canada">Canada</option>
    <option value="Cape Verde">Cape Verde</option>
    <option value="Cayman Islands">Cayman Islands</option>
    <option value="Central African Republic">Central African Republic
    </option>
    <option value="Chad">Chad</option>
    <option value="Chile">Chile</option>
    <option value="China">China</option>
    <option value="Christmas Island">Christmas Island</option>
    <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
    <option value="Colombia">Colombia</option>
    <option value="Comoros">Comoros</option>
    <option value="Congo">Congo</option>
    <option value="Democratic Republic Of Congo">Democratic Republic Of
        Congo
    </option>
    <option value="Cook Islands">Cook Islands</option>
    <option value="Costa Rica">Costa Rica</option>
    <option value="Croatia">Croatia</option>
    <option value="Cuba">Cuba</option>
    <option value="Cyprus">Cyprus</option>
    <option value="Czech Republic">Czech Republic</option>
    <option value="Côte D'Ivoire">Côte D'Ivoire</option>
    <option value="Denmark">Denmark</option>
    <option value="Djibouti">Djibouti</option>
    <option value="Dominica">Dominica</option>
    <option value="Dominican Republic">Dominican Republic</option>
    <option value="Ecuador">Ecuador</option>
    <option value="Egypt">Egypt</option>
    <option value="El Salvador">El Salvador</option>
    <option value="Equatorial Guinea">Equatorial Guinea</option>
    <option value="Eritrea">Eritrea</option>
    <option value="Estonia">Estonia</option>
    <option value="Ethiopia">Ethiopia</option>
    <option value="Falkland Islands">Falkland Islands</option>
    <option value="Faroe Islands">Faroe Islands</option>
    <option value="Fiji">Fiji</option>
    <option value="Finland">Finland</option>
    <option value="France">France</option>
    <option value="French Guiana">French Guiana</option>
    <option value="French Polynesia">French Polynesia</option>
    <option value="French Southern Territories">French Southern
        Territories
    </option>
    <option value="Gabon">Gabon</option>
    <option value="Gambia">Gambia</option>
    <option value="Georgia">Georgia</option>
    <option value="Germany">Germany</option>
    <option value="Ghana">Ghana</option>
    <option value="Gibraltar">Gibraltar</option>
    <option value="Greece">Greece</option>
    <option value="Greenland">Greenland</option>
    <option value="Grenada">Grenada</option>
    <option value="Guadeloupe">Guadeloupe</option>
    <option value="Guam">Guam</option>
    <option value="Guatemala">Guatemala</option>
    <option value="Guernsey">Guernsey</option>
    <option value="Guinea">Guinea</option>
    <option value="Guinea-Bissau">Guinea-Bissau</option>
    <option value="Guyana">Guyana</option>
    <option value="Haiti">Haiti</option>
    <option value="Heard and McDonald Islands">Heard and McDonald Islands
    </option>
    <option value="Holy See (Vatican City State)">Holy See (Vatican City
        State)
    </option>
    <option value="Honduras">Honduras</option>
    <option value="Hong Kong">Hong Kong</option>
    <option value="Hungary">Hungary</option>
    <option value="Iceland">Iceland</option>
    <option value="India">India</option>
    <option value="Indonesia">Indonesia</option>
    <option value="Iran">Iran</option>
    <option value="Iraq">Iraq</option>
    <option value="Ireland">Ireland</option>
    <option value="Isle of Man">Isle of Man</option>
    <option value="Israel">Israel</option>
    <option value="Italy">Italy</option>
    <option value="Jamaica">Jamaica</option>
    <option value="Japan">Japan</option>
    <option value="Jersey">Jersey</option>
    <option value="Jordan">Jordan</option>
    <option value="Kazakhstan">Kazakhstan</option>
    <option value="Kenya">Kenya</option>
    <option value="Kiribati">Kiribati</option>
    <option value="North Korea">North Korea</option>
    <option value="South Korea">South Korea</option>
    <option value="Kuwait">Kuwait</option>
    <option value="Kyrgyzstan">Kyrgyzstan</option>
    <option value="Laos">Laos</option>
    <option value="Latvia">Latvia</option>
    <option value="Lebanon">Lebanon</option>
    <option value="Lesotho">Lesotho</option>
    <option value="Liberia">Liberia</option>
    <option value="Libya">Libya</option>
    <option value="Liechtenstein">Liechtenstein</option>
    <option value="Lithuania">Lithuania</option>
    <option value="Luxembourg">Luxembourg</option>
    <option value="Macao">Macao</option>
    <option value="Republic of Macedonia">Republic of Macedonia</option>
    <option value="Madagascar">Madagascar</option>
    <option value="Malawi">Malawi</option>
    <option value="Malaysia">Malaysia</option>
    <option value="Maldives">Maldives</option>
    <option value="Mali">Mali</option>
    <option value="Malta">Malta</option>
    <option value="Marshall Islands">Marshall Islands</option>
    <option value="Martinique">Martinique</option>
    <option value="Mauritania">Mauritania</option>
    <option value="Mauritius">Mauritius</option>
    <option value="Mayotte">Mayotte</option>
    <option value="Mexico">Mexico</option>
    <option value="Federated States of Micronesia">Federated States of
        Micronesia
    </option>
    <option value="Moldova">Moldova</option>
    <option value="Monaco">Monaco</option>
    <option value="Mongolia">Mongolia</option>
    <option value="Montenegro">Montenegro</option>
    <option value="Montserrat">Montserrat</option>
    <option value="Morocco">Morocco</option>
    <option value="Mozambique">Mozambique</option>
    <option value="Myanmar">Myanmar</option>
    <option value="Namibia">Namibia</option>
    <option value="Nauru">Nauru</option>
    <option value="Nepal">Nepal</option>
    <option value="Netherlands">Netherlands</option>
    <option value="Netherlands Antilles">Netherlands Antilles</option>
    <option value="New Caledonia">New Caledonia</option>
    <option value="New Zealand">New Zealand</option>
    <option value="Nicaragua">Nicaragua</option>
    <option value="Niger">Niger</option>
    <option value="Nigeria">Nigeria</option>
    <option value="Niue">Niue</option>
    <option value="Norfolk Island">Norfolk Island</option>
    <option value="Northern Mariana Islands">Northern Mariana Islands
    </option>
    <option value="Norway">Norway</option>
    <option value="Oman">Oman</option>
    <option value="Pakistan">Pakistan</option>
    <option value="Palau">Palau</option>
    <option value="Palestine">Palestine</option>
    <option value="Panama">Panama</option>
    <option value="Papua New Guinea">Papua New Guinea</option>
    <option value="Paraguay">Paraguay</option>
    <option value="Peru">Peru</option>
    <option value="Philippines">Philippines</option>
    <option value="Pitcairn">Pitcairn</option>
    <option value="Poland">Poland</option>
    <option value="Portugal">Portugal</option>
    <option value="Puerto Rico">Puerto Rico</option>
    <option value="Qatar">Qatar</option>
    <option value="Romania">Romania</option>
    <option value="Russian Federation">Russian Federation</option>
    <option value="Rwanda">Rwanda</option>
    <option value="Réunion">Réunion</option>
    <option value="St. Barthélemy">St. Barthélemy</option>
    <option value="St. Helena, Ascension and Tristan Da Cunha">St. Helena,
        Ascension and Tristan Da Cunha
    </option>
    <option value="St. Kitts And Nevis">St. Kitts And Nevis</option>
    <option value="St. Lucia">St. Lucia</option>
    <option value="St. Martin">St. Martin</option>
    <option value="St. Pierre And Miquelon">St. Pierre And Miquelon</option>
    <option value="St. Vincent And The Grenedines">St. Vincent And The
        Grenedines
    </option>
    <option value="Samoa">Samoa</option>
    <option value="San Marino">San Marino</option>
    <option value="Sao Tome and Principe">Sao Tome and Principe</option>
    <option value="Saudi Arabia">Saudi Arabia</option>
    <option value="Senegal">Senegal</option>
    <option value="Serbia">Serbia</option>
    <option value="Seychelles">Seychelles</option>
    <option value="Sierra Leone">Sierra Leone</option>
    <option value="Singapore">Singapore</option>
    <option value="Slovakia">Slovakia</option>
    <option value="Slovenia">Slovenia</option>
    <option value="Solomon Islands">Solomon Islands</option>
    <option value="Somalia">Somalia</option>
    <option value="South Africa">South Africa</option>
    <option value="South Georgia and the South Sandwich Islands">South
        Georgia and the South Sandwich Islands
    </option>
    <option value="Spain">Spain</option>
    <option value="Sri Lanka">Sri Lanka</option>
    <option value="Sudan">Sudan</option>
    <option value="Suriname">Suriname</option>
    <option value="Svalbard And Jan Mayen">Svalbard And Jan Mayen</option>
    <option value="Swaziland">Swaziland</option>
    <option value="Sweden">Sweden</option>
    <option value="Switzerland">Switzerland</option>
    <option value="Syrian Arab Republic">Syrian Arab Republic</option>
    <option value="Taiwan">Taiwan</option>
    <option value="Tajikistan">Tajikistan</option>
    <option value="Tanzania">Tanzania</option>
    <option value="Thailand">Thailand</option>
    <option value="Timor-Leste">Timor-Leste</option>
    <option value="Togo">Togo</option>
    <option value="Tokelau">Tokelau</option>
    <option value="Tonga">Tonga</option>
    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
    <option value="Tunisia">Tunisia</option>
    <option value="Turkey">Turkey</option>
    <option value="Turkmenistan">Turkmenistan</option>
    <option value="Turks and Caicos Islands">Turks and Caicos Islands
    </option>
    <option value="Tuvalu">Tuvalu</option>
    <option value="Uganda">Uganda</option>
    <option value="Ukraine">Ukraine</option>
    <option value="United Arab Emirates">United Arab Emirates</option>
    <option value="United Kingdom">United Kingdom</option>
    <option value="United States">United States</option>
    <option value="US Minor Outlying Islands">US Minor Outlying Islands
    </option>
    <option value="Uruguay">Uruguay</option>
    <option value="Uzbekistan">Uzbekistan</option>
    <option value="Vanuatu">Vanuatu</option>
    <option value="Venezuela">Venezuela</option>
    <option value="Viet Nam">Viet Nam</option>
    <option value="Virgin Islands, British">Virgin Islands, British</option>
    <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
    <option value="Wallis and Futuna">Wallis and Futuna</option>
    <option value="Western Sahara">Western Sahara</option>
    <option value="Yemen">Yemen</option>
    <option value="Zambia">Zambia</option>
    <option value="Zimbabwe">Zimbabwe</option>
</select>
<!--Modal For Editing Macanta Queries-->
<div class="modal macanta-query" id="macanta-query">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add/Edit Macanta Query</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form action="" method="post" role="form" id="queryForm" class="queryForm">
                <div class="modal-body">
                    <input type="hidden" class="queryId" name="queryId" value="">
                    <fieldset class="macanta-query-fieldset basic-information">
                        <legend class="label-lg">Basic Information</legend>
                        <div class="flex-row">
                            <div class="form-group">
                                <label class="label-sm" for="queryName">Name</label>
                                <input type="text" class="form-control queryName" name="queryName"
                                       placeholder="Enter Query Name.."  autocomplete="off" required>
                                <div class="checkbox">
                                    <label class="label-sm" for="queryDescription">Active?</label>
                                    <input name="queryStatus" type="checkbox" value="1" id="queryStatus" checked>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="label-sm" for="queryDescription">Description</label>
                                <textarea style="resize: vertical;" class="form-control queryDescription" name="queryDescription"
                                          rows="3"></textarea>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="macanta-query-fieldset connected-data-type">
                        <legend class="label-lg">Connected Data Type</legend>
                        <div class="form-group">
                            <select name="queryConnectedDataType" id="queryConnectedDataType" class="form-control queryConnectedDataType" required>
                            </select>
                        </div>
                    </fieldset>
                    <fieldset class="macanta-query-fieldset connected-data-field-set criteria">
                        <legend class="label-lg"><span class="chosen-connected-data-type"></span> Criteria</legend>
                        <div class="macanta-query-criteria-item">
                            <div class="macanta-query-criteria-item-link hide">
                                <div class="btn-group query-link-btn" role="group">
                                    <a class="nb-btn nb-base-color "></a>
                                </div>
                                <input type="hidden" name="queryCDFieldLogic[]" class="queryCDFieldLogic" value="">
                            </div>
                            <div class="flex-row">
                                <div class="form-group">
                                    <select name="queryCDFieldName[]"  class="form-control queryCDFieldName" required>
                                    </select>
                                </div>
                                <div class="form-group col-third">
                                    <select name="queryCDFieldOperator[]" class="form-control queryCDFieldOperator">
                                        <option value="">is</option>
                                        <option value="is not">is not</option>
                                        <option value="greater than">is greater than</option>
                                        <option value="less than">is less than</option>
                                        <!--<option value="between">is between</option>-->
                                        <option value="is null">is null</option>
                                        <option value="not null">is not null</option>
                                    </select>
                                </div>
                                <div class="form-group col-third">
                                    <select name="queryCDFieldValues[]" data-max-options="12" class="form-control dropdown-select queryCDFieldValuesMultiSelect" size="14" multiple="multiple">
                                    </select>
                                    <input type="text"  name="queryCDFieldValue[]" class="form-control queryCDFieldValue" placeholder="Enter Value"  autocomplete="off" required >

                                </div>
                                <div class="macanta-query-criteria-item-delete hide">
                                    <a class="nb-btn icon-btn nb-btn-icon-only" onclick="deleteCriteriaItem(this);"><i class="fa fa-trash-o"></i></a>
                                </div>
                            </div><!-- /.flex-row -->

                        </div>
                        <div class="macanta-query-criteria-item-add">
                            <div class="btn-group query-add-btn" role="group">
                                <a class="nb-btn nb-primary nb-btn-icon-only dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false"><i class="fa fa-plus-circle"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a class="addCriteriaItem-connected_data-or" onclick="addCriteriaItem('or',this,'connected_data');">Or</a></li>
                                    <li><a class="addCriteriaItem-connected_data-and" onclick="addCriteriaItem('and',this,'connected_data');">And</a></li>
                                </ul>
                            </div>
                        </div>

                    </fieldset>
                    <fieldset class="macanta-query-fieldset connected-contact-field-set contact-relationship-criteria">
                        <legend class="label-lg">Contact Relationship and Criteria</legend>
                        <div class="macanta-query-criteria-item">
                            <div class="macanta-query-criteria-item-link hide">
                                <div class="btn-group query-link-btn" role="group">
                                    <a class="nb-btn nb-base-color "></a>
                                </div>
                                <input type="hidden" name="queryContactRelationshipFieldLogic[]" class="queryContactRelationshipFieldLogic" value="">
                            </div>
                            <div class="flex-row">
                                <div class="form-group col-third">
                                    <select name="queryContactRelationship[]" class="form-control queryContactRelationship">
                                        <option value=""> -- any --</option>
                                    </select>
                                </div>
                                    <div class="macanta-query-criteria-item-delete hide">
                                        <a class="nb-btn icon-btn nb-btn-icon-only" onclick="deleteCriteriaItem(this);"><i class="fa fa-trash-o"></i></a>
                                    </div>
                               <!--  <div class="form-group col-third">
                                    <select name="queryContactRelationshipCondition[]" class="form-control queryContactRelationshipCondition">
                                        <optgroup label="Tag Criteria">
                                            <option value=""> -- any --</option>
                                            <option value="withAllTags">has ALL the tags</option>
                                            <option value="withoutAllTags">doesn't have ANY of the tags</option>
                                        </optgroup>
                                        <optgroup class="OptGroupCustomFields" label="Contact Custom Fields">

                                        </optgroup>
                                    </select>
                                </div> -->
                                <!-- <div class="tag-criteria-value-container hide col-third">
                                    <div class="form-group">
                                        <input type="text"  name="queryContactRelationshipTagsValue[]" class="form-control queryContactRelationshipTagsValue"  autocomplete="off" placeholder="Tag IDs Separated By Comma" >
                                    </div>
                                </div> -->
                                <!-- <div class="customfield-value-container hide col-third last">
                                    <div class="form-group">
                                        <select name="queryContactRelationshipOperator[]" class="form-control queryContactRelationshipOperator">
                                            <option value="">is</option>
                                            <option value="is not">is not</option>
                                            <option value="greater than">is greater than</option>
                                            <option value="less than">is less than</option>
                                            
                                            <option value="is null">is null</option>
                                            <option value="not null">is not null</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="text"  name="queryContactRelationshipConditionValue[]" class="form-control queryContactRelationshipConditionValue" placeholder="Enter Value" autocomplete="off" >
                                    </div>
                                    
                                    <div class="macanta-query-criteria-item-delete hide">
                                        <a class="nb-btn icon-btn nb-btn-icon-only" onclick="deleteCriteriaItem(this);"><i class="fa fa-trash-o"></i></a>
                                    </div>
                                    
                                    
                                </div> -->

                            </div><!-- flex-row -->
                        </div>
                        <div class="macanta-query-criteria-item-add">
                            <div class="btn-group query-add-btn" role="group">
                                <a class="nb-btn nb-primary nb-btn-icon-only dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false"><i class="fa fa-plus-circle"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a class="addCriteriaItem-connected_contact-or" onclick="addCriteriaItem('or',this,'connected_contact');">Or</a></li>
                                    <li><a class="addCriteriaItem-connected_contact-and" onclick="addCriteriaItem('and',this,'connected_contact');">And</a></li>
                                </ul>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="macanta-query-fieldset macanta-user-field-set user-relationship-criteria">
                        <legend class="label-lg">User Relationship and Criteria</legend>
                        <div class="macanta-query-criteria-item">
                            <div class="macanta-query-criteria-item-link hide">
                                <div class="btn-group query-link-btn" role="group">
                                    <a class="nb-btn nb-base-color "></a>
                                </div>
                                <input type="hidden" name="queryUserRelationshipFieldLogic[]" class="queryUserRelationshipFieldLogic" value="">
                            </div>
                            <div class="flex-row">
                                <div class="form-group col-third">
                                    <select name="queryUserRelationship[]"  class="form-control queryUserRelationship">
                                        <option value=""> -- any --</option>
                                    </select>
                                </div>
                                <div class="form-group col-third">
                                    <select name="queryUserOperator[]"  class="form-control queryUserOperator">
                                        <option value="">is</option>
                                        <option value="is not">is not</option>
                                    </select>
                                </div>
                                <div class="form-group col-third last">
                                    <select name="queryUserId[]" class="form-control queryUserId">
                                        <option value=""> -- Select One --</option>
                                    </select>
                                </div>
                                <div class="macanta-query-criteria-item-delete hide">
                                    <a class="nb-btn icon-btn nb-btn-icon-only" onclick="deleteCriteriaItem(this);"><i class="fa fa-trash-o"></i></a>
                                </div>
                            </div><!-- /.flex-row -->
                        </div>
                        <div class="macanta-query-criteria-item-add">
                            <div class="btn-group query-add-btn" role="group">
                                <a class="nb-btn nb-primary nb-btn-icon-only dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false"><i class="fa fa-plus-circle"></i></a>
                                <ul class="dropdown-menu">
                                    <li class="disabled"><a class="disabled" onclick="//addCriteriaItem('or',this);" disabled>Or</a></li>
                                    <li><a class="addCriteriaItem-macanta_user-and" onclick="addCriteriaItem('and',this,'macanta_user');">And</a></li>
                                </ul>
                            </div>
                        </div>

                    </fieldset>
                    <fieldset class="macanta-query-fieldset result-columns">
                        <legend class="label-lg">Result Columns</legend>
                        <div class="macanta-query-result-item">
                        </div>
                        <div class="flex-row">
                            <!-- <div class="">
                                <div class="col-sm-12">
                                    <b>Available Fields</b>
                                    <select name="queryColumns[]" id="queryColumnsMultiSelect"  data-live-search="true" data-max-options="12" class="form-control queryColumnsMultiSelect" size="14" multiple="multiple">
                                        <optgroup label="Contact Fields">
                                            <option value="FirstName">First Name</option>
                                            <option value="LastName">Last Name</option>
                                            <option value="Email">Email</option>
                                            <option value="Phone1">Phone No.</option>
                                            <option value="Company">Company</option>
                                            <option value="StreetAddress1">Street Address</option>
                                            <option value="City">City</option>
                                            <option value="Country">Country</option>
                                            <option value="PostalCode">Postal Code</option>
                                        </optgroup>
                                        <optgroup label="Connected Data Fields">
                                        </optgroup>

                                    </select>
                                </div>
                            </div> -->
                            <div class="available-fields">
                                <b class="label-lg">Available Fields</b>
                                <select name="from[]" id="multiselect" class="form-control multiselect" size="14" multiple="multiple">
                                    <optgroup label="Contact And Connected Data Fields">
                                        <option value="FirstName">First Name</option>
                                        <option value="LastName">Last Name</option>
                                        <option value="Email">Email</option>
                                        <option value="Phone1">Phone No.</option>
                                        <option value="Company">Company</option>
                                        <option value="StreetAddress1">Street Address</option>
                                        <option value="City">City</option>
                                        <option value="State">State</option>
                                        <option value="Country">Country</option>
                                        <option value="PostalCode">Postal Code</option>
                                    </optgroup>
                                    <!--<optgroup class="OptGroupFieldNames" label="Connected Data Fields">
                                    </optgroup>-->
                                </select>
                            </div>

                            <div class="result-column-btn-container">
                                <button type="button" id="multiselect_rightAll" class="nb-btn nb-primary"><i class="fa fa-forward"></i></button>
                                <button type="button" id="multiselect_rightSelected" class="nb-btn nb-primary"><i class="fa fa-chevron-right"></i></button>
                                <button type="button" id="multiselect_leftSelected" class="nb-btn nb-primary"><i class="fa fa-chevron-left"></i></button>
                                <button type="button" id="multiselect_leftAll" class="nb-btn nb-primary"><i class="fa fa-backward"></i></button>
                            </div>

                            <div class="chosen-columns flex-row">
                                <b class="label-lg">Chosen Columns</b>
                                <select name="to[]" id="multiselect_to" class="form-control" size="14" multiple="multiple">
                                </select>
                                <div class="btn-up-down-container flex-row">
                                    <button type="button" id="multiselect_move_up" class="nb-btn icon-btn"><i class="fa fa-arrow-up"></i></button>
                                    <button type="button" id="multiselect_move_down" class="nb-btn icon-btn "><i class="fa fa-arrow-down"></i></button>
                                </div>
                            </div>
                        </div>

                    </fieldset>

                </div>
                <div class="modal-footer">
                    <button type="button" class="nb-btn nb-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="nb-btn nb-primary">Save Query</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="macanta-query-criteria-item connecteddata-item-template">
    <div class="macanta-query-criteria-item-link hide">
        <div class="btn-group query-link-btn" role="group">
            <a class="nb-btn nb-base-color "></a>
        </div>
        <input type="hidden" name="queryCDFieldLogic[]" class="queryCDFieldLogic" value="">
    </div>
    <div class="flex-row">
        <div class="form-group col-third">
            <select name="queryCDFieldName[]"  class="form-control queryCDFieldName" required>
            </select>
        </div>
        <div class="form-group col-third">
            <select name="queryCDFieldOperator[]" class="form-control queryCDFieldOperator">
                <option value="">is</option>
                <option value="is not">is not</option>
                <option value="greater than">is greater than</option>
                <option value="less than">is less than</option>
                <!--<option value="between">is between</option>-->
                <option value="is null">is null</option>
                <option value="not null">is not null</option>
                <option value="cd_amended">Amended</option>
            </select>
        </div>
        <div class="form-group cd-criteria-queryCDFieldValues col-third last">
            <input type="text"  name="queryCDFieldValue[]" class="form-control queryCDFieldValue" placeholder="Enter Value"  autocomplete="off" required >            
        </div>
        <div class="macanta-query-criteria-item-delete hide">
            <a class="nb-btn icon-btn nb-btn-icon-only" onclick="deleteCriteriaItem(this);"><i class="fa fa-trash-o"></i></a>
        </div>
    </div><!-- /.flex-row -->

</div>
<div class="macanta-query-criteria-item connecteddata-item-templateB">
    <div class="macanta-query-criteria-item-link hide">
        <div class="btn-group query-link-btn" role="group">
            <a class="nb-btn nb-base-color "></a>
        </div>
        <input type="hidden" name="queryCDFieldLogic[]" class="queryCDFieldLogic" value="">
    </div>
    <div class="flex-row">
        <div class="form-group col-half">
            <select name="queryCDFieldName[]"  class="form-control queryCDFieldName" required>
            </select>
        </div>
        <div class="form-group cd-criteria-queryCDFieldValues col-half last">
            <input type="text"  name="queryCDFieldValue[]" class="form-control queryCDFieldValue" placeholder=""  autocomplete="off" >
        </div>
        <div class="macanta-query-criteria-item-delete hide">
            <a class="btn btn-danger" onclick="deleteCriteriaItem(this);"><i class="fa fa-trash-o"></i></a>
        </div>
    </div><!-- flex-row -->
</div>
<div class="macanta-query-criteria-item connecteddata-item-templateD">
    <div class="macanta-query-criteria-item-link hide">
        <div class="btn-group query-link-btn" role="group">
            <a class="nb-btn nb-base-color "></a>
        </div>
        <input type="hidden" name="queryCDFieldLogic[]" class="queryCDFieldLogic" value="">
    </div>
    <div class="flex-row">
        <div class="form-group col-half">
            <select name="queryCDFieldName[]"  class="form-control queryCDFieldName" data-required="no" required>
            </select>
        </div>
        <div class="form-group cd-criteria-queryCDFieldValues col-half last">
            <input type="text"  name="queryCDFieldValue[]" class="form-control queryCDFieldValue" placeholder=""  autocomplete="off" >            
        </div>
        <div class="macanta-query-criteria-item-delete hide">
            <a class="btn btn-danger" onclick="deleteCriteriaItem(this);"><i class="fa fa-trash-o"></i></a>
        </div>
    </div><!-- /.flex-row -->

</div>
<div class="macanta-query-criteria-item custom connecteddata-item-templateC">
    <div class="macanta-query-criteria-item-link hide">
        <div class="btn-group query-link-btn" role="group">
            <a class="nb-btn nb-base-color "></a>
        </div>
        <input type="hidden" name="queryCDFieldLogic[]" class="queryCDFieldLogic" value="">
    </div>
    <div class="flex-row">
        <div class="form-group col-half">
            <input type="text"  name="queryCDFieldName[]" class="form-control queryCDFieldName" placeholder="Enter Key Name"  autocomplete="off" required >
        </div>
        <div class="form-group cd-criteria-queryCDFieldValues col-half last">
            <input type="text"  name="queryCDFieldValue[]" class="form-control queryCDFieldValue" placeholder="Enter Value"  autocomplete="off" required >
        </div>
        <div class="macanta-query-criteria-item-delete hide">
            <a class="btn btn-danger" onclick="deleteCriteriaItem(this);"><i class="fa fa-trash-o"></i></a>
        </div>

        
    </div><!-- /.flex-row -->
</div>

<div id="AutomationGroupsContentTemplate" class="DynamicTabContent"
     style="display: none;">
<small class="dev-only php">frontpage.php</small>        
    <div class="">
        <form action="" method="post" role="form"  class="queryForm">
            <input type="hidden" class="queryId" name="queryId" value="">
            <input type="hidden" class="queryType" name="queryType" value="">
            
            <div class="field-group-container">
            
                <div class="form-group-container">    
                    
                    <div class="form-group ">
                        <label class="label-sm control-label " for="TabTitleName">
                            <span class="TabTitleLabel"></span> <span class="asteriskField"> * </span> <span><small> i.e. Real State Automation</small></span>
                        </label>
                        <input class="form-control TabTitleInput" name="TabTitleName"
                               placeholder="" type="text" required/>
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        <div class="flex-row">
                            <label class="control-label Group-Status label-sm" for="GroupStatus">
                                Group Status
                            </label>
                            <div data-state="active" class="GroupStatus toggle-iphone"></div>
                        </div><!-- /.flex-row -->
                        <small class="GroupStatusNote">You may Turn on or off all the automation in this Group</small>
                        <input type="hidden" name="GroupStatus" value="active">
                    </div><!-- /.form-group -->
                    
                </div><!-- /.from-group-container -->

                <div class="form-group-container">
                    <div class="form-group">
                        <label class="label-sm" for="queryDescription">Description</label>
                        <textarea style="resize: vertical;" class="form-control queryDescription" name="queryDescription" rows="3"
                                  placeholder="Tell me about this Automation Group.."
                        ></textarea>
                    </div><!-- /.form-group -->
                </div><!-- /.from-group-container -->
                
            </div><!-- /.field-group-container -->
            
            <div class="">
                <div class="table-responsive">
                    <table class="table table-hover AutomationGroupsTable" width="100%;" style="width: 100%;">
                        <small class="dev-only js">tab_admin_automation.php - Line 480</small>
                        <thead>
                        <tr>
                            <th>
                                <h3 class="label-lg">If</h3>
                                <small>Choose your Trigger Condition.</small>
                            </th>
                            <th>
                                <h3 class="label-lg">Wait</h3>
                                <small>Choose your Wait Action.</small>
                            </th>
                            <th>
                                <h3 class="label-lg">Then</h3>
                                <small>Choose your Trigger Action.</small>
                            </th>
                            <th class="col-status">
                                <h3 class="label-lg">Status</h3>
                                <small>Turn Automation On/Off.</small>
                            </th>
<!--
                            <th>

                            </th>
-->
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="AutomationGroupsItem">
                            <td>
                                <select name="IfCondition[]" class="form-control IfCondition" >
                                    <option value="" title="Please Select Trigger Condition"> -- Select One --</option>
                                </select>
                            </td>
                            <td>
                                <input type="text" value="" name="ThenWait[]" class="form-control ThenWait">
                            </td>
                            <td>
                                <select name="ThenAction[]" class="form-control ThenAction" >
                                    <option value=""> -- Select One --</option>
                                </select>
                            </td>
                            <td>
                                <div data-state="inactive" class="ItemStatus toggle-iphone"></div>
                                <input type="hidden" name="IfThenStatus[]" value="inactive" class="IfThenStatus">
                            </td>
                            <td>
                                <i class="fa fa-plus-square-o AddItem" aria-hidden="true"></i>
                                <i class="fa fa-trash-o DeleteItem" aria-hidden="true"></i>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="save-btn-container">
                <button type="submit" class="nb-btn nb-primary icon-btn saveDynamicTab"><i class="fa fa-save"></i> Save <span
                            class="TabButtonLabel"></span></button>
            </div>
        </form>
    </div>

</div>
<!-- Modal -->
<div id="ThenWaitModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-group">
                    <label>Wait For : (Amount)</label>
                    <input type="text" name="WaitAmount" value="" class="WaitAmount form-control"  class="form-control col-md-6" />
                    <div id="WaitAmt_error"></div>
                </div>
                <div class="form-group">
                    <label>Wait For : (Unit)</label>
                    <select name="WaitUnit" class="WaitUnit form-control" class="form-control col-md-6">
                        <option value="0">Please select</option>
                        <option value="None">None</option>
                        <option value="Minutes">Minutes</option>
                        <option value="Hours">Hours</option>
                        <option value="Days">Days</option>
                        <option value="Weeks">Weeks</option>
                        <option value="Months">Months</option>
                    </select>
                    <div id="WaitUnit_error"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="nb-btn nb-primary icon-btn wait_condition" id="wait_condition" name="wait_condition"><i class="fa fa-save"></i> Save Wait Time <span></button>
            </div>
        </div>
    </div>
</div>
<div id="TriggerConditionsContentTemplate" class="DynamicTabContent"
     style="display: none;">

        <form action="" method="post" role="form"  class="queryForm ">
            <input type="hidden" class="queryId" name="queryId" value="">
            <input type="hidden" class="queryType" name="queryType" value="">
            <fieldset class="macanta-query-fieldset">
                <legend class="label-lg">General Info</legend>

                <div class="flex-row">
                    <div class="form-group col-half">
                        <label class="label-sm" for="queryName"><span class="TabTitleLabel">Trigger Condition Label</span></label>
                        <input type="text" class="form-control TabTitleInput" name="TabTitleName"
                               placeholder="Type Trigger Condition Label" autocomplete="off" required>
    
                        <label class="label-sm control-label " for="ConnectedData">
                            <span>Connected Data:</span>
                        </label>
                        <select name="queryConnectedDataType" class="form-control queryConnectedDataType" required>
                        </select>
    
                    </div>
                    <div class="form-group col-half">
                        <label class="label-sm control-label " for="ConnectedData">
                            <span>Restart After:</span>
                        </label>
                        <input type="text" class="form-control queryRestartAfter" name="queryRestartAfter"
                               placeholder="e.g. 1 day or 1 hour or never, dafault to never" autocomplete="off" value="never">
                        <label class="label-sm" for="queryDescription">Description</label>
                        <textarea style="resize: vertical;" class="form-control queryDescription" name="queryDescription"
                                    rows="3"></textarea>
                    </div>
                </div><!-- /.flex-row -->
            </fieldset>
            <fieldset class="macanta-query-fieldset connected-contact-field-set">
                <legend class="label-lg">Contact Relationship and Field Conditions</legend>

                    <div class="macanta-query-criteria-item">
                        <div class="macanta-query-criteria-item-link hide">
                            <div class="btn-group query-link-btn" role="group">
                                <a class="nb-btn nb-base-color"></a>
                            </div>
                            <input type="hidden" name="queryContactRelationshipFieldLogic[]"
                                   class="queryContactRelationshipFieldLogic" value="">
                        </div>
                        <div class="flex-row">
                            <div class="form-group col-third">
                                <select name="queryContactRelationship[]" class="form-control queryContactRelationship">
                                    <option value=""> -- any --</option>
                                </select>
                            </div>
                            <!-- <div class="form-group col-third">
                                <select name="queryContactRelationshipCondition[]"
                                        class="form-control queryContactRelationshipCondition">
                                    <optgroup label="Tag Criteria">
                                        <option value=""> -- any --</option>
                                        q
                                        <option value="withAllTags">has ALL the tags</option>
                                        <option value="withoutAllTags">doesn't have ANY of the tags</option>
                                    </optgroup>
                                    <optgroup class="OptGroupCustomFields" label="Contact Custom Fields">
    
                                    </optgroup>
                                </select>
                            </div> -->
                            <!-- <div class="tag-criteria-value-container hide col-third last">
                                <div class="form-group">
                                    <input type="text" name="queryContactRelationshipTagsValue[]"
                                           class="form-control queryContactRelationshipTagsValue" autocomplete="off"
                                           placeholder="Tag IDs Separated By Comma">
                                </div>
                            </div> -->
                            <!-- <div class="customfield-value-container hide col-third">
                                <div class="form-group">
                                    <select name="queryContactRelationshipOperator[]"
                                            class="form-control queryContactRelationshipOperator">
                                        <option value="">is</option>
                                        <option value="is not">is not</option>
                                        <option value="greater than">is greater than</option>
                                        <option value="less than">is less than</option>
                                        
                                        <option value="is null">is null</option>
                                        <option value="not null">is not null</option>
    
                                    </select>
                                </div>
                                <div class="form-group col-third last">
                                    <input type="text" name="queryContactRelationshipConditionValue[]"
                                           class="form-control queryContactRelationshipConditionValue"
                                           placeholder="Enter Value" autocomplete="off">
                                </div>
                                
                            </div> -->
                                <div class="macanta-query-criteria-item-delete hide">
                                    <a class="btn btn-danger" onclick="deleteCriteriaItem(this);"><i class="fa fa-trash-o"></i></a>
                                </div>
    
                        </div><!-- /.flex-row -->

                    </div>
<!-- <small class="dev-only php">frontpage.php</small> -->
                <div class="macanta-query-criteria-item-add">
                    <div class="btn-group query-add-btn" role="group">
                        <a class="nb-btn nb-btn-icon-only dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false"><i class="fa fa-plus-circle"></i></a>
                        <ul class="dropdown-menu">
                            <li><a class="addCriteriaItem-connected_contact-or"
                                   onclick="addCriteriaItem('or',this,'connected_contact');">Or</a></li>
                            <li><a class="addCriteriaItem-connected_contact-and"
                                   onclick="addCriteriaItem('and',this,'connected_contact');">And</a></li>
                        </ul>
                    </div>
                </div>
            </fieldset>
            <fieldset class="macanta-query-fieldset connected-data-field-set">
                <legend class="label-lg"><span class="chosen-connected-data-type"></span> Connected Data Field Conditions</legend>
                <div class="macanta-query-criteria-item">
                    <div class="macanta-query-criteria-item-link hide">
<small class="dev-only php">frontpage.php</small>
                        <div class="btn-group query-link-btn" role="group">
                            <a class="nb-btn nb-base-color "></a>
                        </div>
                        <input type="hidden" name="queryCDFieldLogic[]" class="queryCDFieldLogic" value="">
                    </div>
                    <div class="flex-row">
                        <div class="form-group col-third">
                            <select name="queryCDFieldName[]" class="form-control queryCDFieldName" required>
                                <option value=""> -- any --</option>
                            </select>
                        </div>
                        <div class="form-group col-third">
                            <select name="queryCDFieldOperator[]" class="form-control queryCDFieldOperator">
                                <option value="">is</option>
                                <option value="is not">is not</option>
                                <option value="greater than">is greater than</option>
                                <option value="less than">is less than</option>
                                <!--<option value="between">is between</option>-->
                                <option value="is null">is null</option>
                                <option value="not null">is not null</option>
                                <option value="cd_amended">Amended</option>
                            </select>
                        </div>
                        <div class="form-group col-third last">
                            <select name="queryCDFieldValues[]" data-max-options="12"
                                    class="form-control dropdown-select queryCDFieldValuesMultiSelect" size="14" multiple="multiple">
                            </select>
                            <input type="text" name="queryCDFieldValue[]" class="form-control queryCDFieldValue"
                                   placeholder="Enter Value" autocomplete="off" required>

                        </div>
                        <div class="macanta-query-criteria-item-delete hide">
                            <a class="btn btn-danger" onclick="deleteCriteriaItem(this);"><i class="fa fa-trash-o"></i></a>
                        </div>
                    </div><!-- /.flex-row -->
                </div>
                <div class="macanta-query-criteria-item-add">
                    <div class="btn-group query-add-btn" role="group">
                        <a class="nb-btn nb-btn-icon-only dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false"><i class="fa fa-plus-circle"></i></a>
                        <ul class="dropdown-menu">
                            <li><a class="addCriteriaItem-connected_data-or"
                                   onclick="addCriteriaItem('or',this,'connected_data');">Or</a></li>
                            <li><a class="addCriteriaItem-connected_data-and"
                                   onclick="addCriteriaItem('and',this,'connected_data');">And</a></li>
                        </ul>
                    </div>
                </div>

            </fieldset>

            <div class="save-btn-container">
                <button type="submit" class="nb-btn nb-primary icon-btn saveDynamicTab"><i class="fa fa-save"></i> Save
                    <span class="TabButtonLabel"></span></button>
            </div>
        </form>

</div>
<div id="TriggerActionsContentTemplate" class="DynamicTabContent" style="display: none;">

        <form action="" method="post" role="form"  class="queryForm">
            <input type="hidden" class="queryId"   name="queryId" value="">
            <input type="hidden" class="queryType" name="queryType" value="">
            <fieldset class="macanta-query-fieldset">
                <legend class="label-lg">General Info</legend>
                <div class="flex-row">
                    <div class="form-group col-half">
                        <label class="label-sm" for="queryName"><span class="TabTitleLabel">Trigger Condition Label</span></label>
                        <input type="text" class="form-control TabTitleInput" name="TabTitleName"
                               placeholder="Type Trigger Condition Label" autocomplete="off" required>
    
                        <label class="label-sm control-label " for="ConnectedData">
                            <span>Connected Data</span>
                        </label>
                        <select name="queryConnectedDataType" class="form-control queryConnectedDataType" required>
                        </select>

                        <label class="label-sm control-label " for="ConnectedData">
                            <span>Assign Email to be sent after this action</span>
                        </label>
                        <select name="queryConnectedDataEmail" class="form-control queryConnectedDataEmail">
                        </select>

                        <label class="label-sm control-label " for="ConnectedData">
                            <span>Assign Field Action</span>
                        </label>
                        <select name="queryConnectedDataFieldAction" class="form-control queryConnectedDataFieldAction">
                        </select>

                    </div>
                    <div class="form-group col-half">
                        <label class="label-sm control-label " for="ConnectedData">
                            <span>Assign HTTP Post to run after this action</span>
                        </label>
                        <select name="queryConnectedDataHTTPPost" class="form-control queryConnectedDataHTTPPost">
                        </select>
                        <label class="label-sm" for="queryDescription">Description</label>
                        <textarea style="resize: vertical;" class="form-control queryDescription" name="queryDescription"
                                  rows="3"></textarea>
                    </div>
                </div><!-- /.flex-row -->
            </fieldset>
            <fieldset class="macanta-query-fieldset connected-contact-field-set">
                <legend class="label-lg">Contact Relationship and Field Conditions</legend>
                
                    <div class="macanta-query-criteria-item">
                        <div class="macanta-query-criteria-item-link hide">
                            <div class="btn-group query-link-btn" role="group">
                                <a class="nb-btn nb-base-color "></a>
                            </div>
                            <input type="hidden" name="queryContactRelationshipFieldLogic[]"
                                   class="queryContactRelationshipFieldLogic" value="">
                        </div>
                        <div class="flex-row">
                            <div class="form-group col-third">
                                <select name="queryContactRelationship[]" class="form-control queryContactRelationship">
                                </select>
                            </div>
                            <!-- <div class="form-group col-third">
                                <select name="queryContactRelationshipCondition[]"
                                        class="form-control queryContactRelationshipCondition">
                                    <optgroup label="Tag Criteria">
                                        <option value=""> -- any --</option>
                                        <option value="withAllTags">has ALL the tags</option>
                                        <option value="withoutAllTags">doesn't have ANY of the tags</option>
                                    </optgroup>
                                    <optgroup class="OptGroupCustomFields" label="Contact Custom Fields">
    
                                    </optgroup>
                                </select>
                            </div>
                            <div class="tag-criteria-value-container hide">
                                <div class="form-group">
                                    <input type="text" name="queryContactRelationshipTagsValue[]"
                                           class="form-control queryContactRelationshipTagsValue" autocomplete="off"
                                           placeholder="Tag IDs Separated By Comma">
                                </div>
                            </div> -->
                            <!-- <div class="customfield-value-container hide">
                                <div class="form-group">
                                    <select name="queryContactRelationshipOperator[]"
                                            class="form-control queryContactRelationshipOperator">
                                        <option value="">is</option>
                                        <option value="is not">is not</option>
                                        <option value="greater than">is greater than</option>
                                        <option value="less than">is less than</option>
                                        <option value="between">is between</option>
                                        <option value="is null">is null</option>
                                        <option value="not null">is not null</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="queryContactRelationshipConditionValue[]"
                                           class="form-control queryContactRelationshipConditionValue"
                                           placeholder="Enter Value" autocomplete="off">
                                </div>
                            </div> -->
                            <div class="macanta-query-criteria-item-delete hide">
                                <a class="btn btn-danger" onclick="deleteCriteriaItem(this);"><i class="fa fa-trash-o"></i></a>
                            </div>  
                        </div><!-- /.flex-row -->
                    </div>
                    <div class="macanta-query-criteria-item-add">
                        <div class="btn-group query-add-btn" role="group">
                            <a class="nb-btn nb-btn-icon-only dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="false"><i class="fa fa-plus-circle"></i></a>
                            <ul class="dropdown-menu">
                                <li><a class="addCriteriaItem-connected_contact-or"
                                       onclick="addCriteriaItem('or',this,'connected_contact');">Or</a></li>
                                <li><a class="addCriteriaItem-connected_contact-and"
                                       onclick="addCriteriaItem('and',this,'connected_contact');">And</a></li>
                            </ul>
                        </div>
                    </div>
            </fieldset>

            <fieldset class="macanta-query-fieldset connected-data-field-set field-values">
                <legend class="label-lg"><span class="chosen-connected-data-type"></span> Field Values</legend>
                <div class="macanta-query-criteria-item">

                </div>
                <div class="macanta-query-criteria-item-add">
                    <div class="btn-group query-add-btn" role="group">
                        <a class="nb-btn nb-btn-icon-only  addCriteriaItem-connected_data" onclick="addCriteriaItem('',this,'connected_data',true,null,false,false);"><i class="fa fa-plus-circle"></i></a>
                    </div>
                </div>

            </fieldset>

            <div class="save-btn-container">
                <button type="submit" class="nb-btn nb-primary icon-btn saveDynamicTab"><i class="fa fa-save"></i> Save
                    <span class="TabButtonLabel"></span></button>
            </div>
        </form>



</div>
<div id="ActionWebhooksContentTemplate" class="DynamicTabContent" style="display: none;">

        <form action="" method="post" role="form" class="queryForm">
            <input type="hidden" class="queryId"   name="queryId" value="">
            <input type="hidden" class="queryType" name="queryType" value="">
            <fieldset class="macanta-query-fieldset">
                <legend class="label-lg">General Info</legend>
                <div class="flex-row">
                    <div class="form-group col-half">
                        <label class="label-sm" for="queryName"><span class="TabTitleLabel">Trigger Condition Label</span></label>
                        <input type="text" class="form-control TabTitleInput" name="TabTitleName"
                               placeholder="Type Trigger Condition Label" autocomplete="off" required>
                    </div>
                    <div class="form-group col-half">
                        <label class="label-sm" for="queryDescription">Description</label>
                        <textarea style="resize: vertical;" class="form-control queryDescription" name="queryDescription"
                                  rows="3"></textarea>
                    </div>
                </div><!-- /.flex-row -->
                <div class="flex-row">
                    <div class="form-group col-full overlap-top">
                        <label class="label-sm" for="queryPostURL"><span>HTTP POST URL</span></label>
                        <input type="text" class="form-control queryPostURL" name="queryPostURL"
                               placeholder="Enter HTTP Post URL" autocomplete="off" required>
                    </div>
                </div><!-- /.flex-row -->
                <div class="flex-row">
                    <div class="form-group col-half">
                        <label class="label-sm control-label " for="ConnectedData">
                            <span>Connected Data:</span>
                        </label>
                        <select name="queryConnectedDataType" class="form-control queryConnectedDataType" required>
                        </select>
                    </div>
                    <div class="form-group col-half">
                        <label class="label-sm control-label " for="ConnectedData">
                            <span>Contact Relationship</span>
                        </label>
                        <select name="queryContactRelationship[]" class="form-control queryContactRelationship">
                        </select>
                    </div>
                </div><!-- /.flex-row -->
            </fieldset>
            <fieldset class="macanta-query-fieldset connected-data-field-set field-values">
                <legend class="label-lg"><span class="chosen-connected-data-type"></span> Field Values</legend>
                <div class="macanta-query-criteria-item">
                    <div class="macanta-query-criteria-item-link hide">
                        <div class="btn-group query-link-btn" role="group">
                            <a class="nb-btn nb-base-color "></a>
                        </div>
                        <input type="hidden" name="queryCDFieldLogic[]" class="queryCDFieldLogic" value="">
                    </div>

                    <div class="flex-row">
                        <div class="form-group col-half">
                            <select name="queryCDFieldName[]" class="form-control queryCDFieldName" data-required="no">

                            </select>
                        </div>
                        <div class="form-group col-half">
                            <select name="queryCDFieldValues[]" data-max-options="12" data-required="no"
                                    class="form-control dropdown-select queryCDFieldValuesMultiSelect" size="14" multiple="multiple">
                            </select>
                            <input type="text" name="queryCDFieldValue[]" class="form-control queryCDFieldValue"
                                   placeholder="Leave blank to use current value" autocomplete="off">
                        </div>
                        <div class="macanta-query-criteria-item-delete hide">
                            <a class="btn btn-danger" onclick="deleteCriteriaItem(this);"><i class="fa fa-trash-o"></i></a>
                        </div>
                    </div><!-- /.flex-row -->
                </div>
                <div class="macanta-query-criteria-item-add">
                    <div class="btn-group query-add-btn" role="group">
                        <a class="nb-btn nb-primary  addCriteriaItem-connected_data-webhook" onclick="addCriteriaItem('webhook',this,'connected_data',true,'Leave blank to use current value',false);"><i class="fa fa-plus-circle"></i></a>
                    </div>
                </div>

            </fieldset>
            <fieldset class="macanta-query-fieldset connected-data-field-set">
                <legend class="label-lg">Custom Key/Value Pairs</legend>
                <div class="macanta-query-criteria-item custom">


                </div>
                <div class="macanta-query-criteria-item-add custom">
                    <div class="btn-group query-add-btn" role="group">
                        <a class="nb-btn nb-primary icon-btn addCriteriaItem-connected_data-custom" onclick="addCriteriaItem('custom',this,'connected_data',true,'Enter Value');"><i class="fa fa-plus-circle"></i></a>
                    </div>
                </div>

            </fieldset>
            <div class="save-btn-container">
                <button type="submit" class="nb-btn nb-primary icon-btn saveDynamicTab"><i class="fa fa-save"></i> Save
                    <span class="TabButtonLabel"></span></button>
            </div>
        </form>


</div>
<div id="EmailActionsContentTemplate" class="DynamicTabContent" style="display: none;">

    <form action="" method="post" role="form" class="queryForm email-action">
        <input type="hidden" class="queryId"   name="queryId" value="">
        <input type="hidden" class="queryType" name="queryType" value="">
        <fieldset class="macanta-query-fieldset">
            <legend class="label-lg">General Info</legend>
            <div class="flex-row">
                <div class="form-group col-half overlap-top">
                    <label class="label-sm" for="queryName"><span class="TabTitleLabel">Email Action Label</span></label>
                    <input type="text" class="form-control TabTitleInput" name="TabTitleName"
                           placeholder="Type Trigger Condition Label" autocomplete="off" required>
                    <label class="label-sm" for="queryFromAddress"><span>From Address</span></label>
                    <input type="email" class="form-control queryFromAddress" name="queryFromAddress"
                           placeholder="Enter From Email Address" autocomplete="off" required>
                    <label class="label-sm" for="queryFromName"><span>From Name</span></label>
                    <input type="text" class="form-control queryFromName" name="queryFromName"
                           placeholder="Enter From Name" autocomplete="off" >

                    <label class="label-sm" for="querySubject"><span>Subject Line</span></label>
                    <input type="text" class="form-control querySubject" name="querySubject"
                           placeholder="Enter Subject" autocomplete="off" required>
                </div>
                <div class="form-group col-half overlap-top">
                    <label class="label-sm" for="queryDescription">Description</label>
                    <textarea style="resize: vertical;" class="form-control queryDescription" name="queryDescription"
                              rows="3"></textarea>

                    <label class="label-sm" for="queryPreviewText"><span>Preview Text</span></label>
                    <textarea  class="form-control queryPreviewText" name="queryPreviewText"
                               placeholder="Enter Preview Text"></textarea>
                </div>
            </div>
            <div class="flex-row">
                <div class="form-group col-full email-preview">
                    <div class="flex-row label-test-email-container">
                        <label class="label-sm control-label col-third" for="ConnectedData">
                            <span>Email Preview</span>
                        </label>
                        <div class="sendTestEmail col-2-thirds" data-emailid="">
                            <span class="subSendTestEmail label-sm">Send Test Email To Contact Id: <input type="text"  class="sendTestEmailValue " placeholder="123456|item_1234abc">
                                <button  type="button"   class="sendTestEmailButton nb-btn nb-primary icon-btn"><i class="fa fa-paper-plane"></i>Send</button>
                            </span>
                            <div class="TemplateNameInfo"></div>
                        </div>
                    </div>

                    <iframe class="previewEmail form-control" data-emailid=""  src="" >

                    </iframe>
                </div>
            </div>
            <div class="flex-row">
                <div class="form-group col-full">

                    <button type="button" class="nb-btn nb-primary icon-btn editEmailButton " data-emilid=""
                            onclick="showStripoModal(this);"><i class="fa fa-envelope-open"></i>Edit Email Content
                    </button>
                </div>
            </div><!-- /.flex-row -->
        </fieldset>
        <fieldset class="collapsible  collapsed ">
            <legend>Previously Used Templates</legend>
            <div class="flex-row RecentlyUsedTemplateContainer">
                <table class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th class="label-lg">Template Name</th>
                        <th class="label-lg">Version</th>
                        <th class="label-lg">Last Updated</th>
                        <th class="label-lg">Created Date</th>
                        <th class="label-lg">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </fieldset>
        <fieldset class="collapsible  collapsed ">
            <legend>Built-in Templates</legend>
            <div class="template-thumb-container" style="width: 100%">

                <?php
                $BuiltInTemplates = macanta_get_email_templates();
                $CounterA=0;
                foreach ($BuiltInTemplates as $BuiltInTemplate) {
                    ?>
                    <div class="template-thumb" data-emailid="<?php echo $BuiltInTemplate->EmailId; ?>"
                         style="background-image: url(<?php echo  $this->config->item('base_url')."assets/img/email-templates/".$BuiltInTemplate->EmailId.".png"; ?>);">
                            <span class="template-options">
                                <div class="template-thumb-btn-container">
                                    <button type="button" class="nb-btn nb-primary icon-btn show-preview"
                                            onclick="showEmailPreviewModal(this);"
                                            data-emailid="<?php echo $BuiltInTemplate->EmailId; ?>"
                                            data-templatename="<?php echo $BuiltInTemplate->Title; ?>"
                                            data-version="<?php echo $BuiltInTemplate->Version; ?>"
                                    >
                                        Preview
                                    </button>
                                    <button type="button" class="nb-btn nb-primary icon-btn choose-template"
                                            onclick="useEmailTempalte(this);"
                                            data-templatename="<?php echo $BuiltInTemplate->Title; ?>"
                                            data-emailid="<?php echo $BuiltInTemplate->EmailId; ?>"
                                            data-version="<?php echo $BuiltInTemplate->Version; ?>"
                                    >
                                        Choose Template
                                    </button>
                                </div>
                               <span class="template-title"><?php echo $BuiltInTemplate->Title ; ?></span>
                            </span>

                    </div>

                    <?php
                }
                ?>
            </div>
        </fieldset>
        <div class="save-btn-container">
            <button type="submit" class="nb-btn nb-primary icon-btn saveDynamicTab"><i class="fa fa-save"></i> Save
                <span class="TabButtonLabel"></span></button>
        </div>
    </form>
</div>
<!-- Field Action form -->
<div id="FieldActionsContentTemplate" class="DynamicTabContent" style="display: none;">

        <form action="" method="post" role="form" class="queryForm crud">
            <input type="hidden" class="queryId" name="queryId" value="">
            <input type="hidden" class="queryType" name="queryType" value="">
            <fieldset class="macanta-query-fieldset">
                <legend class="label-lg">General Info</legend>
                <div class="flex-row">
                    <div class="form-group col-half">
                        <label class="label-sm" for="queryName"><span class="TabTitleLabel">Field Action Label</span></label>
                        <input type="text" class="form-control TabTitleInput" name="TabTitleName"
                               placeholder="Type Field Action Label" autocomplete="off" required>

                        <label class="label-sm control-label " for="ConnectedData">
                            <span>Connected Data:</span>
                        </label>
                        <select name="queryConnectedDataType" class="form-control queryConnectedDataType" required>
                        </select>
                    </div>
                    <div class="form-group col-half">
                        <label class="label-sm" for="queryDescription">Description</label>
                        <textarea style="resize: vertical;" class="form-control queryDescription" name="queryDescription"
                                  rows="3"></textarea>
                    </div>
                </div><!-- /.flex-row -->
                <div class="flex-row">
                    <div class="form-group col-half">
                        <label class="label-sm" for="queryPostURL"><span>Action Type</span></label>
                        <select type="text" class="form-control action_type" name="actionType" id="action_type" required>
                            <option value="">Select Action Type</option>
                            <option value="Addition">Addition</option>
                            <option value="Substraction">Substraction</option>
                            <option value="Multiplication">Multiplication</option>
                            <option value="Division">Division</option>
                            <option value="TicketNumber">Ticket Number</option>
                            <option value="ActionCounter">Action Counter</option>
                            <option value="HOWOLD/HOWLONG">How Old / How Long</option>
                        </select>
                    </div>
                    <div class="form-group col-half">
                        <label class="label-sm control-label " for="ConnectedData">
                            <span>Contact Relationship</span>
                        </label>
                        <select name="queryContactRelationship" class="form-control queryContactRelationship" required>
                        </select>
                    </div>
                </div><!-- /.flex-row -->
            </fieldset>
            <div class="datachange Connected">
                <fieldset class="macanta-query-fieldset connected-data-field-set field-values FieldsData FieldActionFieldSet">
                <legend class="label-lg"><span class="chosen-connected-data-type"></span> Field Values</legend>
                <div class="macanta-query-criteria-item">
                    <div class="macanta-query-criteria-item-link hide">
                        <div class="btn-group query-link-btn" role="group">
                            <a class="nb-btn nb-base-color "></a>
                        </div>
                    </div>

                    <div class="flex-row">
                        <div class="form-group col-half">
                            <label class="label-sm control-label " for="ConnectedData">
                            <span>Field 1:</span>
                            <select name="queryCDFieldName1" class="form-control queryCDFieldName queryCDFieldName1" data-required="no" required>

                            </select>
                        </div>
                        <div class="form-group col-half">
                            <label class="label-sm control-label " for="ConnectedData">
                            <span>Field 2:</span>
                            <select name="queryCDFieldName2" class="form-control queryCDFieldName queryCDFieldName2" data-required="no"  required>

                            </select>
                        </div>
                        <div class="form-group col-half">
                            <label class="label-sm control-label " for="ConnectedData">
                            <span>Result Field:</span>
                            <select name="queryCDFieldNameResult" class="form-control queryCDFieldName queryCDFieldNameResult" data-required="no" required>

                            </select>
                        </div>
                    </div><!-- /.flex-row -->
                </div>
               </fieldset>
            </div>
            <div class="save-btn-container">
                <button type="submit" class="nb-btn nb-primary icon-btn saveDynamicTab"><i class="fa fa-save"></i> Save
                    <span class="TabButtonLabel"></span></button>
            </div>
        </form>
</div>
<!-- Field Action form -->
<!-- StripoPluginModal -->
<div id="StripoPluginModal" data-emailid="" class="modal fade StripoPluginModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <form action="" method="post" role="form" class="StripoPluginModalForm">
                <div class="modal-header">
                    <h4 class="modal-title">Email Builder</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </div>
                <div class="modal-body">
                    <div class="StripoModalBody">
                        <div class="flex-row">
                            <div class="form-group col-half">
                                <label class="label-sm" for="TemplateName"><span
                                            class="TabTitleLabel">Template Name: </span></label>
                                <input type="text" class="form-control TemplateName" name="TemplateName"
                                       placeholder="Type TemplateName" autocomplete="off" required>
                            </div>
                            <div class="form-group col-half">
                                <div id="externalSystemContainer" class="form-group">
                                    <!--This is external system container where you can place plugin buttons -->
                                    <button type="button" id="undoButton" class="undoButton control-button">Undo
                                    </button>
                                    <button type="button" id="redoButton" class="undoButton control-button">Redo
                                    </button>
                                    <button type="button" id="codeEditor" class="undoButton control-button">Code
                                        editor
                                    </button>
                                    <span id="changeHistoryContainer" class="changeHistoryContainer"
                                          style="display: none;">Last change: <a id="changeHistoryLink"></a></span>
                                </div>
                            </div>
                        </div>
                        <div class="flex-row">
                            <div class="form-group col-quarter">

                                <div id="stripoSettingsContainer">

                                </div>

                            </div>
                            <div class="form-group col-3-quarters">

                                <div id="stripoPreviewContainer">

                                </div>
                            </div>
                        </div>
                    </div><!-- /.flex-row -->
                </div>
                <div class="modal-footer">
                    <div class="notification-zone"></div>
                    <button type="submit" class="nb-btn nb-primary icon-btn SaveEmail" ><i class="fa fa-save"></i> Save Email <span></button>
                    <button type="button" class="nb-btn nb-secondary" data-dismiss="modal">Close Window</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- EmailPreviewModal -->
<div id="EmailPreviewModal" data-emailid="" class="modal fade EmailPreviewModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Email Preview</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
            </div>
            <div class="modal-body">
                <div class="EmailPreviewModalBody">
                    <div class="flex-row">
                        <div class="form-group col-full">
                            <label class="label-sm" for="TemplateName"><span
                                        class="TabTitleLabel">Template Name: </span></label>
                            <input type="text" class="form-control TemplateName" name="TemplateName"
                                   placeholder="Type TemplateName" autocomplete="off" readonly>
                        </div>
                    </div>
                    <div class="flex-row">
                        <iframe class="EmailTemplatePreview form-control" data-emailid=""  src="" >

                        </iframe>
                    </div>
                </div><!-- /.flex-row -->
            </div>
            <div class="modal-footer">
                <div class="notification-zone"></div>
                <button type="button"
                        class="nb-btn nb-primary icon-btn UseEmailTemplate"
                        data-currentemailid=""
                        data-templatename=""
                        onclick="useEmailTempalte(this,true);"
                ><i class="fa fa-save"></i> Use Email Template<span></button>
                <button type="button" class="nb-btn nb-secondary" data-dismiss="modal">Close Window</button>
            </div>
        </div>
    </div>
</div>
<?php
$deleteContactWithDORelationship = "
<div id=\"action-confirmation\" title=\"Delete Contact\">
<p class='alertFirstRow'><span class=\"ui-icon ui-icon-alert\"></span>You are about to delete a contact.</p>
<p class='alertAfterFirstRow'>This will also remove the contact from any Data Objects it has a relationship with. </p>
<p class='alertAfterFirstRow'>This cannot be undone. Do you want to process?</p>
<p class='alertAfterFirstRow extraInfo'></p>
</div>
";
$deleteContact = "
<div id=\"action-confirmation\" title=\"Delete Contact\">
<p class='alertFirstRow'><span class=\"ui-icon ui-icon-alert\"></span>You are about to delete a contact.</p>
<p class='alertAfterFirstRow'>This cannot be undone. Do you want to process?</p>
<p class='alertAfterFirstRow extraInfo'></p>
</div>
";
$deleteDataObject = "
<div id=\"action-confirmation\" title=\"Delete Data Object\">
<p class='alertFirstRow'><span class=\"ui-icon ui-icon-alert\"></span>You are about to delete a Data Object Item of <span class='DataObjectName'></span></p>
<p class='alertAfterFirstRow'>This will remove it from ALL contacts who hold a Relationship to this item. </p>
<p class='alertAfterFirstRow'>This cannot be undone. Do you want to process?</p>
<p class='alertAfterFirstRow extraInfo'></p>
</div>
";
?>
<script>
    MacantaDialogMessages['deleteContact'] = <?php echo json_encode($deleteContact); ?>;
    MacantaDialogMessages['deleteContactWithDORelationship'] = <?php echo json_encode($deleteContactWithDORelationship); ?>;
    MacantaDialogMessages['deleteDataObject'] = <?php echo json_encode($deleteDataObject); ?>;
</script>

