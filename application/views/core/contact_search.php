<!--Start of search panel-->
<small class="dev-only php">contact_search.php</small>
<div class="searchCall user-info-b">
    <div class="input-group inside-search">
        <input type="text" class="form-control searchKey InAppSearch "
               placeholder="<?php echo $this->lang->line('text_search_for'); ?>..." data-source="InAppSearch">
        <span class="input-group-btn">
                    <button class="nb-btn nb-primary search" type="button"
                            data-source="InAppSearch"><?php echo $this->lang->line('text_go'); ?>!</button>
                  </span>
    </div>
    <div class="input-group recent-results">
        <a href="<?php
        $URL = $this->config->item('base_url');
        $URL = str_replace('/public', '', $URL);
        echo $URL;
        ?>"><i class="fa fa-angle-double-left" aria-hidden="true"></i> <span
                    class="label showRecentResults"><?php echo $this->lang->line('text_view_recent_results'); ?>
            </span>
        </a>

        <?php
        $APPPATH = APPPATH ;
        $CachePath = $APPPATH . "cache/" . 'searched_cache' . $session_data['InfusionsoftID'];
        $CacheContent = @file_get_contents($CachePath);
        $Next = false;
        $NextContactId = '';
        $theKey = 0;
        $hide = '';
        $IdKey = 'Id';
        if ($CacheContent) {
            $CacheContent = unserialize($CacheContent);
            $ttl = $CacheContent['ttl'];
            $time = $CacheContent['time'];
            $theTime = time();
            $lapse = $theTime - $time;
            if ($lapse < $ttl) {
                $CacheContent = json_decode($CacheContent['data'], true);
                foreach ($CacheContent as $key => $Contact) {
                    $IdKey = isset($Contact['Contact ID']) ? "Contact ID" : $IdKey;
                    $IdKey = isset($Contact['Contact Id']) ? "Contact Id" : $IdKey;
                    $IdKey = isset($Contact['Contact id']) ? "Contact id" : $IdKey;
                    $IdKey = isset($Contact['ContactId']) ? "ContactId" : $IdKey;
                    $IdKey = isset($Contact['ContactID']) ? "ContactID" : $IdKey;
                    if ($Next === true) {
                        $theKey = $key;
                        $NextContactId = $Contact[$IdKey];
                        break;
                    }
                    if ($Contact[$IdKey] == $Id) $Next = true;
                }
                if ($NextContactId == '') {
                    $NextContactId = $CacheContent[0][$IdKey];
                }
                if ($NextContactId == $Id) {
                    $hide = 'hideThis';
                }
            } else {
                $hide = 'hideThis';
                unlink($CachePath);
            }

        } else {
            $hide = 'hideThis';
        }

        ?>
        <span class="label <?php echo $hide; ?>">
            <a href="#contact/<?php echo $NextContactId; ?>" class="showNextResult btn btn-warning btn-sm"
               title="<?php echo $CacheContent[$theKey]['FirstName'] . " " . $CacheContent[$theKey]['LastName'] . " (" . $CacheContent[$theKey]['Email'] . ")"; ?>">
                Next Contact <i class="fa fa-angle-double-right" aria-hidden="true"></i>
            </a>
        </span>
    </div>
</div>
<!--end of search panel-->
