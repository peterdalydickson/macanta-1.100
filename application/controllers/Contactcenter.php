<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Contactcenter extends REST_Controller {

    public $TwilioWorkspace;
    public $TwilioAccountSid;
    public $TwiliOauthToken;
    public $TwilioClient;
    public $_Instance = null;
    public $Activities = [];
    public $TwilioRestURL = "https://taskrouter.twilio.com/v1/";
    public $DefaultActivities = [
        "Offline"=>"",
        "Idle"=>"",
        "Busy"=>"",
        "Reserved"=>"",
    ];
    public $WorkerAttributes = [
        "skills" =>["sales","billing","technical","support"],
        "languages" => ["english","spanish","russian"]
    ];
    public $DefaultTaskQueues =[
        "Sales Support"=>'(skills HAS "sales") AND (languages HAS "english")',
        "Billing Support"=>'(skills HAS "billing") AND (languages HAS "english")',
        "Technical Support"=>'(skills HAS "technical") AND (languages HAS "english")',
        "Customer Support"=>'(skills HAS "support") AND (languages HAS "english")'
    ];
    public $DefaultWorkflow =[
        "WorkFlowName" => ""
    ];
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
        $this->load->helper('url');

    }
    public function action_post(){
        header("Content-Type: application/json");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $PostData = $this->post();
        $session_name = $PostData['session_name'];
        $user_seession_data = macanta_get_user_seession_data($session_name);
        $Session_data = unserialize($user_seession_data->session_data);
        if( !isset($PostData['action']))
            $this->response('Forbidden: Invalid Request', REST_Controller::HTTP_FORBIDDEN);

        if ($user_seession_data == false){
            $this->response("Error: Unauthorized Request!", REST_Controller::HTTP_FORBIDDEN);
        }



        $Action = $PostData['action'];
        $Data = isset($PostData['data']) ? $PostData['data']:[];
        if(method_exists($this, $Action)){
            $this->TwilioAccountSid = $this->config->item('Twilio_Account_SID');
            $this->TwiliOauthToken = $this->config->item('Twilio_TOKEN');
            $this->TwilioClient = new Twilio\Rest\Client($this->TwilioAccountSid, $this->TwiliOauthToken);
            $this->response($this->$Action($Data), REST_Controller::HTTP_OK);
        }else{
            $this->response('Invalid Request: There is no '.$Action.' action', REST_Controller::HTTP_OK);
        }
    }
    /* WORKSPACE CRUD */
    public function CreateWorkspace($Data){
        $Data['eventsFilter'] = 'task.created,task.completed,task.canceled,task.deleted,task.updated,task-queue.entered,task-queue.moved,reservation.created,reservation.accepted,reservation.rejected,reservation.timeout,reservation.canceled,reservation.rescinded,workflow.entered,workflow.timeout,worker.activity.update';
        $workspace = $this->TwilioClient
            ->taskrouter
            ->workspaces
            ->create(
                $Data['WorkspaceName'],
                array(
                    'eventCallbackUrl' => $Data['EventCallbackUrl'],
                    'template' => 'FIFO',
                    'eventsFilter' => $Data['eventsFilter']
                )
            );
        $CreatedWorkspace = [
            "sid"=>$workspace->sid,
            "accountSid"=>$workspace->accountSid,
            "friendlyName"=>$workspace->friendlyName,
            "eventCallbackUrl"=>$workspace->eventCallbackUrl,
            "dateCreated"=>$workspace->dateCreated,
            "dateUpdated"=>$workspace->dateUpdated,
            "defaultActivityName"=>$workspace->defaultActivityName,
            "defaultActivitySid"=>$workspace->defaultActivitySid,
            "timeoutActivityName"=>$workspace->timeoutActivityName,
            "timeoutActivitySid"=> $workspace->timeoutActivitySid,
            "multiTaskEnabled"=>$workspace->multiTaskEnabled,
            "prioritizeQueueOrder"=>$workspace->prioritizeQueueOrder,
            "url"=>$workspace->url,
            "links"=>$workspace->links
        ];
        return $CreatedWorkspace;
    }
    public function ReadWorkspace($Data = []){
        $ReadWorkspace = [];
        if(isset($Data['WorkspaceSid'])){
            $workspace = $this->TwilioClient->taskrouter->workspaces($Data['WorkspaceSid'])->fetch();
            $ReadWorkspace[] = [
                "sid"=>$workspace->sid,
                "accountSid"=>$workspace->accountSid,
                "friendlyName"=>$workspace->friendlyName,
                "eventCallbackUrl"=>$workspace->eventCallbackUrl,
                "defaultActivitySid"=>$workspace->defaultActivitySid,
                "dateCreated"=>$workspace->dateCreated,
                "dateUpdated"=>$workspace->dateUpdated,
                "defaultActivityName"=>$workspace->defaultActivityName,
                "timeoutActivityName"=>$workspace->timeoutActivityName,
                "timeoutActivitySid"=> $workspace->timeoutActivitySid,
                "multiTaskEnabled"=>$workspace->multiTaskEnabled,
                "prioritizeQueueOrder"=>$workspace->prioritizeQueueOrder,
                "url"=>$workspace->url,
                "links"=>$workspace->links
            ];
        }else{
            $workspaces = $this->TwilioClient->taskrouter->workspaces->read();
            foreach ($workspaces as $workspace) {
                $ReadWorkspace[] = [
                    "sid"=>$workspace->sid,
                    "accountSid"=>$workspace->accountSid,
                    "friendlyName"=>$workspace->friendlyName,
                    "eventCallbackUrl"=>$workspace->eventCallbackUrl,
                    "defaultActivitySid"=>$workspace->defaultActivitySid,
                    "dateCreated"=>$workspace->dateCreated,
                    "dateUpdated"=>$workspace->dateUpdated,
                    "defaultActivityName"=>$workspace->defaultActivityName,
                    "timeoutActivityName"=>$workspace->timeoutActivityName,
                    "timeoutActivitySid"=> $workspace->timeoutActivitySid,
                    "multiTaskEnabled"=>$workspace->multiTaskEnabled,
                    "prioritizeQueueOrder"=>$workspace->prioritizeQueueOrder,
                    "url"=>$workspace->url,
                    "links"=>$workspace->links
                ];
            }
        }



        return $ReadWorkspace;
    }
    public function UpdateWorkspace($Data){
        //Remove In Twilio
        $NewValues=[];
        if(isset($Data['NewValues'])){
            foreach ($Data['NewValues'] as $Key=>$Value){
                $NewValues[$Key] = $Value;
                /**
                 * @property string defaultActivitySid
                 * @property string eventCallbackUrl
                 * @property string eventsFilter
                 * @property string friendlyName
                 * @property boolean multiTaskEnabled
                 * @property string timeoutActivitySid
                 * @property string prioritizeQueueOrder
                 */
            }
        }
        $workspace = $this->TwilioClient
            ->taskrouter
            ->workspaces($Data['WorkspaceSid'])
            ->update($NewValues );

        $UpdatedWorkspace = [
            "sid"=>$workspace->sid,
            "accountSid"=>$workspace->accountSid,
            "friendlyName"=>$workspace->friendlyName,
            "eventCallbackUrl"=>$workspace->eventCallbackUrl,
            "defaultActivitySid"=>$workspace->defaultActivitySid,
            "dateCreated"=>$workspace->dateCreated,
            "dateUpdated"=>$workspace->dateUpdated,
            "defaultActivityName"=>$workspace->defaultActivityName,
            "timeoutActivityName"=>$workspace->timeoutActivityName,
            "timeoutActivitySid"=> $workspace->timeoutActivitySid,
            "multiTaskEnabled"=>$workspace->multiTaskEnabled,
            "prioritizeQueueOrder"=>$workspace->prioritizeQueueOrder,
            "url"=>$workspace->url,
            "links"=>$workspace->links
        ];
        return $UpdatedWorkspace;
    }
    public function DeleteWorkspace($Data){
        //Remove In Twilio
        $workspace = $this->TwilioClient
            ->taskrouter
            ->workspaces($Data['WorkspaceSid'])
            ->delete();

        return $workspace;
    }

    /* WORKER CRUD */
    public function CreateWorker($Data){
        $worker = $this->TwilioClient
            ->taskrouter
            ->workspaces($Data['WorkspaceSid'])
            ->workers
            ->create(
                $Data['WorkerName'],
                array('attributes' => json_encode($Data['Attributes']))
            );
        $CreatedWorker = [
            "sid" => $worker->sid,
            "friendlyName" => $worker->friendlyName,
            "accountSid" => $worker->accountSid,
            "activitySid" => $worker->activitySid,
            "activityName" => $worker->activityName,
            "workspaceSid" => $worker->workspaceSid,
            "attributes" => json_decode($worker->attributes),
            "available" => $worker->available,
            "dateCreated" => $worker->dateCreated,
            "dateUpdated" => $worker->dateUpdated,
            "dateSstatusChanged" => $worker->dateStatusChanged,
            "url" => $worker->url,
            "links" => $worker->links
          ];
        return $CreatedWorker;
    }
    public function ReadWorker($Data){
        $ReadWorker = [];
        if(isset($Data['WorkerSid'])){
            $worker = $this->TwilioClient->taskrouter->workspaces($Data['WorkspaceSid'])->workers($Data['WorkerSid'])->fetch();
            $ReadWorker[] = [
                "sid" => $worker->sid,
                "friendlyName" => $worker->friendlyName,
                "accountSid" => $worker->accountSid,
                "activitySid" => $worker->activitySid,
                "activityName" => $worker->activityName,
                "workspaceSid" => $worker->workspaceSid,
                "attributes" => json_decode($worker->attributes),
                "available" => $worker->available,
                "dateCreated" => $worker->dateCreated,
                "dateUpdated" => $worker->dateUpdated,
                "dateSstatusChanged" => $worker->dateStatusChanged,
                "url" => $worker->url,
                "links" => $worker->links
            ];
        }else{
            $workers = $this->TwilioClient->taskrouter->workspaces($Data['WorkspaceSid'])->workers->read();
            foreach ($workers as $worker) {
                $ReadWorker[] = [
                    "sid" => $worker->sid,
                    "friendlyName" => $worker->friendlyName,
                    "accountSid" => $worker->accountSid,
                    "activitySid" => $worker->activitySid,
                    "activityName" => $worker->activityName,
                    "workspaceSid" => $worker->workspaceSid,
                    "attributes" => json_decode($worker->attributes),
                    "available" => $worker->available,
                    "dateCreated" => $worker->dateCreated,
                    "dateUpdated" => $worker->dateUpdated,
                    "dateSstatusChanged" => $worker->dateStatusChanged,
                    "url" => $worker->url,
                    "links" => $worker->links
                ];
            }
        }



        return $ReadWorker;
    }
    public function UpdateWorker($Data){
        //Remove In Twilio
        $NewValues=[];
        if(isset($Data['NewValues'])){
            foreach ($Data['NewValues'] as $Key=>$Value){
                $NewValues[$Key] = $Value;
                /**
                 * @param string $activitySid The activity_sid
                 * @param string $attributes The attributes
                 * @param string $friendlyName The friendly_name
                 */
            }
        }
        $worker = $this->TwilioClient
            ->taskrouter
            ->workspaces($Data['WorkspaceSid'])
            ->workers($Data['WorkerSid'])
            ->update($NewValues);

        $UpdatedWorker = [
            "sid" => $worker->sid,
            "friendlyName" => $worker->friendlyName,
            "accountSid" => $worker->accountSid,
            "activitySid" => $worker->activitySid,
            "activityName" => $worker->activityName,
            "workspaceSid" => $worker->workspaceSid,
            "attributes" => json_decode($worker->attributes),
            "available" => $worker->available,
            "dateCreated" => $worker->dateCreated,
            "dateUpdated" => $worker->dateUpdated,
            "dateSstatusChanged" => $worker->dateStatusChanged,
            "url" => $worker->url,
            "links" => $worker->links
        ];
        $this->db->where('key','CallCenterData');
        $query = $this->db->get('config_data');
        $row = $query->row();
        if (isset($row)) {
            $_theWorkspace = json_decode($row->value, true);
            $WorkerName = $UpdatedWorker['friendlyName'];
            $_theWorkspace['Workers'][$WorkerName] = $UpdatedWorker;
            //save to the database
            $DBdata['value'] = json_encode($_theWorkspace);
            $this->db->where('key','CallCenterData');
            $this->db->update('config_data',$DBdata);
        }
        $_theWorkspace['UpdatedWorker'] = $UpdatedWorker;
        //$_theWorkspace['RealTimeStatistics'] = $this->RealTimeStatistics();
        $_theWorkspace['WorkerAttributes'] = $this->WorkerAttributes;
        return $_theWorkspace;
    }
    public function DeleteWorker($Data){
        //Remove In Twilio
        $workspace = $this->TwilioClient
            ->taskrouter
            ->workspaces($Data['WorkspaceSid'])
            ->workers($Data['WorkerSid'])
            ->delete();

        return $workspace;
    }

    /* TASKQUEUE CRUD */
    public function CreateTaskQueue($Data){

        $taskQueue = $this->TwilioClient
            ->taskrouter
            ->workspaces($Data['WorkspaceSid'])
            ->taskQueues
            ->create(
                $Data['TaskQueueName'], $Data['ReservationActivitySid'], $Data['AssignmentActivitySid'],
                $Data['Parameters']
            );
        $CreateTaskQueue = [
            "sid" => $taskQueue->sid,
            "friendlyName" => $taskQueue->friendlyName,
            "targetWorkers" => $taskQueue->targetWorkers
        ];
        return $CreateTaskQueue;

    }
    public function ReadTaskQueue($Data){
        $ReadTaskQueue = [];
        if(isset($Data['TaskQueueSid'])){
            $taskQueue = $this->TwilioClient
                ->taskrouter
                ->workspaces($Data['WorkspaceSid'])
                ->taskQueues($Data['TaskQueueSid'])
                ->fetch();
            $ReadTaskQueue[] = [
                "sid" => $taskQueue->sid,
                "friendlyName" => $taskQueue->friendlyName,
                "targetWorkers" => $taskQueue->targetWorkers
            ];
        }else{
            $taskQueues = $this->TwilioClient
                ->taskrouter
                ->workspaces($Data['WorkspaceSid'])
                ->taskQueues
                ->read();
            foreach ($taskQueues as $taskQueue) {
                $ReadTaskQueue[] = [
                    "sid" => $taskQueue->sid,
                    "friendlyName" => $taskQueue->friendlyName,
                    "targetWorkers" => $taskQueue->targetWorkers
                ];
            }
        }
        return $ReadTaskQueue;
    }

    /* WORKFLOWS CRUD */
    public function CreateWorkFlow($Data){

        $workFlow = $this->TwilioClient
            ->taskrouter
            ->workspaces($Data['WorkspaceSid'])
            ->workflows
            ->create(
                $Data['WorkFlowName'], $Data['Configuration'], $Data['Options']
                /*
                    $Data['Options']['assignmentCallbackUrl'],
                    $Data['Options']['fallbackAssignmentCallbackUrl'],
                    $Data['Options']['taskReservationTimeout']

                //Configuration
                {  
                  "task_routing":{
                    "filters":[
                      {
                        "friendly_name": "<friendly name>",
                        "expression": "<condition>",
                        "targets":[
                          {
                            "queue": "<task queue sid>",
                            "priority": "<priority>",
                            "timeout": "<duration>",
                            "expression": "<condition>"
                          },
                          ...
                        ]
                      },
                      ...
                    ],
                    "default_filter":{
                      "queue": "<default task queue sid>"
                    }
                  }
                }
                */
            );
        $CreateWorkFlow = [
            "sid" => $workFlow->sid,
            "friendlyName" => $workFlow->friendlyName
        ];
        return $CreateWorkFlow;

    }
    public function ReadWorkFlows($Data){
        $ReadWorkFlows = [];
        if(isset($Data['WorkFlowsSid'])){
            $workFlows = $this->TwilioClient
                ->taskrouter
                ->workspaces($Data['WorkspaceSid'])
                ->workflows($Data['WorkFlowsSid'])
                ->fetch();
            $ReadWorkFlows[] = [
                "sid" => $workFlows->sid,
                "friendlyName" => $workFlows->friendlyName
            ];
        }else{
            $workFlows = $this->TwilioClient
                ->taskrouter
                ->workspaces($Data['WorkspaceSid'])
                ->workflows
                ->read();
            foreach ($workFlows as $workFlow) {
                $ReadWorkFlows[] = [
                    "sid" => $workFlow->sid,
                    "friendlyName" => $workFlow->friendlyName
                ];
            }
        }
        return $ReadWorkFlows;
    }

    /* ACTIVITY CRUD */
    public function ReadActivities($Data){
        $ReadActivities = [];
        if(isset($Data['ActivitySid'])){
            $Activity = $this->TwilioClient
                ->taskrouter->workspaces($Data['WorkspaceSid'])
                ->activities($Data['ActivitySid'])
                ->fetch();
            $ReadActivities[] = [
                "sid" => $Activity->sid,
                "friendlyName" => $Activity->friendlyName,
                /*"accountSid" => $Activity->accountSid,
                "workspaceSid" => $Activity->workspaceSid,
                "available" => $Activity->available,
                "dateCreated" => $Activity->dateCreated,
                "dateUpdated" => $Activity->dateUpdated,
                "url" => $Activity->url*/
            ];
        }else{
            $Activities = $this->TwilioClient->taskrouter->workspaces($Data['WorkspaceSid'])->activities->read();
            foreach ($Activities as $Activity) {
                $ReadActivities[] = [
                    "sid" => $Activity->sid,
                    "friendlyName" => $Activity->friendlyName,
                    /*"accountSid" => $Activity->accountSid,
                    "workspaceSid" => $Activity->workspaceSid,
                    "available" => $Activity->available,
                    "dateCreated" => $Activity->dateCreated,
                    "dateUpdated" => $Activity->dateUpdated,
                    "url" => $Activity->url*/
                ];
            }
        }
        return $ReadActivities;
    }



    /* CallCenterInitialization */
    public function SetupCallcenter(){
        $theWorkspaces = $this->ReadWorkspace();
        $_theWorkspace = [];
        $Logs = [];

        /* ====== WORKSPACE SECTION ====== */
        // No Existing Call Center, Create one.
        if(sizeof($theWorkspaces) == 0){
            //Create Workspace
            $Data = [
                "WorkspaceName"=>"Call Center",
                "EventCallbackUrl"=>"https://".$this->config->item('MacantaAppName').".macanta.".EXT."/contactcenter/webhook"
            ];
            $theWorkspace = $this->CreateWorkspace($Data);
            if(isset($theWorkspace['sid'])){

                $this->db->where('key','CallCenterData');
                $query = $this->db->get('config_data');
                $row = $query->row();
                if (isset($row)) {
                    $DBdata['value'] = json_encode(["Workspaces"=>$theWorkspace]);
                    $this->db->where('key','CallCenterData');
                    $this->db->update('config_data',$DBdata);
                }else{
                    $DBdata['value'] = json_encode(["Workspaces"=>$theWorkspace]);
                    $DBdata['key'] = 'CallCenterData';
                    $this->db->insert('config_data', $DBdata);

                }
            }
            $_theWorkspace['Workspace'] =  $theWorkspace;
            $Logs[] = "Workspace created and recorded to database";

        }
        // Existing Call Center Found!, Get one.
        else{
            $this->db->where('key','CallCenterData');
            $query = $this->db->get('config_data');
            $row = $query->row();
            $CallCenterDataWorkspaceSid = '';
            if (isset($row)) {
                $CallCenterData = trim($row->value) !== '' ? json_decode($row->value, true):[];
                $CallCenterDataWorkspaceSid = isset($CallCenterData["Workspaces"]["sid"]) ? $CallCenterData["Workspaces"]["sid"]:'';
            }
            foreach ($theWorkspaces as $theWorkspace){
                if($theWorkspace["sid"] == $CallCenterDataWorkspaceSid){
                    $_theWorkspace['Workspace'] = $theWorkspace;
                    break;
                }
            }
            //Nothings Recorded In the database, save the first one
            if($_theWorkspace == []){
                foreach ($theWorkspaces as $theWorkspace){
                    $this->db->where('key','CallCenterData');
                    $query = $this->db->get('config_data');
                    $row = $query->row();
                    if (isset($row)) {
                        $DBdata['value'] = json_encode(["Workspaces"=>$theWorkspace]);
                        $this->db->where('key','CallCenterData');
                        $this->db->update('config_data',$DBdata);
                    }else{
                        $DBdata['value'] = json_encode(["Workspaces"=>$theWorkspace]);
                        $DBdata['key'] = 'CallCenterData';
                        $this->db->insert('config_data', $DBdata);

                    }
                    $_theWorkspace['Workspace'] = $theWorkspace;
                    $Logs[] = "Workspace retrieved and recorded to database";
                    break;
                }
            }else{
                $Logs[] = "Workspace already setup in the database";
            }


        }


        /* ====== ACTIVITIES SECTION =======   */
        $Activities = $this->DefaultActivities;
        //"Offline", "Idle", "Busy", and "Reserved"
        // Get Available Activities
        $DataACTIVITIES['WorkspaceSid'] = $_theWorkspace['Workspace']['sid'];
        $_theWorkspace['Activities'] = $this->ReadActivities($DataACTIVITIES);
        //Fill the activities with SID
        foreach ($_theWorkspace['Activities'] as $Activity){
            foreach ($this->DefaultActivities as $ActivityName => $Sid){
                    if($Activity['friendlyName'] == $ActivityName) $Activities[$ActivityName] = $Activity;
            }
        }
        $this->db->where('key','CallCenterData');
        $query = $this->db->get('config_data');
        $row = $query->row();
        if (isset($row)) {
            $OldValue = json_decode($row->value, true);
            $OldValue['Activities'] = $Activities;
            $NewValue = $OldValue;
            $DBdata['value'] = json_encode($NewValue);
            $this->db->where('key','CallCenterData');
            $this->db->update('config_data',$DBdata);
        }


        /* ====== TASKQUEUE SECTION========  */
        $DataTASKQUEUES = $CreatedTaskQueues = [];
        foreach ($this->DefaultTaskQueues as $DefaultTaskQueue=>$TargetWorkers){
            $DataTASKQUEUES[] = [
                'WorkspaceSid'=>$_theWorkspace['Workspace']['sid'],
                'TaskQueueName'=> $DefaultTaskQueue,
                'ReservationActivitySid'=>$Activities['Reserved']['sid'],
                'AssignmentActivitySid'=>$Activities['Busy']['sid'],
                'Parameters'=>[
                    'TargetWorkers' => $TargetWorkers
                ]
            ];
        }
        $ReadTaskQueueParam = [
            "WorkspaceSid" => $_theWorkspace['Workspace']['sid']
        ];

        $ReadTaskQueues = $this->ReadTaskQueue($ReadTaskQueueParam);
        foreach ($DataTASKQUEUES as $DataTASKQUEUE){
            $TaskQueueFound =  false;
            if(sizeof($ReadTaskQueues) > 0){
                foreach ($ReadTaskQueues as $ReadTaskQueue){
                    // If Existing, Record it
                    if(strtolower($ReadTaskQueue['friendlyName']) == strtolower($DataTASKQUEUE['TaskQueueName'])){
                        $CreatedTaskQueues[$ReadTaskQueue['friendlyName']] = $ReadTaskQueue;
                        $TaskQueueFound = true;
                        break;
                    }
                }
                if($TaskQueueFound == false){
                    //Create If not Existing in Twilio
                    $CreatedTaskQueue = $this->CreateTaskQueue($DataTASKQUEUE);
                    $CreatedTaskQueues[$CreatedTaskQueue['friendlyName']] = $CreatedTaskQueue;
                }
            }else{
                //Always Create
                $CreatedTaskQueue = $this->CreateTaskQueue($DataTASKQUEUE);
                $CreatedTaskQueues[$CreatedTaskQueue['friendlyName']] = $CreatedTaskQueue;
            }


        }
        $this->db->where('key','CallCenterData');
        $query = $this->db->get('config_data');
        $row = $query->row();
        if (isset($row)) {
            $OldValue = json_decode($row->value, true);
            $OldValue['TaskQueues'] = $CreatedTaskQueues;
            $NewValue = $OldValue;
            $DBdata['value'] = json_encode($NewValue);
            $this->db->where('key','CallCenterData');
            $this->db->update('config_data',$DBdata);
        }

        $ReadWorkers = [];
        $ReadWorkerQueueParam = [
            "WorkspaceSid" => $_theWorkspace['Workspace']['sid']
        ];
        $Workers = $this->ReadWorker($ReadWorkerQueueParam);
        if(sizeof($Workers) > 0){
            foreach ($Workers as $Worker){
                // If Existing, Record it
                $ReadWorkers[$Worker['friendlyName']] = $Worker;
            }
        }
        $this->db->where('key','CallCenterData');
        $query = $this->db->get('config_data');
        $row = $query->row();
        if (isset($row)) {
            $OldValue = json_decode($row->value, true);
            $OldValue['Workers'] = $ReadWorkers;
            $NewValue = $OldValue;
            $DBdata['value'] = json_encode($NewValue);
            $this->db->where('key','CallCenterData');
            $this->db->update('config_data',$DBdata);
        }


        //todo: get all statistics
        //$_theWorkspace['RealTimeStatistics'] = $this->RealTimeStatistics();
        $_theWorkspace['WorkerAttributes'] = $this->WorkerAttributes;
        return $_theWorkspace;
    }
    public function RealTimeStatistics(){
        $RealTimeStatistics = $this->_RealTimeStatistics();
        return $RealTimeStatistics;
    }
    public function SaveAgent($Data){
        $this->db->where('key','CallCenterData');
        $query = $this->db->get('config_data');
        $row = $query->row();
        if (isset($row)) {
            $OldValue = $_theWorkspace = json_decode($row->value, true);
            $Param['WorkspaceSid'] = $_theWorkspace['Workspaces']['sid'];
            $Param['WorkerName'] = "$Data[ContactId] | ".$Data['FirstName']." ".$Data['LastName'];
            $Param['Attributes'] = [
                "skills"=> $Data['skills'],
                "languages"=> $Data['languages'],
                "email"=> $Data['Email'],
                "infusionsoftId"=> $Data['ContactId'],
            ];
            $NewWorker = $this->CreateWorker($Param);
            $OldValue['Workers'][$Param['WorkerName']] = $NewWorker;
            //save to the database
            $DBdata['value'] = json_encode($OldValue);
            $this->db->where('key','CallCenterData');
            $this->db->update('config_data',$DBdata);

        }
        $_theWorkspace['NewWorker'] = $NewWorker;
        $_theWorkspace['WorkerAttributes'] = $this->WorkerAttributes;

        return $_theWorkspace;
    }
    public function ChosenAgent($Data){
        $Row = '<tr class="agent-tr" data-contactid="'.$Data['ContactId'].'" data-email="'.$Data['Email'].'" data-firstname="'.$Data['FirstName'].'" data-lastname="'.$Data['LastName'].'">';
        $Row .= "<td><i class=\"fa fa-tty\"></i> $Data[ContactId] | $Data[FullName]<small class='footnote'>$Data[Email]</small></td>";
        $Row .= "<td>";
        foreach ($this->WorkerAttributes['skills'] as $skill){
            $Row .= '<div class="skill-check col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad-left">
                        <span class="form-check-label">
                            <input id="contact_skill_'.$skill.'_'.$Data['ContactId'].'" type="checkbox"  class="addGroup checkbox form-check-input" name="skills_id_'.$Data['ContactId'].'" value="'.$skill.'" >
                            <label for="contact_skill_'.$skill.'_'.$Data['ContactId'].'">'.$skill.'</label>
                            </span>
                     </div>';
        }
        $Row .= "</td>";
        $Row .= "<td>";
        foreach ($this->WorkerAttributes['languages'] as $language){
            $Row .= '<div class="skill-check col-xs-4 col-sm-4 col-md-4 col-lg-4  no-pad-left">
                        <span class="form-check-label">
                            <input id="contact_lang_'.$language.'_'.$Data['ContactId'].'" type="checkbox"  class="addGroup checkbox form-check-input" name="languages_id_'.$Data['ContactId'].'" value="'.$language.'" >
                            <label for="contact_lang_'.$language.'_'.$Data['ContactId'].'">'.$language.'</label>
                            </span>
                     </div>';
        }
        $Row .= "</td>";
        $Row .= "<td><button type=\"button\" class=\"btn btn-default btn-save-agent\"> Save </button></td>";
        $Row .= '</tr>';
        return $Row;
    }

    public function UpdateCallcenterAgentActivity($Data){
        $_theWorkspace = [];
        $this->db->where('key','CallCenterData');
        $query = $this->db->get('config_data');
        $row = $query->row();
        if (isset($row)) {
            $newWorkspace = $_theWorkspace = json_decode($row->value, true);
            foreach ($_theWorkspace['Activities'] as $ActivityName=>$ActivityDetails){
                if($ActivityDetails['friendlyName'] == $Data['activityName']){
                    $Attr["WorkspaceSid"] = $Data['workspaceSid'];
                    $Attr["WorkerSid"] = $Data['sid'];
                    $Attr["NewValues"]["activitySid"] = $ActivityDetails['sid'];
                    $Attr["NewValues"]["activityName"] = $ActivityDetails['friendlyName'];
                    $UpdatedAgent = $this->UpdateWorker($Attr);
                    break;
                }
            }
            foreach ($_theWorkspace['Workers'] as $WorkerName=>$WorkerDetails){
                if($WorkerDetails['sid'] == $Data['sid']){
                    $newWorkspace['Workers'][$WorkerName] = $UpdatedAgent;
                    break;
                }
            }
            $DBdata['value'] = json_encode($newWorkspace);
            $this->db->where('key','CallCenterData');
            $this->db->update('config_data',$DBdata);
            $UpdatedWorker['Workers'][$UpdatedAgent['friendlyName']] = $UpdatedAgent;
            $_theWorkspace['UpdatedWorker'] = json_encode($UpdatedWorker);
            $_theWorkspace['RealTimeStatistics'] = $this->RealTimeStatistics();
            $_theWorkspace['WorkerAttributes'] = $this->WorkerAttributes;
        }
        return $_theWorkspace;
    }
    public function CallCenterHeartbeat(){
        $_theWorkspace = [];
        $_theWorkspace['WorkerAttributes'] = $this->WorkerAttributes;
        $_theWorkspace['RealTimeStatistics'] = $this->RealTimeStatistics();
        return $_theWorkspace;
    }
    public function RealTimeStatistics_get(){
        $RealTimeStatistics = $this->_RealTimeStatistics();
        $this->response($RealTimeStatistics, REST_Controller::HTTP_OK);
    }
    public function _RealTimeStatistics(){
        header("Content-Type: application/json");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $RealTimeStatistics = [];
        $BaseURL = "https://".$this->config->item('Twilio_Account_SID').":".$this->config->item('Twilio_TOKEN')."@taskrouter.twilio.com/v1/Workspaces/";
        $this->db->where('key','CallCenterData');
        $query = $this->db->get('config_data');
        $row = $query->row();
        if (isset($row)) {
            $_theWorkspace = json_decode($row->value, true);
            if(isset($_theWorkspace['Workspaces'])){
                $RealTimeStatistics['Workspaces'] = json_decode(file_get_contents($BaseURL.$_theWorkspace['Workspaces']['sid']."/RealTimeStatistics"), true);
                //minify
                unset($RealTimeStatistics['Workspaces']['url']);
                unset($RealTimeStatistics['Workspaces']['tasks_by_status']);
                unset($RealTimeStatistics['Workspaces']['account_sid']);
                unset($RealTimeStatistics['Workspaces']['workspace_sid']);
                unset($RealTimeStatistics['Workspaces']['tasks_by_priority']);
                unset($RealTimeStatistics['Workspaces']['longest_task_waiting_age']);
                unset($RealTimeStatistics['Workspaces']['total_tasks']);
            }
            if(isset($_theWorkspace['TaskQueues'])){
                foreach ($_theWorkspace['TaskQueues'] as $TaskQueueName => $TaskQueueDetails){
                    $RealTimeStatistics['TaskQueues'][$TaskQueueName] = json_decode(file_get_contents($BaseURL.$_theWorkspace['Workspaces']['sid']."/TaskQueues/$TaskQueueDetails[sid]/RealTimeStatistics"), true);
                    unset($RealTimeStatistics['TaskQueues'][$TaskQueueName]['tasks_by_status']);
                    unset($RealTimeStatistics['TaskQueues'][$TaskQueueName]['workspace_sid']);
                    unset($RealTimeStatistics['TaskQueues'][$TaskQueueName]['account_sid']);
                    unset($RealTimeStatistics['TaskQueues'][$TaskQueueName]['tasks_by_priority']);
                    unset($RealTimeStatistics['TaskQueues'][$TaskQueueName]['url']);
                    unset($RealTimeStatistics['TaskQueues'][$TaskQueueName]['task_queue_sid']);
                    unset($RealTimeStatistics['TaskQueues'][$TaskQueueName]['longest_task_waiting_age']);
                    unset($RealTimeStatistics['TaskQueues'][$TaskQueueName]['total_tasks']);
                }
            }
            if(isset($_theWorkspace['Workers'])){
                foreach ($_theWorkspace['Workers'] as $WorkerName => $WorkerDetails){
                    $RealTimeStatistics['Workers'][$WorkerName] = json_decode(file_get_contents($BaseURL.$_theWorkspace['Workspaces']['sid']."/Workers/$WorkerDetails[sid]"), true);
                    unset($RealTimeStatistics['Workers'][$WorkerName]['available']);
                    unset($RealTimeStatistics['Workers'][$WorkerName]['workspace_sid']);
                    unset($RealTimeStatistics['Workers'][$WorkerName]['date_updated']);
                    unset($RealTimeStatistics['Workers'][$WorkerName]['activity_sid']);
                    unset($RealTimeStatistics['Workers'][$WorkerName]['account_sid']);
                    unset($RealTimeStatistics['Workers'][$WorkerName]['url']);
                    unset($RealTimeStatistics['Workers'][$WorkerName]['date_status_changed']);
                    unset($RealTimeStatistics['Workers'][$WorkerName]['date_created']);
                    unset($RealTimeStatistics['Workers'][$WorkerName]['links']);
                }
            }
            //https://taskrouter.twilio.com/v1/Workspaces/WSac74bbb15b5079c31ed876b8ee383ac6/RealTimeStatistics
            //https://taskrouter.twilio.com/v1/Workspaces/WSac74bbb15b5079c31ed876b8ee383ac6/TaskQueues/WQ0476d32cc79741a5ed467632586f59cd/RealTimeStatistics
            //https://taskrouter.twilio.com/v1/Workspaces/WSac74bbb15b5079c31ed876b8ee383ac6/Workers
        }
        return $RealTimeStatistics;

    }
    public function webhook_post(){
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $PostData = $this->post();
        file_put_contents(dirname(__FILE__)."/ContactcenterPostedWebhook_".$this->config->item('MacantaAppName').".txt", json_encode($PostData)."\n\n", FILE_APPEND);
        $this->response($PostData, REST_Controller::HTTP_OK);
    }
    public function webhook_get(){
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $GetData = $this->get();
        file_put_contents(dirname(__FILE__)."/ContactcenterGetWebhook_".$this->config->item('MacantaAppName').".txt", json_encode($GetData)."\n\n", FILE_APPEND);
        $this->response($GetData, REST_Controller::HTTP_OK);
    }
    public function TaskRouterCache($Id = "%", $Force = false, $FormId = false, $GroupId = false, $Label = false)
    {
        $CI =& get_instance();
        $CI->load->helper('rucksack_helper');
        $CI->load->helper('macanta_helper');
        $CustomFields = manual_cache_loader('infusionsoft_get_custom_fields');
        $CustomFields = $Force == true ? false:json_decode($CustomFields);
        if($CustomFields == false) {
            $action = "query_is";
            $returnFields = array('"DataType"', '"DefaultValue"', '"FormId"', '"GroupId"', '"Id"', '"Label"', '"ListRows"', '"Name"', '"Values"');
            $returnFieldsStr = implode(', ', $returnFields);
            $action_details = '{"table":"DataFormField","limit":"1000","page":0,"fields":[' . $returnFieldsStr . '],"query":{"Id":"' . $Id . '"}}';
            $CustomFields = applyFn('rucksack_request',$action, $action_details,false);
            manual_cache_writer('infusionsoft_get_custom_fields', json_encode($CustomFields), 86400);
        }
        if($FormId == false && $GroupId == false && $Label == false){
            return $CustomFields->message;
        }else{
            $NewCustomFields = [];
            if($FormId !== false){
                foreach ($CustomFields->message as $CustomField){
                    if($CustomField->FormId == $FormId) $NewCustomFields[] = $CustomField;
                }
                $CustomFields->message = $NewCustomFields;
            }

            $NewCustomFields = [];
            if($GroupId !== false){
                foreach ($CustomFields->message as $CustomField){
                    if($CustomField->GroupId == $GroupId) $NewCustomFields[] = $CustomField;
                }
                $CustomFields->message = $NewCustomFields;
            }

            $NewCustomFields = [];
            if($Label !== false){
                foreach ($CustomFields->message as $CustomField){
                    if(strtolower(trim($CustomField->Label ))  == strtolower(trim($Label))) $NewCustomFields[] = $CustomField;
                }
                $CustomFields->message = $NewCustomFields;
            }
            return $CustomFields->message;
        }

    }
    public function checkAPIkey($Key){
        $password = $this->config->item('macanta_api_key');
        return  $Key == $password ? true:false;
    }
    public function action_get(){
        $this->response('Forbidden!', REST_Controller::HTTP_FORBIDDEN);
    }
    public function index_get(){

        $this->response('Forbidden!', REST_Controller::HTTP_FORBIDDEN);

    }
    public function index_post(){

        $this->response('Forbidden!', REST_Controller::HTTP_FORBIDDEN);

    }

}
