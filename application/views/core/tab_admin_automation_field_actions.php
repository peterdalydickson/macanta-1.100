<?php
if($actionType == 'TicketNumber')
{
    $fieldaction = '';
    $fieldaction .= '<fieldset class="macanta-query-fieldset connected-data-field-set field-values FieldActionFieldSet TicketNumber">
                <legend class="label-lg"><span class="chosen-connected-data-type"></span>'.$GroupName.' Field Values</legend>
                <div class="macanta-query-criteria-item">
                    <div class="macanta-query-criteria-item-link hide">
                        <div class="btn-group query-link-btn" role="group">
                            <a class="nb-btn nb-base-color "></a>
                        </div>
                    </div>

                    <div class="flex-row">
                        <div class="form-group col-half">
                            <label class="label-sm control-label " for="ConnectedData">
                            <span>Ticket Number:</span>
                            <input name="queryTicketNumber" class="form-control queryTicketNumber" data-required="no" value="'.$fields->queryTicketNumber.'" required>
                        </div>
                        <div class="form-group col-half">
                            <label class="label-sm control-label " for="ConnectedData">
                            <span>Prefix:</span>
                            <input name="queryTicketNumberPrefix" class="form-control queryTicketNumberPrefix" data-required="no"  value="'.$fields->queryTicketNumberPrefix.'">
                        </div>
                        <div class="form-group col-half">
                            <label class="label-sm control-label " for="ConnectedData">
                            <span>Result Field:</span>
                            <select name="queryCDFieldNameResult" class="form-control queryCDFieldName queryCDFieldNameResult" data-required="no" required >
                             
                            </select>
                        </div>
                    </div><!-- /.flex-row -->
                </div>
            </fieldset>';
}
else if($actionType == "Addition" || $actionType == "Substraction" || $actionType == "Multiplication" || $actionType == "Division")
{
    $fieldaction = '';
    $fieldaction .= '<fieldset class="macanta-query-fieldset connected-data-field-set field-values FieldActionFieldSet FieldsData">
                <legend class="label-lg"><span class="chosen-connected-data-type"></span>'.$GroupName.' Field Values</legend>
                <div class="macanta-query-criteria-item">
                    <div class="macanta-query-criteria-item-link hide">
                        <div class="btn-group query-link-btn" role="group">
                            <a class="nb-btn nb-base-color "></a>
                        </div>
                    </div>

                    <div class="flex-row">
                        <div class="form-group col-half">
                            <label class="label-sm control-label " for="ConnectedData">
                            <span>Field 1:</span>
                            <select name="queryCDFieldName1" class="form-control queryCDFieldName queryCDFieldName1" data-required="no"   required>
                                
                            </select>
                        </div>
                        <div class="form-group col-half">
                            <label class="label-sm control-label " for="ConnectedData">
                            <span>Field 2:</span>
                            <select name="queryCDFieldName2" class="form-control queryCDFieldName queryCDFieldName2" data-required="no"  required>

                            </select>
                        </div>
                        <div class="form-group col-half">
                            <label class="label-sm control-label " for="ConnectedData">
                            <span>Result Field:</span>
                            <select name="queryCDFieldNameResult" class="form-control queryCDFieldName queryCDFieldNameResult" data-required="no"   required>

                            </select>
                        </div>
                    </div><!-- /.flex-row -->
                </div>
            </fieldset>';
}
else if($actionType == "ActionCounter")
{
    $fieldaction = '';
    $fieldaction .= '<fieldset class="macanta-query-fieldset connected-data-field-set field-values FieldActionFieldSet TicketNumber">
                <legend class="label-lg"><span class="chosen-connected-data-type"></span>'.$GroupName.' Field Values</legend>
                <div class="macanta-query-criteria-item">
                    <div class="macanta-query-criteria-item-link hide">
                        <div class="btn-group query-link-btn" role="group">
                            <a class="nb-btn nb-base-color "></a>
                        </div>
                    </div>

                    <div class="flex-row">
                        <div class="form-group col-half">
                            <label class="label-sm control-label " for="ConnectedData">
                            <span>Result Field:</span>
                            <select name="queryCDFieldNameResult" class="form-control queryCDFieldName queryCDFieldNameResult" data-required="no" required >
                             
                            </select>
                        </div>
                    </div><!-- /.flex-row -->
                </div>
            </fieldset>';
}
else if($actionType == "HOWOLD/HOWLONG")
{
    $fieldaction = '';
    $fieldaction .= '<fieldset class="macanta-query-fieldset connected-data-field-set field-values FieldActionFieldSet TicketNumber">
                <legend class="label-lg"><span class="chosen-connected-data-type"></span>'.$GroupName.' Field Values</legend>
                <div class="macanta-query-criteria-item">
                    <div class="macanta-query-criteria-item-link hide">
                        <div class="btn-group query-link-btn" role="group">
                            <a class="nb-btn nb-base-color "></a>
                        </div>
                    </div>

                    <div class="flex-row">
                        <div class="form-group col-half">
                            <label class="label-sm control-label " for="ConnectedData">
                            <span>Input Field:</span>
                            <select name="queryCDFieldName1" class="form-control queryCDFieldName queryCDFieldName1" data-required="no" required >
                             
                            </select>
                        </div>
                        <div class="form-group col-half">
                            <label class="label-sm control-label " for="ConnectedData">
                            <span>Formatting:</span>
                            <select name="queryFormatting" class="form-control queryFormatting" data-required="no" required >
                             <option value="0">Select Formatting</option>
                             <option value="Years">Years</option>
                             <option value="Years and Months">Years and Months</option>
                             <option value="Years, Months and Days">Years, Months and Days</option>
                             <option value="Days">Days</option>
                           </select>
                        </div>
                        <div class="form-group col-half">
                            <label class="label-sm control-label " for="ConnectedData">
                            <span>Result Field:</span>
                            <select name="queryCDFieldNameResult" class="form-control queryCDFieldName queryCDFieldNameResult" data-required="no" required >
                             
                            </select>
                        </div>
                    </div><!-- /.flex-row -->
                </div>
            </fieldset>';
}
echo $fieldaction;
