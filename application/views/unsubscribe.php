<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>macanta | Unsubscribe</title>
    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="pragma" content="no-cache">

    <meta content="macanta | Unsubscribe" property="og:title">
    <meta content="crm as unique as your business" name="description">
    <meta content="Macanta" property="og:site_name">
    <meta content="product" property="og:type">
    <meta content="https://staging.macantacrm.com/" property="og:url">
    <meta content="https://staging.macantacrm.com/assets/img/macantaicon.png" property="og:image">
    <meta content="https://staging.macantacrm.com/assets/img/macantaicon.png" name="twitter:image">
    <link rel="mask-icon" href="https://staging.macantacrm.com/assets/img/macantaicon.png" color="#7BBE4A">

    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/floating-labels/">

    <!-- Bootstrap core CSS -->
    <link type="text/css"  href="https://getbootstrap.com/docs/4.4/dist/css/bootstrap.min.css" rel="stylesheet"  crossorigin="anonymous">
    <link type="text/css"  href="https://getbootstrap.com/docs/4.4/examples/floating-labels/floating-labels.css" rel="stylesheet"  crossorigin="anonymous">
    <style>
        form.form-signin {
            background: #FFF;
        }
        h1.h3.mb-3.font-weight-normal {
            color: #663399;
        }
        p {
            color: #663399;
        }
        p.mt-5.mb-3.text-muted.text-center {
            font-size: 12px;
            color: #663399 !important;
        }
        .form-signin {
            width: 100%;
            max-width: 640px;
            padding: 15px;
            margin: auto;
        }
        a.links {
            color: #663399;
        }
        img.loginLogoImg {
            max-width: 150px;
            max-height: 150px;
        }
    </style>
</head>

<body>

<form class="form-signin">
    <div class="text-center mb-4">
        <?php
        $SiteLogo = macanta_db_record_exist('key', 'macanta_custom_logo', 'config_data', true);
        if ($SiteLogo) {
            $SiteLogoArr = json_decode($SiteLogo->value)
            ?>
            <img class="loginLogoImg"
                 src="data:<?php echo $SiteLogoArr->type; ?>;base64,<?php echo $SiteLogoArr->imageData; ?>" style=" "
                 alt="">
        <?php } else { ?>
            <img class="loginLogoImg" src="<?php echo $this->config->item('base_url'); ?>assets/img/Macanta-Logo_Colour.png"
                 style=" " alt="">
        <?php }
        ?>
        <h1 class="h3 mb-3 font-weight-normal"><?php echo $Header;?></h1>
        <p><?php echo $Message;?></p>
    </div>
    <p class="mt-5 mb-3 text-muted text-center"><a href="https://www.macanta.org/" class="links">Macanta Software Limited</a></p>
</form>

</body>

</html>