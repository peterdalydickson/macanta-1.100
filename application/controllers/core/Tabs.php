<?php
/**
 * Created by PhpStorm.
 * User: geover
 * Date: 07/03/16
 * Time: 3:00 PM
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/Inflect.php';

class Tabs extends MY_Controller
{
    //protected $local_stylesheets = array('mystylesheet.css');
    public $content = array();
    public $ajaxResults = array(
        "status" => 0, // true or false
        "message" => "",// any massage
        "data" => array(), // returned data
        "script" => '' // javascript need to execute // returned data
    );
    public $TabMenuName;
    private $MacantaIgniter;

    public function __construct()
    {
        parent::__construct();
        $this->TabMenuName = array(
            'Call' => $this->lang->line('text_call'),
            'Note' => $this->lang->line('text_note')
        );

    }

    public function index($Data)
    {
        set_time_limit(0);
        //ini_set('display_errors', 1);
        //error_reporting(E_WARNING);
        ini_set('memory_limit', '2048M');
        $WriteLog = false;
        $CurrentDir = dirname(__FILE__) . "/";
        $TimeStarted = time();
        $ToRecord = '';
        $ToRecordHeader =  'Time Started: ' . date('Y-m-d H:i:s', $TimeStarted) . "\n----\n";
        $allowedTabs = $Data['Items'];
        $session_name = $Data['session_name'];
        $user_seession_data = macanta_get_user_seession_data($session_name);
        $session_data = unserialize($user_seession_data->session_data);
        $CurrentMacantaUser = macanta_get_user_access_by_id($session_data['InfusionsoftID']);
        $CurrentMacantaUserLevel = $CurrentMacantaUser['Level'];
        $CurrentMacantaUserAccess = $CurrentMacantaUser['Access'];
        $Prams['Id'] = $Data['Id'];
        //Get Contact Details to be available for entire tabs!!
        $ContactInfo = infusionsoft_get_contact_by_id($Data['Id'], array('all'),false,$session_name);
        $TimeEnded = time();
        $TimeLapse = $TimeEnded - $TimeStarted;
        $ToRecordContactGet = $ToRecordHeader."ContactGet Time Lapse\n";
        $ToRecordContactGet .= "ContactGet: " . $TimeLapse . "\n----\n";
        if($WriteLog)
            file_put_contents($CurrentDir . "_TabsTimeLapse_".$this->config->item('MacantaAppName').".txt", $ToRecordContactGet , FILE_APPEND);

        $TimeStarted = time();
        //Make a javascript variable of each contact info
        $ContactJSVars = '';
        foreach ($ContactInfo as $FieldName => $FieldValue) {
            $ContactJSVars .= "ContactInfo['{$FieldName}'] = " . json_encode($FieldValue) . ";";
        }
        $TabMenu = array();
        $TabContent = array();
        $this->MacantaIgniter =& get_instance();
        if($this->config->item('MacantaAppName') == 'tr410' || $this->config->item('MacantaAppName') != ''){
            $TabName = isset($this->TabMenuName['Notes']) ? $this->TabMenuName['Notes'] : 'Notes';
            $TabMenu[$TabName] = array('icon' => 'fa fa-pencil', 'active' => true);
            $loadedController = _loadMacantaController($this->MacantaIgniter, "core/tabs/note", $ContactInfo);
            if (isset($this->$loadedController)) {
                if (method_exists($this->$loadedController, 'loadNoteBtn')) {
                    try{
                        $Content = $this->$loadedController->loadNoteBtn($session_name);
                        //echo "ok ".$Content." ok ";
                        $TabContent[$TabName][] = $Content;
                        //$TabContent[$TabName][] = "Test";
                    } catch (Exception $e) {
                        $Error = $e->getMessage();
                        $TabContent[$TabName][] = $Error;
                    }

                } else {
                    $TabContent[$TabName][] = 'No "loadNoteBtn" Method Existing in ' . $loadedController . "(core/tabs/note) Controller";
                }
            } else {
                $TabContent[$TabName][] = $loadedController;
            }
        }
        else{
            $AllTabs = json_decode($this->config->item('macanta_tabs'), true);
            foreach ($AllTabs as $Tab => $Params) {
                if (!in_array('all', $allowedTabs)) {
                    if (!in_array($Tab, $allowedTabs)) continue;
                }
                $TabName = isset($this->TabMenuName[$Tab]) ? $this->TabMenuName[$Tab] : $Tab;
                $TabMenu[$TabName] = array('icon' => $Params['tab_icon'], 'active' => $Params['active']);
                foreach ($Params['controllers'] as $Controller => $Methods) {
                    $loadedController = _loadMacantaController($this->MacantaIgniter, $Controller, $ContactInfo);
                    if (isset($this->$loadedController)) {
                        foreach ($Methods as $Method => $Items) {
                            if (method_exists($this->$loadedController, $Method)) {
                                try{
                                    $TabContent[$TabName][] = $this->$loadedController->$Method($Items, $session_name);
                                    //$TabContent[$TabName][] = "Test";
                                } catch (Exception $e) {
                                    $Error = $e->getMessage();
                                    $TabContent[$TabName][] = $Error;
                                }

                            } else {
                                $TabContent[$TabName][] = 'No "' . $Method . '" Method Existing in ' . $loadedController . "(" . $Controller . ") Controller";
                            }
                        }
                    } else {
                        $TabContent[$TabName][] = $loadedController;
                    }

                }
            }
        }

        $TimeEnded = time();
        $TimeLapse = $TimeEnded - $TimeStarted;
        $ToRecordContactNotes = $ToRecordHeader."ContactNotes Time Lapse\n";
        $ToRecordContactNotes .= "ContactNotes: " . $TimeLapse . "\n----\n";
        if($WriteLog)
            file_put_contents($CurrentDir . "_TabsTimeLapse_".$this->config->item('MacantaAppName').".txt", $ToRecordContactNotes , FILE_APPEND);

        $UserTabAccess = json_decode($CurrentMacantaUserAccess->TabAccess, true);
        /*For connector tab contents*/
        $ConnectorTabsEncoded = macanta_get_config('connected_info');
        if ($ConnectorTabsEncoded) {
            $ToRecordConnectorTab = "";

            if($WriteLog)
                file_put_contents($CurrentDir . "_TabsTimeLapse_".$this->config->item('MacantaAppName').".txt", "ConnectorTab Time Lapse\n", FILE_APPEND);

            $ConnectorTabs = json_decode($ConnectorTabsEncoded, true);

            foreach ($ConnectorTabs as $key => $ConnectorTab) {
                $TimeStarted = time();
                $icon = 'fa fa-sitemap';
                $ConnectorAccess = $UserTabAccess[$ConnectorTab['id']];
                if(!empty($ConnectorAccess)){
                    if($ConnectorAccess == 'GlobalAccess'){
                        if (isset($ConnectorTab['visibility'])) {
                            if ($ConnectorTab['visibility'] !== "ShowCDToAll") {
                                $icon = 'fa fa-info-circle';
                                if ($CurrentMacantaUserLevel !== "administrator") {
                                    $tagsArr = explode(',', $session_data['Groups']);
                                    if ($ConnectorTab['visibility'] === "ShowCDToAllUserSpecific") {
                                        if (!in_array($ConnectorTab['permission_tag'], $tagsArr)) continue;
                                    }
                                }

                            }
                        }
                    }else{

                    }
                }else{
                    if($CurrentMacantaUserLevel != 'administrator') continue;
                }
                $TabMenu[$ConnectorTab['title']] = array('icon' => $icon, 'active' => false, 'data-groupid'=>$ConnectorTab['id']);
                $ParamsTemp['GUID'] = $ConnectorTab['id'];
                $ParamsTemp['CDTitle'] = $ConnectorTab['title'];
                $ParamsTemp['ContactId'] = $Data['Id'];
                $ParamsTemp['ContactInfo'] = $ContactInfo;
                $ParamsTemp['GlobalClass'] = $ConnectorAccess;
                $TabContent[$ConnectorTab['title']][] = $this->load->view('core/tab_connected_info_lazy', $ParamsTemp, true);
                $TimeEnded = time();
                $TimeLapse = $TimeEnded - $TimeStarted;
                $ToRecordConnectorTab   = $ConnectorTab['title']." Lazy: " . $TimeLapse;
                if($WriteLog)
                    file_put_contents($CurrentDir . "_TabsTimeLapse_".$this->config->item('MacantaAppName').".txt", $ToRecordConnectorTab . "\n", FILE_APPEND);

            }
        }

        //==============================//

        /*For custom tab contents*/
        $TimeStarted = time();

        $CustomTabsEncoded = macanta_get_config('custom_tabs');
        $CustomTabsOffTagId = $this->config->item('custom_tabs_tag');
        $CustomTabOnTagId = $this->config->item('custom_tabs_on_tag');
        $CustomTabOnTagId = $CustomTabOnTagId ? trim($CustomTabOnTagId) : "";
        if (isset($ContactInfo->Groups) && trim($ContactInfo->Groups) != '') {
            $tagsArrSearchedContact = explode(',', $ContactInfo->Groups);

        } else {
            $tagsArrSearchedContact = [];
        }

        if ($CustomTabsEncoded) {
            $CustomTabs = json_decode($CustomTabsEncoded, true);
            if (sizeof($CustomTabs) > 0) {
                foreach ($CustomTabs as $CustomTab) {
                    $CustomTabAccess = $UserTabAccess[$CustomTab['id']];
                    if(!empty($CustomTabAccess)){
                        if($CustomTabAccess == 'GlobalAccess'){
                            if ($CustomTabsOffTagId) {
                                if (in_array($CustomTabsOffTagId, $tagsArrSearchedContact) && $CurrentMacantaUserLevel != "administrator") {
                                    if ($CustomTabOnTagId != '') {
                                        $CustomTabOnTagIdArr = explode(',', $CustomTabOnTagId);
                                        if (in_array($CustomTab['permission_tag'], $CustomTabOnTagIdArr)) {
                                            if (!in_array($CustomTab['permission_tag'], $tagsArrSearchedContact)) continue;
                                        }
                                    } else {
                                        continue;
                                    }

                                }
                            }
                            if ($CustomTab['permission'] == "1" && $CurrentMacantaUserLevel != "administrator") {
                                $tagsArr = explode(',', $session_data['Groups']);


                                if (!in_array($CustomTab['permission_tag'], $tagsArr)) continue;

                                if ($CustomTab['permission_all'] == "0") {
                                    if (!in_array($CustomTab['permission_tag'], $tagsArrSearchedContact)) continue;
                                }
                            }
                            if ($CustomTab['permission'] !== "ShowToAll" && $CurrentMacantaUserLevel !== "administrator") {

                                $tagsArr = explode(',', $session_data['Groups']);
                                $tagsArrSearchedContact = explode(',', $ContactInfo->Groups);

                                if ($CustomTab['permission'] === "ShowToAll_UserSpecific") {
                                    if (!in_array($CustomTab['permission_tag'], $tagsArr)) continue;
                                }
                                if ($CustomTab['permission'] === "ContactUserSpecific") {
                                    if (!in_array($CustomTab['permission_tag'], $tagsArr)) continue;
                                    if (!in_array($CustomTab['permission_tag'], $tagsArrSearchedContact)) continue;
                                }
                                if ($CustomTab['permission'] === "ContactHaveTag") {
                                    if (in_array($CustomTab['permission_tag'], $tagsArrSearchedContact)) continue;
                                }
                            }
                        }
                    }else{
                        if($CurrentMacantaUserLevel != 'administrator') continue;
                    }

                    $TabName = $CustomTab['title'];
                    $icon = 'fa fa-user-circle';

                    if (isset($CustomTab['global']) && $CustomTab['global'] == 'yes') $icon = 'fa fa-globe';
                    $TabMenu[$TabName] = array('icon' => $icon, 'active' => false);
                    $ParamsTemp['title'] = $TabName;
                    $ParamsTemp['contentid'] = $CustomTab['id'];
                    $ParamsTemp['GlobalClass'] = $CustomTabAccess;
                    /*// DISABLE FOR LAZY LOADING
                    $TempContent = str_replace('<p></p>','',base64_decode($CustomTab['content']));
                    $TempContent = handleMergedFields($ContactInfo->Id, $TempContent,$session_name);
                    $ParamsTemp['content'] = handleShortcodes($TempContent, $shortcodes,$session_data,$ContactInfo);*/

                    $TabContent[$TabName][] = $this->load->view('core/tab_custom_lazy', $ParamsTemp, true);
                }
            }
        }
        $TimeEnded = time();
        $TimeLapse = $TimeEnded - $TimeStarted;
        $ToRecord .= "----\nCustomTab Time Lapse\n"."CustomTab: " . $TimeLapse . "\n";
        //==============================//
        //========== ADMIN AREA =========//
        $TimeStarted = time();
        $ActivateAdminTabScript = '';
        if (isset($CurrentMacantaUserLevel) && $CurrentMacantaUserLevel == 'administrator') {
            $ActivateAdminTabScript = "if(ToAdmin()) $('.nav-tabs a[href=#Admin]').tab('show'); ";
            if(isset($_GET['tab'])){
                $ActivateAdminTabScript = "$('.nav-tabs a[href=#".$_GET['tab']."]').tab('show'); ";
            }
            $TabMenu['Admin'] = array('icon' => 'fa fa-wrench', 'active' => false);
            $ParamsTemp['title'] = 'Admin';
            $ParamsTemp['contentid'] = 'macanta-admin';
            $TabContent['Admin'][] = $this->load->view('core/tab_admin_lazy', $ParamsTemp, true);


            /*// DISABLE FOR LAZY LOADING
            $AdminController = _loadMacantaController($this->MacantaIgniter, "core/tabs/admin", $ContactInfo);
            $AdminMethods = array("index"=>"all");
            foreach($AdminMethods as $Method => $Items){
                if(method_exists($this->$AdminController, $Method)){
                    $TabContent['Admin'][] = $this->$AdminController->$Method($Items);
                }else{
                    $TabContent['Admin'][] = 'No "'.$Method .'" Method Existing in '. $AdminController . "(core/tabs/admin) Controller";
                }
            }*/
        }
        $TimeEnded = time();
        $TimeLapse = $TimeEnded - $TimeStarted;
        $ToRecord .= "----\nAdminTab Time Lapse\n"."AdminTab: " . $TimeLapse . "\n";
        if($WriteLog)
            file_put_contents($CurrentDir . "_TabsTimeLapse_".$this->config->item('MacantaAppName').".txt", $ToRecord . "=================\n", FILE_APPEND);
        //==============================//
        $ConnectorRelationship = $this->config->item('ConnectorRelationship') ? json_decode($this->config->item('ConnectorRelationship')) : [];
        $ConnectorFields = macanta_get_config('connected_info');
        $ConnectorFields = $ConnectorFields ? json_decode($ConnectorFields, true) : [];
        $RelationshipRules = [];
        foreach ($ConnectorFields as $FieldGroups) {
            $RelationshipRules[$FieldGroups['id']] = $FieldGroups['relationships'];
        }
        $RelationshipRules = json_encode($RelationshipRules);
        $availableRelationships = [];
        foreach ($ConnectorRelationship as $RelationshipItem) {
            $availableRelationships[] = $RelationshipItem->RelationshipName;
        }

        $availableRelationships = json_encode($availableRelationships);
        $Prams['menu'] = $TabMenu;
        $Prams['content'] = $TabContent;
        $Tabs = $this->load->view('core/tab_index', $Prams, true);
        $availableTags = $this->notetags_get();
        $this->ajaxResults['data'] = $Tabs;
        $this->ajaxResults['message'] = "Tabs Displayed";
        $this->ajaxResults['script'] = 'noteCollapsible();' . $ActivateAdminTabScript . 'availableTags=' . $availableTags . ';relationshipRules=' . $RelationshipRules . ';availableRelationships=' . $availableRelationships . ';Macanta("Tab index");' . $ContactJSVars . '; toggleThisTaskInside();';
        $this->ajaxResults['status'] = 1;
        unset($CustomTabsEncoded);
        unset($ConnectorFields);
        unset($ConnectorTabsEncoded);
        return $this->ajaxResults;
    }

    public function notetags_get()
    {
        $tags = array();
        $tagsStr = array();
        $query = $this->db->get('tags');
        foreach ($query->result() as $row) {
            $tagsStr[] = $row->tag_name;
            $tags[] = array(
                'id' => $row->tag_slug,
                'label' => $row->tag_name,
                'value' => $row->tag_name
            );
        }
        //echo  json_encode($tags);
        return json_encode($tagsStr);
    }

    public function tab_admin($Data)
    {
        $session_name = $Data['session_name'];
        $this->MacantaIgniter =& get_instance();
        $ContactInfo = infusionsoft_get_contact_by_id($Data['ContactId'], array('all'));
        $AdminController = _loadMacantaController($this->MacantaIgniter, "core/tabs/admin", $ContactInfo);
        $AdminMethods = array("index" => "all");
        $Content = '';
        foreach ($AdminMethods as $Method => $Items) {
            if (method_exists($this->$AdminController, $Method)) {
                $Content .= $this->$AdminController->$Method($Items);
            } else {
                $Content .= 'No "' . $Method . '" Method Existing in ' . $AdminController . "(core/tabs/admin) Controller";
            }
        }

        $this->ajaxResults['data'] = $Content;
        $this->ajaxResults['message'] = $Data['title'];
        $this->ajaxResults['script'] = '';
        $this->ajaxResults['status'] = 1;
        return $this->ajaxResults;
    }
    public function tab_cd($Data)
    {
        ini_set('memory_limit', '2048M');
        $WriteLog = $this->config->item('MacantaAppName') == 'no' ? true:false;;
        $CurrentDir = dirname(__FILE__) . "/";
        $ToRecord = "=================\nTAB CD Time Lapse ".date("Y-m-d H:i:s")."\n";
        $Content='';
        $TimeStarted = time();
        $ContactInfo = infusionsoft_get_contact_by_id($Data['ContactId'], array('all'));
        $TimeEnded = time();
        $TimeLapse = $TimeEnded - $TimeStarted;
        $ToRecord .= "ContactInfo: " . $TimeLapse . "\n";

        $ConnectorTabsEncoded = macanta_get_config('connected_info');
        $session_name = $Data['session_name'];
        $TimeStarted = time();
        $user_seession_data = macanta_get_user_seession_data($session_name);
        $session_data = unserialize($user_seession_data->session_data);
        $TimeEnded = time();
        $TimeLapse = $TimeEnded - $TimeStarted;
        $ToStoreScript = '';
        $ToRecord .= "Get seession_data: " . $TimeLapse . "\n";
        if ($ConnectorTabsEncoded) {
            $TimeStarted = time();
            $ConnectorTabs = json_decode($ConnectorTabsEncoded, true);
            $TimeEnded = time();
            $TimeLapse = $TimeEnded - $TimeStarted;
            $ToRecord .= "ConnectorTabs decode: " . $TimeLapse . "\n";
            foreach ($ConnectorTabs as $key => $ConnectorTab) {
                if($ConnectorTab['title'] == $Data['title']){
                    $TimeStarted = time();
                    $UserConnectedInfo = macanta_get_connected_info($Data['ContactId'],$ConnectorTab['id']);
                    $TimeEnded = time();
                    $TimeLapse = $TimeEnded - $TimeStarted;
                    $ToRecord .= "Get_ConnectedInfo $Data[title]: " . $TimeLapse . "\n";
                    $ParamsTemp['title'] = $ConnectorTab['title'];
                    $TimeStarted = time();
                    $ParamsTemp['titleSingular'] = Inflect::singularize($ConnectorTab['title']);
                    $TimeEnded = time();
                    $TimeLapse = $TimeEnded - $TimeStarted;
                    $ToRecord .= "Singularize: " . $TimeLapse . "\n";
                    $ParamsTemp['ConnectorTab'] = $ConnectorTab;
                    $ParamsTemp['GUID'] = $ConnectorTab['id'];
                    $ParamsTemp['ContactInfo'] = $ContactInfo;
                    $ParamsTemp['session_data'] = $session_data;
                    $ParamsTemp['UserValue'] = $UserValue =  isset($UserConnectedInfo[$ConnectorTab['id']]) ? $UserConnectedInfo[$ConnectorTab['id']] : [];
                    $view = 'core/tab_connected_info_new';
                    $TimeStarted = time();
                    $Content = $this->load->view($view, $ParamsTemp, true);
                    $TimeEnded = time();
                    $TimeLapse = $TimeEnded - $TimeStarted;
                    $ToRecord .= "LoadView  $view: " . $TimeLapse . "\n";
                    if(sizeof($UserValue) == 0){
                        $stringify = "{}";
                    }else{
                        $stringify = json_encode($UserValue);
                    }
                    $ToStoreScript = 'localStorage.setItem("'.$ConnectorTab['id'].'", JSON.stringify('.$stringify.'));';
                    break;
                }
            }
        }
        $this->ajaxResults['data'] = $Content;
        $this->ajaxResults['message'] = $Data['title'];
        $this->ajaxResults['script'] = $ToStoreScript;
        $this->ajaxResults['other'] = $ContactInfo;
        $this->ajaxResults['status'] = 1;
        if($WriteLog)
            file_put_contents($CurrentDir . "_TabsTabCDTimeLapse_".$this->config->item('MacantaAppName').".txt", $ToRecord . "=================\n", FILE_APPEND);

        return $this->ajaxResults;
    }
    public function load_cd_section($Data){
        $container = $Data['container'];
        $title = $Data['title'];
        $ContactId = $Data['ContactId'];
        $session_name = $Data['session_name'];
        $StrTemp = explode("-_-",$container);
        $GUID =  end($StrTemp);
        $ConnectorTabsEncoded = macanta_get_config('connected_info');
        $user_seession_data = macanta_get_user_seession_data($session_name);
        $session_data = unserialize($user_seession_data->session_data);
        if ($ConnectorTabsEncoded) {
            $ConnectorTabs = json_decode($ConnectorTabsEncoded, true);
            if(isset($ConnectorTabs[$GUID])){
                $ContactInfo = infusionsoft_get_contact_by_id($ContactId, array('all'));
                $ConnectorTab = $ConnectorTabs[$GUID];
                $ParamsTemp['title'] = $ConnectorTab['title'];
                $ParamsTemp['SectionTitle'] = $title;
                $ParamsTemp['ConnectorTab'] = $ConnectorTab;
                $ParamsTemp['GUID'] = $ConnectorTab['id'];
                $ParamsTemp['ContactInfo'] = $ContactInfo;
                $ParamsTemp['session_data'] = $session_data;
                $view = 'core/tab_connected_info_sections';
                $Content = $this->load->view($view, $ParamsTemp, true);
            }else{
                $Content = 'Error Loading, No CD with the ID: '.$GUID;
            }
        }else{
            $Content = 'Error Loading, No CD Setup!';
        }
        $this->ajaxResults['data'] = $Content;
        $this->ajaxResults['message'] = $Data['title'];
        $Script = 'var theForm = $("form.FormUserConnectedInfo[data-guid='.$ConnectorTab['id'].']");
        if($("fieldset.cd-fields",theForm).hasClass("lightYellow")){
        $("[data-jsonvalue]", theForm).each(function () {
                $(this).attr("data-jsonvalue", "{}");
            });

            $("select.addGroup", theForm).each(function () {
                var defaultValue = $(this).attr(\'data-default\');
                $(this).val(defaultValue);
            });
        $(".addGroup", theForm).removeAttr("disabled");
        $("div.form-group", theForm).first().find("input").focus();
        }';
        $this->ajaxResults['script'] = $Script;
        $this->ajaxResults['other'] = $ContactId;
        $this->ajaxResults['status'] = 1;
        return $this->ajaxResults;
    }
    public function tagsChangeOrder($Data){

        $TagsInfo = infusionsoft_get_tags_applied($Data['ContactId']);
        $ReadOnly = $Data['ReadOnly'];
        $OrderBy = $Data['CurrentOrder'] === 'Tag Name' ? 'date':'name';
        $OrderedTagsInfo = [];
        if($OrderBy !== 'name'){
            foreach ($TagsInfo as $TagId=>$TagDetails){
                $DateCreated = json_decode($TagDetails['DateCreated'], true);
                $OrderedTagsInfo[strtotime($DateCreated['date'])] = ['Id' =>$TagId,'Name'=>$TagDetails['Name']];
            }
            ksort($OrderedTagsInfo);
        }
        $Disabled = $ReadOnly === 'no' ? "" : "disabled";
        $randStr = generateRandomString(5);
        $Tags = infusionsoft_get_tags_by_catId($Data['CatId'], 'GroupName');
        usort($Tags, "sort_obj_arr_tag_categories");
        //if(!isset($ContactInfo['Groups'])) $ContactInfo['Groups'] = '';
        $AppliedTags = explode(',', $Data['ContactGroups']);
        $class = 'Tags' . $randStr;
        $content = '<select  multiple="multiple"  class="ContactTags ' . $class . '" id="' . $class . '" name="ContactTags[]" ' . $Disabled . '> ';
        if($OrderBy !== 'name') {
            foreach ($OrderedTagsInfo as $DateCreated => $TagDetails) {
                foreach ($Tags as $Tag) {
                    if($Tag->Id != $TagDetails['Id']) continue;
                    if(in_array($TagDetails['Id'], $AppliedTags)){
                        $TheDate = "Date Applied: " . date('Y-m-d',$DateCreated);
                        $content .= ' <option data-date="' . $TheDate . '" value="' . $TagDetails['Id'] . '" selected><span>' . $TagDetails['Name'] . ' </span> </option>';

                    }
                }


            }
        }
        foreach ($Tags as $Tag) {
            $DateCreated = json_decode($TagsInfo[$Tag->Id]['DateCreated']);
            if(in_array($Tag->Id, $AppliedTags)){
                if($OrderBy !== 'name') continue;
                $selected = 'selected';
                $TheDate = "Date Applied: ".date('Y-m-d', strtotime($DateCreated->date));
            }else{
                $TheDate = "Date Applied: ".date('Y-m-d');
                $selected = '';
            }

            $content .= ' <option data-date="'.$TheDate.'" value="' . $Tag->Id . '" ' . $selected . '><span>'.$Tag->GroupName.' </span> </option>';
        }
        $content .= ' </select>
                        <script>
                        initMultiSelect("' . $class . '");
                        </script>
                         </div>';



        $this->ajaxResults['data'] = $content;
        $this->ajaxResults['message'] = '';
        $this->ajaxResults['script'] = '';
        $this->ajaxResults['status'] = 1;
        return $this->ajaxResults;
    }
    public function tab_custom($Data)
    {
        $session_name = $Data['session_name'];
        $user_seession_data = macanta_get_user_seession_data($session_name);
        $session_data = unserialize($user_seession_data->session_data);
        $CustomTabsEncoded = macanta_get_config('custom_tabs');
        $CustomTabs = json_decode($CustomTabsEncoded, true);
        $ContactInfo = infusionsoft_get_contact_by_id($Data['ContactId'], array('all'));
        $Content = '';
        $shortcodes = array(
            "ISwebform" => function ($data, $session_data, $ContactInfo) {
                $content = "";
                //Calculate the age
                $randStr = generateRandomString(5);
                if (!isset($data["readonly"])) {
                    $data["readonly"] = 'false';
                }
                if (!isset($data["createnote"])) {
                    $data["createnote"] = 'no';
                }
                if (!isset($data["assign_note_to_contact_owner"])) {
                    $data["assign_note_to_contact_owner"] = 'no';
                }
                if (!isset($data["submitted_by_customfield"])) {
                    $data["submitted_by_customfield"] = '';
                }
                $readonly = trim($data["readonly"]);
                $createnote = trim($data["createnote"]);
                $assign_note_to_contact_owner = trim($data["assign_note_to_contact_owner"]);
                $submitted_by_customfield = trim($data["submitted_by_customfield"]);
                if (isset($data["formid"])) {
                    //process form here
                    $formid = trim($data["formid"]);
                    $class = 'webform' . $randStr;
                    $appname = $this->config->item('MacantaAppName');
                    $content = '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 web-form-panel">
                    <div class="panel panel-default container' . $class . '">
                      <div class="panel-heading webformPanelTitle"><h3 class="panel-title ' . $class . '"></h3></div>
                      <div class="webformPanelBody panel-body ' . $class . '">
                      </div>
                    </div><script>
                    _getWebform("' . $class . '", "' . $appname . '", "' . $formid . '", "' . $readonly . '", "' . $createnote . '", "' . $assign_note_to_contact_owner . '", "' . $submitted_by_customfield . '");
                    </script>
                    </div>';
                } else {
                    $content = 'ISwebform Shortcode Error: Invalid or missing parameters';
                }
                return $content;
            },
            "EmailForm" => function ($data, $session_data, $ContactInfo) {
                $randStr = generateRandomString(5);
                $class = 'EmailForm' . $randStr;
                if (!isset($data["createnote"])) {
                    $data["createnote"] = 'no';
                }
                $createnote = trim($data["createnote"]);
                $content = '<div class="col-md-12">
                    <div class="panel panel-default container' . $class . '">
                      <div class="panel-heading EmailFormPanelTitle"><h3 class="panel-title ' . $class . '">Compose Email</h3></div>
                      <div class="EmailFormPanelBody panel-body ' . $class . '">
                      <div class="form-group  col-md-6">
                        <label for="subject">Subject:</label>
                        <input type="text" class="form-control subject" name="subject">
                      </div>
                      <div class="form-group  col-md-6">
                        <label for="subject">BCC:</label>
                        <input type="text" class="form-control bcc" name="bcc" value="' . $session_data['Email'] . '">
                      </div>
                       <div class="form-group col-md-12">
                        <label for="message">Message:</label>
                        <textarea id="' . $class . '" name="' . $class . '" class="form-control EmailForm TextEditor" placeholder="Text Here">
                        <p></p>
                        <p></p>
                        '.
                        macanta_get_signature($session_data['InfusionsoftID'])
                        .'</textarea>
                       </div>
                       <div class="form-group col-md-12">
                       <button  type="button" value="Send" class="btn btn-default" onclick="sendISEmail(\'' . $class . '\',\'' . $createnote . '\');">Send Email</button>
                       </div>
                      </div>
                    </div>
                     <script>
                        initTinymceById("' . $class . '");
                        </script>
                    </div>
                    ';

                return $content;
            },
            "EmailHistoryBack" => function ($data, $session_data, $ContactInfo) {
                $randStr = generateRandomString(5);
                $GetMore = true;
                $TotalHistory = [];
                $Offset = 0;
                $Limit = 1000;
                while($GetMore == true ){
                    $EmailHistory = infusionsoft_get_email_history($ContactInfo->Id,$Limit,$Offset);
                    if(sizeof($EmailHistory->emails)<1000) $GetMore = false;
                    $TotalHistory = array_merge($TotalHistory,$EmailHistory->emails);
                    $Offset = $Offset + $Limit;
                }
                $class = 'EmailHistory' . $randStr;
                $tbody = '<!--'.print_r($TotalHistory,true).'-->';
                foreach ($TotalHistory as $Email) {
                    if ($Email->sent_to_address == 'geover@gmail.com' && strpos($Email->subject, 'Your Conquer The Chaos Ltd receipt') !== false) continue;
                    $opened_date = $Email->opened_date ? date('d M Y', strtotime($Email->opened_date)):"";
                    $tbody .= '
                            <tr data-historyid="' . $Email->id . '">
                            <td>' . $Email->subject . '</td>
                            <td>' . $Email->sent_to_address . '</td>
                            <td>' . $Email->sent_from_address . '</td>
                            <td data-original="'.$Email->sent_date.'">' . date('d M Y', strtotime($Email->sent_date)) . '</td>
                            <td data-original="'.@$Email->opened_date.'">' . $opened_date . '</td>
                            </tr>
                            ';

                }
                $content = '
                <div class="panel panel-primary right-LookAndFeel">
                    <div class="panel-heading">
                        <h3 class="panel-title "><i class="fa fa-envelope"></i> Email History </h3>
                    </div>
                    <div class="panel-body admin-panelBody">
                    <div class="table-responsive">
                    	<table id="EmailHistoryTable" class="table table-hover table-striped table-bordered" cellspacing="0" width="100%" style="width: 100%;">
                       <thead>
                       <tr>
                          <th>Subject</th>
                          <th>To:</th>
                          <th>From:</th>
                          <th>Date Sent</th>
                          <th>Date Opened</th>
                       </tr>
                       </thead>
                       <tbody>' . $tbody . '</tbody>
                   </table>
                    </div>
                    </div>
                    </div>
                    
                    
                   <script>
                    renderEmailHistory('.$ContactInfo->Id.');
                        </script>
                    ';

                return $content;
            },
            "EmailHistory" => function ($data, $session_data, $ContactInfo,$Data) {
                $content = '<div class="table-responsive">
                <div class="refreshEmailContainter">
                    <button class="refreshEmail nb-btn nb-secondary icon-btn" type="button" onclick="refreshEmail(\''.$Data['container'].'\',\''.$Data['title'].'\')"><i class="fa fa-retweet"></i>Refresh Emails</button>
                    </div>
                    	<table id="EmailHistoryTable" class="table table-hover table-striped table-bordered" cellspacing="0" width="100%" style="width: 100%;">
                       <thead>
                       <tr>
                          <th>Subject</th>
                          <th>To:</th>
                          <th>From:</th>
                          <th>Date Sent</th>
                          <th>Date Opened</th>
                       </tr>
                       </thead>
                       <tbody></tbody>
                   </table>
                    </div>
                    
                  
                   <script>
                    renderEmailHistory('.$ContactInfo->Id.');
                        </script>
                    ';

                return $content;
            },
            "EmailTemplate" => function ($data, $session_data, $ContactInfo) {
                $randStr = generateRandomString(5);
                $template = applyFn('infusionsoft_get_email_template_content', $data["id"]);;
                if (!isset($data["createnote"])) {
                    $data["createnote"] = 'no';
                }
                $createnote = trim($data["createnote"]);
                $class = 'EmailForm' . $randStr;
                if (!isset($template->pieceTitle)) {
                    $Alert = '<div class="col-md-12"><span class="col-md-12 alert alert-danger">Template [' . $data["id"] . '] Did not found or set private</span></div>';
                } else {
                    $Alert = '';
                }
                $content = $Alert . '<div class="col-md-12">
                    <div class="panel panel-default container' . $class . '">
                      <div class="panel-heading EmailFormPanelTitle"><h3 class="panel-title ' . $class . '">' . $template->pieceTitle . '</h3></div>
                      <div class="EmailFormPanelBody panel-body ' . $class . '">
                      <div class="form-group col-md-6">
                        <label for="subject">Subject:</label>
                        <input type="text" class="form-control subject"  name="subject" value="' . $template->subject . '">
                      </div>
                      <div class="form-group  col-md-6">
                        <label for="subject">BCC:</label>
                        <input type="text" class="form-control bcc" name="bcc" value="' . $session_data['Email'] . '">
                      </div>
                       <div class="form-group col-md-12">
                        <label for="message">Message:</label>
                        <textarea id="' . $class . '" name="' . $class . '" class="form-control TextEditor" placeholder="Text Here">
                        ' . $template->htmlBody . '
                        
                        
                        <p></p>
                        <p></p>
                        ' . macanta_get_signature($session_data['InfusionsoftID']) . '
                        </textarea>
                       </div>
                       <div class="form-group col-md-12">
                       <button  type="button" value="Send" class="btn btn-default" onclick="sendISEmail(\'' . $class . '\',\'' . $createnote . '\');">Send Email</button>
                       </div>
                      </div>
                    </div>
                     <script>
                        initTinymceById("' . $class . '");
                        </script>
                    </div>
                    ';
                return $content;
            },
            "TagCat" => function ($data, $session_data, $ContactInfo) {
                $TagsInfo = infusionsoft_get_tags_applied($ContactInfo->Id);
                $ReadOnly = isset($data["readonly"]) ? $data["readonly"] : 'no';
                $OrderBy = isset($data["order_by"]) ? $data["readonly"] : 'name';
                $OrderedTagsInfo = [];
                $OrderByIndsplay = 'Tag Name';
                if($OrderBy !== 'name'){
                    $OrderByIndsplay = 'Date Applied';
                    foreach ($TagsInfo as $TagId=>$TagDetails){
                        $DateCreated = json_decode($TagDetails['DateCreated'], true);
                        $OrderedTagsInfo[strtotime($DateCreated['date'])] = ['Id' =>$TagId,'Name'=>$TagDetails['Name']];
                    }
                    ksort($OrderedTagsInfo);
                }
                $Disabled = $ReadOnly === 'no' ? "" : "disabled";
                $randStr = generateRandomString(5);
                $Tags = infusionsoft_get_tags_by_catId($data["id"], 'GroupName');
                usort($Tags, "sort_obj_arr_tag_categories");
                $TagCategories = json_decode(manual_cache_loader('TagCategories'),true);
                //if(!isset($ContactInfo['Groups'])) $ContactInfo['Groups'] = '';
                $AppliedTags = explode(',', $ContactInfo->Groups);
                $class = 'Tags' . $randStr;
                $content = '<div class="">
                    
                    <small class="dev-only php">CONTENT LOCATION: application > controllers > core > Tabs.php</small>
                    <div class="panel panel-default container' . $class . ' ContacTagsPanelTitle">
                      <div class="panel-heading ">
                         <h3 class="panel-title ' . $class . '">' . $TagCategories[$data["id"]] . '</h3>
                         <small class="nb-btn nb-secondary icon-btn tagsChangeOrder" data-readonly="'.$ReadOnly.'"  data-currentorder="'.$OrderByIndsplay.'"  data-contactid="'.$ContactInfo->Id.'" data-catid="' . $data['id'] . '" ><i class="fa fa-sort"></i><strong>Ordered by: </strong><i>'.$OrderByIndsplay.'</i></small>
                      </div>
                      <div class="panel-body applied-tags-list-'.$data["id"].'"><select  multiple="multiple"  class="ContactTags ' . $class . '" id="' . $class . '" name="ContactTags[]" ' . $Disabled . '> ';
                if($OrderBy !== 'name') {
                    foreach ($OrderedTagsInfo as $DateCreated => $TagDetails) {
                        foreach ($Tags as $Tag) {
                            if($Tag->Id != $TagDetails['Id']) continue;
                            if(in_array($TagDetails['Id'], $AppliedTags)){
                                $TheDate = "Date Applied: " . date('Y-m-d',$DateCreated);
                                $content .= ' <option data-date="' . $TheDate . '" value="' . $TagDetails['Id'] . '" selected><span><strong>' . $TagDetails['Name'] . ' </strong></span> </option>';

                            }
                        }


                    }
                }
                foreach ($Tags as $Tag) {
                    $DateCreated = json_decode($TagsInfo[$Tag->Id]['DateCreated']);
                    if(in_array($Tag->Id, $AppliedTags)){
                        if($OrderBy !== 'name') continue;
                        $selected = 'selected';
                        $TheDate = "Date Applied: ".date('Y-m-d', strtotime($DateCreated->date));
                    }else{
                        $TheDate = "Date Applied: ".date('Y-m-d');
                        $selected = '';
                    }

                    $content .= ' <option data-date="'.$TheDate.'" value="' . $Tag->Id . '" ' . $selected . '><span>'.$Tag->GroupName.' </span> </option>';
                }
                $content .= ' </select></div></div>
                        <script>
                        initMultiSelect("' . $class . '");
                        </script>
                         </div>';
                return $content;
            },
            "ISopportunities" => function ($data, $session_data, $ContactInfo) {
                if (!isset($data["Only_Show_Opps_Assigned_To_Logged_In_User"])) {
                    $data["Only_Show_Opps_Assigned_To_Logged_In_User"] = 'no';
                }
                if (isset($data["only_show_opps_assigned_to_logged_in_user"])) {
                    $data["Only_Show_Opps_Assigned_To_Logged_In_User"] = $data["only_show_opps_assigned_to_logged_in_user"];
                }
                if (!isset($data["pipelines"])) {
                    $data["pipelines"] = 'all';
                }
                $UserOnly = $data["Only_Show_Opps_Assigned_To_Logged_In_User"];
                $Params['Opportunities'] = get_opportunities($ContactInfo->Id, $UserOnly, false, $session_data);
                $Params['Stages'] = get_opp_stages();
                $Params['AllowedPipeline'] = strtoupper($data["pipelines"]);
                $Params['ShortCodeId'] = strtolower(random_string('alpha', 5));
                $Tabs = $this->load->view('core/tab_opportunities', $Params, true);
                return $Tabs;
            },
            "FileBox" => function ($data, $session_data, $ContactInfo,$Data) {
                $randStr = generateRandomString(5);
                $class = 'filebox' . $randStr;
                $Files = infusionsoft_get_files($ContactInfo->Id);
                $Table = '<div class="refreshFileContainter">
                    <button class="refreshFile nb-btn nb-secondary icon-btn" type="button" onclick="refreshFileBox(\''.$Data['container'].'\',\''.$Data['title'].'\',\''.$ContactInfo->Id.'\')"><i class="fa fa-retweet"></i>Refresh Files</button>
                    </div>
                <table class="table table-hover FileBox ' . $class . '" width="100%;" style="width: 100%;">
                	<thead>
                		<tr>
                		<th>Id</th>
                		<th>Name</th>
                		<th>Upload Date</th>
                		<!--<th>Size</th>-->
                		<th>Download</th>
                		<!--<th>Date Uploaded</th>
                		<th>Date Updated</th>-->
                		</tr>
                	</thead>
                	<tbody>';
                foreach ($Files as $File) {
                    //$TheFile = infusionsoft_get_file_by_id_lite($File->Id)->message;
                    //$FileDetails = $TheFile->file_descriptor;
                    //$FileDataEncoded = $TheFile->file_data;
                    //$FileDataDecoded = base64_decode($FileDataEncoded);
                    //mkdir(FCPATH.'tmp/'.$ContactInfo->Id, 0755, true);
                    //$Path = FCPATH.'tmp/'.$ContactInfo->Id.'/'.$File->FileName;
                    //file_put_contents($Path, $FileDataDecoded);
                    /*
                     stdClass Object (  [id] => 1054
                                        [category] => Documents
                                        [date_created] => 2017-11-17T02:28:59.000+0000
                                        [last_updated] => 2017-11-17T02:28:59.000+0000
                                        [file_name] => MyAvatar.jpg
                                        [file_size] => 698463
                                        [created_by] => 1
                                        [public] =>
                                        [contact_id] => 364
                                        [remote_file_key] =>
                                        [file_box_type] => Application
                                        [download_url] => https://qj311.infusionsoft.com/Download?Id=1054 )
                    */
                    $Table .= '<tr>';
                    $Table .= '<td>' . $File->Id . '</td>';
                    $Table .= '<td>' . $File->FileName . '</td>';
                    $this->db->where('Id',$File->Id);
                    $query = $this->db->get('InfusionsoftFileBox');
                    $row = $query->row();
                    if (isset($row))
                    {
                        $FileDataObj = json_decode($row->FileData);
                        $DateCreated =  date("Y-m-d H:i:s", strtotime($FileDataObj->infusionsoft->date_created));
                    }
                    $Table .= '<td>' . $DateCreated. '</td>';
                    /*$Table .= '<td>'.formatBytes($File->FileSize).'</td>';*/
                    $Table .= '<td><a target="macanta" data-fileid="' . $File->Id . '" href="' . $this->config->item('base_url') . 'filebox/fileId/' . $File->Id . '">Download</a></td>';
                    //$Table .= '<td>'.date('d M Y',strtotime($FileDetails->date_created)).'</td>';
                    //$Table .= '<td>'.date('d M Y',strtotime($FileDetails->last_updated)).'</td>';
                    $Table .= '</tr>';
                }
                $Table .= '</tbody></table><script>makeDataTable(".' . $class . '")</script>';
                return $Table;
            }

        );
        foreach ($CustomTabs as $CustomTab) {
            if ($Data['title'] == $CustomTab['title']) {
                $TempContent = str_replace('<p></p>', '', base64_decode($CustomTab['content']));
                $TempContent = handleMergedFields($ContactInfo->Id, $TempContent, $session_name);
                $Content = handleShortcodes($TempContent, $shortcodes, $session_data, $ContactInfo,$Data);
            }

        }
        $this->ajaxResults['data'] = $Content;
        $this->ajaxResults['message'] = $Data['title'];
        $this->ajaxResults['script'] = 'verifyCustomTabPermission("'.$Data['container'].'");';
        $this->ajaxResults['other'] = $ContactInfo;
        $this->ajaxResults['status'] = 1;
        return $this->ajaxResults;
    }
    public function connector_csv_generate_download()
    {
        if($_POST['CurrentConnectorDownLoadType']!='')
        {
            $CurrentConnectorDownLoadData=json_decode(base64_decode($_POST['CurrentConnectorDownLoadType']));
        }

        //echo "<pre>";
        //print_r($_POST);
        //echo "</pre>";

        $CurrentConnectorDownLoadType=$_POST['CurrentConnectorDownLoadType'];
        $ConnectedInfosEncoded = macanta_get_config('connected_info');
        if (!$ConnectedInfosEncoded) $ConnectedInfosEncoded = [];
        $ConnectedInfos = json_decode($ConnectedInfosEncoded, true);

        $ConnectorRelationship = json_decode($this->config->item('ConnectorRelationship'));
        $RelationData=array();
        foreach($ConnectorRelationship as $data)
        {
            $RelationData[$data->Id]=$data->RelationshipName;
        }


        $ContactData=array();
        $ConnectedQuery=$this->db->query("select * from connected_data where `group`='$CurrentConnectorDownLoadType'");
        foreach($ConnectedQuery->result_array() as $ArrKey=>$tempData)
        {
            $connected_contact=json_decode($tempData['connected_contact']);
            $connected_value=json_decode($tempData['value']);
            //echo "<pre>";
            //print_r($connected_value);
            //echo "</pre>";


            foreach($connected_contact as $ContactId=>$ConnectedInfo)
            {
                if($ContactId!='undefined')
                {
                    $tmpData=array();
                    foreach($ConnectedInfo as $Key=>$data)
                    {
                        if(is_array($data))
                        {
                            $tmpData[$Key]=implode(',',$data);
                        }
                        else
                        {
                            if($data!='undefined')
                            {
                                $tmpData[$Key]=$data;
                            }
                        }
                    }
                    $ContactData[$ArrKey][]=$tmpData;
                }
            }

            foreach($ContactData[$ArrKey] as $Key=>$Contact)
            {
                foreach($connected_value as $FieldKey=>$data)
                {
                    if(is_object($data))
                    {
                        $tmpFieldName="id_".$Contact['ContactId'];
                        $ContactData[$ArrKey][$Key][$FieldKey]=$data->$tmpFieldName;
                    }
                    else
                    {
                        $ContactData[$ArrKey][$Key][$FieldKey]=$data;
                    }
                }
            }

        }

        $FinalContact=array();
        foreach($ContactData as $tmpContact)
        {
            foreach($tmpContact as $Contact)
            {
                $FinalContact[]=$Contact;
            }
        }


        $CSVData=array();
        $CSVData[]=$_POST['CSVHeader'];
        foreach($FinalContact as $data)
        {
            $tmpData=array();
            foreach($_POST['CSVHeader'] as $FieldKey=>$FieldName)
            {
                if('ConnectorRelationship'==$FieldKey)
                {
                    $tmpRelation=explode(',',$data['relationships']);
                    $tmpNewRelation=array();
                    foreach($tmpRelation as $relId)
                    {
                        $tmpNewRelation[]=$RelationData[$relId];
                    }
                    $tmpData['ConnectorRelationship']=implode(',',$tmpNewRelation);
                }
                else
                {
                    $tmpData[$FieldName]=$data[$FieldKey];
                }
            }
            $CSVData[]=$tmpData;
        }
        //echo "<pre>";
        //print_r($CSVData);
        //echo "</pre>";
        //die;


        $delimiter=',';
        $filename='Macanta_Connector_Contacts.csv';
        $f = fopen('php://memory', 'w');
        foreach ($CSVData as $line)
        {
            fputcsv($f, $line, $delimiter);
        }
        fseek($f, 0);
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        fpassthru($f);

        die;
        /*
        echo "<pre>";
        print_r($_POST);
        echo "</pre>";

        echo "<pre>";
        print_r($CSVData);
        echo "</pre>";

        echo "<pre>";
        print_r($RelationWithGroup);
        echo "</pre>";
        */

    }
    public function connector_csv_download()
    {
        $Current_Element=$_POST['Current_Element'];

        $ConnectedInfosEncoded = macanta_get_config('connected_info');
        if (!$ConnectedInfosEncoded) $ConnectedInfosEncoded = '[]';
        $ConnectedInfos = json_decode($ConnectedInfosEncoded, true);

        $FieldNameData=array();
        $FieldNameData['ContactId']='ContactId';
        $FieldNameData['FirstName']='FirstName';
        $FieldNameData['LastName']='LastName';
        $FieldNameData['Email']='Email';

        foreach($ConnectedInfos[$Current_Element]['fields'] as $data)
        {
            $FieldNameData[$data['fieldId']]=$data['fieldLabel'];
        }
        $FieldNameData['ConnectorRelationship']='Connector Relationship';
        //echo "<pre>";
        //print_r($FieldNameData);
        //echo "</pre>";


        if(count($FieldNameData))
        {
            ?>
            <form class="connector_csv_generate_download" action="core/tabs/connector_csv_generate_download" method="post" onsubmit="$(this).slideUp();$(this).slideUp();$('#CSVFieldsSelection').modal('toggle');">

                <input type="hidden" name="CurrentConnectorDownLoadType" value="<?php echo $_POST['Current_Element'];?>"/>
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><label>Plase select column to download</label></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <ul class="col-xs-12 col-sm-12 col-md-12 col-lg-12 CDcolumnList no-pad-left no-pad-right">


                            <?php foreach($FieldNameData as $GroupKey=>$FieldName) { ?>

                                <li class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="font-size:14px;">
                                    <input type="checkbox" style="width:30px; height:15px;" checked name="CSVHeader[<?php echo $GroupKey;?>]" value="<?php echo $FieldName;?>"/> <?php echo $FieldName;?>

                                </li>

                            <?php } ?>


                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <button class="btn btn-default col-xs-12 col-sm-12 col-md-6 col-lg-6 no-pad-right no-pad-left"  style="float: right;" >
                    <i class="fa fa-download" aria-hidden="true"></i> <span>Download CSV</span>
                </button>
            </form>
            <?php
        }
        else
        {
            ?>
            <div class="box">
                <div class="box-body">
                    <table class="table table-bordered" width="100%;" style="width: 100%;">
                        <tr><td align="center" style="height:50px; padding:20px;"> Record not found!</td></tr>
                    </table>
                </div>
            </div>
            <?php
        }

    }
}
