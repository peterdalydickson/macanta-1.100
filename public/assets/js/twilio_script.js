/**
 * Created by geover on 28/12/15.
 */
var recentCon = [];
var lastError = '';
var DeviceUsed = '';
var CallerIdUsed = '';
var syncClient;
var CallCenterSyncData = {};
var globalSyncDoc;
var initiateTwilio = function(){
    if(TwilioToken !== 0){
        if(SyncToken !== null && SyncToken !== {}){
            console.log(SyncToken);
            /*    Sync Section    */
            syncClient = new Twilio.Sync.Client(SyncToken.token, { logLevel: 'info' });
            syncClient.on('connectionStateChanged', function(state) {
                if (state !== 'connected') {
                    console.log('Sync is not live (websocket connection  ' + state );
                } else {
                    console.log('Sync is live!');
                }
            });

            //This code will create and/or open a Sync document
            //Note the use of promises
            syncClient.document('CallCenterSync').then(function(syncDoc) {
                //Initialize game board UI to current state (if it exists)
                globalSyncDoc = syncDoc;
                CallCenterSyncData = syncDoc.value;
                if (CallCenterSyncData) {
                    console.log("CallCenterSync Current Status Data:");
                    console.log(CallCenterSyncData);
                }

                //Let's subscribe to changes on this document, so when something
                //changes on this document, we can trigger our UI to update
                syncDoc.on('updated', function(event) {
                    var from =  event.isLocal? "locally." : "by the other guy.";
                    console.log("CallCenterSync was updated "+from+" with the value: ");
                    console.log(event.value)
                    processCallCenterSyncData(event.value);
                    //action here when any changes made with event.value
                });

                //Whenever a board button is clicked, update that document.
                $(document).on('click', function (e) {
                    //Toggle the value: X, O, or empty
                    var Clicked = $(e.target).html();
                    //Send updated document to Sync
                    //This should trigger "updated" events on other clients
                    var data = {stat:Clicked};
                    //syncDoc.set(data);

                });
            });
        }



        /*    Outbound Device Section    */
        Twilio.Device.setup(TwilioToken);

        Twilio.Device.ready(function (device) {
            //console.log("Device Ready!");
            //console.log(device);
            $("#twilio_log").text("Status: Ready");
        });

        Twilio.Device.error(function (error) {
            $("#twilio_log").text("Error: Communication Down or " + error.message);
            lastError = ", "+error.message;
            console.log("Device Error:");
            console.log(error);
        });
        Twilio.Device.cancel(function (conn) {
            console.log("Device Canceled:");
            console.log(conn);
        });
        Twilio.Device.connect(function (conn) {
            //console.log(conn);
            console.log("Device Connect:");
            console.log(conn);
            $("#twilio_log").text("Status: Successfully established call to " +conn.message.PhoneNumber);
        });

        Twilio.Device.disconnect(function (conn) {
            console.log("Device Disconnect:");
            console.log(conn);
            $("#twilio_log").text("Status: Call ended" + lastError);
            lastError = '';
            $('.phoneCall').removeAttr('disabled');
            $('.phoneCall').show();
            $('.EndphoneCall').hide();
            recentCon = conn;
            addNote(ContactId,'call_notes','callNotes','CurrentCallNotes','PreviousCallNotes');// move to EndphoneCall button click
            //getRecord(conn);

        });
    }else {
        $("#twilio_log").text("Status: Communication System Down. ");
    }

}
// this function is merged with addNote because rec_id is not yet created when fired from Twilio.Device.disconnect
function getRecord(conn){
    var CallSid = conn['parameters'].CallSid;
    var jsonData = {"controler":"core/tabs/call","action":"getCallRecord","session_name":session_name,"data":{"CallId":CallSid,"session_name":session_name,"assetsVersion":assetsVersion}};
    console.log("Sending CallID:");
    console.log(jsonData);
    $("button.SaveNote").html('Please wait..');
    $("button.SaveNote").attr('disabled', true);
    $.ajax({
        url: ajax_url,
        type: "POST",
        data: jsonData,
        success: function(e){
            console.log(e)
            // update text editor to contain call recording url
            $("button.SaveNote").removeAttr('disabled');
            $("button.SaveNote").html('Create Note');
            //var URL = "<span style='display:none;'>"+e.data+"</span>";
            //var NotesStr = tinyMCE.get('callNotes').getContent();
            //var NewNoteStr = NotesStr + URL;
            //tinymce.get('callNotes').setContent(NewNoteStr);
            var script = e.script || '';
            eval(script);
        }
    });

/*    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        var DONE = this.DONE || 4;
        if (this.readyState === DONE){
            var x = this.responseText.replace(/\\"/g, '"');
            console.log(x);
            var NotesStr = tinyMCE.get('#callNotes').getContent();
            var NewNoteStr = NotesStr + "Call Recording: " + x
            tinymce.get('#callNotes').setContent(NewNoteStr);
        }
    };
    request.open('POST', ajax_url, true);
    request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');  // Tells server that this call is made for ajax purposes.
    // Most libraries like jQuery/Prototype/Dojo do this
    request.send(JSON.stringify(jsonData));*/
}
function call() {
    Twilio.Device.connect();
}

function hangup() {
    Twilio.Device.disconnectAll();
}
