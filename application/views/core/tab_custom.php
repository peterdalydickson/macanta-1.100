<script>
    $("button").parents(".panel-heading").addClass("has-btn");
</script>
<div class="CustomTabPanel">
    <div class="mainbox-top">
        <div class="panel panel-primary theCustomTabPanel">
            <div class="panel-heading "><h3 class="panel-title NoteName"><i class="fa fa-newspaper-o"></i> <?php echo $title;?></h3></div>
            <div class="panel-body theCustomTabPanelBody"><?php echo $content; ?></div>
        </div>
    </div>
</div>