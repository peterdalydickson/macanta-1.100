<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
require_once(APPPATH . "libraries/PhoneNumberValidation_Interactive_Validate_v2_20.php");
//require APPPATH . '/libraries/Format.php';
//require BASEPATH . '/libraries/Session/Session.php';
/*if ( ! class_exists('CI_Session'))
{
    load_class('Session', 'libraries/Session');
}*/

class Ajax extends REST_Controller
{
    public $OpenCalls = array(
        'login',
        'monitorSync',
        'send_mail_post',
        'initialize_sync'
    );

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->config('version');

    }

    public function index_get()
    {
        $Content = '~CD.Q1~, ~CD.Q2~, ~CD.Q4~ adasd';
        $Content = str_replace('~CD', '~cd', $Content);
        $matches = [];
        //preg_match_all("/~cd\.(.+?)~/", $Content, $matches);
        //$Content = strtolower($Content);
        //print_r($matches) ;
        //$this->response($Content);
        $InitialDate = '2019-10-10';
        $Increment = 3;
        $Currdate = date("Y-m-d H:i", strtotime('0 day'));
        //echo $Currdate;
        //print_r($query);
        $results = infusionsoft_get_contact_by_email_password('geover@gmail.com', 'Macanta2016');
        print_r($results);
    }

    public function http_get()
    {
        print_r($this->get());
        $this->response($this->get());
    }

    public function http_post()
    {
        print_r($this->post());
        $this->response($this->post());
    }

    public function saved_filter_get()
    {
        $this->response(infusionsoft_get_all_saved_search());
    }

    public function columns_get()
    {
        //get all the columns in saved search
        //426505:2319
        $this->response(infusionsoft_get_saved_search_columns(2319, 426505));
    }

    public function webform_get()
    {
        $HTML = infusionsoft_get_web_form('exela', 'dd116c64e31306b08581e35b152b4488');
        $this->response($HTML);
    }

    public function tagcat_get()
    {
        $TagCategories = infusionsoft_get_tag_categories();
        $this->response($TagCategories);
    }

    public function emailtpl_get()
    {
        $EmailTpl = infusionsoft_get_email_template_content("150");
        $this->response($EmailTpl);
    }

    public function webform_post()
    {
        $PostData = $this->post();
        $app = $PostData['app'];
        $formId = $PostData['formId'];
        $HTML = infusionsoft_get_web_form($app, $formId);
        $this->response($HTML);
    }

    public function saved_search_get($savedSearchID, $userID)
    {
        $this->response(infusionsoft_get_all_saved_search_contact($savedSearchID, $userID, 0));
    }

    public function download_get($data)
    {
        $URL = base64_decode($data);
        $TheFile = file_get_contents($URL);
        $extension = strtolower(end(explode(".", $URL)));
        $FileName = basename($URL);
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="' . $FileName . '"');
        $mimes = include APPPATH . "config/mimes.php";
        if (isset($mimes[$extension])) {
            $types = $mimes[$extension];
            if (is_array($types)) {
                $type = $types[0];
            } else {
                $type = $types;
            }
        }
        header('Content-type: ' . $type);
        print_r($TheFile);
    }

    public function index_post()
    {
        header('Content-Type: text/html; charset=UTF-8');
        define('AJAX_REQUEST', true);

        if (macanta_invalid_token()) {
            $Action = "logout";
            $Controller = "core/common";
            $message = '<h4 class="warning-msg">Oops! Looks like there\\\'s a temporary connection issue with Infusionsoft. Please ask your administrator to reconnect Macanta, or <a data-toggle="modal" href="#helpsupport"><span style="color: #77c138">Click Here For Help</span></a></h4>';
            $CacheRefreshScript = 'if(typeof serviceWorkerRegistration !== "undefined"){ serviceWorkerRegistration.unregister();}caches.delete("macanta-cache");';
            $CacheRefreshScript .= "$(\"form.macantaLogin button, form.macantaLogin input\").prop(\"disabled\",true);$(\".login-message\").html('" . $message . "').fadeIn();";
            $Data['script'] = $CacheRefreshScript;
            $Data['reload'] = false;
        } else {
            if (isset($_GET['user'])) {
                $Email = str_replace(' ', '+', $_GET['user']);
                //check if Email is Macanta User
                $Contacts = infusionsoft_get_contact_by_email($Email);
                foreach ($Contacts as $SingleContact) {
                    if (isset($SingleContact->CompanyID)) {
                        if ($SingleContact->CompanyID != $SingleContact->Id) {
                            $Contact = $SingleContact;
                            break;
                        }
                    } else {
                        $Contact = $SingleContact;
                        break;
                    }
                }
                if (isset($Contact->Groups)) {
                    $Type = '';
                    $tagsArr = explode(',', $Contact->Groups);
                    $access_level = json_decode($this->config->item('access_level'), true);
                    foreach ($access_level as $userType => $tag) {
                        if (in_array($tag, $tagsArr)) {
                            $Type = $userType;
                            break;
                        }
                    }
                    if ($Type != '') {
                        $Controller = "core/common";
                        $Action = 'dashboard';
                        $user_seession_data = macanta_get_user_seession_data_by_email($Email);
                        if ($user_seession_data == false) {
                            $MacantaUsers = macanta_get_users();
                            foreach ($MacantaUsers as $MacantaUser) {
                                if ($Email == $MacantaUser->Email) {
                                    $loadedController = $this->load->controller($Controller);
                                    $loginRestults = $this->$loadedController->login($MacantaUser, true);
                                    $user_seession_data = macanta_get_user_seession_data_by_email($Email);
                                    break;
                                }
                            }
                        }
                        $SessionName = $user_seession_data ? $user_seession_data->session_name : '';
                        $AssetsVersion = $this->config->item('macanta_verson');
                        $Data['session_name'] = $SessionName;
                        //$Data['script'] = 'var session_name = "'.$SessionName.'";localStorage.setItem("session_name", "'.$SessionName.'");';
                        $Data['assetsVersion'] = $AssetsVersion;
                    } else {
                        $Action = "logout";
                        $Controller = "core/common";
                        $message = $this->lang->line('text_oops_looks_like');
                        $CacheRefreshScript = '';
                        $CacheRefreshScript .= "localStorage.setItem('LoginMessage', '$message');";
                        $Data['script'] = $CacheRefreshScript;
                        $Data['reload'] = true;
                    }
                } else {
                    $Action = "logout";
                    $Controller = "core/common";
                    $message = $this->lang->line('text_oops_looks_like');
                    $CacheRefreshScript = '';
                    $CacheRefreshScript .= "localStorage.setItem('LoginMessage', '$message');";
                    $Data['script'] = $CacheRefreshScript;
                    $Data['reload'] = true;
                }
            } else {
                $PostData = $this->post();
                $Controller = $PostData['controler'];
                $Action = $PostData['action'];
                $Data = isset($PostData['data']) ? $PostData['data'] : [];
                if ($Action == 'saveCDFileAttachement') $Data = json_decode($PostData['data'], true);
                $SessionName = $Data['session_name'];
                $AssetsVersion = $Data['assetsVersion'];
                $user_seession_data = empty($SessionName) ? false : macanta_get_user_seession_data($SessionName);
            }

            if (!empty($AssetsVersion) && $AssetsVersion != $this->config->item('macanta_verson') && !in_array($Action, $this->OpenCalls)) {
                //New Deployed Update: Logout and give App Update Notice
                $Action = "logout";
                $Controller = "core/common";
                $message = "<h4>Macanta has successfully updated to version " . $this->config->item('macanta_verson') . " Please login again.</h4>";
                $this->db->where('session_name', $SessionName);
                $this->db->delete('user_sessions');
                $asset_version = $this->config->item('macanta_verson');
                $CacheRefreshScript = 'if(typeof serviceWorkerRegistration !== "undefined"){ serviceWorkerRegistration.unregister();}caches.delete("macanta-cache");localStorage.setItem("asset_version", "' . $asset_version . '");';
                $CacheRefreshScript .= "localStorage.setItem('LoginMessage', '$message');";
                $Data['script'] = $CacheRefreshScript;
                $Data['reload'] = true;
            } else {
                if (!in_array($Action, $this->OpenCalls)) {

                    if ($user_seession_data == false && !empty($SessionName)) {
                        //Login Expired: Logout and give App Login Expired Notice
                        $Action = "logout";
                        $Controller = "core/common";
                        $message = '<h4 class="warning-msg">Your login session has expired. Please login again.</h4>';
                        $CacheRefreshScript = '';
                        $CacheRefreshScript .= "localStorage.setItem('LoginMessage', '$message');";
                        $Data['script'] = $CacheRefreshScript;
                        $Data['reload'] = true;

                    } elseif ($user_seession_data == false && empty($PostData['session_name'])) {
                        //Show Login: No Notice
                        $Action = "logout";
                        $Controller = "core/common";
                        $Data['reload'] = false;
                        $login_disabled = $this->config->item('login_disabled');
                        if ($login_disabled && trim($login_disabled) != '') {
                            $Data['login_disabled'] = $login_disabled;
                            $Data['script'] = '$(".login-message").fadeIn();';
                        }

                    }

                }
            }
        }


        if ($loadedController == '') {
            $loadedController = $this->load->controller($Controller);
        }
        if (method_exists($this->$loadedController, $Action)) {
            $return = $this->$loadedController->$Action($Data);
            $this->response($return, REST_Controller::HTTP_OK);
        } else {

            $this->response('POST: No Method Existing-> ' . $Action . " in " . $Controller, REST_Controller::HTTP_OK);
        }

    }

    public function data_email_hitory_post()
    {
        header('Content-Type: application/json; charset=UTF-8');
        $Columns = array(
            '0' => 'subject',
            '1' => 'sent_to_address',
            '2' => 'sent_from_address',
            '3' => 'sent_date',
            '4' => 'opened_date'
        );
        $PostData = $this->post();
        $PostData = array_merge($PostData, $_GET); // merge get for contact informations
        $Draw = isset($PostData['draw']) ? $PostData['draw']++ : 1;
        $start = $PostData['start'];
        $post_columns = $PostData['columns'];
        $length = $PostData['length'];
        $search_value = $PostData['search']['value'];
        $search_regex = $PostData['search']['regex'];
        $request_time = $PostData['_'];
        $order_column = $PostData['order'][0]['column'];
        $order_direction = $PostData['order'][0]['dir'];
        $ContactId = $PostData['ContactId'];


        $FinalRows = [];

        $this->db->where('contact_id', $ContactId);
        $this->db->from('InfusionsoftEmailSent');
        $TotalCount = $this->db->count_all_results();

        $this->db->select('id, subject, sent_to_address, sent_from_address, sent_date, opened_date');
        $this->db->where('contact_id', $ContactId);
        foreach ($Columns as $index => $Column) {
            $key = (int)$index;
            if ($post_columns[$key]['search']['value'] != '') {
                $this->db->like($Column, $post_columns[$key]['search']['value']);
            }
        }
        $this->db->order_by($Columns[$order_column], $order_direction);
        $this->db->limit($length, $start);
        $query = $this->db->get('InfusionsoftEmailSent');
        $LastQuery = $this->db->last_query();
        if (sizeof($query->result()) > 0) {
            foreach ($query->result() as $row) {
                $row->opened_date = empty($row->opened_date) ? '' : date('Y-m-d H:i:s', strtotime($row->opened_date));
                $FinalRows[] = [$row->subject, $row->sent_to_address, $row->sent_from_address, date('Y-m-d H:i:s', strtotime($row->sent_date)), $row->opened_date, $row->id];
            }
        }
        $DataTableResponse = [
            'data' => $FinalRows,
            'draw' => $Draw,
            'recordsFiltered' => $TotalCount, // loolks like the total record also
            'recordsTotal' => $TotalCount, // the total record
            'post' => $PostData,
            'last_query' => $LastQuery

        ];

        unset($ConnectedData);
        unset($ConnectorFields);
        unset($ConnectorTabs);
        unset($ConnectorTabsEncoded);
        $this->response($DataTableResponse, REST_Controller::HTTP_OK);

    }

    public function data_table_post()
    {
        header('Content-Type: application/json; charset=UTF-8');
        $PostData = $this->post();
        $PostData = array_merge($PostData, $_GET); // merge get for contact informations
        $Draw = isset($PostData['draw']) ? $PostData['draw']++ : 1;
        $start = $PostData['start'];
        $length = $PostData['length'];
        $search_value = $PostData['search']['value'];
        $search_regex = $PostData['search']['regex'];
        $request_time = $PostData['_'];
        $order_column = $PostData['order'][0]['column'];
        $order_direction = $PostData['order'][0]['dir'];
        $ContactId = $PostData['ContactId'];
        $FirstName = $PostData['FirstName'];
        $LastName = $PostData['LastName'];
        $Email = $PostData['Email'];
        $GroupId = $PostData['GroupId'];
        $session_name = $PostData['session_name'];


        $FinalRows = [];
        $lengthCount = 0;
        $CacheFile = $GroupId . '-' . $order_column;
        $tempData = manual_cache_loader($CacheFile);
        $tempData = false; // disable cache
        if ($tempData == false) {
            $ConnectorRelationship = $this->config->item('ConnectorRelationship') ? json_decode($this->config->item('ConnectorRelationship')) : [];
            $availableRelationships = [];
            foreach ($ConnectorRelationship as $RelationshipItem) {
                $availableRelationships[$RelationshipItem->Id] = ["RelationshipName" => $RelationshipItem->RelationshipName, "RelationshipDescription" => $RelationshipItem->RelationshipDescription];
            }
            /*For connector tab contents*/
            $ConnectorTabsEncoded = macanta_get_config('connected_info');
            $ConnectorTabs = json_decode($ConnectorTabsEncoded, true);
            $ConnectorTab = [];
            foreach ($ConnectorTabs as $key => $_ConnectorTab) {
                if ($_ConnectorTab['id'] === $GroupId) {
                    $ConnectorTab = $_ConnectorTab;
                    break;
                }
            }
            //Get FirstColumn
            ob_start();
            ?>
            <select
                    data-contactid="<?php echo $ContactId; ?>"
                    class="multiselect-ui field-relationships form-control ConnectedContactRelationshipsBulkAdd ConnectedContactRelationshipsBulkAdd<?php echo $ContactId; ?> addGroup"
                    multiple="multiple">
                <?php
                $ConnectorFields = macanta_get_config('connected_info');
                $ConnectorFields = $ConnectorFields ? json_decode($ConnectorFields, true) : [];
                $RelationshipRules = [];
                foreach ($ConnectorFields as $FieldGroups) {
                    $RelationshipRules[$FieldGroups['id']] = $FieldGroups['relationships'];
                }
                $Allowed = [];
                foreach ($RelationshipRules[$GroupId] as $theRules) {
                    $Allowed[] = $theRules['Id'];
                }
                foreach ($availableRelationships as $Id => $availableRelationship) {
                    if (in_array($Id, $Allowed))
                        echo '<option value="' . $Id . '" >' . $availableRelationship["RelationshipName"] . '</option>';
                }
                ?>
            </select>
            <?php
            $ConstantColumn_0 = ob_get_contents();
            ob_end_clean();

            // Get All the Other Columns
            $Order = [];
            $OrderId = [];
            foreach ($ConnectorTab['fields'] as $field) {
                if ($field["showInTable"] == "yes") {
                    $key = (int)$field["showOrder"];
                    if (isset($Order[$key])) {
                        while (isset($order[$key])) {
                            $key++;
                        }
                        $Order[$key] = $field["fieldLabel"];
                        $OrderId[$key] = $field["fieldId"];
                    } else {
                        $Order[$key] = $field["fieldLabel"];
                        $OrderId[$key] = $field["fieldId"];
                    }
                }
            }
            ksort($Order);
            ksort($OrderId);

            if (sizeof($Order) == 0) {
                $count = 0;
                foreach ($ConnectorTab['fields'] as $field) {
                    $count++;
                    $OrderId[] = $field["fieldId"];
                    if ($count == 3) break;
                }
            }


            $ConnectedData = macanta_get_connected_info_by_group_id($GroupId, $ContactId, $search_value);
            $UserValue = $ConnectedData[$GroupId];
            $AllContact = [];
            $ContainedIds = [];
            $RowIndex = 0;

            foreach ($UserValue as $itemId => $ValuesArr) {

                $RowData[$RowIndex][] = $ConstantColumn_0;
                $UserField = $ValuesArr['value'];
                $ConnectedContact = $ValuesArr['connected_contact'];
                foreach ($ConnectedContact as $_ContactId => $theRalation) {
                    if ($theRalation['ContactId'] === $ContactId) continue;
                    if (in_array($theRalation['ContactId'], $ContainedIds)) continue;
                    $ContainedIds[] = $theRalation['ContactId'];
                    $AllContact[$theRalation['FirstName']] = $theRalation;
                }
                $ContactIdsArr = array_keys($ConnectedContact);
                $ContactIdsStr = implode(',', $ContactIdsArr);
                $ContactIdsStr = $ContactIdsStr === '' ? '0' : $ContactIdsStr;
                $hideThis = array_key_exists($ContactId, $ConnectedContact) ? "hideThis" : "";
                foreach ($OrderId as $fieldName) {
                    if (is_array($UserField[$fieldName])) {
                        $Val = isset($UserField[$fieldName]["id_" . $ContactId]) ? $UserField[$fieldName]["id_" . $ContactId] : '<small><em>Contact Specific</em></small>';
                    } else {
                        $Val = $UserField[$fieldName];
                    }
                    $RowData[$RowIndex][] = $Val;
                }
                $RowData[$RowIndex]['itemid'] = $itemId;
                $RowData[$RowIndex]['contactids'] = $ContactIdsStr;
                $RowData[$RowIndex]['hideThis'] = $hideThis;
                $RowData[$RowIndex]['data_raw'] = str_replace("=", "", base64_encode(json_encode($UserField)));
                $RowData[$RowIndex]['data_connectedcontacts'] = str_replace("=", "", base64_encode(json_encode($ConnectedContact)));
                $RowIndex++;

            }
            // Lets Order by request
            foreach ($RowData as $index => $Row) {
                if (!isset($tempData['data'][$Row[$order_column]])) {
                    $tempData['data'][$Row[$order_column]] = $RowData[$index];
                } else {
                    $tempData['data'][$Row[$order_column] . time()] = $RowData[$index];
                }

            }
            $tempData['recordsFiltered'] = sizeof($RowData);
            $tempData['recordsTotal'] = sizeof($RowData);
            manual_cache_writer($CacheFile, $tempData, 900);
        }
        $order_direction == 'asc' ? ksort($tempData['data']) : krsort($tempData['data']);
        $loopPointer = 0;
        if (isset($PostData['FilteredContactId']) && sizeof($PostData['FilteredContactId']) > 0) {
            //Filter By contact Ids
            foreach ($tempData['data'] as $ColValue => $theRow) {
                if (!empty($theRow['contactids'])) {
                    $contactids = explode(',', $theRow['contactids']);
                    $result = array_diff($PostData['FilteredContactId'], $contactids);
                    if ($result == $PostData['FilteredContactId']) unset($tempData['data'][$ColValue]);
                }

            }
            $tempData['recordsTotal'] = $tempData['recordsFiltered'] = sizeof($tempData['data']);
        }
        foreach ($tempData['data'] as $ColValue => $theRow) {
            if ($loopPointer < $start) {
                $loopPointer++;
                continue;
            }
            $FinalRows[] = $theRow;
            if ($lengthCount == $length) break;
            $lengthCount++;
        }

        $DataTableResponse = [
            'data' => $FinalRows,
            'draw' => $Draw,
            'recordsFiltered' => $tempData['recordsFiltered'], // lools like the total record also
            'recordsTotal' => $tempData['recordsTotal'] // the total record
        ];

        unset($ConnectedData);
        unset($ConnectorFields);
        unset($ConnectorTabs);
        unset($ConnectorTabsEncoded);
        $this->response($DataTableResponse, REST_Controller::HTTP_OK);

    }

    public function connecteddata_export_post()
    {
        /*
        Required Parameters:
            email
            api_key
            connected_group

        Optional Parameters:
            export_fields
         */
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $PostData = $this->post();
        $api_key = isset($PostData['api_key']) ? $PostData['api_key'] : 0;
        $api_key = isset($PostData['access_key']) ? $PostData['access_key'] : $api_key;
        if ($this->checkAPIkey($api_key) == false)
            $this->response('Forbidden: Missing or Invalid API KEY', REST_Controller::HTTP_FORBIDDEN);

        if (
            !isset($PostData['email']) &&
            !isset($PostData['connected_group'])
        ) $this->response('Forbidden: Missing Required Field - ' . json_encode($PostData), REST_Controller::HTTP_FORBIDDEN);

        $connected_group = strtolower($PostData['connected_group']);
        if (isset($PostData['re-export'])) {
            $reexport = trim($PostData['re-export']) === 'yes' ? true : false;
        } else {
            $reexport = false;
        }
        $export_fields = isset($PostData['export_fields']) ? explode(',', strtolower($PostData['export_fields'])) : false;
        if ($export_fields != false) {
            $export_fields = array_map('trim', $export_fields);
            $export_fields = array_map('strtolower', $export_fields);
        }
        if ($reexport == false) {
            $QueryField = 'meta';
            $QueryValue = '"exported":"yes"';
        } else {
            $QueryField = '';
            $QueryValue = '';
        }

        $ConnectedData = macanta_get_connected_info('', false, $QueryField, $QueryValue, false);
        $RelationshipMap = macanta_get_connected_info_relationships_map();
        $GroupFieldsMap = macanta_get_connected_info_group_fields_map();
        //reorganize the array
        $GroupFieldsMapNew = [];
        foreach ($GroupFieldsMap as $Groups) {
            $GroupFieldsMapNew[] = $Groups;
        }
        $ToCSV = [];
        $ExportedItem = [];
        foreach ($ConnectedData as $ConnectedDataGroupId => $Items) {
            //get group name
            foreach ($GroupFieldsMapNew as $GroupDetails) {
                if ($GroupDetails['id'] == $ConnectedDataGroupId) {
                    if ($connected_group != strtolower($GroupDetails['title'])) continue;
                    $ToCSVTemp = [];
                    foreach ($Items as $ItemId => $ItemDetails) {
                        $ExportedItem[] = $ItemId;
                        foreach ($ItemDetails['value'] as $FieldId => $FieldValue) {
                            if ($export_fields != false) {
                                if (!in_array($GroupDetails['fields'][$FieldId]['title'], $export_fields)) continue;
                            }
                            if (is_array($FieldValue)) $FieldValue = json_encode($FieldValue);
                            $ToCSVTemp[ucwords($GroupDetails['fields'][$FieldId]['title'])] = $FieldValue;
                        }
                        $ContactCount = 1;
                        foreach ($ItemDetails['connected_contact'] as $ContactId => $ContactDetails) {
                            $Count = $ContactCount == 1 ? '' : $ContactCount;
                            $ToCSVTemp[trim('Contact Id ' . $Count)] = $ContactId;
                            $ToCSVTemp[trim('Contact FirstName ' . $Count)] = $ContactDetails['FirstName'];
                            $ToCSVTemp[trim('Contact LastName ' . $Count)] = $ContactDetails['LastName'];
                            $ToCSVTemp[trim('Contact Email ' . $Count)] = $ContactDetails['Email'];

                            $relationships = [];
                            foreach ($ContactDetails['relationships'] as $relationship) {
                                $relationships[] = $RelationshipMap[$relationship];
                            }
                            $ToCSVTemp[trim('Contact Relation ' . $Count)] = implode(',', $relationships);
                            $ContactCount++;
                        }
                        $ToCSV[] = $ToCSVTemp;
                    }


                    break;
                }
            }


        }
        $Path = FCPATH . "/exported_cd";
        $appname = $this->config->item('MacantaAppName');
        if (!is_dir($Path)) mkdir($Path, 0777, true);
        $Subject = "macanta exported connected data | $appname | $connected_group | " . date('Y-m-d H:i');
        $pathToGenerate = $Path . "/" . $Subject . ".csv";  // your path and file name
        $header = null;
        $createFile = fopen($pathToGenerate, "w+");
        foreach ($ToCSV as $ToCSVItem) {

            if (!$header) {

                fputcsv($createFile, array_keys($ToCSVItem));
                fputcsv($createFile, $ToCSVItem);   // do the first row of data too
                $header = true;
            } else {

                fputcsv($createFile, $ToCSVItem);
            }
        }
        fclose($createFile);
        $theEmails = explode(',', $PostData['email']);
        $ContactIds = [];
        foreach ($theEmails as $theEmail) {
            $Contacts = infusionsoft_get_contact_by_email($theEmail);
            if (sizeof($Contacts) > 0) {
                foreach ($Contacts as $Contact) {
                    $ContactIds[] = $Contact->Id;
                }
            }
        }

        $htmlBody = "<a href='https://{$appname}.macanta." . EXT . "/exported_cd/{$Subject}.csv'>Click here to download exported connected data in CSV</a>";
        $fromAddress = "Macanta Connected Data Export <noreply@" . $_SERVER['HTTP_HOST'] . ">";
        $toAddress = $theEmails[0];
        $bccAddresses = '';
        $contentType = "HTML";
        $ccAddresses = "";
        $textBody = strip_tags($htmlBody);
        $emailSent = infusionsoft_send_email(base64_encode($Subject), base64_encode($htmlBody), implode(',', $ContactIds), $fromAddress, $toAddress, $bccAddresses, $contentType, $ccAddresses, $textBody);
        if ($emailSent == 1) {
            foreach ($ExportedItem as $CDItemId) {
                $this->db->where('id', $CDItemId);
                $query = $this->db->get('connected_data');
                if (sizeof($query->result()) > 0) {
                    foreach ($query->result() as $row) {
                        $Old = json_decode($row->meta, true);
                        $modified = false;
                        $modification = [];
                        $meta = ["exported" => "yes"];
                        $New = macanta_array_update($meta, $Old, $modification, $modified);
                        if ($New != $Old && $modified === true) {
                            $DBData['meta'] = json_encode($New);
                            macanta_cd_record_history($CDItemId, $modification, 'meta');
                            if (sizeof($DBData) > 0) {
                                $this->db->where('id', $CDItemId);
                                $Results = $this->db->update('connected_data', $DBData);
                            }
                        }
                    }
                }
            }
        }
        $this->response("Exported, Email Sent:" . $emailSent, REST_Controller::HTTP_OK);

    }

    public function checkAPIkey($Key)
    {
        $password = $this->config->item('macanta_api_key');
        return $Key == $password ? true : false;
    }

    public function connecteddata_field_post()
    {
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $RequiredFields = ["connected_group", "field_name", "field_options"];
        $PostData = $this->post();
        $api_key = isset($PostData['api_key']) ? $PostData['api_key'] : 0;
        $api_key = isset($PostData['access_key']) ? $PostData['access_key'] : $api_key;
        $field_action = isset($PostData['field_action']) ? $PostData['field_action'] : 'add'; //replace or add
        $Return = [];
        $Debug = [];
        if ($this->checkAPIkey($api_key) == false) $this->response('Oops: Missing or Invalid API KEY', REST_Controller::HTTP_FORBIDDEN);
        foreach ($RequiredFields as $RequiredField) {
            if (!isset($PostData[$RequiredField])) {
                $this->response('Oops: Missing Required Field - ' . $RequiredField . " from " . json_encode($PostData), REST_Controller::HTTP_FORBIDDEN);
            }
        }
        $connected_group = trim($PostData['connected_group']);
        $field_name = trim($PostData['field_name']);
        $field_options = trim($PostData['field_options']);
        $field_options = explode('|', $field_options);
        $field_options = array_map('trim', $field_options);
        $field_options = implode("\r\n", $field_options);
        $GroupId = '';
        $FieldIndex = '';
        $ConnectedInfoSettings = macanta_get_config('connected_info');
        if ($ConnectedInfoSettings) {
            $ConnectedInfoSettings = json_decode($ConnectedInfoSettings);
            foreach ($ConnectedInfoSettings as $ConnectedInfoSettingGroupId => $ConnectedInfoSetting) {
                if ($connected_group == $ConnectedInfoSetting->title) {
                    $Fields = $ConnectedInfoSetting->fields;
                    foreach ($Fields as $Index => $Field) {
                        if ($Field->fieldLabel == $field_name) {
                            $GroupId = $ConnectedInfoSettingGroupId;
                            $FieldIndex = $Index;
                            break 2;

                        }
                    }

                }
            }
            if ($GroupId !== '' && $FieldIndex !== '') {
                if ($field_action == 'add') {
                    $fieldChoices = [];
                    $fieldChoices[] = $ConnectedInfoSettings->{$GroupId}->fields[$FieldIndex]->{'fieldChoices'};
                    $fieldChoices[] = $field_options;
                    $fieldChoices = implode("\r\n", $fieldChoices);
                    $ConnectedInfoSettings->{$GroupId}->fields[$FieldIndex]->{'fieldChoices'} = $fieldChoices;
                    $DBdata['value'] = json_encode($ConnectedInfoSettings);
                    $this->db->where('key', 'connected_info');
                    $this->db->update('config_data', $DBdata);
                    $this->response($ConnectedInfoSettings->{$GroupId}->fields[$FieldIndex], REST_Controller::HTTP_OK);
                } elseif ($field_action == 'replace') {
                    $ConnectedInfoSettings->{$GroupId}->fields[$FieldIndex]->{'fieldChoices'} = $field_options;
                    $DBdata['value'] = json_encode($ConnectedInfoSettings);
                    $this->db->where('key', 'connected_info');
                    $this->db->update('config_data', $DBdata);
                    $this->response($ConnectedInfoSettings->{$GroupId}->fields[$FieldIndex], REST_Controller::HTTP_OK);
                } elseif ($field_action == 'remove') {
                    $field_options_to_be_removed = explode("\r\n", $field_options);
                    $fieldChoices = explode("\r\n", $ConnectedInfoSettings->{$GroupId}->fields[$FieldIndex]->{'fieldChoices'});
                    $fieldChoices = array_diff($fieldChoices, $field_options_to_be_removed);
                    $fieldChoices = implode("\r\n", $fieldChoices);
                    $ConnectedInfoSettings->{$GroupId}->fields[$FieldIndex]->{'fieldChoices'} = $fieldChoices;
                    $DBdata['value'] = json_encode($ConnectedInfoSettings);
                    $this->db->where('key', 'connected_info');
                    $this->db->update('config_data', $DBdata);
                    $this->response($ConnectedInfoSettings->{$GroupId}->fields[$FieldIndex], REST_Controller::HTTP_OK);
                } else {
                    $this->response("Error: Unknown field_action: {$field_action}", REST_Controller::HTTP_OK);
                }

            } else {
                $this->response("Error: No Connected Data Settings", REST_Controller::HTTP_OK);
            }

        } else {
            $this->response("Error: Connected Data Not Found With Group: {$connected_group} and Field: {$field_name}", REST_Controller::HTTP_OK);

        }

    }

    public function connecteddata_edit_post()
    {
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);

        $RequiredFields = ["contactId", "connected_group", "connecteddata_id", "cd_guid"];
        $PostData = $this->post();
        $api_key = isset($PostData['api_key']) ? $PostData['api_key'] : 0;
        $api_key = isset($PostData['access_key']) ? $PostData['access_key'] : $api_key;
        $Return = [];
        $Debug = [];
        $connected_contacts = [];
        if ($this->checkAPIkey($api_key) == false) $this->response('Oops: Missing or Invalid API KEY', REST_Controller::HTTP_FORBIDDEN);

        foreach ($RequiredFields as $RequiredField) {
            if (!isset($PostData[$RequiredField])) {
                if ($RequiredField == "connecteddata_id") {
                    if (!isset($PostData["cd_guid"])) $this->response("Oops: Missing Required Field - connecteddata_id or cd_guid  from " . json_encode($PostData), REST_Controller::HTTP_FORBIDDEN);
                } elseif ($RequiredField == "cd_guid") {
                    if (!isset($PostData["connecteddata_id"])) $this->response("Oops: Missing Required Field - connecteddata_id or cd_guid  from " . json_encode($PostData), REST_Controller::HTTP_FORBIDDEN);
                } else {
                    $this->response('Oops: Missing Required Field - ' . $RequiredField . " from " . json_encode($PostData), REST_Controller::HTTP_FORBIDDEN);
                }
            }
        }
        macanta_logger('connecteddata_edit_post', json_encode($PostData));
        $contactId = $PostData['contactId']; //
        $connected_groups = strtolower($PostData['connected_group']);
        $cd_guid = isset($PostData['cd_guid']) ? $PostData['cd_guid'] : "";
        $update_fields = isset($PostData['update_fields']) ? strtolower($PostData['update_fields']) : '';
        $connecteddata_id = isset($PostData['connecteddata_id']) ? trim(strtolower($PostData['connecteddata_id'])) : "";
        $connected_relationship = isset($PostData['connected_relationship']) ? trim($PostData['connected_relationship']) : false;
        $connected_relationship = isset($PostData['relationship']) ? trim($PostData['relationship']) : $connected_relationship;
        if ($connected_relationship) {
            $connected_contacts[$contactId] = $connected_relationship;
        }

        unset($PostData['cd_guid']);
        unset($PostData['contactId']);
        unset($PostData['connected_group']);
        unset($PostData['update_fields']);
        unset($PostData['connecteddata_id']);
        unset($PostData['api_key']);
        unset($PostData['access_key']);
        unset($PostData['connected_relationship']);
        unset($PostData['relationship']);
        //get custom field for a given $update_fields and $connecteddata_id
        $UserFields = trim($update_fields) !== '' ? explode(',', $update_fields) : [];
        if ($connecteddata_id != "")
            $UserFields[] = $connecteddata_id;
        $DefaultValues = [];
        $connected_groupsArr = explode(" or ", $connected_groups);
        foreach ($connected_groupsArr as $connected_group) {
            $ConnectedInfos = macanta_get_connected_info_by_groupname($connected_group, $contactId, '', $cd_guid);
            if (sizeof($ConnectedInfos) == 0) {
                $Debug['groups'][$connected_group] = $this->connecteddata_add_post(true);
                continue;
            }
            $ISContactFields = [];
            $UserCustomFieldsArr = [];
            $DateFormat = [];
            $Data = [];
            foreach ($ConnectedInfos[$connected_group] as $ItemId => $ItemDetails) {
                $ISContactFields = [];
                if (trim($update_fields) !== '') {
                    //print_r($ItemDetails['fields']);
                    foreach ($UserFields as $UserField) {
                        $UserField = trim($UserField);
                        $Data[$UserField] = $ItemDetails['fields'][$UserField]['default-value'];
                        //echo $UserField."\n";
                        //echo json_encode($ItemDetails['fields']);
                        if (isset($ItemDetails['fields'][$UserField])) {


                            if ($ItemDetails['fields'][$UserField]['custom-field'] != "") {

                                $ISContactFields[] = "\"_" . $ItemDetails['fields'][$UserField]['custom-field'] . '"';
                                $UserCustomFieldsArr[$UserField] = "_" . $ItemDetails['fields'][$UserField]['custom-field'];
                                if (
                                    $ItemDetails['fields'][$UserField]['field-type'] == 'DateTime' ||
                                    $ItemDetails['fields'][$UserField]['field-type'] == 'Date'
                                ) {
                                    $Format = $ItemDetails['fields'][$UserField]['field-type'];
                                    $DateFormat[$UserField] = $Format == 'Date' ? 'Y-m-d' : 'Y-m-d H:i:s';
                                }

                            }
                        }
                    }
                } else {
                    foreach ($ItemDetails['fields'] as $UserField => $Field) {
                        $Data[$UserField] = $Field['default-value'];
                        if (trim($Field['custom-field']) !== "") {
                            $ISContactFields[] = "\"_" . $Field['custom-field'] . '"';
                            $UserCustomFieldsArr[$UserField] = "_" . $Field['custom-field'];
                            if (
                                $ItemDetails['fields'][$UserField]['field-type'] == 'DateTime' ||
                                $ItemDetails['fields'][$UserField]['field-type'] == 'Date'
                            ) {
                                $Format = $ItemDetails['fields'][$UserField]['field-type'];
                                $DateFormat[$UserField] = $Format == 'Date' ? 'Y-m-d' : 'Y-m-d H:i:s';
                            }
                        }
                    }
                }

                break; // we only need one item to get custom fields
            }
            if (sizeof($ISContactFields) > 0) {
                $update = infusionsoft_get_contact_by_id_simple($contactId, $ISContactFields);
                echo json_encode($update);
                $UserCustomFieldValuesArr = infusionsoft_get_contact_by_id_simple($contactId, $ISContactFields)[0];
            } else {
                $UserCustomFieldValuesArr = [];
                macanta_logger('connecteddata_edit_post_value', 'No Connected Data To Update');
                $this->response('No Connected Data To Update, Data: ' . json_encode($UserCustomFieldValuesArr), REST_Controller::HTTP_OK);

            }

            foreach ($UserCustomFieldsArr as $MacantaField => $CustomField) {
                if (isset($UserCustomFieldValuesArr->$CustomField)) {
                    $Value = $UserCustomFieldValuesArr->$CustomField;
                    if (is_object($Value)) {
                        //file_put_contents(dirname(__FILE__)."/CustomFieldName.txt",$Value->date."\n",FILE_APPEND);
                        //$Value = explode(" ",$Value->date);
                        $Value = isset($DateFormat[$MacantaField]) ? date($DateFormat[$MacantaField], strtotime($Value->date)) : date("Y-m-d H:i:s", strtotime($Value->date));
                    }
                    $Data[$MacantaField] = $Value;
                } elseif (trim($update_fields) !== '') {
                    $Data[$MacantaField] = '';
                }
            }
            $Parsed = sizeof($PostData) == 0 ? [] : infusionsoft_parse_criteria($PostData);
            $Passed = sizeof($Parsed) == 0 ? true : false;
            $Expressions = [];
            if ($connecteddata_id == "" && $cd_guid == "") {
                $Debug['groups'][$connected_group] = macanta_add_update_connected_data($contactId, false, $connected_group, $Data, $connected_contacts, false, false, false);
                macanta_logger('connecteddata_edit_post_value', json_encode([$Data, $connected_contacts]));
            } else {
                foreach ($ConnectedInfos[$connected_group] as $ItemId => $ItemDetails) {
                    $Passed = sizeof($Parsed) == 0 ? true : false;
                    $Fields = $ItemDetails['fields'];
                    //Validate Fields values
                    foreach ($Parsed as $FieldName => $Expression) {
                        $Expressions[] = $Expression;
                        $Passed = macanta_check_value_by_expression($FieldName, $Fields, $Expression);

                    }
                    if ($Passed == true) {
                        $Debug['groups'][$connected_group] = macanta_add_update_connected_data($contactId, false, $connected_group, $Data, $connected_contacts, false, $connecteddata_id, true, $cd_guid);
                        macanta_logger('connecteddata_edit_post_value', json_encode([$Data, $connected_contacts]));
                    }
                }
            }
        }

        $Debug['body'] = $this->post();
        $Debug['is_cf_key'] = $ISContactFields;
        $Debug['is_cf_value'] = $UserCustomFieldValuesArr;
        $this->response($Debug, REST_Controller::HTTP_OK);
    }

    public function connecteddata_add_post($return = false)
    {
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        $PostData = $this->post();
        $api_key = isset($PostData['api_key']) ? $PostData['api_key'] : 0;
        $api_key = isset($PostData['access_key']) ? $PostData['access_key'] : $api_key;
        if ($this->checkAPIkey($api_key) == false)
            $this->response('Forbidden: Missing or Invalid API KEY', REST_Controller::HTTP_FORBIDDEN);

        if (
            !isset($PostData['contactId']) ||
            !isset($PostData['connected_group'])
        ) $this->response('Forbidden: Missing Required Field - ' . json_encode($PostData), REST_Controller::HTTP_FORBIDDEN);
        $connected_relationship = $PostData['connected_relationship'];
        $connected_relationship = isset($PostData['relationship']) ? trim($PostData['relationship']) : $connected_relationship;

        if (empty($connected_relationship)) $this->response('Forbidden: Missing Required Field - ' . json_encode($PostData), REST_Controller::HTTP_FORBIDDEN);

        macanta_logger('connecteddata_add_post', json_encode($PostData));
        $contactId = $PostData['contactId'];
        $connected_groups = $PostData['connected_group'];

        $duplicate_option = isset($PostData['connecteddata_id']) ? trim($PostData['connecteddata_id']) == "" ? false : $PostData['connecteddata_id'] : false;
        $ConnectedContact = [$contactId => $connected_relationship];
        $ConnectorTabsEncoded = macanta_get_config('connected_info');
        $ConnectorTabs = json_decode($ConnectorTabsEncoded, true);
        $Debug = [];
        $DefaultValues = [];
        $connected_groupsArr = explode(" or ", $connected_groups);
        foreach ($connected_groupsArr as $connected_group) {
            $returnFields = [];
            $DataContainer = [];
            $DataContainerType = [];
            $Data = [];
            $DateFormat = [];
            foreach ($ConnectorTabs as $index => $ConnectorTab) {
                if (strtolower(trim($connected_group)) == strtolower(trim($ConnectorTab['title']))) {
                    $Fields = $ConnectorTab['fields'];
                    foreach ($Fields as $sub_index => $Field) {
                        $Data[$Field['fieldLabel']] = $Field['defaultValue'];
                        if (trim($Field['infusionsoftCustomField']) !== "") {
                            $returnFields[] = '"_' . trim($Field['infusionsoftCustomField']) . '"';
                            $DataContainer[$Field['fieldLabel']] = '_' . trim($Field['infusionsoftCustomField']);
                            $DataContainerType[$Field['fieldLabel']] = $Field['fieldType'];

                            if (
                                $Field['fieldType'] == 'DateTime' ||
                                $Field['fieldType'] == 'Date'
                            ) {
                                $Format = $Field['fieldType'];
                                $DateFormat[$Field['fieldLabel']] = $Format == 'Date' ? 'Y-m-d' : 'Y-m-d H:i:s';
                            }


                        }
                    }
                    break;
                }
            }
            $Contact = sizeof($returnFields) > 0 ? infusionsoft_get_contact_by_id_simple($contactId, $returnFields)[0] : [];
            foreach ($DataContainer as $DataFieldName => $CustomFieldName) {
                if (isset($Contact->$CustomFieldName)) {
                    // Handle Date Object
                    if (is_object($Contact->$CustomFieldName)) {
                        if (isset($Contact->$CustomFieldName->date)) {
                            //file_put_contents(dirname(__FILE__)."/CustomFieldName.txt",$Contact->$CustomFieldName->date."\n",FILE_APPEND);
                            //$Value = explode(" ",$Contact->$CustomFieldName->date);
                            //$Value = $Value[0];
                            $Value = isset($DateFormat[$DataFieldName]) ? date($DateFormat[$DataFieldName], strtotime($Contact->$CustomFieldName->date)) : date("Y-m-d H:i:s", strtotime($Contact->$CustomFieldName->date));

                        } else {
                            $Value = json_encode($Contact->$CustomFieldName);
                        }
                        $Data[$DataFieldName] = $Value;
                    } else {
                        if ($DataContainerType[$DataFieldName] == 'Checkbox') {
                            $Contact->$CustomFieldName = str_replace(',', '|', $Contact->$CustomFieldName);
                        }
                        $Data[$DataFieldName] = $Contact->$CustomFieldName;
                    }
                } else {
                    //$Data[$DataFieldName] = '';
                }
            }
            $Debug['groups'][$connected_group] = macanta_add_update_connected_data($contactId, false, $connected_group, $Data, $ConnectedContact, false, $duplicate_option, false, "", true);

        }

        if ($return) return $Debug;

        $this->response($Debug, REST_Controller::HTTP_OK);
    }

    public function expired_app_post()
    {
        header("Content-Type: text/plain");
        $PostData = $this->post();
        //file_put_contents(dirname(__FILE__)."/expired_app_post.txt", json_encode($PostData),FILE_APPEND);
        $FirstName = $PostData['FirstName'];
        $LastName = $PostData['LastName'];
        $Email = $PostData['Email'];
        $Company = $PostData['Company'];
        $ContactId = $PostData['ContactId'];
        $coupon_code = $PostData['coupon_code'];
        $hash = $PostData['hash'];
        $MacantaURL = $PostData['MacantaURL'];
        $Country = $PostData['Country'];
        $app_name = $PostData['app_name'];
        $DateTrialEnd = $PostData['DateTrialEnd'];

        if (empty($hash))
            $this->response('Forbidden: Missing or Invalid HASH Code', REST_Controller::HTTP_FORBIDDEN);

        //$Link = "https://macanta.org/start-subscription/?FirstName={$FirstName}&LastName={$LastName}&Company={$Company}&ContactId={$ContactId}&coupon_code={$coupon_code}&hash={$hash}&MacantaURL={$MacantaURL}&Country={$Country}&app_name={$app_name}&Email={$Email}";
        $Link = "https://bluepeg.org/hosted-macanta/?";
        $Message = "Oops! Your 30 day free trial of Macanta came to a screeching halt on {$DateTrialEnd} :) 
                    Please <a href='{$Link}'>[click here]</a> to start your Hosted Macanta subscription. 
                    Or contact us via the green 'Click Here For Help' button. 
                    Thank you.";

        $DBData['value'] = $Message;
        if (false == $this->DbExists('key', 'login_disabled', 'config_data')) {
            $DBData['key'] = 'login_disabled';
            echo $this->db->insert('config_data', $DBData);
        } else {
            $this->db->where('key', 'login_disabled');
            echo $this->db->update('config_data', $DBData);
        }

    }

    public function DbExists($field, $value, $table)
    {
        $this->db->where($field, $value);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {

            foreach ($query->result() as $row) {
                return true;
            }
            return false;
        } else {

            return false;
        }
    }

    public function disable_app_post()
    {
        header("Content-Type: text/plain");
        $PostData = $this->post();
        $api_key = isset($PostData['api_key']) ? $PostData['api_key'] : 0;
        $api_key = isset($PostData['access_key']) ? $PostData['access_key'] : $api_key;
        if ($this->checkAPIkey($api_key) == false)
            $this->response('Forbidden: Missing or Invalid API KEY', REST_Controller::HTTP_FORBIDDEN);

        if (!isset($PostData['messages'])) $this->response('Forbidden: Missing Required Field - messages', REST_Controller::HTTP_FORBIDDEN);
        $DBData['messages'] = $PostData['messages'];
        if (false == $this->DbExists('key', 'login_disabled', 'config_data')) {
            $DBData['key'] = 'login_disabled';
            $this->db->insert('config_data', $DBData);
        } else {
            $this->db->where('key', 'login_disabled');
            $this->db->update('config_data', $DBData);
        }

    }

    public function enable_app_get()
    {
        header("Content-Type: text/plain");
        $PostData = $this->get();
        //macanta_install_hash
        $hash = isset($PostData['hash']) ? $PostData['hash'] : 0;
        if ($this->checkAppHash($hash) == false)
            $this->response('Forbidden: Missing or Invalid App Install Hash', REST_Controller::HTTP_FORBIDDEN);

        $this->db->where('key', 'login_disabled');
        $this->db->delete('config_data');
        $this->response('App Enabled', REST_Controller::HTTP_OK);
    }

    public function checkAppHash($Hash)
    {
        $macanta_install_hash = $this->config->item('macanta_install_hash');
        return $macanta_install_hash == $Hash ? true : false;
    }

    public function enable_app_post()
    {
        header("Content-Type: text/plain");
        $PostData = $this->post();
        //macanta_install_hash
        $hash = isset($PostData['hash']) ? $PostData['hash'] : 0;
        if ($this->checkAppHash($hash) == false)
            $this->response('Forbidden: Missing or Invalid App Install Hash', REST_Controller::HTTP_FORBIDDEN);

        $this->db->where('key', 'login_disabled');
        $this->db->delete('config_data');
        $this->response('App Enabled', REST_Controller::HTTP_OK);
    }

    public function connecteddata_task_post($return = false)
    {
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        $PostData = $this->post();
        $api_key = isset($PostData['api_key']) ? $PostData['api_key'] : 0;
        $api_key = isset($PostData['access_key']) ? $PostData['access_key'] : $api_key;
        if ($this->checkAPIkey($api_key) == false)
            $this->response('Forbidden: Missing or Invalid API KEY', REST_Controller::HTTP_FORBIDDEN);

        $contactId = isset($PostData['contactId']) ? $PostData['contactId'] : '';
        $connected_group = isset($PostData['connected_group']) ? $PostData['connected_group'] : '';
        $connected_relationship = $PostData['connected_relationship'];
        $connected_relationship = isset($PostData['relationship']) ? trim($PostData['relationship']) : $connected_relationship;
        $task_title = $PostData['task_title'];
        $task_description = $PostData['task_description'];
        $task_due_date = isset($PostData['task_due_date']) ? $PostData['task_due_date'] : date('Y-m-d');
        if (isValidTimeString($task_due_date)) $task_due_date = date('Y-m-d', strtotime($task_due_date));
        $cd_guid = isset($PostData['cd_guid']) ? $PostData['cd_guid'] : '';
        $ConnectedContactsInfo = [];
        $AssignedToId = false;
        $data = [
            "TaskNote" => $task_title,
            "TaskActionDescription" => $task_description,
            "ActionType" => 'MacantaTask',
            "TaskActionPriority" => '2',
            "TaskActionDate" => $task_due_date
        ];
        //print_r($PostData);
        if (!empty($cd_guid)) {
            $connected_info = macanta_get_connected_info_by_groupname($connected_group, $contactId, '', $cd_guid);
        } elseif (!empty($connected_group)) {
            $connected_info = macanta_get_connected_info_by_groupname($connected_group, $contactId);
        } else {
            $this->response('Missing Required Parameter: connected_group or cd_guid', REST_Controller::HTTP_FORBIDDEN);
        }
        if (isset($connected_info[strtolower($connected_group)])) {
            if (!empty($cd_guid)) {
                if (isset($connected_info[strtolower($connected_group)][$cd_guid])) {
                    $ItemDetails = $connected_info[strtolower($connected_group)][$cd_guid];
                    $ConnectedContactsInfo = $ItemDetails['connected_contact'];
                }
            } else {
                foreach ($connected_info[strtolower($connected_group)] as $ItemKey => $ItemDetails) {
                    $ConnectedContactsInfo = array_merge($ConnectedContactsInfo, $ItemDetails['connected_contact']);
                }
            }
            foreach ($ConnectedContactsInfo as $ConnectedContactId => $ConnectedContactDetails) {
                if (in_array(strtolower($connected_relationship), $ConnectedContactDetails['relationships']) && $AssignedToId == false) {
                    $AssignedToId = $ConnectedContactId;
                }
            }
            $data['MacantaUser'] = $ConnectedContactsInfo[$AssignedToId]['FirstName'] . " " . $ConnectedContactsInfo[$AssignedToId]['LastName'];
            if (empty($contactId)) {
                foreach ($ConnectedContactsInfo as $ConnectedContactId => $ConnectedContactDetails) {
                    if (!in_array(strtolower($connected_relationship), $ConnectedContactDetails['relationships'])) {
                        $data['conId'] = $ConnectedContactId;
                        $SaveResult = infusionsoft_add_task($data);
                    }
                }
            } else {
                $data['conId'] = $contactId;
                $SaveResult = infusionsoft_add_task($data);
            }
            $this->response(['data' => $data, 'results' => $SaveResult], REST_Controller::HTTP_OK);
        } else {
            $this->response([], REST_Controller::HTTP_OK);
        }
        //$this->response($ConnectedContactsInfo, REST_Controller::HTTP_OK);

    }

    public function connecteddata_get()
    {

        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $PostData = $this->get();
        $api_key = isset($PostData['api_key']) ? $PostData['api_key'] : 0;
        $api_key = isset($PostData['access_key']) ? $PostData['access_key'] : $api_key;
        if ($this->checkAPIkey($api_key) == false)
            $this->response('Forbidden: Missing or Invalid API KEY', REST_Controller::HTTP_FORBIDDEN);

        if (isset($PostData['ref'])) {
            $Results = [];
            $this->db->where('meta_key', $PostData['ref']);
            $query = $this->db->get('users_meta');
            foreach ($query->result() as $row) {
                $value = json_decode($row->meta_value, true);
                $Items = $value['items'];
                $PaidItems = $value['paid_items'];
                $Results['paid_items'] = $PaidItems;
                foreach ($Items as $Item) {
                    $CD = macanta_get_connected_info('', false, '', '', true, $Item);
                    $Results['items'][] = macanta_api_beautify_connected_info($CD)['message'];
                }
            }
            $this->response($Results, REST_Controller::HTTP_OK);
        } else {
            $this->response('Forbidden: Missing Reference Number', REST_Controller::HTTP_FORBIDDEN);
        }
    }

    public function connecteddata_post()
    {
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $PostData = $this->post();
        $api_key = isset($PostData['api_key']) ? $PostData['api_key'] : 0;
        $api_key = isset($PostData['access_key']) ? $PostData['access_key'] : $api_key;

        if ($this->checkAPIkey($api_key) == false)
            $this->response('Forbidden: Missing or Invalid API KEY', REST_Controller::HTTP_FORBIDDEN);

        if (
            !isset($PostData['to_be_mark_paid']) ||
            trim($PostData['prod_field_title_id']) == '' ||
            trim($PostData['prod_field_date_id']) == '' ||
            trim($PostData['renewal_time']) == ''
        ) $this->response('Forbidden: Missing or Blank Required Field - ' . json_encode($PostData), REST_Controller::HTTP_FORBIDDEN);

        $RenewTime = $PostData['renewal_time'];
        $FieldIdOfTitle = $PostData['prod_field_title_id'];
        $FieldIdOfDateToUpdate = $PostData['prod_field_date_id'];
        $SequnceId = $PostData['sequence_id'];
        $CampaignId = $PostData['campaign_id'];
        $RenewTimeFieldId = $PostData['prod_field_renewal_time_id'];

        //MSC Plugin Feature, Renewing Products
        $Result = $PaidIdArr = [];
        if (isset($PostData['to_be_mark_paid'])) {
            foreach ($PostData['to_be_mark_paid'] as $referrence_id => $Titles) {
                // Update Reference ID Value
                $Titles = array_map('trim', $Titles);
                $Titles = array_map('strtolower', $Titles);
                $DBData = [];
                $this->db->where('meta_key', $referrence_id);
                $query = $this->db->get('users_meta');
                foreach ($query->result() as $row) {
                    $value = json_decode($row->meta_value, true);
                    $Items = $value['items'];
                    $PaidItems = $value['paid_items'];
                    $SetupCampaignId = $value['campaign']['id'];
                    $CampaignId = $SetupCampaignId;
                    $SetupSequenceId = $value['campaign']['post_purchase_sequence'];
                    $SequnceId = $SetupSequenceId == false ? $SequnceId : $SetupSequenceId;
                    foreach ($Items as $Item) {
                        if (in_array($Item, $PaidItems)) continue;
                        $this->db->where('id', $Item);
                        $query = $this->db->get('connected_data');
                        if (sizeof($query->result()) > 0) {
                            foreach ($query->result() as $row) {
                                $id = $row->id;
                                $values = json_decode($row->value, true);
                                $NewConnectedContacts = $ConnectedContacts = json_decode($row->connected_contact, true);
                                $OldDate = $values[$FieldIdOfDateToUpdate];
                                $Expresion = $OldDate . " " . $RenewTime;
                                if (in_array(strtolower(trim($values[$FieldIdOfTitle])), $Titles)) {

                                    //Update Connected Data Renewal Date
                                    $DBData = [];
                                    $PaidItems[] = $id;
                                    if (trim($RenewTimeFieldId) !== '') {
                                        if (isset($values[$RenewTimeFieldId]) && trim($values[$RenewTimeFieldId]) !== '') {
                                            $Expresion = $OldDate . " " . $values[$RenewTimeFieldId];
                                        }
                                    }
                                    $values[$FieldIdOfDateToUpdate] = isValidTimeString($OldDate) ? date("Y-m-d", strtotime($Expresion)) : $OldDate;
                                    $DBData['value'] = json_encode($values);
                                    $this->db->where('id', $id);
                                    $Result['cd_value']['result'] = $this->db->update('connected_data', $DBData);
                                    $Result['cd_value']['expresion'] = $Expresion;
                                    $Result['cd_value']['db_data'] = $DBData;

                                    //Update Reference Information
                                    $DBData = [];
                                    $value['paid_items'] = $PaidItems;
                                    $DBData['meta_value'] = json_encode($value);
                                    $this->db->where('meta_key', $referrence_id);
                                    $Result['users_meta']['result'] = $this->db->update('users_meta', $DBData);
                                    if (!$Result['users_meta']['result']) $Result['users_meta']['error'] = $this->db->error();
                                    $Result['users_meta']['db_data'] = $DBData;

                                    //Update Connected Contact Sequences
                                    $DBData = [];
                                    $ContactIdsForFinalSequence = [];
                                    foreach ($ConnectedContacts as $ContactId => $ContactDetails) {
                                        if (isset($ContactDetails['Sequence'])) {
                                            foreach ($ContactDetails['Sequence'] as $SequenceKey => $SequenceDetails) {
                                                $SequenceKeyArr = explode('.', $SequenceKey);
                                                if ($SequenceKeyArr[0] != $CampaignId) continue;
                                                $ContactIdsForFinalSequence[] = $ContactId;
                                                unset($NewConnectedContacts[$ContactId]['Sequence'][$SequenceKey]);
                                            }
                                        }
                                    }
                                    $DBData['connected_contact'] = json_encode($NewConnectedContacts);
                                    $this->db->where('id', $id);
                                    $Result['cd_contacts']['result'] = $this->db->update('connected_data', $DBData);
                                    $Result['cd_contacts']['expresion'] = $Expresion;
                                    $Result['cd_contacts']['db_data'] = $DBData;

                                    //Move Contacts to Final Sequence
                                    $CampaignDetails = infusionsoft_get_campaign_by_id($CampaignId);
                                    if (isset($CampaignDetails->message->sequences)) {
                                        foreach ($CampaignDetails->message->sequences as $sequence) {
                                            if (strpos($sequence->name, $SequnceId) !== false) {
                                                $sequenceId = $sequence->id;
                                                $Result['sequence']['result'] = infusionsoft_multiple_add_to_sequence($CampaignId, $sequenceId, implode(',', $ContactIdsForFinalSequence));
                                                break;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
            $this->response($Result, REST_Controller::HTTP_OK);
        } else {
            $this->response('Forbidden: Paid Items Parameters', REST_Controller::HTTP_FORBIDDEN);
        }

    }

    public function genpass_post()
    {
        $PostData = $this->post();
        $contactId = $PostData['contactId'];
        $passcode = $PostData['passcode'];
        $password = $this->config->item('macanta_api_key');

        if ($passcode == $password) {
            $result = array();
            $PostData = $this->post();
            $UserLevelTags = json_decode($this->config->item('access_level'));
            $StaffTag = $UserLevelTags->staff;
            $result['AppliedTag'] = $AppliedTag = infusionsoft_apply_tag($contactId, $StaffTag);
            $Contact = infusionsoft_get_contact_by_id($contactId, array('all'));
            if (!isset($Contact->Password) || empty($Contact->Password)) {
                $Password = generateAdjNounPhrase();
                $data = array(0 => array('name' => 'Password', 'value' => $Password));
                $result['UpdatedPassword'] = infusionsoft_update_contact($data, $contactId, 'No');
            }
            $this->response($result);
        } else {
            $this->response('Forbidden', REST_Controller::HTTP_FORBIDDEN);
        }

    }

    public function genpass_get()
    {
        $this->response('GET Request Not Allowed', REST_Controller::HTTP_FORBIDDEN);
    }

    public function read_header_get()
    {

        $HeaderId = infusionsoft_get_contact_action_header_id();
        echo $HeaderId;
    }

    public function customfieldval_get()
    {
        $action = "query_is";
        $action_details = '{"table":"Contact","limit":"1000","page":0,"fields":["Id","Email","FirstName","LastName","_ChildAllergies"],"query":{"Id":455}}';
        $Invoice = rucksack_request($action, $action_details);
        print_r($Invoice);

        $action = "update_is";
        $action_details = '{"table":"Contact","id":455,"fields":{"_ChildAllergies":""}}';
        $result = rucksack_request($action, $action_details);

        print_r($result);

        $action = "query_is";
        $action_details = '{"table":"Contact","limit":"1000","page":0,"fields":["Id","Email","FirstName","LastName","_ChildAllergies"],"query":{"Id":455}}';
        $Invoice = rucksack_request($action, $action_details);
        print_r($Invoice);
    }

    public function customfields_get()
    {
        print_r(infusionsoft_get_custom_fields("%", false, -5, false, 'Macanta Notes'));
    }

    public function json_get()
    {
        $NoteCustomFields = '["A","B","C","D","E"]';
        $NoteCustomFields = json_decode($NoteCustomFields);
        print_r($NoteCustomFields);
    }

    public function tryget_get()
    {
        $NoteCustomFields = array(
            array(
                'Label' => 'Call Recording URL',
                'dType' => 'Website'
            ),
            array(
                'Label' => 'JSON',
                'dType' => 'TextArea'
            )

        );
        $CustomFields = infusionsoft_generate_contact_action_custom_fields($NoteCustomFields);
        print_r($CustomFields);
        //$this->response($Notes, REST_Controller::HTTP_OK);
    }

    public function connected_get()
    {
        header("Content-Type: text/plain");
        $resutls = macanta_search_connected_info(["andrea", "mobile"]);
        print_r(json_encode($resutls));
    }

    public function guid_get()
    {
        echo macanta_generate_key('test_');
    }

    public function cf_get()
    {
        header("Content-Type: text/plain");
        $CF = infusionsoft_contact_custom_fields();
        print_r($CF);
    }

    public function customfield_get()
    {

        $results = infusionsoft_get_custom_fields("%", false, -4, false, 'Assigned To');
        $this->response($results[0]->Name, REST_Controller::HTTP_FORBIDDEN);
    }

    public function parenthesis_get()
    {
        $in = "(semi-detached or detached) and (freehold)";
        echo preg_match_all('/\(([A-Za-z0-9\-_ ]+?)\)/', $in, $out);
        print_r($out);
    }

    public function strtotime_get()
    {
        /*date_default_timezone_set("UTC");
        echo date('Y-m-d H:i:s')."\n";
        date_default_timezone_set("Europe/London");
        echo date('Y-m-d H:i:s')."\n";
        $UTC_date = gmdate('Y-m-d H:i:s');
        echo $UTC_date;*/
        echo date('Y-m-d', strtotime("-8 days")) . "\n";

    }

    public function map_get()
    {
        print_r(macanta_get_connected_info_group_fields_map());
    }

    public function token_get()
    {
        $token = unserialize('O:18:"Infusionsoft\Token":4:{s:11:"accessToken";s:24:"hmfxsnhe4r9vxz2j2dn6yk9h";s:12:"refreshToken";s:24:"btf7hcr78z4cnft9zdczy2r9";s:9:"endOfLife";i:1503547075;s:9:"extraInfo";a:2:{s:10:"token_type";s:6:"bearer";s:5:"scope";s:27:"full|qj311.infusionsoft.com";}}');
        print_r($token->accessToken);
    }

    public function campaign_get()
    {
        print_r(infusionsoft_get_campaign_by_id(132));
    }

    public function sequence_get()
    {
        print_r(infusionsoft_multiple_add_to_sequence(11411, 8, '364'));
    }

    public function tags_get()
    {
        print_r(infusionsoft_get_tags());
    }

    public function phpinfo_get()
    {
        $this->response('Forbidden', REST_Controller::HTTP_FORBIDDEN);

        phpinfo();
        echo "<pre>";
        print_r($_SERVER);
        echo "</pre>";
    }

    public function filebox_get()
    {
        header("Content-Type: text/plain");
        error_reporting(E_ERROR);
        $PostData = $this->get();
        $fileId = isset($PostData['fileId']) ? $PostData['fileId'] : 0;
        if (!$fileId) $this->response('Forbidden', REST_Controller::HTTP_FORBIDDEN);
        $TheFile = infusionsoft_get_file_by_id($fileId, 'no')->message;
        $this->db->where('Id', $fileId);
        $query = $this->db->get("InfusionsoftFileBox");
        $row = $query->row();
        if (isset($row)) {
            $FileName = $row->FileName;
            $extension = strtolower(end(explode(".", $FileName)));
        }
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="' . $FileName . '"');
        switch ($extension) {
            case 'jpg':
                header('Content-type: image/jpeg');
                break;
            case 'jpeg':
                header('Content-type: image/jpeg');
                break;
            case 'png':
                header('Content-type: image/png');
                break;
            case 'gif':
                header('Content-type: image/gif');
                break;
            case 'bmp':
                header('Content-type: image/bmp');
                break;
            case 'txt':
                header('Content-type: text/plain');
                break;
            case 'pdf':
                header('Content-type: application/pdf');
                break;
            case 'doc':
                header('Content-type: application/msword');
                break;
            case 'docx':
                header('Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
                break;
            case 'xls':
                header('Content-type: application/vnd.ms-excel');
                break;
            case 'xlsm':
                header('Content-type: application/vnd.ms-excel.sheet.macroenabled.12');
                break;
            case 'xlsx':
                header('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                break;
            case '7z':
                header('application/x-7z-compressed');
                break;
            case 'zip':
                header('application/zip');
                break;
        }
        print_r($TheFile);
    }

    public function getAllAccount_get()
    {
        header('Content-Type: text/plain; charset=UTF-8');
        print_r(zuora_getAllAccount());
    }

    public function getAllContact_get()
    {
        header('Content-Type: text/plain; charset=UTF-8');
        print_r(zuora_getAllContact());
    }

    public function getContactBy_get()
    {
        header('Content-Type: text/plain; charset=UTF-8');
        print_r(zuora_getContactBy('WorkEmail', 'geover@gmail.com'));
    }

    public function getAccountBy_get()
    {
        header('Content-Type: text/plain; charset=UTF-8');
        print_r(zuora_getAccountBy('BillToId', '2c92c0f86140d9b201615208791f66a1'));
    }

    public function getInvoiceBy_get()
    {
        header('Content-Type: text/plain; charset=UTF-8');
        print_r(zuora_getInvoiceBy('AccountId', '2c92c0f96140e5670161521355315e62'));
    }

    public function getSubscribeResultBy_get()
    {
        header('Content-Type: text/plain; charset=UTF-8');
        $Subscription = zuora_queryData('Id', $InvoiceItemVar['SubscriptionId'], "Subscription");
        print_r(zuora_getSubscribeResultBy('AccountId', '2c92c0f96140e5670161521355315e62'));
    }

    public function stop_cd_get()
    {
        // end all the running sequence.
        $this->db->like('connected_contact', '"MoveAfter":');
        $this->db->or_like('connected_contact', '"running"');
        $query = $this->db->get('connected_data');
        echo "Total Items To Reset: " . $query->num_rows() . "<br>\n";
        foreach ($query->result() as $row) {
            $DBdata = [];
            $Id = $row->id;
            $UpdatedConnectedContacts = $ConnectedContacts = json_decode($row->connected_contact, true);
            foreach ($ConnectedContacts as $ContactId => $Details) {
                $Sequence = [];
                foreach ($Details['Sequence'] as $CampaignStage => $CampaignDetails) {
                    unset($CampaignDetails['NextSequence']);
                    unset($CampaignDetails['MoveAfter']);
                    unset($CampaignDetails['Parsed']);
                    $CampaignDetails['Status'] = 'end';
                    $Sequence[$CampaignStage] = $CampaignDetails;
                }
                $UpdatedConnectedContacts[$ContactId]['Sequence'] = $Sequence;
            }
            $DBdata['connected_contact'] = json_encode($UpdatedConnectedContacts);
            $this->db->where('id', $Id);
            $this->db->update('connected_data', $DBdata);

        }
        echo "Updated" . "<br>\n";;
    }

    public function uniqueify_cd_get()
    {
        // end all the running sequence.
        $param = $this->get();
        if (isset($param['field_id'])) {
            $field_id = $param['field_id'];
            $this->db->like('value', '"' . $field_id . '"');
            $query = $this->db->get('connected_data');
            echo "Total Items To Check: " . $query->num_rows() . "<br>\n";
            $CheckedFieldValueArr = [];
            foreach ($query->result() as $row) {
                $Id = $row->id;
                $ConnectedValues = json_decode($row->value, true);
                if (in_array($ConnectedValues[$field_id], $CheckedFieldValueArr)) continue;
                $CheckedFieldValue = $ConnectedValues[$field_id];
                $CheckedFieldValueArr[] = $CheckedFieldValue;
                $this->db->where('id !=', $Id);
                $this->db->like('value', '"' . $field_id . '":"' . $CheckedFieldValue . '"');
                $this->db->delete('connected_data');
            }
            echo "Updated" . "<br>\n";
        }

    }

    public function set_value_cd_get()
    {
        // end all the running sequence.

        $param = $this->get();
        if (isset($param['field_id']) && isset($param['value'])) {
            $field_id = $param['field_id'];
            $value = $param['value'];
            $this->db->like('value', '"' . $field_id . '"');
            $query = $this->db->get('connected_data');
            echo "Total Items To Be Modified: " . $query->num_rows() . "<br>\n";
            foreach ($query->result() as $row) {
                $DBdata = [];
                $Id = $row->id;
                $ConnectedValues = json_decode($row->value, true);
                $ConnectedValues[$field_id] = $value;
                $DBdata['value'] = json_encode($ConnectedValues);
                $this->db->where('id', $Id);
                $this->db->update('connected_data', $DBdata);
            }
            echo "Updated" . "<br>\n";
        }

    }

    public function remove_campaign_get()
    {
        // end all the running sequence.
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $param = $this->get();
        echo $param['campaign_id'] . "\n";
        if (isset($param['campaign_id'])) {
            $campaign_id = $param['campaign_id'];
            $this->db->like('connected_contact', '"' . $campaign_id . '"');
            $query = $this->db->get('connected_data');
            echo $query->num_rows() . "\n";
            foreach ($query->result() as $row) {
                $Id = $row->id;
                $DBdata = [];
                $connected_contacts = $row->connected_contact;
                $connected_contacts_new = $connected_contacts = json_decode($connected_contacts, true);
                foreach ($connected_contacts as $ContactId => $Details) {
                    unset($connected_contacts_new[$ContactId]["Sequence"]["$campaign_id"]);
                }
                $connected_contacts_new = json_encode($connected_contacts_new);
                $DBdata['connected_contact'] = $connected_contacts_new;
                $this->db->where('id', $Id);
                $this->db->update('connected_data', $DBdata);
            }
            echo "Updated" . "<br>\n";
        }

    }

    public function remove_campaign_before_cd_field_date_get()
    {
        // end all the running sequence.
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $param = $this->get();
        $param['field_id'] = isset($param['field_id']) ? $param['field_id'] : "field_jdlih719";
        $param['date'] = isset($param['date']) ? $param['date'] : "2018-05-22";
        echo "campaign id: " . $param['campaign_id'] . "\n";
        echo "field: " . $param['field_id'] . "\n"; //field_jdlih719
        echo "date: " . $param['date'] . "\n";
        if (isset($param['campaign_id'])) {
            $campaign_id = $param['campaign_id'];
            $this->db->like('connected_contact', '"' . $campaign_id . '"');
            $query = $this->db->get('connected_data');
            echo $query->num_rows() . "\n";
            foreach ($query->result() as $row) {
                $Id = $row->id;
                $connected_contacts = $row->connected_contact;
                $value = json_decode($row->value, true);
                $CD_date_stamp = (int)strtotime($value[$param['field_id']]);
                $Mark_date_stamp = (int)strtotime($param['date']);

                if ($CD_date_stamp >= $Mark_date_stamp) continue;

                echo "To Update " . "$Id $CD_date_stamp < $Mark_date_stamp <br>\n";
                $DBdata = [];
                $connected_contacts_new = $connected_contacts = json_decode($connected_contacts, true);
                foreach ($connected_contacts as $ContactId => $Details) {
                    $connected_contacts_new[$ContactId]["Sequence"]["$campaign_id"]['Status'] = 'end';
                    unset($connected_contacts_new[$ContactId]["Sequence"]["$campaign_id"]['NextSequence']);
                    unset($connected_contacts_new[$ContactId]["Sequence"]["$campaign_id"]['MoveAfter']);
                    unset($connected_contacts_new[$ContactId]["Sequence"]["$campaign_id"]['MoveWhen']);
                }
                $connected_contacts_new = json_encode($connected_contacts_new);
                $DBdata['connected_contact'] = $connected_contacts_new;
                $this->db->where('id', $Id);
                $this->db->update('connected_data', $DBdata);
            }
            echo "Updated" . "<br>\n";
        }

    }

    public function notetag_get()
    {
        $NoteIds = [];
        $NoteIdsIndex = 0;
        $query = $this->db->get('note_tags');
        foreach ($query->result() as $row) {
            if (!is_numeric($row->note_id)) continue;
            if (sizeof($NoteIds[$NoteIdsIndex]) > 999) $NoteIdsIndex++;
            $NoteIds[$NoteIdsIndex][] = (int)$row->note_id;

        }
        print_r($NoteIds);
        $action = "query_is";
        foreach ($NoteIds as $Index => $Batch) {
            $action_details = '{"table":"ContactAction","limit":"1000","page":0,"fields":["Id","ContactId"],"query":{"Id":' . json_encode($Batch) . '}}';
            $Notes = applyFn('rucksack_request', $action, $action_details);
            print_r($Notes);

        }
    }

    public function show_contact_by_get()
    {
        // end all the running sequence.
        header("Content-Type: text/plain");
        error_reporting(E_ALL);
        $param = $this->get();
        $param['campaign_id'] = isset($param['campaign_id']) ? $param['campaign_id'] : false;
        if ($param['campaign_id'] == false) {
            echo 'Missing campaign_id';
            return false;
        }
        $ConnectorTabsEncoded = macanta_get_config('connected_info');
        $ConnectorTabs = json_decode($ConnectorTabsEncoded, true);
        $ConnectedData = [];
        $ConnectedDataFields = [];
        $ConnectedDataTitle = [];
        $ConnectedDataTitleId = [];
        foreach ($ConnectorTabs as $key => $ConnectorTab) {
            $ConnectedData[$ConnectorTab['id']] = ['Name' => $ConnectorTab['title']];
            $ConnectedDataFields[$ConnectorTab['id']] = $ConnectorTab['fields'];

            $Order = [];
            $OrderId = [];
            foreach ($ConnectorTab['fields'] as $field) {
                if ($field["showInTable"] == "yes") {
                    $key = (int)$field["showOrder"];
                    if (isset($Order[$key])) {
                        while (isset($order[$key])) {
                            $key++;
                        }
                        $Order[$key] = $field["fieldLabel"];
                        $OrderId[$key] = $field["fieldId"];
                    } else {
                        $Order[$key] = $field["fieldLabel"];
                        $OrderId[$key] = $field["fieldId"];
                    }
                }
            }
            ksort($Order);
            ksort($OrderId);
            $ConnectedDataTitle[$ConnectorTab['id']] = $Order;
            $ConnectedDataTitleId[$ConnectorTab['id']] = $OrderId;

        }


        $this->db->like('connected_contact', '"' . $param['campaign_id'] . ".");
        $this->db->like('connected_contact', '"Status":"end"');
        $this->db->like('connected_contact', '"MoveAfter"');
        $query = $this->db->get('connected_data');
        $CDCC = [];
        foreach ($query->result() as $row) {

            $Id = $row->id;
            $Group = $row->group;
            $Value = json_decode($row->value, true);
            $ConnectedContacts = json_decode($row->connected_contact, true);
            foreach ($ConnectedContacts as $ContactId => $Details) {
                $ContactInfo = ["Name" => $Details['FirstName'] . " " . $Details['LastName'], "Email" => $Details['Email']];
                foreach ($Details['Sequence'] as $CampaignStage => $CampaignDetails) {
                    if (strpos($CampaignStage, $param['campaign_id']) !== false && isset($CampaignDetails['MoveAfter'])) {
                        $CampaignDetails['MoveAfter'] = date('Y-m-d H:i:s', $CampaignDetails['MoveAfter']);
                        $CampaignDetails['StartedOn'] = date('Y-m-d H:i:s', $CampaignDetails['StartedOn']);
                        //unset($CampaignDetails['Parsed']);
                        $ItemDetails = [];
                        $ShowOnly = 2;
                        $Count = 0;
                        foreach ($ConnectedDataTitle[$Group] as $key => $FieldName) {
                            $Count++;
                            $ItemDetails[$FieldName] = $Value[$ConnectedDataTitleId[$Group][$key]];
                            if ($Count == $ShowOnly) break;
                        }
                        $CDCC[$ConnectedData[$Group]['Name']]['Items'][$Id]['Details'] = $ItemDetails;
                        $CDCC[$ConnectedData[$Group]['Name']]['Items'][$Id]['Contacts'][$ContactId][$CampaignStage] = $CampaignDetails;
                        $CDCC[$ConnectedData[$Group]['Name']]['Items'][$Id]['Contacts'][$ContactId]["ContactDetails"] = $ContactInfo;
                    }
                }
            }

        }
        print_r($CDCC);
    }

    public function date_test_get()
    {
        header("Content-Type: text/plain");
        error_reporting(E_ERROR);
        /*$date = "13/07/2018";
        $date2 = "07/13/2018";
        echo "$date\n";
        $string = isGBDate($date) ? str_replace("/","-",$date):$date;
        echo date('Y-m-d',strtotime($string))."\n<br>";
        echo date('Y-m-d',strtotime($date2));*/
        echo date('Y-m-d H:i:s') . "\n";
        echo date('Y-m-d H:i:s', strtotime("4 Days 3 AM"));
    }

    public function emails_get()
    {
        header("Content-Type: text/plain");
        error_reporting(E_ERROR);
        $GetMore = true;
        $TotalHistory = [];
        $Offset = 0;
        $Limit = 1000;
        while ($GetMore == true) {
            $EmailHistory = infusionsoft_get_email_history(3354, $Limit, $Offset);
            //print_r($EmailHistory);
            if (sizeof($EmailHistory->emails) < 1000) $GetMore = false;
            $TotalHistory = array_merge($TotalHistory, $EmailHistory->emails);
            if (isset($EmailHistory->next)) {
                $parts = parse_url($EmailHistory->next);
                parse_str($parts['query'], $query);
                if (isset($query['offset'])) {
                    $Offset = $query['offset'];
                }
            }
        }
        print_r($TotalHistory);
    }

    public function validate_phone_get()
    {
        header("Content-Type: text/plain");
        error_reporting(E_ERROR);
        $results = macanta_validate_phone_number("205", "+63025023034", "Philippines");
        print_r($results);
    }

    public function Profile_get()
    {
        $AppCountryCode = infusionsoft_get_app_account_profile()->message->address->country_code;
        $d_code = getCountryCode($AppCountryCode, false, true);
        echo "$AppCountryCode $d_code";
    }

    public function email_get()
    {
        header("Content-Type: text/plain");
        error_reporting(E_ERROR);
        $json = macanta_validate_email('peter@conquerthechaos.org');
        print_r($json);

    }

    public function fullcontact_get()
    {
        header("Content-Type: text/plain");
        error_reporting(E_ALL);
        print_r(macanta_full_contact(364, 'geover@gmail.com'));
    }

    public function server_var_get()
    {
        header("Content-Type: text/plain");
        print_r($_SERVER);
    }

    public function app_features_get()
    {
        $features = macanta_get_app_features();
        print_r(json_decode($features, true));
    }

    public function checklogin_get()
    {
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        $return = [
            "status" => false,
            "data" => []
        ];
        $PostData = $this->get();
        $Email = $PostData['Email'];
        $Password = $PostData['Password'];
        $results = infusionsoft_get_contact_by_email_password($Email, $Password);
        $Contact = $results->message[0];
        if (isset($Contact->Email) && isset($Contact->Password)) {
            $return["status"] = true;
            $return["data"] = $Contact;
            $this->response($return, REST_Controller::HTTP_OK);
        }
        $this->response($return, REST_Controller::HTTP_OK);

    }

    public function contact_get()
    {
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        $PostData = $this->get();
        $ContactId = $PostData['ContactId'];
        $Contact = infusionsoft_get_contact_by_id($ContactId, array('all'));
        var_dump($Contact);
        $this->response($Contact, REST_Controller::HTTP_OK);
    }

    public function upload_get()
    {
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        /*$b2_upload = macanta_b2_upload("/var/www/macanta/qj311/production/current/public/assets/custom_img/file_preview/thumb-maxresdefault.png", "test", "CD_ATTACHMENT");
        print_r($b2_upload);*/
        $im = new Imagick();
        print_r($im->queryFormats());
    }

    public function filebox_test_get()
    {
        //print_r(infusionsoft_update_contact($data, 554));
        //echo macanta_b2_download_file_by_name(364,"MyAvatar.jpg", false);
        //$DecodedFile =macanta_b2_download_file_by_fullname('qj311-dm/364/MyAvatar.jpg', true);
        /*$action_details = '{"id":554,"fileName":"MyAvatar.jpg","EncodedData":"'.$DecodedFile.'"}';
        $Upload = applyFn('rucksack_request','uploadFile', $action_details);*/
        $TheFile = infusionsoft_get_file_by_id(308311, 'no')->message;
        print_r($TheFile);
    }

    public function file_get()
    {
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $opts = array(
            'http' => array(
                'method' => "GET",
                'header' => "Accept-language: en\r\n" .
                    "Cookie: foo=bar\r\n"
            )
        );

        $context = stream_context_create($opts);
        $theFileData = parse_url("https://docs.google.com/spreadsheets/d/1RA6qno4lNHt2aqtkveEw8VCHpoVfChOHhxrJCpj0TVw/edit#gid=0");
        print_r($theFileData);
        $theFileData = pathinfo("https://docs.google.com/spreadsheets/d/1RA6qno4lNHt2aqtkveEw8VCHpoVfChOHhxrJCpj0TVw/edit#gid=0");
        print_r($theFileData);
    }

    public function puthook_get()
    {
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        macanta_migrate_centralised();
    }

    public function delhook_get()
    {
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        macanta_delete_hook_subscription();
    }

    public function email_history_get()
    {
        infusionsoft_get_email_history($ContactId, $limit = 1000, $offset = 0);
    }

    public function dup_delete_get($table)
    {
        header("Content-Type: text/plain");
        echo "Checking $table\n";
        $query = $this->db->query('SELECT  Id, COUNT(Id) as Duplicate FROM ' . $table . ' GROUP BY Id HAVING Duplicate > 1');
        echo "Duplicate Count: " . $query->num_rows() . "\n";
        foreach ($query->result() as $row) {
            $Limit = $row->Duplicate - 1;
            $Id = $row->Id;
            if ($Id == 0) continue;
            $this->db->where('Id', $Id);
            $this->db->limit($Limit);
            $this->db->delete($table);
            echo "Deleted: " . $Id . " Limit: $Limit \n";
        }
        /*$query = $this->db->query('SELECT Id, FirstName, LastName, Email, count(*) as Duplicate FROM InfusionsoftContact GROUP BY FirstName, LastName HAVING Duplicate > 1');
        foreach ($query->result() as $row)
        {
            $Limit = $row->Duplicate - 1;
            if($row->FirstName == '' && $row->LastName == '') continue;

            $this->db->where('Id',$Id);
            $this->db->limit($Limit);
            $this->db->delete($table);
            echo "Deleted: ".$Id." Limit: $Limit \n";
        }*/
        $this->response('Done', REST_Controller::HTTP_OK);
    }

    public function sync_delete_get($table)
    {
        header("Content-Type: text/plain");
        echo "Checking $table\n";
        $this->db->where('TableName', $table);
        $Result = $this->db->delete('SyncProcess');
        $this->response($Result, REST_Controller::HTTP_OK);
    }

    public function indextable_get()
    {
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '2000M');
        echo "Checking InfusionsoftContactGroupAssign\n";
        /*
        DELETE FROM yourTable WHERE field in (Select identField1, identField2, count(*) FROM yourTable
        GROUP BY identField1, identField2
        HAVING count(*) >1 )

        ALTER IGNORE TABLE InfusionsoftContactGroupAssign ADD UNIQUE ContactId_GroupId (ContactId,GroupId)

        Select ContactId, GroupId, count(*) FROM InfusionsoftContactGroupAssign
                        GROUP BY ContactId, GroupId
                        HAVING count(*) >1
        */
        $query = $this->db->query('Select ContactId, GroupId, count(*) as Duplicate FROM InfusionsoftContactGroupAssign GROUP BY ContactId, GroupId HAVING count(*) >1');
        echo "Duplicate Count: " . $query->num_rows() . "\n";
        foreach ($query->result() as $row) {
            $Limit = $row->Duplicate - 1;
            $this->db->where('ContactId', $row->ContactId);
            $this->db->where('GroupId', $row->GroupId);
            $this->db->limit($Limit);
            $this->db->delete('InfusionsoftContactGroupAssign');
        }
        $this->db->query('SET foreign_key_checks = 0;');
        $query = 'ALTER TABLE InfusionsoftContactGroupAssign ADD UNIQUE ContactId_GroupId (ContactId, GroupId);';
        $Result = $this->db->query($query);
        $Error = $this->db->error();
        echo "InfusionsoftContactGroupAssign Done: " . json_encode($Result) . " Error:" . json_encode($Error) . "\n";
        $this->response('Done', REST_Controller::HTTP_OK);
    }

    public function test_get()
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        /*infusionsoft_get_macanta_user(true);
        infusionsoft_get_macanta_user(true);*/
        /*$action = "query_is";
        $GroupId = 176;
        $action_details = '{"table":"ContactGroupAssign","limit":"1000","page":0,"fields":["ContactId"],"query":{"GroupId":' . $GroupId . '}}';
        $temp = applyFn('rucksack_request',$action, $action_details,false);
        $Ids = [];
        foreach ($temp->message as $Contact){
            $Ids[] = $Contact->ContactId;
        }
        echo "size: ".sizeof($Ids)."\n";
        print_r(implode(',',$Ids));*/
        echo $this->db->select('CustomField')->where('Id', 1782)->get('InfusionsoftContact')->row()->CustomField;
    }

    public function sei_filemaker_get()
    {
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '2000M');
        define('DBHOST', '159.203.80.134');
        define('DBUSER', 'sei_filemaker');
        define('DBNAME', 'sei_filemaker');
        define('DBPASS', 'wouldest-bawdy-yogi-euphony-cyclone-impress');
        $MariaDB = new PDO('mysql:host=' . DBHOST . ';', DBUSER, DBPASS);
        $Limit = 100;
        $Offset = 0;
        $Query = "SELECT  idnum, alias, avatarmod, AVCOMDT, BIRTHDATE, M1IDNUM, M2IDNUM, NeedTranslation, S2COMDT, SecLanguage 
			   FROM `" . DBNAME . "`.`Avatar` LIMIT $Limit OFFSET $Offset";
        $TableContent = $MariaDB->query(
            $Query,
            PDO::FETCH_ASSOC
        );
        $RowCount = $TableContent->rowCount();

        while ($Content = $TableContent->fetch(PDO::FETCH_ASSOC)) {
            print_r($Content);
        }
    }

    public function tag_get()
    {
        $tag = infusionsoft_get_tags_by_groupname_and_catId('Master-SECLANGU-Russian', 53);
        print_r($tag);
    }

    public function consumer_get()
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        ini_set('memory_limit', '2048M');
        $Message = '{"request":"start_campaign","data":{"restart_after":"2 hours","campaignId":"443","connected_data":"Opportunities","relationship":"Warehouse Manager or Logistics Manager"}}';
        $results = infusionsoft_connected_data_process(base64_encode($Message));
        print_r($results);
    }

    public function ConnectedDataHistory_get()
    {
        //tables to import for this script
        //ConnectedDataFileAttachement
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        header("Content-Type: text/plain");
        $Limit = $Results = 1000;
        $Offset = 0;
        $Created = 0;
        $Failed = 0;
        while ($Results >= 1000) {
            $this->db->limit($Limit, $Offset);
            $query = $this->db->get('connected_data_history');
            $Results = sizeof($query->result());
            if (sizeof($query->result()) > 0) {
                foreach ($query->result() as $row) {
                    // Record Connected Item
                    $ConnectedDataHistory = [
                        'ItemId' => $row->item_id,
                        'UpdatedBy' => $row->update_by,
                        'Type' => $row->type,
                        'Data' => $row->data,
                        'Created' => date('Y-m-d H:i:s', $row->time)
                    ];
                    $result = $this->db->insert('ConnectedDataHistory', $ConnectedDataHistory);
                    if ($result) {
                        $Created++;
                    } else {
                        $Failed++;
                    }
                    $Offset++;
                }
            }
        }

        echo "ConnectedDataHistory Import Starting...\n";
        echo " Total Imported: $Created\n";
        echo " Total Failed: $Failed\n";
        echo "ConnectedDataHistory Import Done!\n";
        echo "========================================\n\n";

    }

    public function ConnectedDataFileAttachement_get()
    {
        //tables to import for this script
        //ConnectedDataFileAttachement
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        header("Content-Type: text/plain");
        $Limit = $Results = 1000;
        $Offset = 0;
        $Created = 0;
        $Skipped = 0;

        while ($Results >= 1000) {
            $this->db->limit($Limit, $Offset);
            $query = $this->db->get('connected_data_file_attachment');
            $Results = sizeof($query->result());
            if (sizeof($query->result()) > 0) {
                foreach ($query->result() as $row) {
                    // Record Connected Item
                    $ConnectedDataFileAttachement = [
                        'ItemId' => $row->item_id,
                        'Filename' => $row->filename,
                        'Thumbnail' => $row->thumbnail,
                        'DownloadURL' => $row->download_url,
                        'FileSize' => $row->file_size,
                        'B2Filename' => $row->b2_filename,
                        'B2FileId' => $row->b2_file_id,
                        'Meta' => $row->meta
                    ];
                    $result = $this->db->insert('ConnectedDataFileAttachement', $ConnectedDataFileAttachement);
                    if ($result) {
                        $Created++;
                    } else {
                        //print_r($this->db->error());
                        $Skipped++;
                    }
                    $Offset++;
                }
            }
        }
        echo "ConnectedDataFileAttachement Import Starting...\n";
        echo " Total Imported: $Created\n";
        echo " Total Skipped: $Skipped\n";
        echo "ConnectedDataFileAttachement Import Done!\n";
        echo "========================================\n\n";
    }


    public function ConnectedDataSequence_get()
    {
        //tables to import for this script
        //ConnectedDataSequence
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        header("Content-Type: text/plain");
        $Limit = $Results = 1000;
        $Offset = 0;

        $CreatedSequence = 0;
        $UpdatedSequence = 0;
        $FailedSequence = 0;
        $SkippedSequence = 0;

        while ($Results >= 1000) {
            $this->db->limit($Limit, $Offset);
            $query = $this->db->get('connected_data');
            $Results = sizeof($query->result());
            if (sizeof($query->result()) > 0) {
                foreach ($query->result() as $row) {
                    $ItemId = $row->id;
                    $GroupId = $row->group;
                    $Meta = $row->meta;
                    $ConnectedContact = json_decode($row->connected_contact, true);
                    foreach ($ConnectedContact as $ContactId => $ConnectionDetails) {
                        // Record ConnectedDataSequence
                        $ConnectedDataSequenceArr = $ConnectionDetails['Sequence'];
                        foreach ($ConnectedDataSequenceArr as $SequenceId => $SequenceDetails) {
                            $ConnectedDataSequence = [
                                'ContactId' => $ContactId,
                                'GroupId' => $GroupId,
                                'ItemId' => $ItemId,
                                'InitialSequence' => $SequenceId,
                                'NextSequence' => isset($SequenceDetails['NextSequence']) ? $SequenceDetails['NextSequence'] : "",
                                'RestartAfter' => isset($SequenceDetails['MoveWhen']) ? $SequenceDetails['MoveWhen'] : "",
                                'MoveAfter' => isset($SequenceDetails['MoveAfter']) ? $SequenceDetails['MoveAfter'] : "",
                                'MoveWhen' => isset($SequenceDetails['MoveWhen']) ? $SequenceDetails['MoveWhen'] : "",
                                'Parsed' => isset($SequenceDetails['Parsed']) ? json_encode($SequenceDetails['Parsed']) : '',
                                'Status' => $SequenceDetails['Status'],
                                'Meta' => ''

                            ];

                            $result = $this->db->insert('ConnectedDataSequence', $ConnectedDataSequence);
                            if ($result) {
                                $CreatedSequence++;
                            } else {
                                unset($ConnectedDataSequence['ContactId']);
                                unset($ConnectedDataSequence['GroupId']);
                                unset($ConnectedDataSequence['ItemId']);
                                unset($ConnectedDataSequence['InitialSequence']);
                                $this->db->where('ContactId', $ContactId);
                                $this->db->where('GroupId', $GroupId);
                                $this->db->where('ItemId', $ItemId);
                                $this->db->where('InitialSequence', $SequenceId);
                                $result = $this->db->update('ConnectedDataSequence', $ConnectedDataSequence);
                                $AffectedRows = $this->db->affected_rows();
                                if ($result) {
                                    if ($AffectedRows > 0) {
                                        $UpdatedSequence++;
                                    } else {
                                        $SkippedSequence++;
                                    }
                                } else {
                                    $FailedSequence++;
                                }
                            }


                        }
                    }
                    $Offset++;
                }
            }
        }

        echo "ConnectedDataSequence Import Starting...\n";
        echo " Total Imported: $CreatedSequence\n";
        echo " Total Updated: $UpdatedSequence\n";
        echo " Total Skipped: $SkippedSequence\n";
        echo " Total Failed: $FailedSequence\n";
        echo "ConnectedDataSequence Import Done!\n";
        echo "========================================\n\n";

    }

    public function ConnectedDataContact_get()
    {
        //tables to import for this script
        //ConnectedDataContact
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        header("Content-Type: text/plain");
        $Limit = $Results = 1000;
        $Offset = 0;
        $CreatedContactRelation = 0;
        $SkippedContactRelation = 0;

        while ($Results >= 1000) {
            $this->db->limit($Limit, $Offset);
            $query = $this->db->get('connected_data');
            $Results = sizeof($query->result());
            if (sizeof($query->result()) > 0) {
                foreach ($query->result() as $row) {
                    $ItemId = $row->id;
                    $ConnectedContact = json_decode($row->connected_contact, true);
                    // Record ConnectedDataContact
                    foreach ($ConnectedContact as $ContactId => $ConnectionDetails) {
                        $RelationshipIdArr = $ConnectionDetails['relationships'];
                        foreach ($RelationshipIdArr as $RelationshipId) {
                            $ConnectedDataContact = [
                                'ContactId' => $ContactId,
                                'ItemId' => $ItemId,
                                'RelationshipId' => $RelationshipId
                            ];
                            $result = $this->db->insert('ConnectedDataContact', $ConnectedDataContact);
                            if ($result) {
                                $CreatedContactRelation++;
                            } else {
                                $SkippedContactRelation++;
                            }
                        }
                    }
                    $Offset++;
                }
            }
        }

        echo "ConnectedDataContact Import Starting...\n";
        echo " Total Imported: $CreatedContactRelation\n";
        echo " Total Skipped: $SkippedContactRelation\n";
        echo "ConnectedDataContact Import Done!\n";
        echo "========================================\n\n";
    }

    public function ConnectedDataItemFieldValue_get()
    {
        //tables to import for this script
        //ConnectedDataItemFieldValue
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        header("Content-Type: text/plain");
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 86400);
        $Limit = $Results = 1000;
        $Offset = 0;

        $CreatedItemFieldValue = 0;
        $UpdatedItemFieldValue = 0;
        $FailedItemFieldValue = 0;
        $SkippedItemFieldValue = 0;

        while ($Results >= 1000) {
            $this->db->limit($Limit, $Offset);
            $query = $this->db->get('connected_data');
            $Results = sizeof($query->result());
            if (sizeof($query->result()) > 0) {
                foreach ($query->result() as $row) {
                    $ItemId = $row->id;
                    $ItemFieldValueArr = json_decode($row->value, true);

                    // Record ConnectedDataItemFieldValue
                    foreach ($ItemFieldValueArr as $FieldId => $FieldValue) {
                        $ConnectedDataItemFieldValue = [
                            'ItemId' => $ItemId,
                            'FieldId' => $FieldId,
                            'FieldValue' => $FieldValue
                        ];
                        $result = $this->db->insert('ConnectedDataItemFieldValue', $ConnectedDataItemFieldValue);
                        if ($result) {
                            $CreatedItemFieldValue++;
                        } else {
                            unset($ConnectedDataItemFieldValue['ItemId']);
                            unset($ConnectedDataItemFieldValue['FieldId']);
                            $this->db->where('ItemId', $ItemId);
                            $this->db->where('FieldId', $FieldId);
                            $result = $this->db->update('ConnectedDataItemFieldValue', $ConnectedDataItemFieldValue);
                            $AffectedRows = $this->db->affected_rows();
                            if ($result) {
                                if ($AffectedRows > 0) {
                                    $UpdatedItemFieldValue++;
                                } else {
                                    $SkippedItemFieldValue++;
                                }
                            } else {
                                $FailedItemFieldValue++;
                            }
                        }

                    }
                    $Offset++;
                }
            }
        }
        echo "ConnectedDataItemFieldValue Import Starting...\n";
        echo " Total Imported: $CreatedItemFieldValue\n";
        echo " Total Updated: $UpdatedItemFieldValue\n";
        echo " Total Skipped: $SkippedItemFieldValue\n";
        echo " Total Failed: $FailedItemFieldValue\n";
        echo "ConnectedDataItemFieldValue Import Done!\n";
        echo "========================================\n\n";

    }

    public function ConnectedDataItem_get()
    {
        //tables to import for this script
        //ConnectedDataItem
        //ConnectedDataSequence
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        header("Content-Type: text/plain");
        $Limit = $Results = 1000;
        $Offset = 0;

        $CreatedItem = 0;
        $UpdatedItem = 0;
        $FailedItem = 0;
        $SkippedItem = 0;

        while ($Results >= 1000) {
            $this->db->limit($Limit, $Offset);
            $query = $this->db->get('connected_data');
            $Results = sizeof($query->result());
            if (sizeof($query->result()) > 0) {
                foreach ($query->result() as $row) {
                    $ItemId = $row->id;
                    $GroupId = $row->group;
                    $Meta = $row->meta;
                    // Record Connected Item
                    $ConnectedDataItem = [
                        'ItemId' => $ItemId,
                        'GroupId' => $GroupId,
                        'Meta' => $Meta
                    ];
                    $result = $this->db->insert('ConnectedDataItem', $ConnectedDataItem);
                    if ($result) {
                        $CreatedItem++;
                    } else {
                        unset($ConnectedDataItem['ItemId']);
                        $this->db->where('ItemId', $ItemId);
                        $result = $this->db->update('ConnectedDataItem', $ConnectedDataItem);
                        $AffectedRows = $this->db->affected_rows();
                        if ($result) {
                            if ($AffectedRows > 0) {
                                $UpdatedItem++;
                            } else {
                                $SkippedItem++;
                            }
                        } else {
                            $FailedItem++;
                        }
                    }
                    $Offset++;
                }
            }
        }

        echo "ConnectedDataItem Import Starting...\n";
        echo " Total Imported: $CreatedItem\n";
        echo " Total Updated: $UpdatedItem\n";
        echo " Total Skipped: $SkippedItem\n";
        echo " Total Failed: $FailedItem\n";
        echo "ConnectedDataItem Import Done!\n";
        echo "========================================\n\n";

    }

    public function ConnectedDataGroupRelationship_get()
    {
        //tables to import for this script
        //ConnectedDataGroupRelationship
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        header("Content-Type: text/plain");
        $connected_info_encoded = macanta_get_config('connected_info');
        $connected_info = json_decode($connected_info_encoded, true);

        $CreatedGroupRelationship = 0;
        $UpdatedGroupRelationship = 0;
        $FailedGroupRelationship = 0;
        $SkippedGroupRelationship = 0;
        foreach ($connected_info as $GroupId => $GroupDetails) {
            $Relationships = $GroupDetails['relationships'];
            foreach ($Relationships as $RelationshipsDetails) {
                $ConnectedDataGroupRelationship = [
                    'GroupId' => $GroupDetails['id'],
                    'RelationshipId' => $RelationshipsDetails['Id'],
                    'Exclusive' => $RelationshipsDetails['exclusive'],
                    'Limit' => $RelationshipsDetails['limit']
                ];
                $result = $this->db->insert('ConnectedDataGroupRelationship', $ConnectedDataGroupRelationship);
                if ($result) {
                    $CreatedGroupRelationship++;
                } else {
                    unset($ConnectedDataGroupRelationship['GroupId']);
                    unset($ConnectedDataGroupRelationship['RelationshipId']);
                    $this->db->where('GroupId', $GroupDetails['id']);
                    $this->db->where('RelationshipId', $RelationshipsDetails['Id']);
                    $result = $this->db->update('ConnectedDataGroupRelationship', $ConnectedDataGroupRelationship);
                    $AffectedRows = $this->db->affected_rows();
                    if ($result) {
                        if ($AffectedRows > 0) {
                            $UpdatedGroupRelationship++;
                        } else {
                            $SkippedGroupRelationship++;
                        }
                    } else {
                        $FailedGroupRelationship++;
                    }
                }

            }


        }

        echo "ConnectedDataGroupRelationship Import Starting...\n";
        echo " Total Imported: $CreatedGroupRelationship\n";
        echo " Total Updated: $UpdatedGroupRelationship\n";
        echo " Total Skipped: $SkippedGroupRelationship\n";
        echo " Total Failed: $FailedGroupRelationship\n";
        echo "ConnectedDataGroupRelationship Import Done!\n";
        echo "========================================\n\n";
    }

    public function ConnectedDataField_get()
    {
        //tables to import for this script
        //ConnectedDataGroup
        //ConnectedDataField
        //ConnectedDataGroupRelationship
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        header("Content-Type: text/plain");
        $connected_info_encoded = macanta_get_config('connected_info');
        $connected_info = json_decode($connected_info_encoded, true);

        $CreatedField = 0;
        $UpdatedField = 0;
        $FailedField = 0;
        $SkippedField = 0;
        $Order = 0;
        foreach ($connected_info as $GroupId => $GroupDetails) {
            $Fields = $GroupDetails['fields'];
            foreach ($Fields as $FieldDetails) {
                $FieldId = $FieldDetails['fieldId'];
                $ConnectedDataField = [
                    'FieldId' => $FieldId,
                    'GroupId' => $GroupDetails['id'],
                    'Label' => $FieldDetails['fieldLabel'],
                    'Order' => $Order,
                    'Type' => $FieldDetails['fieldType'],
                    'DataReference' => $FieldDetails['useAsPrimaryKey'] ? $FieldDetails['useAsPrimaryKey'] : 'no',
                    'Required' => $FieldDetails['requiredField'],
                    'ContactSpecific' => $FieldDetails['contactspecificField'] ? $FieldDetails['contactspecificField'] : 'no',
                    'DefaultValue' => $FieldDetails['defaultValue'],
                    'PlaceHolderText' => $FieldDetails['placeHolder'],
                    'HelperText' => $FieldDetails['helperText'],
                    'CustomField1' => $FieldDetails['infusionsoftCustomField'],
                    'UseAsListHeader' => $FieldDetails['showInTable'],
                    'ListHeaderOrder' => $FieldDetails['showOrder'],
                    'SectionTagId1' => $FieldDetails['sectionTagId'],
                ];
                $result = $this->db->insert('ConnectedDataField', $ConnectedDataField);
                if ($result) {
                    $Order++;
                    $CreatedField++;
                } else {
                    unset($ConnectedDataField['FieldId']);
                    $this->db->where('FieldId', $FieldId);
                    $result = $this->db->update('ConnectedDataField', $ConnectedDataField);
                    $AffectedRows = $this->db->affected_rows();
                    if ($result) {
                        if ($AffectedRows > 0) {
                            $UpdatedField++;
                        } else {
                            $SkippedField++;
                        }
                    } else {
                        $FailedField++;
                    }

                }
            }


        }

        echo "ConnectedDataField Import Starting...\n";
        echo " Total Imported: $CreatedField\n";
        echo " Total Updated: $UpdatedField\n";
        echo " Total Skipped: $SkippedField\n";
        echo " Total Failed: $FailedField\n";
        echo "ConnectedDataField Import Done!\n";
        echo "========================================\n\n";

    }

    public function ConnectedDataGroup_get()
    {
        //tables to import for this script
        //ConnectedDataGroup
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        header("Content-Type: text/plain");
        $connected_info_encoded = macanta_get_config('connected_info');
        $connected_info = json_decode($connected_info_encoded, true);

        $CreatedGroup = 0;
        $UpdatedGroup = 0;
        $FailedGroup = 0;
        $SkippedGroup = 0;
        $Order = 0;
        foreach ($connected_info as $GroupId => $GroupDetails) {
            $Label = $GroupDetails['title'];
            $ItemIdCustomField = $GroupDetails['item_id_custom_field'];
            $Permission = $GroupDetails['visibility'];
            $PermissionTag = $GroupDetails['permission_tag'];
            $ConnectedDataGroup = [
                'GroupId' => $GroupDetails['id'],
                'Label' => $Label,
                'ItemIdCustomField' => $ItemIdCustomField,
                'Permission' => $Permission,
                'PermissionTag' => $PermissionTag,
                'Order' => $Order
            ];

            $result = $this->db->insert('ConnectedDataGroup', $ConnectedDataGroup);
            if ($result) {
                $Order++;
                $CreatedGroup++;
            } else {
                unset($ConnectedDataGroup['GroupId']);
                $this->db->where('GroupId', $GroupDetails['id']);
                $result = $this->db->update('ConnectedDataGroup', $ConnectedDataGroup);
                $AffectedRows = $this->db->affected_rows();
                if ($result) {
                    if ($AffectedRows > 0) {
                        $UpdatedGroup++;
                    } else {
                        $SkippedGroup++;
                    }
                } else {
                    $FailedGroup++;
                }
            }
        }
        echo "ConnectedDataGroup Import Starting...\n";
        echo " Total Imported: $CreatedGroup\n";
        echo " Total Updated: $UpdatedGroup\n";
        echo " Total Skipped: $SkippedGroup\n";
        echo " Total Failed: $FailedGroup\n";
        echo "ConnectedDataGroup Import Done!\n";
        echo "========================================\n\n";

    }

    public function ConnectedDataRelationShip_get()
    {
        //tables to import for this script
        //ConnectedDataRelationship
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        header("Content-Type: text/plain");
        $ConnectorRelationship_encoded = macanta_get_config('ConnectorRelationship');
        $ConnectorRelationship = json_decode($ConnectorRelationship_encoded, true);
        $Created = 0;
        $Updated = 0;
        $Failed = 0;
        $Skipped = 0;
        echo "ConnectedDataRelationShip Import Starting...\n";
        foreach ($ConnectorRelationship as $Relationship => $RelationshipDetails) {
            $ConnectorRelationshipData = [
                'RelationshipId' => $RelationshipDetails['Id'],
                'Label' => $RelationshipDetails['RelationshipName'],
                'Description' => $RelationshipDetails['RelationshipDescription'],

            ];
            $result = $this->db->insert('ConnectedDataRelationship', $ConnectorRelationshipData);
            if ($result) {
                $Created++;
                //echo "ConnectedDataRelationship Created: $RelationshipDetails[RelationshipName]\n";
            } else {
                unset($ConnectorRelationshipData['RelationshipId']);
                $this->db->where('RelationshipId', $RelationshipDetails['Id']);
                $result = $this->db->update('ConnectedDataRelationship', $ConnectorRelationshipData);
                $AffectedRows = $this->db->affected_rows();
                if ($result) {
                    if ($AffectedRows > 0) {
                        $Updated++;
                    } else {
                        $Skipped++;
                    }
                } else {
                    $Failed++;
                }
            }
        }
        echo " Total Imported: $Created\n Total Updated: $Updated\n Total Skipped: $Skipped\n Total Failed: $Failed \n";
        echo "ConnectedDataRelationShip Import Done!\n";
        echo "========================================\n\n";
    }

    public function param_get($var1)
    {
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        header("Content-Type: text/plain");
        echo "$var1\n";
    }

    public function AddCDValue_get()
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 86400);
        $Map = [
            'ci_jor8956h' => 'field_js45f1wf',
            'ci_jor932u6' => 'field_js45l0wy',
            'ci_jor9hdaz' => 'field_js45lmr3',
            'ci_jottt3ab' => 'field_js45m4v1',
            'ci_jor9hdba' => 'field_js45mn8n',
            'ci_jor9hdbb' => 'field_js45n3eu',
            'ci_jorahc8y' => 'field_js45nknu',
            'ci_jorawz6o' => 'field_js45nzp1',
            'ci_jpl8kqjo' => 'field_js45ofkz'
        ];
        foreach ($Map as $GroupId => $FieldId) {
            $this->db->where('group', $GroupId);
            $query = $this->db->get('connected_data');
            echo "Total Items To Be Modified: " . $query->num_rows() . "<br>\n";
            foreach ($query->result() as $row) {
                $DBdata = [];
                $Id = $row->id;
                $ConnectedValues = json_decode($row->value, true);
                if (isset($ConnectedValues[$FieldId])) continue;
                $ConnectedValues[$FieldId] = 'Yes';
                $DBdata['value'] = json_encode($ConnectedValues);
                $this->db->where('id', $Id);
                $this->db->update('connected_data', $DBdata);
            }
        }


    }

    public function UnsetCDSequence_get()
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 86400);
        $this->db->like('connected_contact', '791.1');
        $query = $this->db->get('connected_data');
        echo "Total Items To Be Unset: " . $query->num_rows() . "<br>\n";
        foreach ($query->result() as $row) {
            $DBdata = [];
            $Id = $row->id;
            $ConnectedContact = $ConnectedContactNew = json_decode($row->connected_contact, true);
            foreach ($ConnectedContact as $ContactId => $ContactDetails) {
                unset($ConnectedContactNew[$ContactId]['Sequence']['791.1']);
            }
            $DBdata['connected_contact'] = json_encode($ConnectedContactNew);
            $this->db->where('id', $Id);
            $this->db->update('connected_data', $DBdata);
        }


    }

    public function ChangeCDRelationship_get()
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 86400);
        $TargetRelationship = 're_a052532c';
        $NewRelationship = 're_ca703c98';
        $this->db->like('connected_contact', $TargetRelationship);
        $query = $this->db->get('connected_data');

        foreach ($query->result() as $row) {
            $DBdata = [];
            $Id = $row->id;
            $ConnectedContact = $ConnectedContactNew = json_decode($row->connected_contact, true);
            foreach ($ConnectedContact as $ContactId => $ContactDetails) {
                foreach ($ContactDetails['relationships'] as $Index => $Relationship) {
                    if ($Relationship == $TargetRelationship) {
                        $ConnectedContactNew[$ContactId]['relationships'][$Index] = $NewRelationship;
                    }
                }
            }
            $DBdata['connected_contact'] = json_encode($ConnectedContactNew);
            $this->db->where('id', $Id);
            $this->db->update('connected_data', $DBdata);
        }
        echo "Done";

    }

    public function cd_get()
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        header("Content-Type: text/plain");
        /*$this->load->model('Connecteddata');
        $Results = $this->Connecteddata->get_items_by_group('Insurance Wins');
        //$Results = $this->Connecteddata->get_item_by_id('item_0150e250');
        $FieldValues = [
                'Premium:' => 'Select 1 and Select 2',
        ];*/
        //$Results = $this->Connecteddata->get_items_by_field_value($FieldValues,'Insurance Wins',364);
        //$Results = $this->Connecteddata->get_connected_contact('item_0150e250');
        //$Results = $this->Connecteddata->get_fields_by_group('Insurance Wins');
        //$Results = $this->Connecteddata->get_group();
        /*$AddRecordArr = [
            'Label' => 'Test Label',
            'ItemIdCustomField' => 'TheCustomFields',
            'Permission' => 'ShowCDToAllUserSpecific',
            'PermissionTag' => '1234',
            'Order' => 4,
            'Status' => 'active'
        ];
        $Results = $this->Connecteddata->add_record($AddRecordArr,'ConnectedDataGroup');*/
        $CurrTime = substr(time(), 0, -4);
        $this->db->like('connected_contact', '"NextSequence"');
        $this->db->like('connected_contact', '"MoveAfter":' . $CurrTime);

        $query = $this->db->get('connected_data');
        $num_rows = $query->num_rows();
        echo $num_rows . "\n";
        echo $this->db->last_query() . "\n";
        print_r($query->result());
    }

    public function opttag_get()
    {
        print_r(infusionsoft_get_opt_out_tag('Phone Opt-out'));
    }

    public function countRecord_get()
    {
        $action = "countRecord";
        $this->db->order_by('DateCreated', 'DESC');
        $this->db->limit(1);
        $query = '{"GroupId":"%"}';
        $GroupAssign = $this->db->get('InfusionsoftContactGroupAssign')->row();
        if (isset($GroupAssign)) {
            $LastDateCreatedJson = $GroupAssign->DateCreated;
            $LastDateCreated = json_decode($LastDateCreatedJson);
            $query = '{"DateCreated":"~>~ ' . $LastDateCreated->date . '"}';
        }
        $action_details = '{"table":"ContactGroupAssign","query":' . $query;
        echo $action_details;
        $Records = rucksack_request($action, $action_details);
        print_r($Records);
    }

    public function form_get()
    {
        infusionsoft_get_tags_applied('266707', $Force = false);
    }

    public function contactAction_get()
    {
        print_r(json_encode(infusionsoft_get_contact_notes_by_contact_id(1816)));
    }

    public function mcbr_get($InfusionsoftID)
    {
        //echo $InfusionsoftID;
        $filename = 'Macanta_Query_Builder_Results.csv';
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="' . $filename . '";');
        /*
        $this->db->where('Name','searched_cache'.$InfusionsoftID);
        $query = $this->db->get('InfusionsoftCache');
        $row = $query->row();
        $Content = json_decode($row->Content, true);
        */

        $searched_cache = manual_cache_loader('searched_cache' . $InfusionsoftID, true);
        $Content = json_decode($searched_cache, true);

        $delimiter = ',';
        $f = fopen('php://memory', 'w');
        fputcsv($f, array_keys($Content[0]), $delimiter);
        foreach ($Content as $line) {
            fputcsv($f, $line, $delimiter);
        }
        fseek($f, 0);
        fpassthru($f);
    }

    public function mt_get($SavedSearch)
    {
        //echo $InfusionsoftID;
        $IdArr = explode(':', $SavedSearch);
        $filename = 'Macanta_Query_Builder_Results.csv';
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="' . $filename . '";');
        $this->db->where('IdLocal', $IdArr[0]);
        $query = $this->db->get('InfusionsoftContact');
        $row = $query->row();
        $this->db->like('CustomField', '"_Taskassignedto":"' . $row->FirstName . ' ' . $row->LastName . '"');
        $this->db->where('Origin', 'Native');
        $query = $this->db->get('InfusionsoftContactAction');
        $MainIndex = 0;
        foreach ($query->result() as $row) {

            //$Contacts['NewResults'][$MainIndex]['ContactId'] = (int) $row->ContactId;
            $CreationDate = '';
            $CompletionDate = '';
            if (!empty($row->CreationDate)) {
                $CreationDateObj = json_decode($row->CreationDate);
                $CreationDate = date('Y-m-d', strtotime($CreationDateObj->date));
            }
            if (!empty($row->CompletionDate)) {
                $CompletionDateObj = json_decode($row->CompletionDate);
                $CompletionDate = date('Y-m-d', strtotime($CompletionDateObj->date));
            }
            $Content[$MainIndex]['ContactId'] = (int)$row->ContactId;
            $Content[$MainIndex]['Title'] = $row->CreationNotes;
            $Content[$MainIndex]['Task Note'] = $row->ActionDescription;
            $Content[$MainIndex]['Completion Date'] = $CompletionDate;
            $Content[$MainIndex]['Creation Date'] = $CreationDate;
            $Content[$MainIndex]['Id'] = (int)$row->IdLocal;
            $ContactInfo = infusionsoft_get_contact_by_id_simple((int)$row->ContactId, array('"FirstName"', '"LastName"', '"Email"', '"StreetAddress1"', '"City"', '"Country"', '"PostalCode"'), true);
            $Content[$MainIndex]['First Name'] = $ContactInfo[0]->FirstName;
            $Content[$MainIndex]['Last Name'] = $ContactInfo[0]->LastName;
            $Content[$MainIndex]['Email'] = $ContactInfo[0]->Email;
            $Content[$MainIndex]['Address'] = $ContactInfo[0]->StreetAddress1;
            $Content[$MainIndex]['City'] = $ContactInfo[0]->City;
            $Content[$MainIndex]['Country'] = $ContactInfo[0]->Country;
            $Content[$MainIndex]['ZipCode'] = $ContactInfo[0]->PostalCode;
            $MainIndex++;
        }
        $delimiter = ',';
        $f = fopen('php://memory', 'w');
        fputcsv($f, array_keys($Content[0]), $delimiter);
        foreach ($Content as $line) {
            fputcsv($f, $line, $delimiter);
        }
        fseek($f, 0);
        fpassthru($f);
    }

    public function query_contact_get()
    {
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        $PostData = $this->get();
        $api_key = isset($PostData['api_key']) ? $PostData['api_key'] : 0;
        $api_key = isset($PostData['access_key']) ? $PostData['access_key'] : $api_key;
        if ($this->checkAPIkey($api_key) == false)
            $this->response('Forbidden: Missing or Invalid API KEY', REST_Controller::HTTP_FORBIDDEN);

        $limit = isset($PostData['limit']) ? $PostData['limit'] : 1000;
        $page = isset($PostData['page']) ? $PostData['page'] : 0;
        $start = $limit * $page;
        $nextPage = $page + 1;
        $appname = $this->config->item('MacantaAppName');
        $this->db->select('Id, Title, FirstName, LastName, Email, Phone1, City, State, Country, PostalCode, ZipFour1, TimeZone, Groups, Company, LastUpdated, DateCreated');
        $this->db->order_by('Id');
        $this->db->limit($limit, $start);
        $query = $this->db->get('InfusionsoftContact');
        $result = $query->result();
        $resultCount = sizeof($query->result());
        $Response['app_name'] = $this->config->item('MacantaAppName');
        $Response['contacts'] = $result;
        $Response['count'] = $resultCount;
        $Response['next'] = 'https://' . $appname . '.macanta.' . EXT . '/contacts?limit=' . $limit . '&page=' . $nextPage . '&api_key=' . $api_key;
        $this->response($Response, REST_Controller::HTTP_OK);
    }

    public function add_user_get()
    {
        header("Content-Type: text/plain");
        ini_set('display_errors', 1);
        error_reporting(E_ERROR);
        $PostData = $this->get();
        $Response = [];
        $api_key = isset($PostData['api_key']) ? $PostData['api_key'] : 0;
        $api_key = isset($PostData['access_key']) ? $PostData['access_key'] : $api_key;
        if ($this->checkAPIkey($api_key) == false)
            $this->response('Forbidden: Missing or Invalid API KEY', REST_Controller::HTTP_FORBIDDEN);

        unset($PostData['api_key']);
        unset($PostData['access_key']);

        $RegularField = ['Address1Type', 'Address2Street1', 'Address2Street2', 'Address2Type', 'Address3Street1', 'Address3Street2', 'Address3Type',
            'AssistantName', 'AssistantPhone', 'Anniversary', 'BillingInformation', 'City', 'City2', 'City3', 'Company', 'CompanyID', 'ContactNotes', 'ContactType', 'Country',
            'Country2', 'Country3', 'Email', 'EmailAddress2', 'EmailAddress3', 'Fax1', 'Fax1Type', 'Fax2', 'Fax2Type', 'FirstName', 'JobTitle', 'Language', 'LastName', 'Leadsource', 'MiddleName', 'Nickname',
            'Password', 'Phone1', 'Phone1Ext', 'Phone1Type', 'Phone2', 'Phone2Ext', 'Phone2Type', 'Phone3', 'Phone3Ext', 'Phone3Type', 'Phone4', 'Phone4Ext',
            'Phone4Type', 'Phone5', 'Phone5Ext', 'Phone5Type', 'PostalCode', 'PostalCode2', 'PostalCode3', 'ReferralCode', 'SpouseName', 'State', 'State2', 'State3',
            'StreetAddress1', 'StreetAddress2', 'Suffix', 'TimeZone', 'Title', 'Username', 'Validated', 'Website', 'ZipFour1', 'ZipFour2', 'ZipFour3', 'Birthday', 'DateCreated', 'LastUpdated'
        ];
        $Data = [];
        foreach ($RegularField as $FieldName) {
            if (isset($PostData[$FieldName]) && !empty($PostData[$FieldName])) {
                $PostData[$FieldName] = str_replace(' ', '+', $PostData[$FieldName]);
                $Data[] = ['name' => $FieldName, 'value' => html_entity_decode($PostData[$FieldName])];
            }
        }
        if (sizeof($Data) > 0) {
            $AddedContact = infusionsoft_add_contact($Data);
            $ContactId = $AddedContact->message;
            $exec_string = '/usr/bin/php /var/www/macanta/shared/services/sync.php ' . $this->config->item('MacantaAppName') . ' 2 ' . $ContactId;
            exec($exec_string, $output);
            $Response['ContactId'] = $ContactId;

            $UserLevelTags = json_decode($this->config->item('access_level'));
            $StaffTag = $UserLevelTags->staff;
            $Response['AppliedTag'] = $AppliedTag = infusionsoft_apply_tag($ContactId, $StaffTag);
            $Contact = infusionsoft_get_contact_by_id($ContactId, array('all'));
            if (!isset($Contact->Password) || empty($Contact->Password)) {
                $Password = generateAdjNounPhrase();
                $data = array(0 => array('name' => 'Password', 'value' => $Password));
                $Response['UpdatedPassword'] = infusionsoft_update_contact($data, $ContactId, 'No');
            }

        }
        $this->response($Response, REST_Controller::HTTP_OK);
    }

    public function pre_login_get($Email)
    {
        $Email = str_replace(' ', '+', $Email);
        //check if Email is Macanta User
        if (filter_var($Email, FILTER_VALIDATE_EMAIL)) {
            $Contacts = infusionsoft_get_contact_by_email($Email);
            foreach ($Contacts as $SingleContact) {
                if (isset($SingleContact->CompanyID)) {
                    if ($SingleContact->CompanyID != $SingleContact->Id) {
                        $Contact = $SingleContact;
                        break;
                    }
                } else {
                    $Contact = $SingleContact;
                    break;
                }
            }
            if (isset($Contact->Groups)) {
                $Type = '';
                $tagsArr = explode(',', $Contact->Groups);
                $access_level = json_decode($this->config->item('access_level'), true);
                foreach ($access_level as $userType => $tag) {
                    if (in_array($tag, $tagsArr)) {
                        $Type = $userType;
                        break;
                    }
                }
                if ($Type != '') {
                    $Controller = "core/common";
                    $user_seession_data = macanta_get_user_seession_data_by_email($Email);
                    if ($user_seession_data == false) {
                        $MacantaUsers = macanta_get_users();
                        foreach ($MacantaUsers as $MacantaUser) {
                            if ($Email == $MacantaUser->Email) {
                                $loadedController = $this->load->controller($Controller);
                                $this->$loadedController->login($MacantaUser, true);
                                $user_seession_data = macanta_get_user_seession_data_by_email($Email);
                                $SessionName = $user_seession_data->session_name;
                                break;
                            }
                        }
                    } else {
                        $SessionName = $user_seession_data->session_name;
                    }
                }
            }
        }
        //print_r($Email);
        //print_r($user_seession_data);
        $Data['script'] = 'var session_name = "' . $SessionName . '";localStorage.setItem("session_name", "' . $SessionName . '");';
        echo '
        <script>
        localStorage.setItem("session_name", "' . $SessionName . '");
        window.location.replace("https://' . $_SERVER[HTTP_HOST] . '");
        </script>
        ';
    }

    public function trigger_get()
    {
        header("Content-Type: text/plain");
        //print_r(macanta_check_trigger_conditions());
        //print_r(macanta_execute_valid_triggers()) ;
        //print_r(macanta_get_connected_info_by_groupname("Service Calls",6,'',"item_0f01ca34"));
        //echo strtotime('Thursday');

        /*echo date("Y-m-d H:i",strtotime("2019-09-14t15:17:31.37"))."\n";
        echo "4 Days ". date("Y-m-d H:i", strtotime('4 Days'))."\n";
        echo strtotime(date("Y-m-d H:i",strtotime('now')))."\n" ;
        echo date("Y-m-d H:i")."\n" ;
        echo "2019-09-23: " . strtotime('2019-09-23')."\n";
        echo "x: " . date('x')."\n";
        echo macanta_generate_key('item_')."\n";*/
        //echo date("H:i:s", strtotime('2019-09-09 00:00:00'))."\n";
        echo date("Y-m-d H:i:s", strtotime('-7 days 00:00')) . "\n";
        echo date("Y-m-d H:i:s", strtotime('-7 days 23:59')) . "\n";
        echo date("Y-m-d H:i:s", strtotime('never')) . "\n";
        /*$Criteria = '(Holdings 2)008 Chamberlain Transport (Holdings) (Holdings 2)Ltd - 17';
        preg_match_all('/^\(([\(A-Za-z0-9\-_ \)]+?)\)/', $Criteria, $groups);
        print_r($groups);
        echo "\n".macanta_uniqid()."\n";
        echo "\n".macanta_generate_key()."\n";*/
    }

    public function remove_deleted_contact_get()
    {
        // end all the running sequence.
        header("Content-Type: text/plain");
        $this->db->where('meta_key', 'deleted');
        $this->db->where('meta_value', null);
        $query = $this->db->get('users_meta');
        echo "Total Deleted Contact To Remove: " . $query->num_rows() . "\n";
        foreach ($query->result() as $row) {
            echo "Deleting " . $row->user_id . ": ";
            $this->db->where('Id', $row->user_id);
            echo $this->db->delete('InfusionsoftContact');
            echo "\n";

            $this->db->where('user_id', $row->user_id);
            $this->db->where('meta_key', 'deleted');
            $DbData = [
                'meta_value' => 'removed'
            ];
            $this->db->update('users_meta', $DbData);

        }
        echo "Done" . "\n";;
    }

    public function macanta_generate_group_get()
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        header("Content-Type: text/plain");
        $this->load->model('Connecteddata');
        $this->Connecteddata->generate_regular_groups();
        $this->Connecteddata->migrate_contact_from_local();
    }

    public function hashtag_get()
    {
        $string = "The quick brown #fox jumped over the lazy #fat#dog";
        print_r(macanta_get_hashtags($string));
    }

    public function savedfilter_get()
    {
        var_dump(json_decode(false));
        $action = "query_is";
        $returnFields = array(
            '"FilterName"',
            '"Id"',
            '"ReportStoredName"',
            '"UserId"'
        );
        $returnFieldsStr = implode(', ', $returnFields);
        $page = 0;
        $action_details = '{"table":"SavedFilter","limit":"1000","page":' . $page . ',"fields":[' . $returnFieldsStr . '],"query":{"UserId":"~<>~-1"}}';
        $temp = applyFn('rucksack_request', $action, $action_details, false);
        print_r($temp);
    }

    public function segment_get()
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        header("Content-Type: text/plain");
        print_r(macanta_get_users());
    }

    public function stripo_get()
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        header('Content-Type: application/json; charset=UTF-8');
        $URL = 'https://plugins.stripo.email/api/v1/auth';
        $PostData = [
            'pluginId' => $_SERVER['STRIPO_APP_ID'],
            'secretKey' => $_SERVER['STRIPO_SECRET']
        ];
        $opts = array(
            'http' => array(
                'method' => 'POST',
                'header' =>
                    "Content-Type: application/json\r\n" .
                    "Accept: application/json\r\n"
            ,
                'content' => json_encode($PostData)
            )
        );
        $context = stream_context_create($opts);
        $file_contents = file_get_contents($URL, false, $context);
        echo $file_contents;
    }

    public function unsubscribe_get()
    {
        header("Content-Type: text/html");
        $PostData = $this->get();
        $contactId = isset($PostData['contactId']) ? $PostData['contactId']:'';
        $type = isset($PostData['type']) ? $PostData['type'] : '';
        $itemId = isset($PostData['item']) ? $PostData['item'] : '';
        $Message = "Error: Missing parameters";
        $Header = "OOPS! SOMETHINGS NOT RIGHT!";
        $Unsubscribed = false;
        if ($contactId != ''){
            if ($itemId != "") {
                $ItemDetails = macanta_db_record_exist('id', $itemId, 'connected_data', true);
                if ($ItemDetails) {
                    $GroupId = $ItemDetails->group;
                    $ItemValues = json_decode($ItemDetails->value, true);
                    $ConnectedInfoSettings = macanta_get_config('connected_info');
                    if ($ConnectedInfoSettings) {
                        $ConnectedInfoSettings = json_decode($ConnectedInfoSettings, true);
                        if (isset($ConnectedInfoSettings[$GroupId])) {
                            $HasOptInField = false;
                            $Fields = $ConnectedInfoSettings[$GroupId]['fields'];
                            $Title = $ConnectedInfoSettings[$GroupId]['title'];
                            foreach ($Fields as $FieldDetails) {
                                $FieldId = $FieldDetails['fieldId'];
                                $FieldLabel = $FieldDetails['fieldLabel'];
                                if (strtolower($FieldLabel) == 'opt-in') {
                                    $HasOptInField = true;
                                    break;
                                }
                            }
                            if ($HasOptInField == true) {
                                if (isset($ItemValues[$FieldId])) {
                                    $Header = " IT WON'T BE THE SAME WITHOUT YOU!";
                                    $Message = "Oops, But You are already not in our Subscribers list";
                                    if ($ItemValues[$FieldId] != "") {
                                        if (is_array($ItemValues[$FieldId])) {
                                            $FieldValueKey = "id_" . $contactId;
                                            if (isset($ItemValues[$FieldId][$FieldValueKey])) {
                                                $ItemValues[$FieldId][$FieldValueKey] = 'No';
                                                $NewItemValues = json_encode($ItemValues);;
                                                $Unsubscribed = true;

                                            }
                                        } else {
                                            $FieldValue = strtolower($ItemValues[$FieldId]);
                                            if ($FieldValue == 'yes' || $FieldValue == '1' || $FieldValue == 'true') {
                                                $ItemValues[$FieldId] = 'No';
                                                $NewItemValues = json_encode($ItemValues);
                                                $Unsubscribed = true;
                                            }
                                        }
                                    }
                                    if ($Unsubscribed == true) {
                                        $DBdata = [
                                            'value' => $NewItemValues
                                        ];
                                        $this->db->where('id', $itemId);
                                        $this->db->update('connected_data', $DBdata);
                                        $Message = "You've been successfully unsubscribed from <strong>$Title</strong> notifications";
                                    }
                                } else {
                                    $Message = 'Error: No Opt-In Field Saved For This Data Object Item';
                                }
                            } else {
                                $Message = 'Error: Unable to Subscribe To this Item('.$itemId.') of <strong>' . $Title . "</strong>.<br><strong>No Opt-In Field</strong>";
                            }

                        } else {
                            $Message = 'Error: Connected Info <strong>' . $GroupId . '</strong> Not Existing';
                        }
                    } else {
                        $Message = 'Error: No Connected Info Settings';
                    }
                }
                else {
                    $Message = "Error: Item Subscription Not Found";
                }

            }
            else{
                if ($type != ""){
                    $ConnectedInfoSettings = macanta_get_config('connected_info');
                    if ($ConnectedInfoSettings) {
                        $ConnectedInfoSettings = json_decode($ConnectedInfoSettings, true);
                        foreach ($ConnectedInfoSettings as $GroupId => $GroupDetails){
                            $Fields = $GroupDetails['fields'];
                            foreach ($Fields as $FieldDetails) {
                                $FieldId = $FieldDetails['fieldId'];
                                $FieldLabel = $FieldDetails['fieldLabel'];
                                if (strtolower($FieldLabel) == 'opt-in') {
                                    $HasOptInField = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }else{

        }
        $Param=[
            'Header'=>$Header,
            'Message'=>$Message
            ];
        $HTML = $this->load->view('unsubscribe', $Param, true); // return the view
        echo $HTML;
    }

    public function stripo_html_get()
    {

        $PostData = $this->get();
        $HTML = false;
        if (isset($PostData['HTML']) && $PostData['HTML'] == 'yes') {

        } else {
            if (!isset($PostData['Type'])) {
                $PostData['Type'] = "CompiledEmailHTML";
                $HTML = true;
            } else {
                //header("Content-Type: text/plain");
                header("Content-Type: application/json");
            }

        }
        if (!isset($PostData['EmailId']) || !isset($PostData['Type'])) die("Missing Required Field");
        macanta_check_macantastripo_email_table();
        $EmailId = $PostData['EmailId'];
        $Type = $PostData['Type'];

        $this->db->where('EmailId', $EmailId);

        if (!isset($PostData['Status']))
            $this->db->where('Status', 'active');

        if (isset($PostData['version']))
            $this->db->where('Version', $PostData['version']);

        if (isset($PostData['templateName']))
            $this->db->where('Title', $PostData['templateName']);

        $query = $this->db->get('MacantaStripoEmail');
        //echo $this->db->last_query();
        $DetailsOfChosen = $query->row();
        if (!$DetailsOfChosen) {
            //get_default template
            $DetailsOfChosen = macanta_db_record_exist('EmailId', 'default-macanta', 'MacantaStripoEmail', true);
        }
        if ($HTML) {
            echo base64_decode($DetailsOfChosen->$Type);
        } else {
            echo json_encode($DetailsOfChosen);
        }
    }

    function thumb_get()
    {
        $BuiltInTemplates = macanta_get_email_templates("cars-fathers-day-newsletter");
        $src = "";
        foreach ($BuiltInTemplates as $BuiltInTemplate) {
            $src = macanta_get_web_screenshot($BuiltInTemplate->CompiledEmailHTML, $BuiltInTemplate->EmailId, true);
        }

        echo "<img src=\"" . $src . "\" />";
    }

    function thumb2_get()
    {
        $FileNamePDF = manual_cache_location() . "cars-fathers-day-newsletter.txt";
        macanta_grab_image("https://qj311-dm.macanta.test/ajax/stripo_html?EmailId=cars-fathers-day-newsletter", $FileNamePDF);
    }
}